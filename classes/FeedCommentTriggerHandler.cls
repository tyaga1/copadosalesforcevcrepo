/**=====================================================================
 * Name: FeedCommentTriggerHandler
 * Description:
 * Created Date: Aug 23th, 2017
 * Created By: Mauricio Murillo
 *
 * Date Modified                Modified By       Description of the update
 /**=====================================================================*/
 
 public without sharing class FeedCommentTriggerHandler {
 
    //Before Delete Call
    public static void beforeDelete (List<FeedComment> oldList, Boolean isDataAdmin) {
    
        if (isDataAdmin == false){
            ID currentUserId = UserInfo.getUserId();  
            ID currentUserProfId = UserInfo.getProfileId();
            Boolean isAdmin = false;
            
            String profileName = [SELECT Name FROM Profile WHERE Id = :currentUserProfId].Name;
            // Check if the User belongs to certain profiles and avoid the validations - Certified Sales Admin or Sales Effectiveness
            if(profileName == Constants.PROFILE_EXP_CERT_SALES_ADMIN || 
               profileName == Constants.PROFILE_EXP_SALES_ADMIN || 
               profileName == Constants.PROFILE_SYS_ADMIN || 
               profileName == Constants.PROFILE_EXP_DQ_ADMIN){
               isAdmin = true;      
            }
            
            if (!isAdmin){
                for(FeedComment fc : oldList){
                    if( currentUserId != fc.CreatedById){
                      fc.addError('You cannot delete a post you did not create.');
                    }
                }
            }
        }
    }
 
 }