@isTest
private class ResellerAndStratsCommPrivateDoc_Test {
    
    @isTest static void getPrivateDocsTest() {
        Account testAccount1 = Test_Utils.createAccount();
        insert testAccount1;

        Contact testContact1 = Test_Utils.createContact(testAccount1.id);
        insert testContact1;

/*
        User testUser = Test_Utils.createUser('Xperian Global Community ReSellers');
        testUser.Contactid = testContact1.id;
        insert testUser;
        */

        Confidential_Information__c confInfoStrat = new Confidential_Information__c();
        confInfoStrat.Account__c = testAccount1.id;
        confInfoStrat.Document_Type__c = 'Strat Clients Document';
        insert confInfoStrat;

        Confidential_Information__c confInfoReseller = new Confidential_Information__c();
        confInfoReseller.Account__c = testAccount1.id;
        confInfoReseller.Document_Type__c = 'BIS Community Document';
        insert confInfoReseller;

        Attachment testAttachment = new Attachment();
        testAttachment.ParentId = confInfoStrat.id;
        testAttachment.Name = 'test';
        testAttachment.Body = Blob.valueof('test');
        insert testAttachment;
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        User testUser;

        system.runAs(thisUser)
        {
            testUser = Test_Utils.createUser('Xperian Global Community ReSellers');
            testUser.Contactid = testContact1.id;
            insert testUser;
        }

        system.runAs(testUser)
        {
            test.startTest();
            ResellerAndStratsCommPrivateDoc.getPrivateDocs('Reseller');
            ResellerAndStratsCommPrivateDoc.getPrivateDocs('Strat');
            ResellerAndStratsCommPrivateDoc.getAccountName();
            test.stopTest();
        }
        
    }
    
}