/**=====================================================================
 * Experian
 * Name: ITCA_Homepage_Controller
 * Description: Homepage Controller Class
 * Created Date: June 2017
 * Created By: Alexander McCall
 *
 * Date Modified      Modified By           Description of the update
 * 29th July 2017     James Wills           ITCA:W-008961 - Build list of Employees for Manager - removed to VF Component ITCA_Navigation_Banner
 * 30th July          Richard Joseph        Added employeeSkillDisplay, employeeSkillDisplaySort, compareSkillSet. compareCareerArea
 * 2nd August 2017    Alexander McCall      Updated working to 'Skill Set' and 'Career Area' on drop downs
 * 3rd August 2017    James Wills           ITCA:W-008961 - Updated for new requirements.
 * 8th August 2017    James Wills           Tidying up code.
 * 11th August 2017   James Wills           ITCA:W-009246 Updated to allow editing technical skills.
 * 21st August 2017   James Wills           Moved code to ITCANavigationBannerController.
 * 30th August 2017   James Wills           Deleted code duplicated in other classes (mostly in ITCA_My_Journey_Map_Controller).
 * 6th Sept. 2017     James Wills           ITCA: Commented out constructor method.
 * =====================================================================*/
global with sharing class ITCA_Homepage_Controller{

  //public ITCA_Homepage_Controller(ApexPages.StandardController controller) {
    
  //}
  
  public ITCABannerInfoClass bannerInfoLocal {get;set;}

  public id ITCAChatterGroupID {get{
     return [select id from CollaborationGroup where name=:label.ITCA_Chatter_Group limit 1].id;
  }set;}
  
  
 ////////////////////////////////////////////////////////////////////////////////////////////

  public ITCA_Homepage_Controller(){   
    ITCA_Homepage_Load();
  }
  
  
  public void ITCA_Homepage_Load(){

    bannerInfoLocal = new ITCABannerInfoClass();
    Cookie activeTabCookie = ApexPages.currentPage().getCookies().get('activeTabCookie');
       
    if(activeTabCookie != null){
       bannerInfoLocal.currentActiveTab = activeTabCookie.getValue();
       //testText=activeTabCookie.getValue();
    } else {
       bannerInfoLocal.currentActiveTab = 'isSummary';
    }
       
    //bannerInfoLocal.currentActiveTab='isMyJourneyMap';
       
    //bannerInfoLocal.showChatter=false;
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();
    bannerInfoLocal.currentUserSkillLevelMap = new Map<String,Decimal>();

    bannerInfoLocal.isSummaryBuild();        
   
  }//End of ITCA_Homepage_Load()
       
        
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
  
  public String getitcaCookie() {

        Cookie itcaCook = ApexPages.currentPage().getCookies().get('itca');

        if(itcaCook == null) {

           itcaCook = new Cookie('itca','itcaVisit',null,39999999,false);
           ApexPages.currentPage().setCookies(new Cookie[]{itcaCook});
           return null;
        }
        else{

        return itcaCook.getValue();
        }
    }
    
    public boolean profileExists {get{
  
      Career_Architecture_User_Profile__c[] userprofile = [select id from Career_Architecture_User_Profile__c where employee__c=:UserInfo.getUserId() limit 1];
  
      if (!userprofile.isEmpty()){ return true;}
      else {return false;}
    }
  }


}