/**=====================================================================
 * Name: AssignmentTeamTriggerHandler
 * Description: Trigger Handler class for the Assignment_Team__c object
 * Created Date: May 25th 2017
 * Created By: James Wills
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
public class AssignmentTeamTriggerHandler {
  
  //=================================================
  // Before Delete Call
  //=================================================
  public static void beforeDelete (Map<Id, Assignment_Team__c> oldMap) {    
    accountExecutiveDeleted(oldMap);//Case #02426211
  }
  
  
  //=================================================
  // After Update Call
  //=================================================
  public static void afterUpdate (Map<ID, Assignment_Team__c> newMap, Map<ID, Assignment_Team__c> oldMap) {
    checkAccountExecutiveChanged(newMap, oldMap);//Case #02426211
  }


  public static void checkAccountExecutiveChanged(Map<ID, Assignment_Team__c> newMap, Map<ID, Assignment_Team__c> oldMap){
    Map<ID, Assignment_Team__c> atChanged_BIS_Map = new Map<ID, Assignment_Team__c>();
    Map<ID, Assignment_Team__c> atChanged_CIS_Map = new Map<ID, Assignment_Team__c>();
    
    for(Assignment_Team__c at : newMap.values()){
      if((at.Name.contains('BIS - Growth') || at.Name.contains('BIS - Inside')) && at.Account_Executive__c!=oldMap.get(at.id).Account_Executive__c){
        atChanged_BIS_Map.put(at.id, at);
      } else if((at.Name.contains('CIS - Growth') || at.Name.contains('CIS - Inside')) && at.Account_Executive__c!=oldMap.get(at.id).Account_Executive__c){
        atChanged_CIS_Map.put(at.id, at);
      }
    }
    
    if(atChanged_BIS_Map.isEmpty() && atChanged_CIS_Map.isEmpty()){
      return;
    }
    updateAccountExecutiveForAccount(atChanged_BIS_Map, atChanged_CIS_Map, 'update');
  }
  
  
  public static void accountExecutiveDeleted(Map<ID, Assignment_Team__c> oldMap){
    Map<ID, Assignment_Team__c> atChanged_BIS_Map = new Map<ID, Assignment_Team__c>();
    Map<ID, Assignment_Team__c> atChanged_CIS_Map = new Map<ID, Assignment_Team__c>();
    
    for(Assignment_Team__c at : oldMap.values()){
      if((at.Name.contains('BIS - Growth') || at.Name.contains('BIS - Inside'))){
        atChanged_BIS_Map.put(at.id, at);
      } else if((at.Name.contains('CIS - Growth') || at.Name.contains('CIS - Inside'))){
        atChanged_CIS_Map.put(at.id, at);
      }
    }
    
    if(atChanged_BIS_Map.isEmpty() && atChanged_CIS_Map.isEmpty()){
      return;
    }
    updateAccountExecutiveForAccount(atChanged_BIS_Map, atChanged_CIS_Map, 'delete');
  }
  
    
  public static void updateAccountExecutiveForAccount(Map<ID, Assignment_Team__c> atChanged_BIS_Map, Map<ID, Assignment_Team__c> atChanged_CIS_Map, String mode){
   
    Map<ID, Account_Assignment_Team__c> aat_Map = new Map<ID, Account_Assignment_Team__c>([SELECT id, Account__c, Account__r.BIS_Account_Executive__c, Account__r.CIS_Account_Executive__c, 
                                                                                          Assignment_Team__c 
                                                                                          FROM Account_Assignment_Team__c 
                                                                                          WHERE Assignment_Team__c IN :atChanged_BIS_Map.keySet()
                                                                                          OR    Assignment_Team__c IN :atChanged_CIS_Map.keySet()]);
    
    List<ID> accountsBISFieldsToNull = new List<ID>();
    List<ID> accountsCISFieldsToNull = new List<ID>();
    
    for(Account_Assignment_Team__c aat : aat_Map.values()){
      if(atChanged_BIS_Map.containsKey(aat.Assignment_Team__c)){
        if(mode=='update'){
          aat.Account__r.BIS_Account_Executive__c = atChanged_BIS_Map.get(aat.Assignment_Team__c).Account_Executive__c;
        } else if (mode=='delete'){
          //aat.Account__r.BIS_Account_Executive__c = null;//Need to use further DML to blank out the value.
          accountsBISFieldsToNull.add(aat.Account__c);
        }
      } else if(atChanged_CIS_Map.containsKey(aat.Assignment_Team__c)){
        if(mode=='update'){
          aat.Account__r.CIS_Account_Executive__c = atChanged_CIS_Map.get(aat.Assignment_Team__c).Account_Executive__c;
        } else if (mode=='delete'){
          //aat.Account__r.CIS_Account_Executive__c = null;//Need to use further DML to blank out the value.
          accountsCISFieldsToNull.add(aat.Account__c);
        }
      }    
    }
    
    if(mode=='update'){
      try{
        update aat_Map.values();
      } catch(Exception e){
        system.debug('\n[AssignmentTeamTriggerHandler: updateAccountExecutiveForAccount]: ['+e.getMessage()+']]');
        apexLogHandler.createLogAndSave('AssignmentTeamTriggerHandler','updateAccountExecutiveForAccount', e.getStackTraceString(), e);
      } 
      
    } else if(mode=='delete'){
    
      if(!accountsBISFieldsToNull.isEmpty()){
        List<Account> accsForBISUpdate = [SELECT id, BIS_Account_Executive__c FROM Account WHERE id IN :accountsBISFieldsToNull];
        for(Account acc : accsForBISUpdate){
          acc.BIS_Account_Executive__c = null;
        }
        try{
          update accsForBISUpdate;
        } catch(Exception e){
          system.debug('\n[AssignmentTeamTriggerHandler: updateAccountExecutiveForAccount]: ['+e.getMessage()+']]');
          apexLogHandler.createLogAndSave('AssignmentTeamTriggerHandler','updateAccountExecutiveForAccount', e.getStackTraceString(), e);
        } 
      }
    
      if(!accountsCISFieldsToNull.isEmpty()){
        List<Account> accsForCISUpdate = [SELECT id, BIS_Account_Executive__c FROM Account WHERE id IN :accountsCISFieldsToNull];
        for(Account acc : accsForCISUpdate){
          acc.CIS_Account_Executive__c = null;
        }
        try{
          update accsForCISUpdate;
        } catch(Exception e){
          system.debug('\n[AssignmentTeamTriggerHandler: updateAccountExecutiveForAccount]: ['+e.getMessage()+']]');
          apexLogHandler.createLogAndSave('AssignmentTeamTriggerHandler','updateAccountExecutiveForAccount', e.getStackTraceString(), e);
        } 
      }
    }
    
  }
  

  
  
}