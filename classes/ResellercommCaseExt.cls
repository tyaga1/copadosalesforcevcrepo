public class ResellercommCaseExt{

    
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public ResellercommCaseExt() {
        User curUser = [Select id, Phone from User where id =: UserInfo.getUserId()];
        String caseID = ApexPages.currentPage().getParameters().get('id');
    }
    
        
    
    @AuraEnabled
    public static Case cloneCase(String caseID) {
    System.debug('clone case called ' + caseID);
    Case newCase = new Case();
        if (caseID != null) {
            
            List<Case> newCaseList = [Select Id, CaseNumber, Origin, User_Requestor__c, Requestor_Email__c, Requestor_Work_Phone__c, Requester_BU__c, Priority, Reason, Status, Subject, Description, Sub_Origin__c, Support_Team__c,Experian_Id__c, Account_Sales_Support_Type__c, Type, Secondary_Case_Reason__c, RecordTypeID, Subcode__c from Case where id =: caseID];
            newCase= newCaseList[0].clone(false);
            newCase.status ='Open';
            
        }
    System.debug('clone case called 2' + newCase);        
        return newCase;
    }
    
    @AuraEnabled
    public static List<String> getSubcodes() {
    
        List<String> subcode = new List<String>();
        List<User> curUser = [Select id, Contactid, Contact.Accountid from User where id =: UserInfo.getUserId()];
        if (curUser.size() > 0 ) {
            if (curUser[0].Contact.Accountid != null){
            List<Account> userAcc = [Select id, (Select id, Subscriber_Code__c from Sub_Codes__r) from Account where id =: curUser[0].Contact.Accountid];
            if (userAcc.size() > 0) {
                for(Sub_Code__c sc : userAcc[0].Sub_Codes__r) {
                    subcode.add(sc.Subscriber_Code__c);
                }
            }
            }
        }
        
        return subcode;
    }
    
    @AuraEnabled
    public static Map<String, List<String>> getRTSubcode(String RTName) {
    System.debug('RTName ' + RTName);
        Map<String, List<String>> idCode = new Map<String, List<String>>();

        if (Schema.SObjectType.Case.getRecordTypeInfosByName().get(RTName) != null)
        {
            String rtId= Schema.SObjectType.Case.getRecordTypeInfosByName().get(RTName).getRecordTypeId();
            List<String> subcode = new List<String>();
            List<User> curUser = [Select id, Contactid, Contact.Accountid from User where id =: UserInfo.getUserId()];

            if (curUser.size() > 0 ) {
                if (curUser[0].Contact.Accountid != null){
                    List<Account> userAcc = [Select id, (Select id, Subscriber_Code__c from Sub_Codes__r Order by Subscriber_Code__c ) from Account where id =: curUser[0].Contact.Accountid];
                    if (userAcc.size() > 0) {
                        for(Sub_Code__c sc : userAcc[0].Sub_Codes__r) {
                            subcode.add(sc.Subscriber_Code__c);
                        }
                    }
                }
            }
            subcode.sort();
            idCode.put(rtId, subcode);
        }

        return idCode;
    }
    
    @AuraEnabled
    public static String submitCase(Case newCase, String recordType) {  
    newCase.RecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordType).getRecordTypeId();  
      //RJ Added 
      if(recordType =='CSDA BIS Support')
           newCase.Support_Team__c ='BIS Reseller';
          else
        newCase.Support_Team__c ='Reseller Support';
        
        newCase.Origin= 'Reseller Communnity';
        newCase.Status='New';// RJ added to set to New always
     //newCase.RecordTypeID= [select Id from RecordType where Name = 'CSDA BIS Support' and SobjectType = 'Case'];
    //Fetching the assignment rules on case
        
        Global_Settings__c custSettings = Global_Settings__c.getValues(Constants.GLOBAL_SETTING);
    //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= custSettings.Case_Active_Assignment_Id__c; //rj '01Qi00000005wGi';
        //dmlOpts.assignmentRuleHeader.useDefaultRule = true;
        dmlOpts.EmailHeader.triggerUserEmail = true;


    //Setting the DMLOption on Case instance
        newCase.setOptions(dmlOpts);
 
        insert newCase;

        return newCase.id;

    }
    
  /*
    
    @AuraEnabled
    public static String getRtId(String RTName) {
        
        String rtId= Schema.SObjectType.Case.getRecordTypeInfosByName().get(RTName).getRecordTypeId();
        
        return rtId;
    }
    */

    /*Public static id getAssigmentRuleID(){
        
            AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        
        return AR.id;
    }*/
    
}