/**=====================================================================
 * Experian
 * Name: BatchOpportunityRevenue_Test
 * Description: Test class for BatchOpportunityRevenue batch
 
 * Description: 
 * Created Date: 19 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update

 =====================================================================*/
@isTest
private class BatchOpportunityRevenue_Test {

  static testMethod void testBatchStandard() {
    Test.startTest();
    
    BatchOpportunityRevenue b = new BatchOpportunityRevenue();
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityRevenue.revTotalsMapClean.keySet());
    
    List<Opportunity> checkOpps = (List<Opportunity>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM Opportunity');
    system.assert(checkOpps.size() > 0, 'Opportunity not found');
    
    for(Opportunity opp : checkOpps) {
      for(String fName : fieldNames) {
        if (fName == 'First_12_Months_Revenue_Impact__c') {
          system.assertEquals(1200.0, (Decimal)opp.get(fName), fName + ' should be 1200.0');
        }
        else {
          system.assertEquals(100.0, (Decimal)opp.get(fName), fName + ' should be 100.0');
        }
      }
    }
  }
  
  static testMethod void testBatchAllOpen() {
    Test.startTest();
    
    BatchOpportunityRevenue b = new BatchOpportunityRevenue();
    b.runAllOpenOpps = true;
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityRevenue.revTotalsMapClean.keySet());
    
    List<Opportunity> checkOpps = (List<Opportunity>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM Opportunity');
    system.assert(checkOpps.size() > 0, 'Opportunity not found');
    
    for(Opportunity opp : checkOpps) {
      for(String fName : fieldNames) {
        if (fName == 'First_12_Months_Revenue_Impact__c') {
          system.assertEquals(1200.0, (Decimal)opp.get(fName), fName + ' should be 1200.0');
        }
        else {
          system.assertEquals(100.0, (Decimal)opp.get(fName), fName + ' should be 100.0');
        }
      }
    }
  }
  
  static testMethod void testBatchAllOpps() {
    Test.startTest();
    
    BatchOpportunityRevenue b = new BatchOpportunityRevenue();
    b.runAllOpps = true;
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityRevenue.revTotalsMapClean.keySet());
    
    List<Opportunity> checkOpps = (List<Opportunity>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM Opportunity');
    system.assert(checkOpps.size() > 0, 'Opportunity not found');
    
    for(Opportunity opp : checkOpps) {
      for(String fName : fieldNames) {
        if (fName == 'First_12_Months_Revenue_Impact__c') {
          system.assertEquals(1200.0, (Decimal)opp.get(fName), fName + ' should be 1200.0');
        }
        else {
          system.assertEquals(100.0, (Decimal)opp.get(fName), fName + ' should be 100.0');
        }
      }
    }
  }
  
  static testMethod void testBatchCloseRange() {
    Test.startTest();
    
    BatchOpportunityRevenue b = new BatchOpportunityRevenue();
    b.closeDateStart = Date.today().addDays(-60);
    b.closeDateEnd = Date.today().addDays(60);
    Database.executeBatch(b,10);
    
    Test.stopTest();
    
    List<String> fieldNames = new List<String>(BatchOpportunityRevenue.revTotalsMapClean.keySet());
    
    List<Opportunity> checkOpps = (List<Opportunity>)Database.query('SELECT '+String.join(fieldNames,',')+' FROM Opportunity');
    system.assert(checkOpps.size() > 0, 'Opportunity not found');
    
    for(Opportunity opp : checkOpps) {
      for(String fName : fieldNames) {
        if (fName == 'First_12_Months_Revenue_Impact__c') {
          system.assertEquals(1200.0, (Decimal)opp.get(fName), fName + ' should be 1200.0');
        }
        else {
          system.assertEquals(100.0, (Decimal)opp.get(fName), fName + ' should be 100.0');
        }
      }
    }
  }
  
  @testSetup
  static void setupTestData() {
    Account tstAcc = Test_utils.createAccount();
    
    insert new List<Account>{tstAcc};
    
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp.CloseDate = Date.today().addDays(7);
    Opportunity tstOpp2 = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp2.CloseDate = Date.today().addDays(7);
    
    insert new List<Opportunity>{tstOpp,tstOpp2};
    
    Product2 product = Test_Utils.insertProduct();
    Product.CanUseRevenueSchedule = True;
    update product;
    PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
    
    Date scheduleFrom = Date.today().addDays(-390).toStartOfMonth();
    Date scheduleTo = Date.today().addDays(780).toStartOfMonth().addDays(-1); // end of previous month
    
    OpportunityLineItem tstOli = Test_utils.createOpportunityLineItem(tstOpp.Id, stdPricebookEntry.Id , tstOpp.Type);
    tstOli.Start_Date__c = scheduleFrom;
    tstOli.End_Date__c = scheduleTo;
    OpportunityLineItem tstOli2 = Test_utils.createOpportunityLineItem(tstOpp2.Id, stdPricebookEntry.Id , tstOpp2.Type);
    tstOli2.Start_Date__c = scheduleFrom;
    tstOli2.End_Date__c = scheduleTo;
    
    List<OpportunityLineItem> oliLst = new List<OpportunityLineItem>{tstOli,tstOli2};
    
    insert oliLst;
    
    List<OpportunityLineItemSchedule> oliSchs = new List<OpportunityLineItemSchedule>();
    
    // Rev will be 100 USD per month for the duration of the schedule
    Integer monthsCount = scheduleFrom.monthsBetween(scheduleTo);
    for(OpportunityLineItem oli : oliLst) {
      for(Integer cm = 0; cm < monthsCount; cm++) {
        Date currentMonth = scheduleFrom.addMonths(cm);
        oliSchs.add(new OpportunityLineItemSchedule(
          OpportunityLineItemId = oli.Id,
          Type = 'Revenue',
          Revenue = 100.00,
          ScheduleDate = currentMonth,
          Description = 'According to invoice.'
        ));
      }
    }
    
    insert oliSchs;
    
    Date runDate = Date.today();
    
    Integer fiscalYearStartMonth = [
      SELECT FiscalYearStartMonth 
      FROM Organization 
      WHERE Id = :Userinfo.getOrganizationId()
    ].FiscalYearStartMonth;
    
    Date cfyStart;    
    if (runDate.month() < fiscalYearStartMonth) {
      cfyStart = Date.newInstance(runDate.year()-1, fiscalYearStartMonth, 1);
    }
    else {
      cfyStart = Date.newInstance(runDate.year(), fiscalYearStartMonth, 1);
    }
    
    Date cfyEnd = cfyStart.addYears(1).addDays(-1);
    Date nfyStart = cfyEnd.addDays(1);
    Date nfyEnd = nfyStart.addYears(1).addDays(-1);
    
    
    
    List<OpportunityLineItem> oppLineItemsList = [
      SELECT Id, SystemModStamp, 
             Current_FY_revenue_impact__c, 
             CFY_SR_April__c, CFY_SR_May__c, CFY_SR_June__c,
             CFY_SR_July__c, CFY_SR_August__c, CFY_SR_September__c,
             CFY_SR_October__c, CFY_SR_November__c, CFY_SR_December__c,
             CFY_SR_January__c, CFY_SR_February__c, CFY_SR_March__c,
             Next_FY_revenue_impact__c,
             NFY_SR_April__c, NFY_SR_May__c, NFY_SR_June__c,
             NFY_SR_July__c, NFY_SR_August__c, NFY_SR_September__c,
             NFY_SR_October__c, NFY_SR_November__c, NFY_SR_December__c,
             NFY_SR_January__c, NFY_SR_February__c, NFY_SR_March__c,
             First_12_Months_Revenue_Impact__c,
        (
         SELECT Id, Revenue, ScheduleDate, OpportunityLineItemId
         FROM OpportunityLineItemSchedules
         ORDER BY ScheduleDate ASC
         ) 
      FROM OpportunityLineItem 
      WHERE Id IN :oliLst
    ];
    
    for(OpportunityLineItem oli : oppLineItemsList) {
      oli = BatchOpportunityLIScheduleRevenue.processLineItem(oli, oli.OpportunityLineItemSchedules, cfyStart, cfyEnd, nfyStart, nfyEnd);
    }
    update oppLineItemsList;
  }
  
}