/*=============================================================================
 * Experian
 * Name: BatchFixContactRelationships
 * Description: CRM2:W-005409: Batch to build the relationship field on contacts
 *              *** Can be purged after running in production. ***
 * Created Date: 27 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * Jul 27th, 2016     Paul Kissick          CRM2:W-005409: Batch to build the relationship field on contacts
 * Sep 9th, 2016      Paul Kissick          CRM2:W-005409: Improved batch to report errors back, and filter out inactives.
 =============================================================================*/

public class BatchFixContactRelationships implements Database.Batchable<sObject>, Database.Stateful {

  private Datetime startTime;
  public List<String> errorsFound;

  public BatchFixContactRelationships () {
  }
  
  public Database.Querylocator start (Database.Batchablecontext bc) {
    startTime = Datetime.now();
    errorsFound = new List<String>();
    return Database.getQueryLocator([
      SELECT Id
      FROM Contact
      WHERE Id IN (
        SELECT Contact__c 
        FROM Contact_Team__c
        WHERE Relationship_Owner__c != null
        AND Contact__c != null
      )
    ]);
  }
  
  public void execute (Database.BatchableContext bc, List<Contact> scope) {
    Map<Id, Contact> contMap = new Map<Id, Contact>(scope);
    // Requery to get the contact relationships (not included above for speed)
    List<Contact> cleanContacts = [
      SELECT Id, Contact_Relationship_Team_IDs__c, (
          SELECT Relationship_Owner__r.User_HEX_Id__c
          FROM Contact_Teams__r
          WHERE Relationship_Owner__r.IsActive = true
        )
      FROM Contact
      WHERE Id IN :contMap.keySet()
    ];
    // Contact list to update
    List<Contact> updConts = new List<Contact>();
    
    Set<String> relIds = new Set<String>();
    for (Contact c : cleanContacts) {
      relIds.clear();
      if (c.Contact_Teams__r != null && !c.Contact_Teams__r.isEmpty()) {
        // With every entry, get the unique id and add to the set (unique)
        for (Contact_Team__c ct : c.Contact_Teams__r) {
          relIds.add(ct.Relationship_Owner__r.User_HEX_Id__c);
        }
      }
      // Now build the relationship field
      updConts.add(
        new Contact(
          Id = c.Id, 
          Contact_Relationship_Team_IDs__c = String.join(new List<String>(relIds), ';')
        )
      );
    }
    List<Database.SaveResult> srList = Database.update(updConts, false);
    for (Integer i = 0; i < srList.size(); i++) {
      if (!srList.get(i).isSuccess()) {
        // DML operation failed
        errorsFound.add('Contact Id: ' + updConts.get(i).Id + '\n' + BatchHelper.parseErrors(srList.get(i).getErrors()));
      }
    }
  }
  
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchFixContactRelationships', false);
    if (!errorsFound.isEmpty() || Test.isRunningTest()) {
      bh.batchHasErrors = true;
      bh.emailBody += 'Errors found: \n\n'+String.join(errorsFound, '\n\n');
    }
    
    bh.sendEmail();
  }
  
  // Adding here even though I shouldn't but it's quicker and this class will be deleted soon enough!
  @isTest private static void batchTest() {
    User u1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Account a1 = Test_Utils.insertAccount();
    Contact c1 = Test_Utils.insertContact(a1.Id);
    u1 = [SELECT User_HEX_Id__c FROM User WHERE Id = :u1.Id];
    
    Contact_Team__c ct1 = new Contact_Team__c(Relationship_Owner__c = u1.Id, Contact__c = c1.Id);
    insert ct1;
    
    Test.startTest();
    
    Database.executeBatch(new BatchFixContactRelationships());
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM Contact WHERE Id = :c1.Id AND Contact_Relationship_Team_IDs__c != null]);
    String contRelIds = [SELECT Contact_Relationship_Team_IDs__c FROM Contact WHERE Id = :c1.Id AND Contact_Relationship_Team_IDs__c != null].Contact_Relationship_Team_IDs__c;
    system.assert(contRelIds.contains(u1.User_HEX_Id__c));
  }
  
}