/**=====================================================================
 * Experian
 * Name: WebserviceEndpointAddPassCtrlr
 * Description: Controller for the page to handle setting of webservice passwords.
 * Created Date: Oct 29th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
public with sharing class WebserviceEndpointAddPassCtrlr {
  
  Webservice_Endpoint__c wsend;
  ApexPages.StandardController stdCon;
  
  public Boolean okayToShow {get{if (okayToShow == null) okayToShow = false; return okayToShow;}set;}
  
  public String wsPassword {get;set;}
  
  public WebserviceEndpointAddPassCtrlr(ApexPages.StandardController con) {
    stdCon = con;
    wsend = (Webservice_Endpoint__c)con.getRecord();
  }
  
  public PageReference save() {
    // get the current key and set the password...
    try {
      if (String.isNotBlank(wsPassword)) {
        wsend = WebserviceEndpointUtil.setWebservicePassword(wsend, wsPassword);
      }
    }
    catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
      return null;
    }
    return stdCon.view();
  }
  
  public PageReference checkKey() {
    if (wsend != null && wsend.Id != null) {
      okayToShow = true;
      if ([SELECT COUNT() FROM Webservice_Endpoint__c WHERE Id = :wsend.Id AND Key__c != null] == 0) {
        // generate a key and store it for use later...
        wsend = WebserviceEndpointUtil.createWebserviceKey(wsend);
      }
    }
    else {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unable to perform operation.'));
      return null;
    }
    return null;
  }
  
  
}