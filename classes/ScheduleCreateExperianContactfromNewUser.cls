/*
  * Author:     Diego Olarte (Experian)
  * Description:  The following class is for scheduling the 'BatchCreateExperianContactfromNewUser.cls' class to run at specific intervals.
  */ 

global class ScheduleCreateExperianContactfromNewUser implements Schedulable
{   
  
  global void execute(SchedulableContext ctx)
  {
    Database.executebatch(new BatchCreateExperianContactfromNewUser(),ScopeSizeUtility.getScopeSizeForClass('BatchCreateExperianContactfromNewUser'));
  }
}