/******************************************************************************
 * Name: ExpCommunityProfile.cls
 * Created Date: 2/9/2017
 * Created By: Hay Win
 * Description : Controller for ExpCommunityProfile page for new Exp Community
 *               includes user info, followers, followings, groups, and badges
 * Change Log- 
 ****************************************************************************/
public with sharing class ExpCommunityProfile {
    
    public String userID {get;set;}
    public List<User> curUserList {get;set;}
    public User curUser {get;set;}
    public List<EntitySubscription> displayFollowers {get;set;}
    public List<EntitySubscription> displaySubsc {get;set;}
    public List<EntitySubscription> screenSubsc {get;set;}
    public List<EntitySubscription> followers {get;set;}
    public List<EntitySubscription> followings {get;set;}
    public List<CollaborationGroupMember> chatterMember {get;set;}
    public Integer PostCount {get;set;}
    public Integer postCmt {get;set;}
    public Integer LikeReceivedCount {get;set;}
    public Integer CommentCount {get;set;}    
    public Integer CommentReceivedCount {get;set;}   
    public String ownID {get;set;} 
    public List<WorkBadge> badgeToShow {get;set;}
    public String showNormalPhoto {get;set;}
    public String showEditPhoto{get;set;}
    public Map<id,user> userWithPhotoUrl {get;set;}
    List<WorkBadge> badgeList = new List<WorkBadge>();
    public Integer badgeCounter = 0;
    public Integer badgeLimitSize = 5;
    public Integer badgeTotalSize =0;

    public Integer followerCounter = 0;
    public Integer followerLimitSize = 8;
    public Integer followerTotalSize =0;   
    
    public Integer subscCounter = 0;
    public Integer subscTotalSize = 0;
    
    public Integer groupCounter = 0;
    public Integer groupLimitSize = 3;
    public Integer groupTotalSize =0;   
    
    public ExpCommunityProfile() {
        showEditPhoto = 'none;';                     //editable user info
        showNormalPhoto = 'block;';   
        userID = ApexPages.currentPage().getParameters().get('userID');
        ownID = UserInfo.getUserId();
        String preUser = Schema.SObjectType.User.getKeyPrefix();
        
        displayFollowers = new List<EntitySubscription>();
        displaySubsc = new List<EntitySubscription>(); 
        screenSubsc = new List<EntitySubscription>(); 
        
        /* user */
        List<String> userIDList = new List<String>();
        if (userID == null || userID == ownID) {
            userID = ownID;
            showEditPhoto = 'block;';
            showNormalPhoto ='none;';
        }
        
        curUser = [Select id, name, aboutme, firstname, lastname, CommunityNickname, street, city, state, country, PostalCode, managerid, manager.name, FullPhotoUrl, title, email, MobilePhone, Phone, fax  from user where id =: userID ];
        
        /* followers */
        followers =  [select id, subscriberid,  subscriber.name,  subscriber.title, subscriber.SmallPhotoUrl from EntitySubscription where parentID =: userID Order by subscriberid asc limit: followerLimitSize];
        
        followerTotalSize = [Select count() from EntitySubscription where parentID =: userID limit 1000];
        
        
        //Post, Comments, Like Received etc.
        List<ChatterActivity> chatActivityList = [SELECT Id, PostCount, LikeReceivedCount, CommentCount, CommentReceivedCount FROM ChatterActivity WHERE ParentId =: userID ];
        
        if (chatActivityList.size() > 0) {
            CommentReceivedCount = chatActivityList[0].CommentReceivedCount ;  
            PostCount = chatActivityList[0].PostCount;
            LikeReceivedCount = chatActivityList[0].LikeReceivedCount;
            CommentCount = chatActivityList[0].CommentCount;
        } else {
            CommentReceivedCount = 0 ;  
            PostCount = 0;
            LikeReceivedCount = 0;
            CommentCount = 0;
        }
        
        postCmt = PostCount + CommentCount;
        
        
        //thanks
        badgeList = [Select id, DefinitionId, Description, Message, ImageUrl, GiverID, Giver.Name, RecipientId, Recipient.Name  from WorkBadge where RecipientId =: userID];
        badgeToShow = new List<WorkBadge>();
        badgeTotalSize = badgeList.size();
        if((badgeCounter + badgeLimitSize) <= badgeTotalSize){
            for(Integer i = 0; i < badgeLimitSize; i++){
                badgeToShow.add(badgeList.get(i));
            }
        }else{
            for(Integer i = 0 ; i < badgeTotalSize; i++){
                badgeToShow.add(badgeList.get(i));
            }
        }
        
        //groups involved
        chatterMember = [select id, CollaborationGroupId, CollaborationGroup.MemberCount, CollaborationGroup.SmallPhotoUrl ,CollaborationGroup.name, memberid from CollaborationGroupMember where memberid =: userID  Order by CollaborationGroupId asc limit: groupLimitSize];
        
        //group counter
        groupTotalSize = [Select count() from CollaborationGroupMember where memberid =: userID];
        
        //followings
        followings = [select id, parentid, parent.name, parent.title from EntitySubscription where subscriberid =: userID AND parent.type ='User' Order by parentid limit 1000];
        
        subscTotalSize = followings.size();
        if((subscCounter + followerLimitSize) <= subscTotalSize){
            for(Integer i = 0; i < followerLimitSize; i++){
                displaySubsc.add(followings.get(i));
            }
        } else{
            for(Integer i = 0 ; i < subscTotalSize; i++){
                displaySubsc.add(followings.get(i));
            }
        }
        
        for(EntitySubscription subsc : followings){
            userIDList.add(subsc.parentid);
        }
        
        userWithPhotoUrl = new Map<id,user>([Select id,SmallPhotoUrl from User where ID IN: userIDList]);
    }
    
    
     public PageReference Save() {         
        update curUser;   
        return null;
    }

    public PageReference Cancel() {
        return null;
    }
    
    /*
     * badge pagination
     *
     */
    
    public void badgeNext(){
   
        badgeToShow.clear();
        
        badgeCounter = badgeCounter + badgeLimitSize;  
       
        if((badgeCounter + badgeLimitSize) <= badgeTotalSize){
            for(Integer i = badgeCounter; i<(badgeCounter + badgeLimitSize);i++){
                badgeToShow.add(badgeList.get(i));
            }
        } else{
            for(Integer i = badgeCounter; i < badgeTotalSize; i++){
                badgeToShow.add(badgeList.get(i));
            }
        }
        
    }
   
    public void badgePrevious(){
   
        badgeToShow.clear();
        badgeCounter = badgeCounter - badgeLimitSize;     
        for(Integer i = badgeCounter; i < (badgeCounter + badgeLimitSize); i++){
            badgeToShow.add(badgeList.get(i));
        }
    }
    
    public Boolean getDisableBadgeNext(){
   
        if((badgeCounter + badgeLimitSize) >= badgeTotalSize ){        
            return true ;
        } else{        
            return false ;
        }
    }
   
    public Boolean getDisableBadgePrevious(){
   
        if(badgeCounter == 0)
            return true ;
        else
            return false ;
    } 
    

    /*
     * followers pagination
     *
     */
    
    public void followerNext(){
   
        followerCounter = followerCounter + followerLimitSize;
        
        followers =  [select id, subscriberid,  subscriber.name, subscriber.title,  subscriber.SmallPhotoUrl from EntitySubscription where parentid =: userID Order by subscriberid asc limit :followerLimitSize offset :followerCounter];
        
    }
   
    public void followerPrevious(){
   
        displayFollowers.clear();

        followerCounter = followerCounter - followerLimitSize;       
       
        followers =  [select id, subscriberid,  subscriber.name, subscriber.title, subscriber.SmallPhotoUrl from EntitySubscription where parentid =: userID Order by subscriberid asc limit :followerLimitSize offset :followerCounter];
    }
    
    public Boolean getDisableFollowerNext(){
   
        if((followerCounter + followerLimitSize) >= followerTotalSize ){        
            return true ;
        } else{        
            return false ;
        }
    }
   
    public Boolean getDisableFollowerPrevious(){
   
        if(followerCounter == 0)
            return true ;
        else
            return false ;
    }   


    /*
     * followings pagination
     *
     */
     public void followingStart() {
        screenSubsc.addAll(displaySubsc); 
         
     }
     
     public void followingEnd() {
        screenSubsc.clear(); 
        subscCounter = 0;
        followerCounter = 0;
        groupCounter = 0;
     }
     
    public void followingNext(){
   
        screenSubsc.clear();
        subscCounter = subscCounter + followerLimitSize;
       
        if((subscCounter + followerLimitSize) <= subscTotalSize){
            for(Integer i = subscCounter; i<(subscCounter + followerLimitSize);i++){
                screenSubsc.add(followings.get(i));
            }
        } else{
            for(Integer i = subscCounter; i < subscTotalSize; i++){
                screenSubsc.add(followings.get(i));
            }
        }
    }
   
    public void followingPrevious(){
   
        screenSubsc.clear();

        subscCounter = subscCounter - followerLimitSize;       
       
        for(Integer i = subscCounter; i < (subscCounter + followerLimitSize); i++){
            screenSubsc.add(followings.get(i));
        }
    }
    
    public Boolean getDisableFollowingNext(){
   
        if((subscCounter + followerLimitSize) >= subscTotalSize ){        
            return true ;
        } else{        
            return false ;
        }
    }
   
    public Boolean getDisableFollowingPrevious(){
   
        if(subscCounter == 0)
            return true ;
        else
            return false ;
    }

/*
     * groups pagination
     *
     */
    
    public void groupNext(){
   
        groupCounter = groupCounter + groupLimitSize;
        
        chatterMember = [select id, CollaborationGroupId, CollaborationGroup.MemberCount, CollaborationGroup.SmallPhotoUrl ,CollaborationGroup.name, memberid from CollaborationGroupMember where memberid =: userID  Order by CollaborationGroupId asc limit: groupLimitSize offset :groupCounter];
        
    }
   
    public void groupPrevious(){

        groupCounter = groupCounter - groupLimitSize;       
       
        chatterMember = [select id, CollaborationGroupId, CollaborationGroup.MemberCount, CollaborationGroup.SmallPhotoUrl ,CollaborationGroup.name, memberid from CollaborationGroupMember where memberid =: userID  Order by CollaborationGroupId asc limit: groupLimitSize offset :groupCounter];
    }
    
    public Boolean getDisableGroupNext(){
         

        if((groupCounter + groupLimitSize) >= groupTotalSize ){   
            return true ;
        } else{        
            return false ;
        }
    }
   
    public Boolean getDisableGroupPrevious(){
   
        if(groupCounter == 0)
            return true ;
        else
            return false ;
    }   
}