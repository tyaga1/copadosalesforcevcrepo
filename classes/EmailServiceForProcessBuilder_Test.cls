/**=============================================================================
 * Experian plc
 * Name: EmailServiceForProcessBuilder_Test
 * Description: Case #02296348 UAT - Repository, Distribution and Tracking
 * Created Date: Feb 23rd, 2017
 * Created By: Sanket Vaidya
 * 
 * Date Modified        Modified By                  Description of the update
 * 
 =============================================================================*/

@isTest
public class EmailServiceForProcessBuilder_Test
{    
    @isTest
    public static void testEmailTestersOnTestCase()
    {
        List<Test_Case__c> caseList = new List<Test_Case__c>();
        
        /**Test Cases**/
        Test_Case__c c1 = new Test_Case__c();
        c1.UAT_Owner__c = UserInfo.getUserId();

        Test_Case__c c2 = new Test_Case__c();

        Test_Case__c c3 = new Test_Case__c();
        
        caseList.add(c1);
        caseList.add(c2);
        caseList.add(c3);
        
        insert caseList;
        /************/
        
        /**Test Case Team**/
        List<User> listUsers = [SELECT ID,Name FROM USER WHERE ID != :UserInfo.GetUserId() LIMIT 5];
        
        List<Test_Case_Team__C> teamList = new List<Test_Case_Team__c>();
        
                Test_Case_Team__c member1 = new Test_Case_Team__c();
                member1.Test_Case__c = c1.Id;
                member1.Tester__c = UserInfo.getUserId();
                teamList.add(member1);
                
                Test_Case_Team__c member2 = new Test_Case_Team__c();
                member2.Test_Case__c = c1.Id;
                member2.Tester__c = listUsers[0].Id;
                teamList.add(member2);
        
        Test_Case_Team__c member3 = new Test_Case_Team__c();
        member3.Test_Case__c = c2.Id;
        member3.Tester__c = listUsers[1].Id;
        teamList.add(member3);
        
        Test_Case_Team__c member4 = new Test_Case_Team__c();
        member4.Test_Case__c = c2.Id;
        member4.Tester__c = listUsers[2].Id;
        teamList.add(member4);
        
        Test_Case_Team__c member5 = new Test_Case_Team__c();
        member5.Test_Case__c = c2.Id;
        member5.Tester__c = listUsers[3].Id;
        teamList.add(member5);
        
        Test_Case_Team__c member6 = new Test_Case_Team__c();
        member6.Test_Case__c = c2.Id;
        member6.Tester__c = UserInfo.getUserId();
        teamList.add(member6);
        
        insert teamList;
        
        EmailServiceForProcessBuilder.EmailTestersOnTestCase(caseList);
    }

}