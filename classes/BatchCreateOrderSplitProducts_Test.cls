/**=====================================================================
 * Experian
 * Name: BatchCreateOrderSplitProducts_Test
 * Description: 
 *
 * Created Date: Sept. 21st, 2017
 * Created By: James Wills
 *
 * Date Modified                Modified By                  Description of the update
 *
  =====================================================================*/
@isTest
public class BatchCreateOrderSplitProducts_Test {


  //================================================================================
  // Test method to verify AccountTeamMembers and AccountShares are created properly 
  //================================================================================
  public static testmethod void testAddAccAssigTeamMemberToAccTeam(){
     
    Test.startTest();
      
      BatchCreateOrderSplitProducts b2 = new BatchCreateOrderSplitProducts();
      ID batchedprocessid2 = Database.executeBatch(b2);
      
    Test.stopTest();
         
  }
  
  //==========================================================
  // Creates test data: Assignment_Team__c & members
  // This method is also used by BatchAssignmentTeam_Test.cls
  //==========================================================
  @testSetup
  static void createTestData() {
    // create test data
    User thisUser = [SELECT id FROM User WHERE id =:UserInfo.getUserId()];
    
    // create users
    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN];
    List<User> lstUser = Test_Utils.createUsers(p, 'test1234@experian.com', 'T-AE', 2);
    insert lstUser;
    
    
    System.runAs(thisUser){
      Order__c ord1 = new Order__c();
        
      insert ord1;
        
      Product2 prd1 = new Product2();
      prd1.Name='Test1';
    
      insert prd1;
    
      Order_Line_Item__c oli1 = new Order_Line_Item__c();
      oli1.Order__c   = ord1.id;
      oli1.Product__c = prd1.id;
    
      insert oli1;
    }
  }
 
}