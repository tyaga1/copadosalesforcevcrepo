/**********************************************************************************************
 * Experian
 * Name         : ScheduleAssignmentTeamV2_Test
 * Created By   : Paul Kissick
 * Purpose      : Test class of scheduler class "ScheduleAssignmentTeamV2" & "ScheduleAssignmentTeamMissed"
 * Created Date : 17th Aug, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * [Date]                       [Name]                      [Description]
***********************************************************************************************/
@isTest
private class ScheduleAssignmentTeamV2_Test {
  
  @isTest 
  static void test_ScheduleAssignmentTeam() {
    Global_Settings__c gs = new Global_Settings__c();
    gs.Name = 'Global';
    gs.Batch_Failures_Email__c = 'test@here.com';
    insert gs;
        
    Test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('Schedule Assignment Team V2', CRON_EXP, new ScheduleAssignmentTeamV2());
    
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                                FROM CronTrigger 
                                WHERE id = :jobId];     
    Test.stopTest();
    
    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled  
    system.assertEquals(0, ct.TimesTriggered);
  } 
  
  @isTest 
  static void test_ScheduleAssignmentTeamMissed() {
    Global_Settings__c gs = new Global_Settings__c();
    gs.Name = 'Global';
    gs.Batch_Failures_Email__c = 'test@here.com';
    insert gs;
        
    Test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('Schedule Assignment Team V2', CRON_EXP, new ScheduleAssignmentTeamMissed());
    
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                                FROM CronTrigger 
                                WHERE id = :jobId];     
    Test.stopTest();
    
    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled  
    system.assertEquals(0, ct.TimesTriggered);
  } 
}