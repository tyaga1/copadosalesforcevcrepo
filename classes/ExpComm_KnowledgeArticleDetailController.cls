/**
* @description A page controller for featured article detail visualforce page
* Its main usage is to display the article to the user by reading the article (ka) id from the url parameter
*
* @author Ryan (Weijie) Hu, UC Innovation
*
* @date 02/14/2017
*/
public with sharing class ExpComm_KnowledgeArticleDetailController {

    /* Private controller properties */
    private final sObject mysObject;
    private final Map<String, Wrapper> nodeMap;
    private final String DATA_CATEGORY_GROUPNAME = 'Experian_Internal';

    /* Public controller properties - for visualforce page access */
    public Employee_Article__ka article {get; set;}
    public Employee_Article__kav articleLastestVersion {get; set;}

    public Boolean controllerError {get; set;}
    //public String dataCategoryStructureLevelStr {get; set;}

    public String ownerName {get; set;}
    public Boolean allowCurrentUserToVote {get; set;}
    public Integer currentRatingScore {get; set;}

    public String realURL {get; set;}

    public Boolean hasTopic {get; set;}
    public List<TopicAssignment> topics {get; set;}

    public String lastPublishedDateInUserTimeZone {get; set;}
    public Boolean hasDownload {get; set;}
    public Boolean showRaiseCaseBtn {get; set;}


    public Boolean isPreviewingArticle {get; set;}
    public List<String> dataCategoryStructureList {get; set;}

    /** Constructors */

    /**
     * Constructor - Default (Initialize other control properties and read url parameter if exists)
     *
     * @param   None.
     * @return  None.
     */
    public ExpComm_KnowledgeArticleDetailController() {

        controllerError = false;

        String idStr = ApexPages.currentPage().getParameters().get('id');

        Id currentUserId = UserInfo.getUserId();
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];

        String previewDraftVersionIdStr = ApexPages.currentPage().getParameters().get('vid');

        if (previewDraftVersionIdStr != null && previewDraftVersionIdStr != '') {
            isPreviewingArticle = true;
        }
        else {
            isPreviewingArticle = false;
        }

        if ((idStr == null || idStr == '') && (previewDraftVersionIdStr == null || previewDraftVersionIdStr == '')) {
            controllerError = true;
        }
        else {
            
            // Article Record
            List<Employee_Article__ka> aList;

            if (isPreviewingArticle == false) {
                aList = [SELECT Id FROM Employee_Article__ka WHERE Id =: idStr AND MasterLanguage =: currentUser.LanguageLocaleKey limit 1];
            }
            else {
                // Get Article Record back
                List<Employee_Article__kav> avList = [SELECT Id, KnowledgeArticleId FROM Employee_Article__kav WHERE Id =: previewDraftVersionIdStr limit 1];

                if (avList != null && avList.size() > 0) {
                    String artiId = avList.get(0).KnowledgeArticleId;

                    aList = [SELECT Id FROM Employee_Article__ka WHERE Id =: artiId AND MasterLanguage =: currentUser.LanguageLocaleKey limit 1];
                }
                else {
                    controllerError = true;
                }
            }

            if (aList != null && aList.size() > 0 && controllerError == false) {
                article = aList.get(0);

                // Check current user voting allowrance
                allowCurrentUserToVote = checkIfAllowUserToVote(article.Id, currentUser.Id);
                currentRatingScore = getNormalizedRatingScore(article.Id);

                // Article Version Record
                List<Employee_Article__kav> avList;

                if (isPreviewingArticle == false ) {
                    avList = [SELECT Id, (SELECT Id, ParentId, DataCategoryGroupName, DataCategoryName From DataCategorySelections), 
                                                        LastPublishedDate, OwnerId, Article_Owner__c, Description__c, Create_Request_URL__c, Case_Creation_Target_System__c, 
                                                        Service_Central__c, Downloads__Length__s, Title, Summary, UrlName
                                                        FROM Employee_Article__kav WHERE KnowledgeArticleId =: idStr AND PublishStatus = 'online' AND isLatestVersion = true 
                                                        UPDATE VIEWSTAT];
                }
                else {
                    avList = [SELECT Id, KnowledgeArticleId, (SELECT Id, ParentId, DataCategoryGroupName, DataCategoryName From DataCategorySelections), 
                                                        LastPublishedDate, OwnerId, Article_Owner__c, Description__c, Create_Request_URL__c, Case_Creation_Target_System__c, 
                                                        Service_Central__c, Downloads__Length__s, Title, Summary, UrlName
                                                        FROM Employee_Article__kav WHERE Id =: previewDraftVersionIdStr limit 1];
                }
                
                if (avList != null && avList.size() > 0) {
                    articleLastestVersion = avList.get(0);
                    controllerError = false;
                    
                    // Article last publish date convertion to user local time zone
                    TimeZone userTimezone = UserInfo.getTimeZone();

                    lastPublishedDateInUserTimeZone = articleLastestVersion.LastPublishedDate.format('yyyy-MM-dd', userTimezone.getID());

                    // Article Owner information
                    List<User> userList = [SELECT Id, Name FROM User WHERE Id =: articleLastestVersion.Article_Owner__c];

                    // If Article Owner is not populated, use the object ownwer instead. (User who uploaded this article)
                    if (userList == null || userList.size() == 0) {
                        userList = [SELECT Id, Name FROM User WHERE Id =: articleLastestVersion.OwnerId];
                    }

                    if (userList != null && userList.size() > 0) {
                        ownerName = userList.get(0).Name;
                    }
                    else {
                        ownerName = '';
                    }

                    // Article Download Link Check
                    if (articleLastestVersion.Downloads__Length__s > 0) {
                        hasDownload = true;
                    }
                    else {
                        hasDownload = false;
                    }

                    // Find the Data Category we interested in

                    Employee_Article__DataCategorySelection targetDataCategory;

                    //dataCategoryStructureLevelStr = '';
                    dataCategoryStructureList = new List<String>();


                    List<Employee_Article__DataCategorySelection> articleDataCategoriesList = articleLastestVersion.DataCategorySelections;

                    // Ued the defined category to get the structure level of the Data Category
                    nodeMap = new Map<String, Wrapper>();
                    getDescribeDataCategoryGroupStructureResults(nodeMap);


                    for (Employee_Article__DataCategorySelection oneCat : articleDataCategoriesList) {
                        
                        if (oneCat.DataCategoryGroupName == DATA_CATEGORY_GROUPNAME) {                          
                            targetDataCategory = oneCat;

                            // dataCategoryStructureLevelStr = getStructureLevels(dataCategoryStructureList, nodeMap, targetDataCategory.DataCategoryName);
                            getStructureLevels(dataCategoryStructureList, nodeMap, targetDataCategory.DataCategoryName);
                            break;
                        }
                    }

                    realURL = '';
                    showRaiseCaseBtn = false;
                    if (articleLastestVersion.Create_Request_URL__c != null && articleLastestVersion.Create_Request_URL__c != '') {

                        
                        if (articleLastestVersion.Case_Creation_Target_System__c == 'Salesforce') {

                            // Salesforce case: retrive URL directly
                            realURL = articleLastestVersion.Create_Request_URL__c.unescapeHtml4().substringBetween('<a href=\"','\" target=');
                            showRaiseCaseBtn = true;
                        }
                        else if (articleLastestVersion.Case_Creation_Target_System__c == 'Service Central') {

                            // ServiceNow case:
                            // 1. Read Service Central Catalog Name
                            // 2. Retrive From Id from custom setting by Service Central Catalog Name
                            // 3. Read Url from Service Central URL and append form id to the URL as parameter

                            if (articleLastestVersion.Service_Central__c != null && articleLastestVersion.Service_Central__c != '') {
                                String categoryName = articleLastestVersion.Service_Central__c.escapeUnicode();
                                categoryName = categoryName.replace('\\u00A0', ' ');

                                List<ServiceNow_FormID__c> serviceNowCustomSettingList = [SELECT catelog__c, Id, Name FROM ServiceNow_FormID__c WHERE catelog__c =: categoryName limit 1];

                                if ( serviceNowCustomSettingList != null && serviceNowCustomSettingList.size() > 0) {
                                    String serviceNowFormID = serviceNowCustomSettingList.get(0).Name;

                                    System.debug(articleLastestVersion.Create_Request_URL__c);
                                    realURL = articleLastestVersion.Create_Request_URL__c.unescapeHtml4().substringBetween('<a href=\"','\" target=');
                                    realURL += serviceNowFormID;
                                    showRaiseCaseBtn = true;
                                }
                            }
                        }
                        
                    }

                    topics = [SELECT EntityId, EntityType, Topic.Id, Topic.Name FROM TopicAssignment WHERE EntityId =: articleLastestVersion.Id AND EntityType='Employee_Article'];

                    if (topics != null && topics.size() > 0) {
                        hasTopic = true;
                    }
                    else {
                        hasTopic = false;
                    }

                }
                else {
                    controllerError = true;
                }
            }
            else {
                controllerError = true;
            }
        }
    }

    /** Private Methods */

    /**
     * A helper method that checks if the current user is allowed to vote
     * Yes, if not voted. No, if already voted.
     *
     * @param   articleId           Current article salesforce record id
     * @param   currentUserId       Current user salesforce record id
     * @return  a boolean flag that indicates allowance to vote
     *
     */
    private Boolean checkIfAllowUserToVote(Id articleId, Id currentUserId) {
        Boolean allowToVote = false;

        List<Vote> currentUserVoting = [SELECT CreatedById, Id, Type FROM Vote WHERE ParentId =: articleId AND CreatedById =: currentUserId limit 1];
        if (currentUserVoting == null || currentUserVoting.size() == 0) {
            allowToVote = true;
        }

        return allowToVote;
    }

    /**
     * A helper method that query for the query for the current article's normalized rating score
     *
     * @param   articleId           Current article salesforce record id
     * @return  a rounded up integer that represents the rating score.
     *
     */
    private Integer getNormalizedRatingScore(Id articleId) {
        List<Employee_Article__VoteStat> rating = [SELECT Id, Channel, NormalizedScore FROM Employee_Article__VoteStat WHERE ParentId =: articleId AND Channel = 'AllChannels' limit 1];

        if (rating != null && rating.size() > 0) {
            Double normalizedScore = rating.get(0).NormalizedScore;
            return normalizedScore.round().intValue();
        }
        else {
            return 0;
        }
    }

    /**
     * A helper method that helps to build the data category structure map
     *
     * @param   nodeMap           the nodeMap that passed in and used to store the structure information.
     * @return  None.
     *
     */
    private void getDescribeDataCategoryGroupStructureResults(Map<String, Wrapper> nodeMap){
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        try {
            //Making the call to the describeDataCategoryGroups to
            //get the list of category groups associated
            List<String> objType = new List<String>();
            objType.add('KnowledgeArticleVersion');

            describeCategoryResult = Schema.describeDataCategoryGroups(objType);

            //Creating a list of pair objects to use as a parameter
            //for the describe call
            List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();

            //Looping throught the first describe result to create
            //the list of pairs for the second describe call
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }

            //describeDataCategoryGroupStructures()
            describeCategoryStructureResult = 
            Schema.describeDataCategoryGroupStructures(pairs, false);

            //Getting data from the result
            for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){
                //Get name of the associated Sobject
                singleResult.getSobject();

                //Get the name of the data category group
                singleResult.getName();

                //Get the name of the data category group
                singleResult.getLabel();

                //Get the description of the data category group
                singleResult.getDescription();

                //Get the top level categories
                DataCategory [] toplevelCategories = 
                singleResult.getTopCategories();

                //Recursively get all the categories
                List<DataCategory> allCategories = getAllCategories(toplevelCategories);

                List<DataCategory> categoryToIterate = new List<DataCategory>();

                for(DataCategory category: allCategories){
                    if(category.getName().equalsIgnoreCase('All')){

                        categoryToIterate.add(category);
                    }
                }

                buildCategoryStructureTree(nodeMap, categoryToIterate, null);
            }
        } 
        catch (Exception e){
        }
    }
    
    /**
     * A helper method that helps the above 'getDescribeDataCategoryGroupStructureResults' method
     */
    private DataCategory[] getAllCategories(DataCategory [] categories){
        if(categories.isEmpty()){
            return new DataCategory[]{};
        } else {
            DataCategory [] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[]{category};
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            return allCategories;
        }
    }

    /**
     * A helper method that use the data category structure map to build a tree structure of the data category
     *
     * @param   nodeMap             the nodeMap that passed in and used to store the structure information.
     * @param   allCategories       All child categories of their parent node
     * @param   parentNode          Parent node of the child categories
     * @return  None.
     *
     */
    private void buildCategoryStructureTree(Map<String, Wrapper> nodeMap, List<DataCategory> allCategories, Wrapper parentNode) {
        for (DataCategory oneCategory : allCategories) {

            Wrapper currentNode = new Wrapper();
            currentNode.label = oneCategory.getLabel();
            currentNode.apiName = oneCategory.getName();

            if (parentNode != null) {
                currentNode.parent = parentNode;
            }

            nodeMap.put(oneCategory.getName(), currentNode);

            List<DataCategory> children = oneCategory.getChildCategories();

            if (children.isEmpty() == false) {
                buildCategoryStructureTree(nodeMap, children, currentNode);
            }
        }
    }

    /**
     * A helper method that use the data category structure map and retrive the complete level structure by recursion
     *
     * @param   nodeMap                 the nodeMap that holds the data category structure information in tree structure.
     * @param   lastLevelApiName        deepest level of the selected 
     * @return  A structure string that shows on the webpage
     *
     */
    private void getStructureLevels(List<String> dataCategoryStructureList, Map<String, Wrapper> structureMap, String lastLevelApiName) {
        Wrapper lastWrapper = structureMap.get(lastLevelApiName);

        if (lastWrapper.parent == null) {
            if (lastWrapper.label.equalsIgnoreCase('All')) {
                dataCategoryStructureList.add('');
                //return 'Experian All';
            }
            else {
                dataCategoryStructureList.add(lastWrapper.label);
                //return lastWrapper.label;
            }
        }
        else {
            //String parentCategoryLabel = getStructureLevels(dataCategoryStructureList, structureMap, lastWrapper.parent.apiName);
            getStructureLevels(dataCategoryStructureList, structureMap, lastWrapper.parent.apiName);
            //if (parentCategoryLabel == '') {
            //    //return lastWrapper.label;
            //}
            //else {
            //    dataCategoryStructureList.add(lastWrapper.label);
            //    //return parentCategoryLabel + ' ► ' +  lastWrapper.label;
            //}
            dataCategoryStructureList.add(lastWrapper.label);
        }
        
    }


    /** Public Methods */

    public Integer getNewRatingScore() {
        return getNormalizedRatingScore(article.Id);
    }

    /**
     * A helper method that creates a Up vote record and linked it to the article
     *
     * @param   None.
     * @return  null to refresh the page
     *
     */
    public PageReference upVoteCurrentArticle() {

        if (allowCurrentUserToVote) {
            Vote newVote = new Vote();
            newVote.ParentId = article.Id;
            newVote.Type = 'Up';

            insert newVote;

            allowCurrentUserToVote = false;
            //getNormalizedRatingScore(article.Id);
        }
        return null;
    }

    /**
     * A helper method that creates a Down vote record and linked it to the article
     *
     * @param   None.
     * @return  null to refresh the page
     *
     */
    public PageReference downVoteCurrentArticle() {

        if (allowCurrentUserToVote) {
            Vote newVote = new Vote();
            newVote.ParentId = article.Id;
            newVote.Type = 'Down';

            insert newVote;

            allowCurrentUserToVote = false;
            //getNormalizedRatingScore(article.Id);
        }
        return null;
    }

    /**
     * A Wrapper class that helps to build the tree structure of the data categories
     *
     */
    public class Wrapper {
        public String label {get; set;}
        public String apiName {get; set;}
        public Wrapper parent {get; set;}
        public List<Wrapper> children {get; set;}

        public Wrapper() {
            children = new List<Wrapper>();
        }
    }
}