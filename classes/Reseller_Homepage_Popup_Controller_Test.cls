/**
* @description Unit test for Reseller_Homepage_Popup_Controller
*
* @author Alvin Pico, UC Innovation
*
* @date 06/23/2017
*/
@isTest
private class Reseller_Homepage_Popup_Controller_Test {
	
	static testMethod void testGetUserAgreementAlertFlag() {

		User thisUser = [SELECT Id, UserAgreementAlert__c FROM User WHERE Id = :UserInfo.getUserId()];
		Boolean test = thisUser.UserAgreementAlert__c;

		System.Test.startTest();
		System.runAs(thisUser) {

			System.assertEquals(test, Reseller_Homepage_Popup_Controller.getUserAgreementAlertFlag());
		}
		System.Test.stopTest();

	}

	static testMethod void testSetUserAgreementAlertFlag() {

		User thisUser = [SELECT Id, UserAgreementAlert__c FROM User WHERE Id = :UserInfo.getUserId()];
		Id currentUserId = thisUser.Id;

		System.Test.startTest();
		System.runAs(thisUser) {

			Reseller_Homepage_Popup_Controller.setUserAgreementAlertFlag(true);
			User currentUser = [SELECT Id, UserAgreementAlert__c FROM User WHERE Id = :currentUserId];

			System.assertEquals(true, currentUser.UserAgreementAlert__c);
		}
		System.Test.stopTest();
	}
	
}