/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OppPlanContactRelationshipControllerTest {
  private static Opportunity objOpportunity;
  
  /*
  @isTest
    public static void testNewRelationship() {
      createOpportunityAndAccount();
      
      Contact contactOne = createContact('5464313156', 'CNTACT1');
      Contact contactTwo = createContact('56556896161', 'CNTACT2');
      Opportunity_Plan__c oppPlan = createOpportunityPlan();
      
      Opportunity_Plan_Contact__c oppPlanContactOne = createOpportunityPlanContact(contactOne.Id, oppPlan.Id);
      Opportunity_Plan_Contact__c oppPlanContactTwo = createOpportunityPlanContact(contactTwo.Id, oppPlan.Id);
      
      ApexPages.currentPage().getParameters().put('oppPlanId', oppPlan.Id);
      ApexPages.currentPage().getParameters().put('defaultPlanContact', oppPlanContactOne.Id);
      ApexPages.currentPage().getParameters().put('retURL', oppPlan.Id);
      
      OppPlanContactRelationshipController relationship = new OppPlanContactRelationshipController(new ApexPages.StandardController(oppPlanContactOne));
      
      relationship.getExistingWrapperLists();
      
      relationship.addRelationship();
      relationship.oppPlanRelationshipWrapperList.get(0).getRelationshipType();
      relationship.oppPlanRelationshipWrapperList.get(0).getContactOneSelectList();
      relationship.oppPlanRelationshipWrapperList.get(0).getContactTwoSelectList();
      relationship.wrapperChanged = '1';
      relationship.removeRelationship();
      relationship.oppPlanRelationshipWrapperList.get(0).selectedContactOne = contactOne.Id;
      relationship.oppPlanRelationshipWrapperList.get(0).selectedRelationship = 'Positive/Alliance';
      relationship.oppPlanRelationshipWrapperList.get(0).selectedContactTwo = contactTwo.Id;
      relationship.save();
         
      List <Plan_Contact_Relationship__c> relationshipList = [Select Id From Plan_Contact_Relationship__c Where Contact_1__c =: oppPlanContactOne.Id];
      
      System.assert(relationshipList.size() == 1, 'this assertion failed, size is: ' + relationshipList.size());   
    }
  */
  
  @isTest
    public static void testExistingRelationship() {
      
      createOpportunityAndAccount();
      
      Contact contactOne = createContact('5464313156', 'CNTACT1');
      Contact contactTwo = createContact('56556896161', 'CNTACT2');
      Contact contactThree = createContact('548712131563', '57127789');
      Contact contactFour = createContact('45711614', '9874335');
      Opportunity_Plan__c oppPlan = createOpportunityPlan();
      
      
      Opportunity_Plan_Contact__c oppPlanContactOne = createOpportunityPlanContact(contactOne.Id, oppPlan.Id);
      Opportunity_Plan_Contact__c oppPlanContactTwo = createOpportunityPlanContact(contactTwo.Id, oppPlan.Id);
      Opportunity_Plan_Contact__c oppPlanContactThree = createOpportunityPlanContact(contactThree.Id, oppPlan.Id);
      Opportunity_Plan_Contact__c oppPlanContactFour = createOpportunityPlanContact(contactFour.Id, oppPlan.Id);
      
      
      List<Plan_Contact_Relationship__c> relationshipList = new List<Plan_Contact_Relationship__c>();
      Plan_Contact_Relationship__c relationship1 = new Plan_Contact_Relationship__c();
      relationship1.Contact_1__c = oppPlanContactThree.Id;
      relationship1.Contact_2__c = oppPlanContactFour.Id;
      relationship1.Relationship__c = 'Positive/Alliance';
      relationshipList.add(relationship1);
       
      System.debug('contact 3 is: ' + oppPlanContactThree);
      System.debug('opp plan is: '+ oppPlan.Id);
      String idString = String.valueOf(oppPlan.Id);
      String shortenId = idString.subString(0, 15);
      System.debug('shorted id is: ' + shortenId);
      
      test.StartTest();
       
      insert relationship1;
      
      Plan_Contact_Relationship__c updatedRelationship1 = [select Contact_1_Opp_Plan_ID__c, Contact_1__c, Contact_2__c From Plan_Contact_Relationship__c Where Id =: relationship1.Id];
      
      System.debug('relationship is: ' + updatedRelationship1);
      System.debug('contact 1 opp plan id: ' + updatedRelationship1.Contact_1_Opp_Plan_ID__c);
      ApexPages.currentPage().getParameters().put('oppPlanId', shortenId);
      ApexPages.currentPage().getParameters().put('defaultPlanContact', oppPlanContactOne.Id);
      ApexPages.currentPage().getParameters().put('retURL', oppPlan.Id);
      
      OppPlanContactRelationshipController relationship = new OppPlanContactRelationshipController(new ApexPages.StandardController(oppPlanContactOne));
      System.debug('before getting existing wrappers');
      relationship.getExistingWrapperLists();
      System.debug('after getting existing wrappers');
      
      relationship.addRelationship();
      relationship.oppPlanRelationshipWrapperList.get(0).getRelationshipType();
      relationship.oppPlanRelationshipWrapperList.get(0).getContactOneSelectList();
      relationship.oppPlanRelationshipWrapperList.get(0).getContactTwoSelectList();
      
      relationship.existingRelationshipWrapperList.get(0).getRelationshipType();
      relationship.existingRelationshipWrapperList.get(0).getContactTwoSelectList();
      relationship.existingRelationshipWrapperList.get(0).selectedRelationship = 'Negative/Tension';
      relationship.existingRelationshipWrapperList.get(0).selectedContactTwo = contactFour.Id;
      relationship.wrapperChanged = '1';
      relationship.removeRelationship();
      relationship.existingWrapperChanged = '1';
      relationship.removeExistingRelationship();
      relationship.deletedWrapperChanged = '1';
      relationship.undoDelete();
      relationship.existingRelationshipWrapperList.get(0).selectedRelationship = 'Negative/Tension';
      relationship.existingRelationshipWrapperList.get(0).selectedContactTwo = contactFour.Id;
      relationship.oppPlanRelationshipWrapperList.get(0).selectedContactOne = contactOne.Id;
      relationship.oppPlanRelationshipWrapperList.get(0).selectedContactTwo = contactTwo.Id;
      relationship.save();
      
      Plan_Contact_Relationship__c updatedRelationship = [Select Id, Relationship__c From Plan_Contact_Relationship__c Where Id =: relationship1.Id];
      
      System.assert(updatedRelationship.Relationship__c == 'Negative/Tension', 'the relationship is wrong: ' + updatedRelationship.Relationship__c);
      
      test.stopTest();
        
    }
  
  static void createOpportunityAndAccount() {
        Account objAccount = Test_Utils.insertAccount();
        objOpportunity = Test_Utils.insertOpportunity(objAccount.Id);
    }
    
  //Create Test Contact record
    static Contact createContact(String contactFirstName, String contactLastName) {
        Contact objContact = new Contact(FirstName = contactFirstName, LastName = contactLastName);
        insert objContact;
        system.assertNotEquals(null, objContact.Id, 'Failed to insert Contact record');
        return objContact;
    }
    
    //Create Test Opportunity Plan record
    static Opportunity_Plan__c createOpportunityPlan() {
        //Account objAccount = createAccount();
        Account objAccount = Test_Utils.insertAccount();
        //system.assertNotEquals(null, objAccount.Id, 'Failed to insert Account record');
        system.debug('acc>>' +objAccount);

        objOpportunity = Test_Utils.insertOpportunity(objAccount.Id);
        system.debug('objOpportunity>>' +objOpportunity.Id);
        //system.assertEquals(null, objOpportunity.Id, 'Failed to insert Opportunity record');
        //objOpportunity = createOpportunity(objAccount.Id);
        system.debug('opp>>' +objOpportunity.Id);

        List<Opp_Plan_Score_Calc__c> listOppPlanCalc = new List<Opp_Plan_Score_Calc__c>();

        Opp_Plan_Score_Calc__c oppPlanCalc_InformationScoring = Test_Utils.insertOppPlanScoreCalc('Information Scoring', false);
        listOppPlanCalc.add(oppPlanCalc_InformationScoring);
        Opp_Plan_Score_Calc__c oppPlanCalc_QualificationScoring = Test_Utils.insertOppPlanScoreCalc('Qualification Scoring', false);
        listOppPlanCalc.add(oppPlanCalc_QualificationScoring);
        Opp_Plan_Score_Calc__c oppPlanCalc_BuyingCentre = Test_Utils.insertOppPlanScoreCalc('Buying Centre', false);
        listOppPlanCalc.add(oppPlanCalc_BuyingCentre);
        Opp_Plan_Score_Calc__c oppPlanCalc_CompetitionScoring = Test_Utils.insertOppPlanScoreCalc('Competition Scoring', false);
        listOppPlanCalc.add(oppPlanCalc_CompetitionScoring);
        Opp_Plan_Score_Calc__c oppPlanCalc_SummaryPosition = Test_Utils.insertOppPlanScoreCalc('Summary Position', false);
        listOppPlanCalc.add(oppPlanCalc_SummaryPosition);
        Opp_Plan_Score_Calc__c oppPlanCalc_SolutionAtGlance = Test_Utils.insertOppPlanScoreCalc('Solution at a Glance', false);
        listOppPlanCalc.add(oppPlanCalc_SolutionAtGlance);
        Opp_Plan_Score_Calc__c oppPlanCalc_JointActionPlan = Test_Utils.insertOppPlanScoreCalc('Joint Action Plan', false);
        listOppPlanCalc.add(oppPlanCalc_JointActionPlan);
        Opp_Plan_Score_Calc__c oppPlanCalc_ValueProposition = Test_Utils.insertOppPlanScoreCalc('Value Proposition', false);
        listOppPlanCalc.add(oppPlanCalc_ValueProposition);
        Opp_Plan_Score_Calc__c oppPlanCalc_ActionPlan = Test_Utils.insertOppPlanScoreCalc('Action Plan', false);
        listOppPlanCalc.add(oppPlanCalc_ActionPlan);
        insert listOppPlanCalc;

        List<Opp_Plan_Score_Sub_Calc__c> listSubOppPlanScoreSubCalc = new List<Opp_Plan_Score_Sub_Calc__c>();
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc1 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_InformationScoring.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc2 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_QualificationScoring.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc3 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_BuyingCentre.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc4 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_SummaryPosition.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc5 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_SolutionAtGlance.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc6 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_JointActionPlan.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc7 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_ValueProposition.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc8 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_ActionPlan.Id, false);
        Opp_Plan_Score_Sub_Calc__c oppPlanScoreSubCalc9 = Test_Utils.insertOppPlanScoreSubCalc(oppPlanCalc_CompetitionScoring.Id, false);

        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc1);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc2);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc3);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc4);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc5);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc6);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc7);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc8);
        listSubOppPlanScoreSubCalc.add(oppPlanScoreSubCalc9);
        insert listSubOppPlanScoreSubCalc;

        /*Opp_Plan_Score_Calc__c oppPlnScore = new Opp_Plan_Score_Calc__c ();
        oppPlnScore.Name = 'Information Scoring';
        oppPlnScore.Expected_Score__c = 2;
        insert oppPlnScore;
        system.debug('oppPlnScore>>' +oppPlnScore);

        Opp_Plan_Score_Sub_Calc__c oppPlnScoreSub = new Opp_Plan_Score_Sub_Calc__c();
        oppPlnScoreSub.Name = 'Benefits';
        oppPlnScoreSub.Calculation_Field__c = 'Benefits__c';
        oppPlnScoreSub.Object_API_Name__c = 'Opportunity_Plan__c';
        insert oppPlnScoreSub;*/

        Opportunity_Plan__c objOpportunityPlan = new Opportunity_Plan__c(
            Name = 'Test Plan - 001',
            Account_Name__c = objAccount.Id,
            Opportunity_Name__c = objOpportunity.Id,
            Client_Goal_1__c = 'client goal 1',
            Client_Goal_2__c = 'client goal 2',
            Client_Goal_3__c = 'client goal 3',
            Client_Goal_4__c = 'client goal 4',
            Client_Goal_5__c = 'client goal 5',
            Exp_Risk_1__c = 'Exp risk 1',
            Exp_Risk_2__c = 'Exp risk 2',
            Exp_Risk_3__c = 'Exp risk 3',
            Exp_Risk_4__c = 'Exp risk 4',
            Exp_Risk_5__c = 'Exp risk 5',
            Exp_Strength_1__c = '',
            Exp_Strength_2__c = '',
            Exp_Strength_3__c = '',
            Exp_Strength_4__c = '',
            Exp_Strength_5__c = '',
            Sales_Objective_1__c = 'objective 1',
            Sales_Objective_2__c = 'objective 2',
            Sales_Objective_3__c = 'objective 3',
            Sales_Objective_4__c = 'objective 4',
            Sales_Objective_5__c = 'objective 5',
            CG_1_Importance__c = '1',
            CG_2_Importance__c = '2',
            CG_3_Importance__c = '3',
            CG_4_Importance__c = '4',
            CG_5_Importance__c = '5',
            Opportunity_Expected_Close_Date__c = Date.today(), // NLG June 25, 2014
            Risk_1_Rating__c = '1',
            Risk_2_Rating__c = '2',
            Risk_3_Rating__c = '3',
            Risk_4_Rating__c = '4',
            Risk_5_Rating__c = '5',
            SO_1_Importance__c = '1',
            SO_2_Importance__c = '2',
            SO_3_Importance__c = '3',
            SO_4_Importance__c = '4',
            SO_5_Importance__c = '5',
            Solution_Fulfils_Requirements__c = '',
            Opportunity_Client_Budget__c = '1,001 - 10,000'
        );
        insert objOpportunityPlan;
        system.assertNotEquals(null, objOpportunityPlan.Id, 'Failed to insert Opportunity Plan record');
        return objOpportunityPlan;
    }
    
  //Create Test Opportunity Plan Contact record
    static Opportunity_Plan_Contact__c createOpportunityPlanContact(Id contactId, Id opportunityPlanId) {
        Opportunity_Plan_Contact__c objOpportunityPlanContact = new Opportunity_Plan_Contact__c(
            Contact__c = contactId,
            Opportunity_Plan__c = opportunityPlanId,
            Business_Goal_1__c = 'Based on our experience 1',
            Business_Goal_2__c = 'Based on our experience 2',
            Business_Goal_3__c = 'Based on our experience 3',
            Business_Goal_4__c = 'Based on our experience 4',
            Business_Goal_5__c = 'Based on our experience 5',
            Decision_Criteria_1__c = 'The solution you choose 1',
            Decision_Criteria_2__c = 'The solution you choose 2',
            Decision_Criteria_3__c = 'The solution you choose 3',
            Decision_Criteria_4__c = 'The solution you choose 4',
            Decision_Criteria_5__c = 'The solution you choose 5',
            Exp_Differentiator_1__c = 'From our conversations 1',
            Exp_Differentiator_2__c = 'From our conversations 2',
            Exp_Differentiator_3__c = 'From our conversations 3',
            Exp_Differentiator_4__c = 'From our conversations 4',
            Exp_Differentiator_5__c = 'From our conversations 5',
            Personal_Goal_1__c = '',
            Personal_Goal_2__c = '',
            Personal_Goal_3__c = '',
            Personal_Goal_4__c = '',
            Personal_Goal_5__c = '',
            Solution_Benefits_1__c = 'The solution we have designed 1',
            Solution_Benefits_2__c = 'The solution we have designed 2',
            Solution_Benefits_3__c = 'The solution we have designed 3',
            Solution_Benefits_4__c = 'The solution we have designed 4',
            Solution_Benefits_5__c = 'The solution we have designed 5',
            Confidence_BG_1__c = '1',
            Confidence_BG_2__c = '2',
            Confidence_BG_3__c = '3',
            Confidence_BG_4__c = '4',
            Confidence_BG_5__c = '5',
            Confidence_DC_1__c = '1',
            Confidence_DC_2__c = '2',
            Confidence_DC_3__c = '3',
            Confidence_DC_4__c = '4',
            Confidence_DC_5__c = '5',
            Confidence_PG_1__c = '1',
            Confidence_PG_2__c = '2',
            Confidence_PG_3__c = '3',
            Confidence_PG_4__c = '4',
            Confidence_PG_5__c = '5',
            Importance_BG_1__c = '1',
            Importance_BG_2__c = '2',
            Importance_BG_3__c = '3',
            Importance_BG_4__c = '4',
            Importance_BG_5__c = '5',
            Importance_DC_1__c = '1',
            Importance_DC_2__c = '2',
            Importance_DC_3__c = '3',
            Importance_DC_4__c = '4',
            Importance_DC_5__c = '5',
            Importance_ED_1__c = '1',
            Importance_ED_2__c = '2',
            Importance_ED_3__c = '3',
            Importance_ED_4__c = '4',
            Importance_ED_5__c = '5',
            Importance_PG_1__c = '1',
            Importance_PG_2__c = '2',
            Importance_PG_3__c = '3',
            Importance_PG_4__c = '4',
            Importance_PG_5__c = '5',
            Importance_SB_1__c = '1',
            Importance_SB_2__c = '2',
            Importance_SB_3__c = '3',
            Importance_SB_4__c = '4',
            Importance_SB_5__c = '5'
        );
        insert objOpportunityPlanContact;
        system.assertNotEquals(null, objOpportunityPlanContact.Id, 'Failed to insert Opportunity Plan Contact record');
        return objOpportunityPlanContact;
    }
}