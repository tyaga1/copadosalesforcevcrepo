/******************************************************************************
 * Appirio, Inc
 * Name: BatchAccountSegmentDelete_Test.cls
 * Description: Test class for BatchAccountSegmentDelete.cls
 *
 * Created Date: Sep 18th, 2015.
 * Created By: Nathalie Le Guay (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Nov 17th, 2015               Paul Kissick                 Fixed tests
 * Dec 3rd, 2015                Paul Kissick                 Case 01266075: Fixing use of Global_Settings__c for timings
 ******************************************************************************/
@isTest
private class BatchAccountSegmentDelete_Test {
  
  //public static Datetime batchLastRun = ;
  
  public static testmethod void testBatchFullDelete() {
    
    
    TriggerSettings__c triggerSetting = new TriggerSettings__c(Name='AccountTrigger', isActive__c=false);
    insert triggerSetting;
  
    Datetime fourMonthsAgo = Datetime.now().addDays(-120);
    List<Account> accounts = new List<Account>();
    for (Integer i=0; i<10; i++) {
      accounts.add(new Account(Name='Account #'+i));
    }
    insert accounts;
    List<Account_Segment__c> accountSegments = new List<Account_Segment__c>();
    for (Integer i=0; i <100; i++) {
      accountSegments.add(
        new Account_Segment__c(
          Account__c = accounts.get(Math.mod(i, 10)).Id,
          LastModifiedDate = fourMonthsAgo, 
          CreatedDate = fourMonthsAgo.addDays(-30)
        )
      );
    }
    insert accountSegments;
    system.debug('====accountSegments>>>' +accountSegments);
    
    system.assertEquals(100, [SELECT count() FROM Account_Segment__c]);

    Test.startTest();
    BatchAccountSegmentDelete batch = new BatchAccountSegmentDelete();
    Database.executeBatch(batch);
    Test.stopTest();

    system.assertEquals(0, [SELECT count() FROM Account_Segment__c]);
  }

  public static testmethod void testBatchPartialDeleteBasedOnDate() {
    
    TriggerSettings__c triggerSetting = new TriggerSettings__c(Name='AccountTrigger', isActive__c=false);
    insert triggerSetting;

    Datetime fourMonthsAgo = Datetime.now().addDays(-120);
    List<Account> accounts = new List<Account>();
    for (Integer i=0; i<10; i++) {
      accounts.add(new Account(Name='Account #'+i));
    }
    insert accounts;
    List<Account_Segment__c> accountSegments = new List<Account_Segment__c>();
    for (Integer i=0; i <100; i++) {
      accountSegments.add(
        new Account_Segment__c(
          Account__c = accounts.get(Math.mod(i, 10)).Id,
          LastModifiedDate = fourMonthsAgo, 
          CreatedDate = fourMonthsAgo.addDays(-30)
        )
      );
      accountSegments.add(
        new Account_Segment__c(
          Account__c = accounts.get(Math.mod(i, 10)).Id
        )
      );
    }

    insert accountSegments;

    system.assertEquals(200, [SELECT count() FROM Account_Segment__c]);

    Test.startTest();
    BatchAccountSegmentDelete batch = new BatchAccountSegmentDelete();
    Database.executeBatch(batch);
    Test.stopTest();

    system.assertEquals(100, [SELECT count() FROM Account_Segment__c]);
  }


  public static testmethod void testBatchPartialDeleteBasedOnATM() {

    TriggerSettings__c triggerSetting = new TriggerSettings__c(Name='AccountTrigger', isActive__c=false);
    insert triggerSetting;

    Datetime fourMonthsAgo = Datetime.now().addDays(-120);
    List<Account> accounts = new List<Account>();
    for (Integer i=0; i<10; i++) {
      accounts.add(new Account(Name='Account #'+i));
    }
    insert accounts;

    User me = [SELECT Id, Country__c FROM User WHERE ID =:UserInfo.getUserId()];
    me.Country__c = 'Brazil';
    update me;

    Integer modNumber = 0;
    List<AccountTeamMember> atm = new List<AccountTeamMember>();
    for (Account acc: accounts) {
      if (Math.mod(modNumber, 2) == 0) {
        atm.add(new AccountTeamMember(UserId=UserInfo.getUserId(), AccountId= acc.Id));
      }
      modNumber++;
    }
    insert atm;

    List<Account_Segment__c> accountSegments = new List<Account_Segment__c>();
    Boolean updatedDate = false;
    for (Integer i=0; i <100; i++) {
      if (i>=50 && !updatedDate) {
        updatedDate = true; 
        fourMonthsAgo = fourMonthsAgo.addDays(60);
      }
      accountSegments.add(
        new Account_Segment__c(
          Account__c = accounts.get(Math.mod(i, 10)).Id,
          LastModifiedDate = fourMonthsAgo, 
          CreatedDate = fourMonthsAgo.addDays(-30)
        )
      );
    }

    insert accountSegments;

    system.assertEquals(100, [SELECT count() FROM Account_Segment__c]);

    Test.startTest();
    BatchAccountSegmentDelete batch = new BatchAccountSegmentDelete();
    Database.executeBatch(batch);
    Test.stopTest();

    system.assertEquals(50, [SELECT count() FROM Account_Segment__c]);
  }

  
  public static testmethod void testBatchPartialDeleteBasedOnOppty() {
    
    TriggerSettings__c triggerSetting = new TriggerSettings__c(Name='AccountTrigger', isActive__c=false);
    insert triggerSetting;
    
    TriggerSettings__c triggerSettingOpp = new TriggerSettings__c(Name='OpportunityTrigger', isActive__c=false);
    insert triggerSettingOpp;

    Datetime fourMonthsAgo = Datetime.now().addDays(-120);
    List<Account> accounts = new List<Account>();
    for (Integer i=0; i<10; i++) {
      accounts.add(new Account(Name='Account #'+i));
    }
    insert accounts;
    List<Account_Segment__c> accountSegments = new List<Account_Segment__c>();
    for (Integer i=0; i <100; i++) {
      accountSegments.add(
        new Account_Segment__c(
          Account__c = accounts.get(Math.mod(i, 10)).Id,
          LastModifiedDate = fourMonthsAgo,
          CreatedDate = fourMonthsAgo.addDays(-30)
        )
      );
    }

    insert accountSegments;

    List<Opportunity> opps = new List<Opportunity>();
    Integer i = 0;
    for (Account_Segment__c accSegment: accountSegments) {
      if (Math.mod(i, 2) == 0) {
        opps.add(
          new Opportunity(
            Name = 'Test opp for account: '+ accSegment.Account__r.Name,
            AccountId = accSegment.Account__c,
            Segment_Country__c = accSegment.Id,
            StageName = 'Qualify',
            CloseDate = Date.today().addDays(10)
          )
        );
      }
      i++;
    }
    insert opps;

    system.assertEquals(100, [SELECT count() FROM Account_Segment__c]);

    Test.startTest();
    BatchAccountSegmentDelete batch = new BatchAccountSegmentDelete();
    Database.executeBatch(batch);
    Test.stopTest();

    system.assertEquals(50, [SELECT count() FROM Account_Segment__c]);
  }

  public static testmethod void testBatchPartialDeleteBasedOnOrders() {

    TriggerSettings__c triggerSetting = new TriggerSettings__c(Name='AccountTrigger', isActive__c=false);
    insert triggerSetting;

    Datetime fourMonthsAgo = Datetime.now().addDays(-120);
    List<Account> accounts = new List<Account>();
    for (Integer i=0; i<10; i++) {
      accounts.add(new Account(Name='Account #'+i));
    }
    insert accounts;
  
    User me = [SELECT Id, Country__c FROM User WHERE ID =:UserInfo.getUserId()];
    me.Country__c = 'Brazil';
    update me; 

    Integer modNumber = 0;
    List<AccountTeamMember> atm = new List<AccountTeamMember>();
    for (Account acc: accounts) {
      if (Math.mod(modNumber, 2) == 0) {
        atm.add(new AccountTeamMember(UserId=UserInfo.getUserId(), AccountId= acc.Id));
      }
      modNumber++;
    }
    insert atm;

    List<Account_Segment__c> accountSegments = new List<Account_Segment__c>();
    Boolean updatedDate = false;
    for (Integer i=0; i <100; i++) {
      if (i>=50 && !updatedDate) {
        updatedDate = true; 
        fourMonthsAgo = fourMonthsAgo.addDays(60);
      }
      accountSegments.add(
        new Account_Segment__c(
          Account__c = accounts.get(Math.mod(i, 10)).Id,
          LastModifiedDate = fourMonthsAgo, 
          CreatedDate = fourMonthsAgo.addDays(-30)
        )
      );
    }

    insert accountSegments;
    
    List<Order__c> orders = new List<Order__c>();
    Integer i = 0;
    for (Account_Segment__c accSegment: accountSegments) {
      if (Math.mod(i, 2) == 0) {
        orders.add(
          new Order__c(
            Name = 'Test order for account: '+ accSegment.Account__r.Name,
            Account__c = accSegment.Account__c,
            Segment_Country__c = accSegment.Id,
            Transactional_Sale__c = true
          )
        );
      }
      i++;
    }
    insert orders;
    
    system.assertEquals(100, [SELECT count() FROM Account_Segment__c]);
    
    Test.startTest();
    BatchAccountSegmentDelete batch = new BatchAccountSegmentDelete();
    Database.executeBatch(batch,200);
    Test.stopTest();

    system.assertEquals(75, [SELECT count() FROM Account_Segment__c]);
  } 
  
  
  @testSetup
  private static void setupTestData() {
    
    BatchHelper.setBatchClassTimestamp('BatchAccountSegmentDeleteLastRun', Datetime.now().addMinutes(3));
    
    Global_Settings__c custSettings = new Global_Settings__c(Name=Constants.GLOBAL_SETTING);
    insert custSettings;
    
    CPQ_Settings__c CPQSettings = new CPQ_Settings__c();
    CPQSettings.Company_Code__c = 'Experian';
    CPQSettings.Name = 'CPQ';
    CPQSettings.CPQ_API_Endpoint__c = 'https://rssandbox.webcomcpq.com/wsapi/WsSrv.asmx';
    CPQSettings.CPQ_API_UserName__c = 'richard.joseph#ExperianGlobal';
    CPQSettings.CPQ_API_Access_Word__c = 'password';
    insert CPQSettings;
  }
}