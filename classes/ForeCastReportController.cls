/*=============================================================================
 * Experian
 * Name: ForeCastReportController
 * Description: Case-02184517 : ForeCastReportController
 *                         
 * Created Date: 16th Feb 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/
public class ForeCastReportController {
    
    public string strUser{get;set;} 
    public set<Id> setOwnerIds = new set<Id>();
    public set<string> displayedOwners{get;set;}    
    public boolean isDataAvailable{get;set;}
    public Opportunity opp1{get;set;}          
    public SelectOption[] lstUsers{get;set;}
    public SelectOption[] lstSelectedUsers{get;set;}  
    public map<string,Decimal> mapEDQSum{get;set;}
    public map<string,Decimal> mapQuotaSum{get;set;}
    public map<string,Decimal> mapPercentage{get;set;}
    public map<string,list<ForeCastData>> mapForeCast{get;set;}
    public map<string,Decimal> mapOwnerEDQAmount = new map<string,Decimal>();
    public map<string,Decimal> mapConvRate = new map<string,Decimal>();
    public string strDefCurr{get;set;}

    public ForeCastReportController(){              
        lstSelectedUsers = new list<SelectOption>();
        lstUsers = new list<SelectOption>();
        opp1 = new Opportunity();           
        strDefCurr = UserInfo.getDefaultCurrency();
        map<Id,string> mapUserIdName = new map<Id,string>();  

        //Querying ForecastQuota to display the list of Users
        string strQuery = 'SELECT ForecastingTypeId, Id, QuotaOwner.Name, IsAmount, IsQuantity, PeriodId,ProductFamily,QuotaAmount,QuotaOwnerId,QuotaQuantity,StartDate,SystemModstamp FROM ForecastingQuota where QuotaOwner.Business_Unit__c like \'%Data Quality%\'  order by QuotaOwnerId,StartDate';
        list<ForecastingQuota> lstForeCast = Database.query(strQuery);
        for(ForecastingQuota objFore:lstForeCast){
            mapUserIdName.put(objFore.QuotaOwnerId,objFore.QuotaOwner.Name);
            mapOwnerEDQAmount.put(objFore.QuotaOwnerId+':'+objFore.StartDate.Month(),0);
        }
        
        lstUsers = new list<SelectOption>();
        for(Id objId:mapUserIdName.keySet()){
            lstUsers.add(new SelectOption(objId,mapUserIdName.get(objId)));
        }

        mapConvRate = new map<string,Decimal>();
        for(currencytype objCurrType:[SELECT conversionrate,isocode  FROM currencytype]){
            mapConvRate.put(objCurrType.isocode,objCurrType.conversionrate);
        }
        //runReportData();    
    }

     public PageReference exportData(){
        PageReference pf = new PageReference('/apex/ForecastReportExport');
        return pf;
    }

    
        public void usersSelection() {
        
        list<ForecastingQuota> lstForeCast = new list<ForecastingQuota>();        
        map<Id,string> mapUserIdName = new map<Id,string>();  

       // Diplay the Selected Users related to Forecast
        string strQuery = 'SELECT ForecastingTypeId, Id, QuotaOwner.Name, IsAmount, IsQuantity, PeriodId,ProductFamily,QuotaAmount,QuotaOwnerId,QuotaQuantity,StartDate,SystemModstamp FROM ForecastingQuota where QuotaOwner.Business_Unit__c like \'%Data Quality%\'  ';
        if(strUser!=null && strUser!='')
            strQuery+=' AND QuotaOwner.Name Like \'%'+strUser+'%\' ';
        strQuery+=' order by QuotaOwnerId,StartDate';
        lstForeCast = Database.query(strQuery);
        for(ForecastingQuota objFore:lstForeCast){
            mapUserIdName.put(objFore.QuotaOwnerId,objFore.QuotaOwner.Name);

            
        }
        set<string> setSelVal = new set<string>();
        for(SelectOption opt:lstSelectedUsers){
            setSelVal.add(opt.getValue());
        }
        lstUsers = new list<SelectOption>();
        for(Id objId:mapUserIdName.keySet()){
            if(!setSelVal.contains(objId))
                lstUsers.add(new SelectOption(objId,mapUserIdName.get(objId)));
        }        
    }
    
    public void runReportData(){
        if(lstSelectedUsers == null || lstSelectedUsers.size()<1){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please select the users to see the data'));
            return ;
        }

        mapOwnerEDQAmount = new map<string,Decimal>();    
        list<String> selectedUsers = new list<string>();
        for(SelectOption selOpt:lstSelectedUsers){
            selectedUsers.add(selOpt.getValue());

        }  //Querying the Order related to the selected forecast Users 
        AggregateResult[] ordersResults = [SELECT OwnerId, CALENDAR_MONTH(Close_Date__c), SUM(EDQ_Margin__c) FROM Order__c where OwnerId =:selectedUsers AND Status__c!='Cancelled' AND Close_Date__c>=:opp1.Contract_Start_Date__c AND Close_Date__c<=:opp1.Contract_End_Date__c GROUP BY OwnerId, CALENDAR_MONTH(Close_Date__c)];
        for (AggregateResult ar : ordersResults )  {                          
            mapOwnerEDQAmount.put(ar.get('OwnerId')+':'+ar.get('expr0'),(Decimal)ar.get('expr1'));           
        }

        //Querying the Opp related to the selected forecast Users 
        AggregateResult[] oppResults = [SELECT OwnerId, CALENDAR_MONTH(CloseDate), SUM(Total_Margin_Value__c) FROM Opportunity where OwnerId =:selectedUsers AND Forecast_Category__c='Forecast' AND CloseDate>=:opp1.Contract_Start_Date__c AND CloseDate<=:opp1.Contract_End_Date__c GROUP BY OwnerId, CALENDAR_MONTH(CloseDate)];
        for (AggregateResult ar : oppResults )  { 
            if(mapOwnerEDQAmount.containsKey(ar.get('OwnerId')+':'+ar.get('expr0'))){  
                system.debug('Amount#####'+ar.get('expr1'));
                Decimal tcv = mapOwnerEDQAmount.get(ar.get('OwnerId')+':'+ar.get('expr0')) ;  
                tcv += (Decimal)ar.get('expr1')!=null?(Decimal)ar.get('expr1'):0;                  
                mapOwnerEDQAmount.put(ar.get('OwnerId')+':'+ar.get('expr0'),tcv);  
            }else{
                Decimal tcv = (Decimal)ar.get('expr1');                  
                mapOwnerEDQAmount.put(ar.get('OwnerId')+':'+ar.get('expr0'),tcv); 
            }         
        }
        // Building the Wrapper to display the data.
        list<ForeCastData> lstData = new list<ForeCastData>();     
        for(ForecastingQuota objFore:[SELECT CurrencyIsoCode, ForecastingTypeId, Id, QuotaOwner.Name, IsAmount, IsQuantity, PeriodId,ProductFamily,QuotaAmount,QuotaOwnerId,QuotaQuantity,StartDate,SystemModstamp FROM ForecastingQuota where StartDate>= :opp1.Contract_Start_Date__c AND StartDate<= :opp1.Contract_End_Date__c AND QuotaOwnerId=:selectedUsers order by QuotaOwnerId,StartDate ]){
            ForeCastData objF = new ForeCastData();
            objF.objForeCast = objFore;
            //objF.Quota_Amount = objFore.QuotaAmount;
           // objF.EDQ_Amount = mapOwnerEDQAmount.get(objFore.QuotaOwnerId+':'+objFore.StartDate.Month())!=null?mapOwnerEDQAmount.get(objFore.QuotaOwnerId+':'+objFore.StartDate.Month()):0;            
           
            objF.Quota_Amount = objFore.QuotaAmount;
            system.debug('Iffff'+objF.Quota_Amount+'objFore.CurrencyIsoCode@@'+objFore.CurrencyIsoCode+'##'+UserInfo.getDefaultCurrency()+'@@@'+mapConvRate.get(UserInfo.getDefaultCurrency()));
            if(objFore.CurrencyIsoCode != UserInfo.getDefaultCurrency()){
                objF.Quota_Amount = objFore.QuotaAmount * mapConvRate.get(UserInfo.getDefaultCurrency()).setScale(2);
               //If User default currency is USD then the Quota Amount will be calculated by dividing the Conversion rate of Currencyisocode
                if(UserInfo.getDefaultCurrency() == 'USD')
                    objF.Quota_Amount = (objF.Quota_Amount / mapConvRate.get(objFore.CurrencyIsoCode)).setScale(2);
                 system.debug('Iffff'+objF.Quota_Amount+'objFore.CurrencyIsoCode@@'+objFore.CurrencyIsoCode+'##'+UserInfo.getDefaultCurrency());   
            }
            system.debug('lastttIffff'+objF.Quota_Amount+'objFore.CurrencyIsoCode@@'+objFore.CurrencyIsoCode+'##'+UserInfo.getDefaultCurrency());  
            objF.EDQ_Amount = mapOwnerEDQAmount.get(objFore.QuotaOwnerId+':'+objFore.StartDate.Month())!=null?(mapOwnerEDQAmount.get(objFore.QuotaOwnerId+':'+objFore.StartDate.Month()) * mapConvRate.get(UserInfo.getDefaultCurrency())).setScale(2):0;
            system.debug('lastttIffff'+objF.EDQ_Amount+'objFore.CurrencyIsoCode@@'+objFore.CurrencyIsoCode+'##'+UserInfo.getDefaultCurrency());  
            objF.percentage = ((Decimal)(objF.EDQ_Amount /objFore.QuotaAmount) * 100).setScale(2);
            lstData.add(objF);
        }
        mapEDQSum = new map<string,Decimal>();
        mapQuotaSum = new map<string,Decimal>();
        mapPercentage = new map<string,Decimal>();
        displayedOwners = new set<string>();
        mapForeCast = new map<string,list<ForeCastData>>();
        for(ForeCastData obj:lstData){  
            displayedOwners.add(obj.objForeCast.QuotaOwnerId);
            if(mapForeCast.containsKey(obj.objForeCast.QuotaOwnerId)){
                list<ForeCastData> lst = mapForeCast.get(obj.objForeCast.QuotaOwnerId);
                lst.add(obj);
                mapForeCast.put(obj.objForeCast.QuotaOwnerId,lst);
                
                Decimal edec = mapEDQSum.get(obj.objForeCast.QuotaOwnerId);
                edec = edec.setScale(2) + obj.EDQ_Amount;
                mapEDQSum.put(obj.objForeCast.QuotaOwnerId,edec);
                
                Decimal qdec = mapQuotaSum.get(obj.objForeCast.QuotaOwnerId);
                qdec = qdec.setScale(2) + obj.Quota_Amount;
                mapQuotaSum.put(obj.objForeCast.QuotaOwnerId,qdec);
                
            }else{
                list<ForeCastData> lst = new list<ForeCastData>();
                lst.add(obj);
                mapForeCast.put(obj.objForeCast.QuotaOwnerId,lst);
                mapEDQSum.put(obj.objForeCast.QuotaOwnerId,obj.EDQ_Amount);
                mapQuotaSum.put(obj.objForeCast.QuotaOwnerId,obj.Quota_Amount);
            }           
        }
        
        for(string objOwner:mapForeCast.keySet()){  
            Decimal objDec = (Decimal)(mapEDQSum.get(objOwner) / mapQuotaSum.get(objOwner)) * 100;
            mapPercentage.put(objOwner,objDec.setScale(2));
        }
        isDataAvailable = false;
        if(lstData.size()>0)
            isDataAvailable = true;
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Records to display'));
            return ;
        }
    }
    
    public class ForeCastData{
        public ForecastingQuota objForeCast{get;set;}
        public Decimal Quota_Amount{get;set;}
        public Decimal EDQ_Amount{get;set;}
        public Decimal percentage{get;set;}
    
    }
}