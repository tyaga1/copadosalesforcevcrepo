/*=============================================================================
 * Experian
 * Name: MergeHistoryUtility_Test
 * Description: Test class to verify MergeHistoryUtility methods. Case #29746
 * Created Date: Feb 19th, 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 25 Aug, 2016       Paul Kissick          Case 02107571: Fixing failing test
 * 12 Sep, 2017       Mauricio Murillo      Case 02139856: Added account and contact name
=============================================================================*/
@isTest
private class MergeHistoryUtility_Test {

  static testMethod void testMergeHistoryGoodAccountTest() {
    
    Account parAcc1 = Test_Utils.insertAccount();
    parAcc1.CSDA_Integration_Id__c = '00000001';
    parAcc1.EDQ_Integration_Id__c = '00000001';
    update parAcc1;
    Account acc1 = Test_Utils.insertAccount();
    acc1.CSDA_Integration_Id__c = '00000002';
    acc1.EDQ_Integration_Id__c = '00000002';
    acc1.Saas__c = true;
    acc1.ParentId = parAcc1.Id;
    update acc1;
    
    Account acc2 = Test_Utils.insertAccount();
    acc2.CSDA_Integration_Id__c = '00000003';
    acc2.EDQ_Integration_Id__c = '00000003';
    update acc2;
    
    acc1 = [
      SELECT Id,
        name,
        EDQ_Integration_Id__c, 
        CSDA_Integration_Id__c, 
        Global_Unique_ID__c,
        Experian_ID__c,
        Saas__c,
        CreatedById,
        CreatedDate,
        Ultimate_Parent_Account__c, 
        Ultimate_Parent_Account__r.CSDA_Integration_Id__c
      FROM Account WHERE Id = :acc1.Id 
    ];
    acc2 = [
      SELECT Id,
        name,
        EDQ_Integration_Id__c, 
        CSDA_Integration_Id__c, 
        Global_Unique_ID__c,
        Experian_ID__c,
        Saas__c,
        CreatedById,
        CreatedDate,
        Ultimate_Parent_Account__c, 
        Ultimate_Parent_Account__r.CSDA_Integration_Id__c
      FROM Account WHERE Id = :acc2.Id 
    ];
    // acc2 is loser, acc1 is winner.
    MergeHistory__c mh = MergeHistoryUtility.createMergeHistoryRecord(acc2,acc1);
    system.assertEquals(acc1.Id, mh.Winner_Record_ID__c, 'Winner ID is incorrect');
    system.assertEquals(acc2.Id, mh.Loser_Record_ID__c, 'Loser ID is incorrect');
    system.assertEquals('Account', mh.Object_Type__c, 'Object Type field is incorrect');
    System.debug('account1 saas: ' + acc1.Saas__c);
    system.assertEquals(true, mh.Winner_SaaS_Ind__c, 'Winner Saas is incorrect');
    acc1 = [
      SELECT Id
      FROM Account WHERE Id = :acc1.Id 
    ];
    acc2 = [
      SELECT Id
      FROM Account WHERE Id = :acc2.Id 
    ];
    // acc2 is loser, acc1 is winner.
    MergeHistory__c mh2 = MergeHistoryUtility.createMergeHistoryRecord(acc2,acc1);
    system.assertEquals(acc1.Id, mh2.Winner_Record_ID__c, 'Winner ID is incorrect');
    system.assertEquals(acc2.Id, mh2.Loser_Record_ID__c, 'Loser ID is incorrect');
    system.assertEquals(null, mh2.Loser_Top_Parent_ID__c, 'There is a top parent, there shouldn\'t be.');
 
  }
  
  static testMethod void testMergeHistoryGoodContactTest() {
    
    Account acc1 = Test_Utils.insertAccount();
    Contact cont1 = Test_Utils.insertContact(acc1.Id);
    Contact cont2 = Test_Utils.insertContact(acc1.Id);
    
    cont1 = [
      SELECT Id,firstName, lastName,
        EDQ_Integration_Id__c, 
        // CSDA_Integration_Id__c, 
        Global_Unique_ID__c,
        Experian_ID__c
      FROM Contact WHERE Id = :cont1.Id 
    ];
    cont2 = [
      SELECT Id,firstName, lastName,
        EDQ_Integration_Id__c, 
        // CSDA_Integration_Id__c, 
        Global_Unique_ID__c,
        Experian_ID__c
      FROM Contact WHERE Id = :cont2.Id 
    ];
    // acc2 is loser, acc1 is winner.
    MergeHistory__c mh = MergeHistoryUtility.createMergeHistoryRecord(cont2,cont1);
    system.assertEquals(cont1.Id, mh.Winner_Record_ID__c, 'Winner ID is incorrect');
    system.assertEquals(cont2.Id, mh.Loser_Record_ID__c, 'Loser ID is incorrect');
    system.assertEquals('Contact', mh.Object_Type__c, 'Object Type field is incorrect');
    
    cont1 = [
      SELECT Id
      FROM Contact WHERE Id = :cont1.Id 
    ];
    cont2 = [
      SELECT Id
      FROM Contact WHERE Id = :cont2.Id 
    ];
    
    MergeHistory__c mh2 = MergeHistoryUtility.createMergeHistoryRecord(cont2,cont1);
    system.assertEquals(cont1.Id, mh2.Winner_Record_ID__c, 'Winner ID is incorrect');
    system.assertEquals(cont2.Id, mh2.Loser_Record_ID__c, 'Loser ID is incorrect');
    system.assertEquals(null, mh2.Loser_Top_Parent_ID__c, 'There is a top parent, there shouldn\'t be.');
 
  }
  
  static testMethod void testMergeHistoryBadTest() {
    
    // User records can not be merged, and won't have any info.
    List<Profile> twoProfiles = [SELECT Id FROM Profile ORDER BY LastModifiedDate LIMIT 2];
    
    MergeHistory__c mh = MergeHistoryUtility.createMergeHistoryRecord(twoProfiles[0],twoProfiles[1]);
    
    system.assertEquals(null, mh.Loser_Global_Unique_ID__c, 'Somehow, the Global Unique ID field is populated on Profiles!');
    system.assertEquals(null, mh.Winner_Global_Unique_ID__c, 'Somehow, the Global Unique ID field is populated on Profiles!');
    
    
  }
  
}