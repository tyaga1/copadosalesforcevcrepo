/******************************************************************************
 * Appirio, Inc
 * Name: BatchAccountTeamMemberCI
 * Description:  Sync the Account Team Member with the sharing records on the Confidential Information.
 *
 *                  Story:      S-277737
 *                  Task:       T-357474
 * 
 * When scheduling, please use an appropriate batch size as this class frequently fails, e.g. 200 is too large!
 *
 * Created Date: Feb 03rd, 2015.
 * Created By: Naresh Kr Ojha (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Apr 15th, 2015               Rohit B                      T-375000: Updated finish method to send mail to GCS team for error.
 * May 5th, 2015                Nathalie Le Guay             Query only ATM where user is active
 * May 11th, 2015               Noopur                       I-162122 - modified the code to send failed record Ids in the email
 * Jul 20th, 2015               Paul Kissick                 Case #01029615 - Fix for running for 4 days!
 * Aug 20th, 2015               Paul Kissick                 Case #01029615 - Fixing too many DML errors (10000) due to very large accounts (~1500 ci records)
 * Nov 2nd, 2015                Paul Kissick                 Case 01223378: Fixing test errors when email deliverability is off
 * Nov 9th, 2015                Paul Kissick                 Case 01230646: Trying to fix CPU timeout in start query
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Dec 3rd, 2015                Paul Kissick                 Case 01266075: Removing Global Settings
 ******************************************************************************/
global class BatchAccountTeamMemberCI implements Database.Batchable<sObject>, Database.Stateful {

  // public static final String GLOBAL_SETTING = 'Global';
  // public Global_Settings__c lastRun = Global_Settings__c.getInstance(GLOBAL_SETTING);
  
  global Set<String> failedConInfoIDs; // PK Case #01029615 - Fixing reset of this every time
  private Datetime startTime; // PK Case #01029615 - Adding start time to hold when started, in case members change after it starts!
  //=================================================
  // Query all ATM since last run
  //=================================================
  global Database.Querylocator start ( Database.Batchablecontext bc ) {
    DateTime lastRunTime = BatchHelper.getBatchClassTimestamp('AccountTeamMemberCIJobLastRun');
    failedConInfoIDs = new Set<String>(); // PK Case #01029615 - Fixing reset of this every time
    startTime = system.now();
    // PK Case 01230646: Removed subquery for shares
    return Database.getQueryLocator ([
      SELECT Id, Account__c
      FROM Confidential_Information__c
      WHERE Synch_Account_Team_Members__c = true
      AND Account__c IN (
        SELECT AccountId
        FROM AccountTeamMember 
        WHERE User.IsActive = true AND ( CreatedDate >= :lastRunTime OR LastModifiedDate > :lastRunTime)
      )
      // PK Case 01029615 : Replacing query below with that above (context to confidential information)
      //SELECT Id FROM Account 
      //WHERE IsDeleted = false 
      //AND ID IN ( 
      //  SELECT AccountId 
      //  FROM AccountTeamMember 
      //  WHERE User.IsActive= true AND ( CreatedDate >= :lastRunTime OR LastModifiedDate > :lastRunTime)
      //)
      // PK Case 01029615 : Replacing query below with that above
      // SELECT Id, UserId, AccountId, AccountAccessLevel,
      //                                       TeamMemberRole, IsDeleted, CreatedDate
      //                                FROM AccountTeamMember
      //                                WHERE User.IsActive= true AND 
      //                                ( CreatedDate >= :lastRunTime OR LastModifiedDate >: lastRunTime)
    ]);
    
  }

  //===================================================================
  // Execute - will loop through ATM and provide access to CI records
  //===================================================================
  // PK Case 01029615 - Changing scope to account from AccountTeamMember
  // Changing scope to Confidential_Information__c
  global void execute (Database.BatchableContext bc, List<Confidential_Information__c> scope) {
    // global void execute (Database.BatchableContext bc, List<Account> scope) {
    //Set<ID> accountIDs = new Set<ID>();
    //for (AccountTeamMember atm : (List<AccountTeamMember>) scope) {
    //  accountIDs.add(atm.AccountId);
    //}
    //synchAccountWithConfidentialInformation(accountIDs);
    //Map<Id,Account> accountIdMap = new Map<Id,Account>();
    //accountIdMap.putAll(scope);
    //synchAccountWithConfidentialInformation(accountIdMap.keySet());
    synchConfidentialInformation(scope);
            
  }

  //===================================================================
  // Finish - Revert update to Custom Settings if necessary
  //===================================================================
  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchAccountTeamMemberCI', false);
    
    Integer numberOfErrors = 0;
    numberOfErrors += (failedConInfoIDs != null && failedConInfoIDs.size() > 0) ? failedConInfoIDs.size() : 0;
    
    BatchHelper.setBatchClassTimestamp('AccountTeamMemberCIJobLastRun', startTime);
    
    String emailBody = '';
         
    if (failedConInfoIDs != null && failedConInfoIDs.size() > 0) {
      bh.batchHasErrors = true;
      
      emailBody += '*** Confidential Information Share records insert failed:\n\n'; 
      // emailBody += '<table><tr ><td width="15%">Confidential Information Id</td><td width="15%">User Id</td><td width="*">Exception</td></tr>';
      for (String currentRow : failedConInfoIDs) {
        emailBody += currentRow+'\n\n';
      }
      emailBody += '\n\n';       
    }
    
    bh.emailBody += emailBody;
    
    bh.sendEmail();
  }

  //===================================================================
  // Delete Shares and recreate them
  //===================================================================
  private void synchConfidentialInformation (List<Confidential_Information__c> confInfoList) {
    // List<Confidential_Information__c> confInfoList = new List<Confidential_Information__c>();
    Set<Id> accountIDs = new Set<Id>();
    
    // PK Case 01230646: New query for current shares
    List<Confidential_Information__Share> confShares = [
      SELECT Id 
      FROM Confidential_Information__Share
      WHERE ParentID IN :confInfoList
      AND RowCause = 'Account_Team__c'
    ];
    
    // List<Confidential_Information__Share> confShares = new List<Confidential_Information__Share>();
    for(Confidential_Information__c ci : confInfoList) {
      accountIDs.add(ci.Account__c);
    }

    if (confShares.size() > 0) {
      try {
        Database.delete(confShares, false);
        createConfidentialInfoShare (accountIDs, confInfoList);
      }
      catch (DMLException e) {
        ApexLogHandler.createLogAndSave('BatchAccountTeamMemberCI','synchConfidentialInformation', e.getStackTraceString(), e);
      }
    }
  }
  /*
  private void synchAccountWithConfidentialInformation (Set<Id> accountIDs) {
    List<Confidential_Information__c> confInfoList = new List<Confidential_Information__c>();
    List<Confidential_Information__Share> confShares = new List<Confidential_Information__Share>();
    
    for (Confidential_Information__c confidential : [SELECT Id, Account__c,
                                                            (SELECT Id
                                                             FROM Shares
                                                             WHERE RowCause = 'Account_Team__c')//Constants.ROWCAUSE_ACCOUNT_TEAM)
                                                     FROM Confidential_Information__c
                                                     WHERE Account__c IN: accountIDs
                                                           AND Synch_Account_Team_Members__c = true]) {
      confInfoList.add(confidential);
      if (confidential.Shares.size() > 0) {
        confShares.addAll(confidential.Shares);
      }
    }

    if (confShares.size() > 0) {
      try {
        Database.delete(confShares, false);
        createConfidentialInfoShare (accountIDs, confInfoList);
      }
      catch (DMLException e) {
        ApexLogHandler.createLogAndSave('BatchAccountTeamMemberCI','synchAccountWithConfidentialInformation', e.getStackTraceString(), e);
      }
    }
  }
  */

  //============================================================================
  // Create Confidentials Information shares
  //============================================================================
  public void createConfidentialInfoShare(Set<ID> accountIDs, List<Confidential_Information__c> filteredConfInfos) {

    Map<String, Account> accountID_AccountMap = new Map<String, Account>();
    Map<String, AccountShare> accIdUid_accShareMap = new Map<String, AccountShare>();

    // failedConInfoIDs = new set<String>(); // PK Case #01029615 - This got reset for every batch!  
    // Getting Account share record
    for (Account account : [SELECT Id,
                                   (SELECT UserOrGroupId, RowCause, OpportunityAccessLevel, Id,
                                           ContactAccessLevel, CaseAccessLevel, AccountId,
                                           AccountAccessLevel
                                    FROM Shares),
                                   (SELECT UserId, TeamMemberRole, Id,
                                           AccountId, AccountAccessLevel, User.Name
                                    FROM AccountTeamMembers
                                    WHERE User.IsActive = true)
                            FROM Account
                            WHERE Id IN: accountIDs]) {
        accountID_AccountMap.put(account.ID, account);
        for (AccountShare accShare : account.Shares) {
          if (!accIdUid_accShareMap.containsKey(account.ID + '~~' + accShare.UserOrGroupId)) {
            accIdUid_accShareMap.put(account.ID + '~~' + accShare.UserOrGroupId, accShare);
          }
        }
      }

      // Creating the Confidential_Information__Share according to the AccountShare
      List<Confidential_Information__Share> confInfoShares = new List<Confidential_Information__Share>();
      Confidential_Information__Share newConfInfoShare;
      for (Confidential_Information__c confInfo : filteredConfInfos) {
        for (AccountTeamMember teamMember : accountID_AccountMap.get(confInfo.Account__c).AccountTeamMembers) {
          String accessLevel = 'Read'; //Constants.ACCESS_LEVEL_READ; //Default to be set.

          if (accIdUid_accShareMap.containsKey(teamMember.AccountID + '~~' + teamMember.UserId)) {
            accessLevel = accIdUid_accShareMap.get(teamMember.AccountID + '~~' + teamMember.UserId).AccountAccessLevel;
          }
          // All access level is not on the ConfInfoShare picklist so it will have edit.
          if (accessLevel == 'All') {//Constants.ACCESS_LEVEL_ALL) {
            accessLevel = 'Edit'; //Constants.ACCESS_LEVEL_EDIT;
          }

          System.debug('[ConfidentialInformationTriggerHandler:CreateShares][~~~~teamMember.UserId~~~]:'+teamMember.UserId+'--[~~~accessLevel~~~]:'+accessLevel);

          newConfInfoShare = new Confidential_Information__Share();

          newConfInfoShare.AccessLevel    = accessLevel;
          newConfInfoShare.ParentId       = confInfo.Id;
          newConfInfoShare.RowCause       = Constants.ROWCAUSE_ACCOUNT_TEAM;
          newConfInfoShare.UserOrGroupId  = teamMember.UserId;
          confInfoShares.add(newConfInfoShare);
          System.debug('\n[ConfidentialInformationTriggerHandler:CreateShares]Creating new Share: '+ newConfInfoShare);
        }
      }
      // Insert confidentials information share records
      if (!confInfoShares.isEmpty()) {
        
        List<Database.SaveResult> insertedConInfoShares = Database.insert(confInfoShares, false);
	      Set<String> succeededRecrds = new Set<String>();
	      
	      for(Integer i = 0; i < insertedConInfoShares.size(); i++){
	
	        if (insertedConInfoShares.get(i).isSuccess()){
	          succeededRecrds.add(insertedConInfoShares.get(i).getId());
	        }
	        else if (!insertedConInfoShares.get(i).isSuccess()){
	          // DML operation failed
	          Database.Error error = insertedConInfoShares.get(i).getErrors().get(0);
	          String failedDML = error.getMessage();
	          //confInfoShares.get(i);//failed record from the list
	          //system.debug('Failed ID'+confInfoShares.get(i).ParentId);
	          
	          //String failedDML = 'This is not an error, testing is going on: nojha';
	          String errorStr = 'Id: ' + confInfoShares.get(i).ParentId + '\nUserId: ' + confInfoShares.get(i).UserOrGroupId;
            errorStr += '\nError: ' + failedDML;
	          failedConInfoIDs.add(errorStr);
	        }
	      }
	      System.debug('===failedConInfoIDs.0=='+failedConInfoIDs);
      }
   }
}