/**
* @description A page controller for Search Article visualforce component
* Its main usage is to build a remote action method that returns first 5 articles with the matching
* Title with the user's input. (Optional to provice a data category, then the search will be searching 
* within the provided category name only)
*
* @author Ryan (Weijie) Hu, UC Innovation
*
* @date 02/14/2017
*/
public with sharing class ExpComm_SearchArtiAutoCompleteController {
    // Instance fields
    public String searchKeyword {get; set;}
    public String selectedArticleVersionId {get; set;}
    public String selectedKnowledgeArticleId {get; set;}

    // JS Remoting action called when searching for an article title
    @RemoteAction
    public static List<Employee_Article__kav> searchArticles (String searchKeyword, String optionalDataCategory) {

        Id currentUserId = UserInfo.getUserId();
        User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];
        String userLanguage = currentUser.LanguageLocaleKey;
        
        String queryStr = '';

        if (optionalDataCategory != null && optionalDataCategory != '') {
            queryStr = 'SELECT ArticleNumber,ArticleType,Id,IsLatestVersion,IsMasterLanguage,IsOutOfDate,KnowledgeArticleId,Language,PublishStatus,Title,UrlName'
                        + ' FROM Employee_Article__kav WHERE PublishStatus = \'Online\' AND Language = \'' + userLanguage +'\'' + ' AND Title like \'%'
                        + String.escapeSingleQuotes(searchKeyword) + '%\' WITH DATA CATEGORY Experian_Internal__c BELOW ' + optionalDataCategory + ' limit 5';
        }
        else {
            queryStr = 'SELECT ArticleNumber,ArticleType,Id,IsLatestVersion,IsMasterLanguage,IsOutOfDate,KnowledgeArticleId,Language,PublishStatus,Title,UrlName'
                        + ' FROM Employee_Article__kav WHERE PublishStatus = \'Online\' AND Language = \'' + userLanguage +'\'' + ' AND Title like \'%'
                        + String.escapeSingleQuotes(searchKeyword) + '%\' WITH DATA CATEGORY Experian_Internal__c BELOW All__c limit 5';
        }

        
        List<Employee_Article__kav> articles = Database.query(queryStr);

        return articles;
    }
}