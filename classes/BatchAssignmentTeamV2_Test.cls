/**=====================================================================
 * Experian
 * Name: BatchAssignmentTeamV2_Test
 * Description: Replacement for BatchAssignmentTeam_Test - Will soon deprecate
    Case 01033112
 * Created Date:
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Dec 3rd, 2015                Paul Kissick                 Case 01266075: Removing Global_Settings__c for timings
 * Apr 7th, 2016                Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 * Aug 5th, 2016                Paul Kissick                 CRM2:W-005393: Replacing Lead Qualifier with Sales Rep
 =====================================================================*/
@isTest
private class BatchAssignmentTeamV2_Test {
  
  private static List<User> lstUser; 
  private static Account testAcc,testAcc2,testAcc3,testAcc4,testAcc5;
  private static Assignment_Team__c assgnTeam, assgnTeam2,assgnTeam3, assgnTeam4,assgnTeam5;
  private static List<Assignment_Team_Member__c> lstAssgnTeamMem ;
  private static Assignment_Team_Member__c assgnTeamMem1,assgnTeamMem2,assgnTeamMem3,assgnTeamMem4,assgnTeamMem5,assgnTeamMem6,assgnTeamMem7,assgnTeamMem8;
  private static Account_Assignment_Team__c accAssgnTeam1,accAssgnTeam2,accAssgnTeam3;
  
  // Method to test the case when isActive is set to true and role is modified.
  private static testmethod void testBatchAssignmentTeam() {
    createTestData2();
    set<Id> assignmentTeamIDs = new set<Id>();
    set<Id> accountIds = new set<Id> ();
    for ( Account_Assignment_Team__c atm : [SELECT Id ,Account__c, Assignment_Team__c
                                            FROM Account_Assignment_Team__c
                                            WHERE Account__c = :testAcc.Id OR 
                                            Account__c = :testAcc2.Id]) {
      assignmentTeamIDs.add(atm.Assignment_Team__c);
      accountIds.add(atm.Account__c);
    }
    
    set<Id> userIDsTODel = new set<Id>();
    list<Assignment_Team_Member__c> assgnmentTeams = new List<Assignment_Team_Member__c> ();
    List<Assignment_Team_Member__c> assgnMembers = [SELECT Id, Assignment_Team_Role__c,IsActive__c,
                                                           User__c
                                                    FROM Assignment_Team_Member__c 
                                                    WHERE Id IN :assignmentTeamIDs];
    integer i = 0;
    for ( Assignment_Team_Member__c atm: assgnMembers) {
      if( atm.User__c != lstUser.get(6).Id) {
        atm.User__c = lstUser.get(6).Id;
      }
      else if( atm.Assignment_Team_Role__c != Constants.TEAM_ROLE_SALES_REP ) {
        atm.Assignment_Team_Role__c = Constants.TEAM_ROLE_SALES_REP;
      }
      if( i == assgnMembers.size() - 1){
        atm.IsActive__c = false;
        userIDsTODel.add(atm.User__c);
      }
      i++;
      assgnmentTeams.add(atm);
    }
    
    test.startTest();
    
    update assgnmentTeams;
    BatchAssignmentTeamV2 BAT1 = new BatchAssignmentTeamV2(false);
    BAT1.insertErrors = new List<String>{'Just testing a failure here'}; // PK Case 01029615 - Adding for sending failures
    BAT1.updateErrors = new List<String>{'Just testing a failure here'};
    BAT1.deleteErrors = new List<String>{'Just testing a failure here'};
    Database.executeBatch(BAT1);
    test.stopTest();
    
    map<Id,set<Id>> mapAccTeamMem = new map<Id,set<Id>>();
    
    for ( AccountTeamMember atm : [SELECT Id, UserId, AccountId, TeamMemberRole  
                                   FROM AccountTeamMember 
                                   WHERE AccountId IN : accountIds]) {
      if ( mapAccTeamMem.containsKey(atm.AccountId) ) {
        mapAccTeamMem.get(atm.AccountId).add(atm.userId);
      }
      mapAccTeamMem.put( atm.AccountId, new set<Id>{atm.userId} );
    }
    for ( Id delIds : userIDsTODel) {
      System.assert(!mapAccTeamMem.get(testAcc2.Id).contains(delIds));
    }
    
  }
  
  // Method to test the case when isActive is set to true and role is modified.
  private static testmethod void testBatchAssignmentTeam2() {
    createTestData2();
    
    List<AccountTeamMember> atm = [select id,AccountId from AccountTeamMember where UserId = : lstAssgnTeamMem[0].User__c];
    Id accId = atm[0].AccountId;
    Delete atm;
    
    test.startTest();
    
    lstAssgnTeamMem[0].IsActive__c = true;
    lstAssgnTeamMem[0].IsActive_Starting_Value__c = false;
    lstAssgnTeamMem[0].IsActive_Updated__c = true;
    lstAssgnTeamMem[1].Assignment_Team_Role__c = Constants.TEAM_ROLE_SALES_REP;
    lstAssgnTeamMem[5].User__c = lstUser.get(3).Id;
    
    update lstAssgnTeamMem;
    
    Database.executeBatch(new BatchAssignmentTeamV2(false));
    test.stopTest();
    List<AccountTeamMember> atm1 = [select Id from AccountTeamMember 
                                   where AccountId = :accId 
                                   AND userId =:lstAssgnTeamMem[0].User__c];
    system.assert(atm1.size() > 0);
  }
  
  // Method to test the case When a single record has all the three fields modified
  private static testmethod void testBatchAssignmentTeam3() {
    createTestData2();
    lstAssgnTeamMem[0].IsActive__c = true;
    lstAssgnTeamMem[0].Assignment_Team_Role__c = Constants.TEAM_ROLE_SALES_REP;
    lstAssgnTeamMem[0].User__c = lstUser.get(3).Id;
    lstAssgnTeamMem[4].IsActive__c = false;
    lstAssgnTeamMem[4].Assignment_Team_Role__c = Constants.TEAM_ROLE_SALES_REP;
    lstAssgnTeamMem[4].User__c = lstUser.get(7).Id;
    update lstAssgnTeamMem;
    
    test.startTest();
    
   // update assgnmentTeams;
    
    Database.executeBatch(new BatchAssignmentTeamV2(false));
    test.stopTest();
    
    for ( Account_Assignment_Team__c aat : [SELECT Id,Account__c,Assignment_Team__c
                                            FROM Account_Assignment_Team__c
                                            WHERE Assignment_Team__c = :lstAssgnTeamMem[0].Assignment_Team__c
                                            OR Assignment_Team__c = :lstAssgnTeamMem[4].Assignment_Team__c]) {
      
    }
    
    // Verify the results
    List<AccountTeamMember> atm = [select Id,TeamMemberRole from AccountTeamMember where AccountId = :testAcc.Id AND UserId = :lstUser.get(3).Id];
    system.assert( atm.size() > 0 );
    system.assertEquals(atm[0].TeamMemberRole,Constants.TEAM_ROLE_SALES_REP);
    
  }
  
  private static testmethod void testBatchAssignmentTeamFixer() {
    
    //create data
    createTestData3();
    
    //run the test
    Test.startTest();
    
    system.assertEquals(2,[SELECT COUNT() FROM AccountTeamMember WHERE AccountId = :testAcc.Id OR AccountId = :testAcc2.Id]);
    
    // The default for this test is 6 users added to 2 accounts, through 7 assignment rules.
    Database.executeBatch(new BatchAssignmentTeamV2(true));
    
    Test.stopTest();
    
    system.assertEquals(8,[SELECT COUNT() FROM AccountTeamMember WHERE AccountId = :testAcc.Id OR AccountId = :testAcc2.Id]);
    
  }
  
  private static testMethod void testBatchAssignmentTeamDeleted() {
    //create data
    createTestData2();
    
    system.assertEquals(5,[SELECT COUNT() FROM AccountTeamMember WHERE AccountId = :testAcc2.Id]);
    
    // IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = Userinfo.getOrganizationId(), IsDataAdmin__c = true);
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = Userinfo.getUserId(), IsDataAdmin__c = true);
    insert ida;
    
    //run the test
    Test.startTest();

    system.debug(lstUser.get(8).Id);
    delete accAssgnTeam3;
    
    Database.executeBatch(new BatchAssignmentTeamDeleted());
        
    Test.stopTest();
    
    // Should have 4 members as assgnTeamMem7 is on 2 teams.
    
    system.assertEquals(4,[SELECT COUNT() FROM AccountTeamMember WHERE AccountId = :testAcc2.Id]);
    
  }
  
  private static testMethod void testBatchAssignmentTeamDeletedCustomRun() {
    //create data
    createTestData2();
    
    system.assertEquals(5,[SELECT COUNT() FROM AccountTeamMember WHERE AccountId = :testAcc2.Id]);
    
    // IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = Userinfo.getOrganizationId(), IsDataAdmin__c = true);
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = Userinfo.getUserId(), IsDataAdmin__c = true);
    insert ida;
    
    //run the test
    Test.startTest();

    system.debug(lstUser.get(8).Id);
    delete assgnTeam3; // Deleting the entire assignment team
    
    Database.executeBatch(new BatchAssignmentTeamDeleted(system.now().addDays(-1),system.now().addDays(1)));
    
    Test.stopTest();
    
    // Should have 4 members. 3 from assteam2 and 1 from created by user
    
    system.assertEquals(4,[SELECT COUNT() FROM AccountTeamMember WHERE AccountId = :testAcc2.Id]);
    
  }
  
  private static testmethod void testBatchAssignmentTeam_New() {
    
    //create data
    createTestData2();
    
    //run the test
    test.startTest();
    List<Assignment_Team_Member__c> lstATMUpdate = new List<Assignment_Team_Member__c>();
    assgnTeamMem1.User__c = lstUser.get(7).Id;
    assgnTeamMem1.Assignment_Team_Role__c = Constants.TEAM_ROLE_SALES_REP;
    lstATMUpdate.add(assgnTeamMem1);
    assgnTeamMem5.Assignment_Team_Role__c = Constants.TEAM_ROLE_SALES_REP;
    lstATMUpdate.add(assgnTeamMem5);
    assgnTeamMem3.IsActive__c = false;
    lstATMUpdate.add(assgnTeamMem3);
    assgnTeamMem4.IsActive__c = false;
    lstATMUpdate.add(assgnTeamMem4);
    update lstATMUpdate;
    Database.executeBatch(new BatchAssignmentTeamV2(false));
    test.StopTest();
    
    //verify the result
    map<Id,List<AccountTeamMember>> accTeamMap = new map<Id,List<AccountTeamMember>>();
    for( AccountTeamMember atm : [SELECT Id,UserId,AccountId,TeamMemberRole
                                  FROM AccountTeamMember 
                                  WHERE AccountId = :testAcc.Id 
                                  OR AccountId = :testAcc2.Id]) {
      if( accTeamMap.containsKey(atm.AccountId) ) {
        accTeamMap.get(atm.AccountId).add(atm);
      }
      else {
        accTeamMap.put(atm.AccountId,new List<AccountTeamMember> {atm});
      }
    }
    
    
    // variable to verify that user is not deleted if it is related to Account
    // by some other Assignment team.
    boolean foundMemberInactiveOnlyInOneTeam = false;
    
    for ( AccountTeamMember atm : accTeamMap.get(testAcc2.Id) ) {
      if( atm.UserId == lstUser.get(5).Id ) {
        System.assertEquals(Constants.TEAM_ROLE_SALES_REP,atm.TeamMemberRole);
      }
      else if ( atm.UserId == lstUser.get(4).Id ) {
        foundMemberInactiveOnlyInOneTeam = true;
      }
    }
    
    System.assertEquals(true, foundMemberInactiveOnlyInOneTeam, 'MemberInactiveOnlyInOneTeam '+lstUser.get(4).Id);
    
    boolean foundInactiveMember = false;
    boolean foundDeletedMember = false;
    boolean foundNewMember = false;
    for ( AccountTeamMember atm : accTeamMap.get(testAcc.Id) ) {
      if( atm.UserId == lstUser.get(3).Id ) {
        foundInactiveMember = true;
      }
      else if ( atm.UserId == lstUser.get(1).Id ) {
        foundDeletedMember = true;
      }
      else if( atm.UserId == lstUser.get(7).Id ) {
        foundNewMember = true;
      }
    }
    system.assertEquals(false,foundInactiveMember,'Inactive member found '+lstUser.get(3).Id);
    system.assertEquals(false,foundDeletedMember,'Deleted member found '+lstUser.get(1).Id);
    system.assertEquals(true,foundNewMember,'New member missing '+lstUser.get(7).Id);
  }
  
  //==========================================================
  // Creates test data: Assignment_Team__c & members
  //==========================================================
  
  private static void createTestData2 (){
    // create users 
    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN];
    lstUser = Test_Utils.createUsers(p, 'test1234@experian.com', 'T-AE', 10);
    insert lstUser; 
    
    system.runAs(lstUser.get(9)) {
      testAcc = Test_Utils.createAccount();
      testAcc2 = Test_Utils.createAccount();
      List<Account> accountList = new List<Account>();
      accountList.add(testAcc);
      accountList.add(testAcc2);
      insert accountList;
    }
    
    BatchHelper.setBatchClassTimestamp('AssignmentTeamMemberJobLastRun', system.now().addSeconds(-20));
    
    Global_Settings__c settings = Global_Settings__c.getInstance(Constants.GLOBAL_SETTING);
    if( settings != null ) {
      settings.Batch_Failures_Email__c = 'test@experian.com'; // PK Case 01029615 - Adding for sending failures
      update settings;
    }
    else {
      settings = new Global_Settings__c(name=Constants.GLOBAL_SETTING,Account_Team_Member_Default_Role__c= Constants.TEAM_ROLE_ACCOUNT_MANAGER);
      settings.Batch_Failures_Email__c = 'test@experian.com'; // PK Case 01029615 - Adding for sending failures
      insert settings;
    }
    
    assgnTeam = Test_Utils.insertAssignmentTeam(false, lstUser.get(0).Id);
    assgnTeam2 = Test_Utils.insertAssignmentTeam(false, null);
    assgnTeam3 = Test_Utils.insertAssignmentTeam(false, null);
    
    List<Assignment_Team__c> assignmentTeams = new List<Assignment_Team__c>();
    assignmentTeams.add(assgnTeam);
    assignmentTeams.add(assgnTeam2);
    assignmentTeams.add(assgnTeam3);
    
    insert assignmentTeams;
    
    // create assignment team members
    lstAssgnTeamMem = new List<Assignment_Team_Member__c>();
    
    assgnTeamMem1 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam.Id, lstUser.get(1).Id, Constants.TEAM_ROLE_CHANNEL_MANAGER);   
    lstAssgnTeamMem.add(assgnTeamMem1);

    assgnTeamMem2 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam.Id, lstUser.get(2).Id, Constants.TEAM_ROLE_SALES_REP);
    lstAssgnTeamMem.add(assgnTeamMem2);

    assgnTeamMem3 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam.Id, lstUser.get(3).Id, Constants.TEAM_ROLE_SALES_REP);
    lstAssgnTeamMem.add(assgnTeamMem3);

    assgnTeamMem4 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam2.Id, lstUser.get(4).Id, Constants.TEAM_ROLE_SALES_REP);
    lstAssgnTeamMem.add(assgnTeamMem4);
    
    assgnTeamMem5 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam2.Id, lstUser.get(5).Id, Constants.TEAM_ROLE_SALES_REP);
    lstAssgnTeamMem.add(assgnTeamMem5);

    assgnTeamMem6 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam2.Id, lstUser.get(6).Id, Constants.TEAM_ROLE_SALES_REP);
    lstAssgnTeamMem.add(assgnTeamMem6);
    
    assgnTeamMem7 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam3.Id, lstUser.get(4).Id, Constants.TEAM_ROLE_SALES_REP);
    lstAssgnTeamMem.add(assgnTeamMem7);
    
    assgnTeamMem8 = Test_Utils.insertAssignmentTeamMember(false, assgnTeam3.Id, lstUser.get(8).Id, Constants.TEAM_ROLE_SALES_REP);
    lstAssgnTeamMem.add(assgnTeamMem8);
    
    insert lstAssgnTeamMem;
    
    List<Account_Assignment_Team__c> accountAssignTeamList= new List<Account_Assignment_Team__c>();
    accAssgnTeam1 = Test_utils.insertAccAssignmentTeam(false, assgnTeam.Id, testAcc.Id);
    accAssgnTeam2 = Test_utils.insertAccAssignmentTeam(false, assgnTeam2.Id, testAcc2.Id);
    accAssgnTeam3 = Test_utils.insertAccAssignmentTeam(false, assgnTeam3.Id, testAcc2.Id);
    
    accountAssignTeamList.add(accAssgnTeam1);
    accountAssignTeamList.add(accAssgnTeam2);
    accountAssignTeamList.add(accAssgnTeam3);
    
    insert accountAssignTeamList ;
    User myUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    system.runAs(myUser) {
      lstUser.get(8).IsActive = false;
      update lstUser;
    }
  }
  
  private static void createTestData3 (){
    // Set is data admin to active so no triggers fire!
    IsDataAdmin__c ida = new IsDataAdmin__c(SetupOwnerId = Userinfo.getUserId(), IsDataAdmin__c = true);
    insert ida; 
    createTestData2();
    ida.IsDataAdmin__c = false;
    update ida;
  }
  
}