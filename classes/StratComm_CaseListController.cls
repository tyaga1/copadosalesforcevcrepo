public with sharing class StratComm_CaseListController {


    @AuraEnabled
    public static List<caseWrapper> findAll(String orderBy, String order, String listview, String profileType) {
        List<caseWrapper> caseList = new List<caseWrapper>();
        String query = '';
        User curUser = [SELECT id, ContactId FROM User WHERE id =: UserInfo.getUserId()];
        String fields = '';
        String recordTypes = '';
        if(profileType == 'Strat'){
            fields = '';
            recordTypes = '';

        }else If(profileType == 'Reseller'){
            fields = '';
            recordTypes = ' AND recordType.Name IN (\'CSDA BIS Support\', \'CSDA CIS Support\')';
        }

        if(listview == 'All Open Cases')
        {
            //RJ Added an new field Business_Service__c
            query = 'SELECT id, CaseNumber, SNOW_Case_No__c, Source_System__c, Customer_Case_Number__c, Subject, Status, CreatedDate, LastModifiedDate,Business_Service__c,'
                + ' Subcode__c, Requestor__r.Name, Type, Owner.Name,ClosedDate'
                + ' FROM Case Where isClosed = false' + recordTypes
                + ' Order By ' + orderBy + ' ' + order + ' LIMIT 5000';

        }
        else if(listview == 'My Open Cases')
        {             //RJ added an new field Business_Service__c
            query = 'SELECT id, CaseNumber, SNOW_Case_No__c, Source_System__c, Customer_Case_Number__c, Subject, Status, CreatedDate, LastModifiedDate,Business_Service__c,'
                + ' Subcode__c, Requestor__r.Name, Type, Owner.Name,ClosedDate'
                + ' FROM Case Where isClosed = false AND (OwnerID = \'' + curUser.id + '\'';

            // We only want to query based on contactId if the user has a contactId
            if (curUser.contactid != null)
            {
                query += ' OR ContactId = \'' + curUser.contactid + '\'';
            }
            query += ')' + recordTypes + ' Order By ' + orderBy + ' ' + order;
        }
        else if(listview == 'All Closed Cases')
        {
            query = 'SELECT id, CaseNumber, SNOW_Case_No__c, Source_System__c, Customer_Case_Number__c, Subject, Status, CreatedDate, LastModifiedDate,Business_Service__c,'
                + ' Subcode__c, Requestor__r.Name, Type, Owner.Name,ClosedDate'
                + ' FROM Case Where CreatedDate >= LAST_N_YEARS:2 AND isClosed = true' + recordTypes
                + ' Order By ' + orderBy + ' ' + order + ' LIMIT 5000';
        }
        else if(listview == 'My Closed Cases')
        {
            query = 'SELECT id, CaseNumber, SNOW_Case_No__c, Source_System__c, Customer_Case_Number__c, Subject, Status, CreatedDate, LastModifiedDate,Business_Service__c,'
                + ' Subcode__c, Requestor__r.Name, Type, Owner.Name,ClosedDate'
                + ' FROM Case Where CreatedDate >= LAST_N_YEARS:2 AND isClosed = true AND (OwnerID = \'' + curUser.id + '\'';
            
            // We only want to query based on contactId if the user has a contactId
            if (curUser.contactid != null)
            {
                query += ' OR ContactId = \'' + curUser.contactid + '\'';
            }

            query += ')' + recordTypes + ' Order By ' + orderBy + ' ' + order;
        }

        try {
            if (query != null & query.length() > 0)
            {
                caseList = convertDates(Database.query(query));
            }
        }
        catch (exception e)
        {
            throw new AuraHandledException('Some exception happened on server side controller with the following error:\n\n'+ e.getMessage());
        }
        
        return caseList;
       
    }

    public static List<caseWrapper> convertDates(List<Case> caseList){
        TimeZone userTimezone = UserInfo.getTimeZone();
        List<caseWrapper> caseWrapperList = new List<caseWrapper>();
        for(case c:caseList){
            caseWrapper cw = new caseWrapper();
            cw.Id = c.Id;
            cw.CaseNumber = c.CaseNumber;
            
            if (c.Customer_Case_Number__c != null && c.Customer_Case_Number__c != '') {
                cw.CaseLink = c.Customer_Case_Number__c.unescapeHtml4().substringBetween('<a href=\"','\" target=');
            }
            else {
                cw.CaseLink = '';
            }

            cw.Subject = c.Subject;
            cw.Status = c.Status;
            cw.Subcode = c.Subcode__c;
            cw.EndUserName = c.Requestor__r.Name;
            cw.Type = c.Type;
            cw.AssignedTo = c.Owner.Name;
            //RJ added Business_Service__c
            cw.BusinessService  = c.Business_Service__c;
            cw.CreatedDate = c.CreatedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            cw.LastModifiedDate = c.LastModifiedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            cw.DateOnlyCreatedDate = c.CreatedDate.format('yyyy-MM-dd', userTimezone.getID());
            cw.DateOnlyLastModifiedDate = c.LastModifiedDate.format('yyyy-MM-dd', userTimezone.getID());
            
            if (c.ClosedDate != null)
            {
                cw.ClosedDate = c.ClosedDate.format('yyyy-MM-dd HH:mm:ss', userTimezone.getID());
            }
            
            caseWrapperList.add(cw);   
        }
        return caseWrapperList;
    }

    /****Wrapper Classes***/
    public class caseWrapper{
        @AuraEnabled
        public String Id {get; set;}
        @AuraEnabled
        public String CaseNumber {get; set;}
        @AuraEnabled
        public String Subject {get; set;}
        @AuraEnabled
        public String Status {get; set;}
         //RJ added Business_Service__c
        @AuraEnabled
        public String BusinessService {get; set;} 
        @AuraEnabled    
        public String CreatedDate {get; set;}
        @AuraEnabled
        public String LastModifiedDate {get; set;}
        @AuraEnabled
        public String CaseLink {get; set;}
        @AuraEnabled
        public String DateOnlyCreatedDate {get; set;}
        @AuraEnabled
        public String DateOnlyLastModifiedDate {get; set;}
        @AuraEnabled
        public String CreatedBy {get; set;}
        @AuraEnabled
        public String Subcode {get; set;}
        @AuraEnabled
        public String EndUserName {get; set;}
        @AuraEnabled
        public String Type {get; set;}
        @AuraEnabled
        public String AssignedTo {get; set;}
         @AuraEnabled
        public String ClosedDate {get; set;}

        public caseWrapper(){

        }
    }


}