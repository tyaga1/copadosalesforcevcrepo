/******************************************************************************
 * Name: ExpComm_MockHttp_ServiceNowCaseList .cls
 * Created Date: 3/14/2017
 * Created By: Hay Win, UCInnovation
 * Description : Mock class to use for WebService call out test classes,
                 TestClassforFormCallout.cls
                 ExpComm_CaseList_cmpt_test.cls
 * Change Log- 
 ****************************************************************************/
 
@isTest
public class ExpComm_MockHttp_ServiceNowCaseList implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public ExpComm_MockHttp_ServiceNowCaseList(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse resp = new HttpResponse();
        if(this.responseHeaders != null){
           for (String key : this.responseHeaders.keySet()) {
            resp.setHeader(key, this.responseHeaders.get(key));
           } 
        }
        
        resp.setBody(this.body);
        resp.setStatusCode(this.code);
        resp.setStatus(this.status);
        return resp;
    }

}