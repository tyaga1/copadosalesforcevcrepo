/**=====================================================================
 * Experian
 * Name: CaseTriggerHandler_BusinessHours_Test
 * Description: 
 * Created Date: 11 Nov 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Jan 5th,  2016               James Wills                  Case 01266714: Updated text to reflect updates to Case Record Types (see comemnts below).
 * Feb 1st, 2016                James Wills                  Case 01266714: Further changes made to reflect updates to Case Record Types.

 =====================================================================*/
@isTest
private class CaseTriggerHandler_BusinessHours_Test {
  
  
  static String testUserEmail = 'testUserEmailAddress08972345@experian.com';

  static testMethod void testBusinessHoursRecordType() {
    
    Business_Hour_Assignment__c testBha = Business_Hour_Assignment__c.getAll().get('Test BHA Record Type');
    
    List<BusinessHours> testBusinessHours = [SELECT Id, Name FROM BusinessHours WHERE IsActive = true AND IsDefault = false AND Name = :testBha.Business_Hours_Name__c LIMIT 1];
    
    // We need to test based on role, queue and/or recordtype
    List<RecordType> rts = [SELECT Id, Name FROM RecordType WHERE sObjectType = 'Case' AND Name = :testBha.Record_Type_Name__c];
    String rtName;
    Id rtId;
    if (rts.size() == 1) {
      rtName = rts[0].Name;
      rtId = rts[0].Id;
    }
    else {
      system.assert(false,'Salesforce.com Support case type not found'); //Case #01266714 James Wills; Changed from 'CRM Request case type not found'
    }
    
    Test.startTest();
    
    Case newCase = new Case(Subject = 'Test Case 1', RecordTypeId = rtId);
    insert newCase;
    
    Test.stopTest();
    
    system.assertEquals(testBusinessHours[0].Id,[SELECT BusinessHoursId FROM Case WHERE Id = :newCase.Id].BusinessHoursId,'Business Hours Not Correct');
    
  }
  
  static testMethod void testBusinessHoursRole() {
    
    Business_Hour_Assignment__c testBha = Business_Hour_Assignment__c.getAll().get('Test BHA Role');
    
    List<BusinessHours> testBusinessHours = [SELECT Id, Name FROM BusinessHours WHERE IsActive = true AND IsDefault = false AND Name = :testBha.Business_Hours_Name__c LIMIT 1];
    
    User caseOwner = [SELECT Id FROM User WHERE EMail = :testUserEmail LIMIT 1];
    
    Test.startTest();
    
    Case newCase = new Case(Subject = 'Test Case 1', OwnerId = caseOwner.Id);
    insert newCase;
    
    Test.stopTest();
    
    system.assertEquals(testBusinessHours[0].Id,[SELECT BusinessHoursId FROM Case WHERE Id = :newCase.Id].BusinessHoursId,'Business Hours Not Correct');
    
  }
  
  static testMethod void testBusinessHoursQueue() {
    
    List<BusinessHours> defaultBusinessHours = [SELECT Id, Name FROM BusinessHours WHERE IsActive = true AND IsDefault = true LIMIT 1];
    
    Business_Hour_Assignment__c testBha = Business_Hour_Assignment__c.getAll().get('Test BHA Queue Name');
    
    List<BusinessHours> testBusinessHours = [SELECT Id, Name FROM BusinessHours WHERE IsActive = true AND IsDefault = false AND Name = :testBha.Business_Hours_Name__c LIMIT 1];
    
    List<Group> tstGroup = [SELECT Id, Name FROM Group WHERE Name = :testBha.Queue_Name__c AND Type = 'Queue' AND Id IN (SELECT QueueId FROM QueueSObject WHERE SObjectType = 'Case') LIMIT 1];
    
    String queueName;
    Id queueId;
    if (tstGroup.size() == 1) {
      queueName = tstGroup[0].Name;
      queueId = tstGroup[0].Id;
    }
    else {
      system.assert(false,'Queue not found');
    }

    CaseStatus closedCaseStatus = [
      SELECT Id, MasterLabel 
      FROM CaseStatus 
      WHERE IsClosed = true
      LIMIT 1
    ];

    Test.startTest();
    
    Case newCase = new Case(Subject = 'Test Case 1', OwnerId = queueId);
    Case newCase2 = new Case(Subject = 'Test Case 2', OwnerId = UserInfo.getUserId());
    Case newCase3 = new Case(Subject = 'Test Case 3', OwnerId = UserInfo.getUserId());
    insert new List<Case>{newCase,newCase2,newCase3};
    
    newCase3.OwnerId = queueId;
    update newCase3;
    
    newCase3.Status = closedCaseStatus.MasterLabel;
    newCase3.Resolution__c = 'This is needed!';
    update newCase3;
    
    Test.stopTest();
    
    system.assertEquals(testBusinessHours[0].Id,[SELECT BusinessHoursId FROM Case WHERE Id = :newCase.Id].BusinessHoursId,'Business Hours Not Correct');
    system.assertEquals(defaultBusinessHours[0].Id,[SELECT BusinessHoursId FROM Case WHERE Id = :newCase2.Id].BusinessHoursId,'Business Hours Should be Default');
    system.assertEquals(testBusinessHours[0].Id,[SELECT BusinessHoursId FROM Case WHERE Id = :newCase3.Id].BusinessHoursId,'Business Hours Not Correct');
    
    system.assert([SELECT Time_Taken_On_Case__c FROM Case WHERE Id = :newCase3.Id].Time_Taken_On_Case__c != null,'Time taken should be completed');
    
    
  }
  
  @testSetup
  static void setupTestData() {
    Test_Utils tu = new Test_Utils();
    
    // Get a business hours that isn't the default one!
    List<BusinessHours> currentBusinessHours = [SELECT Id, Name FROM BusinessHours WHERE IsActive = true AND IsDefault = false LIMIT 1];
    if (currentBusinessHours.size() != 1) {
      system.assert(false, 'No non default hours found.');
    }
    
    // Using custom setting to make sure we can assign the correct bh.
    List<Business_Hour_Assignment__c> bhas = new List<Business_Hour_Assignment__c>();
    
    // We need to test based on role, queue and/or recordtype
    List<RecordType> rts = [SELECT Id, Name FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName != 'SFDC_Support' AND IsActive = true LIMIT 1];//Case #01266714 James Wills; Changed from 'CRM_Request'
    String rtName;
    Id rtId;
    if (rts.size() == 1) {
      rtName = rts[0].Name;
      rtId = rts[0].Id;
    }
    else {
      system.assert(false,'Salesforce.com Support case type not found');//Case #01266714 James Wills; Changed from 'CRM Request case type not found'
    }
    
    // We need to test based on role, queue and/or recordtype
    List<Group> queues = [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND Id IN (SELECT QueueId FROM QueueSObject WHERE SObjectType = 'Case') LIMIT 1];
    String queueName;
    Id queueId;
    if (queues.size() == 1) {
      queueName = queues[0].Name;
      queueId = queues[0].Id;
    }
    else {
      system.assert(false,'Queue not found');
    }
    
    // We need to test based on role, queue and/or recordtype
    // Pick a role from the hierarchy...
    List<UserRole> roles = [SELECT Id, Name FROM UserRole WHERE Name = :Constants.ROLE_SALES_EXEC_RETAIL_PROPERTY LIMIT 1];
    String roleName;
    Id roleId;
    if (roles.size() == 1) {
      roleName = roles[0].Name;
      roleId = roles[0].Id;
    }
    else {
      system.assert(false,'Role not found');
    }
    User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    system.runAs(me) {
      User caseOwnerTmp = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
      caseOwnerTmp.UserRoleId = roleId;
      caseOwnerTmp.Email = testUserEmail;
      insert caseOwnerTmp; 
    }
    
    // This will return some existing business hours
    bhas.add(new Business_Hour_Assignment__c(Name = 'Test BHA Record Type', Business_Hours_Name__c = currentBusinessHours[0].Name, Record_Type_Name__c = rtName));
    bhas.add(new Business_Hour_Assignment__c(Name = 'Test BHA Queue Name', Business_Hours_Name__c = currentBusinessHours[0].Name, Queue_Name__c = queueName));
    bhas.add(new Business_Hour_Assignment__c(Name = 'Test BHA Role', Business_Hours_Name__c = currentBusinessHours[0].Name, Role_Name__c = roleName));
    bhas.add(new Business_Hour_Assignment__c(Name = 'Test BHA Empty Details', Business_Hours_Name__c = currentBusinessHours[0].Name));
    
    insert bhas;
    
  }
  
  
}