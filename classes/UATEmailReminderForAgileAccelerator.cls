public class UATEmailReminderForAgileAccelerator {

    public static void SendEmail(Test_Case__C testCase, List<String> toAddresses, String emailOfUATOwnerOfTestCase)
    {
        if(toAddresses.size() < 1)
        {
         System.debug('No email address found. Email list : ' + toAddresses);   
         return;
        }

        string projectName = (String.IsNotEmpty(testCase.Project__r.Name) ? ' on Project: ' + testCase.Project__r.Name : '');
        string strDueDate = (testCase.Work__r.agf__Due_Date__c!= null ? String.valueOf(testCase.Work__r.agf__Due_Date__c.format()) : 'Not available');
        string strShortDescr = (String.IsNotEmpty(testCase.Short_Description__c) ? testCase.Short_Description__c : 'Not available');

        // First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        Messaging.reserveSingleEmailCapacity(2);
       
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(toAddresses);

        if(String.isNotEmpty(emailOfUATOwnerOfTestCase))
        {
            String[] ccAddresses = new String[]{emailOfUATOwnerOfTestCase};
            mail.setCcAddresses(ccAddresses);            
        }

        //mail.setReplyTo(emailOfUATOwnerOfTestCase);
        //mail.setSenderDisplayName('UAT Reminder');
        mail.setSubject('This is a reminder for Test Case : ' + testCase.Name + projectName);        
        mail.setBccSender(false);
        
        mail.setUseSignature(true);

        // Specify the text content of the email.
        mail.setPlainTextBody('Please start UAT on this Test Case <a href=\" ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + testCase.Id + '\">' + testCase.Name + '\"</a>');

        mail.setHtmlBody('<p>Please start UAT on this Test Case :<b>'+ testCase.Name + '</b> <br/>' + 
                         'Test Case Name : <b>' + testCase.Test_Case_Name__c + '</b>  <br/>' + 
                         'UAT Due Date : ' + strDueDate + ' <br/>' + 
                         'Short Description : ' + strShortDescr + ' <br/>' + 
                         'To view your test case <a href=\" ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + testCase.Id + '\">click here.</a> </p>');

        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}