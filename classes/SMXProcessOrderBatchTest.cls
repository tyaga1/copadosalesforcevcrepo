/*=====================================================================
 * Date Modified      Modified By                  Description of the update
 * Oct 29th,2014      Pallavi Sharma(Appirio)      Fix Failure
 * Sep 9th, 2015      Paul Kissick                 I-179463: Duplicate Management Failures: removed seealldata
 * Apr 7th, 2016      Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 * Apr 15th,2016      Sadar Yacob                  State and Country picklists implementation 
=====================================================================*/
@istest 
global class SMXProcessOrderBatchTest{
  
  static String testUserEmail = 'testingUser0909090909@experian.com';
  
  @testSetup
  private static void prepareTestData()  {
    User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    system.runAs(me) {
      Test_Utils.insertGlobalSettings();
    } 
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    system.runAs(me) {
      User testUser = Test_Utils.createEDQUser(p, testUserEmail, 'n1234');
      testUser.Email = testUserEmail;
      testUser.Global_Business_Line__c = 'Corporate';
      testUser.Business_Line__c = 'Corporate';
      testUser.Business_Unit__c = 'APAC:SE';
      testUser.Region__c = 'North America';
      insert testUser;
    }
    User testUser = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    system.runAs(testUser) {
        Account a = Test_Utils.createAccount();
        a.Name = 'AAA1';
        a.BillingPostalCode = '211212';
        a.BillingStreet = 'TestStreet';
        a.BillingCity = 'TestCity'; 
        a.BillingCountry='United States of America';
        insert a;
        Contact c = new Contact(
          FirstName = 'BBB1',
          LastName = 'SMX2SMX',
          AccountId = a.Id,
          Email = 'test123@experian.com'
        );    
        insert c;
        Order__c newOrder =  new Order__c (
          Name = 'Test Order 001', 
          Contract_Start_Date__c = Date.Today(),
          Locked__c = false,
          Type__c = 'New', 
          Account__c = a.Id,
          Contact__c = c.Id, 
          Close_Date__c = Date.Today()+30,
          Contract_Number__c = '1234test',
          Transactional_Sale__c = true,
          Contract_End_Date__c = system.today().addDays(180)
        );
        insert newOrder;
      }
  }
       
  static testmethod void unit_test() {
    Test.startTest();
    SMXProcessOrderBatch batch = new SMXProcessOrderBatch();
    Database.executeBatch(batch);
    Test.stopTest();
  }
  
}