/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMembersListEdit
 * Description: T-366039: Confidential Information: Account Team VF Page
 * Created Date: Jun 18th, 2015
 * Created By: Paul Kissick
 *
 * Date Modified         Modified By           Description of the update
 * Jun 18th, 2015        Paul Kissick          Case #967382: Bug found when adding confidential info sharing and need to split 
                                               this into 2 classes (one with, one without sharing). Removing edit stuff from this class.
 * Dec 12th, 2015        Sadar Yacob           Case #01084405: Add to AccountTeamMember after inserting to AccountShare to make sure Accnt Access is Edit      
 * Jan 29th, 2016        Sadar Yacob           Case #01818627: Fix issue for adding a/c team member when Logged in User is Serasa          
 * Feb 04th, 2016        Sadar Yacob           Fix for Cases getting to Read only by Default. Have to set to Private by default                               
 * Mar. 3rd, 2016        James Wills           Case #01848189 - Account Planning Enhancements
 * Jun 15th, 2016        Paul Kissick          Case #02024883: Fixing AccountShare problems (v37.0) - Extensive rewrite
  =====================================================================*/
public without sharing class AccountTeamMembersListEdit {
  
  public List<accountTeamWrapper> accountTeamMembers {get;set;}
  public List<accountTeamWrapper> newAccountTeamMembers {get;set;}
  public Account acc {get;set;}
  public Boolean showAddMemberSection {get;set;}
  public Boolean displayAccess {get;set;}
  public List<selectOption> accountOppAccessLevels {get;set;}
  public List<selectOption> caseAccessLevels {get;set;}
  public Id selectedId {get;set;}
  public Integer listSize {get;set;}
  public List<accountTeamWrapper> accountTeamMembersToShow {get;set;}
  public Integer currentSizeShown {get;set;}
  public Boolean reachedMax {get;set;}
  
  ApexPages.StandardController stdCon;
  
  public Id accId; //Case #01848189 - now declared as a class variable
  
  //==========================================================================
  // Constructor
  //==========================================================================
  public AccountTeamMembersListEdit (ApexPages.StandardController con) {
    stdCon = con;
    if (!Test.isRunningTest()) {
      stdCon.addFields(new String[]{'Name'});
    }
    showAddMemberSection = true;
    displayAccess = false;
    currentSizeShown = 5;
    reachedMax = false;
    accountTeamMembersToShow = new List<accountTeamWrapper>();
    accountOppAccessLevels = new List<SelectOption>();
    accountOppAccessLevels.add(new SelectOption(Label.AccountTeamMembersList_Read,Label.AccountTeamMembersList_Read_Only));
    accountOppAccessLevels.add(new SelectOption(Label.AccountTeamMembersList_Edit,Label.AccountTeamMembersList_Read_Write));
    caseAccessLevels = new List<SelectOption>();
    caseAccessLevels.add(new SelectOption(Label.AccountTeamMembersList_None,Label.AccountTeamMembersList_Private));
    caseAccessLevels.add(new SelectOption(Label.AccountTeamMembersList_Read,Label.AccountTeamMembersList_Read_Only));
    caseAccessLevels.add(new SelectOption(Label.AccountTeamMembersList_Edit,Label.AccountTeamMembersList_Read_Write));    
    accId = stdCon.getId();//Case #01848189 - now declared as a class variable
    acc = (Account)stdCon.getRecord();
    fetchExistingAccountTeamMembers();
    listSize = accountTeamMembers.size();
    if (listSize == 5) {
      reachedMax = true;
    }
    addTeamMembers();
  }
  
  //==========================================================================
  // Method to fetch the existing Account Team Members and their access levels
  //==========================================================================
  public void fetchExistingAccountTeamMembers () {
    accountTeamMembers = new List<accountTeamWrapper> ();
    accountTeamMembersToShow = new List<accountTeamWrapper>();
    
    Integer i = 0;
    
    Map<Id,AccountShare> accShareMap = new Map<Id,AccountShare>();
    /*
    for (AccountShare accShare : [SELECT Id, OpportunityAccessLevel, CaseAccessLevel, UserOrGroupId
                                  FROM AccountShare
                                  WHERE AccountId = :acc.Id]) {
      accShareMap.put(accShare.UserOrGroupId,accShare);
    }
    */
    for (AccountTeamMember atm : [SELECT Id, TeamMemberRole, UserId, AccountId, OpportunityAccessLevel, CaseAccessLevel,
                                         AccountAccessLevel, User.Sales_Sub_Team__c, 
                                         User.Sales_Team__c, User.Name
                                  FROM AccountTeamMember 
                                  WHERE AccountId = :acc.Id
                                  ORDER BY User.Name ASC]) {
      accountTeamWrapper atWrap = new accountTeamWrapper();
      atWrap.member = atm;
      atWrap.caseAccess = atm.CaseAccessLevel;
      atWrap.opportunityAccess = atm.OpportunityAccessLevel;
      //if (accShareMap != null && accShareMap.containsKey(atm.UserId)) {
      //  AccountShare accShare = accShareMap.get(atm.UserId);
      //}
      accountTeamMembers.add(atWrap);
      if (i < 5) {
        accountTeamMembersToShow.add(atWrap);
        i++;
      }
    }
  }
  
  //==========================================================================
  // Method to create new members list and display the add team members section
  //==========================================================================
  public void addTeamMembers () {
    newAccountTeamMembers = new List<accountTeamWrapper>();
    for (Integer i = 0; i < 5; i++) {
      accountTeamWrapper atm = new accountTeamWrapper();
      atm.member.AccountId = acc.Id;
      newAccountTeamMembers.add(atm);
    }
    showAddMemberSection = true;
  }
  
  //==========================================================================
  // Method to save the newly created team members
  //==========================================================================
  public pagereference saveNewTeamMembers () {
    List<AccountTeamMember> atmToInsert = new List<AccountTeamMember>();
    List<AccountShare> accshareToInsert = new List<AccountShare>();
    List<Confidential_Information__Share> confInfoSharesToInsert = new List<Confidential_Information__Share>();
    try {
      for (accountTeamWrapper atmWrap : newAccountTeamMembers) {
        if (atmWrap.member.userId != null) {
          AccountTeamMember atm = atmWrap.member;
          
          system.debug('---atmWrap.member.AccountAccessLevel---'+atmWrap.accountAccess);
          system.debug('---atmWrap.member.OpportunityAccessLevel ---'+atmWrap.opportunityAccess);
          system.debug('---atmWrap.member.CaseAccessLevel ---'+atmWrap.caseAccess);
          system.debug('---atmWram.member.AccessLevelLabel--:'+ Label.AccountTeamMembersList_Edit);
          
          //AccountShare accshare = new AccountShare();
          //accshare.AccountId = atmWrap.member.AccountId;
          //accshare.UserOrGroupId = atmWrap.member.userId;
          atm.AccountAccessLevel = (atmWrap.accountAccess == Label.AccountTeamMembersList_Edit) ? Constants.ACCESS_LEVEL_EDIT : Constants.ACCESS_LEVEL_READ; //atmWrap.accountAccess; For Non English users, this would be set to specific lang value
          atm.OpportunityAccessLevel = (atmWrap.opportunityAccess == Label.AccountTeamMembersList_Edit) ? Constants.ACCESS_LEVEL_EDIT : Constants.ACCESS_LEVEL_READ; //atmWrap.opportunityAccess;
          
          if (atmWrap.caseAccess == Label.AccountTeamMembersList_Edit) {
            atm.CaseAccessLevel = Constants.ACCESS_LEVEL_EDIT; //atmWrap.caseAccess;
          }
          else if (atmWrap.caseAccess == Label.AccountTeamMembersList_Read_Only) {
            atm.CaseAccessLevel = Constants.ACCESS_LEVEL_READ;
          }
          //Goes to Private by default so only handling Read/Write and Read Only explicitly

          // accshareToInsert.add(accshare);
          // system.debug(accshare);
          atmToInsert.add(atmWrap.member);
        }
      }
      //T-368099 - create Confidential_Information__Share when ATM created
      List<Confidential_Information__c> confInfoList = getConInfoRecords(acc.Id);
      Confidential_Information__Share newConfInfoShare;
      
      for (Confidential_Information__c confInfo : confInfoList) {
        for (accountTeamWrapper atmWrap : newAccountTeamMembers) {
          if (atmWrap.member.userId != null) {
            String accessLevel = Constants.ACCESS_LEVEL_READ; //Default to be set.
            // All access level is not on the ConfInfoShare picklist so it will have edit.
            if (accessLevel == Constants.ACCESS_LEVEL_ALL) {
              accessLevel = Constants.ACCESS_LEVEL_EDIT;
            }
            newConfInfoShare = new Confidential_Information__Share(
              AccessLevel    = accessLevel,
              ParentId       = confInfo.Id,
              RowCause       = Constants.ROWCAUSE_ACCOUNT_TEAM,
              UserOrGroupId  = atmWrap.member.userId
            );
            confInfoSharesToInsert.add(newConfInfoShare);
          }
        }
      }
      // Case 01084405: Insert shares first
      /*
      if (!accshareToInsert.isEmpty()) {
        system.debug(accshareToInsert);
        insert accshareToInsert;
      }
      */
      if (!atmToInsert.isEmpty()) {
        system.debug(atmToInsert);
        insert atmToInsert;
        updateAccountPlanTeams(atmToInsert);//case #01848189 - Account Planning Enhancements
      }
      
      if (!confInfoSharesToInsert.isEmpty()) {
        insert confInfoSharesToInsert;
      }
      return stdCon.view();
    }
    catch (Exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, ex.getMessage()));
      apexLogHandler.createLogAndSave('AccountTeamMembersList','saveNewTeamMembers', ex.getStackTraceString(), ex);
      return null;
    }
  }
  
  //==========================================================================
  // Method to save team members and show the blank list again for the user to enter more records
  //==========================================================================
  public void saveAndMore() {
    saveNewTeamMembers();
    addTeamMembers(); 
  }
  
  //==========================================================================
  // Method to cancel and return back to Account page
  //==========================================================================
  public pagereference doCancel () {
    newAccountTeamMembers = null;
    return stdCon.view();
  }

  //==========================================================================
  //Method to get Confidential_Information__c records
  //==========================================================================
  public List<Confidential_Information__c> getConInfoRecords (Id accID) {
    List<Confidential_Information__c> lstConInfo = [
      SELECT Id, Account__c,
        (SELECT Id
         FROM Shares
         WHERE RowCause = :Constants.ROWCAUSE_ACCOUNT_TEAM)
      FROM Confidential_Information__c
      WHERE Account__c = :accId
      AND Synch_Account_Team_Members__c = true
    ];
    return lstConInfo;
  }
  
  //case #01848189 - Account Planning Enhancements
  public void updateAccountPlanTeams(List<AccountTeamMember> atmToInsert) {
    List<Account_Plan_Team__c> accountPlanTeamNewMembers = new List<Account_Plan_Team__c>();
    
    for (Account_Plan__c accPlan : [SELECT Id FROM Account_Plan__c WHERE Account__c = :accId]) {   
      for (AccountTeamMember atMember : atmToInsert) {
        Account_Plan_Team__c newAPT = new Account_Plan_Team__c(
          Account_Plan__c = accPlan.id,
          Account_Team_Role__c = atMember.TeamMemberRole,     
          User__c = atMember.UserId
        );
        accountPlanTeamNewMembers.add(newAPT);
      }
    }
    
    if (!accountPlanTeamNewMembers.isEmpty()) {
      try {
        insert accountPlanTeamNewMembers;
      }
      catch (DMLException e) {
        system.debug('Problem adding Account Plan Team Member.' );
        system.debug('\n AccountTeamMembersListEdit - updateAccountPlanTeams: '+ e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
      }
    }
  }
  //case #01848189 - Account Planning Enhancements
  
  //==========================================================================
  // Wrapper class to hold the Team Member record as well as their access levels
  //==========================================================================
  public class accountTeamWrapper {
    public AccountTeamMember member {get;set;}
    public String accountAccess {get;set;}
    public String opportunityAccess {get;set;}
    public String caseAccess {get;set;}
    public accountTeamWrapper() {
      member = new AccountTeamMember();
      opportunityAccess = '';
      caseAccess = '';
    }
  }
  
}