/**=====================================================================
 * Experian, Inc
 * Name:CaseActivityTriggerHandler_Test
 * Description: W-007318  Test class for CaseActivityTriggerHandler
 * Created Date: Apr 27th, 2017
 * Created By: Manoj Gopu
 * 
 * Date Modified                Modified By                  Description of the update
   =====================================================================*/
@isTest
private class CaseActivityTriggerHandler_Test {
  
  static testmethod void testMethod1(){
    //create 1 account
      Global_Settings__c custSetting = Test_Utils.insertGlobalSettings();
      custSetting.Case_Access_Request_TeamRole__c = Constants.CASE_TEAM_ROLE_REQUESTOR;
      custSetting.Account_Team_Member_Default_Role__c = 'Manager';
      update custSetting;
            
      Account acc1 = new Account();
      acc1.Name = 'Test Account Test';
      acc1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer Account').getRecordTypeId();
      insert acc1;
      //create 3 addresses
      Address__c address1 = Test_Utils.insertAddress(false);
      address1.Address_id__c = 'testExternalId1';
      Address__c address2 = Test_Utils.insertAddress(false);
      address2.Address_id__c = 'testExternalId2';
      List<Address__c> listAddresses = new List<Address__c>();
      listAddresses.add(address1);
      listAddresses.add(address2);
      insert listAddresses;
      //create 1 account address
      Account_Address__c accAddress1 = Test_Utils.insertAccountAddress(true, address1.Id, acc1.Id);
      //create contact
      Contact contact1 = Test_Utils.insertContact(acc1.Id);
      // get the closed statuses of case object
      List<String> closedStatuses = new List<String>();
      for(CaseStatus cStatus : CaseTriggerHandler.getClosedCaseStatuses()){
        closedStatuses.add(cStatus.MasterLabel);
      }
      ESDEL_Delivery_Project__c deliveryProject = Test_Utils.insertDeliveryProject(true, 'project1', acc1.id, contact1.id, 'bord1');
      // insert new case of type spain delivery task
      Serasa_Assignment__c objAss = new Serasa_Assignment__c();
      objAss.Island__c = 'Test';
      objAss.Case_Reason_Serasa__c = 'Test';
      objAss.Secondary_Case_Reason_Serasa__c = 'Test';
      objAss.Tertiary_Case_Reason_Serasa__c = 'Test';
      objAss.Activity_owner__c = 'Test';
      objAss.SLA__c = 240;
      objAss.Activity_Island__c = 'Test';
      objAss.Type__c = 'Test';
      objAss.Sub_Type__c = 'Test';
      objAss.Activity_SLA__c = 24;
      insert objAss;    
      
      Serasa_Assignment__c objAss1 = new Serasa_Assignment__c();      
      objAss1.Activity_Island__c = 'Test1';
      objAss1.Type__c = 'Test1';
      objAss1.Sub_Type__c = 'Test1';
      objAss1.Activity_SLA__c = 24;
      insert objAss1; 
        
      User logginUser = [select id,Current_Sales_Team__c from User where id=:Userinfo.getUserId()];
      
      BusinessHours bh = [select id from businesshours where IsDefault=true];
      Custom_Fields_Ids__c objCust = new Custom_Fields_Ids__c();
      objCust.Serasa_business_hours_id__c = bh.Id;
      insert objCust;
      
      
      
      Case newCase = Test_Utils.insertCase (false, deliveryProject.id, acc1.id, 'bord1');
      newCase.Type = Constants.CASE_TYPE_SPAIN_DELIVERY_TASK;
      String serasaRecordType = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_CASE, Constants.RECORDTYPE_Serasa_Support_Request);
      newCase.RecordTypeId = serasaRecordType;
      newCase.User_Island__c = logginUser.Current_Sales_Team__c;
      newCase.Island__c = 'Test';
      newCase.Serasa_Case_Reason__c = 'Test';
      newCase.Serasa_Secondary_Case_Reason__c = 'Test';
      newCase.Serasa_Case_Third_Reason__c = 'Test';
      newCase.Description = 'Test Desc';
      newCase.Subject = 'Test Sub';
       newCase.Contact_Not_Identified__c = True;
      
      insert newCase;     
      
      
      Test.startTest();
      CaseTriggerHandler.isCaseCustomActivity = false;
      Case_Activity__c caseCustomActivity = new Case_Activity__c();
      caseCustomActivity.Island__c = 'Test';
      caseCustomActivity.Type__c = 'Test';
      caseCustomActivity.Sub_Type__c = 'Test';      
      caseCustomActivity.Case__c = newCase.Id;
    
      insert caseCustomActivity;
      update caseCustomActivity;
      
      Case_Activity__c caseManualActivity = new Case_Activity__c();
      caseManualActivity.Island__c = 'Test1';
      caseManualActivity.Type__c = 'Test1';
      caseManualActivity.Sub_Type__c = 'Test1';    
      caseManualActivity.Case__c = newCase.Id;   
          
      insert caseManualActivity;
      Test.stopTest();
  }
}