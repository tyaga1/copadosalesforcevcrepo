/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityRoyaltiesController
 * Description: Controller class for OpportunityRoyalties vf page (for T-313541)
 * Created Date: August 28th, 2014
 * Created By: Nathalie LE GUAY (Appirio) 
 * 
 * Date Modified      Modified By                Description of the update 
 * 1/14/2016           Sadar Yacob               Added addnl filter on Royalty lines : OptyId in addition to the Line Item Reference Number
 * 1/28/2016           Sadar Yacob               Case #01150540 : Check if Opty is for APAC and set access levels accordingly
 * 2/17/2016           Sadar Yacob               Modified to use Custom Label for AllowedUserRegion (Currently only APAC), 
                                                 so it can scale when this is opened up to other regions
*  2/19/2016           Sadar Yacob               Added CRM Product Name, Product2.Name and Product Family so it can be defaulted on Targeting Royalty record                                       
 =====================================================================*/
public with sharing class OpportunityLineItemRoyaltiesController {

  public String oliId {get;set;}
  public String optyID {get;set;}
  public OpportunityLineItem oli {get;set;}
  public List<Royalty__c> royalties {get;set;}
  public Id selectedId {get;set;}
  public Boolean hasAccess {get;set;}
  public Boolean hasEditAccess {get;set;}
  public Boolean hasDelAccess {get;set;}
  
  public OpportunityLineItemRoyaltiesController(ApexPages.StandardController controller)  
  {
      oliId = controller.getId();
      System.Debug('DEBUG,OliID:' + oliId);
      hasAccess=false; 
      hasEditAccess = false;
      hasDelAccess = false;
    
      
      oli = [SELECT Id, Name, Item_Number__c,OpportunityId,Opportunity.Name,CRM_Product_Name__c,Product_Family__c,PriceBookEntry.Product2.Name
             FROM OpportunityLineItem
             WHERE Id = :oliId];

       OptyId=oli.OpportunityId; //apexpages.currentpage().getparameters().get('Opportunity__c');
       checkAccess();    
      
      
     /* if (String.isEmpty(oli.Item_Number__c)) {
        royalties = new List<Royalty__c>();
      }
      else { */
        royalties = [SELECT Id, Name, Line_Item_Reference_Number__c, Product_Name__c,Product_Family__c,
                          Quantity_Type__c, Amount__c, Renewal_Royalty_Amount__c, Provider_Name__c,Opportunity__c
                   FROM Royalty__c
                   WHERE //Opportunity__c = :oli.OpportunityId
                   Line_Item_Reference_Number__c != null
                   AND ( Line_Item_Reference_Number__c = :oli.Item_Number__c  OR Line_Item_Reference_Number__c =:oli.Id)
                   ORDER BY Name];
                   
      //}
      
  }   
  
   
      public List<Royalty__c> getRoyalties()
      {
       
         oli = [SELECT Id, Name, Item_Number__c,OpportunityId
         FROM OpportunityLineItem
         WHERE Id = :oliId];
         
           royalties = [SELECT Id, Name, Line_Item_Reference_Number__c, Product_Name__c,Product_Family__c,
                          Quantity_Type__c, Amount__c, Renewal_Royalty_Amount__c, Provider_Name__c,Opportunity__c
                   FROM Royalty__c
                   WHERE //Opportunity__c = :oli.OpportunityId
                   Line_Item_Reference_Number__c != null
                   AND ( Line_Item_Reference_Number__c = :oli.Item_Number__c  OR Line_Item_Reference_Number__c =:oli.Id)
                   ORDER BY Name];
                   
          return royalties;
      }
  
  public PageReference doDelete()
  {
      try{
        if(selectedId !=null)
        {
          integer indexToDel = -1;
          for(Royalty__c inc :royalties )
          {
              indexToDel++;
              if (inc.id == selectedId)
              { break;}
          }
          if (indextoDel !=-1)
          { royalties.remove(indextoDel ); }
          
          Royalty__c targetRoyaltyToDel = [SELECT Id,Name  FROM Royalty__c WHERE Id = :selectedId];
          delete targetRoyaltyToDel; 
        }
      
      }  
      catch ( exception ex) {
      apexLogHandler.createLogAndSave('OpportunityLineItemRoyaltiesController ','doDelete', ex.getStackTraceString(), ex);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
    }
    return null;  
  }
 
//Populating hasAccess,hasEditAccess and hasDelAccess to check access for current User. 
 public void checkAccess()
 {
      String currentUserId = userinfo.getUserID();

      for (UserRecordAccess userAccess  : [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :currentUserId AND RecordId = :oliId ]) {
            if(userAccess.HasEditAccess == true)
            {
              hasAccess = true; 
            }
       }            
       
       System.Debug('HasAccess (After checking UserRecordAccess) :'+hasAccess );

       
    Boolean hasRoyaltyEditAccess =false;
    Boolean hasRoyaltyDeleteAccess =false;
    
    List <ObjectPermissions> objPermEdit = [SELECT Id, SObjectType, PermissionsRead, PermissionsCreate,PermissionsEdit,PermissionsDelete, Parent.label, Parent.IsOwnedByProfile
                                     FROM ObjectPermissions WHERE (ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment
                                                                                WHERE Assignee.Name = :userinfo.getName()))
                                      AND PermissionsEdit= true  AND SobjectType = 'Royalty__c' AND Parent.Name like'Target%' limit 1]; 
    
    if( objPermEdit.size() > 0 ) 
    {                                  
        hasRoyaltyEditAccess  =  true;
    }                                   
    

    //hasRoyaltyDeleteAccess  =
    List <ObjectPermissions> objPermDel = [SELECT Id, SObjectType, PermissionsRead, PermissionsCreate,PermissionsEdit,PermissionsDelete, Parent.label, Parent.IsOwnedByProfile
                                     FROM ObjectPermissions WHERE (ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment
                                                                                WHERE Assignee.Name = :userinfo.getName()))
                                      AND PermissionsDelete= true AND SobjectType = 'Royalty__c' AND Parent.Name like'Target%' limit 1]; 


  if( objPermDel.size() > 0 ) 
    {                                  
        hasRoyaltyDeleteAccess  =  true;
    }  

    System.Debug('Royalty Permission Edit:'+hasRoyaltyEditAccess );
    System.Debug('Royalty Permission Del:'+hasRoyaltyDeleteAccess  );

    String userRegion = [SELECT Region__c FROM User where Id=:currentUserId LIMIT 1].Region__c;
    
    if( userRegion != null ) 
    {

        String allowedUserRegion = Label.TargetingRoyaltyUserRegion;
        System.Debug('Allowed User Region for Targeting Royalties :' + allowedUserRegion);
        
        //if ( hasRoyaltyEditAccess == true && hasAccess == true && userRegion =='APAC')
        if ( hasRoyaltyEditAccess == true && hasAccess == true && alloweduserRegion.indexOf(userRegion ) != -1 )
        { hasEditAccess= true;}
        else
        { hasEditAccess =false; }
    
    
        if ( hasRoyaltyDeleteAccess  == true && hasAccess == true &&  alloweduserRegion.indexOf(userRegion ) != -1 ) //userRegion =='APAC')
        { hasDelAccess= true;}
        else
        { hasDelAccess = false; }
        
        if (hasAccess==true &&  alloweduserRegion.indexOf(userRegion ) == -1 )  //userRegion !='APAC')
        {
            hasAccess=false;
        }
     }        
     else //user region not defined so give default Access 
     {
       hasAccess = false;
       hasEditAccess =false;
       hasDelAccess = false;
     }
     
    System.Debug('Has Edit Access:(FINAL)' + hasEditAccess);
    System.Debug('Has Del Access:(FINAL)' + hasDelAccess);
    System.Debug('Has Access:(FINAL)' + hasAccess);

 }
  
 
}