/*=====================================================================
 * Date Modified      Modified By                  Description of the update
 * Oct 29th,2014      Pallavi Sharma(Appirio)      Fix Failure
 * Sep 9th, 2015      Paul Kissick                 I-179463: Duplicate Management Failures: removed seealldata
 * Apr 7th, 2016      Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 * Apr 15th,2016      Sadar Yacob                  State and Country picklists implementation 
=====================================================================*/
@isTest
global class SMXProcessOpportunityBatchTest{
  
  static String testUserEmail = 'testingUser0909090909@experian.com';
  
  @testSetup
  private static void prepareTestData()  {
    User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    system.runAs(me) {
      Test_Utils.insertGlobalSettings();
    } 
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    system.runAs(me) {
        User testUser = Test_Utils.createEDQUser(p, testUserEmail, 'n1234');
        testUser.Email = testUserEmail;
        testUser.Global_Business_Line__c = 'Corporate';
        testUser.Business_Line__c = 'Corporate';
        testUser.Business_Unit__c = 'APAC:SE';
        testUser.Region__c = 'North America';
        insert testUser;
    }
    User testUser = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    system.runAs(testUser) {
        Account a = Test_Utils.createAccount();
        a.Name = 'AAA2';
        a.BillingPostalCode = '211212';
        a.BillingStreet = 'TestStreet';
        a.BillingCity = 'TestCity';
        a.BillingCountry = 'United States of America';
        insert a;
        
        Address__c addr = Test_Utils.insertAddress(true);
        Account_Address__c accAddr = Test_Utils.insertAccountAddress(true, addr.Id, a.Id);
        
        Contact c = new Contact(
          FirstName = 'BBB2',
          LastName = 'SMX2',
          AccountId = a.Id,
          Email = 'test1234@experian.com'
        );    
        insert c; 
        
        Opportunity newOpp = Test_utils.createOpportunity(a.Id); 
        newOpp.CloseDate = System.today().addDays(90);
        newOpp.Starting_Stage__c = Constants.OPPTY_STAGE_6;
        insert newOpp;
        
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = newOpp.Id, ContactId = c.Id, IsPrimary = TRUE);
        insert ocr;
    }

  }
  
  static testmethod void SmxOpportunityScheduler(){
    Test.startTest();
    SMXProcessOpportunityBatch batch = new  SMXProcessOpportunityBatch();
    Database.executeBatch(batch);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM Feedback__c]);
  }
}