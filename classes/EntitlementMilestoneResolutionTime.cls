/*=============================================================================
 * Experian
 * Name: EntitlementMilestoneResolutionTime
 * Description: Returns the Resolution Time for the Entitlement Milestone
 * Created Date: 25 Apr 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

global class EntitlementMilestoneResolutionTime implements Support.MilestoneTriggerTimeCalculator {
  
  static Map<Id, Case> caseMap = new Map<Id, Case> ();
  static Map<Id, MilestoneType> milestoneTypeMap = new Map<Id, MilestoneType> ();
  
  public static Set<Id> caseMilestoneExecuted = new Set<Id> ();
  
  public static String RESOLUTION_TIME = 'Resolution Time';
  
  global static Integer defaultTime = 480; // Hard coding 8 hour resolution default, if not on the entitlement record.
  
  // Returns the milestone timings based on supplied milestone type id.
  global Integer calculateMilestoneTriggerTime (String caseId, String milestoneTypeId) {
    caseMilestoneExecuted.add(caseId);
    if (!caseMap.containsKey(caseId)) {
      caseMap.putAll([
        SELECT Priority, EntitlementId, Entitlement.Case_Resolution_Time__c 
        FROM Case 
        WHERE Id = :caseId
      ]);
    }
    if (!milestoneTypeMap.containsKey(milestoneTypeId)) {
       milestoneTypeMap.putAll([
        SELECT Name 
        FROM MilestoneType 
        WHERE Id = :milestoneTypeId
      ]);
    }
    
    if (caseMap.containsKey(caseId) && milestoneTypeMap.containsKey(milestoneTypeId)) {
      Case c = caseMap.get(caseId);
      MilestoneType mt = milestoneTypeMap.get(milestoneTypeId);
      if (c.EntitlementId != null) {
        if (mt.Name == RESOLUTION_TIME) {
          if (c.Entitlement.Case_Resolution_Time__c != null) {
            return Integer.valueOf(c.Entitlement.Case_Resolution_Time__c);
          }
        }
      }
    }
    return defaultTime;
  }
  
}