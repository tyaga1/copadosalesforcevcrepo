/*=============================================================================
 * Experian
 * Name: CaseVisualforceController_Test
 * Description: 
 * Created Date: 01/26/2017
 * Created By: Diego Olarte
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 * 
 =============================================================================*/

@isTest
public class CaseVisualforceController_Test {
  
  @isTest
  public static void testCaseCustomEditPage(){
    
    Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(label.CSDA_Online_Price_Change_Request).getRecordTypeId();
     
    PageReference pageRef = Page.CISPriceChangeform;
    Test.setCurrentPage(pageRef);
    
    //Create test data
    
    Account acc = Test_Utils.insertAccount();
    
    Sub_Code__c tSubCode;
    tSubCode = new Sub_Code__c(
          Subscriber_Code__c = 'ABC123',
          Account__c = acc.id);
    
    insert tSubCode;
    
    Case tCase;
    tCase = new Case(
          AccountId= acc.id,
          RecordTypeId= devRecordTypeId,
          Origin = 'Email',
          Status='New',
          Support_Team__c='BIS Reseller',
          Subject='Sales Price Change Request');
          
    insert tCase;
        
    //start test
    Test.startTest();
    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(tCase);
    CaseVisualforceController controller = new CaseVisualforceController(sc);
    Case objcase = controller.GetCase();
    
    Account_SubCode__c tAccSC;
    tAccSC = new Account_SubCode__c(Case__c = tCase.Id, Case_Account__c = acc.id, Sub_Code_Lookup__c = tSubCode.id);
    insert tAccSC;
    
    Price_Change_Detail__c tPriceCD;
    tPriceCD = new Price_Change_Detail__c(Case__c = tCase.Id, Product__c = 'Test Product', Hits_No_Hits__c = 'Hits Only', New_Price__c = 1);
    insert tPriceCD;
    
    controller.saveAndExit();
    Controller.save();
    //stop test
    Test.stopTest();
    
  }
  
}