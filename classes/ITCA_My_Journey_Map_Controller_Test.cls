/**=====================================================================
 * Experian
 * Name: ITCA_My_Journey_Map_Controller_Test
 * Description: Test Class for ITCA_My_Journey_Map_Controller
 *
 * Created Date: Aug. 30th 2017
 * Created By: James Wills for ITCA Project
 * Date Modified      Modified By           Description of the update
 * 4th Sept. 2017     James Wills           Additions to code.
 * 18th Sept. 2017    James Wills           Removed reference to redacted VF page.
=====================================================================*/
@isTest
private class ITCA_My_Journey_Map_Controller_Test{

 private static testMethod void test_ITCA_My_Journey_Map_Controller_1(){
     
   
   ITCA_My_Journey_Map_Controller jmc1 = new ITCA_My_Journey_Map_Controller();   
   jmc1.currentActiveTab='isSummary';
    
   jmc1.bannerInfoLocal = new ITCABannerInfoClass();
    
   jmc1.bannerInfoLocal.currentActiveTab  = 'isSummary';  
    
   List<Career_Architecture_User_Profile__c> caup = [select id from Career_Architecture_User_Profile__c LIMIT 1];
   jmc1.bannerInfoLocal.currentUserProfile = caup;
   
   PageReference pageRef = Page.ITCA_Home_Page;        
   Test.setCurrentPage(pageRef);  
   
   system.currentPageReference().getParameters().put('selectedCareerProfileID', caup[0].id);
            

           
   Test.startTest();
    
    Career_Architecture_Skills_Plan__c careerArchSkillPlan = [SELECT id, Skill__c, Skill__r.Name, Skill__r.sfia_Skill__c, Skill__r.Max_Skill_Level__c, Level_in_Number__c 
                                                              FROM Career_Architecture_Skills_Plan__c LIMIT 1];    
    Map<String, Decimal> maxSkillLevels = new Map<String,Decimal>();
    maxSkillLevels.put(careerArchSkillPlan.Skill__c, careerArchSkillPlan.skill__r.Max_Skill_Level__c);
    
    jmc1.selectedESDS = new ITCABannerInfoClass.employeeSkillDisplay(careerArchSkillPlan, maxSkillLevels);
    
    jmc1.selectedSkillId = careerArchSkillPlan.Skill__c;    
    List<ITCA_My_Journey_Map_Controller.userSkillInfo> sol = jmc1.skillOverview_List;    
    jmc1.selectedSkillId = [SELECT id FROM ProfileSkill Where Name != 'Apex' LIMIT 1].id;  
    List<ITCA_My_Journey_Map_Controller.userSkillInfo> sol2 = jmc1.skillOverview_List;
    jmc1.selectedSkillId = [SELECT id FROM Experian_Skill_Set__c LIMIT 1].id; 
    List<ITCA_My_Journey_Map_Controller.userSkillInfo> sol3 = jmc1.skillOverview_List;
    
    Map<ID,ProfileSkill> psMaxMap = jmc1.skillMaxLevels_Map;
    
    String upcl = jmc1.userProfileCurrentLevel;
    
    String enp = jmc1.employeeNameForProfile;
    
    Integer sizeOfCasp_List = jmc1.casp_List.size();
    
    jmc1.level1_SelectedCareerArea='Select Career Area';    
    Set<String> l1 = jmc1.level1_SelectedCareerAreas_Set;
    jmc1.level2_SelectedSkillSet='Select Skill Set';
    Set<String> l2 = jmc1.level2_selectedSkillSets_Set;   
     
      
   Test.stopTest();
  
  }
  

  @testSetup
  private static void createData(){
    
    buildProfileSkills();
    buildExperianSkillSet();
    buildSkillSetToSKill();
    createProfile();
  }

  private static void buildProfileSkills(){
  
    ProfileSkill ps1 = new ProfileSkill(Name='Apex', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=false);
                                        
    ProfileSkill ps2 = new ProfileSkill(Name='Programming & Software Development', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=true);                                  

    List<ProfileSkill> ps_List = new List<ProfileSkill>{ps1,ps2};
    insert ps_List;
    
  }

  private static void buildExperianSkillSet(){
    Experian_Skill_Set__c exp1 = new Experian_Skill_Set__c(Name='Software Development', Career_Area__c='Applications');
    insert exp1;
  
  }

  private static void buildSkillSetToSKill(){
    List<ProfileSkill> psList = [SELECT id, Name FROM ProfileSkill];
    List<Experian_Skill_Set__c> essList = [SELECT id FROM Experian_Skill_Set__c];
   
    Skill_Set_to_Skill__c sts1 = new Skill_Set_to_Skill__c(Name='Software Development - Application Support', 
                                                          Skill__c=psList[0].id, 
                                                          Experian_Role__c='Professional',
                                                          Experian_Skill_Set__c=essList[0].id,
                                                          Level__c='Level4');
    insert sts1;
  
 }

  private static void createProfile(){  
    
    Career_Architecture_User_Profile__c caup1 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= userInfo.getUserId(),
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup1;
     
    Career_Architecture_Skills_Plan__c casp1 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level6',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill Where Name = 'Apex' LIMIT 1].id  );    
    insert casp1;
  }


}