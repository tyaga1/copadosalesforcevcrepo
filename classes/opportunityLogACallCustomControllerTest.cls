/*=====================================================================
 * Experian
 * Name: opportunityLogACallCustomControllerTest
 * Description: Case#02004426  Provide test coverage to the class "opportunityLogACallCustomController"
 *
 * Created Date: June 30th, 2016
 * Created By: Manoj (Experian)
 *
 * Date Modified            Modified By                 Description of the update
   24th Feb 2017            Manoj Gopu                  Updated the test class to Support the new changes  
   06th June2017            Manoj Gopu                  Inserted custom Setting to fix test class Failure
 ======================================================================*/
@isTest
private class opportunityLogACallCustomControllerTest{
    static testMethod void myUnitTest() {
    
        Test.startTest();
            Global_Settings__c objGlob=new Global_Settings__c();
            objGlob.Name='Global';
            objGlob.Opp_Stage_4_Name__c='Propose';
            objGlob.Opp_Stage_3_Name__c='Qualify';
            insert objGlob;
            
            Opportunity opty=new Opportunity();
            opty.Name='Test Opportunity';
            opty.StageName='Qualify';
            opty.CloseDate=system.today().addDays(30);
            insert opty;
            
            Contact objC=new Contact();
            objC.LastName='Test';
            insert objC;
            
            OpportunityContactRole objPC=new OpportunityContactRole();
            objPC.OpportunityId=opty.Id;
            objPC.ContactId=objC.Id;
            objPC.IsPrimary=true;
            insert objPC;
            ApexPages.currentPage().getParameters().put('opyId',opty.Id);
            opportunityLogACallCustomController obj=new opportunityLogACallCustomController();
            obj.redirect();
            
            objPC.IsPrimary=false;
            update objPC;
            obj.redirect();
            
        Test.stopTest();
    }
    static testMethod void myUnitTest1() {
    
        Test.startTest();
            User testExperianUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
            Experian_Global__c expGlobal = new Experian_Global__c();
            expGlobal.OwnerId__c = testExperianUser.ID;
            insert expGlobal ;
            
            Contact objC=new Contact();
            objC.LastName='Test';
            insert objC;           
            
            
            ApexPages.currentPage().getParameters().put('conId',objC.Id);
            opportunityLogACallCustomController obj1=new opportunityLogACallCustomController();
            obj1.redirect();         
            
            
        Test.stopTest();
    }
    static testMethod void myUnitTest2() {
    
        Test.startTest();
        Account acc = Test_Utils.insertAccount();            
            
            ApexPages.currentPage().getParameters().put('accId',acc.Id);
            opportunityLogACallCustomController obj2=new opportunityLogACallCustomController();
            obj2.redirect();
            
        Test.stopTest();
    }
    
}