/**=====================================================================
 * Experian
 * Name: opportunityPrimaryContactExtension
 * Description: Controller for opportunityPrimaryContactConsole page, which is displayed as a sidebar componant on Sales Console opportunity pages. 
 * 
 * Created Date: Sep 14th, 2017
 * Created By: Malcolm Russell
 * 
 * Date Modified        Modified By                  Description of the update
 *
 =====================================================================*/
public with sharing class opportunityPrimaryContactExtension {

public Id opportunityId {get; set;}

public opportunityPrimaryContactExtension(ApexPages.StandardController controller) {

  opportunity c =(opportunity)controller.getRecord();
   opportunityId = c.Id;
}

Public OpportunityContactRole primaryContact{get{
  OpportunityContactRole[] OppConRole=[select contactId, Contact.Name,Contact.Email,Contact.Phone from OpportunityContactRole where IsPrimary=true  and OpportunityId=:opportunityId];
  if(OppConRole.size() >0){
  return OppConRole[0];
  }
  else{return null;}
}set;}

}