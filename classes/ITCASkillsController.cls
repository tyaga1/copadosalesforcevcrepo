/**=====================================================================
 * Name: ITCA_Skills
 * Description:
 * Created Date: July 12th 2017
 * Created By: James Wills
 * 
 *  Date Modified      Modified By                  Description of the update
 *  July 12th 2017     James Wills                  W-008933 - Created and added first three filters.
 *  July 13th 2017     James Wills                  W-008934 - Added filter for Career Band.
 *  Aug. 1st 2017      James Wills                  W-008934 - Adding filter for SFIA Skill
 *  Aug. 29th 2017     James Wills                  ITCA:I1430 - Filtered out Technical Specialisms from Skill drop-down.
 *  Sep. 1st 2018      Malcolm Russell              Commented out getCareerLevelListPageBlock and getCareerLevelListPageBlockFromMap as not used
 * =====================================================================*/
global class ITCASkillsController{

  public String level1_selectedCareerArea  {get;set;}
  public String level2_selectedSkillSet    {get;set;}
  public String level3_selectedSFIALevel   {get;set;}
  public String level4_selectedCareerLevel {get;set;}
  public String level5_selectedSFIASkill   {get;set;}
  //public String sfiaSkillId {get;set;}
  
  //public Map<String, List<careerLevelsForExperianSkill>> skills_Map {get;set;}
  
  public transient List<careerLevelsForExperianSkill> wrapperlistForTable1 {get;set;}
  
  //public List<List<careerLevelsForExperianSkill>> wrapperlistForTableList_List {get;set;}
    
  //public List<careerLevelsForExperianSkill> wrapperlistForTable2 {get;set;}
  
  public ITCABannerInfoClass bannerInfoLocal {get;set;}
  
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public Set<String> level1_SelectedCareerAreas_Set{
    get{
      if(level1_selectedCareerArea==null || level1_selectedCareerArea=='All Career Areas'){      
        Set<String> options_Set = new Set<String>();
        for(SelectOption sel : level1_CareerAreaOptions){
          options_Set.add(sel.getLabel());
        }      
        return options_Set;
      }
      return new Set<String>{level1_selectedCareerArea};
    }
  
  set;}
  
  public Set<String> level2_selectedSkillSets_Set{
    get{
      if(level2_selectedSkillSet==null || level2_selectedSkillSet=='All Skill Sets'){
        Set<String> options_Set = new Set<String>();
        for(SelectOption sel : level2_skillSetOptions){
          options_Set.add(sel.getValue());
        }      
        return options_Set;
      }    
      return new Set<String>{level2_selectedSkillSet};
    }
    
  set;}
  
  public Set<String> level3_selectedSFIALevels_Set   {
    get{
      if(level3_selectedSFIALevel==null || level3_selectedSFIALevel=='All Levels'){        
        Set<String> options_Set = new Set<String>();
        for(SelectOption sel : level3_sfiaLevelOptions){
          options_Set.add(sel.getLabel());
        }      
        return options_Set;
      }
      return new Set<String>{level3_selectedSFIALevel};
    }
      
  set;}
  
  public Set<String> level4_selectedCareerLevels_Set {
  get{

      if(level4_selectedCareerLevel==null || level4_selectedCareerLevel=='All Career Bands'){
        Set<String> options_Set = new Set<String>();
        for(SelectOption sel :   level4_careerLevelOptions){
          options_Set.add(sel.getValue());
        }      
        return options_Set;
      } 
  
    return new Set<String>{level4_selectedCareerLevel};
  }
  
  set;}
  
  public Set<String> level5_selectedSFIASkill_Set {
  get{

      if(level5_selectedSFIASkill==null || level5_selectedSFIASkill=='All Skills'){
        Set<String> options_Set = new Set<String>();
        for(SelectOption sel :   level5_sfiaSkillOptions){
          options_Set.add(sel.getValue());
        }      
        return options_Set;
      } 
  
    return new Set<String>{level5_selectedSFIASkill};
  }
  
  set;}
  
  
  public Set<String> careerArea_Set {get;set;}
  
  public String skillSelectParameter{get;set;}

  global class careerLevelsForExperianSkill implements Comparable{
    public String careerArea            {get;set;}
    public String skill                 {get;set;}
    public String skillURL              {get;set;}
    public String sfiaLevel             {get;set;}
    public String careerLevel           {get;set;}
    public String skillName             {get;set;} 
    public List<String> descriptionList {get;set;}
    
    public careerLevelsForExperianSkill(String careerArea, String skill, String skillURL, String sfiaLevel, String careerLevel, String skillName, List<String> descriptionList){
      this.careerArea      = careerArea;
      this.skill           = skill;
      this.skillURL        = skillURL;
      this.sfiaLevel       = sfiaLevel;
      this.careerLevel     = careerLevel; 
      this.skillName       = skillName;
      this.descriptionList = descriptionList;
    }
    
    global Integer compareTo(Object compareTo) {
       careerLevelsForExperianSkill compareToSelectedSection = (careerLevelsForExperianSkill)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (careerArea> compareToSelectedSection.careerArea) {
         if (skill> compareToSelectedSection.skill) {
           // Set return value to a positive value.        
           returnValue = 1;
         } else if(skill < compareToSelectedSection.skill){
           return -1;
         }
       
       } else if (careerArea == compareToSelectedSection.careerArea) {
         if (skill> compareToSelectedSection.skill) {
           // Set return value to a positive value.        
           returnValue = 1;
         } else if(skill < compareToSelectedSection.skill){
           return -1;
         }
       }
       return returnValue;
    }
    
    
  }
  
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public List<SelectOption> level1_CareerAreaOptions {
    get{
       List<SelectOption> selList = new List<SelectOption>();

       Schema.DescribeFieldResult fieldResult = Experian_Skill_Set__c.Career_Area__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

       selList.add(new SelectOption('All Career Areas', 'All Career Areas'));

       for( Schema.PicklistEntry f : ple){
          selList.add(new SelectOption(f.getValue(), f.getLabel()));
       }                  
       return selList;
    }
   
    set;
  }
  
  public List<SelectOption> level2_skillSetOptions {
    get{
      List<SelectOption> selList = new List<SelectOption>();
      
      /*if(level1_selectedCareerArea==Null ){
        careerArea_Set.add(level1_CareerAreaOptions[0].getLabel());
      } else {
        careerArea = level1_selectedCareerArea;
      }*/
      
      List<Experian_Skill_Set__c> skillOptions_List = [SELECT id, Name FROM Experian_Skill_Set__c WHERE Career_Area__c IN :level1_SelectedCareerAreas_Set ORDER BY Name]; 
      
      selList.add(new SelectOption('All Skill Sets', 'All Skill Sets'));
      
      for(Experian_Skill_Set__c ess : skillOptions_List){
        SelectOption sel = new SelectOption(ess.id, ess.Name);
        selList.add(sel);
      }      
      return selList;
    }
    
    set;
  }

  public List<SelectOption> level3_sfiaLevelOptions {
    get{
      List<SelectOption> selList = new List<SelectOption>();
      
      Schema.DescribeFieldResult fieldResult = Skill_Set_to_Skill__c.Level__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

      selList.add(new SelectOption('All Levels', 'All Levels'));

      for( Schema.PicklistEntry f : ple){
          selList.add(new SelectOption(f.getValue(), f.getLabel()));
      }            
      
      return selList;
    }
    
    set;
  }
  
  public List<SelectOption> level4_careerLevelOptions {
    get{
      List<SelectOption> selList = new List<SelectOption>();
      
      Schema.DescribeFieldResult fieldResult = Skill_Set_to_Skill__c.Experian_Role__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

      selList.add(new SelectOption('All Career Bands', 'All Career Bands'));

      for( Schema.PicklistEntry f : ple){
          selList.add(new SelectOption(f.getValue(), f.getLabel()));
      }            
      
      return selList;
    }
    
    set;
  }
  
  
  public List<SelectOption> level5_sfiaSkillOptions {
    get{
      List<SelectOption> selList = new List<SelectOption>();
      

      SelectOption selOp = new SelectOption('All Skills','All Skills');
      selList.add(selOp);
      
      List<ProfileSkill> skillOptions_List = [SELECT id, Name, Level1__c, Level2__c, Level3__c, Level4__c, Level5__c, Level6__c, Level7__c 
                                              FROM ProfileSkill 
                                              WHERE sfia_Skill__c=true 
                                              ORDER By Name]; 
      
      if(level3_SelectedSFIALevel!=Null && level3_SelectedSFIALevel!='All Levels'){
        for(ProfileSkill ps : skillOptions_List){             
          //if(level3_selectedSFIALevels_Set.contains('Level1') && ps.Level1__c!=Null){
          if(level3_selectedSFIALevel.contains('Level1') && ps.Level1__c!=Null){            
            selList.add(new SelectOption(ps.id, ps.Name));
          //} else if(level3_selectedSFIALevels_Set.contains('Level2') && ps.Level2__c!=Null){
          } else if(level3_selectedSFIALevel.contains('Level2') && ps.Level2__c!=Null){
            selList.add(new SelectOption(ps.id, ps.Name));
          //} else if(level3_selectedSFIALevels_Set.contains('Level3') && ps.Level3__c!=Null){
          } else if(level3_selectedSFIALevel.contains('Level3') && ps.Level3__c!=Null){
            selList.add(new SelectOption(ps.id, ps.Name));
          //} else if(level3_selectedSFIALevels_Set.contains('Level4') && ps.Level4__c!=Null){
          } else if(level3_selectedSFIALevel.contains('Level4') && ps.Level4__c!=Null){
            selList.add(new SelectOption(ps.id, ps.Name));   
          //} else if(level3_selectedSFIALevels_Set.contains('Level5') && ps.Level5__c!=Null){
          } else if(level3_selectedSFIALevel.contains('Level5') && ps.Level5__c!=Null){
            selList.add(new SelectOption(ps.id, ps.Name));
          //} else if(level3_selectedSFIALevels_Set.contains('Level6') && ps.Level6__c!=Null){
          } else if(level3_selectedSFIALevel.contains('Level6') && ps.Level6__c!=Null){
            selList.add(new SelectOption(ps.id, ps.Name));
          //} else if(level3_selectedSFIALevels_Set.contains('Level7') && ps.Level7__c!=Null){
          } else if(level3_selectedSFIALevel.contains('Level7') && ps.Level7__c!=Null){
            selList.add(new SelectOption(ps.id, ps.Name));
          }          
        } 
      }  else {   
        for(ProfileSkill prof : skillOptions_List){
          selList.add(new SelectOption(prof.id, prof.Name));
        }
      }
  
      //for(ProfileSkill prof : skillOptions_List){
      // SelectOption sel = new SelectOption(prof.id, prof.Name);
      //  selList.add(sel);
      //}      
            
      return selList;
    }
    
    set;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public Map<String, Map<String, List<careerLevelsForExperianSkill>>> careerLevelsWrapperMap {
  
    get{
      Map<String,Map<String,List<careerLevelsForExperianSkill>>> clMap = new Map<String,Map<String,List<careerLevelsForExperianSkill>>>();
      
      if(level2_selectedSkillSet!=Null){
        skillSelectParameter=level2_selectedSkillSet;
      } else{
        if(!level2_skillSetOptions.isEmpty()){
          skillSelectParameter=level2_skillSetOptions[0].getValue();
          level3_selectedSFIALevel = 'All Levels';
        } else {
          return null;
        }
      }      
     
     
      List<Skill_Set_To_Skill__c> ssToSkill_List = [SELECT id, Experian_Skill_Set__c, Experian_Skill_Set__r.Career_Area__c, Experian_Skill_Set__r.Name, Level__c, Experian_Role__c, Skill__c  
                                                    FROM Skill_Set_To_Skill__c 
                                                    WHERE Experian_Skill_Set__r.Career_Area__c IN :level1_SelectedCareerAreas_Set
                                                    AND Experian_Skill_Set__c                  IN :level2_selectedSkillSets_Set 
                                                    AND Level__c                               IN :level3_selectedSFIALevels_Set
                                                    AND Experian_Role__c                       IN :level4_selectedCareerLevels_Set
                                                    AND Skill__c                               IN :level5_selectedSFIASkill_Set
                                                    ORDER BY Experian_Skill_Set__r.Career_Area__c, Experian_Skill_Set__r.Name, Level__C ASC];
      
      
      
      Map<ID,ID> ssToSkills_Map = new Map<ID,ID>();
      
      for(Skill_Set_to_Skill__c sts : ssToSkill_List){
        ssToSkills_Map.put(sts.id, sts.Skill__c);
      }            
      
      if(!ssToSkill_List.isEmpty()){
        Map<ID,ProfileSkill> skillOptions_Map = new Map<ID, ProfileSkill>([SELECT id, Name, Level1__c, Level2__c, Level3__c, Level4__c, Level5__c, Level6__c, Level7__c 
                                                                           FROM ProfileSkill
                                                                           WHERE id IN :ssToSkills_Map.values()]);
      
        for(Skill_Set_To_Skill__c ssToSkill : ssToSkill_List){
          List<String> desclist = new List<String>();
        
          ProfileSkill ps = skillOptions_Map.get(ssToSkill.Skill__c);
        
          if(ssToSkill.Level__c =='Level1' && ps.Level1__c!=Null){        
            descList.add(ps.Level1__c);
          }
        
          if(ssToSkill.Level__c =='Level2' && ps.Level2__c!=Null){
            descList.add(ps.Level2__c);
          }
        
          if(ssToSkill.Level__c =='Level3' && ps.Level3__c!=Null){
            descList.add(ps.Level3__c);
          }
        
          if(ssToSkill.Level__c =='Level4' && ps.Level4__c!=Null){
            descList.add(ps.Level4__c);
          }
        
          if(ssToSkill.Level__c =='Level5' && ps.Level5__c!=Null){
            descList.add(ps.Level5__c);
          }
        
          if(ssToSkill.Level__c =='Level6' && ps.Level6__c!=Null){
            descList.add(ps.Level6__c);
          }
        
          if(ssToSkill.Level__c =='Level7' && ps.Level7__c!=Null){
            descList.add(ps.Level7__c);
          }

          careerLevelsForExperianSkill cl = new careerLevelsForExperianSkill(ssToSkill.Experian_Skill_Set__r.Career_Area__c,
                                                                             ssToSkill.Experian_Skill_Set__r.Name,
                                                                             ssToSkill.Experian_Skill_Set__c,
                                                                             ssToSkill.Level__c,
                                                                             ssToSkill.Experian_Role__c,
                                                                             ps.Name,          
                                                                             desclist);
                                                                             
          //Map<String,Map<String,List<careerLevelsForExperianSkill>>> clMap = new Map<String,List<careerLevelsForExperianSkill>>>();                                                    
          //Key1: Career Area, Key2: Skill Set.
                    
          //Check if we don't have an entry for the Career Area
          if(!clMap.keySet().contains(cl.careerArea)){
            //Create entry for Career Area and add the wrapper instance
            Map<String, List<careerLevelsForExperianSkill>> skill_Map = new Map<String, List<careerLevelsForExperianSkill>>();
            List<careerLevelsForExperianSkill> carLevel_List = new List<careerLevelsForExperianSkill>{cl};
            skill_Map.put(cl.skill, carLevel_List);
            clMap.put(cl.careerArea,skill_Map);
          } else {
            //If there is an entry for Career Area check if there is an entry for the Skill Set
            Map<String, List<careerLevelsForExperianSkill>> skill_Map = clMap.get(cl.careerArea);            
            if(!skill_Map.keySet().contains(cl.skill)){   
              //If not, create one and add the current wrapper instance
              List<careerLevelsForExperianSkill> carLevel_List = new List<careerLevelsForExperianSkill>{cl};
              skill_Map.put(cl.skill, carLevel_List);
            } else {
              //If there is then add the current wrapper instance
              List<careerLevelsForExperianSkill> carLevel_List = skill_Map.get(cl.skill);
              carLevel_List.add(cl);
              skill_Map.put(cl.skill, carLevel_List);            
            }
            clMap.put(cl.careerArea,skill_Map);
          }
        }
      
      }
  
      return clMap;
    }
    set;
  }

/*
  public Component.Apex.pageBlock getCareerLevelListPageBlockFromMap(){
      Component.Apex.PageBlock careerLevelPageBlock = new Component.Apex.PageBlock();

      List<Component.Apex.PageBlockTable> pbt_List = new List<Component.Apex.PageBlockTable>();

      for(String careerKey : careerLevelsWrapperMap.keySet()){                      
        Component.Apex.PageBlockSection careerLevelPageBlockSection = new Component.Apex.PageBlockSection(title = careerKey);               
        
        //Find the Map of Skill sets (composed of instances of skills) for the Career Area
        Map<String, List<careerLevelsForExperianSkill>> skills_Map = careerLevelsWrapperMap.get(careerKey);                

        for(String skillKey : skills_Map.keySet()){         
          
          Component.Apex.PageBlockSection careerLevelPageBlockSubSection = new Component.Apex.PageBlockSection();
          careerLevelPageBlockSubSection.title=skillKey;
          
          /*<table class="table-bordered" style="width:94%;border-spacing: 0 !important;margin-top:{!If(AND(accPlanTeamList.size <= 8 , $Request.mode == 'pdf'), 15, 10)}px !important;">
          <tr><td colspan="2" style="padding-left:0px !important;"> <div class="title">{!$Label.ACCOUNTPLANNING_PDF_Account_Team}</div> </td></tr>
          <tr style="background-color: rgb(207, 207, 207);">
            <td width="25%" style="font-size: 13px; font-weight: bold;text-align:left;padding-left : 10px;">{!$ObjectType.User.fields.Name.label}</td>
            <td width="25%" style="font-size: 13px; font-weight: bold;text-align:left;padding-left : 10px;">{!$ObjectType.Account_Plan_Team__c.fields.Job_Title__c.label}</td>
         </tr>
            <apex:repeat value="{!accPlanTeamList}" var="teamMember">
            <tr>
              
            </tr>
            </apex:repeat>
         </table>*/
  /*        
          Component.Apex.OutputText tbl1 = new Component.Apex.OutputText();
          tbl1.escape=false;
          tbl1.value='<table class="table-bordered">';
          //careerLevelPageBlockSubSection.childComponents.add(tbl1);
          
          Component.Apex.OutputText tr1 = new Component.Apex.OutputText();
          tr1.escape=false;
          tr1.value='<tr><td>SFIA Level</td><td>Career Levels</td><td>Description</td></tr>';
          //careerLevelPageBlockSubSection.childComponents.add(tr1);
          tbl1.childComponents.add(tr1);
          
          //List<careerLevelsForExperianSkill> 
          
          wrapperListForTable1 = skills_Map.get(skillKey);
   
          for(careerLevelsForExperianSkill clw : wrapperListForTable1){
            Component.Apex.OutputText tr2a = new Component.Apex.OutputText();
            tr2a.escape=false;
            tr2a.value='<tr>';
            //careerLevelPageBlockSubSection.childComponents.add(tr2a);
                    
            
              Component.Apex.OutputText td1a = new Component.Apex.OutputText();
              td1a.escape=false;
              td1a.value='<td>';
              //careerLevelPageBlockSubSection.childComponents.add(td1a);  
              //tbl1.childComponents.add(td1a);
              tr2a.childComponents.add(td1a);
              
                Component.Apex.OutputText txt1 = new Component.Apex.OutputText();
                txt1.value=clw.sfiaLevel;
                //careerLevelPageBlockSubSection.childComponents.add(txt1);  
                //tbl1.childComponents.add(txt1); 
                tr2a.childComponents.add(txt1);      
              Component.Apex.OutputText td1b = new Component.Apex.OutputText();
              td1b.escape=false;
              td1b.value='</td>';
              //careerLevelPageBlockSubSection.childComponents.add(td1b);
              //tbl1.childComponents.add(td1b);
              tr2a.childComponents.add(td1b);
            
              /*Component.Apex.OutputText td2a = new Component.Apex.OutputText();
              td2a.escape=false;
              td2a.value='<td>';
              careerLevelPageBlockSubSection.childComponents.add(td2a);
              tbl1.childComponents.add(td2a);
                Component.Apex.OutputText txt2 = new Component.Apex.OutputText();
                txt2.value = clw.careerLevel;
                //careerLevelPageBlockSubSection.childComponents.add(txt2);
                tbl1.childComponents.add(txt2);
              Component.Apex.OutputText td2b = new Component.Apex.OutputText();
              td2b.escape=false;
              td2b.value='</td>';
              //careerLevelPageBlockSubSection.childComponents.add(td2b);
              tbl1.childComponents.add(td2b);*/
    
             /* Component.Apex.OutputText td3a = new Component.Apex.OutputText();
              td3a.escape=false;
              td3a.value='<td>';
              //careerLevelPageBlockSubSection.childComponents.add(td3a);
              tbl1.childComponents.add(td3a);
                Component.Apex.repeat rpt1     = new Component.Apex.repeat();
                rpt1.value=clw.descriptionList;
                rpt1.var='clwl';
                Component.Apex.OutputText txt3 = new Component.Apex.OutputText();
                txt3.expressions.value='{!clwl}';
                rpt1.childComponents.add(txt3);     
                Component.Apex.OutputText txt4 = new Component.Apex.OutputText();
                txt4.escape=false;
                txt4.value='<br/><br/><br/>';
                rpt1.childComponents.add(txt4);          
                //careerLevelPageBlockSubSection.childComponents.add(rpt1); 
                tbl1.childComponents.add(rpt1);  
              Component.Apex.OutputText td3b = new Component.Apex.OutputText();
              td3b.escape=false;
              td3b.value='</td>';
              //careerLevelPageBlockSubSection.childComponents.add(td3b);
              tbl1.childComponents.add(td3b);*/
            

      /*      
            Component.Apex.OutputText tr2b = new Component.Apex.OutputText();
            tr2b.escape=false;
            tr2b.value='</tr>';
            //careerLevelPageBlockSubSection.childComponents.add(tr2b);
            //tbl1.childComponents.add(tr2b);            
            tr2a.childComponents.add(tr2b);         
               
            tbl1.childComponents.add(tr2a);   
          }
          
          Component.Apex.OutputText tbl2 = new Component.Apex.OutputText();
          tbl2.escape=false;
          tbl2.value='</table>';
          //careerLevelPageBlockSubSection.childComponents.add(tbl2);
          tbl1.childComponents.add(tbl2);
          
          careerLevelPageBlockSubSection.childComponents.add(tbl1);
          
          careerLevelPageBlockSection.childComponents.add(careerLevelPageBlockSubSection);           

        }
        careerLevelPageBlock.childComponents.add(careerLevelPageBlockSection);
      }      
      return careerLevelPageBlock;      
  }
  */

  public void resetSkillList(){
    level2_selectedSkillSet=null;
    level2_skillSetOptions.clear();
    
    level5_selectedSFIASkill=null;
    level5_sfiaSkillOptions.clear();
  }
  
  //public transient Component.Apex.pageBlock careerLevelPageBlock_Local  = new Component.Apex.pageBlock();
  
  /*public Component.Apex.pageBlock careerLevelListPageBlock{
     get{ 
       if(careerLevelPageBlock_Local==null){
         careerLevelPageBlock_Local=getCareerLevelListPageBlock_Loc();
       }
       return careerLevelPageBlock_Local;
     }
     set;    
  }*/
  
  public List<careerLevelsForExperianSkill> careerLevelsWrapperList {
  
    get{
      List<careerLevelsForExperianSkill> clList = new List<careerLevelsForExperianSkill>();
      
      if(level2_selectedSkillSet!=Null){
        skillSelectParameter=level2_selectedSkillSet;
      } else{
        if(!level2_skillSetOptions.isEmpty()){
          skillSelectParameter=level2_skillSetOptions[0].getValue();
          level3_selectedSFIALevel = 'All Levels';
        } else {
          return null;
        }
      }      
     
      List<Skill_Set_To_Skill__c> ssToSkill_List = [SELECT id, Experian_Skill_Set__c, Experian_Skill_Set__r.Career_Area__c, Experian_Skill_Set__r.Name, Level__c, Experian_Role__c, Skill__c  
                                                    FROM Skill_Set_To_Skill__c 
                                                    WHERE Experian_Skill_Set__r.Career_Area__c IN :level1_SelectedCareerAreas_Set
                                                    AND Experian_Skill_Set__c                  IN :level2_selectedSkillSets_Set 
                                                    AND Level__c                               IN :level3_selectedSFIALevels_Set
                                                    AND Experian_Role__c                       IN :level4_selectedCareerLevels_Set
                                                    AND Skill__c                               IN :level5_selectedSFIASkill_Set
                                                    ORDER BY Experian_Skill_Set__r.Career_Area__c, Level__C ASC];
      
      Map<ID,ID> ssToSkills_Map = new Map<ID,ID>();
      
      for(Skill_Set_to_Skill__c sts : ssToSkill_List){
        ssToSkills_Map.put(sts.id, sts.Skill__c);
      }            
      
      if(!ssToSkill_List.isEmpty()){
        Map<ID,ProfileSkill> skillOptions_Map = new Map<ID, ProfileSkill>([SELECT id, Name, Level1__c, Level2__c, Level3__c, Level4__c, Level5__c, Level6__c, Level7__c 
                                                                           FROM ProfileSkill
                                                                           WHERE id IN :ssToSkills_Map.values()]);
      
        for(Skill_Set_To_Skill__c ssToSkill : ssToSkill_List){
          List<String> desclist = new List<String>();
        
          ProfileSkill ps = skillOptions_Map.get(ssToSkill.Skill__c);
        
          if(ssToSkill.Level__c =='Level1' && ps.Level1__c!=Null){        
            descList.add(ps.Level1__c);
          }
        
          if(ssToSkill.Level__c =='Level2' && ps.Level2__c!=Null){
            descList.add(ps.Level2__c);
          }
        
          if(ssToSkill.Level__c =='Level3' && ps.Level3__c!=Null){
            descList.add(ps.Level3__c);
          }
        
          if(ssToSkill.Level__c =='Level4' && ps.Level4__c!=Null){
            descList.add(ps.Level4__c);
          }
        
          if(ssToSkill.Level__c =='Level5' && ps.Level5__c!=Null){
            descList.add(ps.Level5__c);
          }
        
          if(ssToSkill.Level__c =='Level6' && ps.Level6__c!=Null){
            descList.add(ps.Level6__c);
          }
        
          if(ssToSkill.Level__c =='Level7' && ps.Level7__c!=Null){
            descList.add(ps.Level7__c);
          }

          careerLevelsForExperianSkill cl = new careerLevelsForExperianSkill(ssToSkill.Experian_Skill_Set__r.Career_Area__c,
                                                                             ssToSkill.Experian_Skill_Set__r.Name,
                                                                             ssToSkill.Experian_Skill_Set__c,
                                                                             ssToSkill.Level__c,
                                                                             ssToSkill.Experian_Role__c,
                                                                             ps.Name,
                                                                             desclist);
          clList.add(cl);
        }
      
      }
      clList.sort();
      
      return clList;
    }
    set;
  }
  /*
  public Component.Apex.pageBlock getCareerLevelListPageBlock(){

      Component.Apex.pageBlock careerLevelPageBlock = new Component.Apex.pageBlock();

      Set<Map<String,String>> skillsByCareerArea_Set = new Set<Map<String,String>>();
      
      for(careerLevelsForExperianSkill careerLevelsWrapperItem : careerLevelsWrapperList){
        Map<String,String> skillCareerCombination_Map = new Map<String,String>();
        skillCareerCombination_Map.put(careerLevelsWrapperItem.careerArea, careerLevelsWrapperItem.skill);
      
        if(!skillsByCareerArea_Set.contains(skillCareerCombination_Map)){
          skillsByCareerArea_Set.add(skillCareerCombination_Map);
        }
      }
      
      for(Map<String,String> mapValue : skillsByCareerArea_Set){
        List<String> mapValueKey_List = new List<String>(mapValue.keySet());
        List<String> mapValue_List = new List<String>(mapValue.values());
        
        Component.Apex.pageBlockSection careerLevelPageBlockSection = new Component.Apex.pageBlockSection(title = mapValueKey_List[0] + ' ' + mapValue_List[0]);               
        
        Component.Apex.pageBlockTable careerLeveList_Table    = new Component.Apex.pageBlockTable();
        
        careerLevelPageBlock.childComponents.add(careerLevelPageBlockSection);
        

      }

      return careerLevelPageBlock;
      

  }
  
*/
  public itcaSkillsController(ApexPages.StandardController con){
    Experian_SKill_Set__c expSkillSet = (Experian_Skill_Set__c) con.getRecord();
    
    bannerInfoLocal = new ITCABannerInfoClass();
    bannerInfoLocal.currentActiveTab = 'isSummary';
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();
    bannerInfoLocal.currentUserSkillLevelMap = new Map<String,Decimal>();
    
  }
  
  
  public void submit(){
    resetSkillList();
  }
}