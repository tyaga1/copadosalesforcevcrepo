/*=============================================================================
 * Experian
 * Name: RegionalScorecardUtils
 * Description: Case 01916186
                Common methods for running the regional scorecard processes.
 * Created Date: 23 May 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 *
 =============================================================================*/

public class RegionalScorecardUtils {

  public static String KEY_SEPARATOR = '~';

  public static void startBatch() {

    List<Regional_Scorecard_Metric__c> allMetrics = [
      SELECT Id, Calculation_Field_Operator__c
      FROM Regional_Scorecard_Metric__c
      WHERE Active__c = true
      ORDER BY Master_Grouping__c DESC, Processing_Order__c
    ];

    Map<Id, Boolean> allMetricTypes = new Map<Id, Boolean>();
    List<Id> allMetricIds = new List<Id>();

    for (Regional_Scorecard_Metric__c rsm : allMetrics) {
      allMetricIds.add(rsm.Id);
      allMetricTypes.put(rsm.Id, String.isNotBlank(rsm.Calculation_Field_Operator__c));
    }

    runBatch(allMetricIds, allMetricTypes);
  }

  public static void runBatch (List<Id> metricIdList, Map<Id, Boolean> metricTypeMap) {
    if (metricIdList.isEmpty() || metricIdList == null) {
      return;
    }
    Id nextMetricId = metricIdList.remove(0);
    if (metricTypeMap.get(nextMetricId) == false) {
      BatchRegionalScorecardEngine brse = new BatchRegionalScorecardEngine();
      brse.metricIdList = metricIdList;
      brse.metricIdTypeMap = metricTypeMap;
      brse.metricId = nextMetricId;
      Database.executeBatch(brse, ScopeSizeUtility.getScopeSizeForClass('BatchRegionalScorecardEngine'));
    }
    else {
      BatchRegionalScorecardCalculated brsc = new BatchRegionalScorecardCalculated();
      brsc.metricIdList = metricIdList;
      brsc.metricIdTypeMap = metricTypeMap;
      brsc.metricId = nextMetricId;
      Database.executeBatch(brsc, 1); //ScopeSizeUtility.getScopeSizeForClass('BatchRegionalScorecardCalculated'));
    }
  }

  public static Regional_Scorecard_Data__c buildDataRecord (Regional_Scorecard_Metric__c currentMetric, metricData mData, String mName, String groupName) {
    Regional_Scorecard_Data__c rsd = new Regional_Scorecard_Data__c(
      Regional_Scorecard_Metric__c = currentMetric.Id,
      Metric_Type__c = currentMetric.Type__c,
      CurrencyIsoCode = 'USD',
      Value_Count__c = (currentMetric.Type__c.equals('Count') ?
                        (currentMetric.Aggregate_Type__c.equals('Average') ? Integer.valueOf(mData.getTotalAverage()) :
                        (currentMetric.Aggregate_Type__c.equals('Sum') ? Integer.valueOf(mData.totalValue) : mData.totalCount))
                        : null),
      Value_Currency__c = (currentMetric.Type__c.equals('Currency') ?
                           (currentMetric.Aggregate_Type__c.equals('Average') ? mData.getTotalAverage() : mData.totalValue)
                         : null),
      Value_Percentage__c = (currentMetric.Type__c.equals('Percentage') ? mData.getTotalAverage() : null),
      Value_Decimal__c = (currentMetric.Type__c.equals('Decimal') ?
                          (currentMetric.Aggregate_Type__c.equals('Average') ? mData.getTotalAverage() : mData.totalValue)
                         : null),
      Name__c = mName,
      Metric_ID__c = currentMetric.Id + KEY_SEPARATOR + groupName + KEY_SEPARATOR + mName,
      Type__c = groupName
    );
    return rsd;
  }

  public static Regional_Scorecard_Data__c buildDataRecord (Regional_Scorecard_Metric__c currentMetric, Decimal mValue, String mName, String groupName) {
    Regional_Scorecard_Data__c rsd = new Regional_Scorecard_Data__c(
      Regional_Scorecard_Metric__c = currentMetric.Id,
      Metric_Type__c = currentMetric.Type__c,
      CurrencyIsoCode = 'USD',
      Value_Count__c = (currentMetric.Type__c.equals('Count') ? Integer.valueOf(mValue) : null),
      Value_Currency__c = (currentMetric.Type__c.equals('Currency') ? mValue : null),
      Value_Percentage__c = (currentMetric.Type__c.equals('Percentage') ? mValue : null),
      Value_Decimal__c = (currentMetric.Type__c.equals('Decimal') ? mValue : null),
      Name__c = mName,
      Metric_ID__c = currentMetric.Id + KEY_SEPARATOR + groupName + KEY_SEPARATOR + mName,
      Type__c = groupName
    );
    return rsd;
  }

  public class metricData {
    public Integer totalCount {get;set;}
    public String metricName {get;set;}
    public Decimal totalValue {get;set;}
    public Decimal getTotalAverage () {
      system.debug(' Getting average for '+metricName + ' of Count: '+totalCount + ', Value: '+totalValue);
      if (totalCount > 0 && totalValue != 0.0) {
        return totalValue.divide(totalCount,5);
      }
      return 0;
    }
    public metricData (String mName) {
      metricName = mName;
      totalCount = 0;
      totalValue = 0;
    }
  }

}