/**=====================================================================
 * Experian
 * Name: BatchWalletSyncOpportunityOwners
 * Description: Designed to update the ownership of opportunities based on the changes to accounts from the WalletSync process
 *              Incorporates tasks on the opportunities too.
 *  NOTE:
 *    This batch is started from BatchWalletSyncAccountOwners
 *
 * Created Date: 3rd, August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Sep 10th, 2015               Paul Kissick                 Added try/catch around email sending
 * Oct 1st, 2015                Paul Kissick                 Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Nov 27th, 2015               Paul Kissick                 Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 =====================================================================*/

global class BatchWalletSyncOpportunityOwners implements  Database.Batchable<sObject>, Database.Stateful {
  
  global List<String> updateErrors;
  
  global Database.QueryLocator start(Database.BatchableContext BC) {

    updateErrors = new List<String>();
    Datetime lastStartDate = WalletSyncUtility.getWalletSyncProcessingLastRun();

    return Database.getQueryLocator([
      SELECT Id, Previous_Account_Owner__c,
        Account_Owner__c, Account__c
      FROM WalletSync__c
      WHERE Previous_Account_Owner__c != null
      AND Account_Owner__c != null
      AND Account__c != null
      AND Last_Processed_Date__c >= :lastStartDate
    ]);
  }
  
  
  
  global void execute(Database.BatchableContext BC, List<WalletSync__c> scope) {
    // Build a map of cnpj to owners, both old and new
    Map<Id,Id> cnpjToNewOwner  = new Map<Id,Id>();
    Map<Id,Id> cnpjToOldOwner  = new Map<Id,Id>();
    
    for(WalletSync__c ws : scope) {
      cnpjToNewOwner.put(ws.Account__c, ws.Account_Owner__c);
      cnpjToOldOwner.put(ws.Account__c, ws.Previous_Account_Owner__c);
    }
    
    // Based on the previous owner, now find all opps for accounts for these users...
    List<Opportunity> allOpps = [
      SELECT Id, OwnerId, AccountId, (
        SELECT Id, OwnerId 
        FROM Tasks 
        WHERE OwnerId IN :cnpjToOldOwner.values() 
        AND Status IN ('In Progress','Not Started')
      )
      FROM Opportunity
      WHERE IsClosed = false
      AND AccountId IN :cnpjToOldOwner.keySet()
      AND OwnerId IN :cnpjToOldOwner.values()
    ];
    List<Task> allTasks = new List<Task>();
    
    for(Opportunity opp : allOpps) {
      if (opp.OwnerId == cnpjToOldOwner.get(opp.AccountId)) {
        opp.OwnerId = cnpjToNewOwner.get(opp.AccountId);
        if (opp.Tasks != null && opp.Tasks.size() > 0) {
          for(Task tsk : opp.Tasks) {
            tsk.OwnerId = cnpjToNewOwner.get(opp.AccountId);
          }
          allTasks.addAll(opp.Tasks);
        }
      }
    }
    
    // TODO: Should the user running this have 'IsDataAdmin = true' to prevent other changes?
    List<Database.SaveResult> updOppsRes = Database.update(allOpps,false);
    for(Database.SaveResult sr : updOppsRes) {
      if(!sr.isSuccess()) {
        for(Database.Error err : sr.getErrors()) {
          updateErrors.add(err.getMessage());
        }
      }
    }
    
    List<Database.SaveResult> updTasksRes = Database.update(allTasks,false);
    for(Database.SaveResult sr : updTasksRes) {
      if(!sr.isSuccess()) {
        for(Database.Error err : sr.getErrors()) {
          updateErrors.add(err.getMessage());
        }
      }
    }
  }
  
  global void finish(Database.BatchableContext BC) {
    try {
	    BatchHelper bh = new BatchHelper();
	    bh.checkBatch(BC.getJobId(), 'BatchWalletSyncOpportunityOwners', false);
	    
	    if (updateErrors.size() > 0) {
	      bh.batchHasErrors = true;
	      bh.emailBody += '\nThe following errors were observed when updating records:\n';
	      bh.emailBody += String.join(updateErrors,'\n');
	    }      
	      
	    bh.sendEmail();
    }
    catch (Exception e) {
      system.debug(e.getMessage()); 
    }

    if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
      // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF ACTIVITY OWNERSHIP
      system.scheduleBatch(new BatchWalletSyncActivityOwners(), 'BatchWalletSyncActivityOwners'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchWalletSyncActivityOwners'));
    }
    
  }
  
  
  
}