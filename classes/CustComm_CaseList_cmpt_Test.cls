@isTest
private class CustComm_CaseList_cmpt_Test {
    
    @isTest static void caseListApexControllertest() {
        Account testAccount1 = Test_Utils.createAccount();
        insert testAccount1;
        List<Case> caseList = new List<Case>();
        String serviceCentralId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Central').getRecordTypeId();
 
        Case c1 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 1',
                        Description = 'Test Description 1',
                        status = 'Closed-Complete');
        caseList.add(c1);

        Case c2 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 2',
                        Description = 'Test Description 2',
                        status = 'Closed-Complete');
        caseList.add(c2);

        Case c3 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 3',
                        Description = 'Test Description 3',
                        Status = 'In-Progress');
        caseList.add(c3);

        Case c4 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 4',
                        Description = 'Test Description 4',
                        Status = 'In-Progress');
        caseList.add(c4);


        insert caseList;

        List<CustComm_CaseListController.caseWrapper> returnedOpenList = CustComm_CaseListController.findAll('Status','DESC','Open');
        List<CustComm_CaseListController.caseWrapper> returnedClosedList = CustComm_CaseListController.findAll('Status','DESC','Closed');

    }
    
}