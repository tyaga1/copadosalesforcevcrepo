/**********************************************************************************************
 * Appirio, Inc 
 * Name         : SetOldestScheduleDate_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleSetOldestScheduleDate" & SetOldestScheduleDate
 * Created Date : July 27, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * [Date]                       [Name]                      [Description]
***********************************************************************************************/

@isTest
private class SetOldestScheduleDate_Test {
    
  static testMethod void testSchedulable() {
    Global_Settings__c gs = new Global_Settings__c();
    gs.Name = 'Global';
    gs.Batch_Failures_Email__c = '';
    insert gs;
      
    test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('Set Oldest ScheduleDate', CRON_EXP, new ScheduleSetOldestScheduleDate());
      
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime
                      FROM CronTrigger 
                      WHERE id = :jobId];     
    test.stopTest();
    // Verify the expressions are the same  
    System.assertEquals(CRON_EXP, ct.CronExpression);
    // Verify the job has not run  
    System.assertEquals(0, ct.TimesTriggered);
  }
    
    
  private static testMethod void testDaystoBill() {
    Test.startTest();
    SetOldestScheduleDate batchToProcess = new SetOldestScheduleDate();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    system.assertEquals(1,[SELECT COUNT() FROM OpportunityLineItem WHERE Product_Days_to_Bill__c <> null]);
  }
  
  @testSetup
  private static void setupData() {
       
    Account tstAcc = Test_utils.createAccount();
                
    insert new List<Account>{tstAcc};
    
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
        tstOpp.CloseDate = system.today().addDays(7);
        
    insert new List<Opportunity>{tstOpp};
    
    Product2 product = Test_Utils.insertProduct();
    Product.CanUseRevenueSchedule = True;
    update product;
    PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
    
    OpportunityLineItem tstOli = Test_utils.createOpportunityLineItem(tstOpp.Id, stdPricebookEntry.Id , tstOpp.Type);
        tstOli.Start_Date__c = Date.today().addDays(5);
        tstOli.End_Date__c = Date.today().addDays(120);
        
    insert new List<OpportunityLineItem>{tstOli};
    
    Test_utils.createMultiOpportunityLineItemSche(tstOli.Id);
    
  }
  
}