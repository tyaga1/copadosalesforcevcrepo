/*=============================================================================
 * Experian
 * Name: BatchHelper_Test
 * Description: Test class for BatchHelper
 * Created Date: 19 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class BatchHelper_Test {
  
  static testMethod void testGetTimestamp() {
    
    Datetime checkVal = BatchHelper.getBatchClassTimestamp('TestClassName1');
    
    system.assertEquals(1, [SELECT COUNT() FROM Batch_Class_Timestamp__c WHERE Name = 'TestClassName1']);
    
    system.assertEquals(null, checkVal);
    
  }
  
  static testMethod void testSetTimestamp() {
    
    Datetime testDt = Datetime.now();
    
    BatchHelper.setBatchClassTimestamp('TestClassName1', testDt);
    
    system.assertEquals(1, [SELECT COUNT() FROM Batch_Class_Timestamp__c WHERE Name = 'TestClassName1' AND Time__c != null]);
    
    Datetime checkVal = BatchHelper.getBatchClassTimestamp('TestClassName1');
    
    system.assertEquals(testDt, checkVal);
    
    BatchHelper.setBatchClassTimestamp('TestClassName1', testDt);
    
  }
  
  static testMethod void testNewGuid() {
    
    system.assertNotEquals(null, BatchHelper.newGuid());
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
  }
  
}