/**
 *  ExpComm_ExpiredArticlesEmail_Schedule
 *  @description Scheduler for triggering Expired Articles Email Batch jobs.
 *  @author Arun Jayaseelan, UC Innovation
 *  @date 2017-02-06
 */
global class ExpComm_ExpiredArticlesEmail_Schedule implements Schedulable {
	public static Id jobId;

	global void execute(SchedulableContext sc) {
		ExpComm_ExpiredArticlesEmail_Batch expArticlesEmail = new ExpComm_ExpiredArticlesEmail_Batch();
        database.executeBatch(expArticlesEmail);
	}

	public static void runScheduler(String jobName, String cronExp){
		ExpComm_ExpiredArticlesEmail_Schedule scheduler = new ExpComm_ExpiredArticlesEmail_Schedule();
		jobId = System.schedule(jobName, cronExp, scheduler);
	}
}