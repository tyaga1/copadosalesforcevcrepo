/******************************************************************************
 * Name: expcommunity_apps.cls
 * Created Date: 2/14/2017
 * Created By: Hay Win
 * Used In: expcommunity_app_launcher.cmpt
 * Description : Controller for expcommunity_app_launcher component, which is used in new Exp Community template
 *               
 * Change Log- 
 ******************/

public with sharing class expcommunity_apps{ 
   
    public List<ExpCommunity_Apps__c> appList{get;set;}
     public boolean featured {get;set;}
    
    public expcommunity_apps() {
        appList= new List<ExpCommunity_Apps__c>();
        //appList= [Select Id, link__c, Name, description__c, Image_Path__c from ExpCommunity_Apps__c where Sub_Communities__c = false AND Feature__c = true Order by Order__c asc];
    }
    
     public boolean getfindApps(){
         String featureString = '';
         if (featured == true) {
             featureString =  ' AND Feature__c =: featured';
         }        
         
         String appQuery = 'Select Id, link__c, Name, description__c, Image_Path__c from ExpCommunity_Apps__c where Sub_Communities__c = false' + featureString + ' Order by Order__c asc';
         //commList = [Select Id, link__c, Name, description__c, Image_Path__c from ExpCommunity_Apps__c where Sub_Communities__c = true AND Feature__c =: featured Order by Order__c asc];
         appList = Database.query(appQuery); 
         
         return true;
     }
   
}