/**=====================================================================
 * Experian
 * Name: ScopeSizeUtility_Test
 * Description: 
 * 
 * Created Date: 12 August 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class ScopeSizeUtility_Test {

  static testMethod void doSomeTests() {
    
    system.assertEquals(ScopeSizeUtility.defaultScopeSize,ScopeSizeUtility.getScopeSizeForClass('PutAnyClassNameHereAndCheckItWorks123'));
    system.assertEquals(ScopeSizeUtility.defaultScopeSize,ScopeSizeUtility.getScopeSizeForClass(''));
    system.assertEquals(ScopeSizeUtility.defaultScopeSize,ScopeSizeUtility.getScopeSizeForClass(null));
    
    Batch_Class_Scope_Size__c bcss1 = new Batch_Class_Scope_Size__c(
      Name = 'PutAnyClassNameHereAndCheckItWorks123',
      Scope_Size__c = 1
    );
    insert bcss1;
    
    system.assertEquals(1,ScopeSizeUtility.getScopeSizeForClass('PutAnyClassNameHereAndCheckItWorks123'));
    
  }
}