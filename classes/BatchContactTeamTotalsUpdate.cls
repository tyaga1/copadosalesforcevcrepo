/**=====================================================================
* Created Date: 14th Jan 2016 
* Name: BatchContactTeamTotalsUpdate
* Description: Batch class for updating totals Custom Fields on Contact Object
* Created by: James Wills
*
* See Change Request #01252420 Creation of a Contact DQ Score Field on the Contact Object
* The original purpose of this Class is to emulate the RollUpSummary functionality for a Lookup Relationship
* for two new fields on the Contact Object:     Total_Contact_Relationship_Count__c
*                                           and Active_Contact_Relationship_Count__c
*
* This class is called from the ContactTeamTriggerHandler Class
*
*/

global class BatchContactTeamTotalsUpdate implements Database.Batchable<sObject> {
    
    global List<Contact_Team__c> listOfContactTeamRecords = new List<Contact_Team__c>();
    
    //global List<User> listOfUserRecords = new List<User>();
    
    global BatchContactTeamTotalsUpdate(List<Contact_Team__c> newContactTeamList){
      listOfContactTeamRecords = newContactTeamList;
    }//ContactTeamUsersUpdate_Batch()
    
    //global ContactTeamUsersUpdate_Batch(List<User> updatedUserList){
    //  listOfUserRecords = updatedUserList;
    //}//ContactTeamUsersUpdate_Batch()
    
    global Database.Querylocator start ( Database.BatchableContext bc ) {
  
      List<ID> parentContactsOfContactTeamRecords = new List<ID>();
      List<Contact> parentContactsList = new List<Contact>();    

      for(Contact_Team__c contactTeamRecord : listOfContactTeamRecords){
        parentContactsOfContactTeamRecords.add(contactTeamRecord.Contact__c);
      }//for    
      
      //if(listOfContactTeamRecords.size()>0){
        return Database.getQueryLocator ([SELECT Id, (SELECT Id, Relationship_Owner_is_Active__c, Relationship_Owner__r.IsActive FROM Contact_Teams__r) 
                                       FROM Contact WHERE Id IN :parentContactsOfContactTeamRecords]);                                        
      //
      
                                          
    }//start()
    
   
    
    global void execute (Database.BatchableContext bc, List<Contact> scope) {
      Integer activeContactTeamCounter = 0;              
      
      for(Contact parentContact   : scope){
      
        //1. Update Contact.Total_Contact_Relationship_Count__c
        parentContact.Total_Contact_Relationship_Count__c = parentContact.Contact_Teams__r.size();
        //1. Update Contact.Total_Contact_Relationship_Count__c
        
        //2. Update Contact.Active_Contact_Relationship_Count__c
        for(Contact_Team__c contactTeam : parentContact.Contact_Teams__r){
          if(contactTeam.Relationship_Owner__r.isActive){
            activeContactTeamCounter++;
          }//if
        }//for
        parentContact.Active_Contact_Relationship_Count__c = activeContactTeamCounter;
        //2. Update Contact.Active_Contact_Relationship_Count__c
                
        activeContactTeamCounter=0;        
      }//for
              
      Database.update(scope, false);
    
    }//execute()
    
    //global void execute (Database.BatchableContext bc, List<User> scope) {
   
    
    //}//execute()
    
    global void finish (Database.BatchableContext bc) {
    
    
    }//finish()
    
    
}