/*=====================================================================
 * 
 * Name: ContactCreateHelper_Test
 * Description: Test class for ContactCreateHelper
 * Created Date: Feb 12th, 2016
 * Created By: Sadar Yacob
 * 
 * Date Modified            Modified By           Description of the update
 * Feb 12th, 2016           Sadar Yacob           Initial Creation 
 * Feb 29th, 2016           Paul Kissick          Case 01877598: Fixed tests for url params in different positions
 * Jun 16th, 2016           Paul Kissick          Case 02024883: Fixing accountshare problem, and updated to v37.0
 * =====================================================================*/
@isTest
private class ContactCreateHelper_Test {

  //=========================================================================
  // Method to verify the functionality of ContactCreateHelper
  //=========================================================================
  static testMethod void baseTest() {
   
    // fetch the records 
    Account acc = [SELECT Id,Name FROM Account WHERE Name = 'testAccount'];
    Contact con = [SELECT Id,Name FROM Contact WHERE LastName = 'testCon'];

    //Test1: 
    //get recordtypeids
    Id rtMasterId = [
      SELECT Id, Name, sObjectType 
      FROM RecordType 
      WHERE Name = 'Master' 
      AND IsActive = true
      AND sObjectType = 'Contact'
    ].Id;
 
     // call the method for ContactCreate from Account
    String url = ContactCreateHelper.createContact('Master', acc.Id, acc.Name);
    system.debug(url);
    // Verify that the url created has the proper fields are added in the url
    // String urlResult ='/'+DescribeUtility.getPrefix('Contact')+'/e?RecordType='+RecordTypeMaster+'&con4='+EncodingUtil.urlEncode(acc.Name,'UTF-8') +'&accid='+ acc.ID +'&retURL=%2F'+acc.Id ; 
    system.assertEquals(true, url.containsIgnoreCase('RecordType='+rtMasterId));
    system.assertEquals(true, url.containsIgnoreCase('con4='+EncodingUtil.urlEncode(acc.Name,'UTF-8')));
    system.assertEquals(true, url.containsIgnoreCase('accid='+acc.ID));
    system.assertEquals(true, url.containsIgnoreCase('retURL='+EncodingUtil.urlEncode('/'+acc.Id,'UTF-8')));
    // system.assert( url.contains(urlResult));
 
    //Test2:
    Id rtSerasaId = [
      SELECT Id, Name, sObjectType 
      FROM RecordType 
      WHERE Name = 'LATAM Serasa' 
      AND IsActive = true
      AND sObjectType = 'Contact'
    ].Id;

    // call the method for ContactCreate from Account as a LATAM Serasa User
    url = ContactCreateHelper.createContact('LATAM Serasa', acc.Id, acc.Name);

    // Verify that the url created has the proper fields are added in the url
    // urlResult ='/'+DescribeUtility.getPrefix('Contact')+'/e?RecordType='+RecordTypeSerasa +'&con4='+EncodingUtil.urlEncode(acc.Name,'UTF-8')+'&accid='+ acc.ID +'&retURL=%2F'+acc.Id ;
    system.assertEquals(true, url.containsIgnoreCase('RecordType=' + rtSerasaId));
    system.assertEquals(true, url.containsIgnoreCase('con4=' + EncodingUtil.urlEncode(acc.Name,'UTF-8')));
    system.assertEquals(true, url.containsIgnoreCase('accid=' + acc.ID));
    system.assertEquals(true, url.containsIgnoreCase('retURL=' + EncodingUtil.urlEncode('/'+acc.Id,'UTF-8'))); 
    
     //Test3:
     Account accSerasa = [SELECT Id, Name FROM Account WHERE Name = 'testAccountSerasa'];

    // call the method for ContactCreate from Account as a LATAM Serasa User who is not part of the Account Team and hence 
    //should get an error message
    url = ContactCreateHelper.createContact('LATAM Serasa', accSerasa.Id, accSerasa.Name);
    String urlResult = 'User is not on Account Team';
    
    system.assertEquals(url, urlResult);
    system.assert( url.contains(urlResult));
  
  }

  @testSetup
  static void createData () {
    Account acc = Test_Utils.createAccount();
    acc.Name = 'testAccount';
    insert acc;
  
    acc = Test_Utils.createAccount();
    acc.Name = 'testAccountSerasa';
    insert acc;
    
    User testUser= Test_Utils.createuser(Constants.PROFILE_EXP_SALES_EXEC);
    testUser.FirstName ='TestSE';
    testUser.LastName ='Executive';
    testUser.EmployeeNumber = '1234';
    insert testUser;

    User testUserSerasa= Test_Utils.createuser('Experian Serasa Sales Executive');
    testUserSerasa.FirstName ='TestSERASA';
    testUserSerasa.LastName ='Executive';
    testUserSerasa.EmployeeNumber = '12345';
    insert testUserSerasa;
    
   
    AccountTeamMember atm  = Test_Utils.createAccountTeamMembers (acc.ID,testUser.ID,true);
    //AccountShare attshare = Test_Utils.createAccountShare(acc.ID,testUser.ID,true);
    AccountTeamMember atm1  = Test_Utils.createAccountTeamMembers (acc.id,testUserSerasa.id,true);
    //AccountShare attshare1 = Test_Utils.createAccountShare(acc.ID,testUserSerasa.ID,true);


    Contact con = Test_Utils.createContact(acc.Id);
    con.Contact_Role__c = 'Primary';
    con.LastName = 'testCon';
    insert con;
  
  }

}