/*=============================================================================
 * Experian
 * Name: BatchOpportunityWinProbBuild_Test
 * Description: 
 * Created Date: 13 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 13th July 2016(QA) Paul Kissick          CRM2:W-005423 - Opportunity Weightings should match owners closing ability
 =============================================================================*/

@isTest
private class BatchOpportunityWinProbBuild_Test {

  static String testUserEmail1 = 'askljfhsaflk898@experian.com';

  static testMethod void testBatch() {
    
    User tstUser1 = [SELECT Id FROM User WHERE Email = :testUserEmail1];
    
    system.assertEquals(0, [SELECT COUNT() FROM Opportunity_Win_Probability__c WHERE SetupOwnerId = :tstUser1.Id]);
    
    Test.startTest();
    
    BatchOpportunityWinProbBuild b = new BatchOpportunityWinProbBuild();
    b.oppThreshold = 1;
    database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM Opportunity_Win_Probability__c WHERE SetupOwnerId = :tstUser1.Id]);
    
    Opportunity_Win_Probability__c checkOwp = Opportunity_Win_Probability__c.getValues(tstUser1.Id);
    
    Opportunity_Win_Probability__c checkRunOwp = Opportunity_Win_Probability__c.getValues(UserInfo.getUserId());
    
    system.assertEquals(16.00, checkOwp.Stage_3__c); // 1 / 6
    system.assertEquals(20.00, checkOwp.Stage_4__c); // 1 / 5
    system.assertEquals(25.00, checkOwp.Stage_5__c); // 1 / 4 
    system.assertEquals(33.00, checkOwp.Stage_6__c); // 1 / 3
    system.assertEquals(33.00, checkOwp.Renewal_Stage_3__c);  // 1 / 3
    system.assertEquals(33.00, checkOwp.Renewal_Stage_4__c);  // 1 / 3
    system.assertEquals(33.00, checkOwp.Renewal_Stage_5__c);  // 1 / 3
    system.assertEquals(50.00, checkOwp.Renewal_Stage_6__c);  // 1 / 2
    
    system.assertEquals(100.00, checkRunOwp.Stage_3__c); // 1 / 1
    system.assertEquals(100.00, checkRunOwp.Stage_4__c); // 1 / 1
    system.assertEquals(100.00, checkRunOwp.Stage_5__c); // 1 / 1 
    system.assertEquals(100.00, checkRunOwp.Stage_6__c); // 1 / 1
    system.assertEquals(100.00, checkRunOwp.Renewal_Stage_3__c);  // 1 / 1
    system.assertEquals(100.00, checkRunOwp.Renewal_Stage_4__c);  // 1 / 1
    system.assertEquals(100.00, checkRunOwp.Renewal_Stage_5__c);  // 1 / 1
    system.assertEquals(100.00, checkRunOwp.Renewal_Stage_6__c);  // 1 / 1
    
  }
  
  static testMethod void testSchedule() {
    
    Test.startTest();
    
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = system.schedule('BatchOpportunityWinProbBuild-Test'+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduleOpportunityWinProbBuild());
          
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];     
    
    Test.stopTest();

    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled 
    system.assertEquals(0, ct.TimesTriggered);
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    
    IsDataAdmin__c myIda = new IsDataAdmin__c(IsDataAdmin__c = true, SetupOwnerId = UserInfo.getUserId());
    insert myIda;
    
    Opportunity_Win_Probability__c owpDefs = new Opportunity_Win_Probability__c(
      SetupOwnerId = UserInfo.getOrganizationId(),
      Renewal_Stage_3__c = 20,
      Renewal_Stage_4__c = 40,
      Renewal_Stage_5__c = 60,
      Renewal_Stage_6__c = 80,
      Stage_3__c = 20,
      Stage_4__c = 40,
      Stage_5__c = 60,
      Stage_6__c = 80
    );
    insert owpDefs;
    
    Opportunity_Win_Probability__c owpRunningUser = new Opportunity_Win_Probability__c(
      SetupOwnerId = UserInfo.getUserId(),
      Renewal_Stage_3__c = 60,
      Renewal_Stage_4__c = 70,
      Renewal_Stage_5__c = 80,
      Renewal_Stage_6__c = 90,
      Stage_3__c = 60,
      Stage_4__c = 70,
      Stage_5__c = 80,
      Stage_6__c = 90,
      NFE_Stage_3__c = 60,
      NFE_Stage_4__c = 70,
      NFE_Stage_5__c = 80,
      NFE_Stage_6__c = 90
    );
    insert owpRunningUser;
    
    User tstUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser1.Email = testUserEmail1;
    insert tstUser1;
    
    Account a1 = Test_Utils.insertAccount();
    
    // 6 Opps of type New from New, each closed at a different stage
    // only 1 is won, all the rest (5 others) will be lost
    
    // Won Opp
    Opportunity opp1a = Test_Utils.createOpportunity(a1.Id);
    opp1a.OwnerId = tstUser1.Id;
    opp1a.Type = Constants.OPPTY_NEW_FROM_NEW;
    opp1a.Reached_Stage_3__c = true;
    opp1a.Reached_Stage_4__c = true;
    opp1a.Reached_Stage_5__c = true;
    opp1a.Reached_Stage_6__c = true;
    opp1a.Reached_Stage_7__c = true;
    opp1a.StageName = Constants.OPPTY_STAGE_7;
    
    // Lost at stage 3
    Opportunity opp1b = Test_Utils.createOpportunity(a1.Id);
    opp1b.OwnerId = tstUser1.Id;
    opp1b.Type = Constants.OPPTY_NEW_FROM_NEW;
    opp1b.Reached_Stage_3__c = true;
    opp1b.Reached_Stage_4__c = false;
    opp1b.Reached_Stage_5__c = false;
    opp1b.Reached_Stage_6__c = false;
    opp1b.Reached_Stage_7__c = false;
    opp1b.StageName = Constants.OPPTY_CLOSED_LOST;
    
    // Lost at stage 4
    Opportunity opp1c = Test_Utils.createOpportunity(a1.Id);
    opp1c.OwnerId = tstUser1.Id;
    opp1c.Type = Constants.OPPTY_NEW_FROM_NEW;
    opp1c.Reached_Stage_3__c = true;
    opp1c.Reached_Stage_4__c = true;
    opp1c.Reached_Stage_5__c = false;
    opp1c.Reached_Stage_6__c = false;
    opp1c.Reached_Stage_7__c = false;
    opp1c.StageName = Constants.OPPTY_CLOSED_LOST;
    
    // Lost at stage 5
    Opportunity opp1d = Test_Utils.createOpportunity(a1.Id);
    opp1d.OwnerId = tstUser1.Id;
    opp1d.Type = Constants.OPPTY_NEW_FROM_NEW;
    opp1d.Reached_Stage_3__c = true;
    opp1d.Reached_Stage_4__c = true;
    opp1d.Reached_Stage_5__c = true;
    opp1d.Reached_Stage_6__c = false;
    opp1d.Reached_Stage_7__c = false;
    opp1d.StageName = Constants.OPPTY_CLOSED_LOST;
    
    // Lost at stage 6
    Opportunity opp1e = Test_Utils.createOpportunity(a1.Id);
    opp1e.OwnerId = tstUser1.Id;
    opp1e.Type = Constants.OPPTY_NEW_FROM_NEW;
    opp1e.Reached_Stage_3__c = true;
    opp1e.Reached_Stage_4__c = true;
    opp1e.Reached_Stage_5__c = true;
    opp1e.Reached_Stage_6__c = true;
    opp1e.Reached_Stage_7__c = false;
    opp1e.StageName = Constants.OPPTY_CLOSED_LOST;
    
    // Lost at stage 6
    Opportunity opp1f = Test_Utils.createOpportunity(a1.Id);
    opp1f.OwnerId = tstUser1.Id;
    opp1f.Type = Constants.OPPTY_NEW_FROM_NEW;
    opp1f.Reached_Stage_3__c = true;
    opp1f.Reached_Stage_4__c = true;
    opp1f.Reached_Stage_5__c = true;
    opp1f.Reached_Stage_6__c = true;
    opp1f.Reached_Stage_7__c = false;
    opp1f.StageName = Constants.OPPTY_CLOSED_LOST;
    
    // 3 Opps of type Renewal, each closed at a different stage
    // only 1 is won, all the rest (2 others) will be lost
    
    // 1 Won
    Opportunity opp2a = Test_Utils.createOpportunity(a1.Id);
    opp2a.OwnerId = tstUser1.Id;
    opp2a.Type = Constants.OPPTY_TYPE_RENEWAL;
    opp2a.Reached_Stage_3__c = true;
    opp2a.Reached_Stage_4__c = true;
    opp2a.Reached_Stage_5__c = true;
    opp2a.Reached_Stage_6__c = true;
    opp2a.Reached_Stage_7__c = true;
    opp2a.StageName = Constants.OPPTY_STAGE_7;
    
    // Lost at stage 5
    Opportunity opp2b = Test_Utils.createOpportunity(a1.Id);
    opp2b.OwnerId = tstUser1.Id;
    opp2b.Type = Constants.OPPTY_TYPE_RENEWAL;
    opp2b.Reached_Stage_3__c = true;
    opp2b.Reached_Stage_4__c = true;
    opp2b.Reached_Stage_5__c = true;
    opp2b.Reached_Stage_6__c = false;
    opp2b.Reached_Stage_7__c = false;
    opp2b.StageName = Constants.OPPTY_CLOSED_LOST;
    
    // Lost at stage 6
    Opportunity opp2c = Test_Utils.createOpportunity(a1.Id);
    opp2c.OwnerId = tstUser1.Id;
    opp2c.Type = Constants.OPPTY_TYPE_RENEWAL;
    opp2c.Reached_Stage_3__c = true;
    opp2c.Reached_Stage_4__c = true;
    opp2c.Reached_Stage_5__c = true;
    opp2c.Reached_Stage_6__c = true;
    opp2c.Reached_Stage_7__c = false;
    opp2c.StageName = Constants.OPPTY_CLOSED_LOST;
    
    // 2 Opps for running user, to test existing values
    // 1 won, no lost of type new from new
    Opportunity opp3a = Test_Utils.createOpportunity(a1.Id);
    opp3a.OwnerId = UserInfo.getUserId();
    opp3a.Type = Constants.OPPTY_NEW_FROM_NEW;
    opp3a.Reached_Stage_3__c = true;
    opp3a.Reached_Stage_4__c = true;
    opp3a.Reached_Stage_5__c = true;
    opp3a.Reached_Stage_6__c = true;
    opp3a.Reached_Stage_7__c = true;
    opp3a.StageName = Constants.OPPTY_STAGE_7;
    
    // 1 won, no lost of type renewal
    Opportunity opp3b = Test_Utils.createOpportunity(a1.Id);
    opp3b.OwnerId = UserInfo.getUserId();
    opp3b.Type = Constants.OPPTY_TYPE_RENEWAL;
    opp3b.Reached_Stage_3__c = true;
    opp3b.Reached_Stage_4__c = true;
    opp3b.Reached_Stage_5__c = true;
    opp3b.Reached_Stage_6__c = true;
    opp3b.Reached_Stage_7__c = true;
    opp3b.StageName = Constants.OPPTY_STAGE_7;
    
    // 1 won, no lost of type renewal
    Opportunity opp3c = Test_Utils.createOpportunity(a1.Id);
    opp3c.OwnerId = UserInfo.getUserId();
    opp3c.Type = Constants.OPPTY_NEW_FROM_EXISTING;
    opp3c.Reached_Stage_3__c = true;
    opp3c.Reached_Stage_4__c = true;
    opp3c.Reached_Stage_5__c = true;
    opp3c.Reached_Stage_6__c = true;
    opp3c.Reached_Stage_7__c = true;
    opp3c.StageName = Constants.OPPTY_STAGE_7;
    
    insert new Opportunity[]{opp1a,opp1b, opp1c, opp1d, opp1e, opp1f, opp2a, opp2b, opp2c, opp3a, opp3b, opp3c};
    
    delete myIda;
    
  }
  
}