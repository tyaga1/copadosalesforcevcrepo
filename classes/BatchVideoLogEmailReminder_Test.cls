/**=============================================================================
 * Experian plc
 * Name: BatchVideoLogEmailReminder_Test
 * Test Class for: BatchVideoLogEmailReminder 
 * Description: Case #02069865 Custom object for Training management console
 * Created Date: March 23rd, 2017
 * Created By: Sanket Vaidya
 * 
 * Date Modified        Modified By                  Description of the update
 * 
 =============================================================================*/
@isTest
public class BatchVideoLogEmailReminder_Test
{
	@isTest (SeeAllData=true) //To make custom settings available in batch file.
    public static void testBatchVideoLogEmailReminder()
    {                
        List<Video_Log__c> listVideoLogs = new List<Video_Log__c>();
        
        for(Integer i=0; i < 10; i++)
        {
            Date reviewDate;
            
            if(Math.mod(i,2) == 0)
            {
                reviewDate = Date.today();
            }
            else
            {
                reviewDate = Date.today().addDays(2);
            }
            
            Video_Log__c vl = new Video_Log__c(Name = 'Video Log ' + i, Video_Review_Date__c = Date.today());
            listVideoLogs.add(vl);
        }
        
        INSERT listVideoLogs;
        
        database.executebatch(new BatchVideoLogEmailReminder());
    }    	
}