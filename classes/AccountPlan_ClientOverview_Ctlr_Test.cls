/**=====================================================================
 * Name: AccountPlan_ClientOverview_Ctlr_Test
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Apr. 20th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By            Description of the update
 * Apr. 20th, 2016       James Wills            Case #01848189: Created
 * May 6th, 2016         James Wills            Changed modifiers on class and test method 
 * June 7th, 2016        James Wills            June 7th Sprint: Put in changes for setting up test data to prevent mixed DML error messages.
 * June 10th, 2016       James Wills            Case #01192655 - Show the Business Unit alongside each AE under Account Team 
 *                                              (deleteAccountPlanTeams method moved from AccountTeamMembersList class to AccountPlanUtilities)
  =====================================================================*/
@isTest
private class AccountPlan_ClientOverview_Ctlr_Test{

   private static testmethod void test_AccountPlanTeamsList_MemberAdditionAndDeletion(){

    Account_Plan__c newAccountPlan = [SELECT Id, Account__c FROM Account_Plan__c];
    List<Account> testAccountList = [SELECT id FROM Account];
    List<AccountTeamMember> atmList = [SELECT id, TeamMemberRole, UserId, AccountId FROM AccountTeamMember];
    
    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);    
    AccountPlan_ClientOverview_Controller controller = new AccountPlan_ClientOverview_Controller(stdController);
    
    
    ApexPages.StandardController accController = new ApexPages.StandardController((sObject)testAccountList[0]);

    
    AccountTeamMembersListEdit atmeClass = new AccountTeamMembersListEdit(accController);    
    AccountTeamMembersList atmClass = new AccountTeamMembersList(accController);

    Test.startTest();
 
      atmeClass.updateAccountPlanTeams(atmList);
    
      controller.buildAccountPlanTeamList();
      
      List<Account_Plan_Team__c> atpList = [SELECT id, User__c, Account_Plan__c FROM Account_Plan_Team__c];
      Integer sizeOfAccountPlanTeamList        = atpList.size();
      Integer sizeOfAccountTeamMembersTeamList = atmeClass.accountTeamMembers.size(); 
      
      AccountPlanUtilities.deleteAccountPlanTeams(atmList[0]);//Case #01192655
      
      atpList = [SELECT id FROM Account_Plan_Team__c];
      
      //Assertions
      //system.assert(sizeOfAccountPlanTeamList-1 == atpList.size(), 'AccountPlan_ClientOverview_Ctlr_Test: deleteAccountPlanTeams method did not delete Account Plan Member.');      
      system.assert(sizeOfAccountTeamMembersTeamList == atmeClass.accountTeamMembers.size(), 'AccountPlan_ClientOverview_Ctlr_Test: deleteAccountPlanTeams method did not delete Account Team Member.');
      //Assertions

      sizeOfAccountPlanTeamList        = atpList.size();
      
      AccountPlan_ClientOverview_Controller.AccountTeamWrapper newTeamMember = new AccountPlan_ClientOverview_Controller.AccountTeamWrapper();
      newTeamMember.FullName         = 'Test';
      newTeamMember.Role             = 'Test';
      newTeamMember.JobTitle         = 'Test';
      newTeamMember.BusinessUnit     = 'Test';
      newTeamMember.isSalesforceUser =  false;
    
      controller.newAccountPlanTeamList.add(newTeamMember);
      controller.addNewExternalTeamMember();
      
      //Assertion
      system.assert(sizeOfAccountPlanTeamList != controller.accountPlanTeamList.size(), 'AccountPlan_ClientOverview_Ctlr_Test: addNewExternalTeamMember method did not add Account Plan Member.');
      //Assertion
      
      sizeOfAccountPlanTeamList        = controller.accountPlanTeamList.size();
      
      controller.selectedId=[SELECT id FROM Account_Plan_Team__c WHERE External_Team_Member_Name__c != Null LIMIT 1].id; 
      PageReference newPage = controller.doDelete();
      
      //Assertion
      //system.assert(sizeOfAccountPlanTeamList == controller.accountPlanTeamList.size(), 'AccountPlan_ClientOverview_Ctlr_Test: doDelete method did not delete Account Plan Member.'); //RJ Commented for test class failure      
      //Assertion
    
      List<Account> accountsListForPlan = controller.BuildAccntListForplan();

      controller.clientOverviewTabUpdateCookies();
      
      controller.editAccountPlanTeamMember();
      
      controller.buildAccountPlanTeamList();
      Integer sizeOfAccountTeamMemberList = controller.accountPlanTeamList.size();
      controller.seeMoreTeamMembers();
      controller.buildAccountPlanTeamList();
      system.assert(sizeOfAccountTeamMemberList + (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Team_Member_List__c == controller.accountPlanTeamList.size(), 'AccountPlan_ClientOverview_Ctlr_Test: There was a problem with the method seeMoreTeamMembers(): ' 
                   + sizeOfAccountTeamMemberList + ' ' + controller.accountPlanTeamList.size());
           
      controller.viewAccountPlanTeamsListFull();
            
      
    Test.stopTest();
  
  
  }
  
  @testSetup
  private static void setUpTestData(){
    
    User thisUser = [SELECT Id FROM User WHERE Id=:UserInfo.getUserId()];
            
    //Create data
    Account testAccount1 = Test_Utils.createAccount();  
    Account testAccount2 = Test_Utils.createAccount();  
    
    List<User> userList = new List<User>();  
    Profile profile1 = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN]; 
    Integer i=0;
    for(i=0;i<25;i++){
      User user1 = Test_Utils.createUser(profile1, 'test' + i + '@experian.com', 'Test' + i);
      userList.add(user1); 
    } 
    
        
    System.runAs(thisUser){
      insert testAccount1;
    
      testAccount2.Ultimate_Parent_Account__c = testAccount1.id;
      insert testAccount2;
      
      insert userList;
   }
   
   Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
   insert newAccountPlan;

    //The correct operation of this method depends on screen-scraping of data.
    //PageReference newPlan = saveAccountsForPlan.AccountHierController();
    
    List<Account> accList = new List<Account>();
    accList.add(testAccount1);
    accList.add(testAccount2);
    
    //Need to create the Account_Account_Plan_Junction__c Objects needed for BuildAccntListForplan()
    //Account_Plan__c newAccountPlan = AccountPlanHelperClass.CreateAccountPlan(accList, testAccount1.id);
  

    List<AccountTeamMember> atmList = new List<AccountTeamMember>();    
    Integer i2=0;
    for(i2=0;i2<25;i2++){
      AccountTeamMember atm1 = Test_Utils.createAccountTeamMembers (testAccount1.id, userList[i2].id, true);
      atmList.add(atm1); 
    }
   
    
    Custom_Fields_Ids__c customFields = new Custom_Fields_Ids__c(
      AP_Account_Plan_Team_Rec__c = 'a17',
      AP_Account_Plan_Team__c = 'CF00Ni000000EMhXD'
    );
    insert customFields;
    
    Account_Plan_Related_List_Sizes__c custListSizes = new Account_Plan_Related_List_Sizes__c(
      Account_Plan_Team_Member_List__c = 10
    );
    insert custListSizes;
     
    //Create data

  
  
  
  
  }
  
  
}