/**=====================================================================
 * Appirio, Inc
 * Name: NewRevenueAdjustmentExtension
 * Description: T-425827
 * 
 * Note: A lot of this code was copy/pasted from ProductSearch. We may want to look at improving it.
 * 
 * Created Date: Aug 12th, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Sep 29th, 2015     Arpita Bose                  I-177358
 * Feb 9th, 2016      Paul Kissick                 Case 01185249: Complete replacement to support search functionality. 
*  =====================================================================*/
global with sharing class NewRevenueAdjustmentExtension {

  public class ProductWrapper {
    public String productName {get;set;}
    public String productGroup {get;set;}
    public String productFamily {get;set;}
    public String globalBusinessLine {get;set;}
    public String productMasterName {get;set;}
    public String businessLine {get;set;}
  }

  private Revenue_Adjustment__c revenueAdjustment;
  
  private ApexPages.StandardSetController setCon;
  
  public ApexPages.StandardController con;

  public String sortField {get{if (sortField == null) sortField = 'Name ASC'; return sortField;}set;}
  // public Boolean filterFollowedProducts {get;set;}
  public ProductWrapper holdingProduct {get;set;}
  
  public String countryFilter {get{if (countryFilter == null) countryFilter = ''; return countryFilter;} set;}
  public String regionFilter {get{if (regionFilter == null) regionFilter = ''; return regionFilter;} set;}
  public Integer newPageSize {get{if (newPageSize == null) newPageSize = 10; return newPageSize;}set;}
  
  private User currentUser {get;set;}
  
  public List<SelectOption> countriesList {get{if (countriesList == null) countriesList = new List<SelectOption>(); return countriesList;} set;}
  public List<SelectOption> regionsList {get{if (regionsList == null) regionsList = new List<SelectOption>(); return regionsList;} set;}
  public List<SelectOption> familyList {get{if (familyList == null) familyList = new List<SelectOption>(); return familyList;} set;}
  public List<SelectOption> gblList {get{if (gblList == null) gblList = new List<SelectOption>(); return gblList;} set;}
  public List<SelectOption> businessLineList {get{if (businessLineList == null) businessLineList = new List<SelectOption>(); return businessLineList;} set;}
 
  public Integer getResultSize() { try {return setCon.getResultSize(); } catch (Exception e) {} return 0;}
    
  public Integer getPageSize() { try {return setCon.getPageSize(); } catch (Exception e) {} return 0;}
    
  public Integer getPageNumber() { try {return setCon.getPageNumber(); } catch (Exception e) {} return 0;}
    
  public Integer getTotalPages() { 
    try {return (Integer)Math.ceil((Double)setCon.getResultSize() / (Double)setCon.getPageSize()); }
    catch (Exception e) { }
    return 0;
  }
  
  public Map<Id,SelectableProduct> selectedProducts {get{if (selectedProducts == null) selectedProducts = new Map<Id,SelectableProduct>(); return selectedProducts;}set;}
  
  public List<SelectableProduct> getRecords() {
    List<SelectableProduct> result = new List<SelectableProduct>();
    try {
      for(Product2 p : (List<Product2>)setCon.getRecords()){
        SelectableProduct sp = this.selectedProducts.get(p.Id);
        if(sp != null) {
          result.add(sp);
        }
        else {
          result.add(new SelectableProduct(p,false,this));
        }
      }
    }
    catch (Exception e) {
      system.debug(e.getMessage());
    }
    return result;
  }
  
  public class SelectableProduct implements Comparable{
    public Product2 product {get;Set;}
    public Boolean selected {get;Set;}
    public OpportunityLineItem lineItem {get;Set;}
    public NewRevenueAdjustmentExtension controller {get;Set;}
      
    public SelectableProduct(Product2 p, Boolean selected, NewRevenueAdjustmentExtension controller){
      this.product = p;
      this.controller = controller;
      this.selected = selected;
      this.lineItem = new OpportunityLineItem();
    }
    
    /* adds or removes a selected item */
    public void selectItem() {
      SelectableProduct p  = this.controller.selectedProducts.get(this.product.Id);
      if (this.selected) {
        if (p == null) {
          this.controller.selectedProducts.put(this.product.Id,this);
        }
        else {
          //nothing to do: item is already selected
        }
      }
      else {
        this.controller.selectedProducts.remove(this.product.Id);
      }
    }

    /*
      Removes an item from the selected list
    */
    public void removeSelected() {
      this.selected = false;
      this.selectItem();
    }
      
    /* Comparable interace */
    public Integer compareTo (Object compareTo) {
      if ((compareTo instanceof SelectableProduct) == false) return 1;
      SelectableProduct compareToSP = (SelectableProduct)compareTo;
      return this.product.Name.compareTo(compareToSP.product.Name);       
    }
  }

  // Constructor
  public NewRevenueAdjustmentExtension(ApexPages.StandardController stdCon) {
    con = stdCon;
    revenueAdjustment = (Revenue_Adjustment__c)con.getRecord();
    
    currentUser = [
      SELECT Id, Region__c, Country__c, Global_Business_Line__c
      FROM User
      WHERE Id = :UserInfo.getUserId()
    ];
    
    holdingProduct = new ProductWrapper();
    holdingProduct.globalBusinessLine = currentUser.Global_Business_Line__c;

    newPageSize = 10; 
    // filterFollowedProducts = false;
    sortField = 'Name ASC';
    // isSecondStep = false;
    
    for(Region__c reg : [SELECT Id, Name 
                         FROM Region__c 
                         ORDER BY Name]) {
      regionsList.add(new SelectOption(reg.Id, reg.Name));
      if(reg.Name.equals(currentUser.Region__c)) {
        regionFilter = reg.Id;
      }
    }
    
    for(Country__c cnt : [SELECT Id, Name
                          FROM Country__c
                          ORDER BY Name]) {
      countriesList.add(new SelectOption(cnt.Id,cnt.Name));
      if (cnt.Name.equals(currentUser.Country__c)) {
        countryFilter = cnt.Id;
      }
    }
    try {
      for(Schema.PicklistEntry familyPicklist : Product2.sObjectType.getDescribe().fields.getMap().get('Family').getDescribe().getPicklistValues()) {
        familyList.add(new SelectOption(familyPicklist.getValue(), familyPicklist.getLabel()));
      }
      familyList.sort();
      
      for(Schema.PicklistEntry gblPicklist : Product2.sObjectType.getDescribe().fields.getMap().get('Global_Business_Line__c').getDescribe().getPicklistValues()) {
        gblList.add(new SelectOption(gblPicklist.getValue(), gblPicklist.getLabel()));
      }
      gblList.sort();
      
      for(Schema.PicklistEntry blPicklist : Product2.sObjectType.getDescribe().fields.getMap().get('Business_Line__c').getDescribe().getPicklistValues()) {
        businessLineList.add(new SelectOption(blPicklist.getValue(), blPicklist.getLabel()));
      }
      businessLineList.sort();
      
      loadData();
    }
    catch (Exception e) {
      system.debug(e);
    }
  }
  
  public void loadData(){
    //sorting
    String orderBy = sortField;
    if(orderBy.endsWith(' ASC')) {
      orderBy += ' NULLS LAST';
    }
    else { 
      orderBy += ' NULLS FIRST';
    }
      
    String query = 'SELECT Id, Name, Product_Desc__c, IsActive, Types_of_Sale__c, DE_Product_Name__c FROM Product2 ';
    List<String> andConditions = new List<String>();
    andConditions.add(' IsActive = true ');

    //filter name
    if (String.isNotBlank(holdingProduct.ProductName)) {
      String nameString = String.escapeSingleQuotes(holdingProduct.ProductName)+'%';
      andConditions.add(' Name LIKE :nameString ');
    }
      
    //filter group
    if (String.isNotBlank(holdingProduct.productGroup)) {
      String prodGrpString = String.escapeSingleQuotes(holdingProduct.productGroup)+'%';
      andConditions.add(' Product_Group__c LIKE :prodGrpString ');
    }
      
    //filter family
    if (String.isNotBlank(holdingProduct.productFamily)) {
      String famString = String.escapeSingleQuotes(holdingProduct.productFamily);
      andConditions.add(' Family = :famString ');
    }
      
    //filter Product Master Name added 06/12/14
    if (String.isNotBlank(holdingProduct.productMasterName)) {
      String prdMastName = '%'+String.escapeSingleQuotes(holdingProduct.productMasterName)+'%';
      andConditions.add(' DE_Product_Name__c LIKE :prdMastName ');
    }
      
    //filter country
    List<Id> noCountryOrRegionList;
    List<Id> countryAndRegionList;
    if (String.isNotBlank(countryFilter) && String.isNotBlank(regionFilter)) {
      //checks exact  country/region matching
      Map<Id, Product2> prodWithCountriesAndRegions = new Map<ID,Product2>([
        SELECT Id, Name
        FROM Product2 
        WHERE Id IN (
          SELECT Product__c 
          FROM Product_Country__c
          WHERE Country__c = :countryFilter
        )
      ]);
      countryAndRegionList = new List<Id>(prodWithCountriesAndRegions.keySet());
        
      //this is the list of all products that matches both selected country and region
      prodWithCountriesAndRegions = new Map<Id,Product2>([
        SELECT Id, Name 
        FROM Product2 
        WHERE Id IN (
          SELECT Product__c 
          FROM Product_Region__c 
          WHERE Region__c = :regionFilter
        )
        AND Id IN :countryAndRegionList
      ]);
        
      countryAndRegionList = new List<ID>(prodWithCountriesAndRegions.keySet()); 
       
      String cond = '( (Id IN :countryAndRegionList)';
      system.debug('countryAndRegionList = ' + countryAndRegionList);
      //checks no country/region products
      prodWithCountriesAndRegions = new Map<ID,Product2>([
        SELECT Id, Name 
        FROM Product2 
        WHERE Id IN (
          SELECT Product__c 
          FROM Product_Country__c
        )
      ]);
        
      noCountryOrRegionList = new List<ID>(prodWithCountriesAndRegions.keySet());
        
      //this is the list of Product2 sobjects with at least one Country/Region junction record (we will negate it to get all the products
      //that don't have either one of them or both)
        
      noCountryOrRegionList = new List<Id>();
        
      for (Product2 prod : [SELECT Id, Name 
                            FROM Product2 
                            WHERE Id IN (
                              SELECT Product__c 
                              FROM Product_Region__c
                            )
                            AND Id IN :noCountryOrRegionList]) {
        prodWithCountriesAndRegions.put(prod.ID, prod);
        noCountryOrRegionList.add(prod.ID);   
      }
        
      if (noCountryOrRegionList.size() > 0) {
        cond+= 'OR (NOT(Id IN :noCountryOrRegionList))';
      }
      cond+=')';
      andConditions.add(cond);
    }
      
    //filter business unit
    if (String.isNotBlank(holdingProduct.globalBusinessLine)) {
      String gblString = String.escapeSingleQuotes(holdingProduct.globalBusinessLine);
      andConditions.add(' Global_Business_Line__c = :gblString ');
    }
      
    //filter business line
    if (String.isNotBlank(holdingProduct.businessLine)) {
      String blString = String.escapeSingleQuotes(holdingProduct.businessLine);
      andConditions.add(' Business_Line__c = :blString ');
    }
      
    //aggregate conditions
    if (andConditions.size() > 0) {
      query += 'WHERE ('+String.join(andConditions,') AND (')+')';
    }
      
    query+=' ORDER BY '+orderBy;
      
    system.debug('\n[NewRevenueAdjustmentExtension : loadData] : QUERY IS: \n '+ query);
    //this is for test pourposes (query don't compile but still want to see what's inside)
    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,query));
    //query = 'SELECT Id, Name, Description, IsActive FROM Product2';
    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query));
    setCon.setPageNumber(1);
    setCon.setPageSize(newPageSize);
   
  }
    
  public PageReference goToFirstPage() {
    setCon.first();
    return null;
  }
    
  public PageReference goToLastPage() {
    setCon.last();
    return null;
  }
    
  public PageReference goToNextPage() {
    setCon.next();
    return null;
  }
    
  public PageReference goToNext2Page() {
    goToNextPage();
    goToNextPage();
    return null;
  }
    
  public PageReference goToPrevPage() {
    setCon.previous();
    return null;
  }
    
  public PageReference goToPrev2Page() {
    goToPrevPage();
    goToPrevPage();
    return null;
  }

  public PageReference sortRecordset() {
    Integer currentPage = getPageNumber();
    loadData();
    setCon.setPageNumber(currentPage);
    return null;
  }
  
  @RemoteAction
  global static List<AggregateResult> searchFilters(String searchTerm, String searchField) {
    return Database.query(
      'SELECT '+searchField+' FROM Product2 '+' WHERE '+searchField+' LIKE \'%' + String.escapeSingleQuotes(searchTerm) + '%\' '
              +' GROUP BY '+searchField+' LIMIT 100'
    );
  }
  
  public PageReference save() {
    if (String.isBlank(revenueAdjustment.Business_Line__c) || String.isBlank(revenueAdjustment.Business_Unit__c)) {
      if (String.isBlank(revenueAdjustment.Business_Line__c)) {
        revenueAdjustment.Business_Line__c.addError('Missing...');
      }
      if (String.isBlank(revenueAdjustment.Business_Unit__c)) {
        revenueAdjustment.Business_Unit__c.addError('Missing...');
      }
      return null;
    }
    if (selectedProducts.size() == 1) {
      for(Id prodId : selectedProducts.keySet()) {
        revenueAdjustment.Product__c = prodId;
      }
      try {
        return con.save();
      }
      catch (Exception e) {
        system.debug(e.getMessage());
      }
    }
    
    return null;
  }
  
  public Boolean getSaveEnabled() {
    return (selectedProducts.size() == 1);
  }
  
}