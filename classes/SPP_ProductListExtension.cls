/**=====================================================================
 * Experian
 * Name: SPP_ProductListExtension
 * Description: Returns list of opportunity products associated to one Sales Planning Process record
 * Created Date: August 9th, 2017
 * Created By: Mauricio Murillo
 *
 * Date Modified      Modified By                  Description of the update
 *
 */
public class SPP_ProductListExtension
{
    private final Sales_Planning_Process__c spp; 
    
    public SPP_ProductListExtension(ApexPages.StandardController controller)
    {
        this.spp = [Select id, opportunity__c from Sales_Planning_Process__c where id = :controller.getRecord().Id];            
    }      
    
    public List<OpportunityLineItem> getOppLineItems()
    {
       List<OpportunityLineItem> lineItems = new List<OpportunityLineItem>();
       
       if (this.spp.opportunity__c != null){
       
           lineItems = [Select ID, 
                               Name, 
                               CRM_Product_Name__c, 
                               application__c,
                               product2.name,
                               product2.id
                        From OpportunityLineItem 
                        Where opportunityId = :this.spp.opportunity__c
                        ];
        
       }
       
       return lineItems;
    }

}