/*=============================================================================
 * Experian
 * Name: BatchAssignPermissionSetToManagers
 * Description: Case:02301636 : This batch is used to assign the EITS Permission Set to the users with Salesforce License and have People reporting to that Users.
 * Created Date: 01st Mar 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
            
 ============================================================================*/
global class BatchAssignPermissionSetToManagers implements Database.Batchable<sObject>,Database.StateFul
{

    @testVisible global List<User> testUsers;
    global BatchAssignPermissionSetToManagers() {
    }
    //start method of batch class
    global Database.QueryLocator start(Database.BatchableContext BC){            
        String query ='select Id,ManagerId from User where IsActive=true';
        if (Test.isRunningTest()) {
          query += ' AND Id IN :testUsers';
        }
        list<User> lst=Database.Query(query);
        system.debug('lstttt'+lst);
        return Database.getQueryLocator(query);      
    }
    //execute method of batch class
    global void execute(Database.BatchableContext BC, List<User> lstUsers){    
        set<Id> setManagerIds = new set<Id>();
        map<Id,User> mapActiveUsers = new map<Id,User>([select Id from User where IsActive=true AND License__c = 'Salesforce' AND Profileid!='00ei00000016UJz']);
        
        for(User objU:lstUsers){
            if(objU.ManagerId!=null)
                setManagerIds.add(objU.ManagerId);
        } 
        system.debug('setManagerIds@@'+setManagerIds);
        map<string,PermissionSetAssignment> mapExistingUsers = new map<string,PermissionSetAssignment>();
        for(PermissionSetAssignment objPerm:[select AssigneeId,PermissionSetId from PermissionSetAssignment where PermissionSetId =: Custom_Fields_Ids__c.getOrgDefaults().EITS_Permission_set_id__c AND AssigneeId IN:setManagerIds]){
            mapExistingUsers.put(objPerm.AssigneeId,objPerm);
        }
        system.debug('mapExistingUsers@@'+mapExistingUsers);
        list<PermissionSetAssignment> lstPermissionTobeInsert = new list<PermissionSetAssignment>();
        for(Id mgrId:setManagerIds){
            if(mapActiveUsers.containsKey(mgrId) && !mapExistingUsers.containsKey(mgrId)){
                PermissionSetAssignment objPerm = new PermissionSetAssignment();
                objPerm.AssigneeId = mgrId;
                objPerm.PermissionSetId = Custom_Fields_Ids__c.getOrgDefaults().EITS_Permission_set_id__c;
                lstPermissionTobeInsert.add(objPerm);
            }
        
        }
        system.debug('lstPermissionTobeInsert@@'+lstPermissionTobeInsert);
        if(!lstPermissionTobeInsert.isEmpty())
            Database.insert(lstPermissionTobeInsert);
        
        map<Id,User> mapActiveManagers = new map<Id,User>();
        
        for(User u:[select id,ManagerId from User where ManagerId IN: lstUsers AND IsActive=true]){
            mapActiveManagers.put(u.ManagerId,u);
        }
        system.debug('setManagerIds@@'+setManagerIds);
        map<string,PermissionSetAssignment> mapExistingPermissionUsers = new map<string,PermissionSetAssignment>();
        for(PermissionSetAssignment objPerm:[select Id,AssigneeId,PermissionSetId from PermissionSetAssignment where PermissionSetId =: Custom_Fields_Ids__c.getOrgDefaults().EITS_Permission_set_id__c AND AssigneeId IN:lstUsers]){
            mapExistingPermissionUsers.put(objPerm.AssigneeId,objPerm);
        }
        system.debug('mapExistingPermissionUsers@@'+mapExistingPermissionUsers);
        list<PermissionSetAssignment> lstPermissionTobeDelete = new list<PermissionSetAssignment>();
        for(User u:lstUsers){
            if(!mapActiveManagers.containsKey(u.Id) && mapExistingPermissionUsers.containsKey(u.Id)){                
                lstPermissionTobeDelete.add(mapExistingPermissionUsers.get(u.Id));
            }
        
        }
        system.debug('lstPermissionTobeDelete@@'+lstPermissionTobeDelete);
        if(!lstPermissionTobeDelete.isEmpty())
            Database.delete(lstPermissionTobeDelete);
    }  
    
    
    global void finish(Database.BatchableContext BC){}
    
}