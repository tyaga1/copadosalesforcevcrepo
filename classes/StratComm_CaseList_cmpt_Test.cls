@isTest
private class StratComm_CaseList_cmpt_Test {
    
    @isTest static void caseListApexControllertest() {
        Account testAccount1 = Test_Utils.createAccount();
        insert testAccount1;
        List<Case> caseList = new List<Case>();
        String serviceCentralId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Central').getRecordTypeId();
 
        Case c1 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 1',
                        Description = 'Test Description 1',
                        status = 'Closed-Complete');
        caseList.add(c1);

        Case c2 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 2',
                        Description = 'Test Description 2',
                        status = 'Closed-Complete');
        caseList.add(c2);

        Case c3 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 3',
                        Description = 'Test Description 3',
                        Status = 'In-Progress');
        caseList.add(c3);

        Case c4 = new Case(
                        RecordTypeId = serviceCentralId,
                        AccountId = testAccount1.Id,
                        Subject = 'Test Case 4',
                        Description = 'Test Description 4',
                        Status = 'In-Progress');
        caseList.add(c4);


        insert caseList;

        
                List<StratComm_CaseListController.caseWrapper> returnedAllCaseListStrat = StratComm_CaseListController.findAll('Status','DESC','All Cases','Strat');
        List<StratComm_CaseListController.caseWrapper> returnedAllOpenCaseListStrat = StratComm_CaseListController.findAll('Status','DESC','All Open Cases','Strat');
        List<StratComm_CaseListController.caseWrapper> returnedMyCaseListStrat = StratComm_CaseListController.findAll('Status','DESC','My Cases','Strat');
        List<StratComm_CaseListController.caseWrapper> returnedMyOpenCaseListStrat = StratComm_CaseListController.findAll('Status','DESC','My Open Cases','Strat');
        
        
        List<StratComm_CaseListController.caseWrapper> returnedAllCaseList = StratComm_CaseListController.findAll('Status','DESC','All Cases','Reseller');
        List<StratComm_CaseListController.caseWrapper> returnedAllOpenCaseList = StratComm_CaseListController.findAll('Status','DESC','All Open Cases','Reseller');
        List<StratComm_CaseListController.caseWrapper> returnedMyCaseList = StratComm_CaseListController.findAll('Status','DESC','My Cases','Reseller');
        List<StratComm_CaseListController.caseWrapper> returnedMyOpenCaseList = StratComm_CaseListController.findAll('Status','DESC','My Open Cases','Reseller');
        
        
    }
    
}