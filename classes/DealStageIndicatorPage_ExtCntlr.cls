public class DealStageIndicatorPage_ExtCntlr {
    
    public Deal__c Deal {get;set;}
    public List<Opportunity_Sales_Process__c> lstStages {get;set;}
  public static final String DEAL_PROCESS = 'Deal Process';
    public DealStageIndicatorPage_ExtCntlr(ApexPages.StandardController controller) {
        
        lstStages = new List<Opportunity_Sales_Process__c>();
    
    for (Opportunity_Sales_Process__c sp : [SELECT Sales_Process_Name__c, Name, Tooltip1__c, Tooltip2__c
                                               FROM Opportunity_Sales_Process__c
                                               WHERE Sales_Process_Name__c =: DEAL_PROCESS 
                                               ORDER BY Sequence__c]) {
      lstStages.add(sp);
    }

}
}