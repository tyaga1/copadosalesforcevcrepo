/**=====================================================================
 * Name: AccountPlan_Competitors_Ctlr_Test
 * Description: See Case #01949954 AP - List views
 * Created Date: May 26th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * May 26th, 2016        James Wills           Case #01949954 AP - List views: Created
 * July 25th, 2016       James Wills           Case #01061020 - AP - Competitors (Current Providers) - Added new test for new Current Providers list
 * Nov 1st, 2016         Manoj Gopu            Case # 02154716 - Fixed the Test class Failure as Part of prod Deployment - Passing boolean as True on 
  =====================================================================*/
@isTest
private Class AccountPlan_Competitors_Ctlr_Test{

   private static testmethod void testAccountPlanCompetitorsList(){
         
     List<Account_Plan__c> newAccountPlanList = [SELECT id, Name FROM Account_Plan__c];   
    
     ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlanList[0]);    
     AccountPlan_Competitors_Controller controller = new AccountPlan_Competitors_Controller(stdController);

     
     Test.startTest();

       controller.competitorsTabUpdateCookies();
       
       //Part 1: Competitors List
       //1
       controller.buildAccountPlanCompetitorsWrapperList();
       Integer numberCompetitors = controller.accountPlanCompetitorsWrapperList.size();
       controller.newAccountPlanCompetitor();     
       controller.buildAccountPlanCompetitorsWrapperList();
       System.Assert(numberCompetitors!=controller.accountPlanCompetitorsWrapperList.size()-1,'AccountPlan_Competitors_Ctlr_Test: problem with newAccountPlanCompetitor() method.');
     
       //2
       numberCompetitors = controller.accountPlanCompetitorsWrapperList.size();
       controller.selectedIDToDelete = [SELECT id FROM Account_Plan_Competitor__c LIMIT 1].id;      
       controller.doDeleteAccountPlanCompetitor();
       controller.buildAccountPlanCompetitorsWrapperList();
       System.Assert(numberCompetitors!=controller.accountPlanCompetitorsWrapperList.size()+1,'AccountPlan_Competitors_Ctlr_Test: problem with doDeleteAccountPlanCompetitor() method.');
       
       //3
       controller.editAccountPlanCompetitor();
       
       //4
       controller.seeMoreCompetitors();
       
       //5          
       controller.viewAccountPlanCompetitorListFull();       
     
      Test.stopTest();
     
   }
     
     
   private static testmethod void testAccountPlanCurrentProvidersList(){
     
     List<Account_Plan__c> newAccountPlanList = [SELECT id, Name FROM Account_Plan__c];   
    
     ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlanList[0]);    
     AccountPlan_Competitors_Controller controller = new AccountPlan_Competitors_Controller(stdController);
    
     Test.startTest();
     
       //Part 2: Current Providers List
       //1
       controller.buildAccountPlanCurrentProvidersWrapperList(true); //  Passing boolean Variable as true -Added by manoj
       for(AccountPlan_Competitors_Controller.AccountPlanCurrentProvidersWrapper CP : controller.accountPlanCurrentProvidersWrapperList){
         System.Assert(CP.CurrentProviderContractEndDate >= Date.Today(), 'AccountPlan_Competitors_Ctlr_Test: problem building accountPlanCurrentProvidersWrapperList.');
       }
       
       //2
       controller.editAccountPlanCurrentProvider();
       
       //3          
       controller.viewAccountPlanCurrentProvidersListFull();  
     
       //4
       controller.seeMoreCurrentProviders();
     

       PageReference apPageReference = controller.backToAccountPlan();
     
     Test.stopTest();
   
   }
  
  @testSetup
  private static void setupTestData(){
    
    Account testAccount1 = Test_Utils.insertAccount();  
    Account testAccount2 = Test_Utils.insertAccount();  
    testAccount2.Ultimate_Parent_Account__c = testAccount1.id;
    update testAccount2;
    
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
    insert newAccountPlan;
    
    List<Account_Plan_Competitor__c> apcList = new List<Account_Plan_Competitor__c>();
    Integer i=0;
    for(i=0;i<25;i++){
      Account_Plan_Competitor__c apc1 = new Account_Plan_Competitor__c(Account_Plan__c=newAccountPlan.id);
      apcList.add(apc1);
    }        
    insert apcList;
    
    List<Current_Provider__c> accountPlanCurrentProvidersList = new List<Current_Provider__c>();
    
    Current_Provider__c newCp1 = new Current_Provider__c(Account__c =  testAccount1.id,
                                                         Current_Provider__c = testAccount1.id,
                                                         Current_Provider_Contract_End_Date__c= Date.Today());
    accountPlanCurrentProvidersList.add(newCP1);
    
    Current_Provider__c newCp2 = new Current_Provider__c(Account__c =  testAccount1.id,
                                                         Current_Provider__c = testAccount1.id,
                                                         Current_Provider_Contract_End_Date__c= Date.Today()-1);

    accountPlanCurrentProvidersList.add(newCP2);
    
    insert accountPlanCurrentProvidersList;
    
    Account_Account_Plan_Junction__c accountPlanJunctionEntry = new Account_Account_Plan_Junction__c(Account__c = testAccount1.id,
                                                                                                     Account_Plan__c = newAccountPlan.id);
    insert accountPlanJunctionEntry;
    
    Custom_Fields_Ids__c customFields = new Custom_Fields_Ids__c(
      AP_Account_Plan_Competitor_Rec__c = 'a0z',
      AP_Account_Plan_Competitor__c     = 'CF00Ni000000EMhX5'
    );
    insert customFields;
  
    Account_Plan_Related_List_Sizes__c custListSizes = new Account_Plan_Related_List_Sizes__c(
      Account_Plan_Competitors_List__c       = 10,
      Account_Plan_Current_Providers_List__c = 10
    );
    insert custListSizes;
    
    
  }
}