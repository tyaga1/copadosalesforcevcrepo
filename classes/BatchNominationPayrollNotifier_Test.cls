/*=============================================================================
 * Experian
 * Name: BatchNominationPayrollNotifier_Test
 * Description: 
 * Created Date: 19 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class BatchNominationPayrollNotifier_Test {

  static String recipEmail = 'sdmfbaskjfbsadfkjbu902u2@experian.com';
  static String giverEmail = 'iueuoiujroeijrenr9083402938@experian.com';
  
  static testMethod void runTestCase1Prepare() {
    
    Employee_Recognition_Payroll__mdt tc1 = [
      SELECT Currency__c, Payroll_Reference__c, DeveloperName FROM Employee_Recognition_Payroll__mdt
      WHERE Test_Case__c = 'Test Case 1'
      LIMIT 1
    ];
    
    BatchNominationPayrollNotifier b = new BatchNominationPayrollNotifier(new List<String>{tc1.DeveloperName}, Datetime.now());
    b.startingProcesses = new List<BatchNominationPayrollNotifier.RunProcess>{
      BatchNominationPayrollNotifier.RunProcess.PrepareReport
    };
    
    List<Nomination__c> nomsCheck = [SELECT Id FROM Nomination__c WHERE Notify_Payroll__c = true];
    
    system.assert(nomsCheck.size() > 0, 'No valid nominations found.');
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(nomsCheck.size(), [SELECT COUNT() FROM Nomination__c WHERE Notify_Payroll__c = true AND Payroll_Notified__c = true]);    
  }
  
  static testMethod void runTestCase1SendReport() {
    
    Employee_Recognition_Payroll__mdt tc1 = [
      SELECT Currency__c, Payroll_Reference__c, DeveloperName FROM Employee_Recognition_Payroll__mdt
      WHERE Test_Case__c = 'Test Case 1'
      LIMIT 1
    ];
    
    String batchGuid = BatchHelper.newGuid();
    
    List<Nomination__c> nomsCheck = [SELECT Id, Payroll_Batch_GUID__c, Payroll_Notified__c FROM Nomination__c WHERE Notify_Payroll__c = true AND Payroll_Notified__c = false];
    
    for (Nomination__c n : nomsCheck) {
      n.Payroll_Batch_GUID__c = batchGuid;
      n.Payroll_Notified__c = true;
    }
    
    update nomsCheck;
    
    BatchNominationPayrollNotifier b = new BatchNominationPayrollNotifier(new List<String>{tc1.DeveloperName}, Datetime.now());
    b.batchGuidList = new List<String>{batchGuid};
    b.startingProcesses = new List<BatchNominationPayrollNotifier.RunProcess>{
      BatchNominationPayrollNotifier.RunProcess.SendReport
    };
    
    system.assert(nomsCheck.size() > 0, 'No valid nominations found.');
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    // system.assertEquals(nomsCheck.size(), [SELECT COUNT() FROM Nomination__c WHERE Notify_Payroll__c = true AND Payroll_Notified__c = true]);    
  }
  
  static testMethod void runTestCase1SendReminder() {
    
    Employee_Recognition_Payroll__mdt tc1 = [
      SELECT Currency__c, Payroll_Reference__c, DeveloperName FROM Employee_Recognition_Payroll__mdt
      WHERE Test_Case__c = 'Test Case 1'
      LIMIT 1
    ];
    
    List<Nomination__c> nomsCheck = [SELECT Id, Payroll_Batch_GUID__c, Payroll_Notified__c FROM Nomination__c WHERE Notify_Payroll__c = true AND Payroll_Notified__c = false];
    
    for (Nomination__c n : nomsCheck) {
      n.Payroll_Batch_GUID__c = 'somebatchid-doesnt-matter';
      n.Payroll_Notified__c = true;
      n.Payroll_Notified_Date__c = Datetime.now().addHours(-1);
    }
    
    update nomsCheck;
    
    BatchNominationPayrollNotifier b = new BatchNominationPayrollNotifier(new List<String>{tc1.DeveloperName}, Datetime.now());
    b.startingProcesses = new List<BatchNominationPayrollNotifier.RunProcess>{
      BatchNominationPayrollNotifier.RunProcess.SendReminder
    };
    
    system.assert(nomsCheck.size() > 0, 'No valid nominations found.');
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    // system.assertEquals(nomsCheck.size(), [SELECT COUNT() FROM Nomination__c WHERE Notify_Payroll__c = true AND Payroll_Notified__c = true]);    
  }
  
  static testMethod void testValidateReportContent() {
    
  }
  
  static testMethod void testSchedule() {
    
    Test.startTest();
    
    String CRON_EXP = '0 0 0 * * ?';
    system.schedule('Payroll Report Test - '+String.valueOf(Datetime.now().getTime()), CRON_EXP, new BatchNominationPayrollNotifier());
    
    Test.stopTest();
  }
  
  
  @testSetup
  private static void setupData() {
    
    NominationTestHelper_Test.createTestUsers();
    NominationTestHelper_Test.createRecognitionBadges();
    
    Employee_Recognition_Payroll__mdt tc1 = [
      SELECT Currency__c, Payroll_Reference__c, DeveloperName FROM Employee_Recognition_Payroll__mdt
      WHERE Test_Case__c = 'Test Case 1'
      LIMIT 1
    ];
    
    for (User u : NominationTestHelper_Test.testRecipUsers.values()) {
      u.Oracle_Payroll_Name__c = tc1.Payroll_Reference__c;
    }
    // Replaces the system applied payroll reference with one from the test metadata
    update NominationTestHelper_Test.testRecipUsers.values();
    
    List<Nomination__c> level2Noms = NominationTestHelper_Test.createNominations(NominationHelper.NomConstants.get('Approved'), NominationHelper.NomConstants.get('Level2SpotAward'), 2, false);

  }
  
}