/**=====================================================================
 * Name: BatchUpdateOpportunityDQScore_TEST
 * Description: CRM2 W-005667: Tests Classes BatchUpdateOpportunityDQScore and BatchUpdateOpportunityDQScoreSchedule
 * Created Date: Aug 19th, 2016
 * Created By: Cristian Torres
 * 
 * Date Modified       Modified By          Description of the update
 * 25 Aug, 2016        Paul Kissick         CRM2:W-005667: Fixed test class to be private and also split into proper test methods
 =====================================================================*/

@isTest
private class BatchUpdateOpportunityDQScore_TEST {
  
  //Test Batch job
  static testMethod void testBatch() {
    
    Test.startTest();
    
    Database.executeBatch(new BatchUpdateOpportunityDQScore());
    
    Test.stopTest();
    
  }
  
  static testMethod void testSchedule() {
    
    Test.startTest();
    
    system.schedule('Update Opportunity DQScore TEST'+String.valueOf(Datetime.now().getTime()), '0 42 23 * * ? *', new BatchUpdateOpportunityDQScoreSchedule());
    
    Test.stopTest();
    
  }
  
  @testSetup
  static void setupData() {
    //Create and insert a new Account
    Account acc1 = Test_Utils.insertAccount();
    // Create and insert a contact
    Contact con1 = Test_Utils.insertContact(acc1.Id);

    //Create an opportunity
    Opportunity opp1 = Test_Utils.createOpportunity(acc1.Id);
    opp1.Contract_Start_Date__c = Date.today().addDays(-20);
    opp1.Contract_End_Date__c = Date.today().addDays(10);
    opp1.CloseDate = Date.today().addDays(5);
    insert opp1;
        
    Product2 prod = Test_Utils.insertProduct();
    PricebookEntry pbEntry = Test_Utils.insertPricebookEntry(prod.Id, Test.getStandardPriceBookId(), Constants.CURRENCY_USD);
    
    OpportunityLineItem oli = Test_Utils.createOpportunityLineItem(opp1.Id, pbEntry.Id, opp1.Type);
    oli.End_Date__c = Date.today().addDays(15);
    oli.Start_Date__c = Date.today().addDays(4);

    //create your opportunity line item.
    insert oli;
    
    //oli.Start_Date__c = Date.today().addDays(7);
    //update oli;
  }
  
  
}