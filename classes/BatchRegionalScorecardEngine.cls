/*=============================================================================
 * Experian
 * Name: BatchRegionalScorecardEngine
 * Description: Case 01916186
                Runs the regional scorecard queries and builds the results for each metric.
 * Created Date: 18 May 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 *

 =============================================================================*/

public class BatchRegionalScorecardEngine implements Database.Batchable<sObject>, Database.Stateful {

  public List<Id> metricIdList; // Used to store the remaining metrics to run
  public Map<Id, Boolean> metricIdTypeMap; // Used to store the types for each metric, e.g. calculated or queried

  public Id metricId; // Current metric to run

  private List<String> errorsFound = new List<String>();

  private Regional_Scorecard_Metric__c rsMetric; // Stored copy of the current metric
  private Map<String, String> groupingNamesMap; // Groupings to build based on queried fields (e.g. Region, BL, BU)

  private Set<String> distinctSet; // Used to hold distinct-'ness' to only count once for each id, or whatever

  private Map<String, RegionalScorecardUtils.metricData> dataMap = new Map<String, RegionalScorecardUtils.metricData>();

  public BatchRegionalScorecardEngine () {
  }

  public Database.Querylocator start (Database.Batchablecontext bc) {

    distinctSet = new Set<String>();
    errorsFound = new List<String>();

    // Load the metric into variable
    if (metricId != null) {
      rsMetric = [
        SELECT Id, Type__c, Object_API_Name__c, Where_Clause__c,
          Grouping__c, Grouping_Names__c, Aggregate_Field__c, Metric_Calculation_Field_1__c,
          Metric_Calculation_Field_2__c, Calculation_Field_Operator__c, Master_Grouping__c,
          Aggregate_Type__c
        FROM Regional_Scorecard_Metric__c
        WHERE Id = :metricId
      ];
    }

    // locate a master grouping first also, if this isn't one
    if (!rsMetric.Master_Grouping__c) {
      // get the prepopulated groupings
      preloadDataMap();
    }

    preloadGroupings();
    // This is a dymanic query based on the queried metric data
    return Database.getQueryLocator(buildQuery(rsMetric));
  }

  // Method to load the grouping based on the current metric.
  public void preloadGroupings() {
    groupingNamesMap = new Map<String, String>();
    List<String> groupingLabels = rsMetric.Grouping_Names__c.split(',');
    List<String> groupingNames = rsMetric.Grouping__c.split(',');
    for (Integer g = 0; g < groupingNames.size(); g++) {
      groupingNamesMap.put(groupingNames[g], groupingLabels[g]);
    }
    return;
  }

  // Method to load the datamap using the master groupings (e.g. no of sales users)
  public void preloadDataMap() {
    List<Regional_Scorecard_Data__c> primaryDatasetList = [
      SELECT Id, Name__c, Type__c
      FROM Regional_Scorecard_Data__c
      WHERE Regional_Scorecard_Metric__r.Master_Grouping__c = true
    ];
    String key;
    for (Regional_Scorecard_Data__c prsd : primaryDatasetList) {
      key = prsd.Type__c + RegionalScorecardUtils.KEY_SEPARATOR + prsd.Name__c;
      dataMap.put(key, new RegionalScorecardUtils.metricData(key));
    }
    return;
  }

  public void execute (Database.BatchableContext bc, List<sObject> scope) {
    processBatch(scope);
  }

  public void processBatch (List<sObject> scope) {
    CurrencyUtility cu = new CurrencyUtility();
    String nameVal;
    String tmpKey;
    String distinctKey;
    Decimal aggData;

    for (sObject s : scope) {
      if (rsMetric.Aggregate_Type__c.equals('Count')) {
        distinctKey = (String)s.get(rsMetric.Aggregate_Field__c);
        // If we've seen this aggr field before, e.g. opp id, move to the next record.
        if (distinctSet.contains(distinctKey)) {
          continue;
        }
        distinctSet.add(distinctKey);
      }
      // here we know that the type and name will be the key, and this will determine how we process the totals...
      for (String fieldGrouping : groupingNamesMap.keySet()) {

        nameVal = (String)extractName(s, fieldGrouping);

        if (String.isNotBlank(nameVal)) {
          tmpKey = groupingNamesMap.get(fieldGrouping) + RegionalScorecardUtils.KEY_SEPARATOR + nameVal;

          if (rsMetric.Master_Grouping__c && !dataMap.containsKey(tmpKey)) {
            // create a new datamap entry
            dataMap.put(tmpKey, new RegionalScorecardUtils.metricData(tmpKey));
          }

          if (dataMap.containsKey(tmpKey)) {

            // Always increment the count
            dataMap.get(tmpKey).totalCount += 1;

            if (rsMetric.Aggregate_Type__c.equals('Sum') || rsMetric.Aggregate_Type__c.equals('Average')) {

              // These types require the actual value from the aggr field
              aggData = (Decimal)s.get(rsMetric.Aggregate_Field__c);
              if (aggData != null) {
                // And currencies must be converted to USD
                if (rsMetric.Type__c.equals('Currency')) {
                  dataMap.get(tmpKey).totalValue += cu.convertToUSD((String)s.get('CurrencyIsoCode'), aggData);
                }
                else {
                  dataMap.get(tmpKey).totalValue += aggData;
                }
              }
              aggData = null;
            }
          }
        }
      }
    }
  }

  public static String buildQuery(Regional_Scorecard_Metric__c currentMetric) {

    String query = 'SELECT {0} FROM {1} WHERE {2}';

    List<String> queryParams = new String[3];

    queryParams.set(1, currentMetric.Object_API_Name__c);
    queryParams.set(2, currentMetric.Where_Clause__c);

    List<String> selectFields = new List<String>{currentMetric.Aggregate_Field__c};
    selectFields.addAll(currentMetric.Grouping__c.split(','));
    selectFields.add('CurrencyIsoCode'); // Always add the currency to the query

    queryParams.set(0, String.join(selectFields,','));
    query = String.format(query, queryParams);

    system.debug(LoggingLevel.INFO, 'Query to execute: '+query);

    return query;
  }

  public void finish (Database.BatchableContext bc) {

    // load the finished data into the db
    createData();

    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchRegionalScorecardEngine', false);
    if (!errorsFound.isEmpty() || Test.isRunningTest()) {
      bh.emailBody += 'The following errors were found while processing.\n\n';
      for (String err : errorsFound) {
        bh.emailBody += err + '\n';
      }
    }
    bh.sendEmail();

    if (!Test.isRunningTest() && metricIdList != null) {
      RegionalScorecardUtils.runBatch(metricIdList, metricIdTypeMap);
    }

  }

  public void createData () {
    try {
      List<Regional_Scorecard_Data__c> dataToUpsert = new List<Regional_Scorecard_Data__c> ();
      for (String rsName : dataMap.keySet()) {
        // here we have the calculated data, so lets start building the upserts....
        dataToUpsert.add(
          RegionalScorecardUtils.buildDataRecord(
            rsMetric,
            dataMap.get(rsName),
            rsName.substringAfter(RegionalScorecardUtils.KEY_SEPARATOR),
            rsName.substringBefore(RegionalScorecardUtils.KEY_SEPARATOR)
          )
        );
      }

      upsert dataToUpsert Metric_ID__c;

      Regional_Scorecard_Metric__c rsm = new Regional_Scorecard_Metric__c(
        Id = rsMetric.Id,
        Last_Ran__c = system.now()
      );
      update rsm;
    }
    catch (DMLException ex) {
      apexLogHandler.createLogAndSave('BatchRegionalScorecardCalculated','execute', ex.getStackTraceString(), ex);
      errorsFound.add(ex.getMessage());
    }
    catch (Exception e) {
      errorsFound.add(e.getMessage());
    }
  }

  public static Object extractName(SObject o, String field){
    if (o == null) {
      return null;
    }
    if (field.contains('.')) {
      String nextField = field.substringAfter('.');
      String relation = field.substringBefore('.');
      return extractName((SObject)o.getSObject(relation), nextField);
    }
    else {
      return o.get(field);
    }
  }

}