/******************************************************************************
 * Experian
 * Name: BatchWalletSyncAccountOwners_Test
 * Description: 
 * Created Date: 6 Aug 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * Aug 28th, 2015     Paul Kissick               Added Previous Account Supervisor to the tests
 * Oct 1st, 2015      Paul Kissick               Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 ******************************************************************************/
@isTest
private class BatchWalletSyncAccountOwners_Test {

  static testMethod void testAccountOwners() {
    
    User newAccOwner = [SELECT Id FROM User WHERE Alias = 'ZWQQ0' LIMIT 1];
    User prevAccOwner = [SELECT Id FROM User WHERE Alias = 'ZWQQ1' LIMIT 1];
    User accSuper = [SELECT Id FROM User WHERE Alias = 'ZWQQ2' LIMIT 1];
    User prevAccSuper = [SELECT Id FROM User WHERE Alias = 'ZWQQ3' LIMIT 1];
    
    List<WalletSync__c> wsObjs = new List<WalletSync__c>();
    
    List<Account> allAccs = [SELECT Id, CNPJ_Number__c FROM Account];
    
    List<AccountTeamMember> testMembers = [SELECT UserId, TeamMemberRole, AccountId FROM AccountTeamMember];
    
    // The previous account owner must be on the team already
    system.assertEquals(5,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :prevAccOwner.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_ACCOUNT_MANAGER]);
    
    system.assertEquals(5,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :prevAccSuper.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_STRAT_ACCOUNT_MANAGER]);
    
    system.assertEquals(0,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :newAccOwner.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_ACCOUNT_MANAGER]);
    
    system.assertEquals(0,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :accSuper.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_STRAT_ACCOUNT_MANAGER]);
    
    for(Account a : allAccs) {
      wsObjs.add(new WalletSync__c(
        CNPJ_Number__c = a.CNPJ_Number__c, 
        Account_Owner__c = newAccOwner.Id, 
        Account_Supervisor__c = accSuper.Id, 
        Previous_Account_Owner__c = prevAccOwner.Id,
        Previous_Account_Supervisor__c = prevAccSuper.Id,
        Account__c = a.Id,
        Last_Processed_Date__c = Datetime.now().addHours(-1),
        LegacyCRM_Account_ID__c = 'ANYTHING1'
      ));
    }
    
    insert wsObjs;
    
    Test.startTest();
    
    BatchWalletSyncAccountOwners b = new BatchWalletSyncAccountOwners();
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(0,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :prevAccOwner.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_ACCOUNT_MANAGER]);
    
    system.assertEquals(0,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :prevAccSuper.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_STRAT_ACCOUNT_MANAGER]);
    
    system.assertEquals(5,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :newAccOwner.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_ACCOUNT_MANAGER]);
    
    system.assertEquals(5,[SELECT COUNT() FROM AccountTeamMember WHERE UserId = :accSuper.Id AND TeamMemberRole = :BatchWalletSyncAccountOwners.TEAM_ROLE_STRAT_ACCOUNT_MANAGER]);
    
  }

  @testSetup
  static void setupTestData() {
    
    Profile p = [select id from profile where name=: Constants.PROFILE_SYS_ADMIN ];
    List<User> tstUsrs = Test_Utils.createUsers(p,'test@experian.com','ZWQQ',4);
    insert tstUsrs;
    
    List<Account> newAccs = new List<Account>();
    User u = [SELECT Id FROM User WHERE Alias = 'ZWQQ1' LIMIT 1];
    User superVisorUser = [SELECT Id FROM User WHERE Alias = 'ZWQQ3' LIMIT 1];
    system.runAs(u) {
	    Account acc1 = Test_utils.createAccount();
	    acc1.CNPJ_Number__c = '62307946000152';
	    newAccs.add(acc1);
	    Account acc2 = Test_utils.createAccount();
	    acc2.CNPJ_Number__c = '17240483000103';
	    newAccs.add(acc2);
	    Account acc3 = Test_utils.createAccount();
	    acc3.CNPJ_Number__c = '43161803000130';
	    newAccs.add(acc3);
	    Account acc4 = Test_utils.createAccount();
	    acc4.CNPJ_Number__c = '34895621000101';
	    newAccs.add(acc4);
	    Account acc5 = Test_utils.createAccount();
	    acc5.CNPJ_Number__c = '18068852000186';
	    newAccs.add(acc5);
	    insert newAccs;
    }
    
    // Add previous supervisor
    List<AccountTeamMember> atms = new List<AccountTeamMember>();
    for(Account a : [SELECT Id FROM Account]) {
      AccountTeamMember atm = new AccountTeamMember(
        UserId = superVisorUser.Id,
        AccountId = a.Id,
        TeamMemberRole = BatchWalletSyncAccountOwners.TEAM_ROLE_STRAT_ACCOUNT_MANAGER
      );
      atms.add(atm);
    }
    insert atms;
    
    Global_Settings__c globalSetting = Global_Settings__c.getInstance('Global');
    globalSetting.Batch_Failures_Email__c = 'test@experian.com';
    globalSetting.Wallet_Sync_Processing_Last_Run__c = system.now().addDays(-1);
    update globalSetting;
  }
}