/******************************************************************************
 * Name: BatchCreateOrderSplitProducts
 * Description: 
 *              
 * Created Date: 21st September, 2017
 * Created By: James Wills
 *
 * Date Modified                Modified By                  Description of the update
 * 25th Sept. 2017              James Wills                  Case #13867785 Made class schedulable.
 ******************************************************************************/
global class BatchCreateOrderSplitProducts implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
   
  private DateTime currentRunTime;
  private DateTime lastRunTime;
   
  global void execute(SchedulableContext sc){
    BatchCreateOrderSplitProducts b = new BatchCreateOrderSplitProducts();
    Database.executebatch(b);
  }
   
      
  //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC){
    
    currentRunTime = System.now();
    lastRunTime    = BatchHelper.getBatchClassTimestamp('BatchCreateOrderSplitProducts');
    
    
    return Database.getQueryLocator([SELECT id, Order__c FROM Order_Split__c 
                                     WHERE LastModifiedDate > :lastRunTime
                                     AND Order__c != null
                                     AND Order__r.Line_Items_Number__c > 0]);
    
  }
  
  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
  
    List<Order_Split__c> newListForUpdate = new List<Order_Split__c>();//Order Splits without OSPs    
    List<Order_Split_Product__c> newOrderSplitProduct_List = new List<Order_Split_Product__c>();//New OSP records  
    
    Map<ID, Order_Split__c> ordSplit_Map = new Map<ID, Order_Split__c>((List<Order_Split__c>)scope);
                                                   
    List<Order_Split_Product__c> ospid_List = [SELECT id, Order_Split__c FROM Order_Split_Product__c WHERE Order_Split__c IN :ordSplit_Map.keySet()];//Existing records.
    Set<ID> ospid_Set = new Set<ID>();
    for(Order_Split_Product__c osp : ospid_List){
       ospid_Set.add(osp.Order_Split__c);
    }
    
    for(Order_Split__c os: ordSplit_Map.values()){
      if(ospid_Set.contains(os.Id)){
        //Do nothing
      } else {
        newListForUpdate.add(os);
      }
    }
    
    
    Set<id> orderIds = new Set<id>();
    for(Order_Split__c os1 : newListForUpdate){
      orderIds.add(os1.Order__c);
    }
    
    Map<Id, Order__c> ord_Map = new Map<Id,Order__c>([SELECT id, (SELECT id, Product__r.id FROM Order_Line_Items__r) 
                                                      FROM Order__c WHERE Id IN :orderIds]);       
    
    for(Order_Split__c os3 : newListForUpdate){
          
      Order__c newOrder = ord_Map.get(os3.Order__c);

      for(Order_Line_Item__c oli : newOrder.Order_Line_Items__r){
          Order_Split_Product__c newOsp = new Order_Split_Product__c();
       
          newOsp.Order_Split__c     = os3.id;
          newOsp.Order__c           = os3.Order__c;      
          newOsp.Order_Line_Item__c = oli.id;  
          newOsp.Product__c         = oli.Product__r.id;   
        
          newOrderSplitProduct_List.add(newOsp);
      } 
    }
    
    try{
      insert newOrderSplitProduct_List;
    } catch(Exception e){
    
    }
       
  }
  

  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchCreateOrderSplitProducts', true);    
    BatchHelper.setBatchClassTimestamp('BatchCreateOrderSplitProducts', currentRunTime);
  }

}