/*=============================================================================
 * Experian
 * Name: OpportunityCloneExt_Test
 * Description: CRM2:W-005338: Tests for the OpportunityCloneExt class
 * Created Date: 14 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 14 Jul 2016        Paul Kissick          CRM2:W-005338: Extension to handle deep cloning of an opportunity.
 * 09 Sep 2016        Paul Kissick          CRM2:W-005338: Minor change to use label for name checking.
 * 17 Oct 2016        Paul Kissick          Case 02165894: Added more testing for allowed to clone and selecting the first opp line.
 * Apr 25th, 2017     Sanket Vaidya         Case 02150014: CRM 2.0- Opportunity Competitor Information [Added Is_competitor__c flag to true for account]  
 =============================================================================*/

@isTest
private class OpportunityCloneExt_Test {

  static testMethod void myUnitTest1() {
    
    Opportunity toClone = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
    
    Test.startTest();
    
    OpportunityCloneExt oce = new OpportunityCloneExt(new ApexPages.StandardController(toClone));
    
    oce.prepareCloning();
    
    oce.availableOppLineItems.get(0).selected = true; // Ensure we select this product.
    system.assertEquals(true, oce.allowedClone);
    
    system.assert(oce.createClone() != null);
    
    Test.stopTest();
    
    String nameCheck = Label.Opp_Clone_Copy_of + '%';
    
    system.assertEquals(1, [SELECT COUNT() FROM Opportunity WHERE Name LIKE :nameCheck]);
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    IsDataAdmin__c myIda = new IsDataAdmin__c(IsDataAdmin__c = true, SetupOwnerId = UserInfo.getUserId());
    insert myIda;
    User tstUser1 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    User tstUser2 = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    
    Account a1 = Test_Utils.insertAccount();
    a1.Is_Competitor__c = true;
    update a1;

    Account a2 = Test_Utils.insertAccount();
    Contact c1 = Test_Utils.insertContact(a1.Id);
    Opportunity o1 = Test_Utils.insertOpportunity(a1.Id);
    
    Product2 prod1 = Test_Utils.insertProduct();
    PricebookEntry pbe = Test_Utils.insertPricebookEntry(prod1.Id, Test.getStandardPricebookId(), 'USD');
    OpportunityLineItem oli1 = Test_Utils.insertOpportunityLineItem(o1.Id, pbe.Id, Constants.OPPTY_NEW_FROM_NEW);
    OpportunityContactRole ocr1 = Test_Utils.insertOpportunityCR(true, c1.Id, o1.Id);
    
    Partner op1 = new Partner(OpportunityId = o1.Id, AccountToId = a2.Id);
    insert op1;
    
    Competitor__c comp1 = Test_Utils.insertCompetitor(o1.Id);
    OpportunityTeamMember otm1 = Test_Utils.insertOpportunityTeamMember(true, o1.Id, tstUser2.Id, Constants.TEAM_ROLE_ACCOUNT_MANAGER);
    
    delete myIda;
    
  }
  
}