@isTest
public class BatchUATReminderForAgileAccelerator_Test
{
    @isTest
    public static void testBatchForTestCases()
    {
        List<Test_Case__c> caseList = new List<Test_Case__c>();
        
        
        Project__c project = new Project__c(UAT_Reminder__c = 10);
        insert project;
        
        agf__ADM_Work__c work = new agf__ADM_Work__c(agf__Due_Date__c = Date.today().addDays(7));        
        work.Experian_Project__c = project.Id;
        insert work;
        
        /**Test Cases**/
        Test_Case__c c1 = new Test_Case__c(work__c = work.Id, project__c = project.id);
        c1.UAT_Owner__c = UserInfo.getUserId();

        Test_Case__c c2 = new Test_Case__c(work__c = work.Id, project__c = project.id);

        Test_Case__c c3 = new Test_Case__c(work__c = work.Id, project__c = project.id);
        
        caseList.add(c1);
        caseList.add(c2);
        caseList.add(c3);
        
                
        for(Integer i=0; i < 100; i++)
        {
            Test_Case__c c = new Test_Case__c();
            c.work__c = work.ID;
            c.project__c = project.ID;
            
            caseList.add(c);
        }
        
        
        insert caseList;
        /************/
        
        /**Test Case Team**/
        List<User> listUsers = [SELECT ID, Name, Email FROM USER WHERE ID != :UserInfo.GetUserId() LIMIT 5];
        System.Debug('list of Users : ' + listUsers);
        
        List<Test_Case_Team__C> teamList = new List<Test_Case_Team__c>();
        
                Test_Case_Team__c member1 = new Test_Case_Team__c();
                member1.Test_Case__c = c1.Id;
                member1.Tester__c = UserInfo.getUserId();
                teamList.add(member1);
                
                Test_Case_Team__c member2 = new Test_Case_Team__c();
                member2.Test_Case__c = c1.Id;
                member2.Tester__c = listUsers[0].Id;
                teamList.add(member2);
        
        Test_Case_Team__c member3 = new Test_Case_Team__c();
        member3.Test_Case__c = c2.Id;
        member3.Tester__c = listUsers[1].Id;
        teamList.add(member3);
        
        Test_Case_Team__c member4 = new Test_Case_Team__c();
        member4.Test_Case__c = c2.Id;
        member4.Tester__c = listUsers[2].Id;
        teamList.add(member4);
        
        Test_Case_Team__c member5 = new Test_Case_Team__c();
        member5.Test_Case__c = c2.Id;
        member5.Tester__c = listUsers[3].Id;
        teamList.add(member5);
        
        Test_Case_Team__c member6 = new Test_Case_Team__c();
        member6.Test_Case__c = c2.Id;
        member6.Tester__c = UserInfo.getUserId();
        teamList.add(member6);
        
             	Test_Case_Team__c member7 = new Test_Case_Team__c();
            	member7.Test_Case__c = caseList[0].Id;            
            	teamList.add(member7);
            
             	Test_Case_Team__c member8 = new Test_Case_Team__c();
            	member8.Test_Case__c = caseList[1].Id;
            	teamList.add(member8);
        
        insert teamList;
        
        Test.startTest();
           
        database.executebatch(new BatchUATReminderForAgileAccelerator());
        
        Test.stopTest();
            
    }    
}