/**********************************************************************************************
 * Experian, Inc 
 * Name         : SMXRestResourceRelSurveyTest
 * Created By   : Satmetrix
 * Purpose      : Test Class for SMXRestResourceRelSurvey
 * Created Date : 08 August 2016
 *
 * Date Modified                Modified By                 Description of the update
 * Sep 7th, 2016                Paul Kissick                Fixed failing test.
 * Sep 8th, 2016                Tyaga Pati                  Fixed failing test.
***********************************************************************************************/
@isTest
private class SMXRestResourceRelSurveyTest{
  
  // test the getConvertDateTime method
  static testMethod void testGetConvertDateTime() {
    Datetime res = SMXRestResourceRelSurvey.getConvertDateTime('2012-01-01 10:25:20');
    system.assertEquals(2012, res.yearGmt());
    system.assertEquals(10, res.hourGmt());

  }
  
  //test WS inteface
  static testMethod void testDoPost() {
    String strFeedbackId =  prepareTestData();
    System.RestContext.request = new RestRequest();
    RestContext.request.requestURI = '/UpdateFeedback/' + strFeedbackId;
    SMXRestResourceRelSurvey.doPost('UpdateESRStatus',9,'Test Comment','Delivered','Not Started','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','ABCDEFGIJ123456789','SMX TEST DC','TEST_1288',1,9,8,'TEST IMprovement Comment',5,4,3,'1234567910',2,UserInfo.getUserId());
    SMXRestResourceRelSurvey.doPost('UpdateFeedbackDetails',9,'Test Comment','Delivered','Not Started','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','2012-01-01 10:25:20','ABCDEFGIJ123456789','SMX TEST DC','TEST_1288',1,9,8,'TEST IMprovement Comment',5,4,3,'1234567910',2,UserInfo.getUserId());
  }
    
  //Prepare Test Data Method
  static string prepareTestData() {
    //Prepare test Contact  
    Account a = Test_Utils.insertAccount();
    
    List<Contact> contactList = new List<Contact>();
    Contact c = Test_Utils.createContact(a.Id);
    c.FirstName = 'SMXTestCtctFName';
    c.LastName = 'SMXTestCtctLName';
    c.MailingCountry = 'Japan';
    contactList.add(c);
    insert contactList;
    
    SMXRestResourceRelSurvey.createSurveyRecord('TESTFBKID','Invitation Delivered',c.Id,'Response Received','TEST_SURVEYNAME','TEST_SURVEYID','TESTACOUNTID');
    //Prepare test feedback record
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'TEST_CRM_12345';
    feedback.Contact__c = c.Id; //ContactName
    feedback.DataCollectionId__c = '123456';
    feedback.Status__c = 'Test_Nominated';               
    feedback.DataCollectionName__c = 'Test Survey Name';
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedbackList.add(feedback);
    insert feedbackList;    
    return feedback.Name;
  }
  
}