/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SetExportedJob implements Database.AllowsCallouts, Database.Batchable<seal__Contract__c> {
    global SetExportedJob() {

    }
    global void execute(Database.BatchableContext BC, List<seal__Contract__c> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global System.Iterable start(Database.BatchableContext BC) {
        return null;
    }
}
