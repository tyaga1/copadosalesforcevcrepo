/**=====================================================================
 * Experian
 * Name: ExportPackageXMLCtrla_Test
 * Description: 
 * Created Date: 28 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By        Description of the update
 * Apr 7th, 2016      Paul Kissick       Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class ExportPackageXMLCtrla_Test {
  
  static String testUserEmail = 'Testuseremail8758756@experian.com';

  static testMethod void testDeployment1() {
    
    Deployment_Request__c dr1 = [SELECT Status__c, Id FROM Deployment_Request__c WHERE Release__c = 'Support Release 1' LIMIT 1];
    dr1.Status__c = 'Approved for Deployment';
    update dr1;
    
    Test.startTest();
    
    ExportPackageXMLCtrla pkgXml = new ExportPackageXMLCtrla(new ApexPages.StandardController(dr1));
    
    system.assert(pkgXml.generatePackageXML() != null);
    
    system.assertEquals(1, [SELECT COUNT() FROM Attachment WHERE ParentId = :dr1.Id]);
    system.assertEquals(1, [SELECT COUNT() FROM Attachment WHERE ParentId = :dr1.Id]);
    
    Test.stopTest();
    
  }
  
  static testMethod void testDeployment2() {
    
    Deployment_Request__c dr2 = [SELECT Status__c, Id FROM Deployment_Request__c WHERE Release__c = 'Support Release 2' LIMIT 1];
    dr2.Status__c = 'Approved for Deployment';
    update dr2;
    
    Test.startTest();
    
    ExportPackageXMLCtrla pkgXml = new ExportPackageXMLCtrla(new ApexPages.StandardController(dr2));
    
    system.assert(pkgXml.generatePackageXML() != null);
    
    system.assertEquals(1, [SELECT COUNT() FROM Attachment WHERE ParentId = :dr2.Id]);
    
    Test.stopTest();
    
  }
  
  static testMethod void testDeployment3() {
    
    Deployment_Request__c dr3 = [SELECT Status__c, Id FROM Deployment_Request__c WHERE Release__c = 'Prod Support Release 3' LIMIT 1];
    dr3.Status__c = 'Approved for Deployment';
    update dr3;
    
    Test.startTest();
    
    ExportPackageXMLCtrla pkgXml = new ExportPackageXMLCtrla(new ApexPages.StandardController(dr3));
    
    system.assert(pkgXml.generatePackageXML() != null);
    
    system.assertEquals(2, [SELECT COUNT() FROM Attachment WHERE ParentId = :dr3.Id]);
    
    Test.stopTest();
    
  }
  
  static testMethod void testDeployment3Bad() {
    
    Metadata_Component__c md3 = new Metadata_Component__c(
      Component_API_Name__c = '*',
      Component_Type__c = 'Profile',
      Object_API_Name__c = '',
      Name = '*'
    );
    insert md3;
    
    Case c3 = [SELECT Id FROM Case WHERE Subject = 'Case Subject 3'];
    
    Case_Component__c cc7 = new Case_Component__c(
      Action__c = 'Delete',
      Case_Number__c = c3.Id,
      Component_Name__c = md3.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    insert cc7;
    
    Deployment_Request__c dr3 = [SELECT Status__c, Id FROM Deployment_Request__c WHERE Release__c = 'Prod Support Release 3' LIMIT 1];
    
    Deployment_Component__c dco7 = new Deployment_Component__c(
      Case_Component__c = cc7.Id,
      Slot__c = dr3.Id
    );
    insert dco7;
    
    dr3.Status__c = 'Approved for Deployment';
    update dr3;
    
    Test.startTest();
    
    ExportPackageXMLCtrla pkgXml = new ExportPackageXMLCtrla(new ApexPages.StandardController(dr3));
    
    system.assert(pkgXml.generatePackageXML() == null);
    
    system.assertEquals(0, [SELECT COUNT() FROM Attachment WHERE ParentId = :dr3.Id]);
    
    Test.stopTest();
    
  }
  
  static testMethod void testCloneRequest() {
    
    Deployment_Request__c dr = [SELECT Id FROM Deployment_Request__c WHERE Release__c = 'Support Release 1' LIMIT 1];
    
    DeploymentRequestCloneExt cloneTest = new DeploymentRequestCloneExt(new ApexPages.StandardController(dr)); 
    
    system.assertNotEquals(null,cloneTest.doClone());
    
    system.assertEquals(1, [SELECT COUNT() FROM Deployment_Request__c WHERE Release__c = 'Support Release 1 - Copy']);
    
  }
  
  @testSetup
  static void setupTestData() {
    
    User mu1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert mu1;
    
    User u1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    u1.Email = testUserEmail;
    u1.ManagerId = mu1.Id;
    insert u1;
    
    
    Account a1 = Test_Utils.insertAccount();
    Case c1 = Test_Utils.insertCase(false,a1.Id);
    Case c2 = Test_Utils.insertCase(false,a1.Id);
    Case c3 = Test_Utils.insertCase(false,a1.Id);
    c3.Subject = 'Case Subject 3';
    insert new List<Case>{c1,c2,c3};
    
    Metadata_Component__c md1 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field1__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    Metadata_Component__c md2 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field2__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    insert new Metadata_Component__c[]{md1,md2};
    
    Case_Component__c cc1 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc2 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc3 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c2.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc4 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c2.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc5 = new Case_Component__c(
      Action__c = 'Delete',
      Case_Number__c = c3.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc6 = new Case_Component__c(
      Action__c = 'Delete',
      Case_Number__c = c3.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    insert new Case_Component__c[]{cc1,cc2, cc3, cc4, cc5, cc6};
    
    Salesforce_Environment__c src = new Salesforce_Environment__c(
      Name = 'Source',
      Environment_Type__c = 'DEV'
    );
    Salesforce_Environment__c trg = new Salesforce_Environment__c(
      Name = 'Target',
      Environment_Type__c = 'UAT'
    );
    insert new Salesforce_Environment__c[]{src,trg};
    
    Deployment_Request__c dr1 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release_Manager__c = UserInfo.getUserId(),
      Release__c = 'Support Release 1',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id
    );
    insert dr1;
    
    Deployment_Request__c dr2 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release_Manager__c = UserInfo.getUserId(),
      Release__c = 'Support Release 2',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id,
      Is_Production__c = true,
      Release_Type__c = 'Standard'
    );
    insert dr2;
    
    Deployment_Request__c dr3 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release_Manager__c = UserInfo.getUserId(),
      Release__c = 'Prod Support Release 3',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id,
      Is_Production__c = true,
      Release_Type__c = 'Standard'
    );
    insert dr3;
    
    Deployment_Component__c dco1 = new Deployment_Component__c(
      Case_Component__c = cc1.Id,
      Slot__c = dr1.Id
    );
    Deployment_Component__c dco2 = new Deployment_Component__c(
      Case_Component__c = cc2.Id,
      Slot__c = dr1.Id
    );
    Deployment_Component__c dco3 = new Deployment_Component__c(
      Case_Component__c = cc3.Id,
      Slot__c = dr2.Id
    );
    Deployment_Component__c dco4 = new Deployment_Component__c(
      Case_Component__c = cc4.Id,
      Slot__c = dr2.Id
    );
    Deployment_Component__c dco5 = new Deployment_Component__c(
      Case_Component__c = cc5.Id,
      Slot__c = dr3.Id
    );
    Deployment_Component__c dco6 = new Deployment_Component__c(
      Case_Component__c = cc6.Id,
      Slot__c = dr3.Id
    );
    insert new Deployment_Component__c[]{dco1,dco2,dco3,dco4, dco5, dco6};
    
    Deployment_Cases__c dca1 = new Deployment_Cases__c(
      Case__c = c1.Id,
      Deployment_Request_Slot__c = dr1.Id
    );
    
    Deployment_Cases__c dca2 = new Deployment_Cases__c(
      Case__c = c2.Id,
      Deployment_Request_Slot__c = dr2.Id
    );
    
    Deployment_Cases__c dca3 = new Deployment_Cases__c(
      Case__c = c3.Id,
      Deployment_Request_Slot__c = dr3.Id
    );
    
    insert new List<Deployment_Cases__c>{dca1,dca2,dca3};
    
  }
}