/**=====================================================================
  * Experian
  * Name: CaseCreateRedirectController
  * Description: W-007516: Controller class for case SObject to retrive redirect back to standard salesforce layout
  *              , auto populate fields by url hacks if matched case recordtype was selected.
  * Created Date: March 22 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * March 28th 2017    Manoj Gopu                   W-007516: Used existing custom setting and Created Field to get the Case Island  Fieldid 
  * March 29th 2017    Ryan (Weijie) Hu             W-007516: Since we use custom setting, test classes will not see data, so skip it
  * April 11th 2017    Ryan (Weijie) Hu             W-007516: Fix the recordType id is missing problem when there is only RT available for the current user
  * April 19th 2017    Ryan (Weijie) Hu             W-007516: Changes made to get url instead of page reference for live agent bugs
  * May   2nd  2017    Ryan (Weijie) Hu             Custom labels for translation added.
  =====================================================================*/
public with sharing class CaseCreateRedirectController {

    private final Case mysObject;
    public Boolean isSerasaCustomCareProfiles {get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CaseCreateRedirectController(ApexPages.StandardController stdController) {
        this.mysObject = (Case)stdController.getRecord();
        setIsSerasaCustomCareProfiles();
    }

    public void setIsSerasaCustomCareProfiles() {
      try{
        Profile thisProfile = [SELECT Name FROM Profile WHERE Id=:userinfo.getProfileId()];
        String profileName = thisProfile.Name;

        if (profileName.contains('Experian Serasa '))
        {
          profileName = profileName.remove('Experian Serasa ');
        }

        Serasa_User_Profiles__c serasaProfileNames = [SELECT Name, Profiles__c FROM Serasa_User_Profiles__c WHERE Name=:Constants.SERASA_CUSTOMER_CARE_PROFILES];
        Set<String> serasaProfiles = new Set<String>(serasaProfileNames.Profiles__c.Split(', '));

        if (serasaProfiles.contains(profileName))
        {
            isSerasaCustomCareProfiles = true;
        }
        else
        {
            isSerasaCustomCareProfiles = false;
        }

        }catch(exception e)
        {
            system.debug('exception in setIsSerasaCustomCareProfiles' + e.getMessage());
        }
    }

    // A helper method that is used on the page action for redirection purpose
    public String getURL() {
        PageReference NewPage;


        if (this.mysObject.RecordTypeId == null) {
          Schema.DescribeSObjectResult R = Case.SObjectType.getDescribe();
          List<Schema.RecordTypeInfo> RTs = R.getRecordTypeInfos();

          for (Schema.RecordTypeInfo RT : RTs) {
            if (RT.isAvailable() == true) {
              this.mysObject.RecordTypeId = RT.getRecordTypeId();
              break;
            }
          }
        }

        // Target record types of case
        List<String> recordTypeDevNameList = new List<String> {'Serasa_Cancellation', 'Serasa_Employee_or_Third_Party', 'Serasa_Support_Request', 'Serasa_Support_Request_Internal'};
        List<RecordType> recordTypeList = [SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName in: recordTypeDevNameList];
        Boolean recordTypeMatched = false;

        // Auto populate case field matching user's Sales_Team__c field
        User currentUser = [SELECT Id, Sales_Team__c, Business_Unit__c FROM USER WHERE Id =: UserInfo.getUserID()];

        // Check if the user selected case record type matches to target record types
        for (RecordType rt : recordTypeList) {
          if (this.mysObject.RecordTypeId == rt.Id) {
            recordTypeMatched = true;
          }
        }



        // Retrive all current page url parameters
        Map<String, String> currentUrlParameters = ApexPages.currentPage().getParameters();

        // Standard salesforce case creation url
        NewPage = new PageReference('/500/e');


        // Add url parameters back to page reference, take out parameters that are not needed.
        for (String key : currentUrlParameters.keySet()) {
          if (key != 'save_new' && key != 'sfdc.override') {
            NewPage.getParameters().put(key, currentUrlParameters.get(key));
          }
        }

        if (recordTypeMatched && Test.isRunningTest() == false) {

          //List<Object_Field_Id__c> customSettingList = [Select Name, Object_Name__c, Field_Name__c, Field_Id__c FROM Object_Field_Id__c WHERE Object_Name__c = 'Case' AND Field_Name__c = 'Island__c' LIMIT 1];

          //if (customSettingList != null && customSettingList.size() > 0 && customSettingList.get(0).Field_Id__c != null) {

            //String fieldId = customSettingList.get(0).Field_Id__c;
            //Created Field in existing custom setting to get the Case Island  Fieldid - Added by Manoj
            String userIslandFieldId = Custom_Fields_Ids__c.getOrgDefaults().Case_User_Island_Fieldid__c ;
            String caseIslandFieldId = Custom_Fields_Ids__c.getOrgDefaults().Case_Island_Fieldid__c;

            if (currentUser.Business_Unit__c == Label.User_Business_Unit_LATAM_Serasa_Customer_Care) {
              NewPage.getParameters().put(userIslandFieldId, currentUser.Sales_Team__c);
            }
            else {
              NewPage.getParameters().put(userIslandFieldId, 'Account Manager'); // translation neeced
              NewPage.getParameters().put(caseIslandFieldId, 'Sales Executive');
            }
          //}
        }

        // Tells salesforce not to redirect to the override case creation page, go to the standard one
        NewPage.getParameters().put('nooverride', '1');
        NewPage.setRedirect(true);
        
        return NewPage.getUrl();
    }
}