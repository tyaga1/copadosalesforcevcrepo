/******************************************************************************
 * Appirio, Inc
 * Name: OpportunityFreeTrial_Helper.cls
 * Description: T-323154: Clone opportunity as Free Trial
 * Created Date: Sept 30th, 2014
 * Created By: Naresh Kr Ojha;l (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Oct 16th, 2014     Pallavi Sharma               T-326259(Free Trials: new fields & Logic)
 * Oct 29th, 2014     Nathalie Le Guay             Populating Free_Trial_Parent_Opportunity__c instead of Previous_Opportunity__c
 * Nov 2nd, 2014      Nathalie Le Guay             Commenting query of the Previous_Opportunity__c field so it doesn't get copied over
 * Nov 06th, 2014     Nathalie Le Guay             OLI Type__c will be set to "One-off.." instead of "Free Trial"
 * Jan 27th, 2015     Nathalie Le Guay             I-146698: Use OLI.List_Price__c instead of OLI.ListPrice
 * Mar 04th, 2015     Arpita Bose                  T-366690: Added SaaS__c, SaaS_Product_Code__c to map on OLI
 * May 24th, 2016     Paul Kissick                 Case 01995435: Added support for returning any error found during opportunity creation
 * Aug 9th, 2016      Paul Kissick                 CRM2:W-005495: Removing fields no longer used
 ******************************************************************************/
global class OpportunityFreeTrial_Helper {
  
  webservice static String createFreeTrialOppty(String opptyID) {
    String returnResult = '';
    Opportunity oppty = [
      SELECT
        (SELECT Id, OpportunityId, SortOrder, PricebookEntryId, CurrencyIsoCode, Quantity, Discount, TotalPrice, 
                UnitPrice, /*ListPrice,*/ List_Price__c, ServiceDate, HasRevenueSchedule, HasQuantitySchedule, Description, HasSchedule, 
                Product_Type_of_Sale__c, Billing_Product__c, CRMOD_ID__c, Schedule_Established__c, DE_Product_Name__c,
                Revenue_Schedule_Amt_Per_Period__c, Revenue_Schedule_Num_of_Periods__c, Revenue_Schedule_Period_Type__c, 
                Type_of_Sale__c, End_Date__c, Finance_BU__c, Global_Business_Line__c, Opportunity_Country__c, Order_Item_ID_18__c, 
                Start_Date__c, Type__c,  Product_Billing_Product__c, CPQ_Quantity__c, EDQ_On_Demand_Product__c, 
                No_Update__c, EDQ_Margin__c, Renewal_EDQ_Margin__c, Renewal_Sale_Price__c, 
                Extended_Amount__c, Renewal_Extended_Amount__c, Partner_Amount__c, Renewal_Partner_Amount__c, Order_Type__c, 
                Program_Required__c, Data_Required__c, Manuals_Required__c, Delivery_Method__c, Program_Media__c, Data_Media__c, 
                Quantity_Type__c, Data_Usage__c, Product_Family__c, Hardware__c, Implementation__c, Compiler__c, Update_Frequency__c, 
                Renewals_Exclude__c, Manual_Registration__c, Renewal_Registration__c, Click_Expiry__c, No_Delivery__c, 
                Precise_Day_License__c , Operating_System__c, Part_Number__c, Renewal_Discount__c, 
                Discount_Amount__c, Renewal_Discount_Amount__c, Partner__c, Renewal_Partner__c, Item_Quantity__c, Users_From__c, 
                Users_To__c, Application__c, CRM_Product_Name__c, Product_Code__c, Product_Reference__c, Product_Data__c, 
                Product_Release_Name__c, Registration_Key__c, Release__c, Distribution_Notes__c, 
                Despatch_Status__c, Delivery_Time__c, Major_Version__c, CD_Name__c, Data_Installer_Password__c, Release_Reference__c, 
                Total_Royalty__c, Renewal_Total_Royalty__c, Item_Number__c, Quote_Number__c, eRenewal_Exception__c, Original_Asset_ID__c,
                SaaS__c, SaaS_Product_Code__c //T-366690
         FROM OpportunityLineItems),
        (SELECT Id, OpportunityId, UserId, TeamMemberRole, OpportunityAccessLevel, CurrencyIsoCode 
         FROM OpportunityTeamMembers),
        (SELECT Id, OpportunityId, ContactId, Role, IsPrimary 
         FROM OpportunityContactRoles),
        ZAR_Conversion_Rate__c, Win_Back_Date__c, USD_Conversion_Rate__c, 
        Type, Turn_Off_Contact_Role_Criteria_Check__c, TotalOpportunityQuantity, 
        Tech_Support_Maintenance_Tiers__c, TWD_Conversion_Rate__c, TCV_GBP__c, 
        SystemModstamp, Starting_Stage__c, Stage_prior_to_closed_lost_no_decision__c, 
        Stage_Number__c, Stage_6_Duration__c, Stage_5_Duration__c, ForecastCategoryName,
        Stage_4_Duration__c, Stage_3_Duration__c, Stage_3_Approver__c, StageName, 
        Senior_Approver__c, SGD_Conversion_Rate__c, SEK_Conversion_Rate__c, 
        Revenue_Type__c, RecordTypeId, Reached_Stage_7__c, Reached_Stage_6__c, 
        Reached_Stage_5__c, Reached_Stage_4__c, Reached_Stage_3__c, External_Contract_ID__c,
        RUB_Conversion_Rate__c, Proposal_Type__c, Product_Service_Tiers__c, 
        Product_Group__c, Prod_Id__c, Probability, Primary_Winning_Competitor__c, 
        Primary_Reason_W_L__c, Pricebook2Id, Previous_Order__c, Forecast_Category__c, 
        /*Previous_Opportunity__c, */PO_Required__c, PO_Number__c, FiscalYear, FiscalQuarter,
        PLN_Conversion_Rate__c, PCU__c, Owner_s_Sales_team__c, Owner_s_Region__c, 
        Owner_s_Country__c, Owner_s_Business_Unit__c, Owner_s_Business_Line__c, 
        Owner_Sales_Team_on_Opp_Close_Date__c, Owner_Sales_Sub_Team_on_Opp_Close_Date__c, 
        Owner_Region_on_Opp_Close_Date__c, Owner_Office_Location__c, Fiscal, External_ID_Source__c,
        Owner_Name_on_Opp_Close_Date__c, Owner_GBL_on_Opp_Close_Date__c, 
        Owner_Country_on_Opp_Close_Date__c, Owner_BU_on_Opp_Close_Date__c, 
        Owner_BL_on_Opp_Close_Date__c, OwnerId, Other_Closed_Reason__c, 
        Other_Capability__c, Originating_Task_ID__c, Originating_Contact_Id__c, 
        Option_To_Terminate__c, Option_To_Terminate_Date__c, 
        Oppty_ID_18_chars__c, Opportunity_Status_Last_Modified__c, Opportunity_Products_Count__c, 
        Opportunity_Name_PCU__c, Number_of_Times_Value_Reduced__c, ForecastCategory,
        Number_of_Times_Stage_Moved_Backwards__c, Number_of_Times_Probability_Reduced__c, 
        Number_of_Times_Close_Date_Moved_Back__c, 
        NextStep, Name, NZD_Conversion_Rate__c, NOK_Conversion_Rate__c, LeadSource,
        MYR_Conversion_Rate__c, Lost_to__c, Legacy_Opportunity_Status__c, Lead_Referrer_Email__c, 
        Key_Deal__c, KRW_Conversion_Rate__c, JPY_Conversion_Rate__c, // Is_There_Legal_Risk__c, 
        // Is_There_Financial_Risk__c, Is_There_Delivery_Risk__c, Is_There_Commercial_Risk__c,
        IsWon, 
        IsSplit, IsInApproval__c, IsClosed, Invoice_to_End_User__c, Id, 
        INR_Conversion_Rate__c, /*Has_There_Been_Significant_Change__c,*/ Has_Stage_3_Approval__c, 
        Has_Senior_Approval__c, Has_Complex_Product__c, HasOpportunityLineItem, HKD_Conversion_Rate__c, 
        GBP_Conversion_Rate__c, Free_Trial_Parent_Opportunity__c, Forecast__c, 
        Forecast_Category_on_Close__c, External_Account_ID__c, Experian_ID__c, ExpectedRevenue, 
        EUR_Conversion_Rate__c, EEK_Conversion_Rate__c, Description, Delivery_Status__c, 
        Delivery_Requirements__c, Delivery_Location__c, Delivery_Email_Alert__c, Delivery_Dates_Planned__c, 
        Delivery_Country__c, Delivery_City__c, Delivery_Address_1__c, 
        Days_in_Stage_6__c, Days_in_Stage_5__c, Days_in_Stage_4__c, Days_in_Stage_3__c, 
        Days_To_Close__c, DKK_Conversion_Rate__c, DB_Competitor__c, 
        CurrencyIsoCode, Creator_Managers_Email__c, Created_Date_2__c,
        Count_of_Renewal_Products__c, Count_of_Products__c, Contract_Term__c, Contract_Start_Date__c, 
        Contract_End_Date__c, Competitor_Count__c, Commit__c, Close_Date_Range__c, CloseDate, 
        Channel_Type__c, Capability__c, CampaignId, CNY_Conversion_Rate__c, CHF_Conversion_Rate__c,
        CAD_Conversion_Rate__c, Budget__c, BRL_Conversion_Rate__c, Annual_Contract_Value__c, 
        Amount_PCU__c, Amount_Corp__c, Amount, AccountId, Accept_Account_is_Do_Not_Deal__c, 
        AUD_Conversion_Rate__c
      FROM Opportunity
      WHERE Id = :opptyID
    ];
    
    //Save point
    Savepoint sp = Database.setSavepoint();
    
    Opportunity newOppty = oppty.clone(false, false, false, false);
    try {
      newOppty.Name = ('FT: ' + newOppty.Name).mid(0,80); // Note the Test class has this hardcoded as well
      newOppty.Type = Constants.OPPTY_TYPE_FREE_TRIAL;
      newOppty.Contract_Start_Date__c = Date.today();
      newOppty.Contract_End_Date__c = Date.today();
      newOppty.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = :Constants.OPPTY_TYPE_FREE_TRIAL AND sObjectType = 'Opportunity' LIMIT 1].Id;
      newOppty.Originating_Contact_Id__c = '';
      newOppty.Originating_Task_Id__c = '';
      newOppty.Prod_Id__c  = '';
      newOppty.StageName = Constants.OPPTY_STAGE_SETUP;
      newOppty.Free_Trial_Parent_Opportunity__c = oppty.Id;
      newOppty.Amount = 0.00;
      
      insert newOppty;
      
      OpportunityLineItem newOli;
      OpportunityContactRole newOcr;
      OpportunityTeamMember  newTeam;
      
      List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
      List<OpportunityContactRole> ocrList = new List<OpportunityContactRole>();
      List<OpportunityTeamMember> opptyTeamList = new List<OpportunityTeamMember>();
      
      //OpportunityLineItems.
      for (OpportunityLineItem oli : oppty.OpportunityLineItems) {
        newOli = oli.clone(false, false, false, false);
        newOli.Type__c = Constants.ORDER_LINE_ITEM_RENEWAL_ONE_OFF;
        newOli.Start_Date__c = Date.today();
        newOli.End_Date__c = Date.today();
        newOli.OpportunityId = newOppty.ID;
        newOli.TotalPrice = null;
        newOli.UnitPrice = 0.00;
        newOli.Order_Type__c = Constants.OPPTY_TYPE_FREE_TRIAL;
        oliList.add(newOli);
      }
      
      //OpportunityContactRoles.
      for (OpportunityContactRole ocr : oppty.OpportunityContactRoles) {
        newOcr = ocr.clone(false, false, false, false);
        newOcr.OpportunityId = newOppty.ID;
        ocrList.add(newOcr);
      }
  
      //OpportunityTeamMember    
      for (OpportunityTeamMember team : oppty.OpportunityTeamMembers) {
        newTeam = team.clone(false, false, false, false);
        newTeam.OpportunityId = newOppty.ID;
        opptyTeamList.add(newTeam);
      }
      
      insert oliList;
      insert ocrList;
      insert opptyTeamList;
      
      returnResult = newOppty.Id;
      
    }
    catch (Exception e) {
      Database.rollback(sp);
      returnResult = Label.Generic_message_for_system_error + e.getMessage();
      system.debug('[OpportunityFreeTrial_Helper: OpportunityFreeTrial_Helper] Exception: ' + e.getMessage());
      ApexLogHandler.createLogAndSave('OpportunityFreeTrial_Helper','OpportunityFreeTrial_Helper', e.getStackTraceString(), e);
    }
    return returnResult;
  }
}