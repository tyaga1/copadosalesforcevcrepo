/**=====================================================================
 * Name: AccountPlan_ClientOverview_Controller
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Mar. 3rd, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * Mar. 3rd, 2016        James Wills           Case #01848189: Created
 * Apr. 15th, 2016       Tyaga Pati            Added Prop and function to show Account List on plan overview Page
 * Apr. 20th, 2016       James Wills           Renamed Class to remove the word Tab from name.
 * May 9th, 2016         Tyaga Pati            01977170 - Changes to the Account List view to add more columns on the Account List on the plan page.
 * May 27th, 2016        James Wills           Case #01949954 AP - List views
 * June 6th, 2016        James Wills           Case #01949948 AP - Client Overview - Include Client URL
 * Oct. 24th, 2016       James Wills           Case #01983126 Added toLabel() translations to SOQL
  =====================================================================*/

global with sharing class AccountPlan_ClientOverview_Controller{

    
  public account_Plan__c accountPlan;
  public ID accountPlanId {get;set;}
  public ID accountId {get;set;}  
  //public ApexPages.standardController stdController {get;set;}
  
  public Boolean showMessages {get;set;}
  
  public String accountURL {get{
    String accountURL;    
    accountURL = [SELECT Website FROM Account WHERE id = :accountId].Website;
    
    if(accountURL!= NULL && !accountURL.contains('http://')){
      accountURL = 'http://' + accountURL;     
    }    
    return accountURL;
    
  }
  
  set;}//Case #01949948
  
  Public List<Account> PlanAccountList {
  
      get {
             return BuildAccntListForplan();
      }
          
  set;}

  //Account Plan Team list navigation parameters
  public Integer pageOffset=0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSize = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Team_Member_List__c;
  
  public Integer teamsLimit=relatedListSize;
  public Integer additionalTeamMembersToView{
    get{
      if(endAPTeamNumber+relatedListSize > totalAPTeams){
        return totalAPTeams-endAPTeamNumber;
      } else {
        return relatedListSize;
      }
    }
    set;
  }
  public Integer startAPTeamNumber {get;set;}
  public Integer endAPTeamNumber {get;set;}
  public Integer totalAPTeams {get;set;}
  
  
  public Id selectedId {get;set;}
  
  public AccountPlan_ClientOverview_Controller() {
    showMessages=false;
  }

  
  global class AccountTeamWrapper implements Comparable{
    public String ID {get;set;}
    public String FullName {get;set;}
    public String Role     {get;set;}
    public String JobTitle {get;set;}
    public String BusinessUnit {get;set;}
    public Boolean isSalesforceUser {get;set;}
    public String isSalesforceUserForDisplay {get;set;}
    
    global Integer compareTo(Object compareTo) {
       // Cast argument to OpportunityWrapper
       AccountTeamWrapper compareToATeam = (AccountTeamWrapper)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (FullName > compareToATeam.FullName) {
         // Set return value to a positive value.
         returnValue = 1;
       } else if (FullName < compareToATeam.FullName) {
         // Set return value to a negative value.
         returnValue = -1;
       }
       return returnValue;
     }
    
     
  }
  
  public List<AccountTeamWrapper> accountPlanTeamList {
    get{  
      return buildAccountPlanTeamList();
    }
    set;
  }  
  
  public List<AccountTeamWrapper> newaccountPlanTeamList {
    get; set;
  }
  
  
  public AccountPlan_ClientOverview_Controller( ApexPages.Standardcontroller std){

    accountPlan = (Account_Plan__c) std.getRecord();
    accountPlanId = accountPlan.id;
    accountId = accountPlan.Account__c;
    
  }
  
  
  Public List<Account> BuildAccntListForplan(){
  
    List<Id> accntList = New List<Id>();
    List<Account> ListAcnts = New List<Account>();
    
    for (Account_Account_Plan_Junction__c JunObj :[Select Account__c, Account__r.Name from  Account_Account_Plan_Junction__c where Account_Plan__c = :accountPlanId]){
        accntList.add(JunObj.Account__c);
      }
    for(Account accntTemp:[select parent.Name, Name, toLabel(Region__c), Country_of_Registration__c, toLabel(Industry), toLabel(Sector__c) from Account where Id in :accntList ORDER BY parent.Name ]){
        ListAcnts.add(accntTemp);
    }
    
  return ListAcnts;
  
  }
  
  
  public PageReference clientOverviewTabUpdateCookies(){
    AccountPlanUtilities.setAccountPlanCookies(accountPlanId, Label.ACCOUNTPLANNING_ClientOverviewTab);
    return null;
  }
  
  
  
  public PageReference editAccountPlanTeamMember(){
    showMessages=true;
    clientOverviewTabUpdateCookies();
    PageReference accountPlanTeamMemberToEdit = new PageReference('/' + selectedId);

    return accountPlanTeamMemberToEdit;
  }
  
  
  //==========================================================================
  // Method to delete the account team member records
  //==========================================================================
  public PageReference doDelete() {
    showMessages=true;
    clientOverviewTabUpdateCookies();
    
    Schema.DescribeSObjectResult describeResult = Account_Plan_Team__c.SObjectType.getDescribe();    
    String accountPlanObjectType = describeResult.getName();
    
    AccountPlanUtilities.deleteAccountPlanRelatedListEntry(selectedId, accountPlanObjectType, Label.ACCOUNTPLANNING_ClientOverviewTab);
    
    return null;
  }
  
  
  //==========================================================================
  // Method to fetch the existing Account Team Members
  //==========================================================================    
  public List<AccountTeamWrapper> buildAccountPlanTeamList(){
    List<AccountTeamWrapper> accountPlanTeamList = new List<AccountTeamWrapper>();
    newaccountPlanTeamList = new List<AccountTeamWrapper>();

    for(Account_Plan_Team__c ac : [SELECT id, User__r.id, User__r.Name, User__r.Title, Account_Team_Role__c, Business_Unit__c, External_Team_Member_Name__c, 
                                   External_Team_Member_Job_Title__c FROM Account_Plan_Team__c WHERE Account_Plan__c = :accountPlanId
                                   ORDER BY External_Team_Member_Name__c
                                   LIMIT :teamsLimit]){
      AccountTeamWrapper atw = new AccountTeamWrapper();
      if(ac.User__r.id!=null){
        atw.FullName = ac.User__r.Name;
        atw.JobTitle = ac.User__r.Title;
        atw.BusinessUnit = ac.Business_Unit__c;
        atw.isSalesforceUser = true;
        atw.isSalesforceUserForDisplay = Label.True;
      } else {
        atw.FullName = ac.External_Team_Member_Name__c;
        atw.JobTitle = ac.External_Team_Member_Job_Title__c;
        atw.BusinessUnit = 'n/a';
        atw.isSalesforceUser = false;
        atw.isSalesforceUserForDisplay = Label.False;   
      }
      atw.ID       = ac.ID;
      atw.Role     = ac.Account_Team_Role__c;
      
      accountPlanTeamList.add(atw);
    }
    
    accountPlanTeamList.sort();
 
    if(accountPlanTeamList.size()>0){
      startAPTeamNumber = pageOffset+1;
      endAPTeamNumber = pageOffset + accountPlanTeamList.size();
      AggregateResult[] APTeams = [SELECT COUNT(Name)TotalAPTeams FROM Account_Plan_Team__c WHERE Account_Plan__c = :accountPlanID];
      totalAPTeams = (Integer)APTeams[0].get('TotalAPTeams');
    } else {
      startAPTeamNumber = 0;
      endAPTeamNumber = 0;
      totalAPTeams = 0;
    }
    
    if(accountPlanTeamList.size()==0){
      AccountTeamWrapper atw = new AccountTeamWrapper();
      atw.FullName = 'There are no current Account Plan Team members.';
      atw.isSalesforceUser = true;
      atw.isSalesforceUserForDisplay = Label.True;
      accountPlanTeamList.add(atw);
    }
    
    AccountTeamWrapper newAccountTeamMemberList = new AccountTeamWrapper();
    
    newAccountTeamMemberList.isSalesforceUser=false;
    newAccountTeamMemberList.isSalesforceUserForDisplay = Label.False;
    
    newaccountPlanTeamList.add(newAccountTeamMemberList);
    
    
    return accountPlanTeamList;
    
  }
      
  
  public void seeMoreTeamMembers(){
    teamsLimit+=additionalTeamMembersToView;
  }


  public PageReference viewAccountPlanTeamsListFull(){
    clientOverviewTabUpdateCookies();
    PageReference newAccountPlanContactPageRef = new PageReference('/' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Team_Rec__c + 
                                                                  '?rlid=' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Team__c.mid(2,15) + '&id=' + accountPlanId);
    return newAccountPlanContactPageRef;
  }


  public void addNewExternalTeamMember(){
    List<Account_Plan_Team__c> newAccountPlanTeamExternalMemberList = new List<Account_Plan_Team__c>();
    
    clientOverviewTabUpdateCookies();
    
    for(AccountTeamWrapper newAccountTeamMember: newaccountPlanTeamList){
      Account_Plan_Team__c newAccountPlanTeamExternalMember = new Account_Plan_Team__c();
      newAccountPlanTeamExternalMember.External_Team_Member_Name__c      = newAccountTeamMember.FullName;
      newAccountPlanTeamExternalMember.Account_Team_Role__c              = newAccountTeamMember.Role;
      newAccountPlanTeamExternalMember.External_Team_Member_Job_Title__c = newAccountTeamMember.JobTitle;
      newAccountPlanTeamExternalMember.Account_Plan__c = accountPlan.id;
      
      newAccountPlanTeamExternalMemberList.add(newAccountPlanTeamExternalMember);
    }
    
    try{
      insert newAccountPlanTeamExternalMemberList;
    } catch(Exception e){
      system.debug('Problem adding Account Plan Team Members.' );
      system.debug('\n AccountPlan_ClientOverviewTab_Controller - addNewExternalTeamMember: '+ e.getLineNumber() + ' Stack:' + e.getStackTraceString() );
    }
  }

 

}