/******************************************************************************
 * Name: expcommunity_quote_ctrl.cls
 * Created Date: 1/26/2017
 * Created By: Hay Win
 * Description : Controller for expcommunity_quote_cmpt for new Exp Community
 * Change Log- 
 ****************************************************************************/
 
public with sharing class expcommunity_quote_ctrl{ 
   
    public List<Employee_Community_News__c> featured {get;set;}
    public List<Employee_Community_News__c> quotes {get;set;}
    public Integer articleNum {get;set;}
    public String appName {get;set;}
    
    public expcommunity_quote_ctrl() {
     featured = new List<Employee_Community_News__c>();
     quotes = new List<Employee_Community_News__c>();  
     articleNum = 0;         
    }    
  
    
    public boolean getQuote(){
     String appQuery = '';
        if (appName != null && appName != '') {
            appQuery = ' AND Application_Name__c =: appName ';
        } else {
            appQuery = ' AND Application_Name__c = \'EITSHome\' ';
        }
        
        String queryFeat = 'Select ID, Detail__c, Headline__c, Author__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where Content_Type__c = \'FeaturedContent\''+ appQuery  + ' AND isFeaturedArticle__c = true and isActive__c = true ORDER BY CreatedDate desc Limit 1';
        String queryQuotes = 'Select ID, Detail__c, Headline__c, Author__c, Image_Link__c, isActive__c, isFeaturedArticle__c, Synopsis__c from Employee_Community_News__c where Content_Type__c = \'FeaturedContent\''+ appQuery  + ' AND isFeaturedArticle__c != true and isActive__c = true ORDER BY CreatedDate desc Limit 2';
        
        featured = Database.query(queryFeat);
        quotes = Database.query(queryQuotes );
        articleNum = quotes.size();
        return true;
    }
}