/**=====================================================================
 * Experian, Inc
 * Name: CopadoUserStoryTriggerHandler
 * Description: US-0000026.
 * Created Date: Jul 26th, 2017
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 * 
  =====================================================================*/


public class CopadoUserStoryTriggerHandler {

  //=========================================================================
  // Before Insert Call
  //=========================================================================    
  public static void beforeInsert(List<copado__User_Story__c> newCopadoUS, Boolean isDataAdmin) {

     if (isDataAdmin == false) {
    
      
    }
  }

  //=========================================================================
  // After Insert Call
  //=========================================================================    
  public static void afterInsert(List<copado__User_Story__c> newCopadoUS, Boolean isDataAdmin) {

     
    if (isDataAdmin == false) {
    
      
   }
  }   

  //=========================================================================
  //Before Update Call
  //=========================================================================
  public static void beforeUpdate(List<copado__User_Story__c> newCopadoUS, Map<Id, copado__User_Story__c> oldCases, Boolean isDataAdmin) {

    if (isDataAdmin == false) {
    
      
    }
    
  }

  //=========================================================================
  // After Update Call
  //=========================================================================
  public static void afterUpdate(List<copado__User_Story__c> lstOld, List<copado__User_Story__c> lstNew, Map<Id, copado__User_Story__c> newMap, Map<Id, copado__User_Story__c> oldMap, Boolean isDataAdmin) {

    if (isDataAdmin == false) {
      
      syncRelatedCase(lstNew, oldMap);
    }
  }

  //=========================================================================
  // Before Delete Call
  //=========================================================================
  
  public static void beforeDelete(List<copado__User_Story__c> oldCopadoUS, Boolean isDataAdmin) {
   
   
  }

  //=========================================================================
  // Methods Called in
  //=========================================================================
  
  //=========================================================================
  // US-0000026: Created by DO, sync's related case with the changes in the User story after saving
  //=========================================================================
  public static void syncRelatedCase (List<copado__User_Story__c> lstNew, Map<Id, copado__User_Story__c> oldMap) {
    
    List<Case> casetoUpdate = new List<Case>();
    
    for (copado__User_Story__c cUS : [SELECT id, Case__c, copado__Developer__c, copado__User_Story_Title__c, Description__c, Implementation_Status__c FROM copado__User_Story__c WHERE id in: lstNew]) {
      
        for (Case c : [SELECT id, OwnerId, Subject, Description, Implementation_Status__c FROM CASE WHERE id =: cUS.Case__c]) {
          
          c.OwnerId = cUS.copado__Developer__c;
          c.Subject = cUS.copado__User_Story_Title__c;
          c.Description = cUS.Description__c;
          c.Implementation_Status__c = cUS.Implementation_Status__c;
          
          casetoUpdate.add(c);
          
        }
    }
    
    update casetoUpdate;
    
   } 
  }