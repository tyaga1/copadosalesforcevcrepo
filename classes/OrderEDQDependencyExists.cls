/**=====================================================================
 * Experian
 * Name: OrderEDQDependencyExists
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Order with an EDQ Dependency
 * Created Date: 7/21/2015
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 * Nov 9th, 2015                Paul Kissick                 Case #01266075: Optimisations for too many queries
 =====================================================================*/
global class OrderEDQDependencyExists implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
  
    return Database.getQueryLocator ([
      SELECT Id, Owner_BU_on_Order_Create_Date__c, Account__c, Saas_Order_Line_Count__c 
      FROM Order__c
      WHERE (Owner_BU_on_Order_Create_Date__c LIKE '%Data Quality%' OR Saas_Order_Line_Count__c > 0)
      AND Account__r.EDQ_Dependency_Exists__c = false
    ]);

  }

  global void execute (Database.BatchableContext bc, List<Order__c> scope) {
    
    Set<Id> accIdSet = new Set<Id>();
    
    List<Account> accountsList = new List<Account>();
    
    for(Order__c ord : scope) {
      accIdSet.add(ord.Account__c);
    }
    
    for(Id accId : accIdSet) {
      accountsList.add(
        new Account(
          Id = accId, 
          EDQ_Dependency_Exists__c = true
        )
      );
    }
    
    if(accountsList != null && accountsList.size() > 0) {
      try {             
        update accountsList;
      }
      catch (DMLException ex) {
        apexLogHandler.createLogAndSave('OrderEDQDependencyExists','execute', ex.getStackTraceString(), ex);
      }
    }
       
  }   

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'OrderEDQDependencyExists', true);
    
    if (!Test.isRunningTest()) {
      system.scheduleBatch(new AccountEDQDependencyCleaner01(),'Account - EDQ Dependency Cleanup 1 '+String.valueOf(Datetime.now().getTime()),2,ScopeSizeUtility.getScopeSizeForClass('AccountEDQDependencyCleaner01'));
    }
    
  }
    
}