/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityPlanContactTriggerHandler
 * Description: T-280558: Create trigger to delete Plan Contact Relationships
 * Created Date: May 23th, 2014
 * Created By: Naresh Kr Ojha (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * July  17th, 2014             Arpita Bose(Appirio)         I-120989: To synch currency with Opportunity Plan
 =====================================================================*/
public with sharing class OpportunityPlanContactTriggerHandler {
  
  //After delete method
  public static void beforeDelete (Map<ID, Opportunity_Plan_Contact__c> oldMap) {
    deleteAssociatedPlanContacts(oldMap);
  }

  //After unDelete method
  public static void afterUnDelete (List<Opportunity_Plan_Contact__c> newList) {
    unDeleteAssociatedPlanContacts(newList);
  }
  
  //Before Insert method
  public static void beforeInsert (List<Opportunity_Plan_Contact__c> newList) {
    synchCurrencyISOCodes(newList, null);
    linkManagerContact(newList, null);
  }
  
  
  //Before Update method
  public static void beforeUpdate (List<Opportunity_Plan_Contact__c> newList,
                                    Map<ID, Opportunity_Plan_Contact__c> oldMap ) {
    synchCurrencyISOCodes(newList, oldMap);
    linkManagerContact(newList, oldMap);
  }
  
  //delete associated plan contacts
  private static void deleteAssociatedPlanContacts (Map<ID, Opportunity_Plan_Contact__c> opptyPlanContMap) {
    
    List<Plan_Contact_Relationship__c> planContacts = new List<Plan_Contact_Relationship__c>();
    
    for (Plan_Contact_Relationship__c pcr : [SELECT ID, Contact_2__c 
                                             FROM Plan_Contact_Relationship__c 
                                             WHERE Contact_2__c  =: opptyPlanContMap.keySet()]) {
        planContacts.add(pcr);
    }
    //Deleting records
    if (planContacts.size() > 0) {
        delete planContacts;
    }
  }
  
   //delete associated plan contacts
  private static void unDeleteAssociatedPlanContacts (List<Opportunity_Plan_Contact__c> newList) {
    
    List<Plan_Contact_Relationship__c> planContacts = new List<Plan_Contact_Relationship__c>();
    
    for (Plan_Contact_Relationship__c pcr : [SELECT ID, Contact_2__c, isDeleted 
                                             FROM Plan_Contact_Relationship__c 
                                             WHERE isDeleted = true AND Contact_2__c IN: newList ALL ROWS]) {
        planContacts.add(pcr);
    }
    //Deleting records
    if (planContacts.size() > 0) {
        undelete planContacts;
    }
  }
  

  //============================================================================
  // I-120989: Method to synch currencies with Opportunity Plan
  //============================================================================
  private static void synchCurrencyISOCodes(List<Opportunity_Plan_Contact__c> newList, 
                                            Map<ID, Opportunity_Plan_Contact__c> oldMap){
    Set<String> oppPlnIDs = new Set<String>();
    List<Opportunity_Plan_Contact__c> lstOppPlnCon = new List<Opportunity_Plan_Contact__c>();
    Map<String, Opportunity_Plan__c> mapOppPlnId_OppPln = new Map<String, Opportunity_Plan__c>();
    
    //Find opp plan Ids related to Opp Plan contact
    for(Opportunity_Plan_Contact__c oPlnCon : newList){
        if(oldMap == null || (oldMap.get(oPlnCon.Id).Opportunity_Plan__c   != oPlnCon.Opportunity_Plan__c  )){
            lstOppPlnCon.add(oPlnCon);
            oppPlnIDs.add(oPlnCon.Opportunity_Plan__c );
        }
    }
    
    if(!oppPlnIDs.isEmpty()){
        for(Opportunity_Plan__c opp : [SELECT Id, CurrencyIsoCode, 
                                      (SELECT Id, CurrencyIsoCode From Opportunity_Plan_Contacts__r)
                                      From Opportunity_Plan__c 
                                      WHERE ID IN: oppPlnIDs]){
            mapOppPlnId_OppPln.put(opp.Id, opp);
        }
        // Update Currency Iso code for Opp Plan Contact
        for(Opportunity_Plan_Contact__c oppPln : lstOppPlnCon){
            oppPln.CurrencyISOCode = mapOppPlnId_OppPln.get(oppPln.Opportunity_Plan__c).CurrencyISOCode;
        }
    }                                           
    
  }
  //============================================================================
  // T-326275: Method to Link to Manager
  //============================================================================
  private static void linkManagerContact(List<Opportunity_Plan_Contact__c> newList, 
                                            Map<ID, Opportunity_Plan_Contact__c> oldMap) {
      
      map<Id, Id> mapOppPlnConManager = new map<Id, Id>();
      map<Id,Id> oppPlanIDs = new map<Id,Id>();
      map<Id, Opportunity_Plan_Contact__c> mapOppPlnCon = new map<Id, Opportunity_Plan_Contact__c>();
      Map<Id, List<Opportunity_Plan_Contact__c>> managerMappedToListOfEmployee = new Map<Id, List<Opportunity_Plan_Contact__c>>();
      map<Id, Opportunity_Plan_Contact__c> mapOppToInsert = new map<Id, Opportunity_Plan_Contact__c>();
      Opportunity_Plan_Contact__c oppPlanCon ;
      for (Opportunity_Plan_Contact__c oPlnCon : newList) {
        if (oldMap == null || (oldMap.get(oPlnCon.Id).Link_Manager__c != oPlnCon.Link_Manager__c  )) {
          if (oPlnCon.Link_Manager__c) {
            mapOppPlnCon.put(oPlnCon.Contact__c, oPlnCon);
             
          }
          else {
            oPlnCon.Reports_to__c = null;  
          }
      }
    }
    
    for (Contact con : [SELECT Id, ReportsToId 
                       FROM Contact 
                       WHERE Id IN : mapOppPlnCon.keySet()]) {
       
       oppPlanCon = mapOppPlnCon.get(con.Id);
       mapOppPlnCon.remove(con.Id); 
       
       /* 
       	This is the original below, it does not account for cases where there are multiple contacts that have the same reports to. 
       
       // reports to -> original opp plan contact
       mapOppPlnCon.put(con.ReportsToId, oppPlanCon);
       */
       
       /*
       	New Change, accomodate for multiple contacts with the same reports to. 
       */
       List<Opportunity_Plan_Contact__c> oppPlanContactList;
       if (managerMappedToListOfEmployee.containsKey(con.ReportsToId)) {
       		oppPlanContactList = managerMappedToListOfEmployee.get(con.ReportsToId);
       } else {
       		oppPlanContactList = new List<Opportunity_Plan_Contact__c>();
       }
       
       oppPlanContactList.add(oppPlanCon);
       managerMappedToListOfEmployee.put(con.ReportsToId, oppPlanContactList);
       
       // reports to -> opportunity plan
       oppPlanIDs.put(con.ReportsToId, oppPlanCon.Opportunity_Plan__c); 
    }
    
    // find all the opportunity plan contacts that are managers (map opp plan con's keyset) in the opportunity plan. 
    /* Original, changed to query for contacts from the map managerMappedToListOfEmployee instead of mapOppPlnCon
    for (Opportunity_Plan_Contact__c opc : [SELECT Id, Contact__c 
                                           FROM Opportunity_Plan_Contact__c 
                                           WHERE Contact__c IN : mapOppPlnCon.keySet() 
                                           AND Opportunity_Plan__c IN :oppPlanIDs.values()]) {
                                           	
       // map the contact id to the opportunity plan contact id
       mapOppPlnConManager.put(opc.Contact__c, opc.Id);
    } 
    */
    
    for (Opportunity_Plan_Contact__c opc : [SELECT Id, Contact__c 
                                           FROM Opportunity_Plan_Contact__c 
                                           WHERE Contact__c IN : managerMappedToListOfEmployee.keySet() 
                                           AND Opportunity_Plan__c IN :oppPlanIDs.values()]) {
                                           	
       // map the contact id to the opportunity plan contact id
       mapOppPlnConManager.put(opc.Contact__c, opc.Id);
    } 
    
    // Build the mapping between contact id to list of opportunity contacts to build the list of roles later. 
    Map<Id, List<OpportunityContactRole>> contactToContactRoleMap = new Map<Id, List<OpportunityContactRole>>();
    Set<Id> opportunityPlanIdList = new Set<Id>();
    List<Id> opportunityIdList = new List<Id>();
    Set<Id> opportunityIdSet = new Set<Id>();
    try {
	    
	    for (Opportunity_Plan_Contact__c oppPlanContact : newList) {
	    	opportunityPlanIdList.add(oppPlanContact.Opportunity_Plan__c);
	    }
	    
	    List<Opportunity_Plan__c> oppPlanList = [Select Id, Opportunity_Name__c From Opportunity_Plan__c Where Id in: opportunityPlanIdList];
	    
	    
	    for (Opportunity_Plan__c oppPlan : oppPlanList) {
	    	opportunityIdList.add(oppPlan.Opportunity_Name__c);
	    }
	    
	   
	    opportunityIdSet.addAll(opportunityIdList);
	    opportunityIdList.clear();
	    opportunityIdList.addAll(opportunityIdSet);
	    
	    List<OpportunityContactRole> opportunityContactRoleList = [select Id, ContactId, Role from OpportunityContactRole Where OpportunityId in:opportunityIdList];
	    
	    
	    for (OpportunityContactRole oppContactRole : opportunityContactRoleList) {
	    	List<OpportunityContactRole> oppContRoleList;
	    	if (contactToContactRoleMap.containsKey(oppContactRole.ContactId)) {
	    		oppContRoleList = contactToContactRoleMap.get(oppContactRole.ContactId);
	    	} else {
	    		oppContRoleList = new List<OpportunityContactRole>();
	    	}
	    	
	    	oppContRoleList.add(oppContactRole);
	    	
	    	contactToContactRoleMap.put(oppContactRole.ContactId, oppContRoleList);
	    } 
    } catch (Exception e) {
    	System.debug('Error is: ' + e.getMessage() + ' stack trace: ' + e.getStackTraceString());
    }
     
    // go through the mapp opp plan contact map (which now holds the 'reports to' ids)
    //for (Id conId : mapOppPlnCon.keySet()) {
    for (Id conId : managerMappedToListOfEmployee.keySet()){	

    	// If the manager list does not contain the 'reports to id', create a new opportunity plan contact for that manager. 
      if (!mapOppPlnConManager.containsKey(conId)) {
        oppPlanCon = new Opportunity_Plan_Contact__c();
       	List<Opportunity_Plan_Contact__c> oppPlanConList = managerMappedToListOfEmployee.get(conId);
       	
       	if (!oppPlanConList.isEmpty()) {
       		// oppPlanCon.Opportunity_Plan__c = mapOppPlnCon.get(conId).Opportunity_Plan__c;
       		
       		// The opportunity plan contact list should hold all contacts in the same opportunity plan. 
	        oppPlanCon.Opportunity_Plan__c = oppPlanConList.get(0).Opportunity_Plan__c;
	        
	        if (contactToContactRoleMap.containsKey(conId)) {
	        	
	        	// If the contact exists as a contact role, build the role list (concat of all the roles). 
	        	String roleList = '';
	        	List<OpportunityContactRole> oppContactRoleList = contactToContactRoleMap.get(conId);
	        	for (Integer i = 0; i < oppContactRoleList.size(); i++) {
	        		OpportunityContactRole oppContactRole = oppContactRoleList.get(i);
	        		
	        		if (i == oppContactRoleList.size() - 1) {
						roleList = roleList + oppContactRole.Role;   	    		
			    	} else {
			    		roleList = roleList + oppContactRole.Role + ', ';
			    	}
	        	}
	        	
	        	oppPlanCon.Role_Read_Only__c = roleList;
	        	
	        }
	        
	        oppPlanCon.Contact__c = conId;
	        mapOppToInsert.put(conId, oppPlanCon);
       	}
        
      }
    }
    if (!mapOppToInsert.isEmpty()) {
      try {
        insert mapOppToInsert.values();
      } catch (DMLException ex) {
        System.debug('[OpportunityPlanContactTriggerHandler:linkManagerContact]'+ex.getMessage()); 
        ApexLogHandler.createLogAndSave('OpportunityPlanContactTriggerHandler','linkManagerContact', ex.getStackTraceString(), ex);
        for (Integer i = 0; i < ex.getNumDml(); i++) {
          newList.get(0).addError(ex.getDmlMessage(i));
        }
      }
    }
    /* Original Code, changed to accommodate for multiple contacts with the same reports to. 
    for (Id conId : mapOppPlnCon.keySet()) {
      oppPlanCon = mapOppPlnCon.get(conId);
      
      if (mapOppPlnConManager.containsKey(conId)) {
          System.debug('con id? ' + conId);
          
          if(mapOppPlnCon.get(conId).Opportunity_Plan__c == oppPlanIDs.get(conId) ) {
             oppPlanCon.Reports_to__c = mapOppPlnConManager.get(conId);
          }
      }
      else if (mapOppToInsert.containsKey(conId)) {
      	
        oppPlanCon.Reports_to__c = mapOppToInsert.get(conId).Id;
      	
      }
    }
    */
    
    // go through the list of 'reports to' ids.  
    for (Id conId : managerMappedToListOfEmployee.keySet()) {
    	
      // get the list of contacts that reports to the 'reports to'
      List<Opportunity_Plan_Contact__c> oppPlanConList = managerMappedToListOfEmployee.get(conId);

	// go thorugh that list of contacts and set the 'reports to'
      for (Opportunity_Plan_Contact__c oppPlanConToUpdate : oppPlanConList) {
	      	if (mapOppPlnConManager.containsKey(conId)) {
	          if(oppPlanConToUpdate.Opportunity_Plan__c == oppPlanIDs.get(conId) ) {
	             oppPlanConToUpdate.Reports_to__c = mapOppPlnConManager.get(conId);
	          }
	      }
	      else if (mapOppToInsert.containsKey(conId)) {
	      	
	        oppPlanConToUpdate.Reports_to__c = mapOppToInsert.get(conId).Id;
	      	
	      }
      }
    }
  }
}