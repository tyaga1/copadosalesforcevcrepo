/**=====================================================================
 * UC INNOVATION
 * Name: ContractDetailListController
 * Description: Controller for grabbing contracts from webservice and creating ContractCancellation records based on contracts selected by user.
 * Created Date: 
 * Created By: Charlie (UC INNOVATION)
 * Date Modified      Modified By                  Description of the update
 * May 16th, 2017     Ryan (Weijie) Hu             Serasa reasons and operation type mapping logic and auto populating operation
*  =====================================================================*/
public class ContCancelReqSubmitController{

    public List<ContractWrapper> listOfContracts {get; set;}
    //public List<ContractWrapper> selectedContracts = new List<ContractWrapper>();
    public string CaseCNPJ {get;set;}
    public string CaseID {get;set;}
    public string AccountID {get;set;}
    public SerasaMainframeContractWSRequest.webserviceResponse serviceResults {get;set;}

    public Map<String, Map<String, Map<String, String>>> caseFieldsAndOperationTypeMapping;
    public Map<String, String> operationValueLabelMap {get;set;}
    public Boolean disableSaveAction {get;set;}
    //public Boolean successfulAndAtleastOne {get; set;}

    public class ContractWrapper 
    {
        public SerasaMainframeContractWSRequest.contractResult thisContract {get;set;}
        public Boolean selected {get;set;}
        public String selectedOperationType {get;set;}
        public ContractWrapper(SerasaMainframeContractWSRequest.contractResult c)
        {
            thisContract = c;
            selected = false;
        }
    }

    public ContCancelReqSubmitController(ApexPages.StandardController controller) {
        CaseID = ApexPages.currentPage().getParameters().get('id');
        Case thisCase = [SELECT ID, Account_CNPJ__c, AccountId, Island__c, Serasa_Case_Reason__c, Serasa_Secondary_Case_Reason__c FROM Case WHERE id =: caseID];
        disableSaveAction = false;

        operationTypeMappingInitialization();
        AccountID = thisCase.AccountId;
        CaseCNPJ = thisCase.Account_CNPJ__c;
        //successfulAndAtleastOne = false;
        
        serviceResults = SerasaMainframeContractWSRequest.CallGetContractsWS(CaseCNPJ);

        listOfContracts = new List<ContractWrapper>();

        for (SerasaMainframeContractWSRequest.contractResult c : serviceResults.contractResults)
        {
            ContractWrapper newContract = new ContractWrapper(c);
            try {
                // Find from map and get the value for auto-populating
                newContract.selectedOperationType = caseFieldsAndOperationTypeMapping.get(thisCase.Island__c).get(thisCase.Serasa_Case_Reason__c).get(thiscase.Serasa_Secondary_Case_Reason__c);
                
                if (newContract.selectedOperationType == null)
                {
                    disableSaveAction = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Failed to populate \'Operation Type\'<br/><br/>Island: ' + thisCase.Island__c + '<br/>Reason: ' + thisCase.Serasa_Case_Reason__c + '<br/>Secondary Reason: ' + thiscase.Serasa_Secondary_Case_Reason__c));
                    newContract.selectedOperationType = '';
                }

            }
            catch (Exception e) {
                disableSaveAction = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Failed to populate \'Operation Type\'<br/><br/>Island: ' + thisCase.Island__c + '<br/>Reason: ' + thisCase.Serasa_Case_Reason__c + '<br/>Secondary Reason: ' + thiscase.Serasa_Secondary_Case_Reason__c));
                newContract.selectedOperationType = '';
            }
            listOfContracts.add(newContract);
        }

        Schema.DescribeFieldResult operationTypeValues = Contract_Cancellation_Request__c.Operation_Type__c.getDescribe();

        operationValueLabelMap = new Map<String, String>();

        for(Schema.Picklistentry picklistEntry : operationTypeValues.getPicklistValues())
        {
            operationValueLabelMap.put(picklistEntry.getValue(), picklistEntry.getLabel());
        }
    }

/*
    public PageReference selectContract()
    {
        selectedContracts.clear();
        for(ContractWrapper cw: listOfContracts)
        {
            if (cw.selected)
            {
                selectedContracts.add(cw);
            }
        }

        return null;
    }


    public List<ContractWrapper> getSelectedContracts()
    {
        if (selectedContracts.size() > 0)
        {
            return selectedContracts;
        }

        return null;
    }
*/

    public PageReference createCancellationRecords()
    {
        List<Contract_Cancellation_Request__c> cancellationRequests = new List<Contract_Cancellation_Request__c>();

        for(ContractWrapper eachContract : listOfContracts)
        {
            if (eachContract.selected)
            {
                Contract_Cancellation_Request__c cancelReq = new Contract_Cancellation_Request__c();
                cancelReq.Contract_Number__c = eachContract.thisContract.ContractNumber;
                cancelReq.Name = eachContract.thisContract.ContractDescription;
                //cancelReq.CurrencyIsoCode = 'USD - U.S. Dollar';
                cancelReq.Case__c = CaseID;
                cancelReq.Account__c = AccountID;
                cancelReq.Operation_Type__c = eachContract.selectedOperationType;
                cancelReq.Source_System__c = eachContract.thisContract.ContractSource;
                cancelReq.Status__c = 'Pending';

                if (eachContract.selectedOperationType == 'Information' || eachContract.selectedOperationType == 'Retention')
                {
                    cancelReq.Status__c = 'Processed';
                }
                
                if (eachContract.selectedOperationType == 'Cancel today')
                {
                    cancelReq.Cancellation_Date__c = Date.today();
                }
                else if(eachContract.selectedOperationType == 'Cancel in 30 days')
                {
                    cancelReq.Cancellation_Date__c = Date.today().addDays(30);
                }
                
                cancellationRequests.add(cancelReq);
            }
        }

        if (cancellationRequests == null || cancellationRequests.isEmpty())
        {
            PageReference pageRef = ApexPages.currentPage();
            //successfulAndAtleastOne = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Please choose at least one contract to cancel.'));
            return null;
        }

        insert cancellationRequests;

        //successfulAndAtleastOne = true;
        return new PageReference('javascript:window.close()');
        //return null;
    }


    // Helper method in CONSTRUCTOR to initialize an mapping from combination of ISLAND, SERASA CASE REASON, and 2nd SERASA CASE REASON
    // to Operation type on Contract Cancellation Request
    public void operationTypeMappingInitialization() {
        caseFieldsAndOperationTypeMapping = new Map<String, Map<String, Map<String, String>>>();

        // First level (Island)- Retention
        caseFieldsAndOperationTypeMapping.put('Retention', new Map<String, Map<String, String>>());
        // First level (Island)- Back Office Retention
        caseFieldsAndOperationTypeMapping.put('Back Office Retention', new Map<String, Map<String, String>>());

        // Second level (Serasa Reason)
        caseFieldsAndOperationTypeMapping.get('Retention').put('Cancellation - BKO', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Cancellation - BKO EV', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Cancellation - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Contract - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Feedback - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Invoice - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Discount request- RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Recording request - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Retention').put('Transfer', new Map<String, String>());
        
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Cancellation - BKO', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Cancellation - BKO EV', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Cancellation - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Contract - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Feedback - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Invoice - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Discount request- RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Recording request - RT', new Map<String, String>());
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').put('Transfer', new Map<String, String>());

        // Third level (Serasa Secondary Reason)
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO').put('Schedule Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO').put('Cancellation - BKO', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO').put('Cancellation not applied', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO').put('Cancel Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO').put('In negotiations', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO EV').put('Schedule Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO EV').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO EV').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO EV').put('Cancellation - BKO EV', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO EV').put('Cancellation not applied', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - BKO EV').put('Cancel Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('withheld argument', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Retained loyalty clause', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('discount retained VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Withheld feature discount', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Withheld discount product', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('withheld downgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('withheld upgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Wait formalization', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Cancellation - RT', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Cancel in the back office', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Customer at attention', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Unaware of the contract', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Forward sales executive', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Cancellation - RT').put('Pending negotiation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('increase VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('Forward sales executive', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('Add new offer', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('New contract to another CNPJ', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('guidance cancellation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('orientation adjustment', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('Guidance on contract', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('Guidance on exchange', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Contract - RT').put('reduce VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Feedback - RT').put('Access', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Feedback - RT').put('Attendance', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Feedback - RT').put('Sales executive', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Feedback - RT').put('no cancellation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Feedback - RT').put('Services / Products', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Feedback - RT').put('Privacy violation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Invoice - RT').put('Invoice cancellation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Invoice - RT').put('Invoice correction', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Discount request- RT').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Discount request- RT').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Discount request- RT').put('Cancellation - RT', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Discount request- RT').put('Discount granted', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Discount request- RT').put('Discount not granted', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Discount request- RT').put('downgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Discount request- RT').put('upgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Recording request - RT').put('Request recording', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Promoter agreement', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Retention', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('eID', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('VIP', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Telesales', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Guidance on exchange', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('reduce VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('increase VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Serasa Consumidor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('SME Logon', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('SME', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Promoter agreement', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Retention', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('eID', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('VIP', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Telesales', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Guidance on exchange', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('reduce VCM', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('increase VCM', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('Serasa Consumidor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('SME Logon', 'Information');
        caseFieldsAndOperationTypeMapping.get('Retention').get('Transfer').put('SME', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO').put('Schedule Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO').put('Cancellation - BKO', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO').put('Cancellation not applied', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO').put('Cancel Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO').put('In negotiations', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO EV').put('Schedule Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO EV').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO EV').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO EV').put('Cancellation - BKO EV', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO EV').put('Cancellation not applied', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - BKO EV').put('Cancel Distributor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('withheld argument', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Retained loyalty clause', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('discount retained VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Withheld feature discount', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Withheld discount product', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('withheld downgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('withheld upgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Wait formalization', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Cancellation - RT', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Cancel in the back office', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Customer at attention', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Unaware of the contract', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Forward sales executive', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Cancellation - RT').put('Pending negotiation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('increase VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('Forward sales executive', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('Add new offer', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('New contract to another CNPJ', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('guidance cancellation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('orientation adjustment', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('Guidance on contract', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('Guidance on exchange', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Contract - RT').put('reduce VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Feedback - RT').put('Access', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Feedback - RT').put('Attendance', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Feedback - RT').put('Sales executive', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Feedback - RT').put('no cancellation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Feedback - RT').put('Services / Products', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Feedback - RT').put('Privacy violation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Invoice - RT').put('Invoice cancellation', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Invoice - RT').put('Invoice correction', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Discount request- RT').put('Schedule cancellation', 'Cancel in 30 days');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Discount request- RT').put('Cancel cancellation', 'Cancel the Cancellation');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Discount request- RT').put('Cancellation - RT', 'Cancel today');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Discount request- RT').put('Discount granted', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Discount request- RT').put('Discount not granted', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Discount request- RT').put('downgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Discount request- RT').put('upgrade', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Recording request - RT').put('Request recording', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('Promoter agreement', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('Retention', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('eID', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('VIP', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('Telesales', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('Guidance on exchange', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('reduce VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('increase VCM', 'Retention');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('Serasa Consumidor', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('SME Logon', 'Information');
        caseFieldsAndOperationTypeMapping.get('Back Office Retention').get('Transfer').put('SME', 'Information');
    }
}