/**=====================================================================
 * Experian
 * Name: DealAttachmentController
 * Description: Controller to display list of Encrypted attachments on the deal records
 * Created Date: Oct 28th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
public with sharing class DealAttachmentController {
  
  public class AttachmentWrapper {
    public Attachment att {get;set;}
    public Integer attSize {get;set;}
    public String attLastMod {get;set;}
    public AttachmentWrapper(Attachment a) {
      att = a;
      att.Name = att.Name.removeEnd('.enc');
      attSize = Integer.valueOf(a.BodyLength / 1024);
      attLastMod = att.LastModifiedDate.formatLong();
    }
  }
  
  Deal__c dealRecord;
  
  public DealAttachmentController(ApexPages.standardController con) {
    dealRecord = (Deal__c)con.getRecord();
  }
  
  public Id delAttachId {get;set;}
  
  public List<AttachmentWrapper> getDealAttachments() {
    List<AttachmentWrapper> attWraps = new List<AttachmentWrapper>();
    List<Attachment> atts = [
      SELECT Id, Name, BodyLength, CreatedBy.Name, LastModifiedDate
      FROM Attachment
      WHERE ParentId = :dealRecord.Id
    ];
    
    for(Attachment a : atts) {
      attWraps.add(new AttachmentWrapper(a));
    }
    return attWraps;
  } 
  
  
  public PageReference delAtt() {
    if (delAttachId != null) {
      try {
        delete new Attachment(Id = delAttachId);
      }
      catch (Exception e) {
        //
      }
    }
    return null;
  }
  
}