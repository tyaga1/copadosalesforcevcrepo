/*=============================================================================
 * Experian
 * Name: MembershipConfidentialAttachmentsExtTest
 * Description: W-005297 : Tests for MembershipConfidentialAttachmentsExt
 * Created Date: 13 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 13 Jul 2016 (QA)   Paul Kissick          CRM2:W-005297 : Tests for MembershipConfidentialAttachmentsExt
 * 18 Jul 2016 (QA)   Paul Kissick          CRM2:W-005297 : Adding extra checks
 =============================================================================*/

@isTest
private class MembershipConfidentialAttachmentsExtTest {
  
  @isTest(SeeAllData=true)
  static void attachmentListTest() {
    
    // Note, this test has to be set to SeeAllData=true to test the CombinedAttachments object in the main class. DO NOT REMOVE!
    // https://help.salesforce.com/apex/HTViewSolution?id=000213759&language=en_US
    
    Account ac1 = Test_Utils.insertAccount();
    Membership__c memb1 = Test_Utils.insertMembership(true, ac1.Id);
    Confidential_Information__c ci1 = new Confidential_Information__c(
      Name = 'Test CI 1',
      Membership__c = memb1.Id
    );
    insert ci1;
    
    Attachment at1 = new Attachment(Name = 'Test 1.txt', Body = Blob.valueOf('test'), ParentId = ci1.Id);
    Attachment at2 = new Attachment(Name = 'Test 2.doc', Body = Blob.valueOf('test'), ParentId = ci1.Id);
    Attachment at3 = new Attachment(Name = 'Test 3.xls', Body = Blob.valueOf('test'), ParentId = ci1.Id);
    
    insert new list<Attachment>{at1, at2, at3};
    
    MembershipConfidentialAttachmentsExt membConfAttExt = new MembershipConfidentialAttachmentsExt(new ApexPages.StandardController(memb1));
    
    system.assertEquals(0, membConfAttExt.allConfAtts.size());
    
    Test.startTest();
    
    membConfAttExt.loadAttachments();
    
    Test.stopTest();
    
    system.assertEquals(3, membConfAttExt.allConfAtts.size(), 'There should be 3 attachments listed.');
    system.assertEquals(false, membConfAttExt.allConfAtts[0].getHasContDocId(), 'This check should be false!');
    
  }
  
}