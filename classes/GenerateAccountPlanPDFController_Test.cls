/**=====================================================================
 * Appirio, Inc
 * Test Class Name: GenerateAccountPlanPDFController_Test
 * Class Name: GenerateAccountPlanPDFController
 * Description: T-286029: To test functionality of GenerateAccountPlanPDFController
 * Created Date: Aug 07th, 2014
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Sep 15th, 2014               Arpita Bose                  T-315815: Modified methods testGenerateAccount1() and testGenerateAccount2()
 *                                                           to increase the code coverage
 * Feb. 21st, 2017              James Wills                  Case #01999757: Added new test methods, commented out old methods to reflect changes to design.
 =====================================================================*/
@isTest
private class GenerateAccountPlanPDFController_Test {
  //private static Account_Plan__c aPlan;
  //private static User testUser;
  //private static Account_Plan_SWOT__c aSOWT;
   
    private static Set<String> validFieldsForList = new Set<String>{Label.ACCOUNT_PLAN_PDF_CUST_INFO,
                                                   Label.ACCOUNT_PLAN_PDF_OBJECTIVES, 
                                                   Label.ACCOUNT_PLAN_PDF_CLIENT_INFORMATION,
                                                   Label.ACCOUNT_PLAN_PDF_RECENT_HISTORY,
                                                   Label.ACCOUNT_PLAN_PDF_PENETRATION,
                                                   Label.ACCOUNT_PLAN_PDF_RELATIONSHIP,
                                                   Label.ACCOUNT_PLAN_PDF_RELATIONSHIP_RADAR,
                                                   Label.ACCOUNT_PLAN_PDF_DASHBOARD,
                                                   Label.ACCOUNT_PLAN_PDF_COMPETITORS,
                                                   Label.ACCOUNT_PLAN_PDF_CURRENT_PROVIDERS,
                                                   Label.ACCOUNT_PLAN_PDF_SWOT_ANALYSIS,
                                                   Label.ACCOUNT_PLAN_PDF_OPPORTUNITIES,
                                                   Label.ACCOUNT_PLAN_PDF_CRITICAL_SUCCESS_FACTORS,
                                                   Label.ACCOUNT_PLAN_PDF_NOTES,
                                                   Label.ACCOUNT_PLAN_PDF_ACCOUNT_TEAM,
                                                   Label.ACCOUNT_PLAN_PDF_CONTACTS};
  
  
   private static testmethod void test_PDFGeneration(){
    
    User user1                     = [SELECT id FROM User WHERE alias = 'Test1' LIMIT 1];
    
    Test.startTest();  
      PageReference pageRef = Page.AccountPlanPDFGenerator;        
      Test.setCurrentPage(pageRef);
      Account_Plan__c newAccountPlan  = [SELECT id FROM Account_Plan__c LIMIT 1];
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)newAccountPlan);
      AccountPlanPDFGeneratorExtension apPDFGenerator = new AccountPlanPDFGeneratorExtension(stdController); 
      apPDFGenerator.accountPlanId    = [SELECT id FROM Account_Plan__c LIMIT 1].id;
      apPDFGenerator.selectedSchemaId = [SELECT id FROM Account_Plan_PDF_Data__c WHERE Name = 'Test PDF Create' LIMIT 1].id;
      
      PageReference pageRef2 = new PageReference('/apex/GenerateAccountPlanPDF?id=' + apPDFGenerator.accountPlanId + '&schemaToUseForPDFId=' + apPDFGenerator.selectedSchemaId);
      Test.setCurrentPage(pageRef2);
      System.CurrentPageReference().getParameters().put('schemaToUseForPDFId', apPDFGenerator.selectedSchemaId);

      ApexPages.StandardController stdController2 = new ApexPages.StandardController((sObject)newAccountPlan);
      System.CurrentPageReference().getParameters().put('Id', apPDFGenerator.accountPlanId);
      
      GenerateAccountPlanPDFController apPDF = new GenerateAccountPlanPDFController(stdController2); 
      
      apPDF.getPDFSections();
         
      AccountPlanPDFSWOTAnalysisController apSWOT = new AccountPlanPDFSWOTAnalysisController();
      
      AccountPlanPDFComponentsController apCon = new AccountPlanPDFComponentsController();
      
      apCon.showExperianGoals              =true;
      apCon.showExperianStrategy           =true;
      apCon.showClientsCoreBusiness        =true;
      apCon.showCapexInOurDomain           =true;
      apCon.showOpexInOurDomain            =true;
      apCon.showClientsStrategy            =true;
      apCon.showClientsObjectives          =true;
      apCon.showClientsChanges             =true;
      apCon.showClientsCompetitors         =true;
      apCon.showClientsPartners            =true;
      apCon.showExperianAnnualisedRevenue  =true;
      apCon.showSummaryRecentHistory       =true;
      apCon.showRecentRelationshipSuccess  =true;
      apCon.showRelationshipObjNotAchieved =true;
      apCon.showRelationshipLessonsLearned =true;
      apCon.showAdditionalInformation      =true;
      
      apCon.fetchRadarChartImageId();
      
      System.assert(apCon.competitorsList.size()>0,       'GenerateAccountPlanPDFController_Test: Cannot find Account Plan Competitors.');      
      System.assert(apCon.taskList.size()>0,              'GenerateAccountPlanPDFController_Test: Task list is empty.');
      System.assert(apCon.criticalSuccessFacList.size()>0,'GenerateAccountPlanPDFController_Test: Critical Success Factor List is empty.');
      System.assert(apCon.accPlanTeamList.size()>0,       'GenerateAccountPlanPDFController_Test: Account Plan Team List is empty.');
      
      apPDFGenerator.selectedSchemaId=null;
      PageReference pageRef4 = Page.GenerateAccountPlanPDF;
                        
      Component.Apex.OutputPanel apPDFPanel = apPDF.getPDFSections();
      System.assert(apPDFPanel.childComponents.size()==validFieldsForList.size() , 'GenerateAccountPlanPDFController_Test: Sections missing from PDF.');
      
      System.assert(apPDFPanel.childComponents[0] instanceOf Component.c.AccountPlanPDFCustInfo, 'GenerateAccountPlanPDFController_Test: Section missing or not in correct place in PDF.');

      
    Test.stopTest();
  
  }
  
  private static testmethod void saveAttachment_Test(){
    PageReference pageRef = Page.GenerateAccountPlanPDF;
    pageRef.getParameters().put('mode','pdf');
    Test.setCurrentPage(pageRef);
        
    Attachment attachTest = new Attachment(Name='Account Plan.pdf');
    attachTest.body = Blob.valueOf('');
    attachTest.ParentID = [SELECT id FROM Account_Plan__c LIMIT 1].Id;
    attachTest.OwnerID = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN).id;
    insert attachTest;
        
    ApexPages.StandardController sc = new ApexPages.standardController([SELECT id FROM Account_Plan__c LIMIT 1]);
    GenerateAccountPlanPDFController gapc = new GenerateAccountPlanPDFController(sc);
        
    gapc.saveAttachment();
    
    System.assert([SELECT id FROM Attachment LIMIT 1].isEmpty()==false, 'GenerateAccountPlanPDFController_Test: Attachment was not saved.');
  } 
  
  
  @testSetup
  private static void setupData(){
    Profile profile1 = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    User user1 = Test_Utils.createUser(profile1, 'test@experian.com', 'Test1');
    
    User thisUser = [SELECT Id FROM User WHERE Id =:UserInfo.getUserId()];
    
    Account testAccount1 = Test_Utils.createAccount();  
    Contact newContact1 =  Test_Utils.createContact(testAccount1.id);
    
    System.runAs(thisUser){
      insert user1;
      insert testAccount1;
      insert newContact1;
    }
    
    
    Account_Plan__c newAccountPlan = new Account_Plan__c(Account__c = testAccount1.id);
    insert newAccountPlan;   
                                                                      
    List<Account_Plan_SWOT__c> apSWOTList = new List<Account_Plan_SWOT__c>();   
    List<String> swotType =new List<String>{'Strength','Weakness','Opportunity','Threat'};

    Integer i;
    for(i=0;i<swotType.size();i++){
      Account_Plan_SWOT__c newAccountPlanSWOT = new Account_Plan_SWOT__c(Account_Plan__c=newAccountPlan.id,
                                                                         Type__c=swotType[i],
                                                                         Who__c='Experian',
                                                                         Importance__c='5');
      apSWOTList.add(newAccountPlanSWOT);
    }
    for(i=0;i<swotType.size();i++){
      Account_Plan_SWOT__c newAccountPlanSWOT = new Account_Plan_SWOT__c(Account_Plan__c=newAccountPlan.id,
                                                                         Type__c=swotType[i],
                                                                         Who__c='Client',
                                                                         Importance__c='5');
      apSWOTList.add(newAccountPlanSWOT);
    }   
   
    insert apSWOTList;
 
 
    List<Account_Plan_Competitor__c> apcList = new List<Account_Plan_Competitor__c>();

    for(i=0;i<25;i++){
      Account_Plan_Competitor__c apc1 = new Account_Plan_Competitor__c(Account_Plan__c=newAccountPlan.id);
      apcList.add(apc1);
    }        
    insert apcList;
    
    List<Current_Provider__c> accountPlanCurrentProvidersList = new List<Current_Provider__c>();
    
    Current_Provider__c newCp1 = new Current_Provider__c(Account__c =  testAccount1.id,
                                                         Current_Provider__c = testAccount1.id,
                                                         Current_Provider_Contract_End_Date__c= Date.Today());
    accountPlanCurrentProvidersList.add(newCP1);
    
    Current_Provider__c newCp2 = new Current_Provider__c(Account__c =  testAccount1.id,
                                                         Current_Provider__c = testAccount1.id,
                                                         Current_Provider_Contract_End_Date__c= Date.Today()-1);

    accountPlanCurrentProvidersList.add(newCP2);
    
    insert accountPlanCurrentProvidersList;
    
    Account_Account_Plan_Junction__c accountPlanJunctionEntry = new Account_Account_Plan_Junction__c(Account__c = testAccount1.id,
                                                                                                     Account_Plan__c = newAccountPlan.id);
    insert accountPlanJunctionEntry;
    
    Custom_Fields_Ids__c customFields = new Custom_Fields_Ids__c(
      AP_Account_Plan_Competitor_Rec__c = 'a0z',
      AP_Account_Plan_Competitor__c     = 'CF00Ni000000EMhX5'
    );
    insert customFields;
  
    Account_Plan_Related_List_Sizes__c custListSizes = new Account_Plan_Related_List_Sizes__c(
      Account_Plan_Competitors_List__c       = 10,
      Account_Plan_Current_Providers_List__c = 10
    );
    insert custListSizes;
 
 
    DateTime eventDate1 = DateTime.now();
    
    
    List<Account_Plan_Objective__c> apoList = new List<Account_plan_Objective__c>();

    for(i=0;i<25;i++){
      Account_Plan_Objective__c accPlObj1 = new Account_Plan_Objective__c(Account_Plan__c = newAccountPlan.id);
      apoList.add(accPlObj1);
    }    
    insert apoList;
    
    
    System.runAs(thisUser){
    
      Task newTask1 = Test_Utils.createTask(newContact1.id, newAccountPlan.id);
    
      Task newTask2 = Test_Utils.createTask(newContact1.id, apoList[0].id);
    
      List<Task> taskList = new List<Task>();
      Integer i2 = 0;
      for(i2=0;i2<20;i2++){
        Task  newTaskForAccountPlan1  = new Task(WhatId=newAccountPlan.id,
                                                 WhoID = newContact1.id,
                                                 OwnerId=user1.id,
                                                 Priority='Normal',
                                                 CreatedById=user1.id,
                                                 CreatedDate=eventDate1,
                                                 TaskSubtype='Task',
                                                 Related_To__c='Account Plan',                                          
                                                 Status='Not Started');
        taskList.add(newTaskForAccountPlan1);      
      }
      insert taskList;
    }
 
    Account_Plan_Critical_Success_Factor__c apcsf = new Account_Plan_Critical_Success_Factor__c(
          Account_Plan__c = newAccountPlan.Id,
          Description__c = 'Test');
    insert apcsf;
    
       
    List<User> userList = new List<User>();  
    for(i=0;i<25;i++){
      User user2 = Test_Utils.createUser(profile1, 'test' + i + '@experian.com', 'Test' + i);
      userList.add(user2); 
    } 
    
   System.runAs(thisUser){
      insert userList;
   }
    
    //List<AccountTeamMember> atmList = new List<AccountTeamMember>();    
    for(i=0;i<25;i++){
      AccountTeamMember atm1 = Test_Utils.createAccountTeamMembers (testAccount1.id, userList[i].id, true);
      //atmList.add(atm1); 
    }
    //insert atmList;
 
    Account_Plan_Team__c apt = new Account_Plan_Team__c(User__c=thisUser.id, Account_Plan__c=newAccountPlan.id);
    insert apt;
    
    //The Account Plan PDF Data
    Account_Plan_PDF_Data__c apPDFData = new Account_Plan_PDF_Data__c();
   
    apPDFData.AccountPlanPDFCustInfo__c               =1;
    apPDFData.AccountPlanPDFClientInformation__c      =0; 
    apPDFData.AccountPlanPDFContacts__c               =0; 
    apPDFData.AccountPlanPDFAccountTeam__c            =0;
    apPDFData.AccountPlanPDFCompetitors__c            =0; 
    apPDFData.AccountPlanPDFCurrentProviders__c       =0; 
    apPDFData.AccountPlanPDFDashboard__c              =0;
    apPDFData.AccountPlanPDFNotes__c                  =0; 
    apPDFData.AccountPlanPDFObjectives__c             =0;
    apPDFData.AccountPlanPDFOpportunities__c          =0; 
    apPDFData.AccountPlanPDFPenetration__c            =0; 
    apPDFData.AccountPlanPDFRecentHistory__c          =0; 
    apPDFData.AccountPlanPDFRelationship__c           =0;
    apPDFData.AccountPlanPDFRelationshipRadar__c      =0;
    apPDFData.AccountPlanPDFSWOTAnalysis__c           =0;
    apPDFData.AccountPlanPDFCriticalSuccessFactors__c =0;
   
    insert apPDFData;
   
    Account_Plan_PDF_Data__c apPDFData2 = new Account_Plan_PDF_Data__c();
   
    apPDFData2.Name                                   ='Test PDF Create';
   
    apPDFData2.AccountPlanPDFCustInfo__c               =1;
    apPDFData2.AccountPlanPDFClientInformation__c      =2; 
    apPDFData2.AccountPlanPDFContacts__c               =3; 
    apPDFData2.AccountPlanPDFAccountTeam__c            =4;
    apPDFData2.AccountPlanPDFCompetitors__c            =5; 
    apPDFData2.AccountPlanPDFCurrentProviders__c       =6; 
    apPDFData2.AccountPlanPDFDashboard__c              =7;
    apPDFData2.AccountPlanPDFNotes__c                  =8; 
    apPDFData2.AccountPlanPDFObjectives__c             =9;
    apPDFData2.AccountPlanPDFOpportunities__c          =10; 
    apPDFData2.AccountPlanPDFPenetration__c            =11; 
    apPDFData2.AccountPlanPDFRecentHistory__c          =12; 
    apPDFData2.AccountPlanPDFRelationship__c           =13;
    apPDFData2.AccountPlanPDFRelationshipRadar__c      =14;
    apPDFData2.AccountPlanPDFSWOTAnalysis__c           =15;
    apPDFData2.AccountPlanPDFCriticalSuccessFactors__c =16;
   
    insert apPDFData2;
 
  }
  
}
  
  // testmethod with all records and for 'Experian SWOT'
  /*static testmethod void testGenerateAccount1(){
    //create test data
    createTestData(); 
    
    // start test     
    Test.startTest();
        PageReference pageRef = Page.GenerateAccountPlanPDF;
        pageRef.getParameters().put('mode','pdf');
        Test.setCurrentPage(pageRef);
        
        Attachment attachTest = new Attachment(Name='Account Plan.pdf');
        attachTest.body = Blob.valueOf('');
        attachTest.ParentID = aPlan.Id;
        attachTest.OwnerID = testUser.Id;
        insert attachTest;
        
        ApexPages.StandardController sc = new ApexPages.standardController(aPlan);
        GenerateAccountPlanPDFController gapc = new GenerateAccountPlanPDFController(sc);
        
        gapc.fetchRadarChartImageId();
        
        gapc.saveAttachment();
        
        aSOWT.Type__c = 'Strength';
        update aSOWT;
        
        sc = new ApexPages.standardController(aPlan);
        gapc = new GenerateAccountPlanPDFController(sc);
        //Asserts for strength Acc Plan SWOT
        system.assert(gapc.strengthAccPlanSWOTs <> null);
        
        aSOWT.Type__c = 'Opportunity';
        update aSOWT;
        sc = new ApexPages.standardController(aPlan);
        gapc = new GenerateAccountPlanPDFController(sc);
        //Asserts for opportunity Acc Plan SWOT
        system.assert(gapc.opportunityAccPlanSWOTs <> null);
        
        aSOWT.Type__c = 'Threat';
        update aSOWT;
        sc = new ApexPages.standardController(aPlan);
        gapc = new GenerateAccountPlanPDFController(sc);
        //Asserts for threat Acc Plan SWOT
        system.assert(gapc.threatAccPlanSWOTs <> null);
               
        // stop test
        Test.stopTest();
        
  }*/
  
  // testmethod for 'Client SWOT'
  /*static testmethod void testGenerateAccount2(){
        //create test data
        createTestData();   
        
        // start test       
        Test.startTest();
        PageReference pageRef = Page.GenerateAccountPlanPDF;
        pageRef.getParameters().put('mode','pdf');
        Test.setCurrentPage(pageRef);
        
        Attachment attachTest = new Attachment(Name='Account Plan.pdf');
        attachTest.body = Blob.valueOf('');
        attachTest.ParentID = aPlan.Id;  
        attachTest.OwnerID = testUser.Id;
        insert attachTest;
        
        ApexPages.StandardController sc = new ApexPages.standardController(aPlan);
        GenerateAccountPlanPDFController gapc = new GenerateAccountPlanPDFController(sc);
        
        gapc.fetchRadarChartImageId();
        
        gapc.saveAttachment();
         
        aSOWT.Who__c = 'Client';
        aSOWT.Type__c = 'Strength';
        update aSOWT;
        
        sc = new ApexPages.standardController(aPlan);
        gapc = new GenerateAccountPlanPDFController(sc);
        //Asserts for client strength Acc Plan SWOT
        system.assert(gapc.clientstrengthAccPlanSWOTs <> null);
        
        aSOWT.Type__c = 'Weakness';
        update aSOWT;
        sc = new ApexPages.standardController(aPlan);
        gapc = new GenerateAccountPlanPDFController(sc);
        //Asserts for client weakness Acc Plan SWOT
        system.assert(gapc.clientweaknessAccPlanSWOTs <> null);
        
        aSOWT.Type__c = 'Opportunity';
        update aSOWT;
        sc = new ApexPages.standardController(aPlan);
        gapc = new GenerateAccountPlanPDFController(sc);
        //Asserts for client Opportunity Acc Plan SWOT
        system.assert(gapc.clientopportunityAccPlanSWOTs <> null);
        
        aSOWT.Type__c = 'Threat';
        update aSOWT;
        sc = new ApexPages.standardController(aPlan);
        gapc = new GenerateAccountPlanPDFController(sc); 
        //Asserts for client threat Acc Plan SWOT
        system.assert(gapc.clientthreatAccPlanSWOTs <> null);
        
        
        // stop test
        Test.stopTest();
        
    }*/
  
  // method to create test data
  /*public static void createTestData(){
    testUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);

    Account account = Test_Utils.insertAccount();
    aPlan = Test_Utils.insertAccountPlan(false, Account.id);
    insert aPlan;
        
    Opportunity testOpp = Test_Utils.insertOpportunity(account.Id);
        
    Account_Plan_Opportunity__c aPlanOpp = Test_Utils.insertAccountPlanOpp(true, aPlan.Id, testOpp.Id);
        
    AccountTeamMember accTeam = Test_Utils.createAccountTeamMembers(account.Id, testUser.Id, true);
        
    Account_Plan_Team__c aPlanTeam = Test_Utils.insertAccountPlanTeam(true, aPlan.Id, testUser.Id);
    
    aSOWT = Test_Utils.insertAccountPlanSwot(false, aPlan.Id);
    aSOWT.Who__c = 'Experian';
    aSOWT.Type__c = 'Weakness';
    insert aSOWT;

    Task task = test_Utils.createTask(account.Id, Constants.ACTIVITY_TYPE_SELECTION_CONFIRMED, Constants.STATUS_COMPLETED);
    insert task; 
    
  }*/

//}