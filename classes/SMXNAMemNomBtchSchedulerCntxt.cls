/**=====================================================================
 * Experian
 * Name: SMXNAMemNomBtchSchedulerCntxt
 * Description: 
 * Created Date: 18/01/2016
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 * 
 =====================================================================*/
 
global class SMXNAMemNomBtchSchedulerCntxt implements Schedulable{
    
  global void execute(SchedulableContext ctx) {
    Database.executeBatch(new SMXNAProcessMembershipBatch(), ScopeSizeUtility.getScopeSizeForClass('SMXNAProcessMembershipBatch'));
  }
    
}