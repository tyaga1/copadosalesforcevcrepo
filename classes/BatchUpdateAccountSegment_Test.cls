/**=====================================================================
 * Appirio, Inc
 * Name: BatchUpdateAccountSegment_Test
 * Description: Test class for Batch job for updating Account Segment if Hierarchy__c 
 *              has Apply_Parent_Change_to_Account_Segments__c set to true
 * Created Date: Aug 28th, 2015
 * Created By: Noopur Sundriyal
 *
 * Date Modified                Modified By                Description of the update
 * 21 Sep, 2015                 Noopur                     Added the asserts in the class.   
 =======================================================================*/
@isTest
private class BatchUpdateAccountSegment_Test {

    static testMethod void myUnitTest() {
    	Account_Segment__c accSeg = [SELECT Id,Parent_Account_Segment__c,Segment__c
    	                             FROM Account_Segment__c
    	                             WHERE Value__c = 'Parent' ];
    	Id ParentId = accSeg.Parent_Account_Segment__c;
    	test.StartTest();
      BatchUpdateAccountSegment bat = new BatchUpdateAccountSegment();
      Database.executeBatch(bat);
      test.StopTest();
      
      Account_Segment__c accSeg2 = new Account_Segment__c ();
      Account_Segment__c accSeg3 = new Account_Segment__c ();
      
      for (Account_Segment__c accSg :  [ SELECT Id,Parent_Account_Segment__c,
                                                Segment__c,Value__c
			                                    FROM Account_Segment__c
			                                    WHERE Value__c = 'Parent' 
			                                    OR Value__c = 'Grand Parent']) {
	    	if ( accSg.Value__c == 'Parent') {
	    		accSeg2 = accSg;
	    	}
	    	else {
	    		accSeg3 = accSg;
	    	}
	    }
                                    
      system.assert(accSeg2.Parent_Account_Segment__c != ParentId);
      system.assert(accSeg2.Parent_Account_Segment__c == accSeg3.Id);
    }
    
    @testSetup 
	  static void createTestData() {
	    Account testAcc = Test_Utils.createAccount();
	    insert testAcc;
	
	    Hierarchy__c grandParentHierarchy = new Hierarchy__c();
	    grandParentHierarchy.Type__c = 'Global Business Line';
	    grandParentHierarchy.Value__c = 'Grand Parent';
	    grandParentHierarchy.Unique_Key__c = 'Global Business Line-Grand Parent';
	    insert grandParentHierarchy;
	
	    Hierarchy__c parentHierarchy = new Hierarchy__c();
	    parentHierarchy.Type__c = 'Business Line';
	    parentHierarchy.Value__c = 'Parent';
	    parentHierarchy.Unique_Key__c = 'Business Line-Parent';
	    parentHierarchy.Parent__c = grandParentHierarchy.Id;
	    insert parentHierarchy;
	
	    Hierarchy__c childHierarchy = new Hierarchy__c();
	    childHierarchy.Type__c = 'Business Unit';
	    childHierarchy.Value__c = 'Child';
	    childHierarchy.Parent__c = parentHierarchy.Id;
	    childHierarchy.Unique_Key__c = 'Business Unit-Child';
	    insert childHierarchy;
	
	   /* Account_Segment__c segment = Test_Utils.insertAccountSegment(false, testAcc.Id, grandParentHierarchy.Id, null);
	    segment.Type__c = 'Global Business Line';
	    segment.Value__c = 'Grand Parent';
	    insert segment;*/
	
	    Account_Segment__c segment2 = Test_Utils.insertAccountSegment(false, testAcc.Id, parentHierarchy.Id, null);
	    segment2.Type__c = 'Business Line';
	    segment2.Value__c = 'Parent';
	    insert segment2;
	
	    Account_Segment__c segment3 = Test_Utils.insertAccountSegment(false, testAcc.Id, childHierarchy.Id, segment2.Id);
	    segment3.Type__c = 'Business Unit';
	    segment3.Value__c = 'Child';
	    insert segment3;
	
	    Account_Segmentation_Mapping__c mapping = new Account_Segmentation_Mapping__c();
	    mapping.Name = 'Child';
	    mapping.Global_Business_Line__c = 'Grand Parent';
	    mapping.Business_Line__c = 'Parent';
	    mapping.Business_Unit__c = 'Child';
	    mapping.Common_View_Name__c = 'CSDA';
	    mapping.Field_Set_API_Name__c = 'CSDA';
	    insert mapping;
	    
	    parentHierarchy.Old_Parent__c = 'test Old Parent';
	    parentHierarchy.Apply_Parent_Change_to_Account_Segments__c = true;
	    parentHierarchy.Processed_Renaming__c = false;
	    update parentHierarchy;
	  }
}