/**********************************************************************************
 * Experian
 * Name: DeploymentCaseTriggerHandler_Test
 * Description: Test Class for DeploymentCaseTriggerHandler
 *             
 * Created Date: Oct 20th, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified       Modified By                  Description of the update
 * Jul 27, 2016        Paul Kissick                 W-005537 : Adding support for AA Work 
 *********************************************************************************/
@isTest
private class DeploymentCaseTriggerHandler_Test {

  static testMethod void testDeletingCase() {
    Test.startTest();
    
    Integer count1 = [SELECT COUNT() FROM Deployment_Component__c];
    
    Deployment_Cases__c dca1 = [SELECT Id FROM Deployment_Cases__c WHERE Case__c != null LIMIT 1]; 
    
    delete dca1;
    
    Test.stopTest();
    
    Integer count2 = [SELECT COUNT() FROM Deployment_Component__c];
    
    system.assertNotEquals(count1,count2,'Failed to delete components');
  }
  
  
  static testMethod void testDeletingStory() {
    Test.startTest();
    
    Integer count1 = [SELECT COUNT() FROM Deployment_Component__c];
    
    Deployment_Cases__c dca1 = [SELECT Id FROM Deployment_Cases__c WHERE Story_Name__c != null LIMIT 1]; 
    
    delete dca1;
    
    Test.stopTest();
    
    Integer count2 = [SELECT COUNT() FROM Deployment_Component__c];
    
    system.assertNotEquals(count1,count2,'Failed to delete components');
  }
  
  static testMethod void testDeletingAAWork() {
    Test.startTest();
    
    Integer count1 = [SELECT COUNT() FROM Deployment_Component__c];
    
    Deployment_Cases__c dca1 = [SELECT Id FROM Deployment_Cases__c WHERE Work__c != null LIMIT 1]; 
    
    delete dca1;
    
    Test.stopTest();
    
    Integer count2 = [SELECT COUNT() FROM Deployment_Component__c];
    
    system.assertNotEquals(count1,count2,'Failed to delete components');
  }
  
  @testSetup
  private static void setupTestData() {
    
    User u1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert u1;
    
    // Need to Query permission set name 'Agile Accelerator Admin' to let the test class pass.
    system.runAs(new User(Id = UserInfo.getUserId())) {
      PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Agile_Accelerator_Admin'];
    
      // Assign the above inserted user for the Agile Accelerator Admin Permission Set.
      PermissionSetAssignment psa = new PermissionSetAssignment();
      psa.AssigneeId = u1.id; // tstUser.Id;
      psa.PermissionSetId = ps.Id;
      insert psa;
    }
    
    Account a1 = Test_Utils.insertAccount();
    Case c1 = Test_Utils.insertCase(true,a1.Id);
    Story__c s1 = new Story__c();
    insert s1;
    
    Metadata_Component__c md1 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field1__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    Metadata_Component__c md2 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field2__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    insert new Metadata_Component__c[]{md1,md2};
    
    Case_Component__c cc1 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc2 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc3 = new Case_Component__c(
      Action__c = 'Create',
      Story__c = s1.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc4 = new Case_Component__c(
      Action__c = 'Create',
      Story__c = s1.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );

    insert new Case_Component__c[]{cc1,cc2, cc3, cc4};
    
    Salesforce_Environment__c src = new Salesforce_Environment__c(Name = 'Source');
    Salesforce_Environment__c trg = new Salesforce_Environment__c(Name = 'Target');
    insert new Salesforce_Environment__c[]{src,trg};
    
   
    
    Deployment_Request__c dr1 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release__c = 'Support Release 1',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id
    );

    Deployment_Request__c dr2 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release__c = 'Support Release 2',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id
    );
    
    Deployment_Request__c dr3 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release__c = 'Project Release 1',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id
    );
    insert new List<Deployment_Request__c>{dr1, dr2, dr3};
    
    Deployment_Component__c dco1 = new Deployment_Component__c(
      Case_Component__c = cc1.Id,
      Slot__c = dr1.Id
    );
    Deployment_Component__c dco2 = new Deployment_Component__c(
      Case_Component__c = cc2.Id,
      Slot__c = dr1.Id
    );
    Deployment_Component__c dco3 = new Deployment_Component__c(
      Case_Component__c = cc3.Id,
      Slot__c = dr2.Id
    );
    Deployment_Component__c dco4 = new Deployment_Component__c(
      Case_Component__c = cc4.Id,
      Slot__c = dr2.Id
    );
    
    insert new Deployment_Component__c[]{dco1,dco2,dco3,dco4};
    
    Deployment_Cases__c dca1 = new Deployment_Cases__c(
      Case__c = c1.Id,
      Deployment_Request_Slot__c = dr1.Id
    );
    
    Deployment_Cases__c dca2 = new Deployment_Cases__c(
      Story_Name__c = s1.Id,
      Deployment_Request_Slot__c = dr2.Id
    );
    
    // AA Support
    
    //Create Scrum Team
    agf__ADM_Scrum_Team__c newTeam  = new agf__ADM_Scrum_Team__c(Name = 'Test Team',agf__Active__c=true,agf__Cloud__c ='IT');
    insert newTeam;
    
    //Create Agile Product Tag
    agf__ADM_Product_Tag__c  newProductTag = new agf__ADM_Product_Tag__c(Name = 'GCSS Product Team',agf__Team__c =newTeam.Id,agf__Team_Tag_Key__c ='123', agf__Active__c=true );
    insert newProductTag;
      
    Id userStoryTypeId = DescribeUtility.getRecordTypeIdByName('agf__ADM_Work__c', 'User Story');
    
    agf__ADM_Work__c agileStory = new agf__ADM_Work__c();
    
    system.runAs(u1) { 
      //Create Agile Story
      agileStory.agf__Assignee__c = u1.Id;
      agileStory.agf__Subject__c = 'Test Agile Story';
      agileStory.agf__Status__c = 'New';
      agileStory.RecordTypeId = userStoryTypeId;
      agileStory.agf__Description__c = 'Agile Description goes here';
      agileStory.agf__Product_Tag__c = newProductTag.Id;
      insert agileStory;
    }
    
    Case_Component__c cc5 = new Case_Component__c(
      Action__c = 'Create',
      User_Story_AA__c = agileStory.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc6 = new Case_Component__c(
      Action__c = 'Create',
      User_Story_AA__c = agileStory.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    
    insert new Case_Component__c[]{cc5, cc6};
    
    Deployment_Component__c dco5 = new Deployment_Component__c(
      Case_Component__c = cc5.Id,
      Slot__c = dr3.Id
    );
    Deployment_Component__c dco6 = new Deployment_Component__c(
      Case_Component__c = cc6.Id,
      Slot__c = dr3.Id
    );
    
    insert new Deployment_Component__c[]{dco5, dco6};
    
    Deployment_Cases__c dca3 = new Deployment_Cases__c(
      Work__c = agileStory.Id,
      Deployment_Request_Slot__c = dr3.Id
    );
    
    insert new Deployment_Cases__c[]{dca1, dca2, dca3};
    
  }
}