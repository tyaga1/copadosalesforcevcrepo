/*=============================================================================
 * Experian
 * Name: BatchAccHierTypeCalculation
 * Description: W-005381 : Create a view based on the child accounts to show the overall relationship for a company.
 *                         Schedule to run Everyday at 12AM.
 * Created Date: 28 Aug 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 * 29th Aug 2016      Manoj Gopu            Create a view based on the child accounts to show the overall relationship for a company.
 * 2nd Sep, 2016      Paul Kissick          CRM2:W-005381: Added batch helper and removed extra query in execute, start query no longer a string and formatted.
 ============================================================================*/

public class BatchAccHierTypeCalculation implements Database.Batchable<sObject> {

  //start method of batch class
  public Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator([
      SELECT Id, Name, Account_Type__c, Account_Hierarchy_Relationship_Type__c, ParentId
      FROM Account
    ]);
  }

  //execute method of batch class
  public void execute(Database.BatchableContext BC, List<Account> lstAcc){

    Set<Id> lstIds = (new Map<Id, Account>(lstAcc)).keySet();

    //Get the child accounts for every account
    Map<Id, List<Account>> mapAccIdToChildAccts = new Map<Id, List<Account>>();

    for (Account objAcc : [SELECT Id, Name, Account_Type__c, Account_Hierarchy_Relationship_Type__c, ParentId
                           FROM Account
                           WHERE ParentId IN :lstIds]) {
      if (mapAccIdToChildAccts.containsKey(objAcc.ParentId)) {
        List<Account> acntList = mapAccIdToChildAccts.get(objAcc.ParentId);
        acntList.add(objAcc);
        mapAccIdToChildAccts.put(objAcc.ParentId, acntList);
      }
      else {
        List<Account> acntList = new List<Account>();
        acntList.add(objAcc);
        mapAccIdToChildAccts.put(objAcc.ParentId, acntList);
      }
    }

    //Checking the conditions to update type
    for (Account objAcc : lstAcc) {
      if (mapAccIdToChildAccts.containsKey(objAcc.Id)) {
        objAcc.Account_Hierarchy_Relationship_Type__c = calculateAccountHierarchyType(objAcc, mapAccIdToChildAccts.get(objAcc.Id));
      }
      else {
        objAcc.Account_Hierarchy_Relationship_Type__c = objAcc.Account_Type__c;
      }
    }
    Database.update(lstAcc);
  }

  //Method is used to calculate Account Hierarchy Type based on It's type and child account's types
  public String calculateAccountHierarchyType (Account currentAcc, List<Account> childAccts) {
    String strType = currentAcc.Account_Type__c;
    for (Account chAcc : childAccts) {
      //If any one of the child account's type is client, then it should be client
      if (chAcc.Account_Type__c == Constants.BU_RELATIONSHIP_TYPE_CLIENT) {
        strType = Constants.BU_RELATIONSHIP_TYPE_CLIENT;
      }
      //if child account's type is former client and no other child type as client then it should be former client
      else if (chAcc.Account_Type__c == Constants.BU_RELATIONSHIP_TYPE_FORMER_CLIENT && strType != Constants.BU_RELATIONSHIP_TYPE_CLIENT) {
        strType = Constants.BU_RELATIONSHIP_TYPE_FORMER_CLIENT;
      }
      //if all child accout's type is prospect then it should be prospect
      else if (chAcc.Account_Type__c == Constants.BU_RELATIONSHIP_TYPE_PROSPECT && strType == Constants.BU_RELATIONSHIP_TYPE_PROSPECT) {
        strType = Constants.BU_RELATIONSHIP_TYPE_PROSPECT;
      }
    }
    return strType;
  }

  public void finish(Database.BatchableContext BC){
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchAccHierTypeCalculation', true);
  }

}