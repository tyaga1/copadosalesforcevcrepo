public with sharing class OpportunityPlanCompetitorController {
    private final Opportunity_Plan_Competitor__c opportunityPlanCompetitor ;
    
    public String pageTitle{get;set;}
    public boolean showSaveButton{get;set;}
    public boolean isEdit {get; set;}
    public List<Boolean> expAdvantage {get; set;}
    public List<Boolean> compAdvantage {get; set;}
    public List<Boolean> compStrategy {get; set;}
    public List<Boolean> expStrategy {get; set;}
    ApexPages.StandardController stdController;
    
    Integer currentExpAdvantage = 1;
    Integer currentCompAdvantage = 1;
    Integer currentCompStrategy = 1;
    Integer currentExpStrategy = 1;

    String error_message = 'You have reached the maximum of 5 entries for this section, no more can be added.';
    
    public OpportunityPlanCompetitorController(ApexPages.StandardController stdController) {
        isEdit = false;
        this.stdController = stdController;
        opportunityPlanCompetitor = (Opportunity_Plan_Competitor__c)stdController.getRecord();
        
        expAdvantage  = new List<Boolean>{true, false, false, false, false};
        compAdvantage  = new List<Boolean>{true, false, false, false, false};
        CompStrategy  = new List<Boolean>{true, false, false, false, false};
        expStrategy  = new List<Boolean>{true, false, false, false, false};
        
        // It is an edit if there is an ID in the parameters.
        if (ApexPages.currentPage().getParameters().containsKey('Id')) {
            isEdit = true;
            pageTitle = opportunityPlanCompetitor.Name;
            
            // Set rendering for the experian advantages
            setRendering(opportunityPlanCompetitor.Exp_Adv_2__c, 1, expAdvantage);
            setRendering(opportunityPlanCompetitor.Exp_Adv_3__c, 2, expAdvantage);
            setRendering(opportunityPlanCompetitor.Exp_Adv_4__c, 3, expAdvantage);
            setRendering(opportunityPlanCompetitor.Exp_Adv_5__c, 4, expAdvantage);
            
            Integer expAdvantageLastTrue = getLastTrue(expAdvantage);
            adjustDisplay(expAdvantage, expAdvantageLastTrue);
            currentExpAdvantage = getLastTrue(expAdvantage) + 1; 


            // Set rendering for competitor advantages
            setRendering(opportunityPlanCompetitor.Comp_Adv_2__c,  1, compAdvantage);
            setRendering(opportunityPlanCompetitor.Comp_Adv_3__c,  2, compAdvantage);
            setRendering(opportunityPlanCompetitor.Comp_Adv_4__c,  3, compAdvantage);
            setRendering(opportunityPlanCompetitor.Comp_Adv_5__c,  4, compAdvantage);
            
            Integer compAdvantageLastTrue = getLastTrue(compAdvantage);
            adjustDisplay(compAdvantage, compAdvantageLastTrue);
            currentCompAdvantage = getLastTrue(compAdvantage) + 1;
            
            
            // Set rendering for Competitor strategy
            setRendering(opportunityPlanCompetitor.Comp_Strat_2__c,  1, compStrategy);
            setRendering(opportunityPlanCompetitor.Comp_Strat_3__c,  2, compStrategy);
            setRendering(opportunityPlanCompetitor.Comp_Strat_4__c,  3, compStrategy);
            setRendering(opportunityPlanCompetitor.Comp_Strat_5__c,  4, compStrategy);
            
            Integer compStrategyLastTrue = getLastTrue(compStrategy);
            adjustDisplay(compStrategy, compStrategyLastTrue);
            currentCompStrategy = getLastTrue(compStrategy) + 1;
            
            // Set rendering for Experian Strategy
            setRendering(opportunityPlanCompetitor.Exp_Strat_2__c,  1, expStrategy);
            setRendering(opportunityPlanCompetitor.Exp_Strat_3__c,  2, expStrategy);
            setRendering(opportunityPlanCompetitor.Exp_Strat_4__c,  3, expStrategy);
            setRendering(opportunityPlanCompetitor.Exp_Strat_5__c,  4, expStrategy);
            
            Integer expStrategyLastTrue = getLastTrue(expStrategy);
            adjustDisplay(expStrategy, expStrategyLastTrue);
            currentExpStrategy = getLastTrue(expStrategy) + 1;
            
        } else {
        
            pageTitle = 'New Opportunity Plan Competitor';
            isEdit = false;
        }
    }
    
    public void adjustDisplay(List<BOolean> renderList, Integer lastTrue) {
        for (Integer i = 0; i < lastTrue; i++) {
            renderList[i] = true;
        }
    }
    
    public integer getLastTrue(List<Boolean> renderList) {
        for (Integer i = renderList.size() - 1; i >= 0; i--) {
            if (renderList[i] == true) {
                return i;
            }
        }
        
        return 0;
    }
    
    public void setRendering(String leftField, Integer rowNumber, List<Boolean> renderList) {
        if ((leftField != null &&  leftField!= '') /*|| (rightField != null && rightField != '--None--')*/) {
            renderList[rowNumber] = true;  
        }
    }
    
    public pageReference addEXPAdvantage() {
        if (currentExpAdvantage < 5) {
            expAdvantage[currentExpAdvantage] = true;
            currentExpAdvantage++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;
    }
    
    public pageReference addCompetitorAdvantage() {
        if (currentCompAdvantage < 5) {
            compAdvantage[currentCompAdvantage] = true;
            currentCompAdvantage++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;
    }
    
    public pageReference addCompetitorStrategy() {
        if (currentCompStrategy < 5) {
            CompStrategy[currentCompStrategy] = true;
            currentCompStrategy++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;   
    }
    
    public pageReference addEXPStrategy() {
        if (currentExpStrategy < 5) {
            expStrategy[currentExpStrategy] = true;
            currentExpStrategy++;
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, error_message ));
        return null;   
    }

    private boolean noGap_experianAdvantages(){
        List<boolean> expAdv_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanCompetitor.Exp_Adv_1__c != null ) expAdv_has_content[0] = true;
        if(this.opportunityPlanCompetitor.Exp_Adv_2__c != null ) expAdv_has_content[1] = true;
        if(this.opportunityPlanCompetitor.Exp_Adv_3__c != null ) expAdv_has_content[2] = true;
        if(this.opportunityPlanCompetitor.Exp_Adv_4__c != null ) expAdv_has_content[3] = true;
        if(this.opportunityPlanCompetitor.Exp_Adv_5__c != null ) expAdv_has_content[4] = true;
        return noGap_Helper(expAdv_has_content);
    }

    private boolean noGap_competitorAdvantages(){
        List<boolean> comAdv_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanCompetitor.Comp_Adv_1__c != null ) comAdv_has_content[0] = true;
        if(this.opportunityPlanCompetitor.Comp_Adv_2__c != null ) comAdv_has_content[1] = true;
        if(this.opportunityPlanCompetitor.Comp_Adv_3__c != null ) comAdv_has_content[2] = true;
        if(this.opportunityPlanCompetitor.Comp_Adv_4__c != null ) comAdv_has_content[3] = true;
        if(this.opportunityPlanCompetitor.Comp_Adv_5__c != null ) comAdv_has_content[4] = true;
        return noGap_Helper(comAdv_has_content);
    }

    private boolean noGap_competitorStrategy(){
        List<boolean> comStra_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanCompetitor.Comp_Strat_1__c != null ) comStra_has_content[0] = true;
        if(this.opportunityPlanCompetitor.Comp_Strat_2__c != null ) comStra_has_content[1] = true;
        if(this.opportunityPlanCompetitor.Comp_Strat_3__c != null ) comStra_has_content[2] = true;
        if(this.opportunityPlanCompetitor.Comp_Strat_4__c != null ) comStra_has_content[3] = true;
        if(this.opportunityPlanCompetitor.Comp_Strat_5__c != null ) comStra_has_content[4] = true;
        return noGap_Helper(comStra_has_content);
    }
    
    private boolean noGap_experianStrategy(){
        List<boolean> exp_has_content = new List<boolean>{false, false, false, false, false};
        if(this.opportunityPlanCompetitor.Exp_Strat_1__c != null ) exp_has_content[0] = true;
        if(this.opportunityPlanCompetitor.Exp_Strat_2__c != null ) exp_has_content[1] = true;
        if(this.opportunityPlanCompetitor.Exp_Strat_3__c != null ) exp_has_content[2] = true;
        if(this.opportunityPlanCompetitor.Exp_Strat_4__c != null ) exp_has_content[3] = true;
        if(this.opportunityPlanCompetitor.Exp_Strat_5__c != null ) exp_has_content[4] = true;
        return noGap_Helper(exp_has_content);
    }

    private boolean noGap_Helper(List<Boolean> lis){
        Integer lastTrue = getLastTrue(lis);
        if( lis[0] == false && lastTrue > 0 ) return false;
        // from the first one to the last one that is true, if there's anything inbetween
        for(Integer i = 1; i <= lastTrue; i++){
            if( lis[i] == false ){
                return false;   // if anything inbetween is false, there's a gap, return false
            }
        }
        // if anything inbetween is true, there's no gap, return true
        return true;
    }

    public pageReference save(){
        // pop up error message if there's a gap
        if( noGap_experianAdvantages() == false ){
            // pop up error message
            String message = 'Please fill in all Experian Advantages before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }

        // pop up error message if there's a gap
        if( noGap_competitorAdvantages() == false ){
            // pop up error message
            String message = 'Please fill in all Competitor Advantages before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }

        // pop up error message if there's a gap
        if( noGap_competitorStrategy() == false ){
            // pop up error message
            String message = 'Please fill in all Competitor Strategy before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }

        if( noGap_experianStrategy() == false ){
            // pop up error message
            String message = 'Please fill in all Experian Strategy before saving.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message ));
            return null;
        }


        try {
            if( isEdit == true ){
                update this.opportunityPlanCompetitor;
            }
            else{
                insert this.opportunityPlanCompetitor;
            }
        } catch (Exception e) {
            ApexPages.addMessages(e);  
            return null;
        }
        
        PageReference pr = new PageReference('/' + this.opportunityPlanCompetitor.Id);
        // Don't redirect with the viewstate of the current record.
        pr.setRedirect(true);
        return pr;
    }

    public pageReference saveAndNew() {
    
        try {
            pageReference tem = this.save();
            if(tem == null) return null;
            
            // Get the Meta Data for Foo__c
            Schema.DescribeSObjectResult describeResult = stdController.getRecord().getSObjectType().getDescribe();
            
            // Create PageReference for creating a new sObject and add any inbound query string parameters.
            PageReference pr = new PageReference('/apex/OpportunityPlanCompetitor');
            // Don't redirect with the viewstate of the current record.
            pr.setRedirect(true);
            return pr;
            
            
        } catch (Exception e) {
            ApexPages.addMessages(e);  
            
            return null;
        }
    }
    
    
    
}