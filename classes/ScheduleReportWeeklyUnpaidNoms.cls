/**=====================================================================
 * Name: ScheduleReportWeeklyUnpaidNoms 
 * Description: ScheduleReportMonthly is used to send an Email Notification with the link to the report every month 
 * Created Date: Jan 24th, 2017
 * Created By: Manoj Gopu - Case : 02268605
 * 
 * Date Modified     Modified By        Description of the update
 
 =====================================================================*/

Public class ScheduleReportWeeklyUnpaidNoms implements Schedulable {
   Public void execute(SchedulableContext SC) {
                  
        EmailTemplate templateId = [Select id,Subject,Body from EmailTemplate where name = 'Unpaid Approved Nominations Every Week'];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'grhr@experian.com'];
        
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        for(GroupMember gm:[SELECT Id, group.id, group.name, group.type,UserOrGroupId  FROM GroupMember where group.name='Recognition Award Committee']){
            string strUserId = gm.UserOrGroupId;
            if(strUserId.startsWith('005')){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSubject(templateId.Subject); 
                mail.setPlainTextBody(templateId.Body);
                mail.setSaveAsActivity(false);
                mail.setTargetObjectId(gm.UserOrGroupId);
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                allmsg.add(mail);
            }
        }
        Messaging.sendEmail(allmsg);
    }
   
}