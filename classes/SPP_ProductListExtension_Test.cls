/**=====================================================================
 * Name: SPP_ProductListExtension_Test
 * Description: Test for SPP_ProductListExtension                 
 * Created Date: Aug 17th, 2017
 * Created By: Mauricio Murillo
 * 
 * Date Modified                Modified By                  Description of the update
**=====================================================================*/
@isTest
public class SPP_ProductListExtension_Test{

    @isTest
    public static void testGetOppLineItems(){
    
      // Create an account    
      Account testAccount = Test_Utils.insertAccount();
      // Create an opportunity
      Opportunity testOpp = Test_Utils.createOpportunity(testAccount.Id);
      testOpp.Type = Constants.OPPTY_NEW_FROM_EXISTING ;
      insert testOpp;
      
      //////////////////////
      // Create Opportunity Line Item
      Product2 product = Test_Utils.insertProduct();
            
      //Pricebook2 pricebook = Test_Utils.getPriceBook2();
      //Pricebook2 standardPricebook = Test_Utils.getPriceBook2('Standard Price Book');
      PricebookEntry stdPricebookEntry = Test_Utils.insertPricebookEntry(product.Id, Test.getStandardPricebookId(), Constants.CURRENCY_USD);
      //insert OLI
      OpportunityLineItem opportunityLineItem2 = Test_Utils.insertOpportunityLineItem(testOpp.Id, stdPricebookEntry.Id, testOpp.Type);      
      
      Sales_Planning_Process__c spp = new Sales_Planning_Process__c();
      spp.Account__c = testOpp.AccountId;
      spp.Opportunity__c = testOpp.Id;
      //spp.OwnerId = testOpp.OwnerId;
      insert spp;
      
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)spp);
      SPP_ProductListExtension sppExtension = new SPP_ProductListExtension(stdController);
      List<OpportunityLineItem> lineItems = sppExtension.getOppLineItems();
      System.assertEquals(lineItems.size(),1); 
        
    }
    
    
}