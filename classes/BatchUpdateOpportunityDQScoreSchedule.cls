/**=====================================================================
 * Name: BatchUpdateOpportunityDQScoreSchedule
 * Description: CRM2 W-005667: Implements Schedule for updateOpportunityDQScoreBatch
 * Created Date: Aug 18th, 2016
 * Created By: Cristian Torres
 * 
 * Date Modified        Modified By          Description of the update
 * 25 Aug, 2016         Paul Kissick         CRM2 W-005667:Cleaned this up and removed hard coded scope size.   
 =====================================================================*/

public class BatchUpdateOpportunityDQScoreSchedule implements Schedulable{

  public void execute(SchedulableContext sc) {
    Database.executeBatch(new BatchUpdateOpportunityDQScore(), ScopeSizeUtility.getScopeSizeForClass('BatchUpdateOpportunityDQScore'));
  }

    /*  
     * This code should be run in order to schedule the job:
     *
      
     system.schedule('Opportunity - Update Opportunity DQ Score Graphic', '0 42 23 * * ? *', new BatchUpdateOpportunityDQScoreSchedule()); 
     
   */

}