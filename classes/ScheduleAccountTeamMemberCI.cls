/**=====================================================================
 * Appirio, Inc
 * Name: ScheduleAccountTeamMemberCI 
 * Description: Story:      S-277737
 *              Task:       T-357474
 *              The purpose is to Sync the Account Team Member with the sharing records on the
 *              Confidential Information, since we can't have triggers on Account Team Member
 * Created Date: Mar 23rd, 2015
 * Created By: Nathalie Le Guay (Appirio)
 * 
 * Date Modified     Modified By        Description of the update
 * Jul 20th, 2015    Paul Kissick       Case #01029615 - Changing batch size to more managable number, 20. Was 200 
 * Aug 17th, 2015    Paul Kissick       Case #01029615 - Changing batch size again to 1, Was 20.
 * Aug 20th, 2015    Paul Kissick       Case #01029615 - Changing batch size again to 100, Was 20.
 * Nov 9th, 2015     Paul Kissick       Case #01230646 - Batch is failing due to APEX CPU Time limit exception, so adding batch scope changes
 =====================================================================*/
global class ScheduleAccountTeamMemberCI implements Schedulable {
  global void execute(SchedulableContext SC) {
    BatchAccountTeamMemberCI batch = new BatchAccountTeamMemberCI();
    Database.executeBatch(batch, ScopeSizeUtility.getScopeSizeForClass('BatchAccountTeamMemberCI'));
  }
}