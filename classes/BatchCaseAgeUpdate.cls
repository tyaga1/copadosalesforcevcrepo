/*=============================================================================
 * Experian
 * Name: BatchCaseAgeUpdate
 * Description: Case 02092488
                Split the original Scheduled class into a batch since the updates were timing out
                due to the number of cases being updated.
 * Created Date: 4 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 *
 =============================================================================*/

public class BatchCaseAgeUpdate implements Database.Batchable<sObject>, Database.Stateful {

  private Datetime startTime;

  @testVisible private static String RECORDTYPE_CASE_EMS = 'EMS';
  @testVisible private static String CASE_STATUS_IN_PROGRESS = 'In progress';
  @testVisible private static String CASE_STATUS_ON_HOLD = 'On Hold';

  public BatchCaseAgeUpdate () {
  }

  public Database.Querylocator start (Database.Batchablecontext bc) {
    startTime = Datetime.now();
    return Database.getQueryLocator([
      SELECT Id, CaseNumber, BusinessHoursId,Business_Hours_Afternoon__c,
             IsClosed, CreatedDate, ClosedDate, BusinessHours.Name,
             Case_Age__c, Age_Calculated__c, Is_Age_Calculating__c, Case_Age_Text__c,
             X2_Hours_In_In_Progress__c, Status ,RecordTypeId, Type,
             Recording_Time__c,
             (SELECT Id, CaseId, CreatedDate, Field, OldValue, NewValue
              FROM Histories
              WHERE Field = 'Status')
      FROM Case
      WHERE Age_Calculated__c = false
      AND RecordType.Name = :RECORDTYPE_CASE_EMS
    ]);
  }

  public void execute (Database.BatchableContext bc, List<Case> scope) {
    Decimal caseAge;
    Datetime startTime;
    Datetime endTime;
    Decimal caseAgeMorning;
    Decimal caseAgeAfternoon;
    Integer hr;

    Case updCase;

    List<Case> updateCases = new List<Case>();

    for (Case c : scope) {
      try {
        updCase = new Case(Id = c.Id);
        system.debug('eachCase>>>' +c);
  
        //variable initialization
        updCase.Is_Age_Calculating__c = true;
        updCase.Age_Calculated__c = c.IsClosed ? true : false;
  
        startTime = c.CreatedDate;
        endTime = c.IsClosed ? c.ClosedDate : system.now();
  
        if (c.Business_Hours_Afternoon__c == null) {
          //calculating the case age (Hours) considering Business Hours of Case
          caseAge = BusinessHours.diff(c.BusinessHoursId, startTime, endTime) / 1000 / 60;
        }
        else {
          caseAgeMorning = BusinessHours.diff(c.BusinessHoursId, startTime, endTime) / 1000 / 60;
          caseAgeAfternoon = BusinessHours.diff(c.Business_Hours_Afternoon__c, startTime, endTime) / 1000 / 60;
          caseAge = caseAgeMorning + caseAgeAfternoon;
        }
  
        hr = Integer.valueOf(caseAge/60);
  
        updCase.Case_Age_Text__c = hr +' hour '+Integer.valueOf(caseAge - (hr*60))+' min';
        updCase.Case_Age__c = caseAge.divide(60, 0);
        //
        if (c.Status == CASE_STATUS_IN_PROGRESS && c.Case_Age__c >= 2) {
          updCase.X2_Hours_In_In_Progress__c = true;
        }
        if (c.Status == CASE_STATUS_IN_PROGRESS) {
          updCase.Recording_Time__c = caseAge; //T-375163
          system.debug('InProgress::Recording_Time__c>>>' +c.Recording_Time__c);
        }
        // for Status ='On Hold' on Case
        else if (c.Status == CASE_STATUS_ON_HOLD) {
          if (c.Histories != null && !c.Histories.isEmpty() &&
              c.Histories[0].OldValue == CASE_STATUS_IN_PROGRESS &&
              c.Histories[0].NewValue == CASE_STATUS_ON_HOLD) {
            startTime = c.Histories[0].createdDate;
          }
          if (c.Business_Hours_Afternoon__c == null) {
            //calculating the case age (Hours) considering Business Hours of Case
            caseAge = BusinessHours.diff(c.BusinessHoursId, startTime, endTime) / 1000 / 60;
          }
          else {
            caseAgeMorning = BusinessHours.diff(c.BusinessHoursId, startTime, endTime) / 1000 / 60;
            caseAgeAfternoon = BusinessHours.diff(c.Business_Hours_Afternoon__c, startTime, endTime) / 1000 / 60;
            caseAge = caseAgeMorning + caseAgeAfternoon;
          }
          updCase.Recording_Time__c = caseAge;
          system.debug('Onhold::Recording_Time__c>>>' +updCase.Recording_Time__c);
        }
        system.debug('=====Case Number: ' + c.CaseNumber);
        system.debug('=====Is Closed?: ' + c.IsClosed);
        system.debug('=====Case Age: ' + updCase.Case_Age__c);
        system.debug('=====Recording_Time__c: ' + updCase.Recording_Time__c);
  
        updateCases.add(updCase);
      }
      catch (Exception ex) {
        system.debug('[BatchCaseAgeUpdate:execute]'+ex.getMessage());
        ApexLogHandler.createLogAndSave('BatchCaseAgeUpdate','execute', ex.getStackTraceString(), ex);
      }
    }
    //updating cases
    try {
      update updateCases;
    }
    catch (DMLException ex) {
      system.debug('[BatchCaseAgeUpdate:execute]'+ex.getMessage());
      ApexLogHandler.createLogAndSave('BatchCaseAgeUpdate','execute', ex.getStackTraceString(), ex);
    }
  }

  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchCaseAgeUpdate', true);
  }

}