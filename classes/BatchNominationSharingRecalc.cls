/*=============================================================================
 * Experian
 * Name: BatchNominationSharingRecalc
 * Description: Batch class for recalculating the sharing on the Nomination__c object.
 * Created Date: 31 Oct 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

public class BatchNominationSharingRecalc implements Database.Batchable<sObject>, Database.Stateful {

  public Database.Querylocator start (Database.Batchablecontext bc) {
    return Database.getQueryLocator([
      SELECT Id, Nominee__c, Nominee_Viewable__c, Master_Nomination__c, Requestor__c, Project_Sponsor__c, Nominees_Manager__c 
      FROM Nomination__c
    ]);
  }
  
  public void execute (Database.BatchableContext bc, List<Nomination__c> scope) {
    // Delete existing shares...
    List<Nomination__Share> oldNomShares = [
      SELECT Id 
      FROM Nomination__Share 
      WHERE ParentId IN :(new Map<Id, Nomination__c>(scope)).keySet()
      AND (RowCause = :Schema.Nomination__Share.rowCause.Nominator__c OR
           RowCause = :Schema.Nomination__Share.rowCause.Team_Member_Manager__c OR
           RowCause = :Schema.Nomination__Share.rowCause.Project_Sponsor__c OR
           RowCause = :Schema.Nomination__Share.rowCause.Nominee__c)
    ];
    
    delete oldNomShares;
    
    NominationHelper.createNominationShares(scope);
    
    List<Nomination__c> nomineeViews = new List<Nomination__c>();
    for (Nomination__c n : scope) {
      if (n.Nominee_Viewable__c) {
        nomineeViews.add(n);
      }
    }
    
    NominationHelper.createNomineeShares(nomineeViews);
  }
  
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchNominationSharingRecalc', true);
  }
  
}