/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ApiSearchRepository {
    global ApiSearchRepository() {

    }
    webService static String Search(String searchQuery, String libraryIds, String tags, String assetTypes, Integer startPage, Integer numResults) {
        return null;
    }
}
