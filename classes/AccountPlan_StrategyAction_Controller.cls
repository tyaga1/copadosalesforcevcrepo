/**=====================================================================
 * Name: AccountPlan_StrategyAction_Controller
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Mar. 11th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * Mar. 24th, 2016       James Wills           Case #01848189: Created
 * Apr. 21st, 2016       James Wills           Changed way Events and Tasks were selected for display to facilitate running of test class.
 * May  6th, 2016        James Wills           Case #01974250 AP - Completing activities from the list view
 *                                             Resolved issue with closing tasks and added sorting of all lists on page by Due Date.
 * May 27th, 2016        James Wills           Case #01949954 AP - List views
 * Oct. 26th, 2016       James Wills           Case #01983126 Added toLabel() translations to SOQL
 * Jan. 25th, 2016       James Wills           Case #01999757
 * May 10th, 2017        James Wills           Case #02400586: Added Investment field to Task lists.
  =====================================================================*/
global with sharing virtual class AccountPlan_StrategyAction_Controller{
  
  public ID accountPlanId {get;set;}

  public Account_Plan__c accountPlan {get;set;}
   
  public ID selectedID {get;set;}
  public ID selectedIdToDelete {get;set;}
  
  //Objectives List Navigation Parameters
  public Integer objectivePageOffset=0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSizeObjectives = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Objectives_List__c;
  
  public Integer objectivesLimit=relatedListSizeObjectives;//maximum number to show before list expanded
  public Integer additionalObjectivesToView {
    get{
      if(endAPObjectivesNumber+relatedListSizeObjectives > totalAPObjectives){
        return totalAPObjectives-endAPObjectivesNumber;
      } else {
        return relatedListSizeObjectives;
      }      
    }
    set;    
  }
  public Integer startAPObjectivesNumber {get;set;}
  public Integer endAPObjectivesNumber {get;set;}
  public Integer totalAPObjectives {get;set;}
  
  //Open Activities Navigation Parameters
  public Integer activitiesPageOffset = 0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSizeOpenActivities = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Open_Activities_List__c;
  
  public Integer activitiesLimit=relatedListSizeOpenActivities;//maximum number to show before list expanded
  public Integer additionalActivitiesToView{
    get{
      if(endAPActivitiesNumber+relatedListSizeOpenActivities > totalAPActivities){
        return totalAPActivities-endAPActivitiesNumber;
      }else{
        return relatedListSizeOpenActivities;
      }
    }
    set;  
  }
  public Integer startAPActivitiesNumber {get;set;}
  public Integer endAPActivitiesNumber {get;set;}
  public Integer totalAPActivities {get;set;}
  
  public Boolean showOpenActivitiesForFullList {get;set;}  
  public Boolean displayFullActivityList = false;
  public Boolean displayFullHistoryList = false;
  
  //Closed Activites Navigation Parameters
  public Integer closedActivitiesPageOffset = 0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSizeClosedActivities = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Closed_Activities_List__c;
  
  public Integer closedActivitiesLimit = relatedListSizeClosedActivities;//maximum number to show before list expanded
  public Integer additionalClosedActivitiesToView{
    get{
      if(endAPClosedActivitiesNumber+relatedListSizeClosedActivities > totalAPClosedActivities){
        return totalAPClosedActivities-endAPClosedActivitiesNumber;
      } else {
        return relatedListSizeClosedActivities;
      }    
    }
    set;    
  }
  public Integer startAPClosedActivitiesNumber {get;set;}
  public Integer endAPClosedActivitiesNumber {get;set;}
  public Integer totalAPClosedActivities {get;set;}

  
  public Account account {get;set;}    

  private string TASK_URL_FORMAT    = '/00T/e?what_id={acc_plan_id}&retURL=%2Fapex%2FAccountPlanTabPanel%3Fid%3D{acc_plan_id}&RecordType={record_type_id}&ent=Task&saveURL=%2F{acc_plan_id}';
  private string EVENT_URL_FORMAT   = '/00U/e?what_id={acc_plan_id}&retURL=%2Fapex%2FAccountPlanTabPanel%3Fid%3D{acc_plan_id}';
  private static final string RECORD_TYPE_TASK_ACCOUNT_PLAN = 'Account Plan';
  
  
 global class strategyActionActivitiesWrapper implements Comparable{
    public String ID {get;set;}
    public String ActivityParent {get;set;}
    public String Subject {get;set;}
    public String Name {get;set;}
    public Boolean isTask {get;set;}
    public Date DueDate {get;set;}
    public String Status {get;set;}
    public Decimal Investment {get;set;}
    public String Priority {get;set;}
    public String AssignedTo {get;set;}
    public Datetime LastMod {get;set;}
    
    
    global Integer compareTo(Object compareTo) {
       strategyActionActivitiesWrapper compareToATeam = (strategyActionActivitiesWrapper)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (DueDate > compareToATeam.DueDate) {
         // Set return value to a positive value.
         returnValue = 1;
       } else if (DueDate < compareToATeam.DueDate) {
         // Set return value to a negative value.
         returnValue = -1;
       }
       return returnValue;
     }
  }
  
  global class accountPlanObjectivesWrapper implements Comparable{
    public String ID {get;set;}
    public String name {get;set;}
    public Date dueDate {get;set;}
    public Decimal percentComplete {get;set;}
    public String status {get;set;}
    public String description {get;set;}
    public String createdBy {get;set;}
    
    global Integer compareTo(Object compareTo) {
       accountPlanObjectivesWrapper compareToATeam = (accountPlanObjectivesWrapper)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (dueDate > compareToATeam.dueDate) {
         // Set return value to a positive value.
         returnValue = 1;
       } else if (dueDate < compareToATeam.dueDate) {
         // Set return value to a negative value.
         returnValue = -1;
       }
       return returnValue;
    }  
  }
  
  
  public List <strategyActionActivitiesWrapper> strategyAndActionActivitiesList {
    get{
      return buildStrategyAndActionActivitiesList();
    }
    set;
  }
    
  public List <strategyActionActivitiesWrapper> strategyAndActionActivityHistoryList {
    get{
      return buildStrategyAndActionActivitiesHistoryList();
    }
    set;
  }
  
  public List <strategyActionActivitiesWrapper> strategyAndActionActivitiesFullList {
    get{
      if(showOpenActivitiesForFullList==true){
        return buildStrategyAndActionActivitiesList();
      } else {
        return buildStrategyAndActionActivitiesHistoryList();
      }
    }
    set;
  }
  
  
  public String accountPlanOpenActivitiesPageBlockTitle{
  get{
   if(showOpenActivitiesForFullList==true){
     return 'Open Activities for Account Plan and Account Plan Objectives';
   } else {
     return 'Activity History for Account Plan and Account Plan Objectives';
   }
   }
   set;   
  }
  
  //This property is used by the Visual Force page GenerateAccountPlanPDF  
  public List<accountPlanObjectivesWrapper> accountPlanObjectivesList {

    get{
      return buildAccountPlanObjectivesList();
    }
  
    set;
  }
  
  public AccountPlan_StrategyAction_Controller( ApexPages.Standardcontroller std){
    
    accountPlan = (Account_Plan__c) std.getRecord();
    accountPlanId = accountPlan.id;
    
  }

   public AccountPlan_StrategyAction_Controller() {

    }


  public void strategyAndActionTabUpdateCookies(){
  
    AccountPlanUtilities.setAccountPlanCookies(accountPlanId, Label.ACCOUNTPLANNING_StrategyAndActionTab);
  
  }
  
  
  public PageReference editMember(){
    strategyAndActionTabUpdateCookies();
    PageReference accountPlanMemberToEdit = new PageReference('/' + selectedId);
    
    return accountPlanMemberToEdit;
  }
  
  public List<accountPlanObjectivesWrapper> buildAccountPlanObjectivesList(){
  
   List<accountPlanObjectivesWrapper> accountPlanObjectivesListLocal = new List<accountPlanObjectivesWrapper>();
      List<Account_Plan_Objective__c> accountPlanObjectives = [SELECT id, Name, Due_Date__c, Percentage_Complete__c, Objective_Status__c, Objective_Detail__c, 
                                                               CreatedBy.Name, CreatedDate FROM Account_Plan_Objective__c 
                                                               WHERE Account_Plan__c = :accountPlan.id
                                                               LIMIT :objectivesLimit
                                                               ];
      
      for(Account_Plan_Objective__c apo : accountPlanObjectives){
        accountPlanObjectivesWrapper newAccPlanObjective = new accountPlanObjectivesWrapper();
        
        newAccPlanObjective.ID              = apo.ID;
        newAccPlanObjective.name            = apo.Name;
        newAccPlanObjective.DueDate         = apo.Due_Date__c;
        newAccPlanObjective.percentComplete = apo.Percentage_Complete__c;
        newAccPlanObjective.status          = apo.Objective_Status__c;
        newAccPlanObjective.description     = apo.Objective_Detail__c;
        newAccPlanObjective.createdBy       = apo.CreatedBy.Name + ', ' + apo.CreatedDate;
        accountPlanObjectivesListLocal.add(newAccPlanObjective);       
      }
      
      accountPlanObjectivesListLocal.sort();
    
      //Now set values used for Account Plan Contact list navigation
      if(accountPlanObjectivesListLocal.size()>0){
        startAPObjectivesNumber = objectivePageOffSet+1;
        endAPObjectivesNumber = objectivePageOffset + accountPlanObjectivesListLocal.size();  
        AggregateResult[] APObjectives = [SELECT COUNT(Name)TotalAPObjectives FROM Account_Plan_Objective__c WHERE Account_Plan__c = :accountPlanID];
        totalAPObjectives  = (Integer)APObjectives[0].get('TotalAPObjectives');    
      } else {
        startAPObjectivesNumber = 0;
        endAPObjectivesNumber = 0;  
        totalAPObjectives = 0;
      }

      return accountPlanObjectivesListLocal;
  }
  
  public PageReference newAccountPlanObjective(){
    
    strategyAndActionTabUpdateCookies();    
    PageReference newAccountPlanObjectivePageRef = new PageReference('/' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Objective_Rec__c + '/e?' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Objective__c + '=' + accountPlan.Name + '&' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Objective__c + '_lkid=' + accountPlanId + '&retURL=%2Fapex%2FAccountPlanTabPanel%3Fid%3D' + accountPlanId);     
    return newAccountPlanObjectivePageRef;
  } 
  
  public void seeMoreObjectives(){   
    objectivesLimit+=additionalObjectivesToView;
  }
  
  
  public PageReference viewAccountPlanObjectivesListFull(){
    strategyAndActionTabUpdateCookies();
    PageReference newAccountPlanObjectivesPageRef = new PageReference('/' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Objective_Rec__c + 
                                                                  '?rlid=' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Objective__c.mid(2,15) + '&id=' + accountPlanId);
    return newAccountPlanObjectivesPageRef;
  }
  

  public PageReference viewAccountPlanActivitiesListFull(){
    strategyAndActionTabUpdateCookies();
    showOpenActivitiesForFullList=true;
    displayFullActivityList=true;
    PageReference newAccountPlanActivitiesPageRef = Page.AccountPlanStrategyActionActivities;

    return newAccountPlanActivitiesPageRef;
  }
  
  public PageReference viewAccountPlanHistoryListFull(){
    strategyAndActionTabUpdateCookies();
    showOpenActivitiesForFullList=false;
    displayFullHistoryList=true;
    PageReference newAccountPlanActivitiesPageRef = Page.AccountPlanStrategyActionActivities;

    return newAccountPlanActivitiesPageRef;
  }
  
  
  
  public List<strategyActionActivitiesWrapper> buildStrategyAndActionActivitiesList(){
    
    List<strategyActionActivitiesWrapper> strategyAndActionActivitiesList = new List<strategyActionActivitiesWrapper>();
    
    DateTime eventDate1 = DateTime.now();
    
    List<Account_Plan__c> activitiesForAccountPlan = [SELECT id, Name, (SELECT id, Subject, WhoId, Who.Name, 
                                                   ActivityDate, 
                                                   Status, toLabel(Priority), Investment__c, OwnerId, Owner.Name 
                                                   FROM Tasks WHERE Status!='Completed') ,
                                                   (SELECT id, Subject, WhoId, Who.Name, 
                                                   ActivityDate, 
                                                   OwnerId, Owner.Name 
                                                   FROM Events WHERE EndDateTime >= :eventDate1)
                                                   FROM Account_Plan__c
                                                   WHERE id = :accountPlanId];
    if(activitiesForAccountPlan.size()>0){
      for(Account_Plan__c accPlan : activitiesForAccountPlan ){
      
        for(Task activ : accPlan.Tasks){
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
      
          stratActive.ID             =    activ.ID;
          stratActive.ActivityParent =    accPlan.Name;
          stratActive.Subject        =    activ.Subject;
          stratActive.Name           =    activ.Who.Name;
          stratActive.isTask         =    true;
          stratActive.DueDate        =    activ.ActivityDate;
          stratActive.Status         =    activ.Status;
          if(activ.Investment__c!=null){
            stratActive.Investment   =    activ.Investment__c;
          } else {
            stratActive.Investment   = 0.0;
          }
          stratActive.Priority       =    activ.Priority;
          stratActive.AssignedTo     =    activ.Owner.Name;
            
          strategyAndActionActivitiesList.add(stratActive);
        }
        
        for(Event activ : accPlan.Events){
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
      
          stratActive.ID             =    activ.ID;
          stratActive.ActivityParent =    accPlan.Name;
          stratActive.Subject        =    activ.Subject;
          stratActive.Name           =    activ.Who.Name;
          stratActive.isTask         =    false;
          stratActive.DueDate        =    activ.ActivityDate;
          stratActive.AssignedTo     =    activ.Owner.Name;
            
          strategyAndActionActivitiesList.add(stratActive);
        }
      }
    }
    
    List<Account_Plan_Objective__c> activitiesForAccountPlanObjective = [SELECT id, Name, (SELECT id, Subject, WhoId, Who.Name, 
                                                                        ActivityDate, 
                                                                       toLabel(Status), Priority, Investment__c, OwnerId, Owner.Name 
                                                                       FROM Tasks WHERE Status!='Completed'),
                                                                       (SELECT id, Subject, WhoId, Who.Name, 
                                                                       ActivityDate, 
                                                                       OwnerId, Owner.Name 
                                                                       FROM Events WHERE EndDateTime >= :eventDate1)
                                                                       FROM Account_Plan_Objective__c
                                                                       WHERE Account_Plan__c = :accountPlanId];
    if(activitiesForAccountPlanObjective.size()>0){
      for(Account_Plan_Objective__c accPlan : activitiesForAccountPlanObjective ){

        for(Task activ : accPlan.Tasks){
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
        
          stratActive.ID             =    activ.ID;
          stratActive.ActivityParent =    accPlan.Name;
          stratActive.Subject        =    activ.Subject;
          stratActive.Name           =    activ.Who.Name;
          stratActive.isTask         =    true;
          stratActive.DueDate        =    activ.ActivityDate;
          stratActive.Status         =    activ.Status;
          if(activ.Investment__c!=null){
            stratActive.Investment   =    activ.Investment__c;
          } else {
            stratActive.Investment   = 0.0;
          }
          stratActive.Priority       =    activ.Priority;
          stratActive.AssignedTo     =    activ.Owner.Name;
            
          strategyAndActionActivitiesList.add(stratActive);
        }
        
        for(Event activ : accPlan.Events){
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
      
          stratActive.ID             =    activ.ID;
          stratActive.ActivityParent =    accPlan.Name;
          stratActive.Subject        =    activ.Subject;
          stratActive.Name           =    activ.Who.Name;
          stratActive.isTask         =    false;
          stratActive.DueDate        =    activ.ActivityDate;
          stratActive.AssignedTo     =    activ.Owner.Name;
            
          strategyAndActionActivitiesList.add(stratActive);
        }
      }
    }
    
    strategyAndActionActivitiesList.sort();
     
    List<strategyActionActivitiesWrapper> strategyAndActionActivitiesListLocal = new List<strategyActionActivitiesWrapper>(); 
        
    //Now set values used for Account Plan Contact list navigation
    if(strategyAndActionActivitiesList.size()>0){
      
      if(displayFullActivityList==true){
        strategyAndActionActivitiesListLocal = strategyAndActionActivitiesList;
      } else {
        Integer i=0;
        while(i<activitiesLimit && i< strategyAndActionActivitiesList.size()){
          strategyAndActionActivitiesListLocal.add(strategyAndActionActivitiesList[i]);    
          i++;
        }
      }
          
      startAPActivitiesNumber = activitiesPageOffset + 1;
      endAPActivitiesNumber =   activitiesPageOffset + strategyAndActionActivitiesListLocal.size();  
      totalAPActivities  = strategyAndActionActivitiesList.size();   
    } else {
      startAPActivitiesNumber = 0;
      endAPActivitiesNumber = 0;  
      totalAPActivities = 0;
    }   
        
    return strategyAndActionActivitiesListLocal;
  }
  
  public void seeMoreActivities(){
    activitiesLimit+=additionalActivitiesToView;  
  }
  
  
  public List <strategyActionActivitiesWrapper> buildStrategyAndActionActivitiesHistoryList(){
    
    
    List<strategyActionActivitiesWrapper> strategyAndActionActivityHistoryList = new List<strategyActionActivitiesWrapper>();

    
    DateTime eventDate1 = DateTime.now();
 
    List<Account_Plan__c> activitiesForAccountPlan = [SELECT id, Name, (SELECT id, Subject, WhoId, Who.Name,
                                                   ActivityDate, 
                                                   toLabel(Status), Priority, Investment__c, OwnerId, Owner.Name, LastModifiedDate
                                                   FROM Tasks WHERE Status='Completed'),
                                                   (SELECT id, Subject, WhoId, Who.Name,
                                                   ActivityDate, 
                                                   OwnerId, Owner.Name, LastModifiedDate
                                                   FROM Events WHERE EndDateTime < :eventDate1)
                                                   FROM Account_Plan__c
                                                   WHERE id = :accountPlanId];
    
    if(activitiesForAccountPlan.size()>0){
      for(Account_Plan__c accPlan : activitiesForAccountPlan ){
      
        for(Task activ : accPlan.Tasks){
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
      
          stratActive.ID            =    activ.ID;
          stratActive.ActivityParent =   accPlan.Name;
          stratActive.Subject       =    activ.Subject;
          stratActive.Name          =    activ.Who.Name;
          stratActive.isTask         =    true;
          stratActive.DueDate       =    activ.ActivityDate;
          stratActive.Status        =    activ.Status;
          if(activ.Investment__c!=null){
            stratActive.Investment   =    activ.Investment__c;
          } else {
            stratActive.Investment   = 0.0;
          }
          stratActive.Priority      =    activ.Priority;
          stratActive.AssignedTo    =    activ.Owner.Name;
          stratActive.LastMod        =    activ.LastModifiedDate;  
            
          strategyAndActionActivityHistoryList.add(stratActive);
        }
        
        for(Event activ : accPlan.Events){
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
      
          stratActive.ID             =    activ.ID;
          stratActive.ActivityParent =    accPlan.Name;
          stratActive.Subject        =    activ.Subject;
          stratActive.Name           =    activ.Who.Name;
          stratActive.isTask         =    false;
          stratActive.DueDate        =    activ.ActivityDate;
          stratActive.AssignedTo     =    activ.Owner.Name;
          stratActive.LastMod        =    activ.LastModifiedDate;  
            
          strategyAndActionActivityHistoryList.add(stratActive);
        }
      }
    }
    
    List<Account_Plan_Objective__c> activitiesForAccountPlanObjective = [SELECT id, Name, (SELECT id, Subject, WhoId, Who.Name,
                                                                       ActivityDate, 
                                                                       toLabel(Status), Priority, Investment__c, OwnerId, Owner.Name, LastModifiedDate
                                                                       FROM Tasks WHERE Status = 'Completed'),
                                                                       (SELECT id, Subject, WhoId, Who.Name,
                                                                       ActivityDate, 
                                                                       OwnerId, Owner.Name, LastModifiedDate
                                                                       FROM Events WHERE EndDateTime < :eventDate1)
                                                                       FROM Account_Plan_Objective__c
                                                                       WHERE Account_Plan__c = :accountPlanId];
    if(activitiesForAccountPlanObjective.size()>0){
      for(Account_Plan_Objective__c accPlan : activitiesForAccountPlanObjective ){

        for(Task activ : accPlan.Tasks){   
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
        
          stratActive.ID             =    activ.ID;
          stratActive.ActivityParent =    accPlan.Name;
          stratActive.Subject        =    activ.Subject;
          stratActive.Name           =    activ.Who.Name;
          stratActive.isTask         =    true;
          stratActive.DueDate        =    activ.ActivityDate;
          stratActive.Status         =    activ.Status;
          if(activ.Investment__c!=null){
            stratActive.Investment   =    activ.Investment__c;
          } else {
            stratActive.Investment   = 0.0;
          }
          stratActive.Priority       =    activ.Priority;
          stratActive.AssignedTo     =    activ.Owner.Name;
          stratActive.LastMod        =    activ.LastModifiedDate;    
          
          strategyAndActionActivityHistoryList.add(stratActive);
        }
        
        for(Event activ : accPlan.Events){
          strategyActionActivitiesWrapper stratActive = new strategyActionActivitiesWrapper();
      
          stratActive.ID             =    activ.ID;
          stratActive.ActivityParent =    accPlan.Name;
          stratActive.Subject        =    activ.Subject;
          stratActive.Name           =    activ.Who.Name;
          stratActive.isTask         =    false;
          stratActive.DueDate        =    activ.ActivityDate;
          stratActive.AssignedTo     =    activ.Owner.Name;
          stratActive.LastMod        =    activ.LastModifiedDate;  
            
          strategyAndActionActivityHistoryList.add(stratActive);
        }
      }
    }
    
    strategyAndActionActivityHistoryList.sort();
       
    List<strategyActionActivitiesWrapper> strategyAndActionActivityHistoryListLocal = new List<strategyActionActivitiesWrapper>(); 
        
    //Now set values used for Account Plan Contact list navigation
    if(strategyAndActionActivityHistoryList.size()>0){
      
      if(displayFullHistoryList==true){
        strategyAndActionActivityHistoryListLocal=strategyAndActionActivityHistoryList;
      } else {
        Integer i=0;
        while(i<closedActivitiesLimit && i < strategyAndActionActivityHistoryList.size()){
          strategyAndActionActivityHistoryListLocal.add(strategyAndActionActivityHistoryList[i]);    
          i++;
        }
      }
          
      startAPClosedActivitiesNumber = closedActivitiesPageOffSet+1;
      endAPClosedActivitiesNumber = closedActivitiesPageOffset + strategyAndActionActivityHistoryListLocal.size();  
      totalAPClosedActivities  = strategyAndActionActivityHistoryList.size();   
    } else {
      startAPClosedActivitiesNumber = 0;
      endAPClosedActivitiesNumber = 0;  
      totalAPClosedActivities = 0;
    }           
    
    return strategyAndActionActivityHistoryListLocal;
    
  }

  public void seeMoreClosedActivities(){
    closedActivitiesLimit+=additionalClosedActivitiesToView;  
  }
  
  
  public PageReference createTask(){
    // this method is invoked on click of the Create Task button.    
    
    strategyAndActionTabUpdateCookies();
    string taskAccPlanRecordTypeId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_TASK, RECORD_TYPE_TASK_Account_PLAN);
    string pageReferenceURL = TASK_URL_FORMAT.replace('{acc_plan_id}',    this.accountPlanId)
                                             .replace('{record_type_id}', taskAccPlanRecordTypeId);                          
    return new pageReference(pageReferenceURL);

  }
  
  public PageReference createEvent(){
    // this method is invoked on click of the Create Event button.  
    
    strategyAndActionTabUpdateCookies();
    
    string taskAccPlanRecordTypeId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_TASK, RECORD_TYPE_TASK_Account_PLAN);
    
    string pageReferenceURL = EVENT_URL_FORMAT.replace('{acc_plan_id}',    this.accountPlanId)
                                             .replace('{record_type_id}', taskAccPlanRecordTypeId);
                                     
    return new pageReference(pageReferenceURL);
    
  }
  
  
  public PageReference doCloseActivity(){
    try{
      List<Task> taskToClose = [SELECT id, Subject FROM Task WHERE Id = :selectedId LIMIT 1];
    
      if(taskToClose.size()>0){
        taskToClose[0].Status = 'Completed';
        update taskToClose;      
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.INFO, 'The Task ' + taskToClose[0].Subject + ' has been Closed.'));      
      }
    }  catch ( exception ex) {
      apexLogHandler.createLogAndSave('AccountPlan_StrategyAction_Controller','doCloseActivity', ex.getStackTraceString(), ex);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
    }
    return null;

  }



  public PageReference doDeleteObjective(){
    
    strategyAndActionTabUpdateCookies();
    Schema.DescribeSObjectResult describeResult = Account_Plan_Objective__c.SObjectType.getDescribe();    
    String accountPlanObjectType = describeResult.getName();
    
    AccountPlanUtilities.deleteAccountPlanRelatedListEntry(selectedIdToDelete, accountPlanObjectType, Label.ACCOUNTPLANNING_StrategyAndActionTab);
    
    return null;

  }
  


  public PageReference doDeleteActivity(){
  
    try{
      if ( selectedIdToDelete != null ) { 
        String subjectOfDeletedObject;       
        //Cannot Select on OpenActivity, so need to select from Event
        List<Event> eventToDel = [SELECT id, Subject FROM Event WHERE Id = :selectedIdToDelete];

        if(eventToDel.size()>0){
          subjectOfDeletedObject = eventToDel[0].Subject;   
          delete eventToDel;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.INFO, 'The Event ' + subjectOfDeletedObject + ' has been deleted.'));
        }
      }
    } catch ( exception ex) {
      apexLogHandler.createLogAndSave('AccountPlan_StrategyAction_Controller','doDeleteActivity', ex.getStackTraceString(), ex);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
    }
    return null;

  }



  public PageReference doDeleteActHist(){
    
    try{
      if ( selectedIdToDelete != null ) {
        String subjectOfDeletedObject;
        
        //Cannot Select on ActivityHistory so need to select from Task and Event        
        List<Task> taskToDel = [SELECT id, Subject FROM Task WHERE Id = :selectedIdToDelete LIMIT 1];
         
        if(taskToDel.size()>0){ 
          subjectOfDeletedObject = taskToDel[0].Subject;             
          delete taskToDel;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.INFO, 'The Task ' + taskToDel[0].Subject + ' has been deleted.'));
        } else {
        
          List<Event> eventToDel = [SELECT id, Subject FROM Event WHERE Id = :selectedIdToDelete LIMIT 1];

          if(eventToDel.size()>0){
            subjectOfDeletedObject = eventToDel[0].Subject; 
            delete eventToDel;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.INFO, 'The Event ' + subjectOfDeletedObject + ' has been deleted.'));
          }
        }

      }
    }
    catch ( exception ex) {
      apexLogHandler.createLogAndSave('AccountPlan_StrategyAction_Controller','doDeleteActHist', ex.getStackTraceString(), ex);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
    }
    
    return null;

  }

  public PageReference backToAccountPlan(){  
    PageReference accountPlan = new PageReference('/' + accountPlanId ); 
    accountPlan.setRedirect(true); 
    return accountPlan;      
  }

}