/**********************************************************************************************
 * Experian 
 * Name         : AutoCaseCopadoUserStory_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class for class "AutoCaseCopadoUserStory"
 * Created Date : August 7, 2017
 *
 * Date Modified                Modified By                 Description of the update
***********************************************************************************************/

@isTest
private class AutoCaseCopadoUserStory_Test {
    
  private static testMethod void testNewUserStory() {
    Case testRecord = [Select id,Subject,Implementation_Status__c,Description,Owner.Id, CaseNumber FROM Case WHERE Subject = 'Test Copado Subject'];
    testRecord.Subject = 'Test Starting Subject';
    update testRecord;
    Test.startTest();
    
    String output= AutoCaseCopadoUserStory.autoCaseCUS(testRecord.id);
    
    system.assertEquals(1,[SELECT COUNT() FROM  copado__User_Story__c WHERE Case__c <> null]);
  }
  
  @testSetup
  private static void setupData() {
    
    //Id crmRecordTypeId = recordTypeNameToIdMap.get(Constants.CASE_REC_TYPE_SFDC_SUPPORT);
    
    //Create test records
       
    User tstUser1 = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
           
    insert new List<User>{tstUser1};
    
    Case newSupportCase = new Case ( 
          
          //RecordType = crmRecordTypeId,
          User_Requestor__c = 'Test Requester',
          Requestor_Email__c = 'Test.requester@experian.com',
          Requestor_Work_Phone__c = '999 999 9999',
          Requester_BU__c = 'NA Corporate GTS',
          Reason = 'Other area',
          Secondary_Case_Reason__c = 'Other',          
          OwnerId = tstUser1.id,
          Subject = 'Test Copado Subject',
          Description = 'Test Descrption',          
          Implementation_Status__c = 'Dev Complete');
                   
          insert newSupportCase;
  }
  
}