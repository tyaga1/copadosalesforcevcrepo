@isTest
public class Account_Competitor_Info_Controller_Test
{
    public static testMethod void test_Controller_Returns_Competitors_Only_For_The_Related_Account()
    {
        PageReference pf = Page.Account_Competitor_RelatedList;
        Test.setCurrentPage(pf);
        
        Account acc1 = Test_Utils.insertAccount(); //Account 1
        acc1.Is_Competitor__c = true;
        UPDATE acc1;
        
        Account acc2 = Test_Utils.insertAccount(); //Account 2
        acc2.Is_Competitor__c = true;
        UPDATE acc2;
        
        Opportunity opp1 = Test_Utils.insertOpportunity(acc1.Id);
                
        List<Competitor__c> listCompetitors = new List<Competitor__c>();
        
        Competitor__c c1 = Test_Utils.insertCompetitor(opp1.Id); //Associated with Account 1
        Competitor__c c2 = Test_Utils.insertCompetitor(opp1.Id);
        
        c2.Account__c = acc2.id; //Update the association to Account 2
        UPDATE C2;        
        
        
        Account_Competitor_Info_Controller ctrl = new Account_Competitor_Info_Controller(new ApexPages.StandardController(acc1));
        List<Account_Competitor_Info> listACI = ctrl.getCompetitors();
        
        System.assertEquals(listACI.size(), 1); //Look for Competitors belonging to Account 1
    }

}