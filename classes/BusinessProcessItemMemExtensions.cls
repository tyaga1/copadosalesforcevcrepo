/**=====================================================================
* Experian
* Name: BusinessProcessitemMemExtensions
* Description: Controller to display Business Process Line Items in Edit Mode.
* Created Date: 2015-09-11
* Created By: Tyaga Pati
* 
* Date Modified         Modified By         Description of the update
* 2015-10-25          Esteban Castro        Added sorting, user without edit permissions support.
=====================================================================*/
public with sharing class BusinessProcessItemMemExtensions {
  private String sortDirection = '';
  private String sortExp = '';
  
  Public Membership__c MemRec{get;set;}
  Public Membership__c Membership {get;set;}
  Public ApexPages.StandardController StdControler ;
  
  Public List<Business_Process_Item__c> BPItems{
    get{
      return BPItems;
    }
    
    set;
  }
  
  //This carries the Id of the BPLine to be Deleted from the Visual Force Page
  Public Id selectedId{get;set;}
  
  //Start of the Constructor
  public BusinessProcessItemMemExtensions(ApexPages.StandardController controller) {
    
    StdControler = controller; 
    this.Membership = (Membership__c)StdControler.getRecord();
    MemRec = [SELECT Id, Name from Membership__c where Id=:Membership.Id];
    ViewData(); //load bpi data
  }//End Constructor
  
  public String sortExpression
  {
    get
    {
      return sortExp;
    }
    set
    {
      //if the column is clicked on then switch between Ascending and Descending modes
      if (value == sortExp)
        sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
      else
        sortDirection = 'ASC';
      sortExp = value;
    }
  }
  
  public String getSortDirection()
  {
    //if not column is selected 
    if (sortExpression == null || sortExpression == '')
      return 'ASC';
    else
      return sortDirection;
  }
  
  public void setSortDirection(String value)
  { 
    sortDirection = value;
  }
  
  //Load the BPI data
  public PageReference ViewData() {
    String MemId = this.Membership.Id;
    String SortExp = sortExpression != '' && sortDirection != '' ? ' ORDER BY ' + sortExpression + ' ' + sortDirection : '';
    
    //query the database based on the sort expression
    BPItems = Database.query('SELECT Id, Name,Business_Process_Template_Item__c,' +
                 'BPI_Name__c,Description__c,Comments__c,CSDA_Sub_Code__c, ' + 
                 'Owner.Alias,Account__r.Id, Status__c, ' + 
                 'UserRecordAccess.HasEditAccess ' +
                 'FROM Business_Process_Item__c WHERE Membership__c = :MemId ' + sortExp
                );
    return null;
  }
  
  //Start of the Save function
  Public PageReference saveEdit(){
    //fill list with only the fields the user can actually update
    List<Business_Process_Item__c> toSaveList = new List<Business_Process_Item__c>();
    for(Business_Process_Item__c bpi : BPItems){
      if(bpi.UserRecordAccess.HasEditAccess){//<< here HasEditAccess
        toSaveList.add(bpi);
      }
    }
    try{
      //update BPItems;
      update toSaveList;
    }
    catch(exception e){
      ApexPages.addMessages(e);
    }
    
    return null;
    
  } //End of Save Function
  
  //Start Of Delete Function 
  Public PageReference doDelete(){
    try{ 
      if(selectedId != Null){
        integer indextoDel = -1;
        for(Business_Process_Item__c BPI :BPItems){
          indextoDel++;
          if(BPI.Id ==selectedId){
            break; 
          } 
        }
        if( indextoDel != -1) {
          BPItems.remove(indextoDel);
        } 
        Business_Process_Item__c BPLnToDel = [SELECT Id,Name FROM Business_Process_Item__c WHERE Id = :selectedId]; 
        delete BPLnToDel;
      }
      
    }
    catch(Exception ex){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, ex.getMessage()));
      
    }
    
    return null;  
  }//End of Delete Function
}