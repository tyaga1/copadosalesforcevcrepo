/**********************************************************************************
 * Experian, Inc
 * Name: BatchContactStatusUpdateOnlyGoLive
 * Description: This job is for updating the new Status field on all the existing contacts.
 * Created Date: Aug 23rd, 2016
 * Created By: Tyaga Pati (Experian)
 *
 * Summary:
 * - The purpose of this class is to update the new status field on the existing contacts with the value in the old status field.
 *
 * Date Modified                Modified By                  Description of the update

 **********************************************************************************/

global class BatchContactStatusUpdateOnlyGoLive implements Database.Batchable<sObject> {

  global Database.QueryLocator start(Database.BatchableContext BC){
    //Get All the Contacts where the new status field is Null. 
    //Retrieve the corresponding status_old__C field and update it.
    String query = 'SELECT Id, Status__c, Status_old__C FROM Contact where Status__C = Null';
    return Database.getQueryLocator(query);
  }

  global void execute(Database.BatchableContext BC, List<Contact> scope) {
    List<Contact> contactsToUpdate = new List<Contact>();
    //List<Contact> contacts= (List<Contact>) scope;
    for (Contact Con: (List<Contact>) scope) 
      {
         if(Con.Status_Old__C  == 'No longer with company' && Con.Status_Old__C != Null )
           {  
             Con.Status__C = 'No Longer With Company';
           } 
         if(Con.Status_Old__C == 'Inactive' && Con.Status_Old__C != Null) 
           {
             Con.Status__C = 'Inactive - Still With Company';
           }
         if(Con.Status_Old__C == 'Active' && Con.Status_Old__C != Null) 
           {
             Con.Status__C = 'Active';
           }
         if(Con.Status_Old__C == 'z.Dormant' && Con.Status_Old__C != Null) 
           {
             Con.Status__C = 'No Longer With Company';
           }
            
          contactsToUpdate.add(Con);           
        }    
    Database.SaveResult[] srList = Database.update(contactsToUpdate,false);
    for (Database.SaveResult sr : srList) { 
	  if (sr.isSuccess()) { 
      	 System.debug('Successfully inserted account. Account ID: ' + sr.getId());
	    }//end of If
      else {
      	for(Database.Error err : sr.getErrors()) { 
            System.debug(' *******The following error has occurred.*********');                     
            System.debug('The Error status code :' + err.getStatusCode() + ': ' + 'And the Error Message is : ' + err.getMessage()); 
            System.debug('Following Fields Errors Occcured: ' + err.getFields()); 
          }
      	}//End of else
     }//End of Error For Loop    
  }//End of Execute Section

  global void finish(Database.BatchableContext BC) {

    BatchHelper bh = new BatchHelper();
    bh.checkBatch(BC.getJobId(), 'BatchContactStatusUpdateOnlyGoLive', true);

  }

}