/**=====================================================================
  * Experian
  * Name: SerasaFailedContractCancellation_Test
  * Description: W-007795: Test class for SerasaFailedContractCancellationAlert
  * Created Date: March 31 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * July 31st 2017    Manoj Gopu                    Fixed Test Class Failure
  *=====================================================================*/
@isTest
private class SerasaFailedContractCancellation_Test {
  
  @isTest static void test_method_one() {
    Test.StartTest();
    SerasaFailedContractCancellationAlert.scheduleMyJob();
    Test.StopTest();
  }

  @isTest static void test_method_two() {

    /*Serasa_Contract_Cancellation_Schedule__c newSetting = new Serasa_Contract_Cancellation_Schedule__c();
    newSetting.Name = 'Selected Hours';
    newSetting.Scheduled_Hour__c = '14';

    insert newSetting;*/

    Test.StartTest();
    SerasaFailedContractCancellationAlert.scheduleMyJob();
    SerasaFailedContractCancellationAlert.rescheduleAlert();
    Test.StopTest();
  }
  
  @isTest static void test_method_three() {

    Serasa_Contract_Cancellation_Supervisors__c email = new Serasa_Contract_Cancellation_Supervisors__c();
    email.Name = 'Ryan';
    email.email__c = 'testing@testing12343219876554.com';

    insert email;

    Contract_Cancellation_Request__c ccr = new Contract_Cancellation_Request__c();
    ccr.name = 'test';
    ccr.Status__c = 'Failed';
    ccr.Integration_ID__c = '123456';
    insert ccr;

    Test.StartTest();
    SerasaFailedContractCancellationAlert sfcca = new SerasaFailedContractCancellationAlert();
    String sch = '0 0 12 ? * *';

    System.schedule('Serasa Fialed Contract Cancellation Alert2', sch, sfcca);
    Test.StopTest();
  }

  @isTest static void test_method_four() {

    Serasa_Contract_Cancellation_Supervisors__c email = new Serasa_Contract_Cancellation_Supervisors__c();
    email.Name = 'Ryan';
    email.email__c = 'testing@testing12343219876554.com';

    insert email;

    Test.StartTest();
    SerasaFailedContractCancellationAlert sfcca = new SerasaFailedContractCancellationAlert();
    String sch = '0 0 12 ? * *';

    sfcca.getCurrentSchedule();

    System.schedule('Serasa Fialed Contract Cancellation Alert1', sch, sfcca);
    sfcca.getCurrentSchedule();
    sfcca.getCurrentCustomSetting();

    sfcca.newScheduledHour = '15';
    sfcca.updateCustomSettingHelper();
    sfcca.getCurrentCustomSetting();

    sfcca.newScheduledHour = '16';
    sfcca.updateCustomSetting();

    sfcca.pageAbortScheduledJobs();
    sfcca.pageScheduleMyJob();
    sfcca.pageRescheduleAlert();

    Test.StopTest();
  }
}