public with sharing class OppPlanContactRelationshipController {
	
	private String oppPlanId {get; set;}
	public static final String NONE_VALUE = '--None--';
	private Id defaultPlanContact {get; set;}
	private Plan_Contact_Relationship__c currentPlanContactRelationship {get; set;}
	public List<OppPlanRelationshipWrapper> oppPlanRelationshipWrapperList {get; set;}
	public List<OppPlanRelationshipWrapper> existingRelationshipWrapperList {get; set;}
	public List<OppPlanRelationshipWrapper> existingRelationshipToDeleteList {get; set;}
	public Map<Id, Opportunity_Plan_Contact__c> originalContactIdToOppPlanContactMap {get; set;}
	public Map<Id, Plan_Contact_Relationship__c> originalExistingContactsMap {get; set;}
	public Integer relationshipWrapperSize;
	public String wrapperChanged {get; set;}
	public String existingWrapperChanged {get; set;}
	public String deletedWrapperChanged {get; set;}
	public Boolean displayExisting {get; set;}
	public Boolean displayExistingToDelete {get; set;}
	
	public OppPlanContactRelationshipController(ApexPages.StandardController stdController) {
		OppPlanRelationshipWrapperList = new List<OppPlanRelationshipWrapper>();
		existingRelationshipWrapperList = new List<OppPlanRelationshipWrapper>();
		existingRelationshipToDeleteList = new List<OppPlanRelationshipWrapper>();
		originalContactIdToOppPlanContactMap = new Map<Id, Opportunity_Plan_Contact__c>();  
		originalExistingContactsMap = new Map<Id, Plan_Contact_Relationship__c>();
		
		displayExistingToDelete = false;
		
		// Get opportunity plan Id from URL if the URL does not contain an ID parameter. 
		
		if (ApexPages.currentPage().getParameters().containsKey('Id')) {
			currentPlanContactRelationship = [Select Id, Contact_1_Opp_Plan_ID__c From Plan_Contact_Relationship__c Where Id =:ApexPages.currentPage().getParameters().get('Id')];
			
			oppPlanId = currentPlanContactRelationship.Contact_1_Opp_Plan_ID__c;
		} else {
			oppPlanId = ApexPages.currentPage().getParameters().get('oppPlanId');
			System.debug('opp plan from URL: ' + oppPlanId);
			defaultPlanContact = ApexPages.currentPage().getParameters().get('defaultPlanContact');
		}
		
		
		
		System.debug('default plan contact: ' + defaultPlanContact);
		
		// Get opportunity plan contacts of the opportunity plan.
		List<Opportunity_Plan_Contact__c> originalOppPlanContactList = [select Id, Contact__c, Contact__r.Name From Opportunity_Plan_Contact__c Where Opportunity_Plan__c =: oppPlanId];
		System.debug('original opp plan contact list: ' + originalOppPlanContactList);
		for (Opportunity_Plan_Contact__c oppPlanContact : originalOppPlanContactList) {
			originalContactIdToOppPlanContactMap.put(oppPlanContact.Contact__c, oppPlanContact);
		}
		
		// Get any existing relationships 
		existingRelationshipWrapperList = getExistingWrapperLists();
		
		if (!existingRelationshipWrapperList.isEmpty()) {
			displayExisting = true;
		} else {
			displayExisting = false;
		}
		
		System.debug('newly assigned: ' + existingRelationshipWrapperList);
		// Make a new wrapper
		relationshipWrapperSize = 0;
		addRelationshipToList();
	}
	
	public List<OppPlanRelationshipWrapper> getExistingWrapperLists() {
		
		// Get the list of existing wrappers to display on the page. 
		List<OppPlanRelationshipWrapper> tempExistingRelationshipList = new List<OppPlanRelationshipWrapper>();
		System.debug('oppPlanId: ' + oppPlanId);
		List<Plan_Contact_Relationship__c> existingRelationships = [Select Id, Contact_1__c, Contact_1__r.Contact__c, Contact_1__r.Name, Contact_1__r.Contact__r.Name, Relationship__c, Contact_2__c, Contact_2__r.Contact__r.Name, Contact_2__r.Contact__c, Contact_2__r.Name From Plan_Contact_Relationship__c Where Contact_1_Opp_Plan_ID__c =: oppPlanId];
		Integer relationshipNumber = 1;
		
		for (Plan_Contact_Relationship__c relationship : existingRelationships) {
			Opportunity_Plan_Contact__c contactOne;
			Opportunity_Plan_Contact__c contactTwo;
			System.debug('contact 1 checked: ' + relationship.Contact_1__c);
			System.debug('contact 2 checked: ' + relationship.Contact_2__c);
			System.debug('original map keyset: ' + originalContactIdToOppPlanContactMap.keyset());
			
			if (originalContactIdToOppPlanContactMap.containsKey(relationship.Contact_1__r.Contact__c) && originalContactIdToOppPlanContactMap.containsKey(relationship.Contact_2__r.Contact__c)) {
				contactOne = originalContactIdToOppPlanContactMap.get(relationship.Contact_1__r.Contact__c);
				contactTwo = originalContactIdToOppPlanContactMap.get(relationship.Contact_2__r.Contact__c);
				
				OppPlanRelationshipWrapper existingWrapper = new ExistingOppPlanRelationshipWrapper(contactOne, contactTwo, originalContactIdToOppPlanContactMap.values(), relationship.relationship__c, relationship.Id, relationshipNumber);
				tempExistingRelationshipList.add(existingWrapper);
				relationshipNumber++;
			}
			
			originalExistingContactsMap.put(relationship.Id, relationship);
		}
		
		System.debug('existing relationships : ' + tempExistingRelationshipList);
		return tempExistingRelationshipList;
	}
	
	
	public pageReference addRelationship() {
		
		// Increment the count of wrappers
		// make a new wrapper and add it to the list. 		
		addRelationshipToList();
		return null;
	} 
	
	public void addRelationshipToList() {
		relationshipWrapperSize++;
		System.debug('contact 1 list: ' + originalContactIdToOppPlanContactMap.values());
		OppPlanRelationShipWrapper relationshipWrapper = new OppPlanRelationshipWrapper(originalContactIdToOppPlanContactMap.values(), originalContactIdToOppPlanContactMap.values(), relationshipWrapperSize, defaultPlanContact);
		OppPlanRelationshipWrapperList.add(relationshipWrapper);
	}
	
	public pageReference removeRelationship() {
		System.debug('relationship wrapper size: ' + relationshipWrapperSize);
		
		// Only delete if there is more than one wrapper. 
		if (relationshipWrapperSize > 1) {
			Integer wrapperChangedInt = Integer.valueOf(wrapperChanged);
			
			// Remove the selected wrapper, the integer changed - 1. 
			oppPlanRelationshipWrapperList.remove(wrapperChangedInt - 1);
			
			// Decrement the count of wrappers. 
			relationshipWrapperSize--;
			updateRelationshipNumbers(OppPlanRelationshipWrapperList);
		}
 
		return null;
	}
	
	public pageReference removeExistingRelationship() {
		
		// Remove the relationship from the existing relationships and add it into a section which shows relationships to be deleted. 
		
		Integer wrapperChangedInt = Integer.valueOf(existingWrapperChanged);
		
		// Remove the selected wrapper, the integer changed - 1. 
		OppPlanRelationshipWrapper existingWrapperToDelete =  existingRelationshipWrapperList.remove(wrapperChangedInt - 1);
		
		// Revert the wrapper back to the original values incase the user has changed the values. 
		Plan_Contact_Relationship__c planContactRelationship = 	originalExistingContactsMap.get(existingWrapperToDelete.existingRelationshipId);
		Opportunity_Plan_Contact__c contactTwo = originalContactIdToOppPlanContactMap.get(planContactRelationship.Contact_2__r.Contact__c);
		
		existingWrapperToDelete.selectedRelationship = planContactRelationship.Relationship__c;
		existingWrapperToDelete.selectedContactTwo = contactTwo.Contact__c;
		existingWrapperToDelete.selectedContactTwoName = contactTwo.Contact__r.Name;
		
		displayExistingToDelete = true;
		
		if (existingRelationshipWrapperList.isEmpty()) {
			displayExisting = false;
		}
		
		// Add the selected wrapper to the list of wrappers to delete. 
		existingRelationshipToDeleteList.add(existingWrapperToDelete);
		
		updateRelationshipNumbers(existingRelationshipToDeleteList);
		updateRelationshipNumbers(existingRelationshipWrapperList);
		
		return null;
	}
	
	public pageReference undoDelete() {
		Integer deletedWrapperChangedInt = Integer.valueOf(deletedWrapperChanged);
		
		OppPlanRelationshipWrapper existingWrapperToUndelete = existingRelationshipToDeleteList.remove(deletedWrapperChangedInt - 1);
		
		displayExisting = true;
		
		if (existingRelationshipToDeleteList.isEmpty()) {
			displayExistingToDelete = false;
		}
		
		existingRelationshipWrapperList.add(existingWrapperToUndelete);
		
		updateRelationshipNumbers(existingRelationshipToDeleteList);
		updateRelationshipNumbers(existingRelationshipWrapperList);
		
		return null;
	}
	
	
	public void updateRelationshipNumbers(List<OppPlanRelationShipWrapper> oppPlanRelationshipWrapperList) {
		
		// Go through the wrappers and reassign the relationship number. 
		for (Integer i = 0; i < oppPlanRelationshipWrapperList.size(); i++) {
			oppPlanRelationshipWrapperList.get(i).relationshipNumber = i + 1;
		}
	}
	
	public PageReference save() {
		PageReference pageRef;
		
		// validate the list to make sure there is no duplicates. 
		if (validate(OppPlanRelationshipWrapperList)) {
			 
			 // If the list is valid, build the record. 
			 List<Plan_Contact_Relationship__c> relationshipToInsert = new List<Plan_Contact_Relationship__c>();
			 List<Plan_Contact_Relationship__c> existingRelationshipToUpdate = new List<Plan_Contact_Relationship__c>();
			 List<Plan_Contact_Relationship__c> existingRelationshipToDelete = new List<Plan_Contact_Relationship__c>();
			 
			 // Build the record for each wrapper. 
			 for (OppPlanRelationshipWrapper wrapper : OppPlanRelationshipWrapperList) {
			 	if (wrapper.selectedContactOne != NONE_VALUE && wrapper.selectedRelationship != NONE_VALUE && wrapper.selectedContactTwo != NONE_VALUE) {
			 		Plan_Contact_Relationship__c newRelationship = new Plan_Contact_Relationship__c();
				 	
				 	newRelationship.Contact_1__c = originalContactIdToOppPlanContactMap.get(wrapper.selectedContactOne).Id;
				 	newRelationship.Relationship__c = wrapper.selectedRelationship;
				 	newRelationship.Contact_2__c = originalContactIdToOppPlanContactMap.get(wrapper.selectedContactTwo).Id;
				 	
				 	relationshipToInsert.add(newRelationship);
			 	}
			 }
			 
			 // Update the records of the plan contact relationship 
			 for (OppPlanRelationshipWrapper wrapper : existingRelationshipWrapperList) {
			 	Plan_Contact_Relationship__c existingRelationship = originalExistingContactsMap.get(wrapper.existingRelationshipId);
			 	Opportunity_Plan_Contact__c oppPlanContactTwo = originalContactIdToOppPlanContactMap.get(wrapper.selectedContactTwo);
			 	Boolean needUpdate = false;
			 	
			 	if (existingRelationship.Relationship__c != wrapper.selectedRelationship) {
			 		existingRelationship.Relationship__c = wrapper.selectedRelationship;
			 		needUpdate = true;
			 	}
			 	
			 	if (existingRelationship.Contact_2__c != oppPlanContactTwo.Id) {
			 		existingRelationship.Contact_2__c = oppPlanContactTwo.Id;
			 		needUpdate = true;
			 	}
			 	
			 	if (needUpdate) {
			 		existingRelationshipToUpdate.add(existingRelationship);
				}
			}
			 
			// Go through the list of contacts to delete and build the list. 
			for (OppPlanRelationshipWrapper wrapper : existingRelationshipToDeleteList) {
				existingRelationshipToDelete.add(originalExistingContactsMap.get(wrapper.existingRelationshipId));
			}
			 
			// Update the records if it is necessary. 
			if (!existingRelationshipToUpdate.isEmpty()) {
				update existingRelationshipToUpdate;
			}
			
			// Delete the relationships
			if (!existingRelationshipToDelete.isEmpty()) {
				delete existingRelationshipToDelete;
			}
			
			// Insert the records
			insert relationshipToInsert;
			 
			// Return back to the RETURL
			pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
			 
		} else {
			
			// If there are more than one relationships between two particular contacts, show an error. 
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 	'There is duplicated relationships between contacts or' +
																					' there is a relationship between the same contact.' + 
																					' Please fix these errors and try again.'));
		}
		
		return pageRef;	
	}
	
	public boolean validate(List<OppPlanRelationShipWrapper> OppPlanRelationshipWrapperList) {
		Boolean validList = true;
		Map<String, Set<String>> contactOneMapContactTwo = new Map<String, Set<String>>();
		
		// Build the mapping with the existing relationships first. 
		for (OppPlanRelationshipWrapper existing : existingRelationshipWrapperList) {
			Set<String> contactTwoSet;
			
			if (contactOneMapContactTwo.containsKey(existing.selectedContactOne)) {
				contactTwoSet = contactOneMapContactTwo.get(existing.selectedContactOne);
				
				if (contactTwoSet.contains(existing.selectedContactTwo)) {
					validList = false; 
					break;
				} 
			} else {
				contactTwoSet = new Set<String>();
			}
			
			contactTwoSet.add(existing.selectedContactTwo);
			contactOneMapContactTwo.put(existing.selectedContactOne, contactTwoSet);
		}
		  
		
		// Go through each wrapper to see if there are any invalid relationships.
		// A relationship is invalid if there is already an relationship from that particular contact one to another particular contact two. 
		for (OppPlanRelationshipWrapper wrapper : OppPlanRelationshipWrapperList) {
			Set<String> contactTwoSet; 
			
			if (wrapper.selectedContactOne == NONE_VALUE || wrapper.selectedRelationship == NONE_VALUE || wrapper.selectedContactTwo == NONE_VALUE) {
				continue;
			} 
			 
			// Make sure the selected contacts are not the same. 
			if (wrapper.selectedContactOne != wrapper.selectedContactTwo) {
				System.debug('contactOneMapContactTwo: ' + contactOneMapContactTwo.keySet());
				System.debug('does the map contain this key: ' + wrapper.selectedContactOne + '?  ' + contactOneMapContactTwo.containsKey(wrapper.selectedContactOne));
				if (contactOneMapContactTwo.containsKey(wrapper.selectedContactOne)) {
					contactTwoSet = contactOneMapContactTwo.get(wrapper.selectedContactOne);
					
					// If the contact one is already in the map, check to make sure the contact two
					// is not already contained in he list. If it is, that means there is a duplicate 
					// and invalid. 
					if (contactTwoSet.contains(wrapper.selectedContactTwo)) {
						validList = false; 
						break;
					} 
				} else {
					contactTwoSet = new Set<String>();
					
				}
				
				contactTwoSet.add(wrapper.selectedContactTwo);
				contactOneMapContactTwo.put(wrapper.selectedContactOne, contactTwoSet);
			} else {
				validList = false;
				break;				
			}
		}
		
		System.debug('valid list? ' + validList);
		return validList;
	}
	 
	public virtual class OppPlanRelationshipWrapper {
		public List<Opportunity_Plan_Contact__c> contactOneList {get; set;}
		public List<Opportunity_Plan_Contact__c> contactTwoList {get; set;}
		public Integer relationshipNumber {get; set;}
		public String selectedContactOne {get; set;}
		public String selectedContactOneName {get; set;}
		public String selectedContactTwo {get; set;}
		public String selectedContactTwoName {get; set;}
		public String selectedRelationship {get; set;}
		public Id existingRelationshipId {get; set;}
		
		public OppPlanRelationshipWrapper(Opportunity_Plan_Contact__c contactOne, Opportunity_Plan_Contact__c contactTwo, List<Opportunity_Plan_Contact__c> contactTwoList, String relationship, Id existingRelationshipId, Integer relationshipNumber) {
			Integer indexToRemove; 
			
			selectedContactOne = contactOne.Contact__c;
			selectedContactOneName = contactOne.Contact__r.Name; 
			selectedContactTwo = contactTwo.Contact__c;
			selectedContactTwoName = contactTwo.Contact__r.Name;
			selectedRelationship = relationship;	
			this.relationshipNumber = relationshipNumber;
			this.contactTwoList = contactTwoList;
			
			for (Integer i = 0; i < contactTwoList.size(); i++) {
				if (contactOne.Id == contactTwoList.get(i).Id) {
					indexToRemove = i;
					break;
				}
			}
			
			contactTwoList.remove(indexToRemove);
			this.existingRelationshipId = existingRelationshipId;
		}
		
		public OppPlanRelationshipWrapper(List<Opportunity_Plan_Contact__c> contactOneList, List<Opportunity_Plan_Contact__c> contactTwoList, Integer relationshipNumber, Id defaultContactOneId) {
			System.debug('contact one list constructor: ' + contactOneList);
			this.contactOneList = contactOneList;
			this.contactTwoList = contactTwoList;
			this.selectedContactOne = defaultContactOneId;
			this.relationshipNumber = relationshipNumber;
		}
		
		public void setRelationshipNumber(Integer relationshipNumber) {
			this.relationshipNumber = relationshipNumber;
		}
		
		public List<SelectOption> getContactOneSelectList() {
			System.debug('get contact one select list: ' + contactOneList);
			return buildContactSelectList(contactOneList);
		}
		
		public List<SelectOption> getContactTwoSelectList() {
			return buildContactSelectList(contactTwoList);
		}
		
		public virtual List<SelectOption> getRelationshipType() {
			List<SelectOption> relationType = new List<SelectOption>();
			
			relationType.add(new SelectOption(NONE_VALUE, NONE_VALUE));
			relationType.add(new SelectOption('Positive/Alliance','Positive/Alliance'));
			relationType.add(new SelectOption('Negative/Tension', 'Negative/Tension'));
			
			return relationType;
		}
		
		public virtual List<SelectOption> buildContactSelectList(List<Opportunity_Plan_Contact__c> contactList) {
			List<SelectOption> contactOptions = new List<SelectOption>();
			
			contactOptions.add(new SelectOption(NONE_VALUE, NONE_VALUE));
			for (Opportunity_Plan_Contact__c oppPlanContact : contactList) {
				System.debug('opp plan contact: ' + oppPlanContact);
				if (oppPlanContact.Contact__c != null) {
					contactOptions.add(new SelectOption(oppPlanContact.Contact__r.Id, oppPlanContact.Contact__r.Name));
				} 
				
				// contactOptions.add(new SelectOption(oppPlanContact.Contact__r.Id, oppPlanContact.Contact__r.Name));
			}
			 
			System.debug('contact options: ' + contactOptions);
			return contactOptions;
		}
	}
	
	public class ExistingOppPlanRelationshipWrapper extends OppPlanRelationshipWrapper {
		public ExistingOppPlanRelationshipWrapper(Opportunity_Plan_Contact__c contactOne, Opportunity_Plan_Contact__c contactTwo, List<Opportunity_Plan_Contact__c> contactTwoList, String relationship, Id existingRelationshipId, Integer relationshipNumber) {
			super(contactOne, contactTwo, contactTwoList, relationship, existingRelationshipId, relationshipNumber);
		}
		
		public override List<SelectOption> getRelationshipType() {
			List<SelectOption> relationType = new List<SelectOption>();
			
			relationType.add(new SelectOption('Positive/Alliance','Positive/Alliance'));
			relationType.add(new SelectOption('Negative/Tension', 'Negative/Tension'));
			
			return relationType;
		}
		
		public override List<SelectOption> buildContactSelectList(List<Opportunity_Plan_Contact__c> contactList) {
			List<SelectOption> contactOptions = new List<SelectOption>();
			
			for (Opportunity_Plan_Contact__c oppPlanContact : contactList) {
				if (oppPlanContact.Contact__c != null) {
					contactOptions.add(new SelectOption(oppPlanContact.Contact__r.Id, oppPlanContact.Contact__r.Name));	
				}
			}
			 
			System.debug('contact options: ' + contactOptions);
			return contactOptions;
		}
	}
}