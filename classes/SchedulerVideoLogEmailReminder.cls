/*
Author:     Sanket Vaidya
Date:       March 23rd 2017
Description:  This class is used to schedule BatchVideoLogEmailReminder.cls which operates on records of object Video_Log__c. 
*/
public class SchedulerVideoLogEmailReminder implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        Database.executeBatch(new BatchVideoLogEmailReminder());
    }
}