/**=====================================================================
 * Experian, Inc
 * Name:   AccountPlanHierController
 * Created Date:  Mar 19, 2016
 * Created By:    Tyaga Pati (Experian)
 * Description: This is the controller for the Visual Force Page AccountPlanHierTree Which is 
                responsible for Building the Hierarchy Data for the Tree Display encompassing
                every single Account from the Ultimate Parent Account to the Lowest Level Child.
                This Controller in turn calls the AccountPlanHierHelper class to build the html.
                and the AccountplanHelper Class to Create the Account plan Record.
                
 *                    
 * Date Modified             Modified By                 Description of the update
 * Mar 19, 2016              Tyaga Pati(Experian)         
 * May 10, 2016              Tyaga Pati(Experian)        01959705 - Updated Html section and Query to make sure Region and Country can be added Account Hierarchy List view.
 * June 10, 2016             Tyaga Pati(Experian)        Account plan Edit Functionality 
 * June 30, 2016             Manoj Gopu                  01966606-Updated code to restrict the users to select the parent accounts to create account plans
 * June 30, 2016             Mauricio Murillo            02346388 - Added method to get account plans from hierarchy
 =====================================================================*/
 
global class AccountPlanHierController{
  public ApexPages.standardController stdController {get; set;}
  public Account AccountObj {get;set;}
  public map<string,string> mapAccountswithParents=new map<string,string>();//Added by Manoj to check whether the selected Account is parent or not
  public boolean isSpecifiedProfile;
  //Public Static Integer countMatchMax ;
  Public Integer TotalNumOfHtmlElmnts {get{return getTotalCountOfElements();} set;}
  Public Id acntPlanIdtoUpdate {get;set;}
  Public String existingAcntLstOnPlan{get;set;}//This property will hold the Accounts already associated with the plan. This will have value only in update scenario
  Public String existingAcntIds {get;set;}
  Public List<Account> accountsInHierarchy {get;set;}//Mauricio: created list to populate in constructor so it can be reused among the code
  //=====================================================================
  // Constructor taking standard controller as a parameter
  //=====================================================================
  public AccountPlanHierController(ApexPages.standardController controller) {
        stdController = controller;
        //String AccountId = ApexPages.currentPage().getParameters().get('id');
        //String existingAcntIds = ApexPages.currentPage().getParameters().get('existingAcntLstOnPlan');
        existingAcntIds = ApexPages.currentPage().getParameters().get('existingAcntLstOnPlan');
        String AccountId = controller.getId();
        if(String.isNotBlank(AccountId)) {
        
            list<string> lstParents=new list<string>();
            AccountObj = [Select Id ,Ultimate_Parent_Account__c, Ultimate_Parent_Account_ID__c, Region__c, Country_of_Registration__c, parentId, Name from Account where Id = :AccountId LIMIT 1];
            
            Id UltimateAcntId = AccountObj.Ultimate_Parent_Account__c;
            if(UltimateAcntId == null)
              UltimateAcntId = AccountObj.Id;
            
            accountsInHierarchy = [Select Id ,Ultimate_Parent_Account__c, Region__c, Country_of_Registration__c, Ultimate_Parent_Account_ID__c, parentId, Name 
                         from Account  
                         where Ultimate_Parent_Account__c =:UltimateAcntId
                         OR Id =:UltimateAcntId];
            
            
            //Added by Manoj to add the acount id
            lstParents.add(AccountId);
            mapAccountswithParents.put(AccountObj.Id,AccountObj.ParentId); // Add the selected accounts to map
            if(AccountObj.ParentId!=null)
                lstParents.add(AccountObj.ParentId);//Added by Manoj to add the parent id
            //Added by Manoj to get the child accounts and same level accounts 
            string profileName='';
            isSpecifiedProfile=false;
            list<Profile> lstProf=[select id,Name from Profile where id=:Userinfo.getProfileId()];
            if(lstProf!=null && lstProf.size()>0)
            {
                profileName=lstProf[0].Name;
            }
            list<Profiles_w_override_access_2_AcctContact__c> lstCustProf= Profiles_w_override_access_2_AcctContact__c.getAll().Values();
            for(Profiles_w_override_access_2_AcctContact__c obj:lstCustProf)
            {
                if(obj.Profile_Name__c==profileName)
                {
                    isSpecifiedProfile=true;
                }
            }
            if(isSpecifiedProfile==false)
            {
                list<string> lstchilds=new list<string>();
                for(Account acc:[select id, Name, ParentId from Account where ParentId=:lstParents])
                {
                    system.debug('Accccccccccccc'+acc);
                    lstchilds.add(acc.Id);
                    mapAccountswithParents.put(acc.Id,acc.ParentId);                    
                }
                for(Account acc1:[select id, Name, ParentId from Account where ParentId=:lstchilds])
                {                    
                    mapAccountswithParents.put(acc1.Id,acc1.ParentId);                  
                }
                for(AccountTeamMember accTMem:[select id,AccountId,UserId from AccountTeamMember where UserId=:Userinfo.getUserId()])
                {
                    mapAccountswithParents.put(accTMem.AccountId,accTMem.AccountId);
                }
            }
        }
  } 
  //=====================================================================
  // Method to Generate the Maximum Count to Iterate through the whole List
  // Needed to be used in vf Page to do Preselection
  //===================================================================== 
  public Integer getTotalCountOfElements(){
    Integer CountOfAcnts = 0;
    Id UltimateAcntId = AccountObj.Ultimate_Parent_Account__c;
    if(UltimateAcntId == null)
      UltimateAcntId = AccountObj.Id;
    for (Account ac : ([Select Id from Account where 
                        Ultimate_Parent_Account__c =:UltimateAcntId
                        OR Id =:UltimateAcntId])) {      
    
      CountOfAcnts++;
    }  
  
  return CountOfAcnts;  
  }
  
  //=====================================================================
  // Method used to create a tree view html. 
  // @return String  generated tree html
  //===================================================================== 
  public String getAcntTreeHierarchy(){
    List<Account> masterAcntList = new List<Account>();
    
    Map<Id,Account> mapIdToAccount = New Map<Id, Account>();
    //adding code for Account plan Edit Scenario
    String existingAcntIds = ApexPages.currentPage().getParameters().get('existingAcntLstOnPlan');
    Id UltimateAcntId;
    UltimateAcntId = AccountObj.Ultimate_Parent_Account_ID__c;
    if(UltimateAcntId == null)
      UltimateAcntId = AccountObj.Id;
    
    for (Account ac : ([Select Id ,Ultimate_Parent_Account__c, Region__c, Country_of_Registration__c, Ultimate_Parent_Account_ID__c, parentId, Name 
                                     from Account  
                                     where Ultimate_Parent_Account__c =:UltimateAcntId
                                     OR Id =:UltimateAcntId])) {
          if (!mapIdToAccount.containsKey(ac.ID)) {
           mapIdToAccount.put(ac.ID, ac);
           }     
          masterAcntList.add(ac);
     }
    masterAcntList.sort();
    String tempTree = '';
    if(masterAcntList.size() ==1){
        //tempTree = '<li style="white-space:nowrap;" id="masterplan" ><span  class="icon-close"></span><input onclick="enableChildPlans(this.id,\'tree\')" type="checkbox" id="checkbx-'+masterAcntList[0].Id+'" /><b onclick="enableCheckbox(this.id)" id="text-'+masterAcntList[0].Id+'">'+masterAcntList[0].Name+'</b></li>';
        tempTree = '<li style="white-space:nowrap;" id="masterplan" ><span  class="icon-close"></span><input onclick="enableChildPlans(this.id,\'tree\')" type="checkbox" id="checkbx-'+masterAcntList[0].Id+'" /><b onclick="enableCheckbox(this.id)" id="text-'+masterAcntList[0].Id+'">'+masterAcntList[0].Name+'  ('+masterAcntList[0].Region__c+')'+'  ('+masterAcntList[0].Country_of_Registration__c+')'+'</b></li>';
  
    }
    else
    {
        if(masterAcntList.size() > 1100){
           system.debug('*** This is a separate function on AccountHelperClass to directly create a plan and take the user to the plan page'); 
           //Call a function on the AccountHierHelper Class with the Hierarchy and the Initial Account to get all the child Accounts below it.
           //AccountPlanHelperClass.CreateAccountPlanOffline(mapIdToAccount,NewAccountObj.Id);
           Id selectedAcntId = AccountObj.Id;
           //Below line will be called to fetch account list. disabled for immediate release.
           //List<Account> acntList = AccountPlanHierHelper.createChildAccountList(mapIdToAccount,selectedAcntId);
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Big Accounts are not supported as of now, please contact administrator'));
           return null;
        }
        else {
            tempTree = AccountPlanHierHelper.createPlanTreeHierarchy(masterAcntList,UltimateAcntId);
            if(tempTree ==null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'System Failed to Build Account Hierarchy, Please Validate Account Hierarchy!'));
            
                }
            Integer counter = 0;
            Integer countMatchMax = tempTree.countMatches('text-');
            countMatchMax = tempTree.countMatches('checkbx-') > countMatchMax ? tempTree.countMatches('checkbx-') : countMatchMax;
            countMatchMax = tempTree.countMatches('label-') > countMatchMax ? tempTree.countMatches('label-') : countMatchMax;
            system.debug('countMatchMax ++++++++++ '+countMatchMax);
            for ( ; countMatchMax > -1; countMatchMax--) { 
              tempTree = tempTree.replaceFirst('text-', ++counter+'-');
              tempTree = tempTree.replaceFirst('checkbx-', ++counter+'-');
              tempTree = tempTree.replaceFirst('label-', ++counter+'-');
            }
      }//End of Else for if the hierarchy is not Big Enough.   
    }//end of Else
        
    system.debug('**** The HTML String Generated for the Account Hierarchy looks like *****: '+ tempTree );
    return tempTree;
  }
  public Boolean noMasterPlan {get;set;} 
  
  //=======================================================================
  // @ return PageReference to the Newly Created Plan Page
  //=======================================================================
  public PageReference saveAccountsForPlan () {
    system.debug('******The Contoller Save is Called with this Account Set *****.'+ApexPages.currentPage().getParameters().get('selectedAcnts'));
    Id PlanIdtoUpdate = ApexPages.currentPage().getParameters().get('acntPlanIdtoUpdate');
    system.debug('Tyaga the Account plan to Edit is ' + PlanIdtoUpdate);
    Account account = (Account)stdController.getRecord();
    Map <String, String> mapParentToChildProcessed = new Map <String, String>();
    Account_Plan__c NewAccPlan = New Account_Plan__c();
    PageReference pr = Page.AccountPlanTabPanel;
    //Account_Plan__c UpdatedAccPlan = New Account_Plan__c();
    try{
        if (String.isNotBlank(ApexPages.currentPage().getParameters().get('selectedAcnts'))){
              system.debug('+++++++++++++++++++++++++++ selectedPlans '+ApexPages.currentPage().getParameters().get('selectedAcnts'));
              Map<Id, Id> mapAccountsToId = new Map<Id, Id> ();
              List<String> selectedAcnts= new List<String> ();
              for(String childAccountId : ApexPages.currentPage().getParameters().get('selectedAcnts').split(',')){
                      selectedAcnts.add(childAccountId.substringAfterLast('-'));
                    }
                    system.debug('mapAccountswithParents@@'+mapAccountswithParents);
                //Added by Maoj to findout whether parent accounts are selected if yes throw an error
                if(isSpecifiedProfile==false)
                {
                    for(string str:selectedAcnts)   
                    {
                        if(!mapAccountswithParents.containsKey(str))
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Account_Hierarchy_Error_When_selecting_Parent_Accounts));
                            return null;
                        }                           
                    }
                }
              //Determine the Primary account by finding out the Highest Level Account.
              Account NewAccountObj = FindHighestLeveAcntSelected(selectedAcnts,AccountObj.Id);
              //Call Helper Class to Create The plan entities.
              system.debug('**** The plan to be updated is'+ acntPlanIdtoUpdate + ' : and the New Primary acnt Obj should be :' + NewAccountObj);
              if(PlanIdtoUpdate <> null){
                 Account_Plan__c UpdatedAccPlan = AccountPlanHelperClass.UpdateAccountPlan(selectedAcnts,NewAccountObj.Id,PlanIdtoUpdate);
                 pr.getParameters().put('id',UpdatedAccPlan.Id);
              }
              else{
                  NewAccPlan = AccountPlanHelperClass.CreateAccountPlan(selectedAcnts,NewAccountObj.Id);
                  pr.getParameters().put('id',NewAccPlan.Id);
              }
              //Function Call to Create the Team Members for the plan 
              AccountPlanHelperClass.updateAccountPlanTeamMembers(NewAccountObj.Id, NewAccPlan);//Updated for Task: Creating a new plan - Insufficient Privleges    
         }
        else if(String.isBlank(ApexPages.currentPage().getParameters().get('selectedAcnts'))){
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Account_Hier_Tree_Error_No_Selection));
              return null;    
        }
     
    } catch(Exception ex){
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Account_Hierarchy_Tree_Error_Saving_Plans));
       return null;
    }

    return pr;
  }  
    //=======================================================================
  // @ return The Primary Account: Iterate through List to see if 
  //   There is another Account thats at a higher level than the
  //   account from which Plan creation was initiated.
  //======================================================================= 
  Public Account FindHighestLeveAcntSelected(List<String> AcntLists, Id AcntId){
      Id AcntHighestInSelect = AcntId;
      //Variable to hold the Ultimate Parent Id
      Id UltimateParentId =[Select  Ultimate_Parent_Account_ID__c From Account where Id = :AcntId].Ultimate_Parent_Account_ID__c;
      Map<Id,List<Id>> ParentToChild = New Map<Id,list<Id>>();  //Map to Hold Parent to All child 
      Map<Id, Id> LowerLevelAcnts = New Map<Id,Id>();
      //Iterate through List to create a map of Accounts that are above the current Account
      Map<Id,Id> AllIdMap = New Map<Id,Id>();
      for(Account acctemp : [Select Id, parentId From Account where Id in :AcntLists]){
          AllIdMap.put(acctemp.Id,acctemp.Id);
          if (!ParentToChild.containsKey(acctemp.parentId)) {
               ParentToChild.put(acctemp.parentId, New List<Id>());
               }
          ParentToChild.get(acctemp.parentId).add(acctemp.Id);
       }
      if(!AllIdMap.containskey(UltimateParentId)&&(UltimateParentId!=null)){
      //Create a Map of All Accounts that are directly or indirectly below the current Account
      LowerLevelAcnts.put(AcntId,AcntId);
      if(ParentToChild.get(AcntId)!= null) {
      for(Id TempacId :ParentToChild.get(AcntId)){
          if(ParentToChild.keyset().contains(TempacId)){
                LowerLevelAcnts.put(TempacId,TempacId);
                for(Id Id1:ParentToChild.get(TempacId)){  
                     LowerLevelAcnts.put(TempacId,TempacId);
                 }
              }
           }  
      }
      Map<Id, Id> IdParentIdMap = New Map<Id,Id>();
      for(Account acc:[Select Id, parentId From Account where Id in :AcntLists and parentId in :AcntLists  ]){
             if(!LowerLevelAcnts.containskey(acc.parentId))
                 IdParentIdMap.put(acc.Id,acc.parentId);        
          }
      for(Id tempacnt : IdParentIdMap.Keyset()){
              Map<Id,Id> tempMap = New Map<Id,Id>();
              if(ParentToChild.get(AcntHighestInSelect) != null){
                  for(Id acid :ParentToChild.get(AcntHighestInSelect)){
                          tempMap.put(acid,acid);
                      }
              }
              if(tempMap!=null){
                  if(!tempMap.containskey(IdParentIdMap.get(tempacnt))){
                      AcntHighestInSelect= IdParentIdMap.get(tempacnt);
                  }
              } 
      }   
   }//end of else for check of ultimate parent id
   else
   {
       if(UltimateParentId==null)       
            AcntHighestInSelect = AcntId;
       else
           AcntHighestInSelect = UltimateParentId ;
   }

return [select Id from Account where Id =:AcntHighestInSelect];
  
  }
  //=======================================================================
  // @ return PageReference to Account Page
  //=======================================================================
  public PageReference GoBacktoAccountPage() {
       Id PlanIdTogoBack = ApexPages.currentPage().getParameters().get('acntPlanIdtoUpdate');
       system.debug('Tyaga the value of plan id is ' + PlanIdTogoBack);
       PageReference prBackNavigation = Page.AccountPlanTabPanel;
       if(PlanIdTogoBack == null){
            
            return stdController.view();
       }
       else{
        
        prBackNavigation.getParameters().put('id',PlanIdTogoBack);
        system.debug('Tyaga the page reference generated is' + prBackNavigation );
        //prBackNavigation = "https://experian--supportuat--c.cs23.visual.force.com/apex/AccountPlanTabPanel?id=a181800000012s2&sfdc.override=1';
        // return prBackNavigation;
        prBackNavigation.setRedirect(true);
        return prBackNavigation ;
       }
  } 
  //MM: added method to get list of account plans
  public List<Account_Plan__c> getAccountPlanList(){
      List<Account_Plan__c> accountPlans = new List<Account_Plan__c>();
      accountPlans = [select id, name, account__c, ownerId from Account_Plan__c where account__c in :accountsInHierarchy];
      return accountPlans;
  }   
  
}