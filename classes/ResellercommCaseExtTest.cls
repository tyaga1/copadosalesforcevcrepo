@isTest
private class ResellercommCaseExtTest {
	
	@isTest static void testMethod1() {
		String serviceCentralId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Central').getRecordTypeId();
	 	Account testAccount1 = Test_Utils.createAccount();
	 	insert testAccount1;
	
		Contact testContact = Test_Utils.createContact(testAccount1.id);
	 	insert testContact;

	 	Case c1 = new Case(
					RecordTypeId = serviceCentralId,
					AccountId = testAccount1.Id,
					Subject = 'Test Case 1',
					Description = 'Test Description 1',
					status = 'Closed-Complete');

		insert c1;

 		User testUser = Test_Utils.createUser('Xperian Global Community ReSellers');
 		testUser.ContactId = testContact.Id;

		

		ApexPages.currentPage().getParameters().put('id', c1.id);

		ResellercommCaseExt controller = new ResellercommCaseExt();

		Case newCase = ResellercommCaseExt.cloneCase(String.valueOf(c1.id));
		ResellercommCaseExt.getSubcodes();
		ResellercommCaseExt.getRTSubcode('Service Central');
		ResellercommCaseExt.submitCase(newCase, 'Service Central');
	}
	
}