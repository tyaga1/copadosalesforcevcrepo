/**=====================================================================
 * Appirio, Inc
 * Name: AccountRedirectController_Test
 * Description: Test class for AccountRedirectController
 * Created Date: Oct 08th, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * Apr 7th, 2016      Paul Kissick                 Case 01932085: Fixing Test User Email Domain
*  =====================================================================*/
@isTest
private class AccountRedirectController_Test {
  
  static testmethod void testAccountRedirect_forSerasa(){
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SERASA_FINANCE ];
    PermissionSet perSet = [SELECT Id, Name FROM PermissionSet
                              WHERE Name =: Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER];
                                
    User testUser = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    testUser.EmployeeNumber = 'E1234';
    insert testUser;
    
    PermissionSetAssignment psetAssgn = new PermissionSetAssignment (AssigneeId = testUser.Id, PermissionSetId = perSet.Id);
    //insert psetAssgn;                        
    system.debug('====psetAssgn>>>' +psetAssgn);
    
    Serasa_User_Profiles__c custSettings = new Serasa_User_Profiles__c(Name = Constants.SERASA, Profiles__c= Constants.PROFILE_EXP_SERASA_FINANCE) ;
    insert custSettings;
     
    Account account = Test_Utils.insertAccount();
    
    system.runAs(testUser) {
    Test.startTest();
    
	    ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)account);
	    AccountRedirectController controller = new AccountRedirectController(stdController);
	    
	    controller.retURL = null;
	    // call controller methods
	    controller.getNewAccountPermSets();
	    controller.getNewAccountProfSets();
	    
	    controller.redirect();
	    //Asserts
      System.assertNotEquals(null, controller.redirect());
	    	    
	    controller.newAccount();
	    controller.newAccountAndAddress();
	    	    
	    Test.stopTest();
    }
  }
  
  //
  static testmethod void testAccountRedirect_forOtherUser(){
    // create test data
    Profile p = [select id from profile where name=: Constants.PROFILE_EXP_SALES_EXEC ];
      
    User testUser = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    insert testUser;
     
    Account account = Test_Utils.insertAccount();
    
    system.runAs(testUser) {
    Test.startTest();
    
      ApexPages.StandardController stdController = new ApexPages.StandardController((sObject)account);
      AccountRedirectController controller = new AccountRedirectController(stdController);
      
      controller.getNewAccountPermSets();
      controller.getNewAccountProfSets();
      
      controller.redirect();
      //Asserts
      System.assertNotEquals(null,controller.redirect());
      
      controller.newAccountAndAddress();
      controller.newAccount();
      
      Test.stopTest();
    }
  }

}