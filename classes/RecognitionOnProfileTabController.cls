public class RecognitionOnProfileTabController {
    
    public List<WorkBadge> badgeList {get;set;}
    public Boolean displayTable {get;set;}
    public Boolean displayNoRecords {get;set;}
    public String noRecords {get;set;}
    
    public RecognitionOnProfileTabController(ApexPages.StandardController controller) {
        
        displayTable = false;
        displayNoRecords = true;
        String currentUserId = ApexPages.currentPage().getParameters().get('sfdc.userId');
        noRecords = 'No records to display';
        String queryStrBadge = 'SELECT ImageUrl, GiverId, Definition.Name, Giver.Name, RecipientId, CreatedDate, LastModifiedDate, DefinitionId, Description, Message, SourceId, LastViewedDate, Id, Nomination__c '+ 
                              'FROM WorkBadge where RecipientId != null ' +
                              ' AND RecipientId = \''+currentUserId+'\' ' +
                              ' ORDER BY CreatedDate DESC';
                              
        if(currentUserId != null && currentUserId != '') {
            badgeList = Database.query(queryStrBadge);
        }
        
        if(badgeList != null) {
            if(badgeList.size() == 0) {
                displayTable = false;
                displayNoRecords = true;
            }
            else {
                displayTable = true;
                displayNoRecords = false;
            }
        }
    }
}