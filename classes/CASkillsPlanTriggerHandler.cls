/**=====================================================================
 * Experian
 * Name: CASkillsPlanTrigger
 * Description: Trigger on Career_Architecture_Skills_Plan__c
 * Created Date: Jul. 19th 2017
 * Created By: James Wills
 * Date Modified      Modified By           Description of the update
 * July 28th 2017     Alexander McCall      Added method to insert the skill records from the CA Plan into ProfileUserSkill object
 * August 30th 2017   Malcolm Russell       Only add skills to profile when user profile is set to allow
 * August 31st 2017   Malcolm Russell       Added skillUserTableDelete to delete skills when removed from CA
=====================================================================*/

public class CASkillsPlanTriggerHandler{

  public static void afterInsert(List<Career_Architecture_Skills_Plan__c> newList){
    skillUserTableInsert(newList);
      
  }
  
  public static void afterUpdate(List<Career_Architecture_Skills_Plan__c> newList){
    skillUserTableUpdate(newList);      
  }
  
  public static void afterDelete(List<Career_Architecture_Skills_Plan__c> oldList){
    skillUserTableDelete(oldList);      
  }
  
  //============================================================================
  // method to insert the skill records from the CA Plan into ProfileUserSkill object
  //============================================================================
  public static void skillUserTableInsert(List<Career_Architecture_Skills_Plan__c> newList){
    List <ProfileSkillUser> skillUserToInsert = new List <ProfileSkillUser> ();

    Id currentUserId = UserInfo.getUserId();  
    boolean addskill = [select Make_Skills_Public__c  from Career_Architecture_User_Profile__c where employee__c=:currentUserId limit 1].Make_Skills_Public__c;
      
    if (addskill) {   
            
    for (Career_Architecture_Skills_Plan__c C : newList){      
      // instantiate the object to put the skill user values into            
      ProfileSkillUser S = new ProfileSkillUser();
        
      // map the fields from skill plan to user skill
      S.UserId = currentUserId;
      S.ProfileSkillID = C.Skill__c;
      //add these objects to the list to insert
      skillUserToInsert.add(S);
    }
   
    //try insert into SF - catch dml operation errors
    try{
       insert skillUserToInsert;}
    catch(system.DmlException e){
       System.debug(e);
       ApexPages.Message dmlWarn = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
       ApexPages.addMessage(dmlWarn);
      } 
      }
    }

    //============================================================================
    // method to insert the skill records from the CA Plan into ProfileUserSkill object
    //============================================================================
    public static void skillUserTableUpdate(List<Career_Architecture_Skills_Plan__c> newList){
      List<Career_Architecture_Skills_Plan__c> newListForUpdates = new List<Career_Architecture_Skills_Plan__c>();
      
      Id currentUserId = UserInfo.getUserId();
      boolean addskill = [select Make_Skills_Public__c  from Career_Architecture_User_Profile__c where employee__c=:currentUserId limit 1].Make_Skills_Public__c;
      
    if (addskill) {

      Map<ID,ProfileSkillUser> psu_Map = new Map<ID,ProfileSkillUser>([SELECT id, UserId, ProfileSkillId FROM ProfileSKillUser WHERE UserId = :currentUserId]);
      Set<ID> psuForUser_Set = new Set<ID>();
    
      for(ProfileSkillUser psu : psu_Map.values()){
        psuForUser_Set.add(psu.ProfileSkillId);
      }
                
      for (Career_Architecture_Skills_Plan__c casp : newList){      
        if(!psuForUser_Set.contains(casp.Skill__c)){    
          newListForUpdates.add(casp);
        }
      }
      
      if(newListForUpdates.isEmpty()){
        return;
      }
      
      skillUserTableInsert(newListForUpdates);                
      }
    }
    
    //============================================================================
  // method to delete the skill records deleted from CA Plan from ProfileUserSkill object
  //============================================================================
  public static void skillUserTableDelete(List<Career_Architecture_Skills_Plan__c> oldList){
    

    Id currentUserId = UserInfo.getUserId();  
    boolean addskill = [select Make_Skills_Public__c  from Career_Architecture_User_Profile__c where employee__c=:currentUserId limit 1].Make_Skills_Public__c;
      
    if (addskill) { 
    
     Set<id> setEmpSkills = new Set<id>();
    
    for (Career_Architecture_Skills_Plan__c C :oldList){      
      setEmpSkills.add(c.skill__c);
    }
    
    List <ProfileSkillUser> skillUserToDelete = new List <ProfileSkillUser> ();
    
    for(ProfileSKillUser ps : [SELECT id, UserId, ProfileSkillId FROM ProfileSKillUser WHERE UserId = :currentUserId]){
                                                   
                 if( setEmpSkills.contains(ps.ProfileSkillId)){
            skillUserToDelete.add(ps); 
         }                           
     } 
            
    
   
    try{
       delete  skillUserToDelete;}
    catch(system.DmlException e){
       System.debug(e);
      // ApexPages.Message dmlWarn = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
      // ApexPages.addMessage(dmlWarn);
      } 
      }
    }
  
  
  
}