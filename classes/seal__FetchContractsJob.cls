/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FetchContractsJob implements Database.AllowsCallouts, Database.Batchable<seal__Contract__c> {
    global FetchContractsJob() {

    }
    global FetchContractsJob(String contractId) {

    }
    global void execute(Database.BatchableContext bc, List<seal__Contract__c> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global System.Iterable start(Database.BatchableContext BC) {
        return null;
    }
}
