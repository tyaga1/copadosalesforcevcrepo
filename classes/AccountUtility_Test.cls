/**=====================================================================
 * Appirio, Inc
 * Name: AccountUtility_Test
 * Description: To test AccountUtility.cls
 * Created Date: Feb 05th 2015
 * Created By: Gaurav Kumar Chadha (Appirio)
 *
 * Date Modified              Modified By          Description of the update
 * Feb 16th, 2015             Noopur               T-360546 : modified the class to add the new features related to @testSetup
 * Apr 7th, 2016              Paul Kissick         Case 01932085: Fixing Test User Email Domain
 * Jun 16th, 2016             Paul Kissick         Case 02024883: Fixing accountshare problem, and updated to v37.0
 * Aug 11th, 2016			  Cristian Torres	   CRM2:W-005383: Added coverage to the methods fetchAllChildAccounts and fetchChildAccounts
 * Sep 6th, 2016              Paul Kissick         CRM2:W-005383: Cleaned up tests to assert
 =====================================================================*/
@isTest
private class AccountUtility_Test {

  //===========================================================================
  // Declaring static variables at class level
  //===========================================================================
  static map<String, List<AccountTeamMember>> testAccountIdListAccountTeamMemberMap;
  static map<String, AccountShare> testAccountidAccountShareMap = new map<String, AccountShare>();
  static set<Id> accIds;
  static Account acc;
  
  static {
    accIds = new set<Id>();
    for (Account accObj : [SELECT Id, Name FROM Account]) {
      if (accObj.Name != 'testAccount2') {
        acc = accObj;
      }
      accIds.add(accObj.Id);
    }
  }

  //============================================================================
  // Test method for testing fetchAccountTeamMember
  //============================================================================
  static testMethod void forFetchAccountTeamMember() {
    
    Test.startTest();
    
    testAccountIdListAccountTeamMemberMap = AccountUtility.fetchAccountTeamMember(accIds);
    
    Test.stopTest();
    
    system.assert(testAccountIdListAccountTeamMemberMap.containskey(String.valueOf(acc.id)));
    system.assertEquals(2, testAccountIdListAccountTeamMemberMap.get(String.valueOf(acc.id)).size());
  }

  //============================================================================
  // Test method for testing fetchAccountShare
  //============================================================================
  static testMethod void forFetchAccountShare() {
    Map<String, List<AccountShare>> testAccountIdListAccountShareMap;
    
    Test.startTest();
    
    testAccountIdListAccountTeamMemberMap = AccountUtility.fetchAccountTeamMember(accIds);
    testAccountIdListAccountShareMap = AccountUtility.fetchAccountShare(accIDs, testAccountIdListAccountTeamMemberMap);
    
    Test.stopTest();
    
    system.assert(testAccountIdListAccountShareMap.containskey(String.valueOf(acc.id)));
    system.assertEquals(2, testAccountIdListAccountShareMap.get(String.valueOf(acc.id)).size());
  }

  //============================================================================
  // Test method for testing populateAccShareMap
  //============================================================================
  static testMethod void forPopulateAccShareMap() {

    Test.startTest();
    
    AccountUtility.populateAccShareMap(accIds, testAccountidAccountShareMap);
    
    Test.stopTest();
  }
    
    
  //============================================================================
  // Test method for testing fetchAllChildAccounts and fetchChildAccounts
  //============================================================================
  static testMethod void forFetchChildAccounts() {
    
    Account acc1 = Test_Utils.insertAccount();
    
    Account acc2 = Test_Utils.createAccount();
    acc2.ParentID = acc1.ID;
    insert acc2;
    Account acc3 = Test_Utils.createAccount();
    acc3.ParentID = acc1.ID;
    insert acc3;
    Account acc2A = Test_Utils.createAccount();
    acc2A.ParentID = acc2.ID;
    insert acc2A;

    Set<Id> accIdsToCheck = new Set<Id>{acc1.Id, acc2.Id, acc3.Id, acc2A.Id};
    
    Test.startTest();
    
    List<Id> checkIds = AccountUtility.fetchAllChildAccounts(acc1.ID);
    system.assertNotEquals(0, checkIds.size(), 'No Accounts returned!');
    
    for (Id accId : checkIds) {
      system.assert(accIdsToCheck.contains(accId));
    }
      
    Test.stopTest();
  }

  //============================================================================
  //  Method for creating test data to be used in various test methods
  //============================================================================
  @testSetup
  static void createTestdata() {

    AccountShare accShare;
    AccountShare accShare1;
    // Insert account
    acc = Test_Utils.insertAccount();
    Account acc1 = Test_Utils.insertAccount();
    acc1.Name = 'testAccount2';
    update acc1;

    accIds = new set<Id>();
    accIds.add(acc.id);
    accIds.add(acc1.id);

    Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN];

    // Insert user
    User testUser = Test_Utils.createUser(p, 'test@experian.com', 'test');
    insert testUser;
    // Insert user
    User testUser1 = Test_Utils.createUser(p, 'test1@experian.com', 'test1');
    insert testUser1;

    // Create Account Team Members
    Test_Utils.createAccountTeamMembers(String.valueOf(acc.id) , String.valueOf(testUser.id) , true);
    Test_Utils.createAccountTeamMembers(String.valueOf(acc1.id) , String.valueOf(testUser1.id) , true);

    // Create account share
    accShare = [
      SELECT Id, AccountAccessLevel, CaseAccessLevel, OpportunityAccessLevel, AccountId, UserOrGroupId
      FROM AccountShare 
      WHERE AccountId = :acc.Id 
      AND UserOrGroupId = :testUser.Id 
      LIMIT 1
    ];
    accShare1 = [
      SELECT Id, AccountAccessLevel, CaseAccessLevel, OpportunityAccessLevel, AccountId, UserOrGroupId
      FROM AccountShare 
      WHERE AccountId = :acc1.Id 
      AND UserOrGroupId = :testUser1.Id 
      LIMIT 1
    ];

    // Populating account share map
    testAccountidAccountShareMap.put(acc.id, accShare);
    testAccountidAccountShareMap.put(acc1.id, accShare1);
  }
}