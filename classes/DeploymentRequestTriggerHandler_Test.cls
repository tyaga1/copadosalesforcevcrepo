/**=====================================================================
 * Experian
 * Name: DeploymentRequestTriggerHandler_Test
 * Description: 
 * Created Date: 18 Nov 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                Description of the update
 * Apr 7th, 2016      Paul Kissick               Case 01932085: Fixing Test User Email Domain
 =====================================================================*/
@isTest
private class DeploymentRequestTriggerHandler_Test {

  static String testUserEmail = 'Testuseremail213231134@experian.com';

  static testMethod void testApprovalProcess() {
    User tu = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    Test.startTest();
    
    Deployment_Request__c dr1 = [SELECT Id, Status__c FROM Deployment_Request__c WHERE Release__c = 'Support Release 1'];
    
    system.runAs(tu) {
      
      dr1.Status__c = Constants.APP_REQUESTED_DR_STATUS;
      update dr1;
    
    }
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM ProcessInstance WHERE TargetObjectId = :dr1.Id]);
    
  }
  
  static testMethod void testUpdateCaseStatus() {
    User tu = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    Test.startTest();
    
    Deployment_Request__c dr1 = [SELECT Id, Status__c FROM Deployment_Request__c WHERE Release__c = 'Support Release 1' LIMIT 1];
    Deployment_Request__c dr2 = [SELECT Id, Status__c FROM Deployment_Request__c WHERE Release__c = 'Support Release 2' LIMIT 1];
    Deployment_Request__c dr3 = [SELECT Id, Status__c FROM Deployment_Request__c WHERE Release__c = 'Prod Support Release 3' LIMIT 1];
    
    system.runAs(tu) {
      
      dr1.Status__c = 'Deployed to UAT';
      
      dr2.Status__c = Constants.APP_FOR_DEPLOY_DR_STATUS;
      
      dr3.Status__c = Constants.DEPLOYED_TO_PROD_DR_STATUS;
      
      update new List<Deployment_Request__c>{dr1,dr2, dr3};
    
    }
    Test.stopTest();
    
    // Should be deployed to uat
    system.assertEquals(1, [SELECT COUNT() FROM Case WHERE Implementation_Status__c = 'Deployed to UAT' AND Id IN (SELECT Case__c FROM Deployment_Cases__c WHERE Deployment_Request_Slot__c = :dr1.Id)]);
    
    // Should be approved to deploy
    system.assertEquals(1, [SELECT COUNT() FROM Case WHERE Implementation_Status__c = :Constants.APP_FOR_DEPLOY_DR_STATUS AND Id IN (SELECT Case__c FROM Deployment_Cases__c WHERE Deployment_Request_Slot__c = :dr2.Id)]);
    
    // Should be set to implemented
    system.assertEquals(1, [SELECT COUNT() FROM Case WHERE Implementation_Status__c = :Constants.CASE_STATUS_IMPLEMENTED AND Id IN (SELECT Case__c FROM Deployment_Cases__c WHERE Deployment_Request_Slot__c = :dr3.Id)]);
    
  }
  
  static testMethod void testUpdateWorkStatus() {
    User tu = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    Test.startTest();
    
    Deployment_Request__c dr4 = [SELECT Id, Status__c FROM Deployment_Request__c WHERE Release__c = 'Work Prod Support Release 4' LIMIT 1];
    
    system.runAs(tu) {
      
      dr4.Status__c = 'Deployed to UAT';
      
      update new List<Deployment_Request__c>{dr4};
    
    }
    Test.stopTest();
    
    // Should be deployed to uat
    system.assertEquals(1, [SELECT COUNT() FROM agf__ADM_Work__c WHERE agf__Status__c = 'Deployed to UAT' AND Id IN (SELECT Work__c FROM Deployment_Cases__c WHERE Deployment_Request_Slot__c = :dr4.Id)]);
    
  }
  
  static testMethod void testDeleteDeploymentCases() {
    User tu = [SELECT Id FROM User WHERE Email = :testUserEmail LIMIT 1];
    Test.startTest();
    
    Deployment_Request__c dr1 = [SELECT Id, Status__c FROM Deployment_Request__c WHERE Release__c = 'Support Release 1' LIMIT 1];
    
    system.runAs(tu) {
      
      dr1.Status__c = Constants.CANCELLED_DR_STATUS;
      update dr1;
    
    }
    Test.stopTest();
    
    // Should be deployed to uat
    system.assertEquals(0, [SELECT COUNT() FROM Deployment_Cases__c WHERE Deployment_Request_Slot__c = :dr1.Id]);
    
  }
  
  
  @testSetup
  static void setupTestData() {
    
    User mu1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert mu1;
    
    User u1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    u1.Email = testUserEmail;
    u1.ManagerId = mu1.Id;
    insert u1;
    
    // Need to Query permission set name 'Agile Accelerator Admin' to let the test class pass.
    system.runAs(new User(Id = UserInfo.getUserId())) {
      PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Agile_Accelerator_Admin'];
    
      // Assign the above inserted user for the Agile Accelerator Admin Permission Set.
      PermissionSetAssignment psa = new PermissionSetAssignment();
      psa.AssigneeId = u1.id; // tstUser.Id;
      psa.PermissionSetId = ps.Id;
      insert psa;
    }
    
    Account a1 = Test_Utils.insertAccount();
    Case c1 = Test_Utils.insertCase(false,a1.Id);
    Case c2 = Test_Utils.insertCase(false,a1.Id);
    Case c3 = Test_Utils.insertCase(false,a1.Id);
    insert new List<Case>{c1,c2,c3};
    
    Metadata_Component__c md1 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field1__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    Metadata_Component__c md2 = new Metadata_Component__c(
      Component_API_Name__c = 'Test_Component_Field2__c',
      Component_Type__c = 'CustomField',
      Object_API_Name__c = 'CustomObject__c',
      Name = 'Test Component'
    );
    insert new Metadata_Component__c[]{md1,md2};
    
    Case_Component__c cc1 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc2 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c1.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc3 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c2.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc4 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c2.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc5 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c3.Id,
      Component_Name__c = md1.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    Case_Component__c cc6 = new Case_Component__c(
      Action__c = 'Create',
      Case_Number__c = c3.Id,
      Component_Name__c = md2.Id,
      Deployment_Type__c = 'Automated Deployment'
    );
    insert new Case_Component__c[]{cc1,cc2, cc3, cc4, cc5, cc6};
    
    Salesforce_Environment__c src = new Salesforce_Environment__c(
      Name = 'Source',
      Environment_Type__c = 'DEV'
    );
    Salesforce_Environment__c trg = new Salesforce_Environment__c(
      Name = 'Target',
      Environment_Type__c = 'UAT'
    );
    insert new Salesforce_Environment__c[]{src,trg};
    
    Deployment_Request__c dr1 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release_Manager__c = UserInfo.getUserId(),
      Release__c = 'Support Release 1',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id
    );
    
    Deployment_Request__c dr2 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release_Manager__c = UserInfo.getUserId(),
      Release__c = 'Support Release 2',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id,
      Is_Production__c = true,
      Release_Type__c = 'Standard'
    );
    
    Deployment_Request__c dr3 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release_Manager__c = UserInfo.getUserId(),
      Release__c = 'Prod Support Release 3',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id,
      Is_Production__c = true,
      Release_Type__c = 'Standard'
    );
    
    Deployment_Request__c dr4 = new Deployment_Request__c(
      Deployment_Date__c = Date.today().addDays(1),
      Deployment_Lead__c = UserInfo.getUserId(),
      Release_Manager__c = UserInfo.getUserId(),
      Release__c = 'Work Prod Support Release 4',
      Status__c = 'Not Started',
      Source__c = src.Id,
      Target__c = trg.Id,
      Is_Production__c = false,
      Release_Type__c = 'Standard'
    );
    
    insert new Deployment_Request__c[]{dr1, dr2, dr3, dr4};
    
    Deployment_Component__c dco1 = new Deployment_Component__c(
      Case_Component__c = cc1.Id,
      Slot__c = dr1.Id
    );
    Deployment_Component__c dco2 = new Deployment_Component__c(
      Case_Component__c = cc2.Id,
      Slot__c = dr1.Id
    );
    Deployment_Component__c dco3 = new Deployment_Component__c(
      Case_Component__c = cc3.Id,
      Slot__c = dr2.Id
    );
    Deployment_Component__c dco4 = new Deployment_Component__c(
      Case_Component__c = cc4.Id,
      Slot__c = dr2.Id
    );
    Deployment_Component__c dco5 = new Deployment_Component__c(
      Case_Component__c = cc5.Id,
      Slot__c = dr3.Id
    );
    Deployment_Component__c dco6 = new Deployment_Component__c(
      Case_Component__c = cc6.Id,
      Slot__c = dr3.Id
    );
    insert new Deployment_Component__c[]{dco1,dco2,dco3,dco4, dco5, dco6};
    
    Deployment_Cases__c dca1 = new Deployment_Cases__c(
      Case__c = c1.Id,
      Deployment_Request_Slot__c = dr1.Id
    );
    
    Deployment_Cases__c dca2 = new Deployment_Cases__c(
      Case__c = c2.Id,
      Deployment_Request_Slot__c = dr2.Id
    );
    
    Deployment_Cases__c dca3 = new Deployment_Cases__c(
      Case__c = c3.Id,
      Deployment_Request_Slot__c = dr3.Id
    );
    
    // AA Support
    
    //Create Scrum Team
    agf__ADM_Scrum_Team__c newTeam  = new agf__ADM_Scrum_Team__c(Name = 'Test Team',agf__Active__c=true,agf__Cloud__c ='IT');
    insert newTeam;
    
    //Create Agile Product Tag
    agf__ADM_Product_Tag__c  newProductTag = new agf__ADM_Product_Tag__c(Name = 'GCSS Product Team',agf__Team__c =newTeam.Id,agf__Team_Tag_Key__c ='123', agf__Active__c=true );
    insert newProductTag;
      
    Id userStoryTypeId = DescribeUtility.getRecordTypeIdByName('agf__ADM_Work__c', 'User Story');
    
    agf__ADM_Work__c agileStory = new agf__ADM_Work__c();
    
    system.runAs(u1) { 
      //Create Agile Story
      agileStory.agf__Assignee__c = u1.Id;
      agileStory.agf__Subject__c = 'Test Agile Story';
      agileStory.agf__Status__c = 'New';
      agileStory.RecordTypeId = userStoryTypeId;
      agileStory.agf__Description__c = 'Agile Description goes here';
      agileStory.agf__Product_Tag__c = newProductTag.Id;
      insert agileStory;
    }
    
    Deployment_Cases__c dca4 = new Deployment_Cases__c(
      Work__c = agileStory.Id,
      Deployment_Request_Slot__c = dr4.Id
    );
    insert new List<Deployment_Cases__c>{dca1, dca2, dca3, dca4};
    
  }
}