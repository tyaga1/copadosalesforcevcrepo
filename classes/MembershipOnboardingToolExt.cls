/*=============================================================================
 * Experian
 * Name: MembershipOnboardingToolExt
 * Description: Case 01222694
 * Created Date: 15 Jun 2016
 * Created By: Paul Kissick
 *
 * Date Modified       Modified By           Description of the update
 * 8th Nov 2016        Paul Kissick          Case 02194366: Replacing update call on docs required to use currentRecord.
 * 26th Jul 2017       Mauricio Murillo      Case 02425960: Added Info Message when Industry selected = 'Property Management/Owner' and
 *                                           Product to be sold = 'Data Furnishing - Consumer'
 * 22nd Sep 2017       Malcolm Russell       Added Document URL
 =============================================================================*/

public with sharing class MembershipOnboardingToolExt {
  
  ApexPages.StandardController stdCon;
  public Membership__c currentRecord {get;set;}
  //02425960
  public Boolean showDataRepMembershipError {get{
    return ( 
             String.isNotBlank(currentRecord.Selected_Product_to_be_Sold__c) &&
             String.isNotBlank(currentRecord.Selected_Client_Industry__c) &&
             currentRecord.Selected_Client_Industry__c == 'Property Management/Owner' &&
             currentRecord.Selected_Product_to_be_Sold__c == 'Data Furnishing - Consumer'
            );
  }set;}
    
  public class documentDetail {
    public Id docId {get;set;}
    public Boolean done {get;set;}
    public Boolean onFile {get;set;}
    public Boolean notAppl {get;set;}
    public documentDetail(Id docId, Boolean notApp) {
      this.docId = docId;
      done = false;
      onFile = false;
      notAppl = notApp;
    }
  }
  
  public class MembershipOnboardingDocumentWrapper implements Comparable {
    public Membership_Onboarding_Document__c m {get;set;}
    public MembershipOnboardingDocumentWrapper(Membership_Onboarding_Document__c mo) {
      m = mo;
    }
    public Integer compareTo(Object obj) {
      MembershipOnboardingDocumentWrapper modw = (MembershipOnboardingDocumentWrapper)(obj);
      Integer retVal = 0;
      if (this.m.Sort_Order__c > modw.m.Sort_Order__c) retVal = 1;
      if (this.m.Sort_Order__c < modw.m.Sort_Order__c) retVal = -1;
      if (this.m.Sort_Order__c == modw.m.Sort_Order__c) {
        if (this.m.Full_Detail__c > modw.m.Full_Detail__c) retVal = 1;
        else if (this.m.Full_Detail__c < modw.m.Full_Detail__c) retVal = -1;
      }
      return retVal;
    }
  }
  
  public Boolean EditMode {get{if (EditMode == null) EditMode = false; return EditMode;}set;}
  public Integer currentStep {get{
    if (currentStep == null) {
      currentStep = 1;
    }
    return currentStep;
  }set;}
  
  public Map<Id, documentDetail> docDetails {get{
    if (docDetails == null) {
      docDetails = new Map<Id, documentDetail>();
    }
    return docDetails;
  }set;}
  
  private Set<String> allowedPermissiblePurposes = new Set<String>();
  private Set<String> allowedProducts = new Set<String>();
  private Set<String> allowedThirdParty = new Set<String>();
  
  public MembershipOnboardingToolExt(ApexPages.StandardController con) {
    stdCon = con;
    if (!Test.isRunningTest()) {
      stdCon.addFields(new List<String>{
        'Selected_Client_Industry__c',
        'Selected_Product_to_be_Sold__c',
        'Selected_Permissible_Purpose__c',
        'Selected_Third_Party_Involvement__c',
        'Onboarding_Documents_JSON__c',
        'Onboarding_Documents_Complete__c',
        'Onboarding_Documents_Required__c',
        'Onboarding_Bypass__c'
      });
    }
    currentRecord = (Membership__c)stdCon.getRecord();
  }
  
  //===========================================================================
  // Initialise the tool page, since if displayed, the membership record will
  // need to check it has all the docs before being allowed to submit.
  //===========================================================================
  public PageReference loadTool() {
    // If the tool loads, it's on a layout that requires the documents. This sets the required field.
    // But, if the bypass is enabled, then don't change the required field.
    if (currentRecord.Onboarding_Documents_Required__c == false && currentRecord.Onboarding_Bypass__c == false) {
      // Case 02194366 - Fix here
      //Membership__c memb = new Membership__c(
      //  Id = currentRecord.Id, 
      //  Onboarding_Documents_Required__c = true
      //);
      currentRecord.Onboarding_Documents_Required__c = true;
      try {
        //update memb;
        update currentRecord;
      }
      catch (DMLException ex) {
        currentRecord.Onboarding_Documents_Required__c = false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Error occurred: '+ex.getMessage()));
      }
    }
    return null;
  }
  
  public PageReference clearSelectedIndustry() {
    currentRecord.Selected_Client_Industry__c = null;
    currentRecord.Selected_Product_to_be_Sold__c = null;
    currentRecord.Selected_Permissible_Purpose__c = null;
    currentRecord.Selected_Third_Party_Involvement__c = null;
    currentStep = 1;
    update currentRecord;
    return null; 
  }
  
  public PageReference clearSelectedProduct() {
    currentRecord.Selected_Product_to_be_Sold__c = null;
    currentRecord.Selected_Permissible_Purpose__c = null;
    currentRecord.Selected_Third_Party_Involvement__c = null;
    updateAllowedOptions();
    currentStep = 2;
    return null;
  }
  
  public PageReference clearSelectedPurpose() {
    currentRecord.Selected_Permissible_Purpose__c = null;
    currentRecord.Selected_Third_Party_Involvement__c = null;
    updateAllowedOptions();
    currentStep = 3;
    return null;
  }
  
  public PageReference clearThirdParty() {
    currentRecord.Selected_Third_Party_Involvement__c = null;
    currentStep = 4;
    updateAllowedOptions();
    return null;
  }
  
  
  public PageReference moveToStep2() {
    if (String.isNotBlank(currentRecord.Selected_Client_Industry__c)) {
      currentStep = 2;
      updateAllowedOptions();
    }
    return null;
  }

  public PageReference moveToStep3() {
    if (String.isNotBlank(currentRecord.Selected_Product_to_be_Sold__c)) {
      currentStep = 3;
      updateAllowedOptions();
    }
    return null;
  }

  public PageReference moveToStep4() {
    if (String.isNotBlank(currentRecord.Selected_Permissible_Purpose__c)) {
      currentStep = 4;
      updateAllowedOptions();
    }
    return null;
  }
  
  public PageReference loadPage() {
    loadTool();
    if (currentRecord.Onboarding_Bypass__c == false) {
      if (getCheckAllSelected()) {
        buildDocumentList();
      }
      else {
        updateAllowedOptions();
        currentStep = getLastStep();
      }
    }
    return null;
  }
  
  public PageReference enableEdit() {
    editMode = true;
    currentStep = 4;
    updateAllowedOptions();
    return null;
  }
  
  public PageReference bypassEnable() {
    currentRecord.Onboarding_Bypass__c = true;
    update currentRecord;
    return null;
  }
  
  public PageReference bypassDisable() {
    currentRecord.Onboarding_Bypass__c = false;
    update currentRecord;
    return null;
  }
  
  public PageReference saveChanges() {
    stdCon.save();
    stdCon.reset();
    editMode = !getCheckAllSelected();
    if (!editMode) {
      buildDocumentList();
    }
    return null;
  }
  
  public Boolean getCheckAllSelected() {
    system.debug(currentRecord);
    if (String.isNotBlank(currentRecord.Selected_Client_Industry__c) && 
        String.isNotBlank(currentRecord.Selected_Product_to_be_Sold__c) && 
        String.isNotBlank(currentRecord.Selected_Permissible_Purpose__c) && 
        String.isNotBlank(currentRecord.Selected_Third_Party_Involvement__c)) {
      return true;
    }
    return false;
  }
  
  public Integer getLastStep() {
    Integer lastStep = 1;
    if (String.isBlank(currentRecord.Selected_Third_Party_Involvement__c)) {
      lastStep = 4;
    }
    if (String.isBlank(currentRecord.Selected_Permissible_Purpose__c)) {
      lastStep = 3;
    }
    if (String.isBlank(currentRecord.Selected_Product_to_be_Sold__c)) {
      lastStep = 2;
    }
    if (String.isBlank(currentRecord.Selected_Client_Industry__c)) {
      lastStep = 1;
    }
    return lastStep;
  }
  
  public PageReference updateAllowedOptions() {
    
    List<String> whereClause = new List<String>();
    
    if (String.isNotBlank(currentRecord.Selected_Client_Industry__c) || 
        String.isNotBlank(currentRecord.Selected_Product_to_be_Sold__c) || 
        String.isNotBlank(currentRecord.Selected_Permissible_Purpose__c)) {
    
      if (String.isNotBlank(currentRecord.Selected_Client_Industry__c)) {
        String tmpInd = currentRecord.Selected_Client_Industry__c;
        whereClause.add(' Client_Industry__c = :tmpInd ');
      }
      
      if (String.isNotBlank(currentRecord.Selected_Product_to_be_Sold__c)) {
        String tmpProd = currentRecord.Selected_Product_to_be_Sold__c;
        whereClause.add(' Product_to_be_Sold__c = :tmpProd ');
      }
  
      if (String.isNotBlank(currentRecord.Selected_Permissible_Purpose__c)) {
        String tmpPur = currentRecord.Selected_Permissible_Purpose__c;
        whereClause.add(' Permissible_Purpose__c = :tmpPur ');
      }
      
      List<Membership_Onboarding_Allowed_Option__c> allOptions = (List<Membership_Onboarding_Allowed_Option__c>)Database.query(
        'SELECT Id, Client_Industry__c, Permissible_Purpose__c, Product_to_be_Sold__c, Third_Party_Involvement__c ' +
        'FROM Membership_Onboarding_Allowed_Option__c ' + 
        (!whereClause.isEmpty() ? ' WHERE '+String.join(whereClause, ' AND ') : '')
      );
      
      allowedProducts = new Set<String>();
      allowedPermissiblePurposes = new Set<String>();
      allowedThirdParty = new Set<String>();
      
      for (Membership_Onboarding_Allowed_Option__c moao : allOptions) {
        allowedProducts.add(moao.Product_to_be_Sold__c);
        allowedPermissiblePurposes.add(moao.Permissible_Purpose__c);
        allowedThirdParty.add(moao.Third_Party_Involvement__c); 
      }
      
      update currentRecord;
    }
    return null;
  }

  public List<SelectOption> getClientIndustry() {
    return MembershipOnboardingToolUtils.getClientIndustry();
  }
  
  public List<SelectOption> getPermissiblePurpose() {
    return MembershipOnboardingToolUtils.getPermissiblePurpose(allowedPermissiblePurposes, true);
  }
  
  public List<SelectOption> getProductToBeSold() {
    return MembershipOnboardingToolUtils.getProductToBeSold(allowedProducts, true);
  }
  
  public List<SelectOption> getThirdParty() {
    return MembershipOnboardingToolUtils.getThirdParty(allowedThirdParty, true);
  }
  
  //===========================================================================
  // Based on all the selected options, build the documents list and separate
  // into 2 lists, client and supporting.
  //===========================================================================
  public PageReference buildDocumentList() {
    
    List<String> whereClause = new List<String>();
    
    List<Membership_Onboarding_Document__c> docList = new List<Membership_Onboarding_Document__c>();
    
    if (String.isNotBlank(currentRecord.Selected_Client_Industry__c)) {
      String tmpInd = currentRecord.Selected_Client_Industry__c;
      whereClause.add(' (Lookup_Type__c = \'Client Industry\' AND Lookup_Value__c = :tmpInd) ');
    }
    
    if (String.isNotBlank(currentRecord.Selected_Permissible_Purpose__c)) {
      String tmpPur = currentRecord.Selected_Permissible_Purpose__c;
      whereClause.add(' (Lookup_Type__c = \'Permissible Purpose\' AND Lookup_Value__c = :tmpPur) ');
    }
    
    if (String.isNotBlank(currentRecord.Selected_Product_to_be_Sold__c)) {
      String tmpProd = currentRecord.Selected_Product_to_be_Sold__c;
      whereClause.add(' (Lookup_Type__c = \'Product to be Sold\' AND Lookup_Value__c = :tmpProd) ');
    }
      
    if (String.isNotBlank(currentRecord.Selected_Third_Party_Involvement__c)) {
      String tmpTrd = currentRecord.Selected_Third_Party_Involvement__c;
      whereClause.add(' (Lookup_Type__c = \'Third-Party Involvement\' AND Lookup_Value__c = :tmpTrd) ');
    }
    
    if (!whereClause.isEmpty()) {
      String query = 'SELECT Sort_Order__c, Full_Detail__c, Id, Type_of_Document__c, Not_Applicable_Document__c,Document_URL__c FROM Membership_Onboarding_Document__c ' +
        ' WHERE (' + String.join(whereClause, ' OR ') + ') ';
      system.debug(query);
      docList = (List<Membership_Onboarding_Document__c>)Database.query(
        query
      );
      
      clientDocuments = new List<Membership_Onboarding_Document__c>();
      supportingDocuments = new List<Membership_Onboarding_Document__c>();
      
      List<MembershipOnboardingDocumentWrapper> cDocsTemp = new List<MembershipOnboardingDocumentWrapper>();
      List<MembershipOnboardingDocumentWrapper> sDocsTemp = new List<MembershipOnboardingDocumentWrapper>();
      
      for (Membership_Onboarding_Document__c memOnbDoc : docList) {
        if (memOnbDoc.Type_of_Document__c == 'Client Document') {
          cDocsTemp.add(new MembershipOnboardingDocumentWrapper(memOnbDoc));
        }
        else {
          sDocsTemp.add(new MembershipOnboardingDocumentWrapper(memOnbDoc));
        }
        if (String.isBlank(currentRecord.Onboarding_Documents_JSON__c)) {
          docDetails.put(memOnbDoc.Id, new documentDetail(memOnbDoc.Id, memOnbDoc.Not_Applicable_Document__c));
        }
      }
      cDocsTemp.sort();
      sDocsTemp.sort();
      
      for (MembershipOnboardingDocumentWrapper modw : cDocsTemp) {
        clientDocuments.add(modw.m);
      }
      for (MembershipOnboardingDocumentWrapper modw : sDocsTemp) {
        supportingDocuments.add(modw.m);
      }
      
      if (String.isNotBlank(currentRecord.Onboarding_Documents_JSON__c)) {
        try {
          docDetails = (Map<Id, documentDetail>)JSON.deserialize(currentRecord.Onboarding_Documents_JSON__c, Map<Id, documentDetail>.class);
        }
        catch (Exception e) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, 'A problem occurred loading the documents you selected.'));
        }
      }
      cleanUpDocDetailsMap();
    }
    return null;
  }
  
  //===========================================================================
  // There's a possibility that the documents in the loaded JSON don't match,
  // so clear these out just in case. The user can always reselect the options.
  //===========================================================================
  public void cleanUpDocDetailsMap() {
    // Store a copy of all the ids queried and use these later
    Set<Id> actualDocIdSet = new Set<Id>();
    for (Membership_Onboarding_Document__c doc : clientDocuments) {
      if (!docDetails.containsKey(doc.Id)) {
        docDetails.put(doc.Id, new documentDetail(doc.Id, doc.Not_Applicable_Document__c));
      }
      actualDocIdSet.add(doc.Id);
    }
    for (Membership_Onboarding_Document__c doc : supportingDocuments) {
      if (!docDetails.containsKey(doc.Id)) {
        docDetails.put(doc.Id, new documentDetail(doc.Id, doc.Not_Applicable_Document__c));
      }
      actualDocIdSet.add(doc.Id);
    }
    // Now pull out all the Ids in docDetails map, and remove any not found by the queries
    docDetails.keySet().retainAll(actualDocIdSet);
  }
  
  public List<Membership_Onboarding_Document__c> clientDocuments {get{
    if (clientDocuments == null) {
      clientDocuments = new List<Membership_Onboarding_Document__c>();
    }
    return clientDocuments;
  }set;}
  
  public List<Membership_Onboarding_Document__c> supportingDocuments {get{
    if (supportingDocuments == null) {
      supportingDocuments = new List<Membership_Onboarding_Document__c>();
    }
    return supportingDocuments;
  }set;}
  
  //===========================================================================
  // Store the selected documents in a JSON encoded format, and also check all
  // documents have been completed if applicable.
  //===========================================================================
  public PageReference buildJson() {
    
    String jsonString = JSON.serialize(docDetails);
    system.debug(jsonString);
    
    Boolean allFound = true;
    
    for (documentDetail dd : docDetails.values()) {
      // Not done, and applicable, and not on file...
      if (!dd.done && !dd.notAppl && !dd.onFile) {
        allFound = false;
      }
    }
    
    Membership__c memb = new Membership__c(
      Id = currentRecord.Id, 
      Onboarding_Documents_JSON__c = jsonString,
      Onboarding_Documents_Complete__c = allFound
    );
    update memb;
    
    return null;
  }
  
}