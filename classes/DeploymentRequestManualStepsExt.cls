/*=============================================================================
 * Experian
 * Name: DeploymentRequestManualStepsExt
 * Description: Case 01946043
                Return all manual step components for a deployment request.
 * Created Date: 12 Apr 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 
 =============================================================================*/

public with sharing class DeploymentRequestManualStepsExt {
  
  ApexPages.StandardController stdCon;
  Deployment_Request__c currentRecord;
  
  public DeploymentRequestManualStepsExt(ApexPages.StandardController con) {
    stdCon = con;
    currentRecord = (Deployment_Request__c)stdCon.getRecord();
  }
  
  public List<Deployment_Component__c> getDeploymentComponentsManual() {
    List<Deployment_Component__c> caseComps = [
      SELECT Id, Case_Component__r.Id, Case_Component__r.Name, Case_Component__r.Action__c, 
             Case_Component__r.Component_Name__c, Case_Component__r.Component_Name__r.Name, 
             Case_Component__r.Component_Name__r.Component_Type__c, Case_Component__r.Component_Name__r.Object_API_Name__c,
             Case_Component__r.Configuration_Type__c, Case_Component__r.Deployment_Type__c, 
             Case_Component__r.Manual_Step_Details__c, Completed__c, Case_Component__r.Case_Number__c,
             Case_Component__r.Story__c, Case_Component__r.User_Story_AA__c
      FROM Deployment_Component__c
      WHERE Slot__c = :currentRecord.Id
      AND Case_Component__r.Deployment_Type__c = 'Manual Step'
      ORDER BY Case_Component__r.Configuration_Type__c DESC NULLS FIRST
    ];
    
    return caseComps;
  }
  
}