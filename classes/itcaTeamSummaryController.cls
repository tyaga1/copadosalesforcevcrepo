/**=====================================================================
 * Name: itcaTeamSUmmaryController
 * Description: 
 * Created Date: July 25th, 2017
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * July 25th, 2017       James Wills            ITCA:W-008961 Created.
 * Aug. 8th, 2017        James Wills            ITCA:W-009320 - Added Date Discussed for display.
 * Aug. 10th, 2017       James Wills            ITCA:W-009246 - Updated getProfileLevel() to allow use outside of Class.
 * =====================================================================*/
public class itcaTeamSummaryController{

  public class itcaTeamUser{
    public ID       currentID                     {get;set;}
    public ID       futureID                      {get;set;}
    public ID       UserID                        {get;set;}
    public String   name                          {get;set;}
    public String   title                         {get;set;}
    public String   userProfileCurrentLevelString {get;set;}
    public Decimal  userProfileCurrentLevel       {get;set;}
    public Datetime dateCurrentCreated            {get;set;}
    public Datetime dateCurrentLastUpdated        {get;set;}
    public Datetime dateCurrentDiscussed          {get;set;}
    public String   currentProfileStatus          {get;set;}
    public String   userProfileFutureLevelString  {get;set;}
    public Decimal  userProfileFutureLevel        {get;set;}
    public Datetime dateFutureCreated             {get;set;}
    public Datetime dateFutureLastUpdated         {get;set;}
    public String   futureProfileStatus           {get;set;}
    public Datetime dateFutureDiscussed           {get;set;}
  }
  
  public ITCABannerInfoClass bannerInfoLocal {get;set;}
  
  
  public Map<ID,ProfileSkill> skillMaxLevels_Map = new Map<ID, ProfileSkill>();
  
  public List<itcaTeamUser> teamList {get{
    List<itcaTeamUser> teamList = new List<itcaTeamUser>();     
    Map<ID,itcaTeamUser> teamMap = new Map<ID,itcaTeamUser>();    
    
    Map<ID,User> team_Map = new Map<ID,User>([SELECT id, Name, Title FROM User WHERE ManagerId = :UserInfo.getUserId()]);
    
    Map<ID, Career_Architecture_User_Profile__c> currentUserProfile_Map = new Map<ID, Career_Architecture_User_Profile__c>
                                                                         ([SELECT id, CreatedDate, LastModifiedDate, Employee__c, Employee__r.Name, Employee__r.Title, Role__c, State__c, 
                                                                          Status__c, Date_Discussed_with_Employee__c,
                                                                          (SELECT id,  Skill__c, Skill__r.id, Skill__r.name, Level__c FROM Career_Architecture_Skills_Plans__r)
                                                                          FROM Career_Architecture_User_Profile__c WHERE (State__c = 'Current' OR State__c = 'Future Plan') AND Employee__c IN :team_Map.values()]);
        
    Map<ID, ProfileSkill> profSkills_Map = new Map<ID, ProfileSkill>([SELECT id, Name, Max_Skill_Level__c FROM ProfileSkill]);
    
    skillMaxLevels_Map = new Map<ID, ProfileSkill>([SELECT id, Name, Max_Skill_Level__c FROM ProfileSkill]);
    
    
    for(Career_Architecture_User_Profile__c caup : currentUserProfile_Map.values()){      
      itcaTeamUser newUser = new itcaTeamUser();
      
      if(teamMap.containsKey(caup.Employee__r.id)){
        newUser=teamMap.get(caup.Employee__r.id);
      } else {
        newUser.userID = caup.Employee__r.id;
        newUser.name   = caup.Employee__r.Name; 
        newUser.title  = caup.Employee__r.Title;
      }
      if(caup.State__c == 'Current'){
        newUser.currentID               = caup.id;
        newUser.dateCurrentCreated      = caup.CreatedDate;
        newUser.dateCurrentLastUpdated  = caup.LastModifiedDate;
        newUser.dateCurrentDiscussed    = caup.Date_Discussed_with_Employee__c;
        newUser.currentProfileStatus    = caup.Status__c;
        newUser.userProfileCurrentLevelString = getProfileLevel(caup.Career_Architecture_Skills_Plans__r, skillMaxLevels_Map);
        newUser.userProfileCurrentLevel = getProfileLevelPercent(getProfileLevel(caup.Career_Architecture_Skills_Plans__r, skillMaxLevels_Map));
      } else if (caup.State__c == 'Future Plan'){
        newUser.futureID                = caup.id;
        newUser.dateFutureCreated       = caup.CreatedDate;
        newUser.dateFutureLastUpdated   = caup.LastModifiedDate;
        newUser.dateFutureDiscussed     = caup.Date_Discussed_with_Employee__c;
        newUser.futureProfileStatus     = caup.Status__c;
        newUser.userProfileFutureLevelString  = getProfileLevel(caup.Career_Architecture_Skills_Plans__r, skillMaxLevels_Map);  
        newUser.userProfileFutureLevel  = getProfileLevelPercent(getProfileLevel(caup.Career_Architecture_Skills_Plans__r, skillMaxLevels_Map));
      }
      
      teamMap.put(newUser.userId,newUser);

    }    
    teamList=teamMap.values();
    
    return teamList;
    
    }
    set;
  }

  
  //////////////////////////////////////////////////////////////////////////////
  public itcaTeamSummaryController(ApexPages.StandardController controller) {
    
    bannerInfoLocal = new ITCABannerInfoClass();
    bannerInfoLocal.currentActiveTab = 'isSummary';
    bannerInfoLocal.employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();
    bannerInfoLocal.currentUserSkillLevelMap = new Map<String,Decimal>();
   
  }
  
  public static String getProfileLevel(List<Career_Architecture_Skills_Plan__c> caSkillsPlans_List, Map<ID,ProfileSkill> skillMaxLevels_Map){
    Map<ID, List<Integer>> convertedUserDetailsBySkill_Map = new Map<ID, List<Integer>>();
    
    Map<ID,List<String>> userDetailsBySkill_Map = new Map<ID,List<String>>();      
    for(Career_Architecture_Skills_Plan__c casp : caSkillsPlans_List){     
      if(userDetailsBySkill_Map.containsKey(casp.Skill__c)){
        List<String> levelList = userDetailsBySkill_Map.get(casp.Skill__c);
        levelList.add(casp.Level__c);
        userDetailsBySkill_Map.put(casp.Skill__c, levelList);
      } else{
        List<String> levelList = new List<String>();
        levelList.add(casp.Level__c);
        userDetailsBySkill_Map.put(casp.Skill__c, levelList);
      }
    }    
    
    for(ID ud : userDetailsBySkill_Map.keySet()){
      for(String level : userDetailsBySkill_Map.get(ud)){
        Integer intToAdd=1;//This is to cover Skills where none of the Level fields have a value.
        if(level=='Level1'){
          intToAdd=1;
        } else if(level=='Level2'){
          intToAdd=2;
        } else if(level=='Level3'){
          intToAdd=3;
        } else if(level=='Level4'){
          intToAdd=4;
        } else if(level=='Level5'){
          intToAdd=5;
        } else if(level=='Level6'){
          intToAdd=6;
        } else if(level=='Level7'){
          intToAdd=7;
        }
        if(convertedUserDetailsBySkill_Map.containsKey(ud)){
          List<Integer> levelList = convertedUserDetailsBySkill_Map.get(ud);
          levelList.add(intToAdd);
          convertedUserDetailsBySkill_Map.put(ud, levelList);
        } else {       
          List<Integer> levelList = new List<Integer>();
          levelList.add(intToAdd);
          convertedUserDetailsBySkill_Map.put(ud, levelList);
        }
      }    
    }
    Integer current  = 0;
    Integer maxLevel = 0;
    for(ID skillId : convertedUserDetailsBySkill_Map.keySet()){
      List<Integer> intListForSkill = convertedUserDetailsBySkill_Map.get(skillId);
      Integer maxForSkill =1;
      if(skillMaxLevels_Map.containsKey(skillId)){
        maxForSkill = (Integer)skillMaxLevels_Map.get(skillId).Max_Skill_Level__c;
      }
      for(Integer level : intListForSkill){
        current  += level;
        maxLevel += maxForSkill;  
      }      
    }        
    
    return current + '/' + maxLevel;

  }
  
  public Decimal getProfileLevelPercent(String level){
     
    Decimal current  = Decimal.valueOf(Integer.valueOf(level.substring(0,level.indexOf('/'))));
    Decimal maxLevel = Decimal.valueOf(Integer.valueOf(level.substring(level.indexOf('/')+1,level.length())));
    
    Decimal percentValue = (current/maxLevel)*100;
      
    return percentValue;  
    
  }

    
}