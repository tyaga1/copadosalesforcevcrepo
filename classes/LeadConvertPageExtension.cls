/**=====================================================================
 * Experian
 * Name: LeadConvertPageExtension
 * Description: Override of the Lead Convert screen to allow Primary Campaign Attribution
 *              Case #580226
 * Created Date: May 20th, 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By                  Description of the update
 * May 20th, 2015     Paul Kissick                 Created 
 * Jul 7th, 2015      Paul Kissick                 Case #00587593, Case #00587589, Case #00587592 - Adding abort field for timed workflow
 * Oct 8th, 2015      Paul Kissick                 I-184302: Changing default if no campaigns to untick 'do not create opp', so default is to create opp
 * Feb 24th, 2016     Paul Kissick                 Case 01816977: Adding checks that user can edit before lead is converted.
 * Jul 18th, 2016     Manoj Gopu(QA)               CRM2.0W-005436 Updating the decision maker selected from lead conversion page
 * Aug 18th, 2016     Manoj Gopu                   CRM2:W-005586 Checking for Primary campaign upon lead conversion
 * Oct 04th, 2016     Manoj Gopu                   CRM2:W-005950 Auto tick campaign checkbox on Lead conversion when only one campaign associated                                                               
 * Oct 11th, 2016     Manoj Gopu                   Case:01143572: Adding a check on Opp Mandatory  fields upon lead conversion.
 * Oct 14th, 2016     Paul Kissick                 Cleaned up the code.
  =====================================================================*/

public with sharing class LeadConvertPageExtension {
  
  ApexPages.StandardController stdCon;
  
  Lead currentLead;
  public Lead objLead{get;set;}//Added by Manoj
  
  public Boolean canConvert {get{ if (canConvert == null) canConvert = true; return canConvert;}set;}
  
  @TestVisible Integer limitSteps = 5; // change this to increase the steps per 'more' call
  
  @TestVisible Integer currentLimit = 5;
  
  public Boolean doNotCreateOpp {get{if(doNotCreateOpp == null) doNotCreateOpp = false; return doNotCreateOpp;}set;}
  public Boolean doNotAssignCamp {get{if(doNotAssignCamp == null) doNotAssignCamp = false; return doNotAssignCamp;}set;}
  
  @TestVisible String getUrlNoOpty() {
    return '/lead/leadconvert.jsp?retURL='+currentLead.Id+'&id='+currentLead.Id+'&nooppti=1';
  }
  @TestVisible String getUrlYesOpty() {
    return '/lead/leadconvert.jsp?retURL='+currentLead.Id+'&id='+currentLead.Id+'&nooppti=0';
  }
  
  public class campaignWrapper {
    public Boolean selected {get;set;}
    public Campaign camp {get;set;}
    public campaignWrapper(Campaign c, Boolean sel) {
      camp = c;
      selected = sel;
    }
  }

  public List<campaignWrapper> allCampaigns {get{
    if (allCampaigns == null) {
      allCampaigns = new list<campaignWrapper>();
    }
    return allCampaigns;
  }set;}
  
  public LeadConvertPageExtension(ApexPages.StandardController c) {
    stdCon = c;
    objLead = new Lead(); //Added by Manoj
    objLead.Decision_Maker__c = false; //Added by Manoj
    if (!Test.isRunningTest()) {
      stdCon.addFields(new List<String>{'Assigned_Campaign__c', 'IsConverted', 'Abort_Timed_Workflow__c'});
    }
    currentLead = (Lead)stdCon.getRecord();
  }
  
  //===========================================================================
  // Load the list of the most recent campaigns
  //===========================================================================
  public PageReference prepareCampaigns() {
    
    allCampaigns.clear();
    // Also check the lead isn't already converted...
    if (currentLead.IsConverted) {
      return new PageReference('/'+currentLead.Id);
    }
    // PK: Case #00587593, Case #00587589, Case #00587592 Adding to abort time based workflow when the conversion page is loaded.
    try {
      currentLead.Abort_Timed_Workflow__c = true;
      update currentLead;
    }
    catch (DMLException ex) {
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        if (ex.getDmlType(i) == StatusCode.INSUFFICIENT_ACCESS_OR_READONLY) {
          canConvert = false;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.LeadConvert_No_Access));
        }
        else {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Generic_message_for_system_error + '-'+ex.getDmlMessage(i)));
        }
      }
      return null;
    }
    catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Generic_message_for_system_error + '-'+e.getMessage()));
    }
    // ---
    currentLimit = (currentLimit > 2000) ? 2000 : currentLimit;
    // Moved the query here to check the list size in for loop
    List<CampaignMember> campMemberList = [
      SELECT Campaign.Id, Campaign.Name, Campaign.StartDate, Campaign.EndDate, Campaign.Type, Campaign.Status 
      FROM CampaignMember 
      WHERE LeadId = :currentLead.Id
      ORDER BY CreatedDate DESC
      LIMIT :currentLimit
    ];
    for (CampaignMember cm : campMemberList) {
      //check the campMemberList size, if there is only one campaign then default it is checked to true
      allCampaigns.add(
        new campaignWrapper(
          cm.Campaign, 
          (cm.Campaign.Id == currentLead.Assigned_Campaign__c || campMemberList.size() == 1)
        )
      );
    }
    
    if (allCampaigns.isEmpty()) {
      // redirect to normal lead convert page as there are no campaigns associated
      //return new PageReference(getUrlYesOpty());    // changing from getUrlNoOpty to getUrlYesOpty
    }
    return null;
    
  }
  
  //===========================================================================
  // Dummy method to force refresh
  //===========================================================================
  public PageReference skipOpp() {
    return null;
  }
  
  //===========================================================================
  // Clicking the show more button should increase the number of available campaigns to show.
  //===========================================================================
  public PageReference showMoreCampaigns() {
    currentLimit += limitSteps;
    return prepareCampaigns();
  }
  
  //===========================================================================
  // Validate the checkboxes on every selection
  //===========================================================================
  public PageReference checkTicks() {
    // only 1 should be ticked, so keep the first one ticked...
    Boolean foundTick = false;
    for(campaignWrapper c : allCampaigns) {
      if (foundTick && c.selected) {
        c.selected = false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,System.Label.LeadConvert_Only_One));
      }
      if (c.selected) foundTick = true;
    }
    return null;
  }
  
  
  //===========================================================================
  // Save the changes and redirect to the convert page
  //===========================================================================
  public PageReference save() {
    // check the ticks...
    if (doNotCreateOpp) {
      // redirect to normal page 
      return new PageReference(getUrlNoOpty());
    }
    else {
      // check the ticks...
      Boolean foundTick = false;
      Boolean errorFound = false;
      Id campId = null;
      for (campaignWrapper c : allCampaigns) {
        if (foundTick && c.selected) {
          c.selected = false;
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,System.Label.LeadConvert_Only_One));
          errorFound = true;
        }
        if (c.selected) {
          foundTick = true;
          campId = c.camp.Id;
        }
      }
      if (campId == null && !doNotAssignCamp && !allCampaigns.isEmpty()) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,System.Label.LeadConvert_PleaseSelectOne));
        errorFound = true;
      }
      //Adding a check on Opp Mandatory Fields upon lead conversion- Added by Manoj
      Lead objLeadOpp = [
        SELECT Id, Target_Close_Date__c, TCV__c, Budget__c, Capability__c
        FROM Lead
        WHERE Id = :currentLead.Id
        LIMIT 1
      ];
      if (objLeadOpp.Target_Close_Date__c == null || 
          (objLeadOpp.TCV__c == null || objLeadOpp.TCV__c == 0.00) || 
          String.isBlank(objLeadOpp.Budget__c) || 
          String.isBlank(objLeadOpp.Capability__c)) {      
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,system.Label.Lead_Convert_Opp_Mandatory_Fields));
        errorFound = true;
      }
      if (!errorFound) {
        // proceed to set the source on the lead and to the next page...
        currentLead.Assigned_Campaign__c = (doNotAssignCamp) ? null : campId;
        currentLead.Update_Campaign__c = true;
        currentLead.Decision_Maker__c = objLead.Decision_Maker__c;//Added by Manoj
        stdCon.save();
        return new PageReference(getUrlYesOpty());
      }
    }
    return null;
  }
  
  //===========================================================================
  // Return to the Lead view page
  //===========================================================================
  public PageReference cancel() {
    return stdCon.view();
  }
  
}