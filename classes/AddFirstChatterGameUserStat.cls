/**=====================================================================
 * Appirio, Inc
 * Name: AddFirstChatterGameUserStat
 * Description: The following batch class is designed to be scheduled to run once every Sunday.
                    This class will get all new Chatter user Stats from an analytic snapshot and assign a Chatter game to them
 * Created Date: 5/5/2015
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 * Apr 7th, 2016                Paul Kissick                 Fixed failing tests - Replaced finish method content
 =====================================================================*/
global class AddFirstChatterGameUserStat implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
  
    String query = 'Select Id, User__c, Chatter_Game__c, User_Region__c, User_Function__c, User_Business_Line__c, X4th_Priority__c FROM User_Chatter_stats__c WHERE This_week_Stat__c = true';
   
    return Database.getQueryLocator (query);
  }

  global void execute (Database.BatchableContext bc, List<User_Chatter_stats__c> scope) {

    List<User_Chatter_stats__c> neochatterUserStatList = [Select Id,First_Chatter_Stat_for_Game__c, Chatter_Game_ID__c  FROM User_Chatter_stats__c WHERE Chatter_Game__c != '' AND First_Chatter_Stat_for_Game__c = '' AND Id in :scope];
        
    //Set off all User Chatter Stat associated games
    
    Set<String> neoStatId = new Set<String>();
    
    for (User_Chatter_stats__c neoUserChatterStat : neochatterUserStatList) {
    if (neoUserChatterStat.Chatter_Game_ID__c != null) {
      neoStatId.add(neoUserChatterStat.Chatter_Game_ID__c);}
    }
    system.debug('Diego neoStatId:'+neoStatId);
    //List of all First Chatter User Stat for the game via Chatter Game Id
    
    List<User_Chatter_stats__c> firstUserChatterStat = [Select Id, Game_Fist_Stat__c, Chatter_Game_ID__c FROM User_Chatter_stats__c WHERE  Game_Fist_Stat__c = true AND Chatter_Game_ID__c in :neoStatId];
    
    //Map to search previous stats
    
    Map<String, User_Chatter_stats__c> fucsToUse = new Map<String, User_Chatter_stats__c>();
    
    for (User_Chatter_stats__c fucs : firstUserChatterStat) {
      fucsToUse.put(fucs.Chatter_Game_ID__c, fucs);
    }
    
    system.debug('Diego fucsToUse:'+fucsToUse);
    //List of records to update
    
    List<User_Chatter_stats__c> lastNeoUserStatListtoUpdate = new List<User_Chatter_stats__c>{};
    
    //Set the Previous Chatter User Stat
    
    if (!fucsToUse.isEmpty()) {
      for (User_Chatter_stats__c neoUserChatterStat : neochatterUserStatList){
        if (neoUserChatterStat.Chatter_Game_ID__c != null) {
          User_Chatter_stats__c fcgid = fucsToUse.get(neoUserChatterStat.Chatter_Game_ID__c);
          system.debug('Diego fcgid:'+fcgid);
          if (fcgid != null) {
            neoUserChatterStat.First_Chatter_Stat_for_Game__c = fcgid.Id;
            lastNeoUserStatListtoUpdate.add(neoUserChatterStat);
          }
        }
      }
    }

    update lastNeoUserStatListtoUpdate;            
  }  

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'AddFirstChatterGameUserStat', true);
    
    if (!Test.isRunningTest()) {
      system.scheduleBatch(new AddPreviousChatterUserStat(), 'UserChatterStat - Assign Previous User Stat '+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('AddPreviousChatterUserStat'));
    }
    
    /*
    // PK Removed this and replaced with above as it's much easier to manage.
    BatchSchedulingIDstorage__c BSIDS = BatchSchedulingIDstorage__c.getOrgDefaults();
    
    if (!Test.isRunningTest()) {
    
    System.abortJob(BSIDS.BSIDS04__c );
    
    }
    
    DateTime n = datetime.now().addMinutes(2);
    String cron = '';

        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();

        BSIDS.BSIDS05__c = System.schedule('UserChatterStat - Assign Previous User Stat', cron, new ScheduleAddPreviousChatterUserStat());

        update BSIDS;    
    
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                               TotalJobItems, CreatedBy.Email
                        FROM AsyncApexJob WHERE Id =: BC.getJobId()];
      
      System.debug('\n[AddFirstChatterGameUserStat: finish]: [The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.]]');
    */
  }
    
}