/**=====================================================================
 * Appirio, Inc
 * Name: BatchAccountSegmentationSetUp
 * Description: Batch job for go-live
 * Created Date: Apr 3rd, 2015
 * Created By: Noopur Sundariyal
 *
 * Date Modified                Modified By                  Description of the update
 * Apr 14th, 2015               Naresh Kumar Ojha            T-375000: Updated finish method to send mail to GCS team for error.
 * Apr 28th, 2015               Arpita Bose                  Updated the query of Opportunity to fix test class failure
 * May 11th, 2015               Naresh Kumar Ojha            I-162122: Modified batch class to have Stageful interface.
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Dec 3rd, 2015                Paul Kissick                 Case 01266075: Replacing Global_Settings__c use for timings
 * Aug 3rd, 2016                Paul Kissick                 CRM2:W-005332: Optimising this a bit, same query used in multiple places!
                                                             Also added new option to just recalc existing segments - for new fields.
 =====================================================================*/
global class BatchAccountSegmentationSetUp implements  Database.Batchable<sObject>, Database.Stateful {

  global Boolean runAccountSegmentCreationAndPopulationFromOpp;
  global Boolean runAccountSegmentCreationAndPopulationFromOrder;
  global Boolean runAccountSegmentRecalculation;
  
  global Set<String> failedOrderRecrdIDs, failedOpptyRecrdIDs, failedAccountSegmentRdcrdIDs;
   
  private static Set<String> fieldNameSet = new Set<String> {
    'Segment_Global_Business_Line__c',
    'Segment_Business_Line__c',
    'Segment_Business_Unit__c',
    'Segment_Region__c',
    'Segment_Country__c'
  };

  global BatchAccountSegmentationSetUp(Boolean runOpps, Boolean runOrders, Boolean runSegs) {
    this.runAccountSegmentCreationAndPopulationFromOpp   = runOpps;
    this.runAccountSegmentCreationAndPopulationFromOrder = runOrders;
    this.runAccountSegmentRecalculation                  = runSegs;
  }

  //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC) {
    // This will create Account Segments given existing Opp's Segment* fields
    String qry = '';
    if (runAccountSegmentCreationAndPopulationFromOpp) {
      qry = 'SELECT Id, Name, AccountId, OwnerId, IsClosed, IsWon,' +
            ' Segment_Business_Line__c, Segment_Business_Unit__c, Segment_Country__c,' +
            ' Segment_Global_Business_Line__c, Segment_Region__c, ' +
            ' Owner_GBL_on_Opp_Close_Date__c, Owner_BL_on_Opp_Close_Date__c,' +
            ' Owner_BU_on_Opp_Close_Date__c, Owner_Region_on_Opp_Close_Date__c,' +
            ' Owner_Country_on_Opp_Close_Date__c' +
            ' FROM Opportunity ' +
            ' ORDER BY Account.Name';
                   
      
    }
    else if (runAccountSegmentCreationAndPopulationFromOrder) {
      qry = 'SELECT Id, Name, Account__c, OwnerId, RecordTypeId, ' +
            ' Segment_Business_Line__c, Segment_Business_Unit__c, Segment_Country__c,' +
            ' Segment_Global_Business_Line__c, Segment_Region__c, ' +
            ' Owner_GBL_on_Order_Create_Date__c, Owner_BL_on_Order_Create_Date__c,' +
            ' Owner_BU_on_Order_Create_Date__c, Owner_Region_on_Order_Create_Date__c,' +
            ' Owner_Country_on_Order_Create_Date__c' +
            ' FROM Order__c ' +
            ' ORDER BY Account__r.Name';
    }
    else if (runAccountSegmentRecalculation) {
      qry = 'SELECT Id FROM Account_Segment__c';
    }
    if (Test.isRunningTest()) {
      qry += ' limit 200';
    }
    return Database.getQueryLocator(qry);
  }

  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
    Set<String> segmentIds = new Set<String>();
    failedOrderRecrdIDs = new Set<String>();
    failedOpptyRecrdIDs = new Set<String>();
    
    failedAccountSegmentRdcrdIDs = new Set<String>();
    
    if (runAccountSegmentCreationAndPopulationFromOrder) {
      List<Order__c> orders = new List<Order__c>();
      orders.addAll((List<Order__c>) scope);
      OrderTrigger_AccountSegmentation.populateSegmentsOnOrders(orders, null);
      OrderTrigger_AccountSegmentation.hasRunBatch = true;
      
      List<Database.SaveResult> updatedOrdersResults = Database.update(orders, false);
      Set<String> succeededOrdRecrds = new Set<String>();
      
      system.debug('===orders1.0=='+orders);
      system.debug('===updatedOrdersResults=='+updatedOrdersResults);

      for (Integer i = 0; i < updatedOrdersResults.size(); i++) {

        if (updatedOrdersResults.get(i).isSuccess()) {
          succeededOrdRecrds.add(updatedOrdersResults.get(i).getId());
        }
        else if (!updatedOrdersResults.get(i).isSuccess()) {
          // DML operation failed
          Database.Error error = updatedOrdersResults.get(i).getErrors().get(0);
          String failedDML = error.getMessage();
          system.debug('Failed ID'+orders.get(i).Id);
          String failedStr = 'Id: '+orders.get(i).Id+'\nName: '+orders.get(i).Name+'\nError: ['+failedDML+']';
          failedOrderRecrdIDs.add(failedStr);
        }
      }
      system.debug('===failedOrderRecrdIDs.0=='+failedOrderRecrdIDs);
      for (Order__c ordr: orders) {
        if (succeededOrdRecrds.contains(ordr.ID)) { //If order has been updated
          for (String fieldName: fieldNameSet) {
            if (ordr.get(fieldName) != null) {
              segmentIds.add((String) ordr.get(fieldName));
            }
          }
        }
      }
    }
    
    if (runAccountSegmentCreationAndPopulationFromOpp) {
      // Set <Id> owners = new Set<Id>();
      // map<Id,User> usersMap = new map<Id,User>();
      List<Opportunity> opportunities = new List<Opportunity>();
      opportunities.addAll((List<Opportunity>) scope);

      OpportunityTrigger_AccountSegmentation.populateSegmentsOnOppty(opportunities, null);
      OpportunityTrigger_AccountSegmentation.hasRunBatch = true;
      List<Database.SaveResult> updatedOpptyResults = Database.update(opportunities, false);
      Set<String> succeededOpptyRecrds = new Set<String>();
      
      for (Integer i = 0; i < updatedOpptyResults.size(); i++) {

        if (updatedOpptyResults.get(i).isSuccess()) {
          succeededOpptyRecrds.add(updatedOpptyResults.get(i).getId());
        }
        else if (!updatedOpptyResults.get(i).isSuccess()) {
          // DML operation failed
          Database.Error error = updatedOpptyResults.get(i).getErrors().get(0);
          String failedDML = error.getMessage();
          system.debug('Failed ID'+opportunities.get(i).Id);
          String failedStr = 'Id:'+opportunities.get(i).Id+'\nName:'+opportunities.get(i).Name+'\nError: ['+failedDML+']';
          failedOpptyRecrdIDs.add(failedStr);
        }
      }
      
      for (Opportunity opp: opportunities) {
        if (succeededOpptyRecrds.contains(opp.ID)) {
          for (String fieldName: fieldNameSet) {
            if (opp.get(fieldName) != null) {
              segmentIds.add((String) opp.get(fieldName));
            }
          }
        }
      }
    }
    
    if (runAccountSegmentRecalculation) {
      for (Account_Segment__c seg : (List<Account_Segment__c>)scope) {
        segmentIds.add(seg.Id);
      }
    }
    
    // No need for further processing if IsDataAdmin is turned off, as the trigger will take care
    // of recalculating the Account Segments
    if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false && runAccountSegmentRecalculation == false) {
      return;
    }

    // If IsDataAdmin is turned on, we need to 'manually' have the Account Segments rollups calculated
    // Given the collected Account Segment Ids, launch the recalculation of the amounts
    Map<Id, Account_Segment__c> accountSegmentMap = AccountSegmentationUtility.getAccountSegmentsByIds(segmentIds);

    List<Account_Segment__c> updatedSegments = AccountSegmentationUtility.segmentRecalculation(accountSegmentMap);
    if (updatedSegments != null && !updatedSegments.isEmpty()) {
      List<Database.Saveresult> segmentResults = Database.update(updatedSegments, false);
      for (Integer i = 0; i < segmentResults.size(); i++) {
        if (!segmentResults.get(i).isSuccess()) {
          Database.Error error = segmentResults.get(i).getErrors().get(0);
          String failedDML = error.getMessage();
          String failedStr = 'Id: ' + updatedSegments.get(i).Id + '\nName: ' + updatedSegments.get(i).Name + 'Error: ' + failedDML;
          failedAccountSegmentRdcrdIDs.add(failedStr);
        }
      }
      system.debug('====updatedSegments>>>' +updatedSegments);
    }
  }

  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchAccountSegmentationSetUp', false);
    
    system.debug('=Finish:==failedOrderRecrdIDs=='+failedOrderRecrdIDs);
    //System.debug('=Finish:==failedAccountSegmentRdcrdIDs=='+failedAccountSegmentRdcrdIDs);
    Integer numOfError = 0;
    
    if (failedOrderRecrdIDs != null && failedOpptyRecrdIDs != null && failedAccountSegmentRdcrdIDs != null) {
       numOfError = failedOrderRecrdIDs.size() + failedOpptyRecrdIDs.size() + failedAccountSegmentRdcrdIDs.size();
       bh.batchHasErrors = true;
    }
    String emailBody = '';
    
    if (failedOpptyRecrdIDs != null && failedOpptyRecrdIDs.size() > 0) {
      emailBody += '*** Opportunities failed on update:\n\n';
      for (String currentRow : failedOpptyRecrdIDs) {
        emailBody += currentRow+'\n\n';
      }
      emailBody += '\n\n---------------\n\n';
    }
    
    if (failedOrderRecrdIDs != null && failedOrderRecrdIDs.size() > 0) {
      emailBody += '*** Orders failed on update: \n\n';
      for (String currentRow : failedOrderRecrdIDs) {
        emailBody += currentRow+'\n\n';
      }
      emailBody += '\n\n---------------\n\n';
    }

    if (failedAccountSegmentRdcrdIDs != null && failedAccountSegmentRdcrdIDs.size() > 0) {
      emailBody += '*** Account Segments failed on update: \n\n';
      for (String currentRow : failedAccountSegmentRdcrdIDs) {
        emailBody += currentRow + '\n\n';
      }
      emailBody += '\n\n---------------\n\n';
    }
    
    bh.emailBody += emailBody;
    
    bh.sendEmail();
  }

}