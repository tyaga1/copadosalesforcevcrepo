/******************************************************************************
 * Appirio, Inc
 * Name: BatchAccountSegmentUserUpdate.cls
 * Description: 
 *
 * Created Date: Sept 4th, 2015.
 * Created By: Arpita Bose (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Sept 9th, 2015               Noopur                       Added the method to create Account 
                                                             Segment for Users where the UserReOrgProcessed checkbox is true.
 * Sep 15th, 2015               Noopur                       I-180201 - made changes to the code to resolve issue                                                             
 ******************************************************************************/
global class BatchAccountSegmentUserUpdate implements  Database.Batchable<sObject>, Database.Stateful {
  
   //private static Integer level = 3;
   global Set<Id> userIdsToUpdate ;
   global Set<String> failedAccountSegmentRdcrdIDs;
   
  //===========================================================================
  // start method
  //===========================================================================
  global Database.QueryLocator start(Database.BatchableContext BC){
    // get all User records
    userIdsToUpdate = new set<Id>();
    String qryString = 'SELECT Id, Global_Business_Line__c, Business_Line__c, Business_Unit__c, Region__c, Country__c,UserReOrgProcessed__c FROM User where UserReOrgProcessed__c = true' ;
    system.debug('----'+[SELECT Id, Global_Business_Line__c, Business_Line__c, Business_Unit__c, Region__c, Country__c,UserReOrgProcessed__c FROM User where UserReOrgProcessed__c = true]);
    return Database.getQueryLocator(qryString);
  }
  
  //===========================================================================
  // execute method
  //===========================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
    Set<String> updatedFieldValues = new Set<String>();
    Set<String> userFieldNameSet = new Set<String>{'Business_Line__c', 'Business_Unit__c',
                                                     'Global_Business_Line__c','Region__c', 'Country__c'};
    set<String> existingAccountSegs = new set<String>();
    Set<String> segmentValues = new Set<String> ();
    set<Id> userIds = new set<Id>();
   // set<Id> accountIds = new set<Id>();
    
    map<Id,User> usermap = new map<Id,User>(); 
    
    for (User usr : (List<User>)scope) {
        userIds.add(usr.Id);
        usermap.put(usr.Id,usr);
        userIdsToUpdate.add(usr.Id);
        if( !String.isBlank(usr.Business_Unit__c) ) {
          segmentValues.add(usr.Business_Unit__c);
        }
        if( !String.isBlank(usr.Business_Line__c) ) {
        	segmentValues.add(usr.Business_Line__c);
        }
        if( !String.isBlank(usr.Region__c) ) {
        	segmentValues.add(usr.Region__c);
        }
        if( !String.isBlank(usr.Country__c) ) {
        	segmentValues.add(usr.Country__c);
        }
        if( !String.isBlank(usr.Global_Business_Line__c) ) {
        	segmentValues.add(usr.Global_Business_Line__c);
        }
    }
    system.debug('====usermap===='+usermap);
    createAccountSegmentRecordforUser(segmentValues,usermap,userFieldNameSet);
  }
  
  //===========================================================================
  // finish method
  //===========================================================================
  global void finish(Database.BatchableContext BC) {
    
    List<User> usersToUpdate = new List<User>();
    if( !userIdsToUpdate.isEmpty() ) {
        for ( User usr : [SELECT Id,UserReOrgProcessed__c 
                          FROM User
                          WHERE Id IN :userIdsToUpdate]) {
            usr.UserReOrgProcessed__c = false;
            usersToUpdate.add(usr);
        }
    }
    if ( !usersToUpdate.isEmpty() ) {
        update usersToUpdate;
    }
  }
  
  public static void createAccountSegmentRecordforUser(Set<String> segmentValues, Map<Id,User> newUsers, Set<String> userFieldNameSet) {
    Map<String,Hierarchy__c> hierarchyMap = new Map<String,Hierarchy__c>();
    Map<String, Account_Segment__c> accountSegmentMap = new Map<String, Account_Segment__c>();
    
    set<Id> accIds = new set<Id>();
    List<Account_Segment__c> newAccountSegments = new List<Account_Segment__c>();
    List<Opportunity> lstOppty = new List<Opportunity>();
    List<Order__c> orderList = new List<Order__c>();
    AccountSegmentationUtility.level = 3;
    AccountSegmentationUtility.accountSegmentMap = new map<String,Account_Segment__c>();
    AccountSegmentationUtility.accountMap = new Map<Id, String> ();
    AccountSegmentationUtility.hierarchyMap = new Map<String,Hierarchy__c> ();

    // Store Opp, Hierarchy and Account Segment in structures
    String accountSegmentMapString = '';
    String hierarchyMapString = '';
    system.debug('---segmentValues----'+segmentValues);
    for (Opportunity opp : [SELECT Id, OwnerId, Account.Id, Account.Name, IsClosed,
                                   Owner.Global_Business_Line__c, Owner.Business_Line__c,
                                   Owner.Business_Unit__c, Owner.Region__c, Owner.Country__c
                            FROM Opportunity
                            WHERE OwnerId in : newUsers.keySet()
                            AND isClosed = false]) {
      system.debug('===Opp.AccountId==='+opp.AccountId);                        
      AccountSegmentationUtility.accountMap.put(opp.AccountId, opp.Account.Name);
      lstOppty.add(opp);
      accIds.add(opp.AccountId);
    }
    
    system.debug('--AccountSegmentationUtility.accountMap---'+AccountSegmentationUtility.accountMap);
    system.debug('---lstOppty---'+lstOppty);
    
    for (Order__c ordr : [SELECT Id, OwnerId, Account__r.Id, Account__r.Name, Account__c, RecordType.Name
                            FROM Order__c
                            WHERE OwnerId in: newUsers.keySet()]) {
      AccountSegmentationUtility.accountMap.put(ordr.Account__c, ordr.Account__r.Name);
      orderList.add(ordr);
    }
    set<Id> parentHierarchies = new set<Id>();
    AccountSegmentationUtility.hierarchyMap = AccountSegmentationUtility.populateHierarchyMap(segmentValues, false);
    system.debug('****AccountSegmentationUtility.hierarchyMap***'+AccountSegmentationUtility.hierarchyMap);
    for ( Account_Segment__c accSeg : [SELECT Id, Segment__c, Segment__r.Value__c, Name, Account__c, Account__r.Name, Segment__r.Name, Segment__r.Type__c
                                       FROM Account_Segment__c
                                       WHERE Account__c IN :accIds //AccountSegmentationUtility.accountMap.keySet()
                                       AND Segment__r.Name IN :AccountSegmentationUtility.hierarchyMap.keySet()]) {
      System.debug('\n[BatchAccountSegmentUserUpdate: createAccountSegmentRecordforUser]\n==> accSeg: '+ accSeg);
     // String key = accSeg.Account__c + '-' + accSeg.Segment__r.Name;
      String key = accSeg.Account__c + '-' + accSeg.Segment__r.Name;
      //if ( segmentValues.contains(accSeg.Segment__r.Value__c) ) {
      if (!AccountSegmentationUtility.accountSegmentMap.containsKey(key)) {
        AccountSegmentationUtility.accountSegmentMap.put(key, accSeg);
        accountSegmentMapString += '\n' + key;
      }
    }

    system.debug('\n[BatchAccountSegmentUserUpdate: createAccountSegmentRecordforUser]\n==> hierarchyMapString:'+hierarchyMapString);
    system.debug('\n[BatchAccountSegmentUserUpdate: createAccountSegmentRecordforUser]\n==> accountSegmentMapString: '+ AccountSegmentationUtility.accountSegmentMap);


    // This will create the 3 levels of Account Segment hierarchy
    while (AccountSegmentationUtility.level > 0) {
      AccountSegmentationUtility.processByLevel(lstOppty, orderList, newUsers);
      AccountSegmentationUtility.level--;
    }

    // Here we are populating the Opp Segment_* fields, now that all Account Segments have been created
    for (Opportunity opp: lstOppty) { 
      User ownerUser = newUsers.get(opp.OwnerId);
      AccountSegmentationUtility.populateOppSegmentFields(opp, ownerUser, AccountSegmentationUtility.userToOppOrderField.keySet());
    } 
    try {
      update lstOppty;
    } catch (Dmlexception ex) {
      ApexLogHandler.createLogAndSave('BatchAccountSegmentUserUpdate','createAccountSegmentRecordforUser', ex.getStackTraceString(), ex);
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        newUsers.values().get(0).addError(ex.getDmlMessage(i)); 
      }
    }
    
    // Here we are populating the Opp Segment_* fields, now that all Account Segments have been created
    for (Order__c ordr: orderList) {
      if ( ordr.RecordType.Name == 'Read Only' ) {
        continue;
      }
      User ownerUser = newUsers.get(ordr.OwnerId); 
      AccountSegmentationUtility.populateOrderSegmentFields(ordr, ownerUser, AccountSegmentationUtility.userToOppOrderField.keySet());
    }
    try {
      update orderList;
    } catch (Dmlexception ex) {
      ApexLogHandler.createLogAndSave('BatchAccountSegmentUserUpdate','createAccountSegmentRecordforUser', ex.getStackTraceString(), ex);
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        newUsers.values().get(0).addError(ex.getDmlMessage(i)); 
      }
    }
    
    // return accountSegmentMap;
  }
  
}