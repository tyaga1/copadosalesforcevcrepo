/**=====================================================================
 * Name: CommunityCaseDetailCntrl_Test
 * Description: T-376603: Test class for CommunityCaseDetailCntrl 
 *
 * Created Date: June 1st, 2016
 * Created By: Richard Joseph
 *
 * Date Modified            Modified By              Description of the update
 ======================================================================*/
@isTest
private class CommunityCaseDetailCntrl_Test {

    private static testMethod void CommunityCaseDetailCntrlTestMethod() {
        
         Account testAcc = Test_Utils.insertAccount();
      
      Contact testCon = Test_Utils.createContact(testAcc.Id);
      insert testCon; 
      
      ID rectypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_REC_TYPE_EDQ_CASE_TECH_SUPPORT).getRecordTypeId();  
      
        
         Case testCase = Test_Utils.insertCase(false, testAcc.Id);
      testCase.RecordTypeId = rectypeid; 
      testCase.ContactId = testCon.Id;
      testCase.Origin = 'Phone';
      testCase.Subject = 'Test Case';
      testCase.RecordTypeId = rectypeid;
      testCase.Description = 'Test data';   
       
        Insert testCase;
        
        ApexPages.StandardController testStdCntrl = new ApexPages.StandardController(testCase);
        CommunityCaseDetailCntrl  testCntrObj = new CommunityCaseDetailCntrl (testStdCntrl);
        testCntrObj.aCComment.CommentBody = 'Test Comment'; 
        testCntrObj.saveNewComment(); 
        
        System.assert( CommunityCaseDetailCntrl.getCaseComments(testCase.id).size() > 0);
        
    }

}