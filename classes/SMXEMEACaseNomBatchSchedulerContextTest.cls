/**=====================================================================
 * Experian
 * Name: SMXProcessEMEACaseBatch 
 * Description: Implements the test clase for the SMXProcessEMEACaseBatch survey scheduler
 * Created Date: Sep 25th, 2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 *                                         
 =====================================================================*/
@istest(seeAllData=TRUE)
global class SMXEMEACaseNomBatchSchedulerContextTest{
    
    static testmethod void SmxcaseScheduler(){
        test.starttest();
        SMXEMEACaseNomBatchSchedulerContext sc= new SMXEMEACaseNomBatchSchedulerContext();
        String schedule = '0 0 4 * * ?';
        system.schedule('Scheduled Process Nomination Cases', schedule, sc);
        test.stoptest();
    }
    
}