/*=============================================================================
 * Experian
 * Name: AutoCompleteV2ConFilter_Test
 * Description: 
 * Created Date: 19 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/
@isTest
private class AutoCompleteV2ConFilter_Test {
  
  @isTest static void testAutoComplete() {
    
    User myUser = [SELECT Name FROM User WHERE Id = :UserInfo.getUserId()];
    String myId = UserInfo.getUserId();
    
    system.assertNotEquals(0,AutoCompleteV2ConFilter.getData('User','Name','Id','IsActive = true', myUser.Name).size());
    system.assertEquals(0,AutoCompleteV2ConFilter.getData('User','Name','Id','IsActive = true','xxxxxxxxxxxxx').size());
    AutoCompleteV2ConFilter controller = new AutoCompleteV2ConFilter();
    controller.setTargetFieldVar(myId);
    system.assertEquals(myId,controller.getTargetFieldVar());
    
    controller.setCacheField(null);
    
    /*
    User testUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);

    Test_Utils.randomNumber();
    system.runAs(testUser) {
      List<Account> testAccountList = new List<Account>();
      //create some test account
      IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(false);
      Account ultimateAccount = Test_Utils.insertAccount();
      Account parentAccount = Test_Utils.createAccount();
      parentAccount.Ultimate_Parent_Account__c = ultimateAccount.Id;
      insert parentAccount;
      
      Test.startTest();
      Account account = Test_Utils.createAccount();
      account.name= 'TestAccountAutoComplete1';
      account.ParentId = parentAccount.Id;
      insert account;
      Account account2 = Test_Utils.createAccount();
      account2.name= 'TestAccountAutoComplete2';
      account2.ParentId = ultimateAccount.Id;
      insert account2;
        
      system.assertEquals(2,AutoCompleteV2ConFilter.getData('Account','Name','Id','TestAccountAutoComplete').size());
      system.assertEquals(1, AutoCompleteV2ConFilter.getData('Account','Name','Id','TestAccountAutoComplete1').size());
      system.assertEquals(0, AutoCompleteV2ConFilter.getData('Account','Name','Id','xxxxxxx').size());
      
      AutoCompleteV2ConFilter controller = new AutoCompleteV2ConFilter();
      controller.setTargetFieldVar(account.Id);
      system.assertEquals(account.Id,controller.getTargetFieldVar());
      controller.setCacheField(null);
        
      Test.stopTest();
    }
    */
  }
  
}