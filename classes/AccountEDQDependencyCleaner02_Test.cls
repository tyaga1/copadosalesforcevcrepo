/**********************************************************************************************
 * Experian 
 * Name         : AccountEDQDependencyCleaner02_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of scheduler class "ScheduleEDQDependencyValidation06" & AccountEDQDependencyCleaner02
 * Created Date : September 8th, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * Nov 11th, 2015               Paul Kissick                Case 01266075: Removed bad global setting entry, and testSchedulable
 * Jun 6th, 2016                Paul Kissick                Case 02013456: Improving coverage
***********************************************************************************************/

@isTest
private class AccountEDQDependencyCleaner02_Test {
    
  static testMethod void testAccountEDQDependencyC2() {
    
    Test.startTest();
    AccountEDQDependencyCleaner02 batchToProcess = new AccountEDQDependencyCleaner02();
    database.executebatch(batchToProcess);
    Test.stopTest();
    
    system.assertEquals(1,[SELECT COUNT() FROM Account WHERE EDQ_Dependency_Exists__c = false]);
  }
  
  @testSetup
  private static void setupData() {
       
    Account tstAcc = Test_utils.insertEDQAccount(false);
    tstAcc.EDQ_Dependency_Exists__c = true;
    tstAcc.SaaS__c = false;
    insert new List<Account>{tstAcc};
        
  }
  
}