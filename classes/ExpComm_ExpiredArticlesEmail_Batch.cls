/**
* @description Email notification to user/manager about expired (English language) knowledge articles.
* Identifies all articles expired in the last 7 days and emails a link of a report to the user or manager(if user is inactive)
* @author Arun Jayaseelan, UC Innovation
* @date 1/30/2017
*/
global class ExpComm_ExpiredArticlesEmail_Batch implements Database.Batchable<sObject> {
    String query;
    private Set<Id> userSet = new Set<Id>();
    private List<User> userList = new List<User>(); 
    private String reportURL = '';
    public Static List<Messaging.SendEmailResult> mailingResults;
    private String userEmailBody = 'Salesforce Knowledge Articles pending review. Click the link below to access a report: \r\n \r\n Note:After opening the link, click the \'Run Report\' button: \r\n';
    private String mgrEmailBody = 'An Invalid User in your team has Salesforce Knowledge Articles pending review. Click the link below to access a report: \r\n';
    private String orphanUserEmailBody = 'An Invalid User with no Manager has Salesforce Knowledge Articles pending review. Click the link below to access a report: \r\n';

    Date startDate;
    Date endDate;
    String strStartDate;
    String strEndDate;
    private String expDateCriteria;
    
    global ExpComm_ExpiredArticlesEmail_Batch() {
        if(Test.isRunningTest()) {
            expDateCriteria = 'Expiration_Date__c = NEXT_N_DAYS:7';
        }else{
            expDateCriteria = 'Expiration_Date__c = LAST_N_DAYS:7';
        }
        startDate = Date.today()-7;
        endDate = Date.today();
        strStartDate = startDate.Month()+'/'+startDate.day()+'/'+startDate.Year();
        strEndDate = endDate.Month()+'/'+endDate.day()+'/'+endDate.Year();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'Select Id, Title, LastModifiedDate, Article_Owner__c, Expiration_Date__c From Employee_Article__kav '
                + ' Where PublishStatus = \'Online\' AND Language = \'en_US\'  AND ' + expDateCriteria;
        system.debug('query results: '+ database.query(query));        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> expiredArticles) {
        for(sObject sObj:expiredArticles){
            Employee_Article__kav ka = (Employee_Article__kav)sObj;
            userSet.add(ka.Article_Owner__c);
        }
        
        userList = [Select Id, Name, isActive, ManagerId From User Where Id In :userSet];
        List<Messaging.SingleEmailMessage> mailingList = new List<Messaging.SingleEmailMessage>();

        for(User u:userList){
            reportURL = URL.getSalesforceBaseURL().toExternalForm() + '/' + Label.Expired_Employee_Articles_ReportId
                            +'?pv0='+u.Id+'&'+'pv1='+strStartDate+'&'+'pv2='+strEndDate+'&'+'pv3=English';
            if(u.isActive){
                mailingList.add(createEmail(reportURL,u.id,userEmailBody, 'Salesforce Knowledge Articles Expiration notice'));
                //reportURL = 'Invalid User Name: ' + u.Name + '\r\n \r\n' + 'Note:After opening the link, click the \'Run Report\' button: \r\n' + reportURL;
                //mailingList.add(createEmail(reportURL,u.ManagerId,mgrEmailBody));
            }else if(u.ManagerId!=null){
                reportURL = 'Invalid User Name: ' + u.Name + '\r\n \r\n' + 'Note:After opening the link, click the \'Run Report\' button: \r\n' + reportURL;
                mailingList.add(createEmail(reportURL,u.ManagerId,mgrEmailBody, 'Salesforce Knowledge Articles Expiration notice for Invalid User'));
                
            }else{
                reportURL = 'Invalid User Name: ' + u.Name + '\r\n \r\n' + 'Note:After opening the link, click the \'Run Report\' button: \r\n' + reportURL;
                mailingList.add(createEmail(reportURL,null,orphanUserEmailBody, 'Salesforce Knowledge Articles Expiration notice'));
            }
        }

        
        

        if(!mailingList.isEmpty()){
            mailingResults = Messaging.sendEmail(mailingList, false);
        }
        system.debug(mailingResults);
    }
    
    global void finish(Database.BatchableContext BC) {
        ExpComm_ExpiredArticlesEmail_es_Batch esBatchable = new ExpComm_ExpiredArticlesEmail_es_Batch();
        database.executebatch(esBatchable);
    }

     /**
     * @description Creates and returns single email messages.
     *
     * @author Arun Jayaseelan, UC Innovation
     * @date 1/30/2017
     */
    public static Messaging.SingleEmailMessage createEmail(String reportURL, Id userId, String emailBody, String emailSubject){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        email.setPlainTextBody(emailBody+reportURL);
        if(userId != null){
            email.setTargetObjectId(userId);    
        }else{
            List<String> toAddressList = new List<String>();
            toAddressList.add('richard.joseph@experian.com');
            email.setToAddresses(toAddressList);
        }
        email.setSubject(emailSubject);
        email.setSaveAsActivity(false);

        return email;
    }
    
}