/**=====================================================================
 * Experian
 * Name: ScheduleAssignmentTeam 
 * Description: Case 01098427
 * Created Date: 17th Aug, 2015
 * Created By: Paul Kissick
 * 
 * Date Modified     Modified By        Description of the update
 * Jan 8th, 2016     Paul Kissick       Case 01662532: Adding scope sizing for this batch
 =====================================================================*/
global class ScheduleAssignmentTeamV2 implements Schedulable {
  global void execute(SchedulableContext SC) {
    // Call the BatchAssignmentTeamV2 with the fixMissedAssignments argument set to false (run normally)
    BatchAssignmentTeamV2 batch = new BatchAssignmentTeamV2(false); 
    Database.executeBatch(batch, ScopeSizeUtility.getScopeSizeForClass('BatchAssignmentTeamV2')); 
  }
}