/**=====================================================================
 * Name: BatchDQFlagPopulation_TEST
 * Description: CRM2 W-005615: Tests Classes BatchDQFlagPopulation and BatchDQFlagPopulationSchedule
 * Created Date: Aug 19th, 2016
 * Created By: Cristian Torres
 * 
 * Date Modified       Modified By          Description of the update
 * 25 Aug, 2016        Paul Kissick         CRM2:W-005615: Fixed test class to be private and also split into proper test methods
 =====================================================================*/

@isTest
private class BatchDQFlagPopulation_TEST {

  static testMethod void testBatch() {
    
    Test.startTest();
    
    Database.executeBatch(new BatchDQFlagPopulation());
    
    Test.stopTest();
  }
  
  static testMethod void testSchedule() {
    
    Test.startTest();

    system.schedule('Populate DQ Flag, Decision Maker TEST'+String.valueOf(Datetime.now().getTime()), '0 0 * * * ? *', new BatchDQFlagPopulationSchedule());
    
    Test.stopTest();
    
  }
  
  @testSetup
  static void setupTest() {
    //Create and insert a new Account  
    Account acc1 = Test_Utils.insertAccount();

    // Create and insert a contact
    Contact con1 = Test_Utils.insertContact(acc1.Id);

    //Create an opportunity
    Opportunity opp1 = Test_Utils.createOpportunity(acc1.Id);
    insert opp1;
     
    OpportunityContactRole oppCR1 = new OpportunityContactRole();
    oppCR1.Role = Constants.DECIDER;
    oppCR1.ContactId = con1.Id;
    oppCR1.OpportunityId = opp1.Id;
    insert oppCR1;

    //Create and insert a new Account  
    Account acc2 = Test_Utils.insertAccount();
    // Create and insert a contact
    Contact con2 = Test_Utils.insertContact(acc2.Id);

    //Create an opportunity
    Opportunity opp2 = Test_Utils.createOpportunity(acc2.Id);
    insert opp2;
        
    OpportunityContactRole oppCR2 = new OpportunityContactRole();
    oppCR2.Role = 'Assessor';
    oppCR2.ContactId = con2.Id;
    oppCR2.OpportunityId = opp2.Id;
    insert oppCR2;
    
  }
	
}