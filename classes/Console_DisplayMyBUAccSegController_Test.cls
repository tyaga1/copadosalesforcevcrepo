/**=====================================================================
 * Appirio, Inc
 * Name: Console_DisplayMyBUAccSegController_Test
 * Description: Test class for Console_DisplayMyBUAccSegController
 * Created Date: Aug 19th, 2015
 * Created By: Parul Gupta
 *
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest(seeAllData = false)
public with sharing class Console_DisplayMyBUAccSegController_Test {
  
  static testMethod void controllerTest() {
    Account account = [SELECT Id FROM account Limit 1];
    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(account);

    Test.startTest();
    Console_DisplayMyBUAccSegController controller = new Console_DisplayMyBUAccSegController(sc);

    System.assertNotEquals(null, controller.fieldSet);
    System.assertEquals(false, controller.isEdit);

    controller.edit();
    System.assertEquals(true, controller.isEdit);

    controller.cancel();
    System.assertEquals(false, controller.isEdit);

    controller.accSeg.Total_Won__c = 15;
    controller.save();
    System.assertEquals(false, controller.isEdit);
    
    controller.edit();    
    controller.accSeg.AHS_Health_Status__c = 'Yellow';
    controller.accSeg.AHS_Include_in_Status_Report__c = true;
    controller.save();
    
    // Enforce validation rules to fail and assert for page messages
    controller.edit(); 
    controller.accSeg.AHS_Include_in_Status_Report__c = false;
    controller.save();
		system.assert(ApexPages.getMessages().size() > 0);
		
    Test.stopTest();
  }

  @testSetup 
  static void createTestData() {
    Account testAcc = Test_Utils.createAccount();
    insert testAcc;

    Hierarchy__c grandParentHierarchy = new Hierarchy__c();
    grandParentHierarchy.Type__c = 'Global Business Line';
    grandParentHierarchy.Value__c = 'Grand Parent';
    grandParentHierarchy.Unique_Key__c = 'My BU';
    insert grandParentHierarchy;

    Hierarchy__c parentHierarchy = new Hierarchy__c();
    parentHierarchy.Type__c = 'Business Line';
    parentHierarchy.Value__c = 'Parent';
    parentHierarchy.Unique_Key__c = 'My BU Blah';
    parentHierarchy.Parent__c = grandParentHierarchy.Id;
    insert parentHierarchy;

    Hierarchy__c childHierarchy = new Hierarchy__c();
    childHierarchy.Type__c = 'Business Unit';
    childHierarchy.Value__c = 'Child';
    childHierarchy.Parent__c = parentHierarchy.Id;
    childHierarchy.Unique_Key__c = 'My BU Blah2';
    insert childHierarchy;

    Account_Segment__c segment = Test_Utils.insertAccountSegment(false, testAcc.Id, grandParentHierarchy.Id, null);
    insert segment;

    Account_Segment__c segment2 = Test_Utils.insertAccountSegment(false, testAcc.Id, parentHierarchy.Id, segment.Id);
    insert segment2;

    Account_Segment__c segment3 = Test_Utils.insertAccountSegment(false, testAcc.Id, childHierarchy.Id, segment2.Id);
    insert segment3;

    Account_Segmentation_Mapping__c mapping = new Account_Segmentation_Mapping__c();
    mapping.Name = 'Child';
    mapping.Global_Business_Line__c = 'Grand Parent';
    mapping.Business_Line__c = 'Parent';
    mapping.Business_Unit__c = 'Child';
    mapping.Common_View_Name__c = 'CSDA';
    mapping.Field_Set_API_Name__c = 'CSDA';
    insert mapping;
    
    for(User user : [Select id, Business_Unit__c, Business_Line__c, Global_Business_Line__c from User where id =: UserInfo.getUserId()]){
    	user.Global_Business_Line__c = 'Grand Parent';
    	user.Business_Line__c = 'Parent';
    	user.Business_Unit__c = 'Child';
    	update user;
    }
  }

}