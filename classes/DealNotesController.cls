/**=====================================================================
 * Experian
 * Name: DealNotesController
 * Description: Controller to display list of Notes, because the standard 
    related list has been removed
 * Created Date: Oct 29th, 2015
 * Created By: Paul Kissick 
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
public with sharing class DealNotesController {

  Deal__c deal;
  
  public Id delNoteId {get;set;}
  
  public class NoteWrapper {
    public Note rNote {get;set;}
    public String noteLastMod {get;set;}
    public NoteWrapper(Note n) {
      rNote = n;
      noteLastMod = n.LastModifiedDate.formatLong();
    }
  }

  public DealNotesController(ApexPages.StandardController con) {
    deal = (Deal__c)con.getRecord();
  }
  
  public List<NoteWrapper> getDealNotes() {
    List<NoteWrapper> returnNotes = new List<NoteWrapper>();
    List<Note> notes = [
      SELECT Id, CreatedBy.Name, LastModifiedDate, Title 
      FROM Note 
      WHERE ParentId = :deal.Id
    ]; 
    for(Note n : notes) {
      returnNotes.add(new NoteWrapper(n));
    }
    return returnNotes;
  }
  
  public PageReference delNote() {
    if (delNoteId != null) {
      try {
        delete new Note(Id = delNoteId);
      }
      catch (Exception e) {
        //
      }
    }
    return null;
  }

}