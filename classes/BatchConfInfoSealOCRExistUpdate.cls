/*=============================================================================
 * Experian
 * Name: BatchConfInfoSealOCRExistUpdate
 * Description: Case 02224510 : Set the Seal OCR File Exists to true on the Confidential Information record when Chatter Feed Attachment is created by          SealAPIUSer
 * Schedule to run Everyday at 6pm CST.
 * Created Date: 06th Dec 2016
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 
 ============================================================================*/

global class BatchConfInfoSealOCRExistUpdate implements Database.Batchable<sObject>, Database.Stateful
{
    
    //start method of batch class
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        string strSealAPIUserId = Global_Settings__c.getValues('Global').Seal_API_User__c;//Get the Seal API UserId from Custom setting           
        String query ='select Id,ParentId from Confidential_Information__Feed where CreatedDate >= LAST_N_DAYS:1 AND CreatedById = :strSealAPIUserId '; 
        return Database.getQueryLocator(query);       
    }
    //execute method of batch class
    global void execute(Database.BatchableContext BC, List<Confidential_Information__Feed> lstConf){    
        set<string> lstConfids = new set<string>();
        for(Confidential_Information__Feed conf:lstConf){
            lstConfids.add(conf.ParentId);
        } 
        
        list<Confidential_Information__c> lstConfUpdate = [select id,Seal_OCR_File_Exists__c from Confidential_Information__c where Seal_OCR_File_Exists__c = false AND Id IN :lstConfids];    
        for(Confidential_Information__c conf:lstConfUpdate){
            conf.Seal_OCR_File_Exists__c = true;
        }
        Database.update(lstConfUpdate);
    }   
    
    global void finish(Database.BatchableContext BC){
       /*BatchHelper bh = new BatchHelper();
       bh.checkBatch(bc.getJobId(), 'BatchConfInfoSealOCRExistUpdate', true);*/
    }
}