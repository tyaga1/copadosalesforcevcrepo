/*=============================================================================
 * Experian
 * Name: NominationHelper
 * Description: 
 * Created Date: 4 Oct 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 31 Oct, 2016       Paul Kissick          Added 'OtherReason' to Constants
 * 03 Nov, 2016      Paul Kissick          Added Recognition_Category__c to the state listing
 =============================================================================*/

public class NominationHelper {
  
  public static List<String> errorsFound = new List<String>();
  
  //===========================================================================
  // Q. WHY IS THIS HERE?
  // A. Because it provides a single point of change should the business decide to change the naming
  // of any of the picklists
  //===========================================================================
  public static Map<String, String> NomConstants {get{
    if (NomConstants == null) {
      NomConstants = new Map<String, String>{
        'Level1ProfileBadge' => 'Level 1 - Profile Badge',
        'Level2SpotAward' => 'Level 2 - Spot Award',
        'Level3Individual' => 'Level 3 - Half Year Nomination (Individual)',
        'Level3Team' => 'Level 3 - Half Year Nomination (Team)',
        'Level4Elite' => 'Level 4 - Elite',
        
        'Submitted' => 'Submitted',
        'PendingApproval' => 'Pending Approval',
        'Approved' => 'Approved',
        'Won' => 'Won',
        'Rejected' => 'Rejected',
        'RejectedBadge' => 'Rejected with Badge',
        'DowngradedToLevel2' => 'Downgraded To Level 2',
        'Nominated' => 'Nominated',
        'PendingManagerApproval' => 'Pending Manager Approval',
        'PendingPanelApproval' => 'Pending Panel Approval',
        'ManagerApproved' => 'Manager Approved',
        
        'OtherReason' => 'Other (Please Specify)'
      };
    }
    return NomConstants;
  } private set;}
  
  //===========================================================================
  // For the current user, am I a Recognition Coordinator user
  //===========================================================================
  public static Boolean isRecogCoord {get {
    if (isRecogCoord == null) {
      isRecogCoord = hasCustomPermission('Recognition_Coordinator');
    }
    return isRecogCoord;
  } set;}
  
  //===========================================================================
  // For the current user, am I a Recognition Data Admin user
  //===========================================================================
  public static Boolean isRecogDataAdmin {get {
    if (isRecogDataAdmin == null) {
	    isRecogDataAdmin = hasCustomPermission('Recognition_Data_Admin');
    }
    return isRecogDataAdmin;
  } set;}
  
  //===========================================================================
  // This could be placed in a global helper class - at some point.
  // Checks if the supplied custom permission is applied for the current user. 
  //===========================================================================
  private static Boolean hasCustomPermission(String permApiName) {
    if (String.isBlank(permApiName)) {
      return false;
    }
    CustomPermission cpAllow = [
      SELECT Id 
      FROM CustomPermission 
      WHERE DeveloperName = :permApiName
      LIMIT 1
    ];
    Integer testCount = [
      SELECT COUNT()
      FROM PermissionSetAssignment
      WHERE PermissionSetId IN (
        SELECT ParentId
        FROM SetupEntityAccess
        WHERE SetupEntityType = 'CustomPermission' 
        AND SetupEntityId = :cpAllow.Id
      )
      AND AssigneeId = :UserInfo.getUserId()
    ];
    return (testCount == 0) ? false : true;
  }
  
  //===========================================================================
  // Using the Nomination State field and Recognition Category, creates a map
  // of the badges to apply for these 'states', e.g.
  // Collaborate to Win,Approved,Level 2 - Spot Award => 'badgeid'
  //===========================================================================
  public static Map<String, Id> stateToBadge {get{
    if (stateToBadge == null) {
      stateToBadge = new Map<String, Id>();
      for (WorkBadgeDefinition badge : [SELECT Id, Nomination_State__c, Recognition_Category__c
                                        FROM WorkBadgeDefinition
                                        WHERE Nomination_State__c != null
                                        AND Recognition_Category__c != null
                                        AND IsActive = true]) {
        List<String> nomStates = badge.Nomination_State__c.split(';');
        for (String nomState : nomStates) {
          stateToBadge.put(badge.Recognition_Category__c + ',' + nomState.trim(), badge.Id);
        }
      }
    }
    return stateToBadge;
  } private set;}
  
  
  
  //===========================================================================
  // Needed when added as another extension to the New/View/Home pages
  //===========================================================================
  public NominationHelper(ApexPages.StandardController s) {
  }
  
  
  //===========================================================================
  // Overriden method with 5 parameters for postBadgeToProfile (6 params)
  //===========================================================================
  public static Boolean postBadgeToProfile (Id userId, Id giverId, String msg, Id badgeId, Id nomId) {
    return postBadgeToProfile(userId, giverId, msg, badgeId, nomId, null);
  }
  
  //===========================================================================
  // Takes a recipient, giver, message, badge, nomination and group and posts
  // a badge for that single user.
  //===========================================================================
  public static Boolean postBadgeToProfile (Id userId, Id giverId, String msg, Id badgeId, Id nomId, Id groupId) {
    return postBadgeToProfilesMany(
      new Map<String, Id>{'1' => userId},
      new Map<String, Id>{'1' => giverId}, 
      new Map<String, String>{'1' => msg},
      new Map<String, Id>{'1' => badgeId},
      new Map<String, Id>{'1' => nomId},
      new Map<String, Id>{'1' => groupId}
    );
  }
  
  //===========================================================================
  // Generic method to take a map of users, with maps of messages, and map of
  // badge Ids, and post this as a chatter profile badge to the users. 
  //===========================================================================
  public static Boolean postBadgeToProfilesMany (Map<String, Id> refToUserMap, 
                                                 Map<String, Id> refToGiverMap, 
                                                 Map<String, String> refToMessageMap, 
                                                 Map<String, Id> refToBadgeMap, 
                                                 Map<String, Id> refToNomMap) {
    return postBadgeToProfilesMany(refToUserMap, refToGiverMap, refToMessageMap, refToBadgeMap, refToNomMap, new Map<String, Id>());
  }
  
  //===========================================================================
  // General method to post a badge to a profile for many users.
  //===========================================================================
  public static Boolean postBadgeToProfilesMany (Map<String, Id> refToUserMap, 
                                                 Map<String, Id> refToGiverMap, 
                                                 Map<String, String> refToMessageMap, 
                                                 Map<String, Id> refToBadgeMap, 
                                                 Map<String, Id> refToNomMap,
                                                 Map<String, Id> refToGroupMap) {
    
    Savepoint sp = Database.setSavepoint();
    Boolean successInsert = false;
    errorsFound = new List<String>();
    
    // Will only hold valid nominations, instead of all!
    Map<String, WorkThanks> workThanksMap = new Map<String, WorkThanks>();
    Map<String, WorkBadge> workBadgeMap = new Map<String, WorkBadge>();
    Map<String, FeedItem> feedItemMap = new Map<String, FeedItem>();
    
    // Get the names of everyone, if we need to post to a group...
    // Maybe pull this out into a separate static variable?
    Map<Id, User> userNameMap = new Map<Id, User>([
      SELECT Id, Name
      FROM User
      WHERE Id IN :refToUserMap.values()
    ]);
    
    // Create a WorkThanks record for each recipient, from each 'giver'.
    // These create the base object to then add the badge and then the profile post to.
    WorkThanks wt;
    for (String ref : refToUserMap.keySet()) {
      wt = new WorkThanks(
        GiverId = refToGiverMap.get(ref), 
        Message = refToMessageMap.get(ref),
        OwnerId = refToGiverMap.get(ref) // PK: Added owner setting.
      );
      if (refToGroupMap.containsKey(ref) && 
          refToGroupMap.get(ref) != null && 
          refToUserMap.containsKey(ref) && 
          refToUserMap.get(ref) != null && 
          userNameMap.containsKey(refToUserMap.get(ref))) {
        wt.Message = userNameMap.get(refToUserMap.get(ref)).Name + ' - ' + wt.Message;
      }
      workThanksMap.put(ref, wt);
    }
    
    if (!workThanksMap.isEmpty()) {
      successInsert = false;
      try {
        insert workThanksMap.values();
        successInsert = true;
      }
      catch (Exception e) {
        Database.rollback(sp);
        errorsFound.add(e.getMessage());
      }
      
      if (!successInsert) {
        return false;
      }
      
      // After adding the WorkThanks, now add each WorkBadge referencing who is receiving the thanks, and the badge. Also adding custom nomination field.
      for (String ref : refToUserMap.keySet()) {
        workBadgeMap.put(
          ref,
          new WorkBadge(
            DefinitionId = refToBadgeMap.get(ref), 
            RecipientId = refToUserMap.get(ref), 
            SourceId = workThanksMap.get(ref).Id,
            Nomination__c = ((refToNomMap != null && refToNomMap.containsKey(ref) && refToNomMap.get(ref) != null) ? refToNomMap.get(ref) : null)
          )
        );
      }
      
      if (!workBadgeMap.isEmpty()) {
        successInsert = false;
        try {
          insert workBadgeMap.values();
          successInsert = true;
        }
        catch (Exception e) {
          Database.rollback(sp);
          errorsFound.add(e.getMessage());
        }
          
        if (!successInsert) {
          return false;
        }
        
        Map<Id, List<String>> groupIdToRef = new Map<Id, List<String>>();
        Map<Id, String> groupIdToOrigMessage = new Map<Id, String>();
        
        // Finally, after each badge is added, we need a FeedItem to link this badge to the profile. 
        for (String ref : refToUserMap.keySet()) {
          feedItemMap.put(
            ref,
            new FeedItem(
              ParentId = refToUserMap.get(ref),
              CreatedById = refToGiverMap.get(ref),
              Body = workThanksMap.get(ref).Message, //refToMessageMap.get(ref),
              RelatedRecordId = workThanksMap.get(ref).Id,
              Type = 'RypplePost'
            )
          );
          
          if (refToGroupMap.containsKey(ref) && 
              refToGroupMap.get(ref) != null && 
              userNameMap.containsKey(refToUserMap.get(ref))) {
            // Posting to a group, will group the noms per group id...
            if (!groupIdToRef.containsKey(refToGroupMap.get(ref))) {
              groupIdToRef.put(refToGroupMap.get(ref), new List<String>());
            }
            groupIdToRef.get(refToGroupMap.get(ref)).add(ref);
            groupIdToOrigMessage.put(refToGroupMap.get(ref), refToMessageMap.get(ref));
          }
        }
        
        if (!groupIdToRef.isEmpty()) {
          // Group Posting - Requires a single post for the group containing the names of each recipient.
          for (Id groupId : groupIdToRef.keySet()) {
            List<String> nomRefs = groupIdToRef.get(groupId);
            Id holdingSourceId = null;
            Id holdingGiverId = null;
            List<String> nomNames = new List<String>();
            for (String ref : nomRefs) {
              if (holdingSourceId == null) {
                holdingSourceId = workBadgeMap.get(ref).SourceId;
                holdingGiverId = workThanksMap.get(ref).GiverId;
              }
              if (userNameMap.containsKey(workBadgeMap.get(ref).RecipientId)) {
                nomNames.add(userNameMap.get(workBadgeMap.get(ref).RecipientId).Name);
              }
            }
            feedItemMap.put(
              groupId,
              new FeedItem(
                ParentId = groupId,
                CreatedById = holdingGiverId,
                RelatedRecordId = holdingSourceId, // just for the display, pull the first one.
                Body = String.join(nomNames, ', ') + ' - ' + groupIdToOrigMessage.get(groupId),
                Type = 'RypplePost'
              )
            );
          }
        }
        if (!feedItemMap.isEmpty()) {
          successInsert = false;
          try {
            insert feedItemMap.values();
            successInsert = true;
          }
          catch (Exception e) {
            Database.rollback(sp);
            errorsFound.add(e.getMessage());
          }
            
          if (!successInsert) {
            return false;
          }
        }
      }
    }
    return successInsert;
  }
  
  //===========================================================================
  // As a nominee, I should be able to see my nominations, once they are approved.
  //===========================================================================
  public static void createNomineeShares(List<Nomination__c> nomList) {
    
    Set<Id> usersToCheck = new Set<Id>();
    
    List<Nomination__Share> newReadShares = new List<Nomination__Share>();
    
    for (Nomination__c nom : nomList) {
      if (nom.Nominee__c != null) {
        usersToCheck.add(nom.Nominee__c);
      }
    }
    
    Map<Id, User> activeUserMap = new Map<Id, User>([
      SELECT Id
      FROM User
      WHERE IsActive = true
      AND Id IN :usersToCheck
    ]);
    
    for (Nomination__c nom : nomList) {
      if (nom.Nominee__c != null && activeUserMap.containsKey(nom.Nominee__c)) {
        newReadShares.add(new Nomination__Share(
          ParentId = nom.Id,
          UserOrGroupId = nom.Nominee__c,
          AccessLevel = Constants.ACCESS_LEVEL_READ,
          RowCause = Schema.Nomination__Share.RowCause.Nominee__c
        ));
      }
    }
    
    insert newReadShares;
    
  }
  
  
  //===========================================================================
  // When a nomination is created, add the required sharing records.
  //===========================================================================
  public static void createNominationShares(List<Nomination__c> nomList) {
    
    List<Nomination__Share> newEditShares = new List<Nomination__Share>();
    
    Set<Id> masterNomIds = new Set<Id>();
    Set<Id> usersToCheck = new Set<Id>();
    
    for (Nomination__c nom : nomList) {
      if (nom.Project_Sponsor__c != null) {
        usersToCheck.add(nom.Project_Sponsor__c);
      }
      if (nom.Master_Nomination__c != null && nom.Nominees_Manager__c != null) {
        usersToCheck.add(nom.Nominees_Manager__c);
      }
      if (nom.Requestor__c != null) {
        usersToCheck.add(nom.Requestor__c);
      }
    }
    
    if (usersToCheck.isEmpty()) {
      return;
    }
    
    Map<Id, User> activeUserMap = new Map<Id, User>([
      SELECT Id
      FROM User
      WHERE IsActive = true
      AND Id IN :usersToCheck
    ]);
    
    for (Nomination__c nom : nomList) {
      if (nom.Requestor__c != null && activeUserMap.containsKey(nom.Requestor__c)) {
        newEditShares.add(new Nomination__Share(
          ParentId = nom.Id,
          UserOrGroupId = nom.Requestor__c,
          AccessLevel = Constants.ACCESS_LEVEL_READ,
          RowCause = Schema.Nomination__Share.RowCause.Nominator__c
        ));
      }
      // Create a Share for the Project Sponsor on the child record
      if (nom.Project_Sponsor__c != null && activeUserMap.containsKey(nom.Project_Sponsor__c)) {
        newEditShares.add(new Nomination__Share(
          ParentId = nom.Id,
          UserOrGroupId = nom.Project_Sponsor__c,
          AccessLevel = Constants.ACCESS_LEVEL_EDIT,
          RowCause = Schema.Nomination__Share.RowCause.Project_Sponsor__c
        ));
      }
      // Create a Share for the Nominees Manager on the parent record
      if (nom.Master_Nomination__c != null && nom.Nominees_Manager__c != null && activeUserMap.containsKey(nom.Nominees_Manager__c)) {
        masterNomIds.add(nom.Master_Nomination__c);
        newEditShares.add(new Nomination__Share(
          ParentId = nom.Master_Nomination__c,
          UserOrGroupId = nom.Nominees_Manager__c,
          AccessLevel = Constants.ACCESS_LEVEL_EDIT,
          RowCause = Schema.Nomination__Share.RowCause.Team_Member_Manager__c
        ));
      }
    }
    
    insert newEditShares;
    
    if (masterNomIds.isEmpty()) {
      return;
    }
    
    Map<Id, Set<Id>> nomToManagers = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> nomToChildNoms = new Map<Id, Set<Id>>();
    
    // Filters out inactive managers
    for (Nomination__c cNom : [SELECT Id, Nominees_Manager__c, Nominees_Manager__r.IsActive, Master_Nomination__c 
                               FROM Nomination__c
                               WHERE Master_Nomination__c IN :masterNomIds
                               AND Nominees_Manager__c != null
                               AND Nominees_Manager__r.IsActive = true]) {
      if (!nomToManagers.containsKey(cNom.Master_Nomination__c)) {
        nomToManagers.put(cNom.Master_Nomination__c, new Set<Id>());
      }
      if (!nomToChildNoms.containsKey(cNom.Master_Nomination__c)) {
        nomToChildNoms.put(cNom.Master_Nomination__c, new Set<Id>());
      }
      nomToManagers.get(cNom.Master_Nomination__c).add(cNom.Nominees_Manager__c);
      nomToChildNoms.get(cNom.Master_Nomination__c).add(cNom.Id);
    }
    
    // Create Read only shares on the child nominations
    List<Nomination__Share> newReadShares = new List<Nomination__Share>();
    
    for (Id masterNomId : nomToChildNoms.keySet()) {
      for (Id childNomId : nomToChildNoms.get(masterNomId)) {
        for (Id managerId : nomToManagers.get(masterNomId)) {
          newReadShares.add(new Nomination__Share(
            ParentId = childNomId,
            UserOrGroupId = managerId,
            AccessLevel = Constants.ACCESS_LEVEL_READ,
            RowCause = Schema.Nomination__Share.RowCause.Team_Member_Manager__c
          ));
        }
      }
    }
    insert newReadShares;
    
  }

}