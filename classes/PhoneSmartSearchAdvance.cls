/**=====================================================================
 * Experian, Inc
 * Name: PhoneSmartSearchAdvance
 * Description: Smart Search to Retrive Contacts Account and Leads using the Phone Number on them 
 * Created Date: 06/12/2015
 * Created By: Tyaga Pati
 * 
 * Date Modified                Modified By                  Description of the update
=====================================================================*/
public with sharing class PhoneSmartSearchAdvance{

       public list <Contact> Con {get;set;}
       public list <Account> acnt {get;set;}
       public list <Lead> Led {get;set;}
       public string searchstring {get;set;}
       public void clear(){  
                  Con.clear();  
                  acnt.Clear();
                  Led.Clear();
               }
               
       Public Void doQuery(){
               String queryString = '';
               String searchstringfinal='';
               if(searchstring !=null)
                   searchstringfinal=searchstring.replaceAll('[^a-zA-Z0-9]+','');
                   queryString='%'+searchstringfinal+'%';
                   con=[select Name, Account.name, Account.Id, Phone, MobilePhone, EMail from Contact where PhoneNumberFormatted__c like:queryString
                   OR formattedAssistantPhone__c like:queryString
                   OR formattedDirectLine__c like:queryString
                   OR formattedHomePhone__c like:queryString
                   OR formattedMobilePhone__c like:queryString 
                   OR formattedOtherPhone__c like:queryString 
                   OR formattedDirectPhoExt__c like:queryString 
                   OR FormattedPhoneExt__c like:queryString limit 50];
                   
               acnt = [select Name, Phone, Region__c, Type, Industry, DQ_Status__c from Account where PhoneNumberFormatted__c like:queryString limit 50]; 
               Led= [select Name, Company, Phone, MobilePhone, Email, Status, Owner.Alias from Lead where PhoneNumberFormatted__c like:queryString 
               OR formattedDirctLine__c like:queryString 
               OR FormattedDirectlienExt__c like:queryString 
               OR formattedMobilePhone__c like:queryString 
               OR formattedPhoneExten__c like:queryString limit 50]; 
       
               }                   

       public PhoneSmartSearchAdvance() {
               
               doQuery();
               clear();

     }

}