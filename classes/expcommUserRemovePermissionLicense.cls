/******************************************************************************
 * Name: expcommUserRemovePermissionLicense.cls
 * Created Date: 2/24/2017
 * Created By: Hay Win, UCInnovation
 * Description : Invocable method to use from Process Builder: User License Change 
 *                 to remove assignment PermissionSetLicense for
 *                 inActive User with Salesforce Platform License
                   or User without Salesforce Platform License
 * Change Log- 
 * Modified Date        Modified By            Comments
 * May 8th, 2017        Ryan (Weijie) Hu       Fixing the issues where removing permission set license assignment from users requires
 *                                             removal of permission sets assignment first.
 * May 12th, 2017       Charlie Park           Remove correct permission set before trying to remove permission set license
 ****************************************************************************/
 
public class expcommUserRemovePermissionLicense {

  @InvocableMethod(label='Remove permission License' description='Remove permission set license assignment')
  public static void removePermissionLicense(List<String> userID) {

  // Get all permission set under "Company Community for Force.com(API Name: CompanyCommunityPsl)" PermissionSetLicense
  List<PermissionSetAssignment> permissionSetRemovalList = [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId IN: userID AND PermissionSet.Label = 'Global Experian Employee Case Access'];

  // Remove assignments of permission sets from given user list
  if (permissionSetRemovalList != null && permissionSetRemovalList.size() > 0) {
    delete permissionSetRemovalList;
  }

  // And then proceed to permission set license assignment removal

  List<PermissionSetLicenseAssign> removeList = new List<PermissionSetLicenseAssign>();
    
    for(PermissionSetLicenseAssign assignment: [SELECT AssigneeId, PermissionSetLicenseId FROM PermissionSetLicenseAssign WHERE PermissionSetLicense.MasterLabel = 'Company Community for Force.com' and AssigneeId IN: userID]) {       
         removeList.add(assignment);
    }
    
    if (removeList.size() > 0) {
        delete removeList;
    }
  }
}