/**=====================================================================
 * Name: FeddItemTriggerTest
 * Description: Trigger for FeedItem object                 
 * Created Date: Aug 14th, 2017
 * Created By: Mauricio Murillo
 * 
 * Date Modified                Modified By                  Description of the update
**=====================================================================*/
@isTest
public class FeedItemTriggerHandler_Test {

    @isTest
    public static void testDeletePosts(){
    
        // create test data
        User testUser1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
        insert testUser1;
        Account acc = Test_Utils.insertAccount();
        User testUser2 = Test_Utils.createUser(Constants.PROFILE_EXP_SALES_MANAGER);
        insert testUser2;
        Id postId;
        Id postId2;
        Id postId3;
        Id postId4;
        FeedItem post;
        FeedItem post2;
        FeedItem post3;
        FeedItem post4;
        Test.startTest();
        
        system.runAs(testUser1) {
            //create and insert post
            post = new FeedItem();
            post.Body = 'HelloThere';
            post.ParentId = acc.Id;
            post.Title = 'FileName';
            insert post;            
            postId = post.Id;

            post2 = new FeedItem();
            post2.Body = 'HelloThere';
            post2.ParentId = acc.Id;
            post2.Title = 'FileName';
            insert post2;            
            postId2 = post2.Id;
                
            try{        
                delete post;
            }catch(Exception e){System.debug('Exception: ' + e);}
        }
        
        system.runAs(testUser2) {
            //create and insert post
            post3 = new FeedItem();
            post3.Body = 'HelloThere';
            post3.ParentId = acc.Id;
            post3.Title = 'FileName';
            insert post3;            
            postId3 = post3.Id;

            post4 = new FeedItem();
            post4.Body = 'HelloThere';
            post4.ParentId = acc.Id;
            post4.Title = 'FileName';
            insert post4;            
            postId4 = post4.Id;
            
            try{        
                delete post3;
                delete post2; //should not be able to
            }catch(Exception e){System.debug('Exception: ' + e);}
                        
        }   
        
        system.runAs(testUser1) {
            try{        
                delete post4; //should be able to
            }catch(Exception e){System.debug('Exception: ' + e);}
        }
        
        Test.stopTest();
        
        List<FeedItem> post1List = [Select id, title from FeedItem where id = :postId];
        List<FeedItem> post2List = [Select id, title from FeedItem where id = :postId2];
        List<FeedItem> post3List = [Select id, title from FeedItem where id = :postId3];
        List<FeedItem> post4List = [Select id, title from FeedItem where id = :postId4];
                                
        System.assertEquals(post1List.size(), 0);                   
        System.assertEquals(post2List.size(), 1);
        System.assertEquals(post3List.size(), 0);
        System.assertEquals(post4List.size(), 0);
        
    }
    
    
}