@isTest
private class ExpComm_KnowledgeArticlePageCtrl_Test {
    
    @isTest static void test_page_initailization1() {
        String articleId = articlesSetup1();

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?id=' + articleId);
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();

    }

    @isTest static void test_page_initailization2() {
        String articleId = articlesSetup2();

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?id=' + articleId);
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();
        ctrl.getNewRatingScore();
        ctrl.downVoteCurrentArticle();
    }

    @isTest static void test_page_initailization3() {
        String articleId = articlesSetup3();

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?id=' + articleId);
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();

        ctrl.upVoteCurrentArticle();
        ctrl.getNewRatingScore();
    }
    
    @isTest static void test_page_initailization4() {
        String articleId = articlesSetup4();

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?vid=' + articleId);
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();
    }

    @isTest static void test_page_initailization_error1() {

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?id=');
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();
    }

    @isTest static void test_page_initailization_error2() {

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?vid=');
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();
    }

    @isTest static void test_page_initailization_error3() {

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?id=123456');
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();
    }

    @isTest static void test_page_initailization_error4() {

        PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleDetailPage?vid=123456');
        Test.setCurrentPageReference(pg);
        ExpComm_KnowledgeArticleDetailController ctrl = new ExpComm_KnowledgeArticleDetailController();
    }

    @isTest static void test_page_related_component1() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            ExpComm_FeaturedArticlesCmpController ctrl = new ExpComm_FeaturedArticlesCmpController();
            String userLang = ctrl.userLanguage;
        }
    }

    @isTest static void test_page_related_component2() {

        articlesSetup1();
        ExpComm_SearchArtiAutoCompleteController ctrl = new ExpComm_SearchArtiAutoCompleteController();
        ctrl.searchKeyword = 'testing';
        ctrl.selectedArticleVersionId = '';
        ctrl.selectedKnowledgeArticleId = '';

        List<Employee_Article__kav> testArtiList = ExpComm_SearchArtiAutoCompleteController.searchArticles('testing', null);
        testArtiList = ExpComm_SearchArtiAutoCompleteController.searchArticles('testing', 'BackUp__c');
    }

    @isTest static void test_filter_search_page1() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleFilterSearch');
            Test.setCurrentPageReference(pg);
            ExpComm_SearchArtiFilterPageController ctrl = new ExpComm_SearchArtiFilterPageController();
            String searchKeyword = ctrl.searchKeyword;
            ctrl.searchKeyword = 'testing';
            ctrl.searchKeyword = 'Enter your seaching keyword here';
            String dataCategory = 'BackUp__c';
            String articleTypes = 'Employee_Article__kav';
            String sorting = 'ASC';
            Integer pageSize = 10;
            Integer currentPageNumber = 1;
            dataCategory = ctrl.dataCategory;
            articleTypes = ctrl.articleTypes;
            sorting = ctrl.sorting;
            pageSize = ctrl.pageSize;
            currentPageNumber = ctrl.currentPageNumber;

            String userLanguage = ctrl.userLanguage;
        }
    }

    @isTest static void test_filter_search_page2() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            PageReference pg = new PageReference('/apex/ExpComm_KnowledgeArticleFilterSearch?searchTerm=testing');
            Test.setCurrentPageReference(pg);
            ExpComm_SearchArtiFilterPageController ctrl = new ExpComm_SearchArtiFilterPageController();
        }
    }

    private static String articlesSetup1() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        String articleId;

        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User u = new User(Alias = 'stdtest', Email='test@test.in.experian.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testing123@test.in.experian.com');
            insert u;

            Employee_Article__kav newEmpArti = new Employee_Article__kav (
                Title = 'testing article',
                UrlName = 'testing',
                Language = 'en_US'
            );

            insert newEmpArti;
            newEmpArti = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :newEmpArti.Id];
            
            articleId = newEmpArti.KnowledgeArticleId;
            KbManagement.PublishingService.publishArticle(articleId, true);

            Employee_Article__DataCategorySelection artiDataCate = new Employee_Article__DataCategorySelection(
                DataCategoryGroupName = 'Experian_Internal',
                DataCategoryName = 'BackUp',
                ParentId = newEmpArti.Id
            );

            insert artiDataCate;
        }
        return articleId;
    }

    private static String articlesSetup2() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        String articleId;

        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User u = new User(Alias = 'stdtest', Email='test@test.in.experian.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testing123@test.in.experian.com');
            insert u;

            Employee_Article__kav newEmpArti = new Employee_Article__kav (
                Title = 'testing article',
                UrlName = 'testing',
                Language = 'en_US',
                Case_Creation_Target_System__c = 'Salesforce'
            );

            insert newEmpArti;
            newEmpArti = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :newEmpArti.Id];
            
            articleId = newEmpArti.KnowledgeArticleId;
            KbManagement.PublishingService.publishArticle(articleId, true);

            Employee_Article__DataCategorySelection artiDataCate = new Employee_Article__DataCategorySelection(
                DataCategoryGroupName = 'Experian_Internal',
                DataCategoryName = 'BackUp',
                ParentId = newEmpArti.Id
            );

            insert artiDataCate;
        }
        return articleId;
    }

    private static String articlesSetup3() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        String articleId;

        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User u = new User(Alias = 'stdtest', Email='test@test.in.experian.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testing123@test.in.experian.com');
            insert u;

            Employee_Article__kav newEmpArti = new Employee_Article__kav (
                Title = 'testing article',
                UrlName = 'testing',
                Language = 'en_US',
                Case_Creation_Target_System__c = 'Service Central',
                Service_Central__c = 'test form'
            );

            insert newEmpArti;
            newEmpArti = [SELECT KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :newEmpArti.Id];
            
            articleId = newEmpArti.KnowledgeArticleId;
            KbManagement.PublishingService.publishArticle(articleId, true);

            Employee_Article__DataCategorySelection artiDataCate = new Employee_Article__DataCategorySelection(
                DataCategoryGroupName = 'Experian_Internal',
                DataCategoryName = 'BackUp',
                ParentId = newEmpArti.Id
            );

            insert artiDataCate;

            ServiceNow_FormID__c cs = new ServiceNow_FormID__c (Name = 'abctest', catelog__c = 'test form');
            insert cs;
        }
        return articleId;
    }

    private static String articlesSetup4() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        String articleId;
        String articleVersionId;

        System.runAs (thisUser) {
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User u = new User(Alias = 'stdtest', Email='test@test.in.experian.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testing123@test.in.experian.com');
            insert u;

            Employee_Article__kav newEmpArti = new Employee_Article__kav (
                Title = 'testing article',
                UrlName = 'testing',
                Language = 'en_US'
            );

            insert newEmpArti;
            newEmpArti = [SELECT Id, KnowledgeArticleId FROM Employee_Article__kav WHERE Id = :newEmpArti.Id];
            
            articleId = newEmpArti.KnowledgeArticleId;
            articleVersionId = newEmpArti.Id;
            KbManagement.PublishingService.publishArticle(articleId, true);

            Employee_Article__DataCategorySelection artiDataCate = new Employee_Article__DataCategorySelection(
                DataCategoryGroupName = 'Experian_Internal',
                DataCategoryName = 'BackUp',
                ParentId = newEmpArti.Id
            );

            insert artiDataCate;
        }
        return articleVersionId;
    }
}