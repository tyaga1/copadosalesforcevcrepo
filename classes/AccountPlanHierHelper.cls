/**=====================================================================
 * Experian, Inc
 * Name:          AccountPlanHierHelper
 * Created Date:  Mar 19, 2016
 * Created By:    Tyaga Pati (Experian)
 * Description:   This is a Helper Class Called from the AccountPlanHierController Class to take a selected List of Account
                  Ids as input and return a Html string to be used to display the hierarchy structure. 
 *                    
 * Date Modified             Modified By                  Description of the update
 * Mar 19, 2016              Tyaga Pati(Experian)    
 * May 10, 2016              Tyaga Pati(Experian)        01959705 - Updated Html section and Query to make sure Region and Country can be added Account Hierarchy List view.
 
 =====================================================================*/ 
public class AccountPlanHierHelper{
  
  public static Set<Id> parentIds = new Set<Id>();
  public static Integer AccountHierTotlCount;
  public Map<Id,Account> mapIdToAccountshare = new Map<Id,Account>();
  public static Set<Id> masterAccntIds = new Set<Id>();
  public static Map<Id, Integer> mapOrder = new Map<Id, Integer>();
  public static Map<Integer,Id> mapLevelId = new Map<Integer,Id>();
  public static String outputTree = '';
  public static Integer MaxLevel =0;
   
  public class AriaHierarchy {
    public Account acnt {get; set;}
    public Integer level {get; set;}
    public List<Account> childAcnts {get; set;}
    public Account ParentAccnt {get; set;}
    public Integer ParentLvl {get;set;}
    /* Constrcutor */
    public AriaHierarchy(Account acnt, Integer level, List<Account> childAcnts) {
      this.acnt = acnt;
      this.level = level;
      this.childAcnts = childAcnts;
    }
  }
 
  /*
  * @ Description : Method used to create a Account tree hierarcgy
  * @ Param Account List list of master
  * @ return String  Generated Account tree hieraychy 
  */
  public static String createPlanTreeHierarchy(List<Account> masterAcntList, Id UltimateParentId) {
    for (Account Acount : masterAcntList) {
      // Create a set of master plans Iterate through the complete Account List and Add the Ids to a List. This List Created above.
      masterAccntIds.add(Acount.Id);
    }
    Map<Id, Account> mapIdToAccount = new Map<Id, Account>();
    Map<Id, Id> mapChildToParent = new Map<Id, Id>();  
    Map<Id, Set<Id>> mapPlansRelationshipIds = new Map<Id, Set<Id>>();
    
    
    for (Account ac : ([SELECT Id ,Ultimate_Parent_Account__c,Region__C, Country_of_Registration__c, Ultimate_Parent_Account_ID__c, parentId, Name 
                        FROM Account  
                        WHERE Ultimate_Parent_Account__c = :UltimateParentId
                        OR Id = :UltimateParentId])) {
      if (!mapIdToAccount.containsKey(ac.ID)) {
        mapIdToAccount.put(ac.ID, ac);
      }                                             
    }
    for (Account AllAccntsInHier : mapIdToAccount.values()) {
      if (AllAccntsInHier.parentId != null) {
        mapChildToParent.put (AllAccntsInHier.Id, AllAccntsInHier.parentId);
        if (!mapPlansRelationshipIds.containsKey(AllAccntsInHier.parentId)) {
          mapPlansRelationshipIds.put(AllAccntsInHier.parentId, new Set<Id>());
        }
        mapPlansRelationshipIds.get(AllAccntsInHier.parentId).add(AllAccntsInHier.Id);
      }
    }
    
    Map<Integer, List<AriaHierarchy>> mapTreeOrder = new Map<Integer, List<AriaHierarchy>> ();
    Map<Integer,List<AriaHierarchy>> MapForHtmlBuild = new Map<Integer,List<AriaHierarchy>> ();
    
    MapForHtmlBuild = createMapTreeOrder(masterAcntList,mapPlansRelationshipIds,mapChildToParent,mapIdToAccount,UltimateParentId);
    
    return CreateTreeView(MapForHtmlBuild,mapOrder,mapIdToAccount,mapPlansRelationshipIds);
  }

/* TP: NOT NEEDED FOR THIS IMMEDIATE RELEASSE,
//========================================================//        
/*     
* @ Description : Method used to create a list of All Accounts that 
    are below the account from which plan creation was initiated. 
//========================================================//         
       
 public Static List<Account> createChildAccountList(Map<Id,Account> mapIdToAccount, Id acntIdtemp){         
     Map<Id,Id> mapChildToParent = New Map<Id,Id>();        
     Map<Id,Set<Id>> mapPlansRelationshipIds = New Map<Id,Set<Id>>();                   
     for (Account AllAccntsInHier : mapIdToAccount.values()) {      
          if (AllAccntsInHier.parentId != null) {       
              mapChildToParent.put (AllAccntsInHier.Id, AllAccntsInHier.parentId);      
              if (!mapPlansRelationshipIds.containsKey(AllAccntsInHier.parentId)){      
                  mapPlansRelationshipIds.put(AllAccntsInHier.parentId, new Set<Id>());     
                        
              }     
              mapPlansRelationshipIds.get(AllAccntsInHier.parentId).add(AllAccntsInHier.Id);        
          }     
      }     
            
     //Get the Entry in the mapPlansRelationshipIds corresponding to the acntIdtemp i:e the Account Which Initiated the plan creation       
     Set<Id> allDirectParentLst= mapPlansRelationshipIds.get(acntIdtemp);       
     //Iterate through this List to get all the Child elements from the mapChildToParent Map        
            
     List<Account> Test = [Select Id from Account Where Id in :mapChildToParent.KeySet()];         
    return Test ;       
  }     

*/ 
//REMOVE THIS AFTER THIS RELEASE TO RETEST BIG ACCOUNT FUNCTIONALITY 



  /*
  * @ Description : Method used to create a map of hierarchy level to its elements
  * @Param masterAcntsList list of master plans
  * @Param mapPlansRelationshipIds map of parent plan id to set of child ids
  * @Param mapChildToParent map of child id to its parent id
  * @ return Map<Integer, List<AccountHierarchy>>  map of hierarchy level to its elements
  */
  public static Map<Integer, List<AriaHierarchy>> createMapTreeOrder(List<Account> masterAcntList, 
                                           Map<Id, Set<Id>>  mapPlansRelationshipIds, Map<Id,Id> mapChildToParent, Map<Id,Account> mapIdToAccount, Id UltimateParentId) {
    Map<Integer, List<AriaHierarchy>> mapTreeOrder = new Map<Integer, List<AriaHierarchy>>();
    for (String s : mapPlansRelationshipIds.KeySet()) {
      system.debug ('++++++++++each key '+s) ;
    }
    system.debug('***mapPlansRelationshipIds.KeySet().size*()**'+mapPlansRelationshipIds.KeySet().size());
    Integer tempLevel;
    Integer Totallvl =0;
    for (String s : mapPlansRelationshipIds.KeySet()) {
      system.debug ('++++++++++each key '+s) ;
      Totallvl ++;          
    }
    
    system.debug('***** the List of'+mapPlansRelationshipIds);
    
    for (Id AcnntId : mapPlansRelationshipIds.KeySet()) { 
      Account Acnt1 = mapIdToAccount.get(AcnntId );
      Id ParentId = mapChildToParent.get(AcnntId);                     
      if (!mapOrder.containskey(AcnntId)) {
        if (AcnntId == UltimateParentId) {
          mapOrder.put(AcnntId,1);
          parentIds.add(AcnntId);
        }             
        else if (!mapOrder.containskey(ParentId)) {
          Integer count = 0;
          Integer Iteration = 1;
          Id tempParentId = ParentId;
          //Below Loop is for finding out current account is below how many Levels from Ultimate Parent.     
          do {
            Iteration++;
            if (tempParentId == UltimateParentId) {     
              count = Totallvl;
            }
            else {
              tempParentId = mapIdToAccount.get(tempParentId).parentid; 
              count++;
            }
          } while (count < Totallvl);
          mapOrder.put(AcnntId,Iteration);
        }
        else if (mapOrder.containskey(ParentId)) {
          tempLevel = mapOrder.get(ParentId);
          mapOrder.put(AcnntId,tempLevel+1);
          parentIds.add(AcnntId);          
        }
      }//End of mapOrder Map.
    }
    
    //Create a new map to Hold the Level to Id mapping
    
    for (Id AccID: mapOrder.keyset()) {
      mapLevelId.put(mapOrder.get(AccID),AccID);
    }

    Map<Integer,List<AriaHierarchy>> MapForHtmlBuild = new Map<Integer,List<AriaHierarchy>>();
    for (Id acId:  mapOrder.keyset()) {
      AriaHierarchy AcntHierTree = new AriaHierarchy(mapIdToAccount.get(acId), mapOrder.get(acId), createChildPlans(mapPlansRelationshipIds.get(acId),mapIdToAccount));
      if (!MapForHtmlBuild.containskey(mapOrder.get(acId))) {
        MaxLevel++;
        MapForHtmlBuild.put(mapOrder.get(acId),new List<AriaHierarchy>());
      }
      MapForHtmlBuild.get(mapOrder.get(acId)).add(AcntHierTree);
    }

    system.debug('Tyaga the final map for html build is' + MapForHtmlBuild );
    return MapForHtmlBuild;
  }

  /*
  * @ Description : Method used to put child tree into the parent tree
  * @Param mapTreeOrder  map of hierarchy level to its elements
  * @ return String Generated tree html 
  */
  @TestVisible private static String CreateTreeView(Map<Integer, List<AriaHierarchy>> MapForHtmlBuild,Map<Id,Integer> mapOrder1,Map<Id,Account> mapIdToAccount,Map<Id,Set<Id>> mapPlansRelationshipIds) {
    Map<Id,String> IdTohtmltree = new Map<Id,String>();
    Integer Inte;
    String OutputtreeFinal ='';
    Map<Id,String> LovTreeMapToUseNxtLvl = new Map<Id,String>();
    Map<Id,Account> LoweLevelParentAcc = new Map<Id,Account>();
    String OutPuttreeForAccHierObj = '';
    for (Inte = MaxLevel; inte > 0; inte--) {
      for (AriaHierarchy formattedPlan : MapForHtmlBuild.get(Inte)) {
        String TreeString1st = '';
        String TreeStringTwo = '';
        system.debug('The Child Account Set for Iteration :' + Inte +' :is :' + formattedPlan.childAcnts );   
        String OutPuttreeForAccHierObjtemp = '';
        List<Account> NewChildAcntSet = new List<Account>(formattedPlan.childAcnts);
        List<Account> OldChildAcntSet = new List<Account>();
        if (LovTreeMapToUseNxtLvl.keyset().size() > 0) {
          NewChildAcntSet.clear();
          OldChildAcntSet.clear();
          for (Account accnt12:formattedPlan.childAcnts) {
            system.debug('Tyaga for iteration :' + Inte +'it came in here' + accnt12.Name );        
            if (!LovTreeMapToUseNxtLvl.containskey(accnt12.Id)) {
              NewChildAcntSet.add(accnt12);  
              system.debug('Tyaga for iteration :' + Inte +'it came in here' + accnt12.Name + 'the set is' + NewChildAcntSet );   
            }
            else {
              OldChildAcntSet.add(accnt12);
            }
          }
        }
        if (NewChildAcntSet.size() > 0) {
          String childTree;
          if (NewChildAcntSet.size() > 0) {
            childTree = childTreeStringCreator(NewChildAcntSet,mapIdToAccount);
            system.debug('For Iteration' + Inte + 'and Accountset : '+ NewChildAcntSet  + ' : the childtree generated is' + childTree );
          }
          TreeString1st = childTree;
        }//End of if that checks if Id is at lower level
        if (OldChildAcntSet.size() > 0) {
          for (Account accntr:OldChildAcntSet) {
            if (LovTreeMapToUseNxtLvl.containskey(accntr.Id)) {
              system.debug('tyaga the the 1st is 11111:'+ OutPuttreeForAccHierObj); 
              OutPuttreeForAccHierObjtemp += LovTreeMapToUseNxtLvl.get(accntr.Id);
              system.debug(' ****** For this iteration '+ inte + ' : is : '+ LovTreeMapToUseNxtLvl.get(accntr.Id));   
              LovTreeMapToUseNxtLvl.remove(accntr.Id);
            }
          }
          TreeStringTwo = OutPuttreeForAccHierObjtemp;
          system.debug('******* the temp string is :' + OutPuttreeForAccHierObjtemp);
        }
        system.debug('**** for this iteration :' + inte + 'the value of string 1 is :' + TreeString1st + 'and the value of strign 2 is :' +TreeStringTwo  );
      
        if (TreeString1st != null || TreeStringTwo != null) {
          String TempOutputString =  TreeStringTwo + TreeString1st ;
          //OutPuttreeForAccHierObj = '<li style="white-space:nowrap;" id="masterplan" ><span  class="icon-close"></span><input onclick="enableChildPlans(this.id,\'tree\')" type="checkbox" id="checkbx-'+formattedPlan.acnt.Id+'" /><b onclick="enableCheckbox(this.id)" id="text-'+formattedPlan.acnt.Id+'">'+formattedPlan.acnt.Name+' ('+formattedPlan.acnt.Region__c+')'+'  ('+formattedPlan.acnt.Country_of_Registration__c+')'+</b><ul>'+ TempOutputString +'</ul></li>';
          OutPuttreeForAccHierObj = '<li style="white-space:nowrap;" id="masterplan" ><span  class="icon-close"></span><input onclick="enableChildPlans(this.id,\'tree\')" type="checkbox" id="checkbx-'+formattedPlan.acnt.Id+'" /><b onclick="enableCheckbox(this.id)" id="text-'+formattedPlan.acnt.Id+'">'+formattedPlan.acnt.Name+'  ('+formattedPlan.acnt.Region__c+')'+'  ('+formattedPlan.acnt.Country_of_Registration__c+')'+'</b><ul>'+ TempOutputString +'</ul></li>';
          system.debug(' : ****** for the current Iteration :' + Inte + ' : the string generated is : ' +  TempOutputString );
        }
        //End of Section Created today
         
        if (!LovTreeMapToUseNxtLvl.containskey(formattedPlan.acnt.Id)) {
          LovTreeMapToUseNxtLvl.put(formattedPlan.acnt.Id,OutPuttreeForAccHierObj);
          system.debug('****+++++ The map after iteration :'  +  Inte +' : Is : ' +LovTreeMapToUseNxtLvl.get(formattedPlan.acnt.Id)+ ' : and key is : ' + formattedPlan.acnt.Id);
        }
      }
    }
    outputTree = OutPuttreeForAccHierObj;
    system.debug('*****The output tree is: '+ OutPuttreeForAccHierObj);
    return outputTree; 
  }

  /*
  * @ Description :Tyaga Method used to create a child tree for the parent plan
  * @Param areaPlansList  list of supplemental plans
  * @ return String child tree html
  */
  @TestVisible private static String childTreeStringCreator(List<Account> ChildAccSet, Map<Id,Account> mapIdToAccount) {
    String childTree = '';
    if (ChildAccSet != null) {
      childTree += '<ul>';
      for (Account accIter : ChildAccSet) {
        if (accIter != null) {
        childTree +=  '<li style="white-space:nowrap;">'+
                                  (parentIds.contains(accIter.Id) ? '<span class="icon-close"></span>' : '<span class="icon-close" style="visibility:hidden;" ></span>')+
                                  '<input onclick="enablePlan(this.id,\'tree\')" type="checkbox" id="checkbx-'+accIter.Id+'" />'+
                                  '<span class="plan" onclick="enableCheckbox(this.id)" id="text-'+accIter.Id+'">'+accIter.Name+' ('+accIter.Region__c +') '+' ('+accIter.Country_of_Registration__c+') '+'</span>'+
                                  //'<span class="plan" onclick="enableCheckbox(this.id)" id="text-'+accIter.Id+'">'+accIter.Name+'</span>'+
                                  '<Label style="display:none;margin-left:2px;" id="label-'+accIter.Id+'">('+ accIter.Name+ ')('+ accIter.Region__C+ ')('+ accIter.Country_of_Registration__c+ ')</Label></li>';
        }   
      } 
      childTree += '</ul>';
    } 
    return childTree;
  }

  /*
  * @ Description : Method used to create a child tree for the parent plan
  * @Param areaPlansList  list of supplemental plans
  * @ return String child tree html
  */
/*  @TestVisible private static String childTreeString(set<Id> ChildAccSet, Map<Id,Account> mapIdToAccount) {
    String childTree = '';
    if (ChildAccSet!= null) {
      childTree += '<ul>';
      for (Id accIter : ChildAccSet) {
        if (accIter != null) {
        Account accountiter = mapIdToAccount.get(accIter);
        childTree +=  '<li style="white-space:nowrap;">'+
                                  (parentIds.contains(accountiter.Id) ? '<span class="icon-close"></span>' : '<span class="icon-close" style="visibility:hidden;" ></span>')+
                                  '<input onclick="enablePlan(this.id,\'tree\')" type="checkbox" id="checkbx-'+accountiter.Id+'" />'+
                                  '<span class="plan" onclick="enableCheckbox(this.id)" id="text-'+accountiter.Id+'">'+accountiter.Name+'</span>'+
                                   '<Label style="display:none;margin-left:2px;" id="label-'+accountiter.Id+'">('+ accountiter.Name   + ')</Label></li>';
        }   
      } 
      childTree += '</ul>';
    } 
    return childTree;
  }
*/
  @TestVisible private static List<Account> createChildPlans(Set<Id> childAcntIds, Map<Id, Account> mapIdToAccount) {
    List<Account> childAcnts = new List<Account>();
    if (childAcntIds != null) {
      for (Id AcntId : childAcntIds) {
        childAcnts.add(mapIdToAccount.get(AcntId));
      }
    }
    return childAcnts;
  }
}