/**=====================================================================
 * Experian
 * Name: ITCABannerInfoClass
 * Description: This Class is needed to transfer data from the Navigation Banner VF Component to the calling VF Page and then to the My Journey Map VF Component.
 *              Transfer of data via this class is necessary since it is possible to request navigation to the Profile section of the ITCA Home Page from any of the other ITCA
 *              VF Pages.  A cookie is also used to ensure that the correct tab is selected.  The cookie is set via Javascript on the Navigation Banner VF component.
 * Created Date: 18th August 2017
 * Created By: James Wills
 *
 * Date Modified      Modified By           Description of the update
 * 1st Sept. 2017     James Wills           Tidied up class.
 * =====================================================================*/
public virtual class ITCABannerInfoClass {

  public class employeeSkillDisplay{   
    public String  skillName           {get;set;}
    public String  skillId             {get;set;}
    public Boolean sfiaSkill           {get;set;}
    public Integer curentSkillValue    {get;set;}
    public Integer maxSkillValue       {get;set;}
    public Double  skillAccPercentage  {get;set;}
    public Integer endorsementReceived {get;set;}
    public Boolean ismanagerApproved   {get;set;}
    
    //default method to formate current user skill.
    public employeeSkillDisplay(Career_Architecture_Skills_Plan__c careerArchSkillPlan, Map<String, Decimal> maxSkillLevels){
    
      this.skillId              = careerArchSkillPlan.Skill__r.Id;
      this.skillName            = careerArchSkillPlan.Skill__r.Name;
      this.sfiaSkill            = careerArchSkillPlan.Skill__r.sfia_Skill__c;//ITCA:W-009246
      this.curentSkillValue     = (Integer)careerArchSkillPlan.Level_In_Number__c;
      this.maxSkillValue        = (Integer) maxSkillLevels.get(careerArchSkillPlan.Skill__r.Id);
      
      if (maxSkillValue > 0){
        this.skillAccPercentage = (((Double)curentSkillValue/(Double)maxSkillValue) *100);
      } else {
        this.skillAccPercentage = 0;
      }
    }    
        
    //Used in SkillSet and CareerArea select options 
    public employeeSkillDisplay(AggregateResult skillforCurrentSkillSelRec, Map<String, Decimal> currentUserSkillMap){
    
      this.skillId              = (String)skillforCurrentSkillSelRec.get('Id');
      this.skillName            = (String)skillforCurrentSkillSelRec.get('name');
      this.maxSkillValue        = (Integer)((decimal) skillforCurrentSkillSelRec.get('expr0') );
      
      if (currentUserSkillMap.containsKey((string)skillforCurrentSkillSelRec.get('Id')) && maxSkillValue >0){
        this.curentSkillValue   = (Integer)currentUserSkillMap.get((string)skillforCurrentSkillSelRec.get('Id'));
        this.skillAccPercentage = (((Double)curentSkillValue/(Double)maxSkillValue) *100) ;
      } else {
        this.curentSkillValue   = 0;
        this.skillAccPercentage = 0;
      }

    }    
        
    //Used in match top 5 skill set.
    public employeeSkillDisplay(AggregateResult skillforCurrentSkillSelRec, Map<String, Integer> currentUserSkillMap){
      
      this.skillId              = (String)skillforCurrentSkillSelRec.get('Id');
      this.skillName            = (String)skillforCurrentSkillSelRec.get('name');
      this.curentSkillValue     = (Integer)((Decimal) skillforCurrentSkillSelRec.get('expr0') );
      this.maxSkillValue        = currentUserSkillMap.get((String)skillforCurrentSkillSelRec.get('name'));
            
      if(maxSkillValue > 0){
        this.skillAccPercentage = (((Double)curentSkillValue/(Double)maxSkillValue) *100);
      } else {
        this.skillAccPercentage = 0;
      }
    }              
  }


  //Compare Starts  
  public class employeeSkillDisplaySort implements Comparable {

    public ITCABannerInfoClass.employeeSkillDisplay employeeSkillDisplayRec;
    
    // Constructor
    public employeeSkillDisplaySort (ITCABannerInfoClass.employeeSkillDisplay empDispRec) {
        employeeSkillDisplayRec = empDispRec ;
    }
    
    // Compare opportunities based on the opportunity amount.
    public Integer compareTo(Object compareTo) {

      // Cast argument to OpportunityWrapper
      employeeSkillDisplaySort compareToemployeeSkillDisplayRec = (employeeSkillDisplaySort)compareTo;
        
      // The return value of 0 indicates that both elements are equal.
      Integer returnValue = 0;
      
      //if(employeeSkillDisplayRec.sfiaSkill==true){//ITCA:W-009246
        if(employeeSkillDisplayRec.skillAccPercentage > compareToemployeeSkillDisplayRec.employeeSkillDisplayRec.skillAccPercentage){
          // Set return value to a positive value.
          returnValue = -1;
        } else if (employeeSkillDisplayRec.skillAccPercentage < compareToemployeeSkillDisplayRec.employeeSkillDisplayRec.skillAccPercentage ) {
          // Set return value to a negative value.
          returnValue = 1;
        }
        
       /*} else if(employeeSkillDisplayRec.sfiaSkill==false){
        if (employeeSkillDisplayRec.skillAccPercentage > compareToemployeeSkillDisplayRec.employeeSkillDisplayRec.skillAccPercentage ) {
          // Set return value to a positive value.
          returnValue = -1;
        } else if (employeeSkillDisplayRec.skillAccPercentage < compareToemployeeSkillDisplayRec.employeeSkillDisplayRec.skillAccPercentage ) {
          // Set return value to a negative value.
          returnValue = 1;
        }       
       }*/
        
      return returnValue;       
    }
  }
  //Compare Ends
  
  public List<Career_Architecture_User_Profile__c> currentUserProfile {get{ return new List<Career_Architecture_User_Profile__c>([SELECT id, Employee__r.Name, role__c, State__c, Status__c, 
                                                                                                                Manager_Comments__c, Date_Discussed_with_Employee__c
                                                                                                                FROM Career_Architecture_User_Profile__c 
                                                                                                                WHERE State__c = 'Current' AND Employee__c = :UserInfo.getUserId() LIMIT 1]);
  } set;}

  public List<Career_Architecture_Skills_Plan__c> casp_List {get{ return new list<Career_Architecture_Skills_Plan__c>([SELECT Id, Skill__r.Id, Skill__r.Name, Skill__r.sfia_Skill__c,
                                                                                                      Skill__r.Level1__c,Skill__r.Level2__c,
                                                                                                      Skill__r.Level3__c,skill__r.Level4__c,Skill__r.Level5__c,
                                                                                                      Skill__r.Level6__c,Skill__r.Level7__c,
                                                                                                      Level__c, Experian_Skill_Set__r.Name, Experian_Skill_Set__r.Career_Area__c,
                                                                                                      Career_Architecture_User_Profile__c,
                                                                                                      Skill__r.Max_Skill_Level__c
                                                                                                      FROM Career_Architecture_Skills_Plan__c 
                                                                                                      WHERE Career_Architecture_User_Profile__c 
                                                                                                      IN :currentUserProfile
                                                                                                      ORDER BY Skill__r.Name]);} set;}
  
  
  public void isSummaryBuild(){
            
    //Get the current users skills form their current profile
    List<Career_Architecture_Skills_Plan__c> currentUserSkills= new List<Career_Architecture_Skills_Plan__c>([SELECT skill__r.id, Level_In_Number__c, skill__r.name, Skill__r.sfia_Skill__c, Current_Level__c 
                                                                     FROM Career_Architecture_Skills_Plan__c 
                                                                     WHERE Career_Architecture_User_Profile__c IN :currentUserProfile]);        
                                                                           
    //Create a set and Map to list all the skill__r.id from currentUserSkills
    Set<String> currentUserSkillsId          = new Set<String>();
    currentUserSkillLevelMap = new Map<String,Decimal> ();
    
    for (Career_Architecture_Skills_Plan__c currentUserSkillRec : currentUserSkills){
      currentUserSkillsId.add(currentUserSkillRec.skill__r.id);
      currentUserSkillLevelMap.put((String)currentUserSkillRec.skill__r.id , (Decimal) currentUserSkillRec.Level_In_Number__c);            
    }
            
    //Get all information             
    List<Skill_set_to_skill__c> currentUserskillsSelection = new List<Skill_set_to_skill__c>(
                                          [SELECT id, skill__r.id,skill__r.Max_Skill_Level__c,skill__r.Minimum_Skill_Level__c,level__c, skill__r.name 
                                           FROM Skill_set_to_skill__c 
                                           WHERE Skill_set_to_skill__c.skill__r.id IN :currentUserSkillsId 
                                           ORDER BY Skill_set_to_skill__c.skill__r.name ]);         
    
    Map<String, Decimal> maxSkillLevels= new Map<String, Decimal>();
    
    //Get the maximum and minimum Levels     
    for(Skill_set_to_skill__c currentSkillsSelectRec: currentUserskillsSelection){               
      maxSkillLevels.put(currentSkillsSelectRec.Skill__r.Id, currentSkillsSelectRec.skill__r.Max_Skill_Level__c);
    }
    
    for(Career_Architecture_Skills_Plan__c casp : casp_List){
      if(casp.Skill__r.sfia_Skill__c==false){
        maxSkillLevels.put(casp.Skill__r.id, casp.Skill__r.Max_Skill_Level__c);
      }
    }
    
    system.debug(maxSkillLevels.values());
        
    //Set<String> minLevels = minSkillLevels.keySet();
    Set<String> maxLevels = maxskillLevels.keySet();
        
        
    //RJ Added
    employeeSkillDisplaySortList = new List<ITCABannerInfoClass.employeeSkillDisplaySort >();
    for (Career_Architecture_Skills_Plan__c currentUserSkillRec : currentUserSkills){
      //Instantiate employeeSkillDisplay with constructor #1 = (Career_Architecture_Skills_Plan__c careerArchSkillPlan, Map<String, Decimal> maxSkillLevels
      employeeSkillDisplaySortList.add(new ITCABannerInfoClass.employeeSkillDisplaySort(new ITCABannerInfoClass.employeeSkillDisplay(currentUserSkillRec,maxskillLevels) ));       
    }
    employeeSkillDisplaySortList.sort();
    
      
    employeeSkillDisplayList = new List<ITCABannerInfoClass.employeeSkillDisplay>();
            
    for(ITCABannerInfoClass.employeeSkillDisplaySort employeeSkillDisplaySortRec : employeeSkillDisplaySortList){    
      employeeSkillDisplayList.add(employeeSkillDisplaySortRec.employeeSkillDisplayRec );
    }
    //testString= 'Size is ' +  String.valueOf(employeeSkillDisplayList.size());
    /*currentUserMatchedSkillsSelection = new list<AggregateResult>([SELECT Count(ID),Skill__r.Name Skill,Experian_Skill_Set__r.Name 
                                                                   FROM Skill_Set_to_Skill__c 
                                                                   WHERE Skill__r.ID IN :currentUserSkillsId 
                                                                   GROUP BY Skill__r.Name,Experian_Skill_Set__r.Name]);*/
        
    matchSkillSetsAction();
  }
  
  public void matchSkillSetsAction(){
    
    if(currentUserSkillLevelMap.size()>0){
    
       List<AggregateResult> matchedSkillCount = new list<AggregateResult>(
                 [SELECT COUNT_DISTINCT(Skill__r.Name), Experian_Skill_Set__r.Name,Experian_Skill_Set__r.Id 
                  FROM Skill_Set_to_Skill__c 
                  WHERE Skill__r.Id IN :currentUserSkillLevelMap.keyset() 
                  GROUP BY Experian_Skill_Set__r.name,Experian_Skill_Set__r.Id LIMIT 5 ]);

       List<AggregateResult> totalSkillCount = new list<AggregateResult>(
                 [SELECT COUNT_DISTINCT(Skill__r.Name), Experian_Skill_Set__r.Name 
                 FROM Skill_Set_to_Skill__c 
                 GROUP BY Experian_Skill_Set__r.name ]);
                 
       Map<String,Integer> totalSkillSetMatchMap = new map<String,Integer>();
       for(AggregateResult totalSkillCountRec:totalSkillCount){                    
         totalSkillSetMatchMap.put((String)totalSkillCountRec.get('Name'),(Integer)totalSkillCountRec.get('expr0'));
       }
       employeeSkillDisplay_SkillSets_SortList = new List<ITCABannerInfoClass.employeeSkillDisplaySort >();
       for(AggregateResult matchedSkillCountRec : matchedSkillCount){
         //Instantiate employeeSkillDisplay with constructor #3 = (AggregateResult skillforCurrentSkillSelRec, Map<String, Integer> currentUserSkillMap)
         employeeSkillDisplay_SkillSets_SortList.add(new ITCABannerInfoClass.employeeSkillDisplaySort(new ITCABannerInfoClass.employeeSkillDisplay(matchedSkillCountRec,totalSkillSetMatchMap)));
       }
       employeeSkillDisplay_SkillSets_SortList.sort();
            
       employeeSkillDisplay_SkillSets_List = new List<ITCABannerInfoClass.employeeSkillDisplay>();
       employeeSkillDisplay_SkillSets_List.clear();
       for(ITCABannerInfoClass.employeeSkillDisplaySort employeeSkillDisplaySortRec : employeeSkillDisplay_SkillSets_SortList){
         employeeSkillDisplay_SkillSets_List.add(employeeSkillDisplaySortRec.employeeSkillDisplayRec );
       }                           
    }
  }
  
  
  public String currentActiveTab                                                {get;set;}
  
  public List<employeeSkillDisplay>     employeeSkillDisplayList                {get;set;}
  public List<employeeSkillDisplaySort> employeeSkillDisplaySortList            {get;set;} 
  
  public List<employeeSkillDisplay>     employeeSkillDisplay_SkillSets_List     {get;set;}
  public List<employeeSkillDisplaySort> employeeSkillDisplay_SkillSets_SortList {get;set;} 
  
  
  public List<employeeSkillDisplay>     employeeSkillDisplay_SkillComp_List     {get;set;}
  public List<employeeSkillDisplaySort> employeeSkillDisplay_SkillComp_SortList {get;set;} 
  
  public List<employeeSkillDisplay>     employeeSkillDisplay_CarComp_List       {get;set;}
  public List<employeeSkillDisplaySort> employeeSkillDisplay_CarComp_SortList   {get;set;} 
  
  
  public Map<String, Decimal> currentUserSkillLevelMap                          {get;set;}
  
}