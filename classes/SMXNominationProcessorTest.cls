/**=====================================================================
 * Experian
 * Name: SMXNominationProcessorTest
 * Description: Test class for SMXNominationProcessor
 * Created Date: Unknown
 * Created By: Unknown
 *
 * Date Modified     Modified By                  Description of the update
 * Oct 30th,2014     Pallavi Sharma(Appirio)      Fix Failure
 * Mar 15th, 2016    Paul Kissick                 Case 01899325: Improving coverage
=====================================================================*/
@isTest
private class SMXNominationProcessorTest {
  
  private static String contactEmail = 'this.is.a.smx.test@acmedemo.com';

  private static void setTestResponseValues(Integer testCaseNum) {
    if (testCaseNum == 1) {
      SMXNominationProcessor.testHttpStatusCode = 200;
      SMXNominationProcessor.testHttpResponseXML = '<webserviceresponse><code>0</code><description></description><row value="success"></row></webserviceresponse>';
    }
    else if (testCaseNum  == 2) {
      SMXNominationProcessor.testHttpStatusCode = 200;
      SMXNominationProcessor.testHttpResponseXML = '<webserviceresponse><code>0</code><description></description><row value="No Send Rule is applied for the provider"></row></webserviceresponse>';        
    }
    else if (testCaseNum  == 3) {
      SMXNominationProcessor.testHttpStatusCode = 200;
      SMXNominationProcessor.testHttpResponseXML = '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
    }
    else if (testCaseNum  == 4) {
      SMXNominationProcessor.testHttpStatusCode = 404;
      SMXNominationProcessor.testHttpResponseXML = '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
    }      
  }

  @isTest
  static void testFeedbackUpdate() {
    List<Feedback__c> lstFeedback = [
      SELECT Id, Name, Contact__c, DataCollectionId__c, DataCollectionName__c,
             PrimaryScore__c, PrimaryComment__c, Status__c,
             StatusDescription__c,SurveyDetailsURL__c
      FROM Feedback__c
    ];

    for (Feedback__c fbk : lstFeedback) {
      for (Integer i = 1; i <= 4; i++) {
        SMXNominationProcessorTest.setTestResponseValues(i);
        SMXNominationProcessor.processNomination(fbk.Name);
      }
    }
  }
  
  @isTest
  static void testProcessNomination106615() {
    
    SMXNominationProcessorTest.setTestResponseValues(1);
    
    Contact con = [SELECT Id FROM Contact WHERE Email = :contactEmail LIMIT 1];
    
    User tstUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Country__c = 'United Kingdom';
    tstUser.Region__c = 'UK&I';
    insert tstUser;
    
    Case c = new Case(
      OwnerId = tstUser.Id,
      Description = 'This is a test description',
      Subject = 'Test Case'
    );
    insert c;
    Test.setCreatedDate(c.Id, Datetime.now().addHours(-1));
    c.Status = 'Closed';
    update c;
    
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'feedbacktest'+String.valueOf(Math.random()*1000000);
    feedback.Contact__c = con.Id;
    feedback.DataCollectionId__c = SMXNominationProcessor.edqSupportExperience;
    feedback.DataCollectionName__c = 'Test Survey';                     
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedback.Case__c = c.Id;
    
    insert feedback;
    
    Test.startTest();
    
    SMXNominationProcessor.processNomination(feedback.Name);
    
    Test.stopTest();
    
    // SMXNominationProcessor.edqNaSupportExperience = 'EXPERIAN_109584'; = CASE
    // SMXNominationProcessor.naCsClientSupport      = 'EXPERIAN_108224'; = CASE (Support_Team__c)
    // SMXNominationProcessor.naOnboardingMembership = 'EXPERIAN_108225'; = MEMBERSHIP
    // SMXNominationProcessor.edqPurchaseExperience  = 'EXPERIAN_106613'; = OPP
    // SMXNominationProcessor.edqContractRenewal     = 'EXPERIAN_106614'; = ORDER
    
  }
  
  
  @isTest
  static void testProcessNomination109584() {
    
    SMXNominationProcessorTest.setTestResponseValues(1);
    
    Contact con = [SELECT Id FROM Contact WHERE Email = :contactEmail LIMIT 1];
    
    User tstUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Country__c = 'United States of America';
    tstUser.Region__c = 'North America';
    insert tstUser;
    
    Case c = new Case(
      OwnerId = tstUser.Id,
      Description = 'This is a test description',
      Subject = 'Test Case'
    );
    insert c;
    Test.setCreatedDate(c.Id, Datetime.now().addHours(-1));
    c.Status = 'Closed';
    update c;
    
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'feedbacktest'+String.valueOf(Math.random()*1000000);
    feedback.Contact__c = con.Id;
    feedback.DataCollectionId__c = SMXNominationProcessor.edqNaSupportExperience;
    feedback.DataCollectionName__c = 'Test Survey';                     
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedback.Case__c = c.Id;
    
    insert feedback;
    
    Test.startTest();
    
    SMXNominationProcessor.processNomination(feedback.Name);
    
    Test.stopTest();
    
    // SMXNominationProcessor.naCsClientSupport      = 'EXPERIAN_108224'; = CASE (Support_Team__c)
    // SMXNominationProcessor.naOnboardingMembership = 'EXPERIAN_108225'; = MEMBERSHIP
    // SMXNominationProcessor.edqPurchaseExperience  = 'EXPERIAN_106613'; = OPP
    // SMXNominationProcessor.edqContractRenewal     = 'EXPERIAN_106614'; = ORDER
    
  }
  
  @isTest
  static void testProcessNomination108224() {
    
    SMXNominationProcessorTest.setTestResponseValues(1);
    
    Contact con = [SELECT Id FROM Contact WHERE Email = :contactEmail LIMIT 1];
    
    User tstUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Country__c = 'France';
    tstUser.Region__c = 'EMEA';
    insert tstUser;
    
    Case c = new Case(
      OwnerId = tstUser.Id,
      Description = 'This is a test description',
      Subject = 'Test Case',
      Support_Team__c = 'Client Billing Questions'
    );
    insert c;
    Test.setCreatedDate(c.Id, Datetime.now().addHours(-1));
    c.Status = 'Closed';
    update c;
    
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'feedbacktest'+String.valueOf(Math.random()*1000000);
    feedback.Contact__c = con.Id;
    feedback.DataCollectionId__c = SMXNominationProcessor.naCsClientSupport;
    feedback.DataCollectionName__c = 'Test Survey';                     
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedback.Case__c = c.Id;
    
    insert feedback;
    
    Test.startTest();
    
    SMXNominationProcessor.processNomination(feedback.Name);
    
    Test.stopTest();
    
    // SMXNominationProcessor.naOnboardingMembership = 'EXPERIAN_108225'; = MEMBERSHIP
    // SMXNominationProcessor.edqPurchaseExperience  = 'EXPERIAN_106613'; = OPP
    // SMXNominationProcessor.edqContractRenewal     = 'EXPERIAN_106614'; = ORDER
    
  }
  
  @isTest
  static void testProcessNomination108225() {
    
    SMXNominationProcessorTest.setTestResponseValues(1);
    
    Contact con = [SELECT Id FROM Contact WHERE Email = :contactEmail LIMIT 1];
    
    User tstUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Country__c = 'France';
    tstUser.Region__c = 'EMEA';
    insert tstUser;
    
    Membership__c m = new Membership__c(
      OwnerId = tstUser.Id,
      Membership_Welcome_Activity_Sent_date__c = Datetime.now(),
      Contact_Name__c = con.Id
    );
    insert m;
    
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'feedbacktest'+String.valueOf(Math.random()*1000000);
    feedback.Contact__c = con.Id;
    feedback.DataCollectionId__c = SMXNominationProcessor.naOnboardingMembership;
    feedback.DataCollectionName__c = 'Test Survey';                     
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedback.Membership__c = m.Id;
    
    insert feedback;
    
    Test.startTest();
    
    SMXNominationProcessor.processNomination(feedback.Name);
    
    Test.stopTest();
    
    // SMXNominationProcessor.edqPurchaseExperience  = 'EXPERIAN_106613'; = OPP
    // SMXNominationProcessor.edqContractRenewal     = 'EXPERIAN_106614'; = ORDER
    
  }
  
  @isTest
  static void testProcessNomination106613() {
    
    SMXNominationProcessorTest.setTestResponseValues(1);
    
    Contact con = [SELECT Id, AccountId FROM Contact WHERE Email = :contactEmail LIMIT 1];
    
    
    User tstUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Country__c = 'France';
    tstUser.Region__c = 'EMEA';
    insert tstUser;
    
    
    Opportunity o = Test_Utils.createOpportunity(con.AccountId);
    o.CloseDate = Date.today();
    o.OwnerId = tstUser.Id;
    insert o;
    
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'feedbacktest'+String.valueOf(Math.random()*1000000);
    feedback.Contact__c = con.Id;
    feedback.DataCollectionId__c = SMXNominationProcessor.edqPurchaseExperience;
    feedback.DataCollectionName__c = 'Test Survey';                     
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedback.Opportunity__c = o.Id;
    
    insert feedback;
    
    Test.startTest();
    
    SMXNominationProcessor.processNomination(feedback.Name);
    
    Test.stopTest();
    
    // SMXNominationProcessor.edqContractRenewal     = 'EXPERIAN_106614'; = ORDER
    
  }
  
  @isTest
  static void testProcessNomination106614() {
    
    SMXNominationProcessorTest.setTestResponseValues(1);
    
    Contact con = [SELECT Id, AccountId FROM Contact WHERE Email = :contactEmail LIMIT 1];
    
    
    User tstUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Country__c = 'France';
    tstUser.Region__c = 'EMEA';
    insert tstUser;
    
    
    Opportunity o = Test_Utils.createOpportunity(con.AccountId);
    o.CloseDate = Date.today();
    o.OwnerId = tstUser.Id;
    o.Contract_End_Date__c = Date.today();
    insert o;
    
    Order__c ord = Test_Utils.insertOrder(false, con.AccountId, con.Id, o.Id);
    ord.OwnerId = tstUser.Id;
    insert ord;
    
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'feedbacktest'+String.valueOf(Math.random()*1000000);
    feedback.Contact__c = con.Id;
    feedback.DataCollectionId__c = SMXNominationProcessor.edqContractRenewal;
    feedback.DataCollectionName__c = 'Test Survey';                     
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedback.Order__c = ord.Id;
    
    insert feedback;
    
    Test.startTest();
    
    SMXNominationProcessor.processNomination(feedback.Name);
    
    Test.stopTest();
    
  }
     
  @testSetup
  private static void prepareTestData() {
    Global_Settings__c custSettings = Global_Settings__c.getValues('Global');
    if (custSettings == null) {
      custSettings = new Global_Settings__c(name= Constants.GLOBAL_SETTING ,Account_Team_Member_Default_Role__c= Constants.TEAM_ROLE_ACCOUNT_MANAGER);
      insert custSettings;
    }
    
    Account a = Test_Utils.createAccount();
    a.Name = 'SMX Test Account';
    insert a; 
    
    Contact c = Test_Utils.createContact(a.Id); 
    c.FirstName = 'SMX TestFName1';
    c.LastName = 'SMX TestLName1';
    c.Email = contactEmail;
    c.Phone = '9999999';
    insert c;    
    c.HasOptedOutOfEmail = false;                    
    update c;
        
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Feedback__c feedback = new Feedback__c(
      Name = 'TESTCRM12345',
      Contact__c = c.Id,
      DataCollectionId__c = 'SPARTASYSTEMS_126684',
      DataCollectionName__c = 'Test Survey',
      PrimaryScore__c = 9,
      PrimaryComment__c = 'Test comment',
      Status__c = 'Test Status',
      StatusDescription__c = 'Test Description',
      SurveyDetailsURL__c = ''
    );
    feedbackList.add(feedback);  
    insert feedbackList;    
  }  
      
}