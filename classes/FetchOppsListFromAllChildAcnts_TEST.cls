/**=====================================================================
 * Name: FetchOppsListFromAllChildAcnts_TEST
 * Description: Test Class for FetchOppsListFromAllChildAcnts
 * Created Date: August 9th, 2016
 * Created By: Cristian Torres
 * 
 * Date Modified          Modified By           Description of the update
 * Sep 6th, 2016          Paul Kissick          Fixed tests to actually test!
 * Oct 25th, 2016         Manoj Gopu            CRM2.0 W-005616 Updated test class to cover exportToExcel Method      
 * =====================================================================*/
@isTest
private class FetchOppsListFromAllChildAcnts_TEST {
    
  static testMethod void oppsListTest1() {
    //Create and insert a new Account  
    Account acc1 = Test_Utils.insertAccount();
    // Create and insert a contact
    Contact con = Test_Utils.insertContact(acc1.Id);
        
    //Create one child account
    Account acc1A = Test_Utils.createAccount();
    acc1A.Name = 'TEST Child Account';
    acc1A.ParentId = acc1.ID;
    insert acc1A;
        
    //Create one child account
    Account acc1A_A = Test_Utils.createAccount();
    acc1A_A.Name = 'TEST Child Account';
    acc1A_A.ParentId = acc1A.ID;
    insert acc1A_A;
        
   //Create one child account
    Account acc1A_B = Test_Utils.createAccount();
    acc1A_B.Name = 'TEST Child Account';
    acc1A_B.ParentId = acc1A.ID;
    insert acc1A_B;
        
    // Create an opportunity
    Opportunity opp1 = Test_Utils.createOpportunity(acc1A.Id);
    opp1.Contract_Start_Date__c = Date.today().addDays(-20);
    opp1.Contract_End_Date__c = Date.today().addDays(10);
    opp1.CloseDate = Date.today().addDays(5);
    insert opp1;
        
    // Create an opportunity
    Opportunity opp2 = Test_Utils.createOpportunity(acc1A_A.Id);
    opp2.Contract_Start_Date__c = Date.today().addDays(-20);
    opp2.Contract_End_Date__c = Date.today().addDays(10);
    opp2.CloseDate = Date.today().addDays(5);
    insert opp2;
        
    // Create an opportunity
    Opportunity opp3 = Test_Utils.createOpportunity(acc1A_B.Id);
    opp3.Contract_Start_Date__c = Date.today().addDays(-20);
    opp3.Contract_End_Date__c = Date.today().addDays(10);
    opp3.CloseDate = Date.today().addDays(5);
    insert opp3;
    
    Test.startTest();
        
    FetchOppsListFromAllChildAcnts oppsList = new FetchOppsListFromAllChildAcnts(new ApexPages.StandardController(acc1));
    
    oppsList.getopps4llChildAcnt();
    
    system.assertEquals(3, oppsList.getOpps().size(), 'There should be 3 opps.');
        
    //Search button is cliked for the first time
    oppsList.queryChildOpps();

    //we query once more for all the children
    oppsList.getopps4llChildAcnt();

    //sortExpression is null so we get one line of code covered
    oppsList.sortExpression = null;
    oppsList.getSortDirection();
        
    //we simulate clickin the sort button 
    oppsList.sortExpression = 'name';        
        
    //we set now the sort direction
    oppsList.setSortDirection('ASC');
    system.assertEquals('ASC', oppsList.getSortDirection());
        
    //we simulate clickin the sort button 
    oppsList.sortExpression = 'type';
        
    //click it once more to simulate ASC vs DESC
    oppsList.sortExpression = 'type';
        
    oppsList.opp1.Type = 'Test';
    oppsList.queryChildOpps();
    system.assertEquals(3, oppsList.getOpps().size(), 'There should be 3 opps, of this type.');
    
    oppsList.opp1.Type = null;
    oppsList.opp1.Mash_Approval_Date__c = Date.today();
    oppsList.queryChildOpps();
    system.assertEquals(3, oppsList.getOpps().size(), 'There should be 3 opps, closing after today.');
        
    oppsList.opp1.Mash_Approval_Date__c = null;
    oppsList.opp1.Contract_End_Date__c = Date.today();
    oppsList.queryChildOpps();
    system.assertEquals(0, oppsList.getOpps().size(), 'There should be 3 opps, closing before today.');
    
    oppsList.opp1.Contract_End_Date__c = null;
    oppsList.opp1.StageName = 'Random Stage';
    oppsList.queryChildOpps();
    system.assertEquals(0, oppsList.getOpps().size(), 'There should be 0 opps, with Random Stage stage.');
    
    system.assert(oppsList.backToAccount() != null);
    oppsList.exportToExcel(); //added by manoj
    Test.stopTest();
  }
  
}