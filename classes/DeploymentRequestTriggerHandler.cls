/**=====================================================================
 * Experian
 * Name: DeploymentRequestTriggerHandler 
 * Description: Trigger handler on Deployment Request object
 *
 * Created Date: November 9th, 2015
 * Created By: Nur Azlini (Experian)
 *
 * Date Modified            Modified By              Description of the update
 * Nov 18th, 2015           Paul Kissick             Case 01244379: Improved the updateCaseStatus method
 * Nov 25th, 2015           Paul Kissick             Case 01250947: Adding Release Type fixes
 * Mar 8th, 2016            Paul Kissick             Case 01891441: Adding fix to not update the status if already set to Implemented.
 * Jul 21st, 2016           Paul Kissick             W-005536: Extending to support Agile Accelerator
 ======================================================================*/
public class DeploymentRequestTriggerHandler {
    
  //===========================================================================
  //After Update
  //===========================================================================
  public static void afterUpdate (List<Deployment_Request__c> newList, Map<Id, Deployment_Request__c> oldMap) {
    createApprovalProcess (newList, oldMap);
    updateParentStatus (newList, oldMap);
    deleteAllDeploymentCases (newList, oldMap);
  }
    
  //===========================================================================
  // Method to create Approval Process
  //===========================================================================
  public static void createApprovalProcess (List<Deployment_Request__c> newList, Map<Id, Deployment_Request__c> oldMap) {
    try {
      List<Deployment_Request__c> deployReqList = new List<Deployment_Request__c>();
      for (Deployment_Request__c dr : newList) {
        if (dr.Status__c == Constants.APP_REQUESTED_DR_STATUS && 
            oldMap.get(dr.Id).Status__c != Constants.APP_REQUESTED_DR_STATUS) {
          
          Approval.ProcessSubmitRequest submitReq = new Approval.ProcessSubmitRequest();
          submitReq.setComments('Submitting request for approval.');
          submitReq.setObjectId(dr.Id); 
          submitReq.setNextApproverIds(new Id[]{UserInfo.getUserId()}); // Set current user as the approver 
          Approval.ProcessResult result = Approval.process(submitReq);
        }
      }
    }
    catch (Exception ex) {
      system.debug('[DeploymentRequestTriggerHandler:createApprovalProcess]'+ex.getMessage()); 
      ApexLogHandler.createLogAndSave('DeploymentRequestTriggerHandler','createApprovalProcess', ex.getStackTraceString(), ex);
    }
  }

  //===========================================================================
  // updateParentStatus : Updates the status on the Deployment case parent, 
  // which could be a case or a work record
  //===========================================================================
  public static void updateParentStatus (List<Deployment_Request__c> newList, Map<Id,Deployment_Request__c> oldMap) {
    try {
      List<Case> caseToUpdateList = new List<Case>();
      List<agf__ADM_Work__c> workToUpdateList = new List<agf__ADM_Work__c>();
      
      set<Id> caseSetId = new set<Id>();
      set<Id> drSetId = new set<Id>();
      // String drStatus;
      // Boolean isProd;
      
      for (Deployment_Request__c dr : newList) {
        // Constants.DEPLOYED_DR_STATUS = 'Deployed'
        // Constants.APP_FOR_DEPLOY_DR_STATUS = 'Approved for Deployment'
        if ((dr.Status__c.startsWithIgnoreCase(Constants.DEPLOYED_DR_STATUS) || dr.Status__c == Constants.APP_FOR_DEPLOY_DR_STATUS) &&
            dr.Status__c != oldMap.get(dr.Id).Status__c) {
          // Status has been changed to Deployed or Approved for Deployment
          drSetId.add(dr.Id);
        }
      }
      if (!drSetId.isEmpty()) {
        // Adding check to only update case implementation status if not already implemented
        List<Deployment_Cases__c> relatedDepCaseList = [
          SELECT Id, Case__c, Case__r.Implementation_Status__c,
                 Deployment_Request_Slot__r.Status__c, Deployment_Request_Slot__r.Is_Production__c,
                 Deployment_Request_Slot__r.Release_Type__c,
                 Work__c, Work__r.agf__Status__c
          FROM Deployment_Cases__c
          WHERE Deployment_Request_Slot__c IN :drSetId
          AND (Work__c != null OR Case__c != null)
        ];
        
        Case newCase;
        agf__ADM_Work__c aaWork;
        
        if (!relatedDepCaseList.isEmpty()) {
          for (Deployment_Cases__c dCase : relatedDepCaseList) {
            
            // Related to a case...
            if (dCase.Case__c != null && dCase.Case__r.Implementation_Status__c != Constants.CASE_STATUS_IMPLEMENTED) {
              newCase = new Case(Id = dCase.Case__c);
              if (dCase.Deployment_Request_Slot__r.Status__c == Constants.APP_FOR_DEPLOY_DR_STATUS &&
                  dCase.Deployment_Request_Slot__r.Is_Production__c == true) {
                newCase.Implementation_Status__c = Constants.APP_FOR_DEPLOY_DR_STATUS;
                //using the same constant as having the same value in case picklist value
              }
              else if (dCase.Deployment_Request_Slot__r.Status__c == Constants.APP_FOR_DEPLOY_DR_STATUS &&
                       dCase.Deployment_Request_Slot__r.Is_Production__c == false) {
                // Don't change anything...
              }
              else if (dCase.Deployment_Request_Slot__r.Status__c == Constants.DEPLOYED_TO_PROD_DR_STATUS) {
                newCase.Implementation_Status__c = Constants.CASE_STATUS_IMPLEMENTED;
                newCase.Release__c = dCase.Deployment_Request_Slot__r.Release_Type__c;
              }
              else {
                // Are we sure this is what we want to happen?
                newCase.Implementation_Status__c = dCase.Deployment_Request_Slot__r.Status__c;
              }
              caseToUpdateList.add(newCase);
            }
            
            // Related to a work...
            if (dCase.Work__c != null && dCase.Work__r.agf__Status__c != Constants.CASE_STATUS_IMPLEMENTED) {
              aaWork = new agf__ADM_Work__c(Id = dCase.Work__c);
              if (dCase.Deployment_Request_Slot__r.Status__c.startsWithIgnoreCase(Constants.DEPLOYED_DR_STATUS)) {
                aaWork.agf__Status__c = dCase.Deployment_Request_Slot__r.Status__c;
              }
              workToUpdateList.add(aaWork);
            }
          }
          update caseToUpdateList;
          update workToUpdateList;
        }
      }
    }
    catch (Exception ex) {
      system.debug('[DeploymentRequestTriggerHandler:updateCaseStatus]'+ex.getMessage()); 
      ApexLogHandler.createLogAndSave('DeploymentRequestTriggerHandler','updateCaseStatus', ex.getStackTraceString(), ex);
    }
  }
  
  //===========================================================================
  // Method to delete Deployment Cases if status changed to cancelled.
  //===========================================================================
  public static void deleteAllDeploymentCases (List<Deployment_Request__c> newList, Map<Id, Deployment_Request__c> oldMap) {
    try {
      Set<Id> drSetId = new Set<Id>();
      for (Deployment_Request__c dr : newList) {
        if (dr.Status__c == Constants.CANCELLED_DR_STATUS && oldMap.get(dr.Id).Status__c != Constants.CANCELLED_DR_STATUS) {
          drSetId.add(dr.Id);
        }
      }
      if (!drSetId.isEmpty()) {
        List<Deployment_Cases__c> dCList = [
          SELECT Id 
          FROM Deployment_Cases__c
          WHERE Deployment_Request_Slot__c IN :drSetId
        ];
        delete dcList;
      }
    }
    catch (Exception ex) {
      system.debug('[DeploymentRequestTriggerHandler:deleteAllDeploymentCases]'+ex.getMessage()); 
      ApexLogHandler.createLogAndSave('DeploymentRequestTriggerHandler','deleteAllDeploymentCases', ex.getStackTraceString(), ex);
    }
        
  }
    
}