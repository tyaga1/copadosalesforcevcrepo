/**=====================================================================
  * Experian
  * Name: SerasaCreateArticleOnCloseCase
  * Description: W-007962: Apex class to pre-populate article information when user clicks
  *              'close and create article'
  * Created Date: April 10 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * April 19th 2017    Ryan (Weijie) Hu             W-007962: Saving 'Resolution' field info from Case to 'Answer' field on Article
  *=====================================================================*/
public with sharing class SerasaCreateArticleOnCloseCase {
  public SerasaCreateArticleOnCloseCase(ApexPages.KnowledgeArticleVersionStandardController ctrl) {
    FAQ__kav article = (FAQ__kav) ctrl.getRecord();   //this is the SObject for the new article. 
                       //It can optionally be cast to the proper article type, e.g. FAQ__kav article = (FAQ__kav) ctl.getRecord();
    
    String sourceId = ctrl.getSourceId(); //this returns the id of the case that was closed.
    Case c = [select Subject, Resolution__c from Case where id=:sourceId];
    
    article.Title = 'From Case: ' + c.Subject;  //this overrides the default behavior of pre-filling the title of the article with the subject of the closed case. 
    article.Answer__c = c.Resolution__c;
    
    //ctl.selectDataCategory('Geography','USA');  //Only one category per category group can be specified.
    //ctl.selectDataCategory('Topics','Maintenance');
  }
}