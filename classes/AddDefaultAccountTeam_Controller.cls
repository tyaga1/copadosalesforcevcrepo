/**=====================================================================
 * Appirio, Inc
 * Name: AddDefaultAccountTeam_Controller
 * Description: 
 * Created Date: 
 * Created By: 
 * 
 * Date Modified                Modified By                  Description of the update
 * Jan 30th, 2014               Jinesh Goyal(Appirio)        T-232760: Homogenize the comments
 * Mar 03rd, 2014               Arpita Bose (Appirio)        T-243282: Added constants in place of string.
 * Mar 28th, 2014               Arpita Bose (Appirio)        T-267517:Removed references to ContactAccessLevel - Contact Sharing setting is set as 'Controlled by Parent'
 * Jun 15th, 2016               Paul Kissick                 Case #02024883: Fixing AccountShare problems (v37.0)
 =====================================================================*/
public without sharing class AddDefaultAccountTeam_Controller {
    
  private final Account acct;
    
  public AddDefaultAccountTeam_Controller(ApexPages.StandardController stdController){
    this.acct = (Account)stdController.getRecord();
    List<UserAccountTeamMember> uatm = [
      SELECT AccountAccessLevel, CaseAccessLevel, ContactAccessLevel, TeamMemberRole, OwnerId, UserId 
      FROM UserAccountTeamMember
      WHERE UserId = :UserInfo.getUserId()
    ];
  }
    
  public void addTeamMember(){
    AccountTeamMember atm = new AccountTeamMember();
    atm.AccountId = acct.id;
    atm.UserId = [select id from User where IsActive=true and Profile.Name =: Constants.PROFILE_EXP_SALES_MANAGER limit 1].id;//'005c0000000l5YX';
    atm.TeamMemberRole = Constants.TEAM_ROLE_ACCOUNT_MANAGER;
    atm.AccountAccessLevel = Constants.ACCESS_LEVEL_READ;
    atm.OpportunityAccessLevel = Constants.ACCESS_LEVEL_READ;
    insert atm;
  }
  
}