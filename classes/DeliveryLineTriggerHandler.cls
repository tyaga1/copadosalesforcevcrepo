/**=====================================================================
 * Appirio, Inc
 * Name: DeliveryLineTriggerHandler
 * Description:
 * Created Date: July 31, 2014
 * Created By: Nathalie LE GUAY (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Jul 31, 2014                 Nathalie Le Guay             T-309151: Create a Project Resource record if a DL's Owner is updated to a new User
 * Aug 02, 2014                 Nareseh Kr Ojha               Update method createProjectResources () for null pointer exception, put check for map.
 * Aug 19, 2014                 Arpita Bose(Appirio)         T-313220: Updated method createProjectResources() to create project resource for User only
 * Sep 26, 2014                 Naresh Kr Ojha               T-322781: added method createZeroHourTimecardEntry() method for completed delivery lines.
 * Apr 03, 2015                 Nur Azlini                   Case#555032: Update method createZeroHourTimecardEntry() to make the complete method works for expired status as well.
 * May 29th, 2015               Paul Kissick                 Case #593374: Adding method checkForProjectDeliveryType() to update estimated LOE on projects
 * Oct 3rd, 2016                Paul Kissick                 Case #02152746: Added checking to createZeroHourTimecardEntry to prevent duplication
 * Dec 21, 2016                 Ruthwik Reddy                Case #02187106: New Formula field on the Project Object to calculate Revenue Discrepancy
 * Jan 03, 2017                 Manoj Gopu                   Case #02187106: Updated the Method calculateRevenueDiscrepancy() as this is not Updating EDQ Revenue Discrepency before. 
 * May 16, 2017                 James Wills                  Case #02301359: Auto time card creation on DL complete status - amendment to date
 =====================================================================*/
public without sharing class DeliveryLineTriggerHandler {
  //After Insert Call
  public static void afterInsert (List<Delivery_Line__c> newList) {
    createProjectResources(newList, null);
    createZeroHourTimecardEntry(newList, null);//As per: T-322781
    checkForProjectDeliveryType(newList, null); // PK Case #593374
    calculateRevenueDiscrepancy(newList, null); //Case #02187106
  }

  //After Update Call
  public static void afterUpdate (List<Delivery_Line__c> newList, Map<Id, Delivery_Line__c> oldMap) {
    createProjectResources(newList, oldMap);
    createZeroHourTimecardEntry(newList, oldMap);//As per: T-322781
    checkForProjectDeliveryType(newList, oldMap); // PK Case #593374
    calculateRevenueDiscrepancy(newList, null); //Case #02187106
  }

  // After delete call
  public static void afterDelete (Map<Id, Delivery_Line__c> oldMap) {
    checkForProjectDeliveryType(null, oldMap); // PK Case #593374
  }

  // Case #593374 - List to filter to update based on service_type__c
  public static Set<String> deliveryLineServiceTypesLoeSet = new Set<String>{'Delivery','International Delivery'};
  
  // Case #02187106 - To calulate the Revenue Discrepency on Project object
  public static void calculateRevenueDiscrepancy(list<Delivery_Line__c> newLstDeliveryLine,list<Delivery_Line__c> oldLstDeliveryLine){
        List<Project__c> lstProjectUpdate = new List<Project__c>();
        set<Id> setProjectIds = new Set<Id>();
        for(Delivery_Line__c objDeliveryLines : newLstDeliveryLine){
            if(objDeliveryLines.Project__c !=Null){
                setProjectIds.add(objDeliveryLines.Project__c);//Replaced Project__r.Id with Project__c
            }
        }
        
        List<Project__c> lstProject = [select id,EDQ_Revenue_Discrepancy__c,CurrencyISOCode FROM Project__c where id in : setProjectIds];
        map<Id,list<Delivery_Line__c>> mapProjectDelLines = new map<Id,list<Delivery_Line__c>>();//Used to Store the Project Id related to all delivery lines       
        for(Delivery_Line__c objDel:[Select Id,convertCurrency(Revenue__c) rev,convertCurrency(OL_EDQ_Margin__c) edq,Order_Line_Item__c,Project__c FROM Delivery_Line__c where Project__c IN:lstProject])
        {
            if(mapProjectDelLines.containsKey(objDel.Project__c)){
                list<Delivery_Line__c> lstExiDel = mapProjectDelLines.get(objDel.Project__c);
                lstExiDel.add(objDel);
                mapProjectDelLines.put(objDel.Project__c,lstExiDel);
            }else{
                list<Delivery_Line__c> lstDel = new list<Delivery_Line__c>();
                lstDel.add(objDel);
                mapProjectDelLines.put(objDel.Project__c,lstDel);
            }
        }
        //Getting the Conversion Rate dynamic
        map<string,Decimal> mapConvRate = new map<string,Decimal>();
        for(currencytype objCurrType:[SELECT conversionrate,isocode  FROM currencytype]){
            mapConvRate.put(objCurrType.isocode,objCurrType.conversionrate);
        }
        for(Project__c objProject : lstProject)
        {
            Decimal sumdeliveryrevenue = 0;
            Decimal oplineitemsum = 0;
            map<string,string> mapOrderLines = new map<string,string>();
            if(mapProjectDelLines.containsKey(objProject.Id)){
                for(Delivery_Line__c objDelivery_Lines : mapProjectDelLines.get(objProject.Id)){//Replaced New list with objProject.Delivery_Lines__r
                    if(objDelivery_Lines.get('rev') !=NUll)
                        sumdeliveryrevenue = sumdeliveryrevenue  + (decimal)objDelivery_Lines.get('rev');
                    if(objDelivery_Lines.get('edq') !=NUll && !mapOrderLines.containsKey(objDelivery_Lines.Order_Line_Item__c)){
                        oplineitemsum = oplineitemsum   + (decimal)objDelivery_Lines.get('edq');
                        mapOrderLines.put(objDelivery_Lines.Order_Line_Item__c,objDelivery_Lines.Order_Line_Item__c);                   
                    }
                }
            }
            objProject.EDQ_Revenue_Discrepancy__c = (oplineitemsum - sumdeliveryrevenue) * mapConvRate.get(objProject.CurrencyISOCode) ;
            lstProjectUpdate.add(objProject);
        }
        if(!lstProjectUpdate.isEmpty())
        update lstProjectUpdate;
           
         }

  //========================================================================================
  // Name: createProjectResources
  // Will take a list of Delivery_Line__c records and will create Project_Resource__c records
  // where necessary
  //========================================================================================
  private static void createProjectResources(List<Delivery_Line__c> deliveryLines, Map<Id, Delivery_Line__c> oldDeliveryLines) {

    // Retrieve related projects, if owner of Delivery Line was changed
    Set<String> projectIds = new Set<String>();
    for (Delivery_Line__c dl : deliveryLines) {
      Id ownerId = dl.OwnerId; // PK: Replaced String with Id
      if (oldDeliveryLines == null) {
        if (dl.Project__c != null) {
          projectIds.add(dl.Project__c);
        }
      }
      else {
        // PK: Replaced the check for prefix with sobject checks
        if (oldDeliveryLines.get(dl.Id).OwnerId != dl.OwnerId && ownerId.getSobjectType() == User.SObjectType) {
          if (dl.Project__c != null) {
            projectIds.add(dl.Project__c);
          }
        }
      }
    }
    
    

    // Query all projects and related Project Resources
    Map<Id, Project__c> projectMap = new Map<Id, Project__c>([
      SELECT Id, OwnerId,
        (SELECT Id, Resource__c FROM Project_Resources__r)
      FROM Project__c
      WHERE Id IN :projectIds
    ]);

    // Create a Project Resource record if none exists
    List<Project_Resource__c> prToCreate = new List<Project_Resource__c>();
    for (Delivery_Line__c dl : deliveryLines) {
      if (projectMap.containsKey(dl.Project__c) &&
          !projectMap.get(dl.Project__c).Project_Resources__r.isEmpty() &&
          !ProjectResource_Utils.hasProjectResource(dl.OwnerId, projectMap.get(dl.Project__c).Project_Resources__r)) {
        prToCreate.add(ProjectResource_Utils.createNewProjectResourceRecord(dl.Project__c, dl.OwnerId));
      }
    }
    if (!prToCreate.isEmpty()) {
      ProjectResource_Utils.createProjectResourceRecords(prToCreate);
    }
  }

  //===========================================================================
  // Case 02152746:
  // Creates a 'key' from the delivery line for checking timesheets are unique
  //===========================================================================
  private static String createKeyFromDeliveryLine(Delivery_Line__c dl) {
    if (dl.Project__c == null || dl.Id == null || dl.OwnerId == null || dl.Actual_End_Date__c == null) {
      return null;
    }
    String keyVal = dl.Project__c + '_';
    keyVal += dl.Id + '_';
    keyVal += dl.OwnerId + '_';
    keyVal += Datetime.newInstance(dl.Actual_End_Date__c, Time.newInstance(12,0,0,0)).format('yyyy-MM-dd');
    return keyVal;
  }

  //==========================================================================
  // T-322781: Creating zero hour timecard on completion of delivery line
  // Case#555032
  //==========================================================================
  private static void createZeroHourTimecardEntry (List<Delivery_Line__c> newList,
                                                   Map<ID, Delivery_Line__c> oldMap) {
    List<Timecard__c> newTimecards = new List<Timecard__c>();

    // Case 02152746
    Set<Id> resourceIdSet = new Set<Id>();
    Set<Id> deliveryLineIdSet = new Set<Id>();
    Set<Id> projectIdSet = new Set<Id>();
    Set<Date> dateSet = new Set<Date>();
    Map<String, Timecard__c> tcKeyToTS = new Map<String, Timecard__c>();

    try {
      //For completed and Expired delivery lines, creating timecard entries
      for (Delivery_Line__c delLine : newList) {
        if ((delLine.Status__c == Constants.STATUS_COMPLETED || delLine.Status__c == Constants.STATUS_EXPIRED) &&
            ((oldMap != null) ? (delLine.Status__c != oldMap.get(delLine.ID).Status__c) : true)) {
          
          DateTime dT = System.now();//Case #02301359
          Date createdDate = date.newinstance(dT.year(), dT.month(), dT.day());
          
          Timecard__c newTimecard = new Timecard__c(
            Resource__c      = delLine.OwnerId,
            Delivery_Line__c = delLine.Id,
            Project__c       = delLine.Project__c,
            Description__c   = delLine.Description__c,
            Type__c          = Constants.TIMECARD_TYPE_DELIVERY,            
            Date__c          = createdDate //delLine.Actual_End_Date__c
          );
          // newTimecards.add(newTimecard);
          // Case 02152746
          String tcKey = createKeyFromDeliveryLine(delLine);
          if (String.isNotBlank(tcKey)) {
            tcKeyToTS.put(tcKey, newTimecard);

            resourceIdSet.add(delLine.OwnerId);
            deliveryLineIdSet.add(delLine.Id);
            projectIdSet.add(delLine.Project__c);
            dateSet.add(delLine.Actual_End_Date__c);
          }
        }
      }
    }
    catch (Exception ex) {
      ApexLogHandler.createLogAndSave('DeliveryLineTriggerHandler','createZeroHourTimecardEntry', ex.getStackTraceString(), ex);
    }
    try {
      // Case 02152746
      // Now, check for existing timecards matching the same values as the ones we are inserting,
      // and remove these so they don't get created.

      List<Timecard__c> existingTimecards = [
        SELECT Id, Delivery_Line__c, Delivery_Line__r.Id, Delivery_Line__r.OwnerId,
               Delivery_Line__r.Project__c, Delivery_Line__r.Actual_End_Date__c
        FROM Timecard__c
        WHERE Resource__c IN :resourceIdSet
        AND Delivery_Line__c IN :deliveryLineIdSet
        AND Project__c IN :projectIdSet
        AND Date__c IN :dateSet
        AND Type__c = :Constants.TIMECARD_TYPE_DELIVERY
      ];

      for (Timecard__c tcCheck : existingTimecards) {
        String tcKey = createKeyFromDeliveryLine(tcCheck.Delivery_Line__r);
        if (String.isNotBlank(tcKey)) {
          system.debug('Preventing duplicate Delivery Line : '+tcKey);
          if (tcKeyToTS.containsKey(tcKey)) {
            tcKeyToTS.remove(tcKey);
          }
        }
      }

      // Insert timecards
      if (!tcKeyToTS.isEmpty()) {
        insert tcKeyToTS.values();
      }
    }
    catch (DMLException ex) {
      ApexLogHandler.createLogAndSave('DeliveryLineTriggerHandler','createZeroHourTimecardEntry', ex.getStackTraceString(), ex);
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        newList.get(0).addError(ex.getDmlMessage(i));
      }
    }
  }

  //==========================================================================
  // Case #593374: Roll up estimated LOE onto the project.
  // Sets the Update_LOE_From_Delivery_Lines__c field to kick off the refresh.
  //==========================================================================
  public static void checkForProjectDeliveryType(List<Delivery_Line__c> newList, Map<Id, Delivery_Line__c> oldMap) {
    List<Project__c> projectsToUpdate = new List<Project__c>();
    List<Delivery_Line__c> dlsToCheck = new List<Delivery_Line__c>();
    if (newList == null) {
      dlsToCheck = oldMap.values();
    }
    else {
      dlsToCheck = newList;
    }

    Set<Id> projectIds = new Set<Id>();
    for(Delivery_Line__c dl : dlsToCheck) {
      Boolean addProject = false;
      if (Trigger.isUpdate) {
        if (oldMap.get(dl.Id).Service_Type__c != dl.Service_Type__c) {
          addProject = true;
        }
      }
      // Note, only update project to sync Estimated LOE if this is a 'Delivery' project.
      if (deliveryLineServiceTypesLoeSet.contains(dl.Service_Type__c) && !projectIds.contains(dl.Project__c)) {
        addProject = true;
      }
      if (addProject) {
        projectsToUpdate.add(
          new Project__c(
            Id = dl.Project__c,
            Update_LOE_From_Delivery_Lines__c = true
          )
        );
        projectIds.add(dl.Project__c); // Hold a set of project ids to prevent duplication, thus allowing the update call to work properly.
      }
    }
    try {
      update projectsToUpdate;
    }
    catch (DMLException ex) {
      ApexLogHandler.createLogAndSave('DeliveryLineTriggerHandler','checkForProjectDeliveryType', ex.getStackTraceString(), ex);
      for (Integer i = 0; i < ex.getNumDml(); i++) {
        newList.get(0).addError(ex.getDmlMessage(i));
      }
    }
  }

}