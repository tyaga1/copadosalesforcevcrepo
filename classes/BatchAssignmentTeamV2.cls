/**=====================================================================
 * Experian
 * Name: BatchAssignmentTeamV2
 * Description: Replacement for BatchAssignmentTeam - Will soon deprecate
     Case 01033112
 * Created Date:
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Nov 2nd, 2015                Paul Kissick                 Case 01223378: Fixing test errors when email deliverability is off
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Dec 3rd, 2015                Paul Kissick                 Case 01266075: Removing Global_Settings__c for timings
 * Dec 15th, 2015               Paul Kissick                 Case 01084405: Insert shares before account team
 * Jan 5th, 2015                Paul Kissick                 Case 01662532: Adding changes to help with missed assignments
 * Mar 07th, 2016               Sadar Yacob                  Case 01843114 - Set Opty Access to Read instead of Edit
 * Jun 15th, 2016               Paul Kissick                 Case 02024883: Fixing issue due to changes to AccountTeamMember object (v37.0)
 =====================================================================*/

global class BatchAssignmentTeamV2 implements Database.Batchable<sObject>, Database.Stateful {

  //private static final String GLOBAL_SETTING = 'Global';
  //private Global_Settings__c lastRun;
  private DateTime lastRunTime;
  
  private Datetime startTime;
  
  global List<String> insertErrors;
  global List<String> updateErrors;
  global List<String> deleteErrors;
  global Set<String> inactiveErrors;  // Making this a set to minimise the number of errors, but still report important ones
  
  global Map<Id,Assignment_Team_Member__c> assTeamMemberMapUpd;
  //global Map<Id,Assignment_Team_Member__c> allAssTeamMemberMap;
  
  global Boolean fixMissedAssignments;
  
  global BatchAssignmentTeamV2 (Boolean fix) {
    fixMissedAssignments = fix;
  }
  
  //=====================================================================================================
  // Start - Will query all Assignment_Team_Member__c that have been updated (check on _Updated__c field)
  //=====================================================================================================
  global Database.Querylocator start (Database.Batchablecontext bc) {
    // lastRun = Global_Settings__c.getInstance(GLOBAL_SETTING);
    if (fixMissedAssignments) {
      lastRunTime = BatchHelper.getBatchClassTimestamp('AccountAssignmentTeamJobLastRun');
      if (lastRunTime == null) {
        lastRunTime = Datetime.now().addYears(-5);
      }
    }
    else {
      lastRunTime = BatchHelper.getBatchClassTimestamp('AssignmentTeamMemberJobLastRun'); 
    }
    
    startTime = system.now();
    
    assTeamMemberMapUpd = new Map<Id,Assignment_Team_Member__c>();
    
    if (insertErrors == null) insertErrors = new List<String>();
    if (updateErrors == null) updateErrors = new List<String>();
    if (deleteErrors == null) deleteErrors = new List<String>();
    if (inactiveErrors == null) inactiveErrors = new Set<String>();
    
    if (fixMissedAssignments) {
      return Database.getQueryLocator([
        SELECT Id, Account__c, Assignment_Team__c, Assignment_Triggered__c, IsDeleted
        FROM Account_Assignment_Team__c 
        WHERE Assignment_Triggered__c = false
      ]);
    }
    else {
      return Database.getQueryLocator([
        SELECT Id, Account__c, Assignment_Team__c, IsDeleted
        FROM Account_Assignment_Team__c 
        WHERE Assignment_Team__c IN (
          SELECT Assignment_Team__c
          FROM Assignment_Team_Member__c
          WHERE CreatedDate >= :lastRunTime OR
            User_Updated__c = true OR 
            Assignment_Team_Role_Updated__c = true OR 
            IsActive_Updated__c = true 
        )
      ]);
    }
  }
  
  global void execute (Database.BatchableContext bc, List<Account_Assignment_Team__c> scope) {
    
    Map<String, AccountTeamMember> teamMembersToInsert = new Map<String, AccountTeamMember>();
    Map<String, AccountTeamMember> teamMembersToUpdate = new Map<String, AccountTeamMember>();
    Map<String, AccountTeamMember> teamMembersToDelete = new Map<String, AccountTeamMember>();
    //List<AccountShare> accountShareList = new List<AccountShare>();
    List<Account_Assignment_Team__c> aatToUpdate = new List<Account_Assignment_Team__c>();
    
    // THESE ARE ALL USED TO CHECK THE ASSIGNMENT TEAM LINKS WHEN DELETING
    Set<Id> usersToDelete = new Set<Id>();
    Set<Id> accsToCheck = new Set<Id>();
    Set<Id> assTeamsToCheck = new Set<Id>();
    
    // Now we have a list of accounts to process, and the related assignment team.
    // Convert this assignment team into a list of members that have changed, so we can process the required changes on the account.
    Set<Id> assignmentTeamIdSet = new Set<Id>();
    Set<Id> accountIdSet = new Set<Id>();
    for(Account_Assignment_Team__c aat : scope) {
      assignmentTeamIdSet.add(aat.Assignment_Team__c);
      accountIdSet.add(aat.Account__c);
    }
    
    Map<Id,Account> accountMap = new Map<Id,Account>([
      SELECT Id,
        (SELECT Id, AccountId, UserId, TeamMemberRole
        FROM AccountTeamMembers),
        (SELECT Assignment_Team__c FROM Account_Assignment_Teams__r) 
      FROM Account
      WHERE Id IN :accountIdSet
    ]);
    
    Map<Id,Assignment_Team__c> assignTeamMap;
    
    Map<Id,Assignment_Team__c> assignTeamToCheckMap = new Map<Id,Assignment_Team__c>([
      SELECT Id, Name,
       (SELECT User__c FROM Assignment_Team_Members__r WHERE IsActive__c = true) 
      FROM Assignment_Team__c 
      WHERE Id IN (SELECT Assignment_Team__c FROM Account_Assignment_Team__c WHERE Account__c IN :accountIdSet) 
    ]);
     
    if (fixMissedAssignments) {
      assignTeamMap = new Map<Id,Assignment_Team__c>([
        SELECT Id, Account_Executive__c, (
          SELECT Id, Assignment_Team__c, 
            Assignment_Team_Role__c, IsActive__c, User__c, CreatedDate,
            User__r.IsActive, User__r.Name
          FROM Assignment_Team_Members__r 
          // WHERE IsActive__c = true
          ) 
        FROM Assignment_Team__c 
        WHERE Id IN :assignmentTeamIdSet
      ]);
      
    }
    else {
      assignTeamMap = new Map<Id,Assignment_Team__c>([
        SELECT Id, Account_Executive__c, (
          SELECT Id, Assignment_Team__c, 
            Assignment_Team_Role__c, Assignment_Team_Role_Starting_Value__c, Assignment_Team_Role_Updated__c,
            IsActive__c, IsActive_Starting_Value__c, IsActive_Updated__c,
            User__c, User_Starting_Value__c,  User_Updated__c,
            CreatedDate, User__r.IsActive, User__r.Name
          FROM Assignment_Team_Members__r 
          WHERE User_Updated__c = true OR 
            Assignment_Team_Role_Updated__c = true OR 
            IsActive_Updated__c = true OR 
            CreatedDate >=: lastRunTime
          ) 
        FROM Assignment_Team__c 
        WHERE Id IN :assignmentTeamIdSet
      ]);
    }
    
    Savepoint sp = Database.setSavepoint();
  
    for (Account_Assignment_Team__c aat : scope) {
      
      // here we have 2 ids, 1 for the account, 1 for the assignment team. Now we need to see what we have to update for each entry here...
      Id accountId = aat.Account__c;
      Id assignmentTeamId = aat.Assignment_Team__c;
      
      Account a = accountMap.get(accountId);
      Assignment_Team__c assTeam = assignTeamMap.get(assignmentTeamId);
      
      for (Assignment_Team_Member__c assTeamMember : assTeam.Assignment_Team_Members__r) {
        // system.debug(assTeamMember);
        // Id accountExecId = assTeam.Account_Executive__c;
        
        if (!fixMissedAssignments) {
          if (assTeamMember.isActive__c && (
              (assTeamMember.User__c != assTeamMember.User_Starting_Value__c) ||
              (assTeamMember.CreatedDate >= lastRunTime) ||
              (assTeamMember.IsActive_Updated__c))) {
            system.debug('CreatedDate Check '+assTeamMember);
            // Most likely new, so add them here....
            if (assTeamMember.User__r.IsActive) {
              AccountTeamMember tmpATM = createAccntTeamMember(aat,assTeamMember);
              teamMembersToInsert.put(aat.Id+'~'+assTeamMember.User__c,tmpATM);
              //  accountShareList.add(createAccShare(tmpATM,aat));
            }
            else {
              // inactive user found...
              inactiveErrors.add('Unable to assign inactive user - Assignment Team ID: ' + assTeam.Id + ', User ID: ' + assTeamMember.User__c + ', User Name: ' + assTeamMember.User__r.Name);
            }
          }
          
          Set<Id> otherAssTeamMembers = new Set<Id>();
            for (Account_Assignment_Team__c aatOther : a.Account_Assignment_Teams__r) {
              Assignment_Team__c tmpAt = assignTeamToCheckMap.get(aatOther.Assignment_Team__c);
              for (Assignment_Team_Member__c astm1 : tmpAt.Assignment_Team_Members__r) {
                otherAssTeamMembers.add(astm1.User__c);
              }
            }
          
          // here we have each member, see what we need to change based on the assTeamMember data...
          if (assTeamMember.User__c != assTeamMember.User_Starting_Value__c && assTeamMember.isActive__c == true) {
            // replace the old member (User_Starting_Value__c), with the new one (User__c)
            if (a.AccountTeamMembers != null && !a.AccountTeamMembers.isEmpty()) {
              for (AccountTeamMember AcTM : a.AccountTeamMembers) {
                if (AcTM.UserId == assTeamMember.User_Starting_Value__c && !otherAssTeamMembers.contains(AcTM.UserId)) {
                  // system.debug('Deleting '+AcTM);
                  teamMembersToDelete.put(AcTM.Id,AcTM); // Only add to delete if the user is not a member of another ass team on this account.
                }
              }
            }
          }
          if (assTeamMember.Assignment_Team_Role__c != assTeamMember.Assignment_Team_Role_Starting_Value__c && 
              assTeamMember.isActive__c == true) {
            // Role has changed and is still active...
            if (a.AccountTeamMembers != null && !a.AccountTeamMembers.isEmpty()) {
              for (AccountTeamMember AcTM : a.AccountTeamMembers) {
                if (AcTM.UserId == assTeamMember.User__c) {
                  if (assTeamMember.User__r.IsActive) {
                    AcTM.TeamMemberRole = assTeamMember.Assignment_Team_Role__c;
                    teamMembersToUpdate.put(AcTM.Id,AcTM);
                    system.debug('Updating '+AcTM);
                  }
                  else {
                    inactiveErrors.add('Unable to assign inactive user - Assignment Team ID: ' + assTeam.Id + ', User ID: ' + assTeamMember.User__c + ', User Name: ' + assTeamMember.User__r.Name);
                  }
                }
              }
            }
          }
          
          if (assTeamMember.IsActive_Updated__c) {
            // either been made inactive, or active....
            if (assTeamMember.IsActive__c == false) {
              // remove acc team member
              if (a.AccountTeamMembers != null && !a.AccountTeamMembers.isEmpty()) {
                for (AccountTeamMember AcTM : a.AccountTeamMembers) {
                  if (AcTM.UserId == assTeamMember.User__c && !otherAssTeamMembers.contains(AcTM.UserId)) {
                    teamMembersToDelete.put(AcTM.Id, AcTM);
                    system.debug('Deleting '+AcTM);
                    // As we are removing, check the account & user isn't linked by another ass team member linkage...
                  }
                }
              }
            }
          }
          assTeamMemberMapUpd.put(assTeamMember.Id,assTeamMember);
        }
        // This is fixing missed assignments
        else {
          if (assTeamMember.isActive__c) {
            if (assTeamMember.User__r.IsActive) {
              AccountTeamMember tmpATM = createAccntTeamMember(aat, assTeamMember);
              teamMembersToInsert.put(aat.Id+'~'+assTeamMember.User__c,tmpATM);
              // accountShareList.add(createAccShare(tmpATM,aat));
            }
            else {
              inactiveErrors.add('Unable to assign inactive user - Assignment Team ID: ' + assTeam.Id + ', User ID: ' + assTeamMember.User__c + ', User Name: ' + assTeamMember.User__r.Name);
            }
          }
        }
      }
      aat.Assignment_Triggered__c = true;
      aatToUpdate.add(aat);
    }
    if (fixMissedAssignments) {
      List<Database.SaveResult> updateSaveAat = Database.update(aatToUpdate,false);
      for (Database.SaveResult sr : updateSaveAat) {
        if (!sr.isSuccess()) {
          updateErrors.addAll(parseErrors(sr.getErrors()));
        }
      }
    }
    // Case 01084405 - Moved this above the teammembers
    /*
    List<Database.SaveResult> insertSaveRes2 = Database.insert(accountShareList, false);
    for (Database.SaveResult sr : insertSaveRes2) {
      if (!sr.isSuccess()) {
        insertErrors.addAll(parseErrors(sr.getErrors()));
      }
    }
    */
    List<Database.SaveResult> insertSaveRes = Database.insert(teamMembersToInsert.values(), false);
    for (Database.SaveResult sr : insertSaveRes) {
      if (!sr.isSuccess()) {
        insertErrors.addAll(parseErrors(sr.getErrors()));
      }
    }
    List<Database.SaveResult> updateSaveRes = Database.update(teamMembersToUpdate.values(), false);
    for (Database.SaveResult sr : updateSaveRes) {
      if (!sr.isSuccess()) {
        updateErrors.addAll(parseErrors(sr.getErrors()));
      }
    }
    if (teamMembersToDelete.size() > 0) {
      system.debug(teamMembersToDelete);
      List<Database.DeleteResult> delRes = Database.delete(teamMembersToDelete.values(), false);
      for (Database.DeleteResult dr : delRes) {
        if (!dr.isSuccess()) {
          deleteErrors.addAll(parseErrors(dr.getErrors()));
        }
      }
    }
  }
  
  global void finish (Database.BatchableContext bc) {
    
    if (!assTeamMemberMapUpd.isEmpty() && !fixMissedAssignments) {
      for (Id atmId : assTeamMemberMapUpd.keySet()) {
        assTeamMemberMapUpd.get(atmId).IsActive_Starting_Value__c = assTeamMemberMapUpd.get(atmId).IsActive__c;
        assTeamMemberMapUpd.get(atmId).Assignment_Team_Role_Starting_Value__c = assTeamMemberMapUpd.get(atmId).Assignment_Team_Role__c;
        assTeamMemberMapUpd.get(atmId).User_Starting_Value__c = assTeamMemberMapUpd.get(atmId).User__c;
        assTeamMemberMapUpd.get(atmId).User_Updated__c = false;
        assTeamMemberMapUpd.get(atmId).Assignment_Team_Role_Updated__c = false;
        assTeamMemberMapUpd.get(atmId).IsActive_Updated__c = false;
      }
      
      List<Database.SaveResult> updateSaveRes3 = Database.update(assTeamMemberMapUpd.values(), false);
      for (Database.SaveResult sr : updateSaveRes3) {
        if (!sr.isSuccess()) {
          updateErrors.addAll(parseErrors(sr.getErrors()));
        }
      }
    }
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchAssignmentTeamV2', false);
    
    String emailBody = '';
    
    if (!deleteErrors.isEmpty() || !insertErrors.isEmpty() || !updateErrors.isEmpty() || !inactiveErrors.isEmpty() || Test.isRunningTest()) {
      
      bh.batchHasErrors = true;
      
      if (!insertErrors.isEmpty()) {
        emailBody += '\nThe following errors were observed when inserting records:\n';
        emailBody += String.join(insertErrors,'\n');
      }
      if (!updateErrors.isEmpty()) {
        emailBody += '\nThe following errors were observed when updating records:\n';
        emailBody += String.join(updateErrors,'\n');
      }
      if (!deleteErrors.isEmpty()) {
        emailBody += '\nThe following errors were observed when deleting records:\n';
        emailBody += String.join(deleteErrors,'\n');
      }
      if (!inactiveErrors.isEmpty()) {
        emailBody += '\nThe following errors were observed due to Inactive records:\n';
        List<String> inactiveErrorsList = new List<String>();
        inactiveErrorsList.addAll(inactiveErrors);
        emailBody += String.join(inactiveErrorsList,'\n');
      }
      
      bh.emailBody += emailBody;
    }
    
    bh.sendEmail();
    
    if (fixMissedAssignments) {
      BatchHelper.setBatchClassTimestamp('AccountAssignmentTeamJobLastRun', startTime);
      
      if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
        // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF FINDING DELETED ACCOUNT ASSIGNMENTS
        system.scheduleBatch(new BatchAssignmentTeamDeleted(), 'BatchAssignmentTeamDeleted'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchAssignmentTeamDeleted'));
      }
    }
    else {
      BatchHelper.setBatchClassTimestamp('AssignmentTeamMemberJobLastRun', startTime);
    }
    // update lastRun;
  }
  
  private static List<String> parseErrors(List<Database.Error> dbErrors) {
    List<String> errorsFound = new List<String>();
    for(Database.Error err : dbErrors) {
      errorsFound.add('Code: '+err.getStatusCode()+' - Message:'+err.getMessage()+' - Fields:'+String.join(err.getFields(),','));
    }
    return errorsFound;
  }
  
  //============================================================================
  // T-370563: Utility method to create Account Team Member records
  //============================================================================
  private static AccountTeamMember createAccntTeamMember(Account_Assignment_Team__c aat, Assignment_Team_Member__c atm) {    
    AccountTeamMember newATM = new AccountTeamMember();
    newATM.AccountId = aat.Account__c;
    newATM.UserId = atm.User__c;
    newATM.TeamMemberRole = atm.Assignment_Team_Role__c; 
    newATM.AccountAccessLevel = Constants.ACCESS_LEVEL_EDIT;
    newATM.OpportunityAccessLevel =  Constants.ACCESS_LEVEL_READ; //SY:03/07/16: Fix to make Opty Access read only
    newATM.CaseAccessLevel = 'None';
    return newATM;   
  }
  
  //============================================================================
  // T-370563: Utility method to create AccountShare records
  //============================================================================
  /*
  private static AccountShare createAccShare(AccountTeamMember newATM, Account_Assignment_Team__c aat) {
    AccountShare newShare = new AccountShare();
    newShare.UserOrGroupId = newATM.UserId;
    newShare.AccountId = aat.Account__c;
    newShare.
    newShare.OpportunityAccessLevel =  Constants.ACCESS_LEVEL_READ; //SY:03/07/16: Fix to make Opty Access read only : 
    newShare. //Constants.ACCESS_LEVEL_READ;
    return newShare;
  }
  */

}