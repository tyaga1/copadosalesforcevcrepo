/**
* @description A page controller for Filter Search Article visualforce page
* Its main usage is to record down user's defined searching criteria and pass these criteria back
* to the visualforce knowledge article list visualforce default component.
*
* @author Ryan (Weijie) Hu, UC Innovation
*
* @date 02/14/2017
*/
public with sharing class ExpComm_SearchArtiFilterPageController {
    public String searchKeyword {get; set
        {
            searchKeyword = value;
            if (searchKeyword == 'Enter your seaching keyword here') {
                searchKeyword = '';
            }
        }
    }
    public String dataCategory {get; set;}
    public String articleTypes {get{return 'Employee_Article__kav';} set;}
    public String sorting {get; set;}
    public Integer pageSize {get; set;}
    public Integer currentPageNumber {get; set;}
    
    public String userLanguage {get {
            Id currentUserId = UserInfo.getUserId();
            User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];
            return currentUser.LanguageLocaleKey;
        }
    }

    /**
     * Constructor - Default (Initialize other control properties and read url parameter if exists)
     *
     * @param   None.
     * @return  None.
     */
    public ExpComm_SearchArtiFilterPageController() {

        // Initialize searching properties
        String searchTermUrlParam = ApexPages.currentPage().getParameters().get('searchTerm');
        if (searchTermUrlParam == null) {
            searchKeyword = '';
        }
        else {
            searchKeyword = searchTermUrlParam;
        }

        dataCategory = 'Experian_Internal:All';
        articleTypes = 'Employee_Article__kav';
        sorting = '';
        pageSize = 10;
        currentPageNumber = 1;
    }
}