/**=====================================================================
 * Experian
 * Name: SMXNACSClBISSupProCaseBatchSdlrContext 
 * Description: 
 * Created Date: 22/07/2016
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 *
 =====================================================================*/
 
global class SMXNACSClBISupProCaseBatchSdlrContext implements Schedulable {

  global SMXNACSClBISupProCaseBatchSdlrContext(){}
  
  global void execute(SchedulableContext ctx){
    SMXNACSClntBISSupportProcessCaseBatch b = new SMXNACSClntBISSupportProcessCaseBatch();
    if (!Test.isrunningTest()) {
      Database.executeBatch(b);
    }
  }

}