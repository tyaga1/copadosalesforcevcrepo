/*=============================================================================
 * Experian
 * Name: ForeCastReportController_Test
 * Description: Case-02184517 : Test Class for ForeCastReportController.
 *                         
 * Created Date: 21st Feb 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/
@isTest(seeAllData=true)
private class ForeCastReportController_Test{
    static testMethod void myUnitTest(){
        Test.startTest();
            
            ForecastingQuota obj = [select id,QuotaOwnerId,QuotaOwner.Name from ForecastingQuota limit 1];
            
            ForeCastReportController objC = new ForeCastReportController();
            objC.exportData();
            objC.usersSelection();
            objC.runReportData();
            
            List<SelectOption> lstSelectedUsers1 = new list<SelectOption>();
            lstSelectedUsers1.add(new SelectOption(obj.QuotaOwnerId,obj.QuotaOwner.Name)); 
            
            objC.lstSelectedUsers = lstSelectedUsers1; 
            
            objC.runReportData();   
            
            objC.strUser='Test';
            objC.usersSelection();  
        Test.stopTest();
        
    }
}