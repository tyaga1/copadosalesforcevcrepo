/**=====================================================================
 * Appirio, Inc
 * Name: AccountPlanContactTriggerHandler
 * Description: I-120524
 * Created Date: Jul 14th, 2014
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Sept 22nd, 2015              Jagjeet Singh                T-435624 - Added before delete method.
 =====================================================================*/
public with sharing class AccountPlanContactTriggerHandler {
  
  //============================================================================
  // Method called on Before Insert
  //============================================================================
  public static void beforeInsert(List<Account_Plan_Contact__c> newList){
    synchCurrencyISOCodes(newList, null);
  }
  
  //============================================================================
  // Method called on Before Update
  //============================================================================
  public static void beforeUpdate(List<Account_Plan_Contact__c> newList, 
                                  Map<Id, Account_Plan_Contact__c> oldMap){
    synchCurrencyISOCodes(newList, oldMap);
  }
    
  //============================================================================
  // Method called on Before Delete
  //============================================================================
  public static void beforeDelete(List<Account_Plan_Contact__c> oldList){
    validateRecordsToBeDeleted(oldList);
  }
  
  //============================================================================
  // Method to update Currency Codes on Account Plan Contact
  //============================================================================
  private static void synchCurrencyISOCodes(List<Account_Plan_Contact__c> newList, 
                                            Map<ID, Account_Plan_Contact__c> oldMap){
    Set<String> aPlanIDs = new Set<String>();
    List<Account_Plan_Contact__c> lstAPlanComp = new List<Account_Plan_Contact__c>();
    Map<String, Account_Plan__c> mapAPlanId_APlan = new Map<String, Account_Plan__c>();
    
    //Find acc plan Ids related to Acc Plan Contact
    for(Account_Plan_Contact__c aPComp : newList){
        if(oldMap == null || (oldMap.get(aPComp.Id).Account_Plan__c  != aPComp.Account_Plan__c )){
            lstAPlanComp.add(aPComp);
            aPlanIDs.add(aPComp.Account_Plan__c);
        }
    }
    
    if(!aPlanIDs.isEmpty()){
        for(Account_Plan__c aPlan : [SELECT Id, CurrencyIsoCode, 
                                    (SELECT Id, CurrencyIsoCode From Account_Plan_Contacts__r)
                                     From Account_Plan__c 
                                     WHERE ID IN: aPlanIDs]){
            mapAPlanId_APlan.put(aPlan.Id, aPlan);
        }
        // Update Currency Iso code for Account Plan Contact
        for(Account_Plan_Contact__c aPcomp : lstAPlanComp){
            aPcomp.CurrencyISOCode = mapAPlanId_APlan.get(aPcomp.Account_Plan__c).CurrencyISOCode;
        }
    }                                           
    
  }
  
    //============================================================================
    // Method to validate the records to be deleted, check for the current user to be part of AccountTeamMember
    //============================================================================
    private static void validateRecordsToBeDeleted(List<Account_Plan_Contact__c> oldList){
       AccountPlanUtility.validateDeletingRecords(oldList);
    } 
}