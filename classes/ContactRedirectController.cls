/**=====================================================================
 * Appirio, Inc
 * Name: ContactRedirectController
 * Description: T-424805
 * Created Date: Aug 12th, 2015
 * Created By: Arpita Bose (Appirio)
 * 
 * Date Modified      Modified By                  Description of the update
 * 14/8/2015                Siddharth                          Added brazil country condition
 * Aug 25th, 2015     Arpita Bose                  Updated to populate Account & removed reference of Serasa Profiles
 * Sep 16th, 2015     Paul Kissick                 I-180579: Adding changes for allowing 'No Address' for some users
 * Oct 7th, 2015      Paul Kissick                 I-184075: Adding fix for recordtype not passed to next page
 * Oct 20th, 2015     Paul Kissick                 Case 01198515: Fixing redirection order for serasa
 * Mar 14th, 2017     Ryan (Weijie) Hu             W-007090: Consumer Contact record type added as special case
 * Mar 29th, 2017     Ryan (Weijie) Hu             W-007431: Added support for consumer account record type when creating dummy acct for W-007090
 * April 25th, 2017   Ryan (Weijie) Hu             W-007121: I953: add valid serasa customer care profiles into custom setting 
 * =====================================================================*/
public with sharing class ContactRedirectController {
  
  private ApexPages.StandardController controller;
  public String retURL {get; set;}
  public static Account account {get; set;}
  public static Contact con {get; set;}
  public string accId {get; set;}
  public String accName {get; set;}
  public List<SelectOption> CPFRequiredList {get;set;}
  public String selectedCPFOption {get;set;}
  public Boolean showCPFSelect {get;set;}
  public Boolean showNewContactButtons {get;set;}
  private String recordTypeId;
  private Boolean isConsumerContact;
  
  private Set<String> permissionSetsAllowedForNewAccount = new Set<String>{
      Constants.PERMISSIONSET_EDQ_SAAS_DEPLOYMENT_MANAGER
  };
  private Set<String> profilesAllowedForNewAccount = new set<String>{Constants.PROFILE_SYS_ADMIN};
  
  public String getNewAccountPermSets() {
    List<String> tmpList = new List<String>(permissionSetsAllowedForNewAccount);
    return String.join(tmpList,',').replace('_',' ');
  }
  
  public String getNewAccountProfSets() {
    List<String> tmpList = new List<String>(profilesAllowedForNewAccount);
    return String.join(tmpList,',');
  }
  
  //constructor  
  public ContactRedirectController(ApexPages.StandardController controller) {
    selectedCPFOption = '';
    CPFRequiredList = new List<SelectOption>();
    CPFRequiredList.add(new SelectOption('CPFNeeded', Label.Create_Contact_with_CPF_Number)); 
    CPFRequiredList.add(new SelectOption('CPFNotNeeded', Label.Create_Contact_without_CPF_Number));
    this.controller = controller;
    con = (Contact)controller.getRecord();
    if(con.AccountId != null) {
      account = [SELECT Id, Name FROM Account WHERE Id = :con.AccountId];
    }
    recordTypeId = (ApexPages.currentPage().getParameters().containsKey('RecordType')) ? ApexPages.currentPage().getParameters().get('RecordType') : null;
    retURL = ApexPages.currentPage().getParameters().get(Constants.PARAM_NAME_RETURL);
    accId = ApexPages.currentPage().getParameters().get('accId');
    showCPFSelect = false;
    showNewContactButtons = false;
    isConsumerContact = false;
    //accName = ApexPages.currentPage().getParameters().get('accName');
  }
  
  // method to redirect Page based on Profile of current user
  public PageReference redirect(){
    PageReference returnURL;
    
    List<PermissionSet> permissionSetsAllowed = [
      SELECT Id, Name
      FROM PermissionSet
      WHERE Name in: permissionSetsAllowedForNewAccount
    ];
    
    User usrRec = [
      SELECT Profile.Name, Country__c, Region__c, 
      (SELECT Id FROM PermissionSetAssignments WHERE PermissionSetId in: permissionSetsAllowed) 
      FROM User 
      WHERE Id= :UserInfo.getUserId()
    ];

    // Check for if the current creating contact is a consumer contact record type
    RecordType consumer_contact_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Consumer_Contact'];
    if (!String.isBlank(recordTypeId) && consumer_contact_rt.Id == Id.valueOf(recordTypeId)) {
      
      selectedCPFOption = 'CPFNeeded';
      isConsumerContact = true;

      List<Serasa_Consumer_Contact_Active_Account__c> serasaActiveAcctCustomSettingList = [SELECT Active_Account_Id__c FROM Serasa_Consumer_Contact_Active_Account__c LIMIT 1];
      Account serasaActiveAcct;

      if (serasaActiveAcctCustomSettingList == null || serasaActiveAcctCustomSettingList.size() == 0) {
        RecordType consumer_account_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumer_Account'];
        serasaActiveAcct = new Account (
          Serasa_Consumer_Active_Account__c = true,
          Name = 'Serasa Consumer Account',
          Serasa_Consumer_Contact_Count__c = 0,
          RecordTypeId = consumer_account_rt.Id
        );
        insert serasaActiveAcct;

        Serasa_Consumer_Contact_Active_Account__c newRecordOnSetting = new Serasa_Consumer_Contact_Active_Account__c();
        newRecordOnSetting.Active_Account_Id__c = serasaActiveAcct.Id;
        insert newRecordOnSetting;
      }
      else {
        String activeSerasaAcctIdStr = serasaActiveAcctCustomSettingList.get(0).Active_Account_Id__c;
        List<Account> serasaActiveAcctList = [SELECT Id, Name, Serasa_Consumer_Contact_Count__c, Serasa_Consumer_Active_Account__c FROM Account WHERE Id =: activeSerasaAcctIdStr];
        if (serasaActiveAcctList == null || serasaActiveAcctList.size() == 0) {

          RecordType consumer_account_rt = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumer_Account'];
          serasaActiveAcct = new Account (
            Serasa_Consumer_Active_Account__c = true,
            Name = 'Serasa Consumer Account',
            Serasa_Consumer_Contact_Count__c = 0,
            RecordTypeId = consumer_account_rt.Id
          );
          insert serasaActiveAcct;

          Serasa_Consumer_Contact_Active_Account__c newRecordOnSetting = serasaActiveAcctCustomSettingList.get(0);
          newRecordOnSetting.Active_Account_Id__c = serasaActiveAcct.Id;
          update newRecordOnSetting;
        }
        else {
          serasaActiveAcct = serasaActiveAcctList.get(0);
        } 
      }

      accId = serasaActiveAcct.Id;
      accName = serasaActiveAcct.Name;
      PageReference pr = redirectToCPF();
      pr.getParameters().put('RecordType',consumer_contact_rt.Id);

      return pr;
    }


    Set<String> validSeresaProfiles = new Set<String>();
    Serasa_User_Profiles__c seresaUsrProfiles = Serasa_User_Profiles__c.getall().get(Constants.SERASA);

    Set<String> validSerasaCustomerCareProfiles = new Set<String>();
    Serasa_User_Profiles__c serasaCustomerCareProfiles = Serasa_User_Profiles__c.getall().get(Constants.SERASA_CUSTOMER_CARE_PROFILES);


    validSerasaCustomerCareProfiles.add('API Only');

    if(seresaUsrProfiles != null && seresaUsrProfiles.Profiles__c != null){
      for(String profileName : seresaUsrProfiles.Profiles__c.split(',')){
        validSeresaProfiles.add(profileName.trim());
      }
    }

    if(serasaCustomerCareProfiles != null && serasaCustomerCareProfiles.Profiles__c != null){
      for(String profileName : serasaCustomerCareProfiles.Profiles__c.split(',')) {

        // Adding the profile name and the 'Experian Serasa ' prefix back to the profile name to the set
        validSerasaCustomerCareProfiles.add('Experian Serasa ' + profileName.trim());
      }
    }


    if (profilesAllowedForNewAccount.contains(usrRec.Profile.Name) || usrRec.PermissionSetAssignments.size() > 0) {
      showNewContactButtons = true;
      return null;
    }
    else if (validSeresaProfiles.contains(usrRec.Profile.Name)) {
      showCPFSelect = true;
      if (accId == null) {
        returnURL = Page.ContactNewAlert;
      }
      else {
        return null;
      }
    }
    else if (validSerasaCustomerCareProfiles.contains(usrRec.Profile.Name)) {
      if (accId == null) {
        returnURL = Page.ContactNewAlert;
      }
      else {
        return redirectToCPF();
      }
    }
    else {
      returnURL = newContactAndAddress();
    }
        
    returnURL.setRedirect(true);
    return returnURL;
  }
  
  public PageReference newContactAndAddress() {
    PageReference pr = new PageReference('/apex/CSS_AddOrCreateNewContactAddress');
    if (accId != null) {
      pr.getParameters().put('accId',accId);
    }
    if (recordTypeId != null) {
      pr.getParameters().put('RecordType',recordTypeId);
    }
    pr.setRedirect(true); 
    return pr;
  }
  
  public PageReference newContact() {
    PageReference pr = newContactAndAddress();
    pr.getParameters().put('bypassQAS','1');
    return pr;
  }
  
  public pagereference redirectToCPF () {
    PageReference returnURL;
    if (accId == null && isConsumerContact == false) {
      returnURL = Page.ContactNewAlert;
    }
    else {
      if ( selectedCPFOption == 'CPFNotNeeded') {
        returnURL = new PageReference('/apex/CSS_AddOrCreateNewContactAddress?accId='+accId);
      }
      else {
        returnURL = new PageReference('/apex/ContactCPFSearch?accId='+accId+'&accName='+accName); 
      }
    }
    
    returnURL.setRedirect(true); 
    return returnURL;
  }
}