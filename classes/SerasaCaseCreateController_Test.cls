/**=====================================================================
  * Experian
  * Name: SerasaCaseCreateController_Test
  * Description: W-007274: Apex test class for SerasaCaseCreateController
  * Created Date: June 26 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the 
  *
  *=====================================================================*/
@isTest
private class SerasaCaseCreateController_Test {

    static Id serasaCaseCancellationRecordTypeId;
    static Id serasaAccountRecordTypeId;
    static Id serasaContactRecordTypeId;
    static User testUser;
    static Contact contact;
    static Account account;
    
    private static testmethod void basicPageControllerTest_cancelCancellation_1() {
        
        testUser = createTestUser();
        
        System.runAs(testUser) {
            serasaCaseCancellationRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Serasa_Cancellation' AND SobjectType = 'Case' LIMIT 1].Id;
          serasaAccountRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'LATAM_Serasa' AND SobjectType = 'Account' LIMIT 1].Id;
            serasaContactRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'LATAM_Serasa' AND SobjectType = 'Contact' LIMIT 1].Id;

            account = Test_Utils.insertAccount();
            account.RecordTypeId = serasaAccountRecordTypeId;
            update account;
            
            contact = Test_Utils.createContact(account.Id);
            contact.RecordTypeId = serasaContactRecordTypeId;
            insert contact;
            
            PageReference pageRef = Page.SerasaCaseCreate;
            Test.setCurrentPage(pageRef);
            
            Case parentCase = createTestData_cancellationParentCase();
            insert parentCase;
            createTestData_ContractCancellationRq(parentCase);
            
            ApexPages.currentPage().getParameters().put('pCaseId', parentCase.Id);
            ApexPages.currentPage().getParameters().put('RecordType', serasaCaseCancellationRecordTypeId);
            ApexPages.currentPage().getParameters().put('cType', 'cccr'); // Indication of creation of a cancellation case for contract cancellation request
            
            
            Case newCase = new Case();
            newCase.Origin = parentCase.Origin;
            newCase.Status = parentCase.Status;
            newCase.Priority = parentCase.Priority;
            
            newCase.Subject = parentCase.Subject;
            newCase.Description = parentCase.Description;
            newCase.User_Island__c = parentCase.User_Island__c;
            newCase.Island__c = parentCase.Island__c;
            newCase.Case_Type__c = parentCase.Case_Type__c;
            newCase.Serasa_Case_Reason__c = parentCase.Serasa_Case_Reason__c;
            newCase.Serasa_Secondary_Case_Reason__c = parentCase.Serasa_Secondary_Case_Reason__c;
            
            newCase.ContactId = parentCase.ContactId;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
            SerasaCaseCreateController ctrl = new SerasaCaseCreateController(sc);
            ctrl.getFields();
            ctrl.save(); // Should cover no ccr selected row
            ctrl.ccrWrapperList[0].selected = true; // select one and save again
            ctrl.save();
            
            ctrl.cancel();
        }
    }
    
    private static testmethod void basicPageControllerTest_cancelCancellation_2() {
        
        testUser = createTestUser();
        
        System.runAs(testUser) {
            serasaCaseCancellationRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Serasa_Cancellation' AND SobjectType = 'Case' LIMIT 1].Id;
            account = Test_Utils.insertAccount();
            contact = Test_Utils.createContact(account.Id);
            insert contact;
            
            PageReference pageRef = Page.SerasaCaseCreate;
            Test.setCurrentPage(pageRef);
            
            Case parentCase = createTestData_cancellationParentCase();
            insert parentCase;
            createTestData_ContractCancellationRq(parentCase);
            
            ApexPages.currentPage().getParameters().put('pCaseId', parentCase.Id);
            ApexPages.currentPage().getParameters().put('RecordType', serasaCaseCancellationRecordTypeId);
            ApexPages.currentPage().getParameters().put('cType', 'cccr'); // Indication of creation of a cancellation case for contract cancellation request
            
            
            Case newCase = new Case();
            newCase.Origin = parentCase.Origin;
            newCase.Status = parentCase.Status;
            newCase.Priority = parentCase.Priority;
            
            newCase.Subject = parentCase.Subject;
            newCase.Description = parentCase.Description;
            newCase.User_Island__c = parentCase.User_Island__c;
            newCase.Island__c = parentCase.Island__c;
            newCase.Case_Type__c = parentCase.Case_Type__c;
            newCase.Serasa_Case_Reason__c = parentCase.Serasa_Case_Reason__c;
            newCase.Serasa_Secondary_Case_Reason__c = parentCase.Serasa_Secondary_Case_Reason__c;
            
            newCase.ContactId = parentCase.ContactId;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
            SerasaCaseCreateController ctrl = new SerasaCaseCreateController(sc);
            ctrl.getFields();
            ctrl.save(); // Should cover no ccr selected row
            ctrl.ccrWrapperList[0].selected = true; // select one and save again
            ctrl.save();
            
            ctrl.cancel();
        }
    }
    
    private static Case createTestData_cancellationParentCase() {
        Case p = new Case();
        p.RecordTypeId = serasaCaseCancellationRecordTypeId;
        p.Origin = 'Phone';
        p.Status = 'Open';
        p.Priority = 'Low';
        
        p.Subject = 'Testing';
        p.Description = 'Testing';
        p.User_Island__c = testUser.Sales_Team__c;
        p.Island__c = p.User_Island__c;
        p.Case_Type__c = 'Cancellation';
        p.Serasa_Case_Reason__c = 'Contract - RT';
        p.Serasa_Secondary_Case_Reason__c = 'Add new offer';
        
        p.ContactId = contact.Id;
        
        return p;
    }
    
    private static User createTestUser() {
        Profile p = [SELECT id from profile where name =: Constants.PROFILE_SYS_ADMIN ];
        User testUser = new User(alias = 'testUser', email='standarduser' + Math.random()  + '@experian.com',
                           emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                           localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', 
                           username='teststandarduser' + Math.random() + '@experian.global.test', IsActive=true,
                           CompanyName = 'test Company');
        testUser.Country__c = 'United States of America';
        testUser.Business_Unit__c = 'LATAM Serasa Customer Care';
        testUser.Sales_Team__c = 'Retention';
      insert testUser;
        
        return testUser;
    }
    
    private static Contract_Cancellation_Request__c createTestData_ContractCancellationRq(Case c) {
        
        Contract_Cancellation_Request__c ccr = new Contract_Cancellation_Request__c();
        ccr.Name = 'testing ccr';
        ccr.Status__c = 'Pending';
        ccr.Account__c = account.Id;
        ccr.Case__c = c.Id;
        ccr.Operation_Type__c = 'Cancel in 30 days';
        ccr.Source_System__c = 'Legacy CRM';
        
        insert ccr;
        return ccr;
    }
}