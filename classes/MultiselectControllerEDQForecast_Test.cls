/*=============================================================================
 * Experian
 * Name: MultiselectControllerEDQForecast_Test
 * Description: Case-02184517 : Test Class for MultiselectControllerEDQForecast
 *                         
 * Created Date: 21st Feb 2017
 * Created By: Manoj Gopu
 *
 * Date Modified      Modified By           Description of the update
 ============================================================================*/

@isTest
private class MultiselectControllerEDQForecast_Test {
    static testMethod void testMultiselectController() {
        MultiselectControllerEDQForecast c = new MultiselectControllerEDQForecast();
        
        c.leftOptions = new List<SelectOption>();
        c.rightOptions = new List<SelectOption>();

        c.leftOptionsHidden = 'A&a&b&b&C&c';
        c.rightOptionsHidden = '';
        
        System.assertEquals(c.leftOptions.size(), 3);
        System.assertEquals(c.rightOptions.size(), 0);
    }
}