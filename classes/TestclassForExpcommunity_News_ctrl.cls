/**
 * This class contains testclass for expcommunity_News_ctrl.cls
 */
@isTest
private class TestclassForExpcommunity_News_ctrl{

    static testMethod void myUnitTest() {
    
    Test.startTest();
        Profile getProfile = [SELECT Id FROM Profile where name ='Standard Platform User' limit 1]; 
        Profile stdProfile = [SELECT Id FROM Profile where name ='Standard User' limit 1]; 
        User newUser = new User(Alias = 'testu', Email='sampleuser@test.in.experian.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = getProfile.Id, phone = '555555555',
        TimeZoneSidKey='America/Los_Angeles', UserName='sampleUser@test.in.experian.com', Department ='test');
        insert newUser; 
        
        
         System.runAs(newUser) {
         
        Employee_Community_News__c featQuote = new Employee_Community_News__c(Name ='test1',  isActive__c = true, Content_Type__c  = 'News', isFeaturedArticle__c  = true, Application_Name__c = 'EITSHome');
        Employee_Community_News__c quote1 = new Employee_Community_News__c(Name ='test2',  isActive__c = true, Content_Type__c  = 'News', isFeaturedArticle__c  = false , Application_Name__c = 'EITSHome');
        Employee_Community_News__c quote2 = new Employee_Community_News__c(Name ='test3',  isActive__c = true, Content_Type__c  = 'News', isFeaturedArticle__c  = false, Application_Name__c = 'EITSHome');                
         insert featQuote;
        insert quote1;
        insert quote2;    
        
        
             expcommunity_News_ctrl newsCall = new expcommunity_News_ctrl();
             newsCall.getfindFeature();
            newsCall.appName = 'EITSHome';
            newsCall.getfindFeature();
         }
         
         Test.stopTest();
        
    }
    
}