/**=====================================================================
 * Experian
 * Name: ITCA_Profile_Builder_controller_Test
 * Description: Test Class for ITCA_Profile_Builder_controller_Controller
 *
 * Created Date: Sept. 4th 2017
 * Created By: James Wills for ITCA Project
 * Date Modified      Modified By           Description of the update
 * 6th Sept. 2017     James Wills           Updates.
 * 15th Sept. 2017    Malcolm Russell       Resolved Detete test issue
=====================================================================*/
@isTest
private class ITCA_Profile_Builder_controller_Test{


 private static testMethod void test1_ITCA_BuildProfile_Page(){
  
    ITCA_Profile_Builder_controller pbc1 = new ITCA_Profile_Builder_controller();   
    
    pbc1.bannerInfoLocal = new ITCABannerInfoClass();

    
    List<Career_Architecture_User_Profile__c> casp = [select id from Career_Architecture_User_Profile__c LIMIT 1];
    pbc1.bannerInfoLocal.currentUserProfile = casp;
   
  
    PageReference pageRef = Page.ITCA_BuildProfile_Page;//Calls ITCA_Profile_Builder_Load()      
    Test.setCurrentPage(pageRef);

    System.currentPageReference().getParameters().put('selectedCareerProfileID', casp[0].id);
            
  
    Test.startTest();
    
      List<Experian_skill_set__c> exp1 = pbc1.careerAreaSkillSets;//VF: ITCA_BuildProfile_Page; ITCA_Profile_Builder_Load()
      List<ITCA_Profile_Builder_controller.skillSetsDisplayStruct> csdl = pbc1.careerSkillSetDisplayList;//VF: ITCA_BuildProfile_Page; ITCA_Profile_Builder_Load()
    
      String[] ca = pbc1.careerAreas;//VF: ITCA_BuildProfile_Page;  
      String[] sl = pbc1.skillLevels;//VF: ITCA_BuildProfile_Page;
      String[] sids = pbc1.skillIDs;//VF: ITCA_BuildProfile_Page;
 
      List<Skill_set_to_skill__c> sss = pbc1.skillSetSkills;//VF: ITCA_BuildProfile_Page; ITCA_Profile_Builder_Load()
                                                            //VF: ITCA_SkillSelect_Page; ITCA_Profile_Skill_Builder_Load()
                                                            
                                                       
      pbc1.ITCA_Profile_Builder_Load();
      PageReference pr = pbc1.SkillSetSubmit();//VF:ITCA_BuildProfile_Page  

    Test.stopTest();
  
  }
  
  private static testMethod void test2_ITCA_SkillSelect_Page(){
  
    ITCA_Profile_Builder_controller pbc1 = new ITCA_Profile_Builder_controller();   
    
    pbc1.bannerInfoLocal = new ITCABannerInfoClass();

    
    List<Career_Architecture_User_Profile__c> casp = [select id from Career_Architecture_User_Profile__c LIMIT 1];
    pbc1.bannerInfoLocal.currentUserProfile = casp;
   
    PageReference pageRef2 = Page.ITCA_SkillSelect_Page;//Calls ITCA_Profile_Skill_Builder_Load()
    Test.setCurrentPage(pageRef2);
   
    Test.startTest();

      pbc1.ITCA_Profile_Skill_Builder_Load();
       
      String selectedId = [SELECT id FROM Experian_Skill_Set__c LIMIT 1].id;
      //pbc1.careerSkillSetDisplayList[0].skillSetRecord.Id;
                                                          
      System.currentPageReference().getParameters().put('skillsetID', selectedId);  
      
            
       pbc1.ITCA_Profile_Skill_Builder_Load();

      List<Skill_set_to_skill__c> sts = pbc1.skillSetSkills;//VF: ITCA_BuildProfile_Page; ITCA_Profile_Builder_Load()
                                                            //VF: ITCA_SkillSelect_Page; ITCA_Profile_Skill_Builder_Load()
 
      Set<string> ssn = pbc1.skillSetNames;//VF: ITCA_SkillSelect_Page; ITCA_Profile_Skill_Builder_Load()
      String sd = pbc1.skillDelete;//VF: ITCA_SkillSelect_Page; 
      List<ITCA_Profile_Builder_controller.displayStruct> dr = pbc1.displayRecords;//VF: ITCA_SkillSelect_Page; ITCA_Profile_Skill_Builder_Load(); saveToPlan(); removeSkill(); RJ added

  
      PageReference sts2 = pbc1.saveTechSkills_ProfileCreate();//VF: ITCA_SkillSelect_Page;
  
      PageReference stp = pbc1.saveToPlan();//VF: ITCA_SkillSelect_Page

      /////////////////////////////////////////////////////////////////////////
      //VF: ITCA_SkillSelect_Page VF: ITCA_Profile_Update_Skills
      /////////////////////////////////////////////////////////////////////////
   
      //ITCA:I1426 MR
      Pagereference rs = pbc1.removeSkill();//VF: ITCA_SkillSelect_Page
  
      //Build the list for the Technical Specialisms modal to display
      List<ITCA_Profile_Builder_controller.techSkill> batsl = pbc1.buildAddTechSkillsList();//VF: ITCA_Profile_Update_Skills; VF: ITCA_SkillSelect_Page;
  
      ID skillID = [SELECT id FROM ProfileSkill LIMIT 1].id;
      
     
      List<SelectOption> gdfi = pbc1.getSkillLevelsFromID(skillID);//VF: ITCA_Profile_Update_Skills; VF: ITCA_SkillSelect_Page;
  
  
      pbc1.addSelectedTechSkillsToPage();{}//VF: ITCA_Profile_Update_Skills; VF: ITCA_SkillSelect_Page;
  
    Test.stopTest();
  
  }
  
  
  private static testMethod void test3_ITCA_Profile_Update_Skills(){
  
    ITCA_Profile_Builder_controller pbc1 = new ITCA_Profile_Builder_controller();   
    
    pbc1.bannerInfoLocal = new ITCABannerInfoClass();
    
   
    List<Career_Architecture_User_Profile__c> casp = [select id from Career_Architecture_User_Profile__c LIMIT 1];
    pbc1.bannerInfoLocal.currentUserProfile = casp;
   

    PageReference pageRef3 = Page.ITCA_Profile_Update_Skills;//Calls ITCA_Profile_Skill_Update_Load()
    Test.setCurrentPage(pageRef3);
   

    system.currentPageReference().getParameters().put('selectedCareerProfileID', casp[0].id);
            
    //pageRef2.mySkillsSummaryAction();
  
    Test.startTest();
      
      pbc1.ITCA_Profile_Skill_Update_Load();
      
      List<Career_Architecture_User_Profile__c> cep = pbc1.currentEndorsementPreference;//VF: ITCA_Profile_Update_Skills; ITCA_Profile_Skill_Update_Load W-009303
    
      ID usd = pbc1.userSkillToDelete;//VF: ITCA_Profile_Update_Skills;
  
      Set<String> essn = pbc1.experianSkillSetNameForUser_Set;//VF: ITCA_Profile_Update_Skills;

      /////////////////////////////////////////////////////////////////////////
      //VF: ITCA_SkillSelect_Page VF: ITCA_Profile_Update_Skills
      /////////////////////////////////////////////////////////////////////////
   
      //ITCA:I1426 MR
      Pagereference rs = pbc1.removeSkill();//VF: ITCA_SkillSelect_Page
  
      //Build the list for the Technical Specialisms modal to display
      List<ITCA_Profile_Builder_controller.techSkill> bst = pbc1.buildAddTechSkillsList();//VF: ITCA_Profile_Update_Skills; VF: ITCA_SkillSelect_Page;
  
      ID skillID = [SELECT id FROM ProfileSkill LIMIT 1].id;
      List<SelectOption> getSkills = pbc1.getSkillLevelsFromID(skillID);//VF: ITCA_Profile_Update_Skills; VF: ITCA_SkillSelect_Page;
    
      pbc1.addSelectedTechSkillsToPage();//VF: ITCA_Profile_Update_Skills; VF: ITCA_SkillSelect_Page;
  
      /////////////////////////////////////////////////////////////////////////
      //VF: ITCA_Profile_Update_Skills
      /////////////////////////////////////////////////////////////////////////
      
      
      
      List<ITCA_Profile_Builder_controller.techSkillWrapper> gsfia = pbc1.getSFIASkillList();//VF: ITCA_Profile_Update_Skills;
      
      List<Career_Architecture_Skills_Plan__c> casp1 = [SELECT id, Skill__c, Skill__r.Name, Skill__r.Level1__c, Skill__r.Level2__c, Skill__r.Level3__c, Skill__r.Level4__c,
                                                        Skill__r.Level5__c, Skill__r.Level6__c, Skill__r.Level7__c,
                                                        Skill__r.sfia_Skill__c, Skill__r.Max_Skill_Level__c, Level_in_Number__c 
                                                        FROM Career_Architecture_Skills_Plan__c LIMIT 1];    
      
      List<SelectOption> gsl1 = pbc1.getSkillLevels(casp1[0]);{}//VF: ITCA_Profile_Update_Skills;
  
      List<ITCA_Profile_Builder_controller.techSkillWrapper> getTSL = pbc1.getTechSkillList();//VF: ITCA_Profile_Update_Skills;
  
      List<ITCA_Profile_Builder_controller.techSkill> bsfiaList = pbc1.buildSFIASkillsList();//VF: ITCA_Profile_Update_Skills;
  
      List<Career_Architecture_Skills_Plan__c> gpcasp = pbc1.getProposedCasp_List();//VF: ITCA_Profile_Update_Skills;
  
      //ITCA:1428
      List<Career_Architecture_User_Profile__c> caup = [select id from Career_Architecture_User_Profile__c LIMIT 1];
      List<Career_Architecture_Skills_Plan__c> getNP = pbc1.getNewProfileProposedCasp_List(caup[0]);//VF: ITCA_Profile_Update_Skills;
  
      PageReference saveSFIA = pbc1.saveSFIASkills();//VF: ITCA_Profile_Update_Skills;
  
      PageReference saveTech = pbc1.saveTechSkills_ProfileUpdate();//VF: ITCA_Profile_Update_Skills;
    
      PageReference saveSk1 = pbc1.saveSkills();//VF: ITCA_Profile_Update_Skills;
    
      //ITCA:I1426 JW
      // Added MR
      pbc1.userSkillToDelete=[select id from Career_Architecture_Skills_Plan__c LIMIT 1].id;
      pbc1.userSkillToDeleteSkillID=[select skill__c from Career_Architecture_Skills_Plan__c LIMIT 1].skill__c;
      
      PageReference delSkill = pbc1.deleteUserSkill();//VF: ITCA_Profile_Update_Skills;
        
      //ITCA W-009303 
      Pagereference updEndChoice = pbc1.updateEndorsementChoice();//VF: ITCA_Profile_Update_Skills;
    

    Test.stopTest();
  
  }
  


  @testSetup
  private static void createData(){
    
    buildProfileSkills();
    buildExperianSkillSet();
    buildSkillSetToSKill();
    createProfile();
  }

  private static void buildProfileSkills(){
  
    ProfileSkill ps1 = new ProfileSkill(Name='Apex', 
                                        Level4__c='Maintains Code.', 
                                        Level5__c='Writes test classes.', 
                                        Level6__c='Designs Code.',
                                        itca_Skill__c = true,
                                        sfia_Skill__c=false);
                                        
    ProfileSkill ps2 = new ProfileSkill(Name='Programming & Software Development', 
                                        Level4__c='Maintains Code.', 
                                        Level5__c='Writes test classes.', 
                                        Level6__c='Designs Code.',
                                        itca_Skill__c = true,
                                        sfia_Skill__c=true); 
                                        
    ProfileSkill ps3 = new ProfileSkill(Name='PMI', 
                                        Level4__c='Maintains Code.', 
                                        Level5__c='Writes test classes.', 
                                        Level6__c='Designs Code.',
                                        itca_Skill__c = true,
                                        sfia_Skill__c=false);
                                        
    ProfileSkill ps4 = new ProfileSkill(Name='System Design', 
                                        Level5__c='Writes design plans.', 
                                        Level6__c='Writes design plans.', 
                                        Level7__c='Writes design plans.',
                                        itca_Skill__c = true,
                                        sfia_Skill__c=true);                                                                          

    List<ProfileSkill> ps_List = new List<ProfileSkill>{ps1,ps2,ps3,ps4};
    insert ps_List;
    
  }

  private static void buildExperianSkillSet(){
    Experian_Skill_Set__c exp1 = new Experian_Skill_Set__c(Name='Software Development', Career_Area__c='Applications');
    insert exp1;
  
  }

  private static void buildSkillSetToSKill(){
    List<ProfileSkill> psList = [SELECT id, Name FROM ProfileSkill];
    List<Experian_Skill_Set__c> essList = [SELECT id FROM Experian_Skill_Set__c];
   
    Skill_Set_to_Skill__c sts1 = new Skill_Set_to_Skill__c(Name='Software Development - Application Support', 
                                                          Skill__c=psList[0].id, 
                                                          Experian_Role__c='Professional',
                                                          Experian_Skill_Set__c=essList[0].id,
                                                          Level__c='Level4');
    insert sts1;
  
 }

  private static void createProfile(){
  
    //Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN ];
    //User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    //insert testUser1;
    
    Career_Architecture_User_Profile__c caup1 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= userInfo.getUserId(),
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup1;
     
    Career_Architecture_Skills_Plan__c casp1 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level6',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill Where Name = 'Apex' LIMIT 1].id  );
    
    insert casp1;
    
    Career_Architecture_Skills_Plan__c casp2 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level7',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill Where Name = 'Programming & Software Development' LIMIT 1].id  );
    insert casp2;
  }

}