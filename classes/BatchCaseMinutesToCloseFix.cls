/**=====================================================================
 * Experian
 * Name: BatchCaseMinutesToCloseFix
 * Description: 
 * Created Date: 28 Jan 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By        Description of the update
 =====================================================================*/
global class BatchCaseMinutesToCloseFix implements Database.Batchable<sObject>, Database.Stateful	{
  
  global string customQuery; 
  
  global Database.QueryLocator start(Database.BatchableContext bc) {
    if (customQuery != null && String.isNotBlank(customQuery)) {
      return Database.getQueryLocator(customQuery);
    }
    return Database.getQueryLocator([
      SELECT Id, BusinessHoursId, Minutes_To_Close__c, CreatedDate, ClosedDate 
      FROM Case 
      WHERE BusinessHours.Name != 'Default' 
      AND IsClosed = true 
      AND Minutes_To_Close__c = null
    ]);
  }

  global void execute(Database.BatchableContext BC, List<Case> scope){
    Decimal businessMilliDiff;
    List<Case> updateList = new List<Case>();
    for(Case c : scope) {
      businessMilliDiff = System.BusinessHours.diff(c.BusinessHoursId, c.CreatedDate, c.ClosedDate);
      updateList.add(
        new Case(
          Id = c.Id, 
          Minutes_To_Close__c = Integer.valueOf(businessMilliDiff / (1000*60))
        )
      ); 
      businessMilliDiff = null;
    }
    update updateList;
  }

  global void finish(Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchCaseMinutesToCloseFix', true);
  }
	
}