/**=====================================================================
 * Name: 
 * Description: CRM2 W-005615: Updates the DQ Flag
 * Created Date: Aug 16th, 2016
 * Created By: Cristian Torres
 * 
 * Date Modified        Modified By          Description of the update
 * 25 Aug, 2016         Paul Kissick         CRM2 W-005615:Cleaned this up and removed hard coded scope size.         
 =====================================================================*/

public class BatchDQFlagPopulationSchedule implements Schedulable{
    
  public void execute(SchedulableContext SC) {
    Database.executeBatch(new BatchDQFlagPopulation(), ScopeSizeUtility.getScopeSizeForClass('BatchDQFlagPopulation'));
  }

  /*  
   * This code should be run in order to schedule the job:
   *
   system.schedule('Opportunity - Populate DQ Flag (Decision Maker)', '0 0 * * * ? *', new BatchDQFlagPopulationSchedule());
   */
}