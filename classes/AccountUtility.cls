/**=====================================================================
 * Appirio Inc
 * Name: AccountUtility.cls
 * Description: T-257202: AccountUtility class to store logic that will be reused by
                multiple classes in a Utility class
 * Created Date: 10th March 2014
 * Created By: Arpita Bose(Appirio)
 *
 * Date Modified        Modified By                    Description of the update
 * March 10th, 2014     Naresh Kr Ojha (Appirio)       Added populateAccShareMap method
 * March 28th, 2014     Arpita Bose (Appirio)          T-267517:Removed references to ContactAccessLevel - Contact Sharing setting is set as 'Controlled by Parent'
 * Jul 11th, 2016(QA)   Tyaga Pati (Experian)          CRM2:W-005501: Story Item from CRM 2.0 to fetch the child accounts for an account.
 * Sep 6th, 2016        Paul Kissick                   CRM2:W-005501: Updated methods to support empty levels variable (tests)
 =====================================================================*/

public without sharing class AccountUtility {

  //=========================================================================
  // Method to get the records of AccountTeamMember for the given AccountIds
  //=========================================================================
  public static Map<String, List<AccountTeamMember>> fetchAccountTeamMember(Set<ID> accIDs) {
    // Map to store account IDs, and related AccountTeamMembers to each account ID.
    Map <String, List<AccountTeamMember>> accIdToListTeamMember = new Map <String, List<AccountTeamMember>>();
    List <AccountTeamMember> atmList;

    // Iterate through the AccountTeamMember to fill accIdToListTeamMember map
    for (AccountTeamMember accTeam : [SELECT Id, UserId, AccountId, AccountAccessLevel,
                                             TeamMemberRole, IsDeleted, CreatedDate
                                      FROM AccountTeamMember
                                      WHERE AccountId IN : accIDs]) {
      if (accIdToListTeamMember.containsKey(accTeam.AccountId)) {
        accIdToListTeamMember.get(accTeam.AccountId).add(accTeam);
      } else {
        atmList = new  List <AccountTeamMember>();
        atmList.add(accTeam);
        accIdToListTeamMember.put(accTeam.AccountId, atmList);
      }
    } // End of For loop
    return accIdToListTeamMember;
  } // End of fetchAccountTeamMember


  //=========================================================================
  // TP: Method to get all Child Accounts of an Account given the Accountid
  //=========================================================================
  //public static Map<Id, List<Account>> fetchChildAccounts(Set<ID> accIDs) {
  //public static List<Id> fetchChildAccounts(List<ID> accIDs) {
  public static List<Id> fetchAllChildAccounts(ID accID) {
    List<Id> allAccountIdList = New List<Id>(); //This list will hold the final list of Accounts that are the total set of children under the account selected
    
    Global_Settings__c custSettings = Global_Settings__c.getValues(Constants.GLOBAL_SETTING);
    Decimal MaxHierlvl = (custSettings.Account_Hierarchy_Max_Levels__c != null) ? custSettings.Account_Hierarchy_Max_Levels__c : 10;     
    
    allAccountIdList.add(accID);
    
    Integer hierCount = 0;
    List<Id> tempAcntList1 = New List<Id>();
    List<Id> tempAcntList2 = New List<Id>();
    tempAcntList1.add(accID);
    while (hierCount < MaxHierlvl) {
      tempAcntList2 = AccountUtility.fetchChildAccounts(tempAcntList1);
      if (!tempAcntList2.isEmpty()) {
        allAccountIdList.addall(tempAcntList2);
        tempAcntList1.clear();
        tempAcntList1.addall(tempAcntList2);
        tempAcntList2.clear();
        hierCount++;
      }
      else {
        break;
      }
    }
    // Iterate through the Account to fill accIdToListchildAccount map for all Account where Parent Id is the Id that was passed
    return allAccountIdList;
  } // End of fetchAllChildAccounts

  public static List<Id> fetchChildAccounts(List<ID> accIDs) {
    // Map to store account IDs, and related Child Accounts to each account ID that was passed in the function call
    //there is a map built if anyone wants to use is later. 
    Map <Id, List<Account>> accIdToListchildAccount = new Map <Id, List<Account>>();
    system.debug('The account list passed is '+ accIDs);
    
    List<Id> allAccountIdListTmp = new List<Id>();
    
    for (Account acc : [SELECT Id, ParentId 
                        FROM Account
                        WHERE Parentid IN :accIDs]) {
      allAccountIdListTmp.add(acc.Id);
      if (!accIdToListchildAccount.containsKey(acc.parentid)) {
        accIdToListchildAccount.put(acc.ParentId, new List<Account>{acc});
      }
      accIdToListchildAccount.get(acc.parentid).add(acc);
    }
    return allAccountIdListTmp;
  }

  //==================================================================================================
  // Method to get the records of AccountShare for the given AccountIds and related AccountTeamMembers
  //==================================================================================================
  public static Map<String, List<AccountShare>> fetchAccountShare(Set<ID> accIDs, Map<String, List<AccountTeamMember>> accIdToListTeamMember){
    // Set to store UserIds related to AccountTeamMember
    Set<String> setUserIds = new Set<String>();
    Map<String, List<AccountShare>> accIdToListAccountshare = new Map<String, List<AccountShare>>();

    // Populating set of userIds
    for (List<AccountTeamMember> atm : accIdToListTeamMember.values()) {
      for (AccountTeamMember tempATM : atm) {
        setUserIds.add(tempATM.UserId);
      }
    }

    List <AccountShare> accShareList;
    // Loop through AccountShare to fill the accIdToListAccountshare map
    for(AccountShare accShare : [SELECT Id, AccountId, UserOrGroupId, AccountAccessLevel,
                                        OpportunityAccessLevel, CaseAccessLevel //ContactAccessLevel T-267517:Removed references to ContactAccessLevel
                                 FROM AccountShare
                                 WHERE AccountId IN : accIDs AND UserOrGroupId = :setUserIds ]) {
      if (accIdToListAccountshare.containsKey(accShare.AccountId)) {
        accIdToListAccountshare.get(accShare.AccountId).add(accShare);
      } else {
        accShareList = new List <AccountShare>();
        accShareList.add(accShare);
        accIdToListAccountshare.put (accShare.AccountId, accShareList);
      }
    } // End of For Loop

    return accIdToListAccountshare;
  } // End of method fetchAccountShare


  //=========================================================================
  // Populating map of accountshare records of given accounts
  //=========================================================================
  public static void populateAccShareMap (Set<ID> accIDs, Map<String, AccountShare> accShareMap) {
    for (Account acc : [SELECT Id,
                          (SELECT Id, AccountId, UserOrGroupId, AccountAccessLevel,
                                  OpportunityAccessLevel, CaseAccessLevel //ContactAccessLevel T-267517:Removed references to ContactAccessLevel
                           FROM Shares)
                        FROM Account
                        WHERE ID IN : accIDs]) {
      for (AccountShare accShare : acc.Shares) {
        if (!accShareMap.containsKey(accShare.AccountId+'~~'+accShare.UserOrGroupId)) {
          accShareMap.put (accShare.AccountId+'~~'+accShare.UserOrGroupId, accShare);
        }
      }
    }
  }

}