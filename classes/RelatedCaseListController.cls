/**=====================================================================
  * Experian
  * Name: RelatedCaseListController
  * Description: Controller class for desired SObject to retrive related cases list
  *              that can be filtered.
  * Created Date: March 08 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * April 4th 2017     Ryan (Weijie) Hu             Change picklist fields to Serasa_Case_Reason__c and Serasa_Secondary_Case_Reason__c
  =====================================================================*/
public with sharing class RelatedCaseListController {

  private final sObject mysObject;
  private final String DO_NOT_FILTER_CONSTANT = System.Label.Related_Case_List_Filter_Do_Not_Filter;
  private final String CASE_STATUS_OPEN_LABEL = System.Label.Related_Case_List_Filter_Open;
  private final String CASE_STAUTS_CLOSED_LABEL = System.Label.Related_Case_List_Filter_Closed;
  private final String CASE_FIELDS_TO_QUERY = 'Id, CaseNumber, CreatedDate, OwnerId, Owner.Name, Status, Subject, Type, Serasa_Case_Reason__c, Serasa_Secondary_Case_Reason__c, Priority, RecordTypeId, ContactId, Contact.Name';
  private static String query;
  private Boolean firstLoad;
  private Boolean isClosed;
  private static Id sObjectId;

  public List<Case> filteredCaseList {
    get {
      return (List<Case>) setCon.getRecords();
    } 
    private set;
  }
  public Integer noOfRecords {get; set;}              // Total number of records
  public Integer pageSize {get; set;}                 // Number of records in one page

  public String selectedStatus {get; set;}
  public String selectedReason {get; set;}
  public String selectedSecondaryReason {get; set;}
  public List<SelectOption> caseStatus {
    get {
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('Open', CASE_STATUS_OPEN_LABEL));
      options.add(new SelectOption('Closed', CASE_STAUTS_CLOSED_LABEL));

      return options;
    }
  }
  public List<SelectOption> caseReasons {
    get {
      List<SelectOption> options = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Case.Serasa_Case_Reason__c.getDescribe();
      List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();

      options.add(new SelectOption(DO_NOT_FILTER_CONSTANT, DO_NOT_FILTER_CONSTANT));
      for (Schema.PicklistEntry f : picklistValues) {
        options.add(new SelectOption(f.getValue(), f.getLabel()));
      }

      return options;
    }
  }
  public List<SelectOption> caseSecondaryReasons {
    get {
      List<SelectOption> options = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Case.Serasa_Secondary_Case_Reason__c.getDescribe();
      List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();

      options.add(new SelectOption(DO_NOT_FILTER_CONSTANT, DO_NOT_FILTER_CONSTANT));
      for (Schema.PicklistEntry f : picklistValues) {
        options.add(new SelectOption(f.getValue(), f.getLabel()));
      }

      return options;
    }
  }

  public ApexPages.StandardSetController setCon {
    get {
      if (setCon == null) {
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        setCon.setPageSize(pageSize);
        noOfRecords = setCon.getResultSize();
      }
      return setCon;
    }
    private set;
  }

  /**
   * The Boolean type method hasNext is used to check it has next page or not                                                                     
   */
  public Boolean hasNext {
    get {
      return setCon.getHasNext();
    }
    private set;
  }

  /**
   * The Boolean type method hasPrevious is used to check it has previous page or not                                                                     
   */
  public Boolean hasPrevious {
    get {
      return setCon.getHasPrevious();
    }
    private set;
  }

  /**
   * The Integer type method pageNumber is used to get the current page number                                                                    
   */
  public Integer pageNumber {
    get {
      return setCon.getPageNumber();
    }
    set;
  }

  /**
   * The Integer type method totalNumber is used to calculate the total page number                                                                  
   */
  public Integer totalNumber {
    get {
      if (math.mod(noOfRecords, pageSize) > 0) {
        return noOfRecords / pageSize + 1;
      } else  {
        return (noOfRecords / pageSize);
      }
    }
    private set;
  }

  // Stadnard extension constructor initializes the private member
  // variable mysObject by using the getRecord method from the standard
  // controller.
  public RelatedCaseListController(ApexPages.StandardController stdController) {
      this.mysObject = (sObject)stdController.getRecord();
      this.pageSize = 10;
      firstLoad = true;
      updateFilterCaseList();
  }

  // Update filtered case list by selected filtering criteria.
  public PageReference updateFilterCaseList() {

    setCon = null;

    // Required filtering criterion (Case Status)
    isClosed = true;
    if (selectedStatus == 'Open') {
      isClosed = false;
    }

    sObjectId = mysObject.Id;

    // Optional other filtering criteria (Case Reason and Secondary Case Reason)
    if (firstLoad == true) {
      firstLoad = false;
      query = 'SELECT ' + CASE_FIELDS_TO_QUERY + ' FROM Case WHERE IsClosed = false';
    }
    else if (selectedReason == DO_NOT_FILTER_CONSTANT && selectedSecondaryReason == DO_NOT_FILTER_CONSTANT) {
      query = 'SELECT ' + CASE_FIELDS_TO_QUERY + ' FROM Case WHERE IsClosed =: isClosed';
    }
    else if (selectedReason != DO_NOT_FILTER_CONSTANT && selectedSecondaryReason == DO_NOT_FILTER_CONSTANT) {
      query = 'SELECT ' + CASE_FIELDS_TO_QUERY + ' FROM Case WHERE IsClosed =: isClosed AND Serasa_Case_Reason__c =: selectedReason';
    }
    else if (selectedReason == DO_NOT_FILTER_CONSTANT && selectedSecondaryReason != DO_NOT_FILTER_CONSTANT) {
      query = 'SELECT ' + CASE_FIELDS_TO_QUERY + ' FROM Case'
              + ' WHERE IsClosed =: isClosed AND Serasa_Secondary_Case_Reason__c =: selectedSecondaryReason';
    }
    else {
      query = 'SELECT ' + CASE_FIELDS_TO_QUERY + ' FROM Case'
              + ' WHERE IsClosed =: isClosed AND Serasa_Case_Reason__c =: selectedReason AND Serasa_Secondary_Case_Reason__c =: selectedSecondaryReason';
    }
    
    // Chceck for current base record type and determine the query string
    if (mysObject.getSObjectType() == Schema.Account.getSObjectType()) {
      query += ' AND AccountId =: sObjectId';
    }
    else if (mysObject.getSObjectType() == Schema.Contact.getSObjectType()) {
      query += ' AND ContactId =: sObjectId';
    }
    else {
      filteredCaseList = new List<Case>();
      return null;
    }

    // Desired range of case records base on audit field - CreadtedDate
    query += ' AND CreatedDate >= LAST_N_MONTHS:6 ORDER BY CreatedDate DESC limit 50';

    //System.debug(Logginglevel.ERROR, '----------------------------- ' + query);
    //filteredCaseList = Database.query(query);

    return null;
  }

  /**
   * The void type method first is used to get the first page 
   */
  public void first() {
    setCon.first();
  }

  /**
   * The void type method last is used to get the last page 
   */
  public void last() {
    setCon.last();
  }

  /**
   * The void type method previous is used to get the previous page 
   */
  public void previous() {
    setCon.previous();
  }

  /**
   * The void type method next is used to get the next page 
   */
  public void next() {
    setCon.next();
  }
}