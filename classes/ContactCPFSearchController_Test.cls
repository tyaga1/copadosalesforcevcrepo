/**=====================================================================
 * Experian
 * Name: ContactCPFSearchController_Test
 * Description: 
 * Created Date: 16 Nov 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update

 =====================================================================*/
@isTest
private class ContactCPFSearchController_Test {

  static String testEmail = 'testemailte0213402184@experian.com';

  static testMethod void testCpfLookupNewContact() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockCalloutCPF());
    
    Account a1 = [SELECT Name,Id FROM Account LIMIT 1];
    
    Test.startTest();
    
    ApexPages.currentPage().getParameters().put('accId',a1.Id);
    ApexPages.currentPage().getParameters().put('accName',a1.Name);
    
    ContactCPFSearchController s = new ContactCPFSearchController(new ApexPages.StandardController(new Contact()));
    
    system.assert(String.isBlank(s.cpfNum));
    
    s.cpfNum = '77676471613'; // This is a valid number.
    
    s.searchContacts();
    
    system.assertEquals(true, s.isMainframe);
    
    system.assertNotEquals(null, s.createFromServiceClick());
    
  }
  
  static testMethod void testCpfLookupExistingContact() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockCalloutCPF());
    
    Account a1 = [SELECT Name,Id FROM Account LIMIT 1];
    
    Contact c1 = Test_Utils.insertContact(a1.Id);
    c1.CPF__c = '77676471613';
    update c1;
    
    Test.startTest();
    
    ApexPages.currentPage().getParameters().put('accId',a1.Id);
    ApexPages.currentPage().getParameters().put('accName',a1.Name);
    
    ContactCPFSearchController s = new ContactCPFSearchController(new ApexPages.StandardController(new Contact()));
    
    system.assert(String.isBlank(s.cpfNum));
    
    s.cpfNum = '77676471613'; // This is a valid number.
    
    s.searchContacts();
    
    system.assertEquals(false, s.isMainframe);
    system.assertEquals(true, s.isSearchresult);
    system.assertEquals(1, s.searchResults.size());
    
    system.assertNotEquals(null, s.processButtonClick());
    
  }
  
  
  static testMethod void testCpfLookupServiceOffline() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockCalloutCPF());
    
    Account a1 = [SELECT Name,Id FROM Account LIMIT 1];
    
    Webservice_Endpoint__c wsEnd = [SELECT Id, Active__c FROM Webservice_Endpoint__c WHERE Name = 'SerasaMainframe'];
    wsEnd.Active__c = false;
    update wsEnd;
    
    
    Test.startTest();
    
    ApexPages.currentPage().getParameters().put('accId',a1.Id);
    ApexPages.currentPage().getParameters().put('accName',a1.Name);
    
    ContactCPFSearchController s = new ContactCPFSearchController(new ApexPages.StandardController(new Contact()));
    
    system.assert(String.isBlank(s.cpfNum));
    
    s.cpfNum = '77676471613'; // This is a valid number.
    
    s.searchContacts();
    
    system.assertEquals(0, s.searchResults.size());
    
    s.resetResults();
    
  }
  
  @testSetup
  static void setupTestData() {
    // Create some contacts and a test user to use...
    User u1 = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    u1.Email = testEmail;
    insert u1;
    
    Account a1 = Test_utils.insertAccount();
    
    Webservice_Endpoint__c wsEnd = new Webservice_Endpoint__c(Name = 'SerasaMainframe',Username__c = 'ausername', URL__c = 'https://somewhere.com', Timeout__c = 10, Active__c = true);
    insert wsEnd;
    
    WebserviceEndpointUtil.createWebserviceKey(wsEnd);
    WebserviceEndpointUtil.setWebservicePassword(wsEnd,'apassword');
    
  }
}