/**=====================================================================
 * Experian
 * Name: EmailHandler_GCSSErrorLog_Test
 * Description: 
 * Created Date: 7 Mar 2016
 * Created By: Paul Kissick (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 =====================================================================*/
@isTest
private class EmailHandler_GCSSErrorLog_Test {
    
  static testMethod void testCaseCreation() {
    
    //
    
    User testUser = Test_Utils.insertUser(Constants.PROFILE_SYS_ADMIN);
    Experian_Global__c expGlobal = new Experian_Global__c();
    expGlobal.SetupOwnerId = UserInfo.getOrganizationId();
    expGlobal.OwnerId__c = testUser.Id;
    expGlobal.GCS_Queue_ID__c = testUser.Id;
    insert expGlobal;
    
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
    
    Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
    
    EmailHandler_GCSSErrorLog ehgel = new EmailHandler_GCSSErrorLog();
    
    email.subject = 'This is a test email';
    email.plainTextBody = 'Hello, this a test email body. for testing purposes only. Bye';
    email.htmlBody = 'Hello, this a test email body. for testing purposes only. Bye';
    email.fromAddress = 'experianerror@test.com';
    email.fromName = 'Experian Error';
    email.toAddresses = new String[]{'errorlog@salesforce.com.test'};
    envelope.fromAddress = 'experianerror@test.com';
    envelope.toAddress = 'errorlog@salesforce.com.test';
    
    inAtt.Body = Blob.valueOf('Test Attachment Body');
    inAtt.FileName ='test.txt';
    inAtt.MimeTypeSubType = 'plain/txt';
    
    email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt};
    
    Messaging.InboundEmailResult res = ehgel.handleInboundEmail(email,envelope);
    
    system.assertEquals(true, res.success);
    
    system.assertEquals(1,[SELECT COUNT() FROM Case WHERE Subject = 'This is a test email']);
    
    system.assertEquals(1,[SELECT COUNT() FROM Attachment WHERE Name = 'test.txt']);
    
    
  }
    
}