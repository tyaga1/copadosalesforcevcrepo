/**=====================================================================
 * Name: AccountPlan_Contacts_Controller
 * Description: See Case #01949954 AP - List views
 * Created Date: May 24th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * May 24th, 2016        James Wills           Case #01949954 AP - List views: Created
 * July 12th, 2016       James Wills           Case #1966786 AP - Contacts tab - Diagram Formatting
 * Oct. 26th, 2016       James Wills           Case #01983126 Added toLabel() translation to SOQL
  =====================================================================*/

global with sharing Class AccountPlan_Contacts_Controller{


  public ID accountPlanId {get;set;}
  Account_Plan__c accountPlan {get;set;}
  
  public ID accountId {get;set;}
  public String accountName {get;set;}
  public Boolean showMessages {get; set;}
  
  public ID selectedID {get;set;}
  public ID selectedIdToDelete {get;set;}

  public Integer pageOffset=0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSize = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Contacts_List__c;
  
  public Integer contactsLimit=relatedListSize;
  public Integer additionalContactsToView {
    get{
      if(endAPContactNumber+relatedListSize > totalAPContacts){
        return totalAPContacts-endAPContactNumber;
      } else {
        return relatedListSize;
      } 
    }
  set;}
  

  public Integer startAPContactNumber {get;set;}
  public Integer endAPContactNumber {get;set;}
  public Integer totalAPContacts {get;set;}

  //public Integer noOfRecords {get;set;}
  //public Integer size {get;set;}

  global Class AccountPlanContactsWrapper implements Comparable{
    public String ID {get;set;}
    public String AccountPlanContact {get;set;}
    public String Contact {get;set;}
    public String ContactName {get;set;}
    public String ContactJobTitle {get;set;}
    public String ExperianRelationship {get;set;}
    public String ReportsTo {get;set;}
    public Boolean IncludeInHealthStatus {get;set;}

    global Integer compareTo(Object compareTo) {
       AccountPlanContactsWrapper compareToContact = (AccountPlanContactsWrapper)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (AccountPlanContact > compareToContact.AccountPlanContact) {
         // Set return value to a positive value.
         returnValue = 1;
       } else if (AccountPlanContact < compareToContact.AccountPlanContact) {
         // Set return value to a negative value.
         returnValue = -1;
       }
       return returnValue;
    }

  }

  public List <AccountPlanContactsWrapper> accountPlanContactsWrapperList {
    get{
      return buildAccountPlanContactsWrapperList();  
    }
    set;
  }

  public AccountPlan_Contacts_Controller( ApexPages.Standardcontroller std){

    accountPlan = (Account_Plan__c) std.getRecord();
    accountPlanId = accountPlan.id;
    
    List<Account_Plan__c> accountPlanDetails = [SELECT id, Account__r.id, Account__r.Name FROM Account_Plan__c WHERE id= :accountPlanId LIMIT 1];
    
    accountId   = accountPlanDetails[0].Account__r.id;
    accountName = accountPlanDetails[0].Account__r.Name;
    showMessages = false;
  }

  
  public void contactsTabUpdateCookies(){
    AccountPlanUtilities.setAccountPlanCookies(accountPlanId, Label.ACCOUNTPLANNING_ContactsTab);
  }

  public PageReference editAccountPlanContact(){
    contactsTabUpdateCookies();
    PageReference accountPlanContactToEdit = new PageReference('/' + selectedId);
    return accountPlanContactToEdit;
  }


  public List<AccountPlanContactsWrapper> buildAccountPlanContactsWrapperList(){
    List<AccountPlanContactsWrapper> accountPlanContactsWrapperList = new List<AccountPlanContactsWrapper>();
  
    List<Account_Plan_Contact__c> accountPlanContactsList = [SELECT ID, Name, Contact__c, Contact_Name__c, Contact_Job_Title__c,
                                                             toLabel(Experian_Relationship__c), Reports_To__c, Include_In_Overall_Health_Status__c 
                                                             FROM Account_Plan_Contact__c WHERE Account_Plan__c = :accountPlanID
                                                             LIMIT :contactsLimit
                                                             ];

    for(Account_Plan_Contact__c apContact : accountPlanContactsList){
      AccountPlanContactsWrapper conWrap = new AccountPlanContactsWrapper();
      conWrap.ID                     = apContact.ID; 
      conWrap.AccountPlanContact     = apContact.Name; 
      conWrap.Contact                = apContact.Contact__c;
      conWrap.ContactName            = apContact.Contact_Name__c;
      conWrap.ContactJobTitle        = apContact.Contact_Job_Title__c;
      conWrap.ExperianRelationship   = apContact.Experian_Relationship__c;
      conWrap.ReportsTo              = apContact.Reports_To__c;
      conWrap.IncludeInHealthStatus  = apContact.Include_In_Overall_Health_Status__c;
        
      accountPlanContactsWrapperList.add(conWrap);    
    }
  
    accountPlanContactsWrapperList.sort();
  
    //Now set values used for Account Plan Contact list navigation
    if(accountPlanContactsWrapperList.size()>0){
      startAPContactNumber = pageOffSet+1;
      endAPContactNumber = pageOffset + accountPlanContactsWrapperList.size();  
      AggregateResult[] APContacts = [SELECT COUNT(Name)TotalAPContacts FROM Account_Plan_Contact__c WHERE Account_Plan__c = :accountPlanID];
      totalAPContacts  = (Integer)APContacts[0].get('TotalAPContacts');    
    } else {
      startAPContactNumber = 0;
      endAPContactNumber = 0;  
      totalAPContacts = 0;
    }
  
    return accountPlanContactsWrapperList;
  }


  public PageReference newAccountPlanContact(){
  
    contactsTabUpdateCookies();  
    
    PageReference newAccountPlanContactPageRef = new PageReference('/' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Contact_Rec__c + 
                                                                  '/e?' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Contact__c + '=' + accountPlan.Name + 
                                                                  '&' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Contact__c + '_lkid=' + accountPlanId + 
                                                                  '&saveURL=' + accountPlanId +
                                                                  '&retURL=' + accountPlanId);

    return newAccountPlanContactPageRef;
  }


  public PageReference viewAccountPlanContactListFull(){
  
    contactsTabUpdateCookies();
  
    PageReference newAccountPlanContactPageRef = new PageReference('/' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Contact_Rec__c + 
                                                                  '?rlid=' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Contact__c.mid(2,15) + '&id=' + accountPlanId);
    return newAccountPlanContactPageRef;
  }


  public void seeMoreContacts(){   
    contactsLimit+=additionalContactsToView;
  }


  public PageReference doDeleteAccountPlanContact(){
  
    contactsTabUpdateCookies();
    
    Schema.DescribeSObjectResult describeResult = Account_Plan_Contact__c.SObjectType.getDescribe();
        
    String accountPlanObjectType = describeResult.getName();
    
    AccountPlanUtilities.deleteAccountPlanRelatedListEntry(selectedIdToDelete, accountPlanObjectType, Label.ACCOUNTPLANNING_ContactsTab);
    
    
    return null;

  }



}