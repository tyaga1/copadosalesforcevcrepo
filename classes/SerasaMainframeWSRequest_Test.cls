/**=====================================================================
 * Experian
 * Name: SerasaMainframeWSRequest_Test
 * Description:  
 *
 * Created Date: 13 Oct, 2015
 * Created By: Sadar Yacob
 * 
 * Date Modified      Modified By                Description of the update
 =====================================================================*/
@isTest
private class SerasaMainframeWSRequest_Test {
  
  private static testMethod void testSerasaWSCpf() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockCalloutCPF());
    
    Test.startTest();
    
    SerasaMainframeWSRequest.webserviceResponse res = SerasaMainframeWSRequest.CallCPFCheckWS('17113441840');
    
    system.assertEquals(true, res.success);
    system.assertEquals(1, res.cpfResults.size());
    
    system.debug('res.ContactName:'  + res.cpfResults[0].ContactName);
    system.debug('res.CPFNumber:'  + res.cpfResults[0].CPFNumber);
    system.debug('res.StatusCode:'  + res.cpfResults[0].StatusCode); //200 if good CNPF and 500 if bad CNPF
    system.debug('res.ErrorMessage:'  + res.cpfResults[0].ErrorMessage);
    system.debug('Final Response: '+ res.cpfResults[0].returnStr());
    system.assertEquals('200',res.cpfResults[0].StatusCode);
    
    Test.stopTest();
  }
  
  private static testMethod void testSerasaWSCpfFail() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockBadCalloutCPF());
    
    Test.startTest();
    
    SerasaMainframeWSRequest.webserviceResponse res = SerasaMainframeWSRequest.CallCPFCheckWS('234252352');
    
    system.assertEquals(false, res.success);
    system.assertEquals(0, res.cpfResults.size());
    
    Test.stopTest();
  }
  
  private static testMethod void testSerasaWSCnpj() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockCalloutCNPJ());
    
    Test.startTest();
    
    SerasaMainframeWSRequest.webserviceResponse res = SerasaMainframeWSRequest.CallCNPJCheckWS('00000424000156');
    
    system.assertEquals(1, res.cnpjResults.size());
    
    system.debug('res.AccountName:'  + res.cnpjResults[0].AccountName);
    system.debug('res.CNPJNumber:'  + res.cnpjResults[0].CNPJNumber);
    system.debug('res.StatusCode:'  + res.cnpjResults[0].StatusCode); //200 if good CNPF and 500 if bad CNPF
    system.debug('res.ErrorMessage:'  + res.cnpjResults[0].ErrorMessage);
    system.debug('Final Response: '+ res.cnpjResults[0].returnStr());
    system.assertEquals('200',res.cnpjResults[0].StatusCode);
    
    Test.stopTest();
  }
  
  private static testMethod void testSerasaWSCnpjBad() {
    
    Test.setMock(HttpCalloutMock.class, new SerasaMainframeWSRequest.MockBadCalloutCNPJ());
    
    Test.startTest();
    
    SerasaMainframeWSRequest.webserviceResponse res = SerasaMainframeWSRequest.CallCNPJCheckWS('1231551251525');
    
    system.assertEquals(0, res.cnpjResults.size());
    system.assertEquals(false, res.success);
    
    Test.stopTest();
  }
  
  @testSetup
  private static void setupData() {
    Webservice_Endpoint__c wsEnd = new Webservice_Endpoint__c(Name = 'SerasaMainframe',Username__c = 'ausername', URL__c = 'https://somewhere.com', Timeout__c = 10, Active__c = true);
    insert wsEnd;
    
    WebserviceEndpointUtil.createWebserviceKey(wsEnd);
    WebserviceEndpointUtil.setWebservicePassword(wsEnd,'apassword');
    
  }

}