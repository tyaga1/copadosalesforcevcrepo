/*=============================================================================
 * Experian
 * Name: MembershipOnboardingToolMgmt
 * Description: Case 01222694
 * Created Date: 15 Jun 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 22nd Sep 2017      Malcolm Russell       Added Document URL
 
 =============================================================================*/

public with sharing class MembershipOnboardingToolMgmt {
  
  public MembershipOnboardingToolMgmt() {
  }
  
  public class optionWrapper {
    public String industry {get;set;}
    public String productName {get;set;}
    public String purpose {get;set;}
    public Map<String, Id> thirdPartyMap {get;set;}
    public List<String> thirdParties {get;set;}
    public String getAnyId() {
      if (!thirdParties.isEmpty()) {
        return thirdPartyMap.get(thirdParties[0]);
      }
      return '';
    }
    public optionWrapper(String ind, String prod, String pur) {
      industry = ind;
      productName = prod;
      purpose = pur;
      thirdPartyMap = new Map<String, Id>();
      thirdParties = new List<String>();
    }
  }
  
  public String optionId {get;set;}
  public String docId {get;set;}
  
  public Membership_Onboarding_Document__c newDoc {get{
    if (newDoc == null) {
      newDoc = (Membership_Onboarding_Document__c)Membership_Onboarding_Document__c.sObjectType.newSObject(null, true);
    }
    return newDoc;
  }set;}
  
  public List<SelectOption> getClientIndustry() {
    return MembershipOnboardingToolUtils.getClientIndustry();
  }
  
  public List<SelectOption> getPermissiblePurpose() {
    return MembershipOnboardingToolUtils.getPermissiblePurpose(new Set<String>(), false);
  }
  
  public List<SelectOption> getProductToBeSold() {
    return MembershipOnboardingToolUtils.getProductToBeSold(new Set<String>(), false);
  }
  
  public List<SelectOption> getThirdParty() {
    return MembershipOnboardingToolUtils.getThirdParty(new Set<String>(), false);
  }
  
  public List<SelectOption> getAllLookupOptions() {
    List<SelectOption> masterList = new List<SelectOption>();
    for (SelectOption so : getClientIndustry()) {
      masterList.add(so);
    }
    for (SelectOption so : getProductToBeSold()) {
      masterList.add(so);
    }
    for (SelectOption so : getPermissiblePurpose()) {
      masterList.add(so);
    }
    for (SelectOption so : getThirdParty()) {
      masterList.add(so);
    }
    return masterList;
  }
  
  public List<String> selectedIndustries {get{
    if (selectedIndustries == null) {
      selectedIndustries = new List<String>();
    }
    return selectedIndustries;
  }set;}
  
  public List<String> selectedProducts {get{
    if (selectedProducts == null) {
      selectedProducts = new List<String>();
    }
    return selectedProducts;
  }set;}
  
  public List<String> selectedPurposes {get{
    if (selectedPurposes == null) {
      selectedPurposes = new List<String>();
    }
    return selectedPurposes;
  }set;}
  
  public List<String> selectedThirdParties {get{
    if (selectedThirdParties == null) {
      selectedThirdParties = new List<String>();
    }
    return selectedThirdParties;
  }set;}
  
  public List<optionWrapper> getAllOptions() {
    
    Map<String, optionWrapper> optMap = new Map<String, optionWrapper>();
    String key; 
    for (Membership_Onboarding_Allowed_Option__c opt : [SELECT Id, Client_Industry__c, Permissible_Purpose__c, Product_to_be_Sold__c, Third_Party_Involvement__c
                                                       FROM Membership_Onboarding_Allowed_Option__c
                                                       WHERE Client_Industry__c != null
                                                       AND Permissible_Purpose__c != null
                                                       AND Product_to_be_Sold__c != null
                                                       AND Third_Party_Involvement__c != null
                                                       ORDER BY Client_Industry__c, Product_to_be_Sold__c, Permissible_Purpose__c, Third_Party_Involvement__c]) {
      // now build the wrapper data...
      key = opt.Client_Industry__c + '|' + opt.Product_to_be_Sold__c + '|' + opt.Permissible_Purpose__c;
      if (!optMap.containsKey(key)) {
        optMap.put(key, new optionWrapper(opt.Client_Industry__c, opt.Product_to_be_Sold__c, opt.Permissible_Purpose__c));
      }
      optMap.get(key).thirdPartyMap.put(opt.Third_Party_Involvement__c, opt.Id);
      optMap.get(key).thirdParties.add(opt.Third_Party_Involvement__c);
    }
    List<String> optMapKeys = new List<String>(optMap.keySet());
    optMapKeys.sort();
    List<optionWrapper> output = new List<optionWrapper>();
    for (String optMapKey : optMapKeys) {
      output.add(optMap.get(optMapKey));
    }
    return output;
  }
  
  public PageReference addNewOptions() {
    if (!selectedIndustries.isEmpty() && 
        !selectedProducts.isEmpty() && 
        !selectedPurposes.isEmpty() && 
        !selectedThirdParties.isEmpty()) {
      //
      Membership_Onboarding_Allowed_Option__c opt;
      List<Membership_Onboarding_Allowed_Option__c> optList = new List<Membership_Onboarding_Allowed_Option__c>();
      for (String ind : selectedIndustries) {
        for (String prd : selectedProducts) {
          for (String pur : selectedPurposes) {
            for (String trd : selectedThirdParties) {
              opt = new Membership_Onboarding_Allowed_Option__c(
                Client_Industry__c = ind,
                Product_to_be_Sold__c = prd,
                Permissible_Purpose__c = pur,
                Third_Party_Involvement__c = trd
              );
              optList.add(opt);
            }
          }
        }
      }
      List<Database.SaveResult> srList = Database.insert(optList, false);
      for (Database.SaveResult sr : srList) {
        if (!sr.isSuccess()) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, 'Duplicate found in the list. It was not added.'));
        }
      }
    }
    else {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please complete all options.'));
    }
    return null;
  }
  
  public PageReference editOptions() {
    if (String.isNotBlank(optionId)) {
      Membership_Onboarding_Allowed_Option__c editOpt = [
        SELECT Id, Client_Industry__c, Product_to_be_Sold__c, Permissible_Purpose__c, Third_Party_Involvement__c
        FROM Membership_Onboarding_Allowed_Option__c
        WHERE Id = :optionId
        LIMIT 1
      ];
      Set<String> selIndustries = new Set<String>();
      Set<String> selProducts = new Set<String>();
      Set<String> selPurposes = new Set<String>();
      Set<String> selThirdParties = new Set<String>();
      // now build the selected options for this (i.e. all but the third party to multi select)
      for (Membership_Onboarding_Allowed_Option__c remainingOpt : [SELECT Id, Client_Industry__c, Product_to_be_Sold__c, Permissible_Purpose__c, Third_Party_Involvement__c
                                                                    FROM Membership_Onboarding_Allowed_Option__c
                                                                    WHERE Client_Industry__c = :editOpt.Client_Industry__c
                                                                    AND Product_to_be_Sold__c = :editOpt.Product_to_be_Sold__c
                                                                    AND Permissible_Purpose__c = :editOpt.Permissible_Purpose__c]) {
        //
        selIndustries.add(remainingOpt.Client_Industry__c);
        selProducts.add(remainingOpt.Product_to_be_Sold__c);
        selPurposes.add(remainingOpt.Permissible_Purpose__c);
        selThirdParties.add(remainingOpt.Third_Party_Involvement__c);
      }
      selectedIndustries = new List<String>(selIndustries);
      selectedProducts = new List<String>(selProducts);
      selectedPurposes = new List<String>(selPurposes);
      selectedThirdParties = new List<String>(selThirdParties);
    }
    return null;
  }
  
  public PageReference deleteOption() {
    if (String.isNotBlank(optionId)) {
      delete [SELECT Id FROM Membership_Onboarding_Allowed_Option__c WHERE Id = :optionId LIMIT 1];
    }
    return null;
  }
  
  public List<Membership_Onboarding_Document__c> getAllDocuments() {
    return [
      SELECT Id, Full_Detail__c, Type_of_Document__c, Lookup_Type__c, Lookup_Value__c, Not_Applicable_Document__c, Sort_Order__c,Document_URL__c
      FROM Membership_Onboarding_Document__c
      ORDER BY Lookup_Type__c, Lookup_Value__c, Type_of_Document__c
    ];
  }
  
  public PageReference addNewDocument() {
    if (String.isNotBlank(newDoc.Type_of_Document__c) && 
        String.isNotBlank(newDoc.Lookup_Type__c) && 
        String.isNotBlank(newDoc.Lookup_Value__c) &&
        String.isNotBlank(newDoc.Full_Detail__c)) {
      Membership_Onboarding_Document__c insDoc = newDoc.clone(true);
      // insDoc.Id = null;
      try {
        upsert insDoc;
        newDoc = new Membership_Onboarding_Document__c();
      }
      catch (Exception e) {
        if (e.getMessage().contains('DUPLICATE_VALUE')) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, 'Documents are already configured.'));
        }
        else {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
        }
      }
    }
    else {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please complete all options.'));
    }
    return null;
  }
  
  public PageReference editDocument() {
    if (String.isNotBlank(docId)) {
      newDoc = [
        SELECT Id, Full_Detail__c, Type_of_Document__c, Lookup_Type__c, Lookup_Value__c, Not_Applicable_Document__c, Sort_Order__c,Document_URL__c
        FROM Membership_Onboarding_Document__c
        WHERE Id = :docId
      ];
    }
    return null;
  }
  
  public PageReference cloneDocument() {
    if (String.isNotBlank(docId)) {
      Membership_Onboarding_Document__c tmpDoc = [
        SELECT Id, Full_Detail__c, Type_of_Document__c, Lookup_Type__c, Lookup_Value__c, Not_Applicable_Document__c, Sort_Order__c,Document_URL__c
        FROM Membership_Onboarding_Document__c
        WHERE Id = :docId
      ];
      newDoc = tmpDoc.clone(false,false,false,false);
    }
    return null;
  }
  
  public PageReference deleteDocument() {
    if (String.isNotBlank(docId)) {
      delete [SELECT Id FROM Membership_Onboarding_Document__c WHERE Id = :docId LIMIT 1];
    }
    return null;
  }
  
}