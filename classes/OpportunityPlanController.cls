/*==================================================================
 * Experian
 * Created By: Unknown
 * Created Date: Unknown
 *
 * Description: Controller to manage the creation of opportunity plans
 *
 * Modified Date          By                      Description Of Change
 * Oct 26th, 2015         Paul Kissick            Case 01139502: Opp Plan Owner permissions incorrect, also replaced messages with labels
 *
 *==================================================================*/
public class OpportunityPlanController {

  private final Opportunity_Plan__c opportunityPlan;

  public List<Boolean> clientGoal {get;set;}
  public List<Boolean> clientObjc {get;set;}

  public String pageTitle {get;set;}
  public boolean showSaveButton {get;set;}
  public boolean showOwner {get;set;}

  private boolean isEdit;

  Integer currentClientGoal = 1;
  Integer currentClientObjc = 1;

  public opportunityPlanController(ApexPages.StandardController stdController) {
    // grab parameters
    String oppId = ApexPages.currentPage().getParameters().get('oppId');

    // before doing anything, check if an existing opportunity exist,
    // if so, not show save button, and display error message
    List<Opportunity_Plan__c> oppPlan = [
      SELECT Id
      FROM Opportunity_Plan__c
      WHERE Opportunity_Name__c = :oppId
    ];

    opportunityPlan = (Opportunity_Plan__c)stdController.getRecord();

    showSaveButton = false;

    clientGoal = new List<Boolean>{true, false, false, false, false};
    clientObjc = new List<Boolean>{true, false, false, false, false};

    showOwner = true;

    if (ApexPages.currentPage().getParameters().containsKey('Id')) {
      showOwner = false;
    }

    if( oppId != null ){
      // if making a new page, and there's an existing one
      if( oppPlan.size() != 0 ){
        // not display save button
        showSaveButton = true;
        // display error message

        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Opp_Plan_One_Per_Opp )); // PK: Replaced error message with Label
      }
      isEdit = false;
      pageTitle = 'New Opportunity Plan';  // PK: TODO: Replace with Label? Or something else?

      // pull datas that are supposed to show up in a new opportunity plan page
      Opportunity opp = [
        SELECT Name, AccountId, CloseDate, OwnerId, Budget__c
        FROM Opportunity
        WHERE Id = :oppId
        LIMIT 1
      ];

      // this.opportunityPlan.OwnerId = opp.OwnerId; // PK: Case 01139502 - Replacing owner with the current user.
      opportunityPlan.OwnerId = UserInfo.getUserId();
      opportunityPlan.Name = 'OP-' + opp.Name;
      opportunityPlan.Opportunity_Name__c = opp.Id;
      opportunityPlan.Account_Name__c = opp.AccountId;
      opportunityPlan.Opportunity_Owner__c = opp.OwnerId;
      opportunityPlan.Opportunity_Client_Budget__c = opp.Budget__c;
      opportunityPlan.Opportunity_Expected_Close_Date__c = opp.CloseDate;
    }
    else{
      // if it is edit, whic means there's an existing record, show clientGoals and clientObjcs if they exist
      if(opportunityPlan.Client_Goal_1__c != null ) clientGoal[0] = true;
      if(opportunityPlan.Client_Goal_2__c != null ) clientGoal[1] = true;
      if(opportunityPlan.Client_Goal_3__c != null ) clientGoal[2] = true;
      if(opportunityPlan.Client_Goal_4__c != null ) clientGoal[3] = true;
      if(opportunityPlan.Client_Goal_5__c != null ) clientGoal[4] = true;
      adjustDisplay(clientGoal);
      currentClientGoal = getLastTrue(clientGoal) + 1; // next one as new one

      if(opportunityPlan.Sales_Objective_1__c != null ) clientObjc[0] = true;
      if(opportunityPlan.Sales_Objective_2__c != null ) clientObjc[1] = true;
      if(opportunityPlan.Sales_Objective_3__c != null ) clientObjc[2] = true;
      if(opportunityPlan.Sales_Objective_4__c != null ) clientObjc[3] = true;
      if(opportunityPlan.Sales_Objective_5__c != null ) clientObjc[4] = true;
      adjustDisplay(clientObjc);
      currentClientObjc = getLastTrue(clientObjc) + 1; // next one as new one;

      pageTitle = opportunityPlan.Name;
      oppId = ApexPages.currentPage().getParameters().get('Id');
      isEdit = true;
    }
  }

  public pageReference addClientGoal() {
    if (currentClientGoal < 5) {
      clientGoal[currentClientGoal] = true;
      currentClientGoal++;
      return null;
    }
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Opp_Plan_Five_Per_Section)); // PK: Replaced message with Label
    return null;
  }

  public pageReference addSalesObjective() {
    if (currentClientObjc < 5) {
      clientObjc[currentClientObjc] = true;
      currentClientObjc++;
      return null;
    }

    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Opp_Plan_Five_Per_Section )); // PK: Replaced message with Label
    return null;
  }

  public pageReference save() {
    // pop up error message if there's a gap
    if (noGap_ClientGoal() == false) {
      // pop up error message
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Opp_Plan_Client_Goals_Missing)); // PK: Replaced message with Label
      return null;
    }

    // pop up error message if there's a gap
    if (noGap_clientObjc() == false) {
      // pop up error message
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Opp_Plan_Sales_Objective_Missing)); // PK: Replaced message with Label
      return null;
    }

    // insert this.Opportunity_Plan__c;
    // if error happen in DML, catch message , show it on page
    // use page message
    try {
      if(isEdit == true) {
        update opportunityPlan;
      }
      else {
        insert opportunityPlan;
      }
    }
    catch (Exception e) {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage())); // PK: TODO: Consider improved handling of exceptions
      return null;
    }

    PageReference reRend = new PageReference('/' + opportunityPlan.Id);
    reRend.setRedirect(true);
    return reRend;
  }

  private boolean noGap_ClientGoal(){
    List<boolean> goal_has_content = new List<boolean>{false, false, false, false, false};
    if(opportunityPlan.Client_Goal_1__c != null ) goal_has_content[0] = true;
    if(opportunityPlan.Client_Goal_2__c != null ) goal_has_content[1] = true;
    if(opportunityPlan.Client_Goal_3__c != null ) goal_has_content[2] = true;
    if(opportunityPlan.Client_Goal_4__c != null ) goal_has_content[3] = true;
    if(opportunityPlan.Client_Goal_5__c != null ) goal_has_content[4] = true;

    Integer lastTrue = getLastTrue(goal_has_content);

    if (goal_has_content[0] == false && lastTrue > 0) {
      return false;
    }

    // from the first one to the last one that is true, if there's anything inbetween
    for (Integer i = 1; i <= lastTrue; i++) {
      if (goal_has_content[i] == false) {
        return false;   // if anything inbetween is false, there's a gap, return false
      }
    }

    // if anything inbetween is true, there's no gap, return true
    return true;
  }

  private boolean noGap_clientObjc(){
    List<boolean> objc_has_content = new List<boolean>{false, false, false, false, false};
    if(opportunityPlan.Sales_Objective_1__c != null ) objc_has_content[0] = true;
    if(opportunityPlan.Sales_Objective_2__c != null ) objc_has_content[1] = true;
    if(opportunityPlan.Sales_Objective_3__c != null ) objc_has_content[2] = true;
    if(opportunityPlan.Sales_Objective_4__c != null ) objc_has_content[3] = true;
    if(opportunityPlan.Sales_Objective_5__c != null ) objc_has_content[4] = true;

    Integer lastTrue = getLastTrue(objc_has_content);

    if (objc_has_content[0] == false && lastTrue > 0) {
      return false;
    }

    // from the first one to the last one that is true, if there's anything inbetween
    for (Integer i = 1; i <= lastTrue; i++) {
      if (objc_has_content[i] == false) {
        return false;   // if anything inbetween is false, there's a gap, return false
      }
    }

    // if anything inbetween is true, there's no gap, return true
    return true;
  }

  public pageReference cancel() {
    PageReference reRend = new PageReference('/' + opportunityPlan.Opportunity_Name__c);
    reRend.setRedirect(true);
    return reRend;
  }

  private Integer getLastTrue(List<Boolean> lis) {
    for (Integer i = lis.size()-1; i >= 0; i--) {
      if(lis[i] == true) return i;
    }
    return 0;
  }

  private void adjustDisplay(List<boolean> lis) {
    Integer lastTrue = getLastTrue(lis);
    for (Integer i = 0; i <= lastTrue; i++) {
      lis[i] = true;
    }
  }

}