global class ScheduleSetOldestScheduleDate implements Schedulable
{
  /*
  * Author:     Diego Olarte (Experian)
  * Description:  The following class is for scheduling the 'SetOldestScheduleDate.cls' class to run at specific intervals
  */  
  
  global ScheduleSetOldestScheduleDate(){}  
  global void execute(SchedulableContext ctx)
  {
    SetOldestScheduleDate batchToProcess = new SetOldestScheduleDate();
    database.executebatch(batchToProcess);
  }
}