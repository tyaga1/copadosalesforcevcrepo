public with sharing class ContentVersion_TriggerHandler {
    public ContentVersion_TriggerHandler() {
        
    }

    public static void LibraryMemberAlerts(List<ContentVersion> contentList){
        String defaultRecipientId = Label.Community_Default_Email_Recipient;
        List<Profile> profileList = [Select Id From Profile Where Name = 'Xperian Global Community Strategic'];
        List<EmailTemplate> templateIdList = [SELECT Id FROM EmailTemplate
                                                        WHERE DeveloperName = 'Content_Update_Notification'
                                                        ];
        String communityURL = '';
        ConnectApi.CommunityPage cPage = ConnectApi.Communities.getCommunities();
        for(ConnectApi.Community comm : cpage.communities){
            if(comm.Name == 'ResellerAndStrat'){
                communityURL  = comm.siteURL + '/s/librarydocumentlist';
            }
        }

        Set<Id> docIds = new Set<Id>();
        for(ContentVersion cv : contentList){
            docIds.add(cv.ContentDocumentId);
        }
        system.debug('The doc Ids: '+docIds);

        //Get a list of ContentWorkspaceDoc (junc object between library and Content Document) and create a necessary Maps
        List<ContentWorkspaceDoc> libContent = [Select Id, ContentWorkspaceId, ContentWorkspace.Name, ContentDocumentId, ContentDocument.Title From ContentWorkspaceDoc Where ContentDocumentId IN : docIds];
        system.debug('The contentworkspacedoc: '+libContent );
        Set<Id> libIds = new Set<Id>();
        Map<Id,List<Id>> contentLibIDMap = new Map<Id,List<Id>>();
        for(ContentWorkspaceDoc cwd: libContent){
            if(!contentLibIDMap.KeySet().Contains(cwd.ContentDocumentId)){
                contentLibIdMap.put(cwd.ContentDocumentId , new List<Id>());
            }
            contentLibIdMap.get(cwd.ContentDocumentId).add(cwd.ContentWorkspaceId);
            libIds.add(cwd.ContentWorkspaceId);
        }
        
        Map<Id,ContentDocument> contentMap = new Map<Id,ContentDocument>([Select Id, Title From ContentDocument Where Id IN :contentLibIdMap.KeySet()]);

        Map<Id,ContentWorkspace> LibMap = new Map<Id,ContentWorkspace>([Select Id, Name, (Select Id, ContentWorkspaceId, MemberId, MemberType, Member.ProfileId 
                                                        From ContentWorkspaceMembers
                                                        Where MemberType = 'U' AND Member.ProfileId = :profileList[0].Id)
                                                                     From ContentWorkspace Where Id IN :libIds]);
        
        
        List<Messaging.SingleEmailMessage> mailingList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SendEmailResult> mailingResults = new List<Messaging.SendEmailResult>();

        for(Id contentId : contentLibIdMap.KeySet()){            
            List<Id> recipientIDs = new List<Id>();
            String DocName = contentMap.get(contentId).Title;
            for(Id libID : contentLibIdMap.get(contentId)){
                for(ContentWorkSpaceMember cwsMember : LibMap.get(libID).ContentWorkspaceMembers){
                    recipientIDs.add(cwsMember.MemberId);
                }    
            }
            Set<Id> recipientIDset = new Set<Id>(recipientIDs);
            recipientIDs = new List<Id>(recipientIDset);
            
            Messaging.SingleEmailMessage email = CreateEmail(DocName, recipientIDs, defaultRecipientId , templateIdList[0].Id, contentId, communityURL);
            mailingList.add(email);
        }
        system.debug('The Mailing list' + mailingList);
        if(!mailingList.isEmpty()){
            mailingResults = Messaging.sendEmail(mailingList, false);
            system.debug('the sent email results: ' + mailingResults);
        } 

    }
    
    
    public static Messaging.SingleEmailMessage CreateEmail(String DocName, List<Id> recipientIDs, String defaultRecipientId, Id templateId, Id contentId, String communityURL){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTargetObjectId(defaultRecipientId);
        email.setBccAddresses(recipientIDs);
        email.setSubject('Salesforce Content Updated');
        email.setHTMLBody('The Document ' +  DocName + ' was uploaded/updated' + '<BR>' + '<a href=\"' + communityURL + '\">' + 'My Libraries' + '</a>');
        email.setPlainTextBody('The Document ' +  DocName + 'was uploaded/updated' + '/r/n' + communityURL);
        email.setTreatTargetObjectAsRecipient(false);
        email.setTreatBodiesAsTemplate(true);
        email.setTemplateId(templateId);
        system.debug('The Email: ' + email);
        return email;
    }
}