/*=============================================================================
 * Experian
 * Name: BatchUserRoleBuilder
 * Description: Build missing role hierarchy for new users (all employees) for global recognition project.

Requires: 1. Manager populated on User Record
          2. User is a Salesforce Platform licensed user
 
 * NOTES: All roles for this process must have a developer name of Z_AutoRole_Experian_Employee_Lxx

   DO NOT CREATE THIS MANUALLY, THE BATCH CAN DO THIS:
   BatchUserRoleBuilder b = new BatchUserRoleBuilder(new List<BatchUserRoleBuilder.RunProcess>{BatchUserRoleBuilder.RunProcess.BuildRoles},null);
   b.newRoleDepth = 11;
   Database.executeBatch(b);

 This allows the depth to be set to 11 instead of the original amount.

 It was decided to create this as an all in one class, which can be called either from a schedule to do the standard processes,
 or from the console with specific processes.


 *
 *
 * Created Date: 26 Aug 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 *

 =============================================================================*/

public class BatchUserRoleBuilder implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

  public Enum RunProcess {
    BuildRoles,         // 1. Create the roles we need for this process - Only call if we have to extend over 10
    AssignRolesByLevel, // 2. Starting with level 1, cycle through everyone without a role and assign the role.
    VerifyHierarchy,    // 3. Cycles through everyone and checks their role is consistent with their manager.
    MissingRoleUsers,   // 4. Anyone still without a role, return these in an exceptions report.
    UnassignAutoRoles,  // 5. Remove all auto assigned roles from users - Use in exceptional circumstances to clear down all automatic roles.
    ReparentAutoRoles,  // 6. Updates all auto created roles to be parented by the same root role, helps with deleting.
    DeleteAutoRoles,    // 7. Delete all the auto created roles from the system
    MissingParentRole   // 8. Used to call the alert if no Global_Internal role exists
  }

  private RunProcess currentProcess;
  @testVisible private UserRole ultimateParentRole;  // Holds the Global_Internal role reference
  private Integer roleDepth;            // Maximum depth for the role hierarchy - currently 10
  
  private Integer currentRoleLevel;     // Holds the current depth for the process
  private Integer totalScopeCount;      // Total results in the overall batch

  @testVisible private String autoRoleIdent = 'Z|AutoRole' + ((Test.isRunningTest()) ? 'Test ' : ' '); // Default start of the name - used to find all the system generated roles.

  @testVisible private String nameStart = autoRoleIdent + 'Experian Employee L';
  @testVisible private String devNameEquiv = nameStart.replace(' ','_').replace('|','_');

  public Integer newRoleDepth;          // Set to override and create new roles
  
  public List<RunProcess> remainingProcesses;
  public List<String> errorsFound;
  
  public Map<Id, Id> usersToNewRoleMap;

  private List<UserRole> checkRoles;
  
  @testVisible static String userEmailSuffix = '%test35235@experian.com'; // Created only for tests to limit the batches
  
  public BatchUserRoleBuilder() {
    
  }

  public BatchUserRoleBuilder (List<RunProcess> processOrder, Integer crl) {
    remainingProcesses = processOrder;
    currentRoleLevel = crl;
  }

  // Helper methods
  @testVisible private String addLevelToName (String roleName, Integer lvl) {
    return roleName + (String.valueOf(lvl)).leftPad(2).replace(' ','0');
  }

  @testVisible private Integer getLevelFromDevName (String devName, String devNameEquiv) {
    return Integer.valueOf(devName.removeStart(devNameEquiv));
  }

  //===========================================================================
  // Added the schedulable context to this class to keep things cleaner.
  //===========================================================================
  public void execute(SchedulableContext sc) {
    // Note, the processes listed are mapped in the start method to pull back a specific process/query to run each time.
    Database.executeBatch(
      new BatchUserRoleBuilder(
        new List<RunProcess>{
          RunProcess.AssignRolesByLevel,
          RunProcess.VerifyHierarchy,
          RunProcess.MissingRoleUsers
        },
        null
      ),
      ScopeSizeUtility.getScopeSizeForClass('BatchUserRoleBuilder')
    );
  }
  
  // Moving the init function out to help with testing!
  public void init() {
    errorsFound = new List<String>();

    totalScopeCount = 0;
    
    String devNameGlobalInternal = 'Global_Internal' + ((Test.isRunningTest()) ? '_Test' : '');

    List<UserRole> globalInternalRoleCheck = [
      SELECT Id
      FROM UserRole
      WHERE DeveloperName = :devNameGlobalInternal
      LIMIT 1
    ];

    // If no role found, clear the process list, and add the one we need.
    if (globalInternalRoleCheck.isEmpty()) {
      remainingProcesses = new List<RunProcess>{RunProcess.MissingParentRole};
    }
    else {
      ultimateParentRole = globalInternalRoleCheck[0];
    }

    checkRoles = getAllAutoRoles();
    if (!checkRoles.isEmpty()) {
      roleDepth = checkRoles.size(); // reset the roleDepth since we know it's larger
    }
    if (newRoleDepth == null) {
      newRoleDepth = roleDepth;
    }
  }

  //===========================================================================
  // Start the batch!
  //===========================================================================
  public Database.Querylocator start (Database.Batchablecontext bc) {
    
    init();

    if (remainingProcesses != null && !remainingProcesses.isEmpty()) {
      // We can run this same process multiple times for both of these
      if (remainingProcesses.get(0) == RunProcess.AssignRolesByLevel ||
          remainingProcesses.get(0) == RunProcess.VerifyHierarchy) {
        currentProcess = remainingProcesses.get(0);  // don't remove the current process from remaining Processes since we will be calling this up to 10 times!
      }
      else {
        currentProcess = remainingProcesses.remove(0);
      }

      if (currentProcess == RunProcess.BuildRoles) {
        return getTheRunningUser(); // The query doesn't matter, but we have to get to the execute part somehow!
      }

      if (currentProcess == RunProcess.AssignRolesByLevel) {
        return queryAssignRolesByLevel(currentRoleLevel); // This will be set at the end of the batch, if not known.
      }

      if (currentProcess == RunProcess.VerifyHierarchy) {
        return queryVerifyHierarchy(currentRoleLevel);
      }

      if (currentProcess == RunProcess.MissingRoleUsers) {
        return queryMissingRoleUsers();
      }

      if (currentProcess == RunProcess.UnassignAutoRoles) {
        return queryUnassignAutoRoles();
      }

      if (currentProcess == RunProcess.DeleteAutoRoles) {
        return queryDeleteAutoRoles();
      }

      if (currentProcess == RunProcess.ReparentAutoRoles) {
        return queryReparentAutoRoles();
      }

      if (currentProcess == RunProcess.MissingParentRole) {
        return getTheRunningUser();
      }

    }
    return getTheRunningUser();
  }

  //===========================================================================
  // Given the current level, return all employees that match based on the
  // manager's level, unless we are on level 1, then anyone without a manager
  // will be added here. In theory, this should only be 1 person, but it's
  // possible for others to go here.
  //===========================================================================
  private Database.QueryLocator queryAssignRolesByLevel(Integer lvl) {
    if (lvl == null) {
      currentRoleLevel = lvl = 1; // load the level into the class variable!
    }
    // Return everyone, with no role already, is a Platform user (via profile) and is active.
    String query = 'SELECT Id, ManagerId, UserRoleId '
      + ' FROM User '
      + ' WHERE UserRoleId = null '
      + ' AND Profile.UserLicense.Name = \'Salesforce Platform\' '
      + ' AND IsActive = true ';
    if (lvl == 1) {
      // Return everyone without a manager
      query += ' AND ManagerId = null ';
    }
    if (lvl > 1) {
      // Note, lvl is decremented below to find managers with the previous role level
      query += ' AND ManagerId != null ';
      query += ' AND Manager.UserRole.DeveloperName = \''+addLevelToName(devNameEquiv,lvl-1)+'\' ';
    }
    if (Test.isRunningTest()) {
      query += ' AND Email LIKE \''+userEmailSuffix+'\' ';
    }
    
    system.debug(query);
    
    return Database.getQueryLocator(query);
  }

  //===========================================================================
  // Find anyone who doesn't have a role, and is a platform user, and is active
  // and is linked to a manager and who's manager is not in the normal hierarchy.
  //===========================================================================
  private Database.QueryLocator queryMissingRoleUsers() {
    String query = 'SELECT Id, Name, ManagerId, UserRoleId, Manager.Name, Manager.UserRole.Name '
      + ' FROM User '
      + ' WHERE UserRoleId = null '
      + ' AND Profile.UserLicense.Name = \'Salesforce Platform\' '
      + ' AND IsActive = true '
      + ' AND ManagerId != null '
      + ' AND (NOT Manager.UserRole.DeveloperName LIKE \'' + devNameEquiv + '%\')'; // roles not consistent
    
    if (Test.isRunningTest()) {
      query += ' AND Email LIKE \''+userEmailSuffix+'\' ';
    }

    system.debug(query);
    
    return Database.getQueryLocator(query);
  }

  //===========================================================================
  // 
  //===========================================================================
  private Database.QueryLocator queryVerifyHierarchy(Integer lvl) {
    if (lvl == null) {
      currentRoleLevel = lvl = 1; // load the level into the class variable!
    }
    
    lvl--; // Decrement lvl to be 0 if 1,
    lvl = (lvl < 0) ? 0 : lvl; // Just checking.

    String roleDevNameMngr = addLevelToName(devNameEquiv, lvl);
    String roleDevNameUser = addLevelToName(devNameEquiv, lvl+1);
    
    String query = 'SELECT Id, Name, ManagerId, UserRoleId, Manager.Name, Manager.UserRoleId, Manager.UserRole.Name '
     + ' FROM User '
     + ' WHERE IsActive = true '
     + ' AND Profile.UserLicense.Name = \'Salesforce Platform\' '
     + ((lvl > 0) ? ' AND ( Manager.UserRole.DeveloperName = \'' + roleDevNameMngr + '\' OR UserRole.DeveloperName = \''+roleDevNameUser+'\' ) ' : ' AND ManagerId = null ')    
     + (Test.isRunningTest() ? ' AND Email LIKE \''+userEmailSuffix+'\' ' : '');
    
    /*  + ' AND UserRoleId != null ' */
    system.debug(query);

    return Database.getQueryLocator(query);
  }

  //===========================================================================
  // Get all users, which were assigned the auto created roles.
  //===========================================================================
  private Database.QueryLocator queryUnassignAutoRoles() {
    
    return Database.getQueryLocator(
      ' SELECT Id, UserRoleId ' +
      ' FROM User ' +
      ' WHERE UserRoleId IN ( '+
        ' SELECT Id ' +
        ' FROM UserRole ' +
        ' WHERE DeveloperName LIKE \''+ devNameEquiv + '%\'' +
      ')' + (Test.isRunningTest() ? ' AND Email LIKE \''+userEmailSuffix+'\' ' : '')
    );
  }

  //===========================================================================
  // Any roles, that are automatic, will be moved under the Global_Internal role
  // if not there already. They must not be assigned to any users.
  //===========================================================================
  private Database.QueryLocator queryReparentAutoRoles() {
    return Database.getQueryLocator(
      ' SELECT Id, ParentRoleId '+
      ' FROM UserRole '+
      ' WHERE DeveloperName LIKE \''+ devNameEquiv + '%\' '+
      ' AND Id NOT IN (SELECT UserRoleId FROM User) '+
      ' AND PortalType = \'None\' '+
      ' AND ParentRoleId != \'' + ultimateParentRole.Id +'\' '+
      ' ORDER BY ParentRoleId ' 
    );
  }

  //===========================================================================
  // Any roles, with Z_AutoRole in the Dev Name are automatic
  //===========================================================================
  private Database.QueryLocator queryDeleteAutoRoles() {
    return Database.getQueryLocator(
      ' SELECT Id, ParentRoleId '+
      ' FROM UserRole '+
      ' WHERE DeveloperName LIKE \''+ devNameEquiv + '%\' '+
      ' AND Id NOT IN (SELECT UserRoleId FROM User) '+
      ' AND PortalType = \'None\' '+
      ' AND ParentRoleId = \'' + ultimateParentRole.Id +'\' '+  // Only diff with queryReparentAutoRoles
      ' ORDER BY ParentRoleId '
    );
  }

  //===========================================================================
  // Just return the current user, nothing else.
  //===========================================================================
  private Database.QueryLocator getTheRunningUser() {
    return Database.getQueryLocator([
      SELECT Id, Email
      FROM User
      WHERE Id = :UserInfo.getUserId()
    ]);
  }

  //===========================================================================
  // Check the current process, and run the relevant method
  //===========================================================================
  public void execute (Database.BatchableContext bc, List<sObject> scope) {

    totalScopeCount += scope.size();

    // Process the roles to build - if missing
    if (currentProcess == RunProcess.BuildRoles) {
      processBuildRoles();
    }

    // Assign the roles by level - Incremented level for each batch scheduled.
    if (currentProcess == RunProcess.AssignRolesByLevel) {
      processAssignRolesByLevel(scope, currentRoleLevel);
    }

    // Verify the hierarchy is consistent (user -> manager levels)
    if (currentProcess == RunProcess.VerifyHierarchy) {
      processVerifyHierarchy(scope);
    }

    // Report on anyone still without a role, due to managers roles, most likely.
    if (currentProcess == RunProcess.MissingRoleUsers) {
      processMissingRoleUsers(scope);
    }

    // Cleanup process to remove the auto roles from anyone
    if (currentProcess == RunProcess.UnassignAutoRoles) {
      processUnassignAutoRoles(scope);
    }

    // Reparent all the auto roles to be under the "Global Internal" role, helps delete them!
    if (currentProcess == RunProcess.ReparentAutoRoles) {
      processReparentAutoRoles(scope);
    }

    // Cleanup process to delete the auto roles from the system
    if (currentProcess == RunProcess.DeleteAutoRoles) {
      processDeleteAutoRoles(scope);
    }

    if (currentProcess == RunProcess.MissingParentRole) {
      // add an error to then pull onto an email later!
      errorsFound.add('No Role found with the Dev Name of Global_Internal. Unable to continue!');
    }
  }
  
  //===========================================================================
  // Return all AutoRole based roles
  //===========================================================================
  private List<UserRole> getAllAutoRoles() {
    String devName = devNameEquiv + '%';
    return (List<UserRole>)Database.query(
      ' SELECT Id, Name, DeveloperName, ParentRoleId ' +
      ' FROM UserRole ' +
      ' WHERE DeveloperName LIKE :devName ' +
      ' ORDER BY DeveloperName ASC '
    );
  }

  //===========================================================================
  // Create the roles needed for the hierarchy.
  //===========================================================================
  public void processBuildRoles() {

    // we know the depth required, so lets check for existing roles
    Integer startLevel = 0;

    Id lastParentRoleId = ultimateParentRole.Id;

    Map<Integer, UserRole> lvlToRoleMap = new Map<Integer, UserRole>();

    if (!checkRoles.isEmpty()) {
      // cycle through and find the lowest...
      for (UserRole ur : checkRoles) {
        Integer lvl = getLevelFromDevName(ur.DeveloperName, devNameEquiv);
        if (lvl > startLevel) {
          startLevel = lvl;
          lastParentRoleId = ur.Id;
        }
        lvlToRoleMap.put(lvl, ur);
      }
    }
    
    system.debug('Start Level is : '+startLevel);
    system.debug('Role Depth is : '+roleDepth);

    if (startLevel >= newRoleDepth) {
      return;
    }

    for (Integer lev = startLevel+1; lev <= newRoleDepth; lev++) {
      lvlToRoleMap.put(
        lev,
        new UserRole(
          Name = addLevelToName(nameStart, lev),
          ParentRoleId = lastParentRoleId
        )
      );
    }

    upsert lvlToRoleMap.values();

    for (Integer i : lvlToRoleMap.keySet()) {
      // 'i' will be the level, so reparent based on the level-1, if the parentrole is the lastparentroleid...
      if (lvlToRoleMap.get(i).ParentRoleId == lastParentRoleId) {
        if (lvlToRoleMap.containsKey(i-1)) {
          lvlToRoleMap.get(i).ParentRoleId = lvlToRoleMap.get(i-1).Id;
        }
      }
      if (lvlToRoleMap.get(i).Name.startsWith(autoRoleIdent)) {
        lvlToRoleMap.get(i).Name = lvlToRoleMap.get(i).Name.removeStart(autoRoleIdent);
      }
    }
    upsert lvlToRoleMap.values();
  }

  //===========================================================================
  // Given the current role level, assign this to all the users in the scope.
  //===========================================================================
  public void processAssignRolesByLevel (List<sObject> scope, Integer roleLevel) {

    String roleDevName = addLevelToName(devNameEquiv, roleLevel);

    // get the current level, and get the role associated with this level...
    List<UserRole> rolesToAssign = [
      SELECT Id, Name, DeveloperName
      FROM UserRole
      WHERE DeveloperName = :roleDevName
    ];
    
    // No role found, to return an error!
    if (rolesToAssign.isEmpty() || rolesToAssign.size() > 1) {
      errorsFound.add('Role with Dev Name '+roleDevName+' not found.');
      return;
    }

    UserRole roleToAssign = rolesToAssign.get(0);

    // all users found will now get this role...
    for (User u : (List<User>)scope) {
      u.UserRoleid = roleToAssign.Id;
    }
    update scope;

  }

  //===========================================================================
  // 
  //===========================================================================
  private void setNewRole (Map<Id, User> userMap, Id userId, Id roleId) {
    if (!userMap.containsKey(userId)) {
      userMap.put(userId, new User(Id = userId));
    }
    userMap.get(userId).UserRoleId = roleId;
  }

  //===========================================================================
  // 
  //===========================================================================
  public void processVerifyHierarchy (List<sObject> scope) {

    if (usersToNewRoleMap == null) {
      usersToNewRoleMap = new Map<Id, Id>();
    }

    // Get all the roles into a map with the levels
    Map<Integer, UserRole> lvlToRoleMap = new Map<Integer, UserRole>();
    Map<Id, Integer> roleToLevelMap = new Map<Id, Integer>();

    for (UserRole ur : getAllAutoRoles()) {
      Integer lvl = getLevelFromDevName(ur.DeveloperName, devNameEquiv);
      lvlToRoleMap.put(lvl, ur);
      roleToLevelMap.put(ur.Id, lvl);
    }

    Map<Id, User> usersToUpdateMap = new Map<Id, User>();

    // cycle through all the users, and check their role level and the managers role level are consistent, otherwise replace their current role.
    for (User u : (List<User>)scope) {

      // Anyone with no manager, should be at the top (i.e. role 1)
      if (u.ManagerId == null) {
        if (u.UserRoleId != lvlToRoleMap.get(1).Id) {
          setNewRole(usersToUpdateMap, u.Id, lvlToRoleMap.get(1).Id);
          usersToNewRoleMap.put(u.Id, lvlToRoleMap.get(1).Id);
          errorsFound.add('NOTE: '+u.Name + ' should now be in level 1');
        }
      }
      else {
        // get the managers roleid, and check this is above the current user...

        if (u.Manager.UserRoleId == null) {
          errorsFound.add('ALERT: Manager for '+u.Name+' ('+u.Id+') has no role: '+  u.Manager.Name);
        }
        else if (!roleToLevelMap.containsKey(u.Manager.UserRoleId)) {
          // Manager role not in the proper hierarchy, add warning about this.
          errorsFound.add(
            'WARNING: Manager for '+u.Name+' ('+u.Id+') has role outside of standard hierarchy: '+
            u.Manager.Name + ', Role: ' + u.Manager.UserRole.Name
          );
        }
        else {
          Id holdingManagerRoleId = (!usersToNewRoleMap.containsKey(u.ManagerId)) ? u.Manager.UserRoleId : usersToNewRoleMap.get(u.ManagerId);
          Integer currentUserRoleLevelCheck = (roleToLevelMap.get(holdingManagerRoleId) + 1);

          if (currentUserRoleLevelCheck != roleToLevelMap.get(u.UserRoleId)) {
            // Role is part of hierarchy, and managers role + 1 (e.g. 3+1) is not the current users role (e.g. 4)
            // roles are not consistent, so set the user to the correct role, based on their managers role.
            setNewRole(usersToUpdateMap, u.Id, lvlToRoleMap.get(currentUserRoleLevelCheck).Id);
            usersToNewRoleMap.put(u.Id, lvlToRoleMap.get(currentUserRoleLevelCheck).Id);
            errorsFound.add('NOTE: '+u.Name + ' should now be in level '+currentUserRoleLevelCheck+' with a new manager of '+u.Manager.Name);
          }
        }
      }
    }

    List<Database.SaveResult> srList = Database.update(usersToUpdateMap.values(), false);
    for (Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        errorsFound.addAll(BatchHelper.parseErrors(sr.getErrors()));
      }
    }

  }

  //===========================================================================
  // For every user misisng a role, add an error, which will be sent later.
  //===========================================================================
  public void processMissingRoleUsers (List<sObject> scope) {
    for (User u : (List<User>)scope) {
      errorsFound.add('Manager for '+u.Name+' ('+u.Id+') has role outside of standard hierarchy: '+ u.Manager.Name + ', Role: ' + u.Manager.UserRole.Name);
    }
  }

  //===========================================================================
  // For all users found, set their role to Empty (NULL).
  //===========================================================================
  public void processUnassignAutoRoles(List<sObject> scope) {
    List<User> usersToUpdate = new List<User>();
    for (User u : (List<User>)scope) {
      usersToUpdate.add(
        new User(
          Id = u.Id,
          UserRoleId = null
        )
      );
    }
    List<Database.SaveResult> srList = Database.update(usersToUpdate, false);
    for (Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        errorsFound.addAll(BatchHelper.parseErrors(sr.getErrors()));
      }
    }
  }

  //===========================================================================
  // For all roles found, set their parent to the Global Internal role.
  //===========================================================================
  public void processReparentAutoRoles(List<sObject> scope) {
    List<UserRole> rolesToReparent = new List<UserRole>();
    for (UserRole ur : (List<UserRole>)scope) {
      rolesToReparent.add(
        new UserRole(
          Id = ur.Id,
          ParentRoleId = ultimateParentRole.Id
        )
      );
    }
    List<Database.SaveResult> srList = Database.update(rolesToReparent, false);
    for (Database.SaveResult sr : srList) {
      if (!sr.isSuccess()) {
        errorsFound.addAll(BatchHelper.parseErrors(sr.getErrors()));
      }
    }
  }

  //===========================================================================
  // Delete all roles found.
  //===========================================================================
  public void processDeleteAutoRoles(List<sObject> scope) {
    List<Database.DeleteResult> drList = Database.delete(scope, false);
    for (Database.DeleteResult dr : drList) {
      if (!dr.isSuccess()) {
        errorsFound.addAll(BatchHelper.parseErrors(dr.getErrors()));
      }
    }
  }

  //===========================================================================
  // Finish - Send any error emails, and prepare the next process, if needed.
  //===========================================================================
  public void finish (Database.BatchableContext bc) {
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchUserRoleBuilder', false);

    if (!errorsFound.isEmpty() || Test.isRunningTest()) {
      bh.batchHasErrors = true;
      bh.emailBody += 'Errors found: \n\n'+String.join(errorsFound, '\n');
      system.debug(bh.emailBody);
    }

    bh.sendEmail();

    if (currentProcess == RunProcess.AssignRolesByLevel || currentProcess == RunProcess.VerifyHierarchy) {
      if (currentRoleLevel >= roleDepth) {
        // remove from remaining processes
        remainingProcesses.remove(0);
        currentRoleLevel = null; // Reset this for the next possible process
      }
      else {
        currentRoleLevel++;
      }
    }

    if (!remainingProcesses.isEmpty() && !Test.isRunningTest()) {
      system.scheduleBatch(new BatchUserRoleBuilder(remainingProcesses, currentRoleLevel), 'User - BuildRoleHierarchy - '+remainingProcesses.get(0).name()+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchUserRoleBuilder'));
    }

  }

}