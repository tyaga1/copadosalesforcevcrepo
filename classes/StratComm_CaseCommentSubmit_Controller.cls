public with sharing class StratComm_CaseCommentSubmit_Controller {

	@AuraEnabled
    public static Boolean checkIfCaseIsClosed(Id caseID) {
        Case thisCase = [SELECT id, isClosed FROM Case WHERE id=:caseID];
        if (thisCase.isClosed)
        {
            return true;
        }
        return false;
    }


	@AuraEnabled
	public static integer saveComment(String newComment, Id caseId){
		try{
			caseComment newCommentRec = new caseComment();
			newCommentRec.ParentId = caseId;
			newCommentRec.commentBody = newComment;
			newCommentRec.isPublished = true;
			upsert newCommentRec;
			return 1;

			}catch(exception e){
				system.debug(e.getMessage());
				return 0;
			}
	}

	@AuraEnabled
	public static void saveAttachment(Id caseId, String fileName, String base64Data, String contentType)
	{
		try{
			Attachment newAttachment = new Attachment();
			newAttachment.parentId = caseId;
			newAttachment.body = EncodingUtil.base64Decode(base64Data);
			newAttachment.name = fileName;
			newAttachment.contentType = contentType;

			system.debug('charlie attachment to insert' + newAttachment);

			insert newAttachment;
		}
		catch (exception e)
		{
			system.debug('exception thrown in saveAttachment' + e.getMessage());
		}
		
	}

	/*
	// NOT COMPLETE
	@AuraEnabled
	public static void saveAttachments(Id caseId, String arrayJSON)
	{
		try{
			attachment a = (Attachment)JSON.deserializeStrict(arrayJSON, Attachment.class);
		}
		catch (exception e)
		{
			system.debug('exception thrown in saveAttachments' + e.getMessage());
		}
	}
	*/
}