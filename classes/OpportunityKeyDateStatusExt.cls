/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityKeyDateStatusExt
 * Description: T-258164: Check opportunity dates with revenue schedules
 * Created Date: 10th April, 2014
 * Created By: Naresh Kr Ojha (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Jul 28, 2016(QA)             Cristian Torres              CRM2:W-005403: Display message for No longer with company and Inactive contacts
 * Aug 19th, 2016               Paul Kissick                 CRM2:W-005403: Fixing formatting
 =====================================================================*/

public with sharing class OpportunityKeyDateStatusExt {
  
  public List<String> errorList {get;set;}
  public List<String> errorContactRolesList {get;set;}
  //Constructor
  public OpportunityKeyDateStatusExt(ApexPages.StandardController controller) {
    init();    
    errorList = OpportunityUtility.checkOpportunityDates(controller.getRecord().ID);
    if (errorList.size() > 0) {
      String err = '';
      for (String e : errorList) {
        system.debug('ERROR ' + e);
        err += e + ', ';
        //err.substring(0, err.length()-2)
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, e);
        ApexPages.addMessage(msg);
      }  
    }
    
    errorContactRolesList = OpportunityUtility.checkOpportunityContactRoles(controller.getRecord().ID);
    if (errorContactRolesList.size() > 0){
      String err = '';
      for (String e: errorContactRolesList){
        system.debug('ERROR ' + e);
        err += e + ', ';
        //err.substring(0, err.length()-2)
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, e);
        ApexPages.addMessage(msg);
      }
    }
  }
  
  private void init() {
    errorList = new List<String>();
  }
}