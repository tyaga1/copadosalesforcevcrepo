/**=====================================================================
 * Name: AccountPlan_Competitors_Controller
 * Description: See Case #01949954 AP - List views
 * Created Date: May 26th, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By           Description of the update
 * May 26th, 2016        James Wills           Case #01949954 AP - List views: Created
 * July 19th, 2016       James Wills           Case #01061020 - AP - Competitors (Current Providers)
 * Oct. 19th, 2016       James Wills           Case #02154716 PDF - Updated to allow PDF to display all Current Providers without need for list expansion.
 * Oct. 26th, 2016       James Wills           Case #01983126 Added toLabel() translations to SOQL
 * Jan. 25th, 2017       James Wills           Case #01999757 Added Constructor
  =====================================================================*/

global with sharing Class AccountPlan_Competitors_Controller{

  public ID accountPlanId {get;set;}
  public Account_Plan__c accountPlan {get;set;}

  
  public ID selectedID {get;set;}
  public ID selectedIdToDelete {get;set;}

  public String userCurrency {
    get{return userInfo.getDefaultCurrency();}
    set;
  }
  public Boolean showMessages {get; set;}
  
  public Integer pageOffset=0;
  
  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer relatedListSize = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Competitors_List__c;

  public Integer competitorsLimit=relatedListSize; 
  public Integer additionalCompetitorsToView {
    get{
      if(endAPCompetitorNumber+relatedListSize > totalAPCompetitors){
        return totalAPCompetitors-endAPCompetitorNumber;
      } else {
        return relatedListSize;
      } 
    }
  set;}
  
  public Integer startAPCompetitorNumber {get;set;}
  public Integer endAPCompetitorNumber {get;set;}
  public Integer totalAPCompetitors {get;set;}

  
  public Integer cpPageOffset=0;

  //This is the initial number of records to display for this list and then the increment for displaying more records (if applicable).
  public Integer cpRelatedListSize = (Integer)Account_Plan_Related_List_Sizes__c.getInstance().Account_Plan_Current_Providers_List__c;

  public Integer currentProvidersLimit=cpRelatedListSize; 
  public Integer additionalCurrentProvidersToView {
    get{
      if(endAPCurrentProviderNumber+cpRelatedListSize > totalAPCurrentProviders){
        return totalAPCurrentProviders-endAPCurrentProviderNumber;
      } else {
        return cpRelatedListSize;
      } 
    }
  set;}
    
  public Integer startAPCurrentProviderNumber {get;set;}
  public Integer endAPCurrentProviderNumber {get;set;}
  public Integer totalAPCurrentProviders {get;set;}


  global Class AccountPlanCompetitorsWrapper implements Comparable{
    public String ID {get;set;}
    public String AccountPlanCompetitor {get;set;}
    public String Competitor {get;set;}
    public String CompetitorName {get;set;}
    public String ClientHistoryWithCompetitor {get;set;}
    public String AreasCompetingAgainstExperian {get;set;}
    public List<String> CompetitorAdvantages  {get;set;}
    public List<String> ExperianAdvantages  {get;set;}      

    global Integer compareTo(Object compareTo) {
       AccountPlanCompetitorsWrapper compareToCompetitor = (AccountPlanCompetitorsWrapper)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (AccountPlanCompetitor > compareToCompetitor.AccountPlanCompetitor) {
         // Set return value to a positive value.
         returnValue = 1;
       } else if (AccountPlanCompetitor < compareToCompetitor.AccountPlanCompetitor) {
         // Set return value to a negative value.
         returnValue = -1;
       }
       return returnValue;
    }

  }

  public List <AccountPlanCompetitorsWrapper> accountPlanCompetitorsWrapperList {
    get{
      return buildAccountPlanCompetitorsWrapperList();  
    }
    set;
  }


  global Class AccountPlanCurrentProvidersWrapper implements Comparable{
    public String ID {get;set;}
    public String AccountPlanCurrentProviderID {get;set;} 
    public String AccountPlanCurrentProvider {get;set;}
    public String Account {get;set;}
    public String AccountName {get;set;}
    public Date CurrentProviderContractEndDate {get;set;}       
    public Decimal EstimatedAmount {get;set;}
    public String CompetitorProducts {get;set;}
    public String BusinessUnit {get;set;}
    public String ExperianProvider {get;set;}

    global Integer compareTo(Object compareTo) {
       AccountPlanCurrentProvidersWrapper compareToCurrentProvider = (AccountPlanCurrentProvidersWrapper)compareTo;
       // The return value of 0 indicates that both elements are equal.
       Integer returnValue = 0;
       if (CurrentProviderContractEndDate > compareToCurrentProvider.CurrentProviderContractEndDate) {
         // Set return value to a positive value.
         returnValue = 1;
       } else if (CurrentProviderContractEndDate < compareToCurrentProvider.CurrentProviderContractEndDate) {
         // Set return value to a negative value.
         returnValue = -1;
       }
       return returnValue;
    }

  }

  public List <AccountPlanCurrentProvidersWrapper> accountPlanCurrentProvidersWrapperList {
    get{
      return buildAccountPlanCurrentProvidersWrapperList(false);  
    }
    set;
  }


  public List <AccountPlanCurrentProvidersWrapper> accountPlanCurrentProvidersWrapperList_PDF {
    get{
      return buildAccountPlanCurrentProvidersWrapperList(true);  
    }
    set;
  }


  public AccountPlan_Competitors_Controller( ApexPages.Standardcontroller std){

    accountPlan = (Account_Plan__c) std.getRecord();
    accountPlanId = accountPlan.id;
    
    showMessages = false;
  }

  public AccountPlan_Competitors_Controller(){

  }

  public void competitorsTabUpdateCookies(){
  
    AccountPlanUtilities.setAccountPlanCookies(accountPlanId, Label.ACCOUNTPLANNING_CompetitionTab);
      
  }
 
  public PageReference newAccountPlanCompetitor(){
  
    competitorsTabUpdateCookies();
    
    PageReference newAccountPlanCompetitorPageRef = new PageReference('/' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Competitor_Rec__c + 
                                                                  '/e?' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Competitor__c + '=' + accountPlan.Name + 
                                                                  '&' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Competitor__c + '_lkid=' + accountPlanId + 
                                                                  '&saveURL=' + accountPlanId +
                                                                  '&retURL=' + accountPlanId);
    return newAccountPlanCompetitorPageRef ;
    
  }


  public PageReference editAccountPlanCompetitor(){
    competitorsTabUpdateCookies();
    PageReference accountPlanCompetitorToEdit = new PageReference('/' + selectedId);
    //accountPlanCompetitorToEdit.setRedirect(true);
    return accountPlanCompetitorToEdit;
  }


  public PageReference editAccountPlanCurrentProvider(){
    competitorsTabUpdateCookies();
    PageReference accountPlanCurrentProviderToEdit = new PageReference('/' + selectedId);
    return accountPlanCurrentProviderToEdit;
  }


  public List<AccountPlanCompetitorsWrapper> buildAccountPlanCompetitorsWrapperList(){
    List<AccountPlanCompetitorsWrapper> accountPlanCompetitorsWrapperList = new List<AccountPlanCompetitorsWrapper>();
  
    List<Account_Plan_Competitor__c> accountPlanCompetitorsList = [SELECT ID, Name, Competitor__c, Competitor__r.Name, Client_History_with_Competitor__c, Areas_Competing_Against_Experian__c,
                                                                  Comp_Adv_1__c, Comp_Adv_2__c, Comp_Adv_3__c, Comp_Adv_4__c, Comp_Adv_5__c, 
                                                                  Exp_Adv_1__c, Exp_Adv_2__c, Exp_Adv_3__c, Exp_Adv_4__c, Exp_Adv_5__c 
                                                                  FROM Account_Plan_Competitor__c WHERE Account_Plan__c = :accountPlanID
                                                                  LIMIT :CompetitorsLimit
                                                                  ];

    for(Account_Plan_Competitor__c apCompetitor : accountPlanCompetitorsList){
      AccountPlanCompetitorsWrapper conWrap = new AccountPlanCompetitorsWrapper();
      conWrap.ID                            = apCompetitor.ID;
      conWrap.AccountPlanCompetitor         = apCompetitor.Name; 
      conWrap.Competitor                    = apCompetitor.Competitor__c;
      conWrap.CompetitorName                = apCompetitor.Competitor__r.Name;
      conWrap.ClientHistoryWithCompetitor   = apCompetitor.Client_History_with_Competitor__c;
      conWrap.AreasCompetingAgainstExperian = apCompetitor.Areas_competing_against_Experian__c;
      conWrap.CompetitorAdvantages          = concatenateAdvantages('Competitor', apCompetitor);
      conWrap.ExperianAdvantages            = concatenateAdvantages('Experian', apCompetitor);                                      

      accountPlanCompetitorsWrapperList.add(conWrap);    
    }
  
    accountPlanCompetitorsWrapperList.sort();
  
    //Now set values used for Account Plan Competitor list navigation
    if(accountPlanCompetitorsWrapperList.size()>0){
      startAPCompetitorNumber = pageOffSet+1;
      endAPCompetitorNumber = pageOffset + accountPlanCompetitorsWrapperList.size();  
      AggregateResult[] APCompetitors = [SELECT COUNT(Name)TotalAPCompetitors FROM Account_Plan_Competitor__c WHERE Account_Plan__c = :accountPlanID];
      totalAPCompetitors  = (Integer)APCompetitors[0].get('TotalAPCompetitors');    
    } else {
      startAPCompetitorNumber = 0;
      endAPCompetitorNumber = 0;  
      totalAPCompetitors = 0;
    }
  
    return accountPlanCompetitorsWrapperList;
  }



  public List<AccountPlanCurrentProvidersWrapper> buildAccountPlanCurrentProvidersWrapperList(Boolean isForPDF){
    List<AccountPlanCurrentProvidersWrapper> accountPlanCurrentProvidersWrapperList = new List<AccountPlanCurrentProvidersWrapper>();
    List<Current_Provider__c> accountPlanCurrentProvidersList = new List<Current_Provider__c>();
    
    
    List<Account_Account_Plan_Junction__c> accountPlanHierarchyList = [SELECT Account__c from Account_Account_Plan_Junction__c where Account_Plan__c = :accountPlanId];
  
    List<ID> accountsInHierarchy = new List<ID>();
    for(Account_Account_Plan_Junction__c  acc : accountPlanHierarchyList){
       accountsInHierarchy.add(acc.Account__c);
    }
    
    if(isForPDF==false){
      accountPlanCurrentProvidersList = [SELECT ID, Name, Account__c, Account__r.Name, Current_Provider__r.Name, Current_Provider_Contract_End_Date__c,
                                         Estimated_Amount__c, Competitor_Product_s__c, toLabel(Business_Unit__c), toLabel(Experian_Provider__c)
                                         FROM Current_Provider__c WHERE Account__c IN :accountsInHierarchy
                                         AND Current_Provider_Contract_End_Date__c >= :Date.Today()
                                         LIMIT :CompetitorsLimit];
    } else {
       accountPlanCurrentProvidersList = [SELECT ID, Name, Account__c, Account__r.Name, Current_Provider__r.Name, Current_Provider_Contract_End_Date__c,
                                         Estimated_Amount__c, Competitor_Product_s__c, Business_Unit__c, Experian_Provider__c
                                         FROM Current_Provider__c WHERE Account__c IN :accountsInHierarchy
                                         AND Current_Provider_Contract_End_Date__c >= :Date.Today()];
    }

    for(Current_Provider__c apCurrentProvider : accountPlanCurrentProvidersList){
      AccountPlanCurrentProvidersWrapper cpWrap = new AccountPlanCurrentProvidersWrapper();
      cpWrap.ID                               = apCurrentProvider.ID;
      cpWrap.AccountPlanCurrentProviderID     = apCurrentProvider.Name; 
      cpWrap.AccountPlanCurrentProvider       = apCurrentProvider.Current_Provider__r.Name;
      cpWrap.Account                          = apCurrentProvider.Account__c;
      cpWrap.AccountName                      = apCurrentProvider.Account__r.Name;
      cpWrap.CurrentProviderContractEndDate   = apCurrentProvider.Current_Provider_Contract_End_Date__c;
      if(apCurrentProvider.Estimated_Amount__c!= NULL){
        cpWrap.EstimatedAmount                 = apCurrentProvider.Estimated_Amount__c;
      } else {
        cpWrap.EstimatedAmount                  = 0.0;
      }
      cpWrap.CompetitorProducts               = apCurrentProvider.Competitor_Product_s__c;
      cpWrap.BusinessUnit                     = apCurrentProvider.Business_Unit__c;
      cpWrap.ExperianProvider                 = apCurrentProvider.Experian_Provider__c;
      accountPlanCurrentProvidersWrapperList.add(cpWrap);
    }
  
    accountPlanCurrentProvidersWrapperList.sort();
  
    //Now set values used for Account Plan Competitor list navigation
    if(accountPlanCurrentProvidersWrapperList.size()>0){
      startAPCurrentProviderNumber = cpPageOffSet+1;
      endAPCurrentProviderNumber = cpPageOffset + accountPlanCurrentProvidersWrapperList.size();  
      AggregateResult[] APCurrentProviders = [SELECT COUNT(Name)TotalAPCurrentProviders FROM Current_Provider__c WHERE  Account__c IN :accountsInHierarchy
                                              AND Current_Provider_Contract_End_Date__c >= :Date.Today()];
      totalAPCurrentProviders  = (Integer)APCurrentProviders[0].get('TotalAPCurrentProviders');    
    } else {
      startAPCurrentProviderNumber = 0;
      endAPCurrentProviderNumber = 0;  
      totalAPCurrentProviders = 0;
    }
  
    return accountPlanCurrentProvidersWrapperList;
  }

  
  public List<String> concatenateAdvantages(String listType, Account_Plan_Competitor__c apCompetitor){
    
    List<String> advantagesList = new List<String>();
    
    if(listType == 'Competitor'){       
       
       if(apCompetitor.Comp_Adv_1__c!=null){advantagesList.add('1. ' + apCompetitor.Comp_Adv_1__c);}
       if(apCompetitor.Comp_Adv_2__c!=null){advantagesList.add('2. ' + apCompetitor.Comp_Adv_2__c);}
       if(apCompetitor.Comp_Adv_3__c!=null){advantagesList.add('3. ' + apCompetitor.Comp_Adv_3__c);}
       if(apCompetitor.Comp_Adv_4__c!=null){advantagesList.add('4. ' + apCompetitor.Comp_Adv_4__c);}
       if(apCompetitor.Comp_Adv_5__c!=null){advantagesList.add('5. ' + apCompetitor.Comp_Adv_5__c);}  
    
    } else {
     
       if(apCompetitor.Exp_Adv_1__c!=null){advantagesList.add('1. ' + apCompetitor.Exp_Adv_1__c);}
       if(apCompetitor.Exp_Adv_2__c!=null){advantagesList.add('2. ' + apCompetitor.Exp_Adv_2__c);}
       if(apCompetitor.Exp_Adv_3__c!=null){advantagesList.add('3. ' + apCompetitor.Exp_Adv_3__c);}
       if(apCompetitor.Exp_Adv_4__c!=null){advantagesList.add('4. ' + apCompetitor.Exp_Adv_4__c);}
       if(apCompetitor.Exp_Adv_5__c!=null){advantagesList.add('5. ' + apCompetitor.Exp_Adv_5__c);}  
       
    }

    return advantagesList;

  }

  

  public PageReference viewAccountPlanCompetitorListFull(){  
    CompetitorsTabUpdateCookies();  
    PageReference newAccountPlanCompetitorPageRef = new PageReference('/' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Competitor_Rec__c + 
                                                                  '?rlid=' + Custom_Fields_Ids__c.getInstance().AP_Account_Plan_Competitor__c.mid(2,15) + '&id=' + accountPlanId);
    return newAccountPlanCompetitorPageRef;
  }


  public void seeMoreCompetitors(){   
    CompetitorsLimit+=additionalCompetitorsToView;
  }


  public PageReference doDeleteAccountPlanCompetitor(){    
    competitorsTabUpdateCookies();
    Schema.DescribeSObjectResult describeResult = Account_Plan_Competitor__c.SObjectType.getDescribe();    
    String accountPlanObjectType = describeResult.getName();
    
    showMessages = true;
    
    AccountPlanUtilities.deleteAccountPlanRelatedListEntry(selectedIdToDelete, accountPlanObjectType, Label.ACCOUNTPLANNING_CompetitionTab);
       
    return null;

  }

  //public PageReference doDeleteAccountPlanCurrentProvider(){    
    //competitorsTabUpdateCookies();
    //Schema.DescribeSObjectResult describeResult = Current_Provider__c.SObjectType.getDescribe();    
    //String accountPlanObjectType = describeResult.getName();
    //AccountPlanUtilities.deleteAccountPlanRelatedListEntry(selectedIdToDelete, accountPlanObjectType, Label.ACCOUNTPLANNING_CompetitionTab);
    //return null;
  //}

  public PageReference viewAccountPlanCurrentProvidersListFull(){  
    CompetitorsTabUpdateCookies();  

    PageReference newAccountPlanCurrentProvidersPageRef = Page.AccountPlanCurrentProviders; 

    return newAccountPlanCurrentProvidersPageRef;

  }


  public void seeMoreCurrentProviders(){   
    currentProvidersLimit+=additionalCurrentProvidersToView;
  }

  public PageReference backToAccountPlan(){  
    PageReference accountPlan = new PageReference('/' + accountPlanId ); 
    accountPlan.setRedirect(true); 
    return accountPlan;      
  }



}