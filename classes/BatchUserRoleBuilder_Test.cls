/*=============================================================================
 * Experian
 * Name: BatchUserRoleBuilder_Test
 * Description: Test class for the BatchUserRoleBuilder class 
 * Created Date: 13 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * June21st 2017      Manoj Gopu            Fixed test class Failure for June 22nd release Making isdataadmin to true to avoid future runs with in the batch class 
 =============================================================================*/

@isTest
private class BatchUserRoleBuilder_Test {
  
  //===========================================================================
  // 
  //===========================================================================
  static testMethod void testMissingParentRole() {
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.MissingParentRole
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    // system.assert(b.errorsFound.size() > 0); // This returns a null reference, so we need another way of checking this.
    
  }

  //===========================================================================
  // testSetup creates 5 roles, this process should add 2 more. Test there are 7.
  //===========================================================================
  static testMethod void testBuildRoles() {
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.BuildRoles
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    b.newRoleDepth = 7;
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    String devNameLike = b.devNameEquiv + '%';
    system.assertEquals(7, [SELECT COUNT() FROM UserRole WHERE DeveloperName LIKE :devNameLike]);
    
  }
  
  //===========================================================================
  // Tests that anyone without a manager, will be assigned level 1 role.
  //===========================================================================
  static testMethod void testAssignRolesByLevel1() {
    
    system.assertEquals(1, [SELECT COUNT() FROM User WHERE ManagerId = null AND UserRoleId = null AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix]);
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.AssignRolesByLevel
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    Test.startTest();
    IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(True);
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM User WHERE ManagerId = null AND UserRole.Name = 'Experian Employee L01' AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix]);
    
  }
  
  //===========================================================================
  // Tests that those with a manager in level 1 is assigned a L2 role
  //===========================================================================
  static testMethod void testAssignRolesByLevel2() {
    
    User lvl1User = [
      SELECT Id, UserRoleId
      FROM User 
      WHERE ManagerId = null 
      AND UserRoleId = null 
      AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix
      LIMIT 1
    ];
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.AssignRolesByLevel
    };
    
    BatchUserRoleBuilder bTmp = new BatchUserRoleBuilder();
    bTmp.init();
    bTmp.processAssignRolesByLevel(new List<User>{lvl1User}, 1);
    
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, 2);
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM User WHERE UserRole.Name = 'Experian Employee L02' AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix]);
    
  }
  
  //===========================================================================
  // Verify Hierarchy test
  //===========================================================================
  static testMethod void testVerifyHierarchyNoRoles() {
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.VerifyHierarchy
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    Test.startTest();
    IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(True);
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM User WHERE ManagerId = null AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix AND UserRoleId != null]);
    
  }
  
  //===========================================================================
  // Verify Hierarchy test with roles assigned
  //===========================================================================
  static testMethod void testVerifyHierarchyWithRolesLvl1() {
    
    User lvl1User = [
      SELECT Id, UserRoleId
      FROM User 
      WHERE ManagerId = null 
      AND UserRoleId = null 
      AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix
      LIMIT 1
    ];
    
    User lvl2User = [
      SELECT Id, UserRoleId
      FROM User 
      WHERE ManagerId = :lvl1User.Id 
      AND UserRoleId = null 
      AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix
      LIMIT 1
    ];

    BatchUserRoleBuilder bTmp = new BatchUserRoleBuilder();
    bTmp.init();
    bTmp.processAssignRolesByLevel(new List<User>{lvl1User}, 1);
    bTmp.processAssignRolesByLevel(new List<User>{lvl2User}, 2);
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.VerifyHierarchy
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, 2);
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
  }
  
  //===========================================================================
  // 
  //===========================================================================
  static testMethod void testUnassignAutoRoles() {
    
    User lvl1User = [
      SELECT Id, UserRoleId
      FROM User 
      WHERE ManagerId = null 
      AND UserRoleId = null 
      AND Email LIKE :BatchUserRoleBuilder.userEmailSuffix
      LIMIT 1
    ];
    BatchUserRoleBuilder bTmp = new BatchUserRoleBuilder();
    bTmp.init();
    bTmp.processAssignRolesByLevel(new List<User>{lvl1User}, 1);
    
    system.assertEquals(1, [SELECT COUNT() FROM User WHERE Id = :lvl1User.Id AND UserRoleId != null]);
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.UnassignAutoRoles
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    system.assertEquals(1, [SELECT COUNT() FROM User WHERE Id = :lvl1User.Id AND UserRoleId = null]);
    
  }
  
  //===========================================================================
  // This should change the parent of all the auto roles to the ultimate parent.
  //===========================================================================
  static testMethod void testReparentAutoRoles() {
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.ReparentAutoRoles
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    Id ultParId = [SELECT Id FROM UserRole WHERE Name = 'Global Internal Test'].Id;
    
    system.assertEquals(5, [SELECT COUNT() FROM UserRole WHERE ParentRoleId = :ultParId]);
    
  }
  
  //===========================================================================
  // This should delete the system generated auto roles.
  //===========================================================================
  static testMethod void testDeleteAutoRoles() {
    
    // Reset the parent of the auto roles to the ultimate parent.
    
    Id ultParId = [SELECT Id FROM UserRole WHERE Name = 'Global Internal Test'].Id;
    List<UserRole> autoRoles = [
      SELECT Id, ParentRoleId
      FROM UserRole
      WHERE DeveloperName LIKE 'Z_AutoRoleTest_Experian_Employee%'
    ];
    for (UserRole ur : autoRoles) {
      ur.ParentRoleId = ultParId;
    }
    
    system.runAs(new User(Id = UserInfo.getUserId())) {
      update autoRoles;
    }
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.DeleteAutoRoles
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
    String devNameLike = b.devNameEquiv + '%';
    system.assertEquals(0, [SELECT COUNT() FROM UserRole WHERE DeveloperName LIKE :devNameLike]);
    
  }
  
  //===========================================================================
  // 
  //===========================================================================
  static testMethod void testMissingRoleUsers() {
    
    List<BatchUserRoleBuilder.RunProcess> procs = new List<BatchUserRoleBuilder.RunProcess>{
      BatchUserRoleBuilder.RunProcess.MissingRoleUsers
    };
    BatchUserRoleBuilder b = new BatchUserRoleBuilder(procs, null);
    
    Test.startTest();
    
    Database.executeBatch(b);
    
    Test.stopTest();
    
  }
  
  static testMethod void testSchedule() {
    
    Test.startTest();
    IsDataAdmin__c isDateAdmin = Test_Utils.insertIsDataAdmin(True);
    system.schedule('Test User Role Builder - Test', '0 0 0 * * ?', new BatchUserRoleBuilder());
    
    Test.stopTest();
    
  }
  
  @testSetup
  private static void setupData() {
    // Initialise data for test here.
    
    IsDataAdmin__c ida = Test_Utils.insertIsDataAdmin(true);
    
    // Create a load of users, with no roles, and create a hierarchy.
    
    system.runAs(new User(Id = UserInfo.getUserId())) {
      UserRole globIntTest = new UserRole(Name = 'Global Internal Test');
      insert globIntTest;
      
      List<UserRole> autoRoles = new List<UserRole>();
      
      UserRole lvl1 = new UserRole(Name = 'Z|AutoRoleTest Experian Employee L01', ParentRoleId = globIntTest.Id);
      insert lvl1;
      autoRoles.add(lvl1);
      
      UserRole lvl2 = new UserRole(Name = 'Z|AutoRoleTest Experian Employee L02', ParentRoleId = lvl1.Id);
      insert lvl2;
      autoRoles.add(lvl2);
      
      UserRole lvl3 = new UserRole(Name = 'Z|AutoRoleTest Experian Employee L03', ParentRoleId = lvl2.Id);
      insert lvl3;
      autoRoles.add(lvl3);
      
      UserRole lvl4 = new UserRole(Name = 'Z|AutoRoleTest Experian Employee L04', ParentRoleId = lvl3.Id);
      insert lvl4;
      autoRoles.add(lvl4);
      
      UserRole lvl5 = new UserRole(Name = 'Z|AutoRoleTest Experian Employee L05', ParentRoleId = lvl4.Id);
      insert lvl5;
      autoRoles.add(lvl5);
      
      for (UserRole ur : autoRoles) {
        ur.Name = ur.Name.removeStart('Z|AutoRoleTest ');
      }
      update autoRoles;
      
    }
    
    system.runAs(new User(Id = UserInfo.getUserId())) {
      
      // Let's create some users, and use the 'standard platform' profile, which should exist since we cannot delete it.
      
      Profile stdPlatProf = [
        SELECT Id
        FROM Profile
        WHERE Name = 'Standard Platform User'
        LIMIT 1
      ];
      
      List<User> newUsers = Test_Utils.createUsers(stdPlatProf, BatchUserRoleBuilder.userEmailSuffix.removeStart('%'), 'kjjhsd1', 5);
      
      insert newUsers;
      for (Integer ind = 1; ind < newUsers.size(); ind++) {
        newUsers.get(ind).ManagerId = newUsers.get(ind-1).Id;
      }
      update newUsers;
      
    }
    
    delete ida;
    
  }
  
  
}