/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SupportController {
    global SupportController() {

    }
    webService static void DeleteAccessTokenByUserId(String userId) {

    }
    webService static String GetAccessTokenByUserId(String userId) {
        return null;
    }
    webService static List<User> GetUsersByEMail(String userEmail) {
        return null;
    }
    webService static String getAccessToken() {
        return null;
    }
}
