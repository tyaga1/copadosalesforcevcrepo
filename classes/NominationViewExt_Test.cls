/*=============================================================================
 * Experian
 * Name: NominationViewExt_Test
 * Description:Test class to cover NominationViewExt
 * Created Date: 
 * Created By: 
 *
 * Date Modified      Modified By           Description of the update
 * 
 =============================================================================*/

@isTest
private class NominationViewExt_Test {
  
  @isTest
  private static void testNominationHistory() {
    Test.startTest();
    
    Nomination__c l2pending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level2SpotAward')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l2pending));
    nomView.getNominationHistory();
    nomView.checkOwnerIsUser('test');
    // This doesn't return anything, but adding just for coverage.
    
    system.assertEquals(1, nomView.errorMessages.size());
    
    nomView.approveNomination();
    system.assertEquals(1, nomView.errorMessages.size());
    nomView.upgradeToLevel3Ind();
    system.assertEquals(1, nomView.errorMessages.size());
    nomView.rejectNomination();
    system.assertEquals(1, nomView.errorMessages.size());
    nomView.rejectWithBadge();
    system.assertEquals(1, nomView.errorMessages.size());
    system.assert(nomView.edit() != null);
    
    nomView.downgradeTeamToLevel2();
    nomView.wonNomination();
    nomView.runnerUpNomination();
    nomView.getOtherTeamNominations();
    
    List<Nomination__c> lstNom = nomView.getChildNominations();
    NominationViewExt.CustomFieldHistory objField = new NominationViewExt.CustomFieldHistory();
    objField.CreatedDate = system.now();
    objField.fieldApiName  = 'Name';
    objField.createdBy = 'test';
    objField.oldValue = 'test1';
    objField.newValue = 'test2';
    objField.getFieldLabel();
    objField.getCreatedDateStr();
  }

  @isTest
  private static void testLevel2Approval() {
    Test.startTest();
    
    Nomination__c l2pending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level2SpotAward')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l2pending));
      nomView.currentRecord.Spot_Award_Amount__c = Nomination__c.Spot_Award_Amount__c.getDescribe().getPicklistValues().get(0).getValue();
      
      system.assertEquals(UserInfo.getUserId(), nomView.getOwnerDetails().Id);
      
      nomView.approveNomination();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l2pending.Id AND Status__c = :NominationHelper.NomConstants.get('Approved')]);
    }
    
  }
  
  @isTest
  private static void testLevel2To3Upgd() {
    Test.startTest();
    
    Nomination__c l2pending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level2SpotAward')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l2pending));
      nomView.upgradeToLevel3Ind();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c 
                              WHERE Id = :l2Pending.Id 
                              AND Status__c = :NominationHelper.NomConstants.get('PendingApproval') 
                              AND Type__c = :NominationHelper.NomConstants.get('Level3Individual') ]);
    }
    
  }
  
  
  @isTest
  private static void testLevel2Rejection() {
    Test.startTest();
    Nomination__c l2pending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level2SpotAward')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l2pending));
      nomView.currentRecord.Rejection_Reason__c = nomView.getRejectReasons().get(0).val;
      nomView.currentRecord.Notes__c = 'Some notes about why I rejected.';
      nomView.rejectNomination();
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l2pending.Id AND Status__c = :NominationHelper.NomConstants.get('Rejected')]);
    }
  }

  @isTest
  private static void testLevel2RejectWithBadge() {
    Test.startTest();
    Nomination__c l2pending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level2SpotAward')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l2pending));
      nomView.currentRecord.Rejection_Reason__c = nomView.getRejectReasons().get(0).val;
      nomView.currentRecord.Notes__c = 'Some notes about why I rejected.';
      nomView.rejectWithBadge();
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l2pending.Id AND Status__c = :NominationHelper.NomConstants.get('RejectedBadge')]);
    }
  }

  @isTest
  private static void testLevel3aApproval() {
    Test.startTest();
    
    Nomination__c l3apending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level3Individual')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l3apending));
      nomView.currentRecord.H1_or_H2__c = Nomination__c.H1_or_H2__c.getDescribe().getPicklistValues().get(0).getValue();
      
      
      nomView.approveNomination();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l3apending.Id AND Status__c = :NominationHelper.NomConstants.get('PendingPanelApproval')]);
      nomView.wonNomination();   
    }
    
  }
  @isTest
  private static void testLevel3aPanelApproval() {
    Test.startTest();
    
    Nomination__c l3apending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level3Individual')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l3apending));
      nomView.currentRecord.H1_or_H2__c = Nomination__c.H1_or_H2__c.getDescribe().getPicklistValues().get(0).getValue();
      
      
      nomView.approveNomination();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l3apending.Id AND Status__c = :NominationHelper.NomConstants.get('PendingPanelApproval')]);
      nomView.runnerUpNomination();
    }
    
  }
  
  @isTest
  private static void testLevel3aTo2Dngd() {
    Test.startTest();
    
    Nomination__c l3apending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level3Individual')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l3apending));
      
      nomView.downgradeToLevel2();
      
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c 
                              WHERE Id = :l3apending.Id 
                              AND Status__c = :NominationHelper.NomConstants.get('PendingApproval') 
                              AND Type__c = :NominationHelper.NomConstants.get('Level2SpotAward') ]);
    }
    
  }
  
  
  @isTest
  private static void testLevel3aRejection() {
    Test.startTest();
    Nomination__c l3apending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level3Individual')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l3apending));
      nomView.currentRecord.Rejection_Reason__c = nomView.getRejectReasons().get(0).val;
      nomView.currentRecord.Notes__c = 'Some notes about why I rejected.';
      nomView.rejectNomination();
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l3apending.Id AND Status__c = :NominationHelper.NomConstants.get('Rejected')]);
    }
  }

  @isTest
  private static void testLevel3aRejectWithBadge() {
    Test.startTest();
    Nomination__c l3apending = [
      SELECT Id, OwnerId, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level3Individual')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.managEmail)) {
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l3apending));
      nomView.currentRecord.Rejection_Reason__c = nomView.getRejectReasons().get(0).val;
      nomView.currentRecord.Notes__c = 'Some notes about why I rejected.';
      nomView.rejectWithBadge();
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l3apending.Id AND Status__c = :NominationHelper.NomConstants.get('RejectedBadge')]);
    }
  }

  @isTest
  private static void testLevel3bMasterApproval() {
    
    Test.startTest();
    
    Nomination__c l3bpendingMaster = [
      SELECT Id, OwnerId, Project_Sponsor__c, Status__c, Type__c, Master_Nomination__c, Upgraded_to_Level_3__c, Downgraded_to_Level_2__c, Spot_Award_Amount__c
      FROM Nomination__c 
      WHERE Type__c = :NominationHelper.NomConstants.get('Level3Team')
      AND Status__c = :NominationHelper.NomConstants.get('PendingApproval')
      AND Master_Nomination__c = null
    ];
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.projSponEmail)) {
      NominationViewExt nomView = new NominationViewExt(new ApexPages.StandardController(l3bpendingMaster));
      nomView.approveNomination();
      nomView.getNominationHistory();
      system.assertEquals(1, [SELECT COUNT() FROM Nomination__c WHERE Id = :l3bpendingMaster.Id AND Status__c = :NominationHelper.NomConstants.get('PendingManagerApproval')]);
    }
    
  }

  
  @testSetup
  private static void setupData() {
    
    NominationTestHelper_Test.createTestUsers();
    
    NominationTestHelper_Test.createRecognitionBadges();
    
    system.runAs(NominationTestHelper_Test.testUsers.get(NominationTestHelper_Test.dataAdminEmail)) {
      
      NominationTestHelper_Test.createNominations(
        NominationHelper.NomConstants.get('PendingApproval'), 
        NominationHelper.NomConstants.get('Level2SpotAward'), 
        1,
        false
      );
      
      NominationTestHelper_Test.createNominations(
        NominationHelper.NomConstants.get('PendingApproval'), 
        NominationHelper.NomConstants.get('Level3Individual'), 
        1,
        false
      );
      
      NominationTestHelper_Test.createNominations(
        NominationHelper.NomConstants.get('Submitted'), 
        NominationHelper.NomConstants.get('Level3Team'), 
        4,
        true
      );
      
      // Fix the team nomination master to pending approval.
      Nomination__c fixTeamNom = [SELECT Id, Status__c FROM Nomination__c WHERE Type__c = :NominationHelper.NomConstants.get('Level3Team') AND Master_Nomination__c = null AND Status__c = :NominationHelper.NomConstants.get('Submitted')];
      fixTeamNom.Status__c = NominationHelper.NomConstants.get('PendingApproval');
      update fixTeamNom;
      
      
    }
    
  }
  
}