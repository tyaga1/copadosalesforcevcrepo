/**=====================================================================
 * Appirio, Inc
 * Name: ScheduledApexOnCase_Test
 * Description: Class to verify the functionality of ScheduledApexOnCase_Test
 *             (installed from Asset)
 * Created Date: 27 Apr 2015
 * Created By: Noopur (Appirio)
 *
 * Date Modified               Modified By                  Description of the update
 * May 13th 2015               Arpita Bose(Appirio)         T-375163: Updated for calulating Record_time__c on Case
 * Sep 09th 2015               Naresh kr Ojha               Updated method ScheduledApexOnCase_Test to test all the scenarios.
 * Nov 17th, 2015              Paul Kissick                 Case 01244121: Fixing for changes to parent class
 * Apr 7th, 2016               Paul Kissick                 Case 01932085: Fixing Test User Email Domain
 * Aug 4th, 2016               Paul Kissick                 Case 02092488: Fixing test due to split into batch
 *========================================================================*/
@isTest
private class ScheduledApexOnCase_Test {

  static String CRON_EXP = '0 0 0 15 3 ? 2022';

  static testMethod void testSchedule() {
    //String timeOfRun = '0 0 * * * ?'; //every hour is scheduling
    String jobId = System.schedule('ScheduleApexClassTest-'+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduledApexOnCase());

    // Get the information from the CronTrigger API object
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];

    // Verify the expressions are the same
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run
    system.assertEquals(0, ct.TimesTriggered);

    // Verify the next time the job will run
    system.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
  }

  static testMethod void testBatch() {
    BusinessHours bh = [SELECT Id FROM BusinessHours WHERE isDefault = true LIMIT 1];
    List<Case> lstCases = new List<Case>();
    lstCases.add(createTestCase('Test Case 1', bh));
    lstCases.add(createTestCase('Test Case 2', bh));

    Case newCase = createTestCase('Test Case 1', bh);
    newCase.Business_Hours_Afternoon__c = bh.ID;
    newCase.X2_Hours_In_In_Progress__c = true;
    newCase.Status = BatchCaseAgeUpdate.CASE_STATUS_IN_PROGRESS;
    newCase.Case_Age__c = 5;
    lstCases.add(newCase);
    insert lstCases;

    for (Case cs: lstCases) {
      if (cs.Business_Hours_Afternoon__c != null) {
        system.assertNotEquals(null, cs.Case_Age__c, 'Case_Age__c should not be null!');
      }
      else {
        system.assertEquals(null, cs.Case_Age__c, 'Case_Age__c should be null.');
      }
      system.assert(!cs.Age_Calculated__c);
      system.assert(!cs.IsClosed);
    }

    lstCases[0].Status = BatchCaseAgeUpdate.CASE_STATUS_ON_HOLD;
    update lstCases[0];

    //Test ScheduledApexOnCase class
    Test.startTest();

    Database.executeBatch(new BatchCaseAgeUpdate(), 200);

    Test.stopTest();

    lstCases = [
      SELECT CreatedDate, Is_Age_Calculating__c, Age_Calculated__c, IsClosed, Case_Age__c, Status,
             X2_Hours_In_In_Progress__c, Recording_Time__c
      FROM Case
      WHERE Id IN :lstCases
    ];

    for (Case cs : lstCases) {
      system.assert(cs.Case_Age__c != null);
      if (cs.IsClosed){
        system.assert(cs.Age_Calculated__c);
      }
      else {
        system.assert(!cs.Age_Calculated__c);
      }
    }

    for (Case cs: lstCases) {
      if (cs.Status == BatchCaseAgeUpdate.CASE_STATUS_IN_PROGRESS) {
        system.assert(cs.Recording_Time__c != null);
      }
      else if (cs.Status == BatchCaseAgeUpdate.CASE_STATUS_ON_HOLD) {
        system.assert(cs.Recording_Time__c != null);
      }
    }
  }

  private static Case createTestCase(String subject, BusinessHours bh) {
    String EMSRecordTypeId = DescribeUtility.getRecordTypeIdByName('Case', 'EMS');
    
    Case cs = new Case(
      BusinessHoursId = bh.Id,
      Subject = subject,
      Origin = 'Phone',
      Status = BatchCaseAgeUpdate.CASE_STATUS_IN_PROGRESS,
      Age_Calculated__c = false,
      RecordtypeId = EMSRecordTypeId
    );
    return cs;
  }

  @testSetup
  static void createData() {
    // create users
    Profile p = [SELECT Id FROM Profile WHERE Name = :Constants.PROFILE_SYS_ADMIN];
    //List<User> lstUser = Test_Utils.createUsers(p, 'test1234@gmail.com', 'T-AE', 1);
    //insert lstUser;
    User user1 = Test_Utils.createUser(p, 'test1234@experian.com', 'T-AE');
    List<User> lstUser = new List<User>();
    lstUser.add(user1);
    insert lstUser;

    system.runAs(lstUser[0]) {
      CaseTeamRole cst = new CaseTeamRole();
      cst.Name = 'Decider';
      cst.AccessLevel = 'Read';
      insert cst;
    }

    Global_Settings__c custSetting = Test_Utils.insertGlobalSettings();
    custSetting.Case_Access_Request_TeamRole__c = 'Decider';
    update custSetting;

  }

}