/*=============================================================================
 * Experian
 * Name: OpportunityCloneExt
 * Description: CRM2:W-005338: Extension to handle deep cloning of an opportunity.
 * Created Date: 14 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 18 Jul 2016        Paul Kissick          CRM2:W-005338: Removed Partners from the process. Kept failing due to permissions for Standard Sales Users
 * 22 Jul 2016        Paul Kissick          CRM2:W-005338: Removing specific fields from the clone process.
 * 09 Sep 2016        Paul Kissick          CRM2:W-005338: Create new Field Sets on Opportunity and Opp Line Item, to handle fields not to clone.
 * 17 Oct 2016        Paul Kissick          Case 02165894: Adding support for owner being emptied. Also added checks on the Account or Team to allow cloning.
 * May 11 2017        Sanket Vaidya         Case 02322497: Clone Opportunities does not work [Setting converted_lead_id to null for cloned opportunity]
 =============================================================================*/

public with sharing class OpportunityCloneExt {
  
  ApexPages.StandardController stdCon;
  Opportunity currentRecord;
  
  public User currentUser {get {
    if (currentUser == null) {
      currentUser = [
        SELECT CPQ_User__c
        FROM User
        WHERE Id = :UserInfo.getUserId()
      ]; 
    }
    return currentUser;
  } set;}
  
  //===========================================================================
  // If the user has access to either the account or the opp (team), then this 
  // is allowed to clone.
  //===========================================================================
  public Boolean allowedClone {get{
    if (allowedClone == null) {
      allowedClone = (checkRecordAccess(currentRecord.AccountId) || checkRecordAccess(currentRecord.Id));
    }
    return allowedClone;
  }set;}
  
  public Opportunity newOpp {get;set;}
  public List<OpportunityLineItemWrapper> availableOppLineItems {get;set;}
  public List<OpportunityContactRoleWrapper> availableOppContRoles {get;set;}
  public List<CompetitorWrapper> availableCompetitors {get;set;}
  public List<OpportunityTeamMemberWrapper> availableOppTeamMembs {get;set;}
  public List<OpportunitySplitWrapper> availableOppSplits {get;set;}
  
  public OpportunityCloneExt(ApexPages.StandardController con) {
    stdCon = con;
    if (!Test.isRunningTest()) {
      stdCon.addFields(new String[]{'AccountId'});
    }
    currentRecord = (Opportunity)stdCon.getRecord();
  }
  
  //===========================================================================
  // This resets the probability to the default for the stage when saved.
  //===========================================================================
  private Decimal populateProbability(String stageName) {
    List<OpportunityStage> oppStages = [
      SELECT DefaultProbability
      FROM OpportunityStage
      WHERE IsActive = true
      AND ApiName = :stageName
      LIMIT 1
    ];
    if (oppStages.isEmpty()) {
      return null;
    }
    return oppStages[0].DefaultProbability;
  }
  
  //===========================================================================
  // Adding support to ensure the user has access to records for this opp
  //===========================================================================
  private Boolean checkRecordAccess(Id recId) {
    UserRecordAccess ura = [
      SELECT RecordId, HasReadAccess, HasEditAccess 
      FROM UserRecordAccess 
      WHERE UserId = :UserInfo.getUserId() 
      AND RecordId = :recId
    ];
    return ura.HasEditAccess;
  }
  
  //===========================================================================
  // Loads the existing information on the opp into vars
  //===========================================================================
  public PageReference prepareCloning() {
    
    Id tmpId = currentRecord.Id;
    
    availableOppLineItems = new List<OpportunityLineItemWrapper>();
    availableOppContRoles = new List<OpportunityContactRoleWrapper>();
    availableCompetitors = new List<CompetitorWrapper>();
    availableOppTeamMembs = new List<OpportunityTeamMemberWrapper>();
    availableOppSplits = new List<OpportunitySplitWrapper>();
    
    Set<String> oppFields = new Set<String>();
    for (String oppField : Opportunity.sObjectType.getDescribe().fields.getMap().keySet()) {
      oppFields.add(oppField.toLowerCase());
    }
    
    Set<String> oppLineFields = new Set<String>();
    for (String oppField : OpportunityLineItem.sObjectType.getDescribe().fields.getMap().keySet()) {
      oppLineFields.add(oppField.toLowerCase());
    }
    
    if (Opportunity.sObjectType.getDescribe().fieldsets.getMap().containsKey('Opportunity_Clone_Ignore')) {
      for (Schema.FieldSetMember fsm : Opportunity.sObjectType.getDescribe().fieldsets.getMap().get('Opportunity_Clone_Ignore').getFields()) {
        oppFields.remove(fsm.getFieldPath().toLowerCase());
      }
    }
    
    if (OpportunityLineItem.sObjectType.getDescribe().fieldsets.getMap().containsKey('Opportunity_Clone_Ignore')) {
      for (Schema.FieldSetMember fsm : OpportunityLineItem.sObjectType.getDescribe().fieldsets.getMap().get('Opportunity_Clone_Ignore').getFields()) {
        oppLineFields.remove(fsm.getFieldPath().toLowerCase());
      }
    }
    
    // Query all available fields on the related objects.
    String queryString = 'SELECT ' + String.join(new List<String>(oppFields), ', ');
    queryString += ',(SELECT '+ String.join(new List<String>(oppLineFields), ', ') +' FROM OpportunityLineItems)';
    queryString += ',(SELECT '+ String.join(new List<String>(OpportunityContactRole.sObjectType.getDescribe().fields.getMap().keySet()), ', ') +' FROM OpportunityContactRoles)';
    queryString += ',(SELECT '+ String.join(new List<String>(Competitor__c.sObjectType.getDescribe().fields.getMap().keySet()), ', ') +' FROM Competitors__r)';
    queryString += ',(SELECT '+ String.join(new List<String>(OpportunityTeamMember.sObjectType.getDescribe().fields.getMap().keySet()), ', ') +' FROM OpportunityTeamMembers)';
    queryString += ',(SELECT '+ String.join(new List<String>(OpportunitySplit.sObjectType.getDescribe().fields.getMap().keySet()), ', ') +' FROM OpportunitySplits)';
    queryString += ' FROM Opportunity WHERE Id = :tmpId';
    
    system.debug(queryString);
    
    Opportunity holdingOpp;
    
    try {
      holdingOpp = (Opportunity)Database.query(queryString);
    }
    catch (Exception e) {
      system.debug(e.getMessage());
    }
    
    // Clone the queried opp into a new one
    newOpp = holdingOpp.clone(false, false, false, false);
    newOpp.Converted_Lead_ID__c = null;
    newOpp.Received_signed_contract_date__c = null;
    newOpp.Selection_confirmed_date__c = null;

    
    // Adding 'Copy of ' to the start of the opp name.
    newOpp.Name = (Label.Opp_Clone_Copy_of +' ' + holdingOpp.Name).mid(0,80);
    
    for (OpportunityLineItem oli : holdingOpp.OpportunityLineItems) {
      // Set to selected, if not a CPQ user.
      availableOppLineItems.add(new OpportunityLineItemWrapper(oli, (currentUser.CPQ_User__c == false)));
    }
    
    for (OpportunityContactRole ocr : holdingOpp.OpportunityContactRoles) {
      availableOppContRoles.add(new OpportunityContactRoleWrapper(ocr));
    }
    
    for (Competitor__c cmp : holdingOpp.Competitors__r) {
      availableCompetitors.add(new CompetitorWrapper(cmp));
    }
    
    for (OpportunityTeamMember otm : holdingOpp.OpportunityTeamMembers) {
      availableOppTeamMembs.add(new OpportunityTeamMemberWrapper(otm));
    }
    
    for (OpportunitySplit os : holdingOpp.OpportunitySplits) {
      availableOppSplits.add(new OpportunitySplitWrapper(os));
    }
    
    return null;
  }
  
  //===========================================================================
  // Create the cloned record.
  //===========================================================================
  public PageReference createClone() {
    
    // Savepoint to catch any problems, and allow rollback to the transaction
    Savepoint sp = Database.setSavepoint();
    
    List<OpportunityLineItem> newOliList = new List<OpportunityLineItem>();
    List<OpportunityContactRole> newOcrList = new List<OpportunityContactRole>();
    List<Competitor__c> newCompList = new List<Competitor__c>();
    List<OpportunityTeamMember> newOtmList = new List<OpportunityTeamMember>();
    List<OpportunitySplit> newOsList = new List<OpportunitySplit>();
    
    OpportunityLineItem oliNew;
    OpportunityContactRole ocrNew;
    Competitor__c compNew;
    OpportunityTeamMember newOTM;
    OpportunitySplit oppSplit;
    
    Opportunity insOpp = newOpp.clone(false,false,false,false);
    insOpp.Probability = populateProbability(insOpp.StageName);
    if (Opportunity.sObjectType.getDescribe().fieldsets.getMap().containsKey('Opportunity_Clone_Ignore')) {
      for (Schema.FieldSetMember fsm : Opportunity.sObjectType.getDescribe().fieldsets.getMap().get('Opportunity_Clone_Ignore').getFields()) {
        clearObjectField(insOpp, fsm);
      }
    }
    
    if (!checkRecordAccess(insOpp.AccountId)) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, system.label.Opp_Clone_No_Account_Access));
      return null;
    }
    
    // Insert the new opportunity and use the Id later to link the other related records.
    try {
      insert insOpp;
    }
    catch (DMLException ex) {
      Database.rollback(sp);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,ex.getMessage()));
      return null;
    }
    try {
      for (OpportunityLineItemWrapper oliw : availableOppLineItems) {
        if (oliw.selected) {
          oliNew = new OpportunityLineItem();
          oliNew = oliw.oli.clone(false, true);
          oliNew.TotalPrice = null;
          oliNew.OpportunityId = insOpp.Id;
          if (OpportunityLineItem.sObjectType.getDescribe().fieldsets.getMap().containsKey('Opportunity_Clone_Ignore')) {
            for (Schema.FieldSetMember fsm : OpportunityLineItem.sObjectType.getDescribe().fieldsets.getMap().get('Opportunity_Clone_Ignore').getFields()) {
              clearObjectField(oliNew, fsm);
            }
          }
          newOliList.add(oliNew);
        }
      }
      
      for (OpportunityContactRoleWrapper ocrw : availableOppContRoles) {
        if (ocrw.selected) {
          ocrNew = new OpportunityContactRole();
          ocrNew = ocrw.ocr.clone(false, true);
          ocrNew.OpportunityId = insOpp.Id;
          newOcrList.add(ocrNew);
        }
      }
      
      for (CompetitorWrapper cmpw : availableCompetitors) {
        if (cmpw.selected) {
          compNew = new Competitor__c();
          compNew = cmpw.comp.clone(false, true);
          compNew.Opportunity__c = insOpp.Id;
          newCompList.add(compNew);
        }
      }
      
      for (OpportunityTeamMemberWrapper otmw : availableOppTeamMembs) {
        if (otmw.selected) {
          newOTM = new OpportunityTeamMember();
          newOTM = otmw.otm.clone(false, true);
          newOTM.OpportunityId = insOpp.Id;
          newOtmList.add(newOTM);
        }
      }
      
      for (OpportunitySplitWrapper osw : availableOppSplits) {
        if (osw.selected) {
          oppSplit = new OpportunitySplit();
          oppSplit = osw.os.Clone(false,false);
          oppSplit.OpportunityId = insOpp.Id;
          // Only insert splits for non owners
          if (oppSplit.SplitOwnerId != insOpp.OwnerId) {
            newOsList.add(oppSplit);
          }
        }
      }
   
      insert newOliList;
      insert newOcrList;
      insert newCompList;
      insert newOtmList;
      insert newOsList;
    }
    catch (Exception ex) {
      Database.rollback(sp);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,ex.getMessage() + ' - ' + ex.getStackTraceString()));
      return null;
    }
    
    // Assuming all is well, send the user to the edit page for the new opportunity.
    if (insOpp.Id != null) {
      PageReference pr = new ApexPages.StandardController(insOpp).edit();
      // Explicitly set the retURL to the view page for the new opp, otherwise it goes back to the clone page.
      pr.getParameters().put('retURL',new ApexPages.StandardController(insOpp).view().getUrl());
      return pr;
    }
    
    return null;
  }
  
  //===========================================================================
  // For a given field, either null it or set to false (for boolean)
  // Case 02165894: Added support for owner being emptied
  //===========================================================================
  private void clearObjectField(sObject obj, FieldSetMember fsm) {
    try {
      if (fsm.getType() == Schema.DisplayType.Boolean) {
        system.debug(fsm.getFieldPath() +' setting to False');
        obj.put(fsm.getFieldPath().toLowerCase(), false);
        return;
      }
      system.debug(fsm.getFieldPath() +' setting to null');
      obj.put(fsm.getFieldPath().toLowerCase(), null);
      if (fsm.getFieldPath().endsWithIgnoreCase('ownerid')) {
        obj.put(fsm.getFieldPath().toLowerCase(), UserInfo.getUserId());
      }
    }
    catch (Exception e) {
      system.debug(e.getMessage());
    }
  }
  
  public class OpportunityLineItemWrapper {
    public OpportunityLineItem oli {get;set;}
    public Boolean selected {get;set;}
    public OpportunityLineItemWrapper(OpportunityLineItem o, Boolean s) {
      oli = o;
      selected = s;
    }
  }
  
  public class OpportunityContactRoleWrapper {
    public OpportunityContactRole ocr {get;set;}
    public Boolean selected {get;set;}
    public OpportunityContactRoleWrapper(OpportunityContactRole o) {
      ocr = o;
      selected = true;
    }
  }
  
  public class CompetitorWrapper {
    public Competitor__c comp {get;set;}
    public Boolean selected {get;set;}
    public CompetitorWrapper(Competitor__c c) {
      comp = c;
      selected = true;
    }
  }
  
  public class OpportunityTeamMemberWrapper {
    public OpportunityTeamMember otm {get;set;}
    public Boolean selected {get;set;}
    public OpportunityTeamMemberWrapper(OpportunityTeamMember o) {
      otm = o;
      selected = true;
    }
  }
  
  public class OpportunitySplitWrapper {
    public OpportunitySplit os {get;set;}
    public Boolean selected {get;set;}
    public OpportunitySplitWrapper(OpportunitySplit o) {
      os = o;
      selected = true;
    }
  }
  
}