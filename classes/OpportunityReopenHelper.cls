/******************************************************************************
 * Appirio, Inc
 * Name: OpportunityReopenHelper
 * Description: T-323320: Change Opportunity Process: Custom Opp Button.
 * Created Date: Oct 09th, 2014
 * Created By:  Noopur (Appirio)
 * Date Modified        Modified By                  Description of the update
 * Oct 18th, 2014       Nathalie Le Guay (Appirio)   reopenCurrentOpportunity() and reopenPreInvoiceOpp() run for edq users only
 * Nov 03rd, 2014       Nathalie Le Guay             Check is no longer on EDQ business unit, but rather on the COPS roles
 * Nov 11th, 2014       Nathalie Le Guay             I-138510: blank the Primary_Reason_W_L__c and Other_Closed_Reason__c fields
 * Feb 06th, 2015       James Weatherall             Case #542401 - Update Record Type to Standard in reopenPreInvoiceOpp
 * May 19th, 2015       Nur Azlini                   Case #630336 - get the list of COPS role from public group(EDQ COPS Team) instead of role name from constant class.
                                                     Changed UserRole.Name to UserRole.DeveloperName
 * Jun 10th, 2015       Paul Kissick                 Case #970272 - Bug found in finding orders which have been exported. Also fixed formatting.
 * Apr 5th,  2016       Miguel Rodriguez (UC Team)   Case # 1879856 - Validation to check for approved Credit Note Request Form on Oppty Re-Open
 * May 13th, 2016       Paul Kissick                 Case #01950454 - Adding proper exception handling to entire reopening process
 * Jan 11th, 2017       Manoj Gopu                   Case 02227421 - Checking for the logged in User if the User region is EMEA
 * Jan 26th, 2017       Manoj Gopu                   Case 02157126 - Updating the Opp field to true Re_opened_to_Credit__c when the Opp is Reopened.  
 * Jun 28th, 2017       James Wills                  Case 02079142 - Added Label.Other_Closed_Reason_for_Credited_EDQ_Opportunities to reopenPostInvoiceOpp().
 ******************************************************************************/
global class OpportunityReopenHelper {
  
  static Opportunity oppToReopen;
  //Start Case #630336
  static List<Group> copsList = [SELECT Id, DeveloperName FROM Group
                                 WHERE Id in (SELECT UserOrGroupId FROM GroupMember WHERE Group.Name =: Constants.GROUP_EDQ_COPS)];
  
  static Set<String> copsRoles = new Set<String>{'Global_Admin'};

  //End Case #630336
  static User currentUser {
    get {
      if (currentUser == null) {
        currentUser = [SELECT Id, UserRole.DeveloperName,Region__c FROM User WHERE Id =: UserInfo.getUserId()];
      }
      return currentUser;
    }
    set;
  }
  
  // webservice to fetch the Opportunity record by using the Id sent by the button
  webservice static String reopenCurrentOpportunity (String opptyId) {
    //Start Case #630336
    for (Group cR : copsList) {
      String copRole = cR.DeveloperName;
      copsRoles.add(copRole);
      system.debug( 'FinalList: '+copsRoles) ;
    }
    //End Case #630336
    if (String.isBlank(currentUser.UserRoleId) || !copsRoles.contains(currentUser.UserRole.DeveloperName)) {
      return Label.Opportunity_Reopen_Not_a_COPS_User;
    }

    for (Opportunity opp : [SELECT Id, StageName, Forecast_Category__c, Type, Owner.Region__c,
                             (SELECT Id, Exported_for_Invoicing__c FROM Orders__r)
                            FROM Opportunity
                            WHERE Id = :opptyId]) {
      oppToReopen = opp;
      Boolean foundExported = false;
      if (!oppToReopen.Orders__r.isEmpty()) {
        // PK Case #970272 - Adding checks for all orders, not just the first one!
        for(Order__c ord : oppToReopen.Orders__r) {
          if (ord.Exported_for_Invoicing__c) {
            foundExported = true;
          }
        }
      }
      if (foundExported) {
      
        // Case 1879856 update. MRodriguez : Check if there is a Credit Note Request Form that has status = Approved
        List<Credit_Note_Request_Form__c> cReqForms = [Select Status__c, Opportunity_ID__c, Order__c, Name, Account__c
                                                 From Credit_Note_Request_Form__c
                                                 Where Status__c = 'Approved'
                                                 And Opportunity_ID__c =: opptyId];
        system.debug('Credit Note Request Form ' + cReqForms);
        //Checking for Logged User region or Opp Owner region is EMEA then Create the Opp
        if (cReqForms.size()==0 && oppToReopen.Owner.Region__c <> Constants.REGION_EMEA && currentUser.Region__c <> Constants.REGION_EMEA){
          return Label.Opportunity_Reopen_No_Approved_Credit_Form;
        }
        // End case 1879856;
        return reopenPostInvoiceOpp();
      }
      else {
        return reopenPreInvoiceOpp();
      }
    }
    return '';
  }

  // Method to re-open Opportunity when the Export to Invoicing is false.
  static String reopenPreInvoiceOpp() {
    if (String.isBlank(currentUser.UserRoleId) || !copsRoles.contains(currentUser.UserRole.DeveloperName)) {
      return Label.Opportunity_Reopen_Not_a_COPS_User;
    }
    try {
      // Start: Case #542401 - 6/2/2015 - JW
      String recTypeId = Record_Type_Ids__c.getOrgDefaults().Opportunity_Standard__c;
      oppToReopen.RecordTypeId = recTypeId;
      // End
      oppToReopen.StageName = Constants.OPPTY_STAGE_4;
      oppToReopen.Forecast_Category__c = 'Pipeline';
      oppToReopen.Primary_Reason_W_L__c = '';
      oppToReopen.Other_Closed_Reason__c = '';
      update oppToReopen;
    } 
    catch (DMLException ex) {
      system.debug('[OpportunityReopenHelper: reopenPreInvoiceOpp] Exception: ' + ex.getMessage());
      ApexLogHandler.createLogAndSave('OpportunityReopenHelper','reopenPreInvoiceOpp', ex.getStackTraceString(), ex);
      String error = '';
      for (Integer i=0; i < ex.getNumDml(); i++) {
        error += ex.getDMLMessage(i);
      }
      return error;
    }
    return '';
  }

  // Method to re-open Opportunity when the Export to Invoicing is true.
  static String reopenPostInvoiceOpp() {
    Savepoint sp = Database.setSavepoint();
    try {
      oppToReopen.StageName = Constants.OPPTY_CLOSED_LOST;
      oppToReopen.Type = Constants.OPPTY_TYPE_CREDITED;
      oppToReopen.Primary_Reason_W_L__c = '';
      //oppToReopen.Other_Closed_Reason__c = Label.Other_Closed_Reason_for_EDQ_Opportunities;    
      oppToReopen.Other_Closed_Reason__c = Label.Other_Closed_Reason_for_Credited_EDQ_Opportunities;//Case #02079142    
      oppToReopen.Re_opened_to_Credit__c = true; // Added by Manoj
      update oppToReopen;
    }
    catch (DMLException ex) {
      Database.rollback(sp); 
      system.debug('[OpportunityReopenHelper: reopenPostInvoiceOpp] Exception: ' + ex.getMessage());
      ApexLogHandler.createLogAndSave('OpportunityReopenHelper','reopenPostInvoiceOpp', ex.getStackTraceString(), ex);
      String error = '';
      for (Integer i=0; i < ex.getNumDml(); i++) {
        error += ex.getDMLMessage(i);
      }
      return error;
    }
    catch (Exception e) {
      Database.rollback(sp); 
      system.debug('[OpportunityReopenHelper: reopenPostInvoiceOpp] Exception: ' + e.getMessage());
      ApexLogHandler.createLogAndSave('OpportunityReopenHelper','reopenPostInvoiceOpp', e.getStackTraceString(), e);
      return e.getMessage();
    }
    return '';
  }

}