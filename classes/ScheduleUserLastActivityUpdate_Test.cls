/**=====================================================================
 * Appirio, Inc
 * Test Class Name: ScheduleUserLastActivityUpdate_Test
 * Class Name: ScheduleUserLastActivityUpdate
 
 * Created Date: Sep 05th, 2014
 * Created By: Aditi Bhardwaj (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * Sep 7th, 2015                Paul Kissick                 Fixed test class to cover ScheduleUserLastActivityUpdate
 
 * Apr 4, 2016                  Paul Kissick                 DEPRECATED: All tests moved to BatchUserLastActivityUpdate_Test
 
 =====================================================================*/

@isTest
private class ScheduleUserLastActivityUpdate_Test {
  
  static testMethod void testScheduler() {
    
    /*
    Global_Settings__c gs = new Global_Settings__c();
    gs.Name = 'Global';
    gs.Batch_Failures_Email__c = '';
    insert gs;
    
    test.startTest();
    // Schedule the test job
    String CRON_EXP = '0 0 0 * * ?';
    String jobId = System.schedule('ScheduleUserLastActivityUpdate '+String.valueOf(Datetime.now().getTime()), CRON_EXP, new ScheduleUserLastActivityUpdate());
    
    // Get the information from the CronTrigger API object  
    CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];     
    test.stopTest();

    // Verify the Schedule has been scheduled with the same time specified  
    system.assertEquals(CRON_EXP, ct.CronExpression);

    // Verify the job has not run, but scheduled  
    system.assertEquals(0, ct.TimesTriggered);
    */
    
    system.assert(true);
    
  }
  
}