/**=====================================================================
 * Appirio, Inc
 * Name: LeadTriggerHandler_Test
 * Description: Test class for lead Trigger
        T-194935 , T-213204
 * Created Date: Nov 19th, 2012
 * Created By: Pankaj Mehra (Appirio)
 * 
 * Date Modified          Modified By              Description of the update
 * Jan 30th, 2014         Jinesh Goyal(Appirio)    T-232760: Homogenize the comments
 * Feb 13th, 2014         Jinesh Goyal(Appirio)    T-232763: Added Exception Logging
 * Feb 27th, 2014         Arpita Bose(Appirio)     Fixed failure.
 * Mar 03rd, 2014         Arpita Bose(Appirio)     T-243282:Added constants in place of String
 * Mar 14th, 2014         Arpita Bose(Appirio)     T-253906: Fixed error
 * Apr 22nd, 2014         Arpita Bose              Updated method testLeadConversion()to increase the code coverage
 * Jul 15th, 2015         Paul Kissick             Added fixes for test failed from recent changes to lead timed workflows
 * Aug 04th, 2015         Arpita Bose              Removed method testLeadDeleteBySerasaUser() as story S-338322 is cancelled
 * Sep 9th, 2015          Paul Kissick             S-291640: Duplicate Management Fixes - Removed seealldata
 * Oct 9th, 2015          Paul Kissick             Adding tests for opp creation on conversion and existing address
 * Jul 18th, 2016         Manoj Gopu(QA)           CRM2:W-005436 Updatedmethod testLeadConversion() to increase code coverage 
 =====================================================================*/
@isTest
private class LeadTriggerHandler_Test { 
  
  @isTest
  private static void testLeadConversionNoOpportunity() {
    
    Lead lead = Test_Utils.createLead();
    lead.LastName = 'Test_Lead_Convert';
    lead.Capability__c = 'Application Processing';
    lead.Budget__c = 'Less than 1,000';
    insert lead;
    system.debug('#########lead#########'+lead);
        
    ELQA_Marketing_Activity__c marketActivity = Test_Utils.createMarketingActivity(); 
    marketActivity.Lead__c = lead.Id;
    insert marketActivity;
    system.debug('#########marketActivity#########'+marketActivity);
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser;
    
    lead.Abort_Timed_Workflow__c = true; // PK: Added to fix tests
    update lead;
    
    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(lead.id);    
    lc.setOwnerId(testUser.Id);
    //Test.startTest();
    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
    lc.convertedStatus =convertStatus.MasterLabel;
    lc.setDoNotCreateOpportunity(true);
    system.debug('#########lc#########'+lc);
    
    Test.startTest();

    Database.LeadConvertResult lcr = Database.convertLead(lc);
    system.debug('#########lcr#########'+lcr);

    system.assert(lcr.isSuccess());
    
    Test.stopTest();
    
    Lead existingLead = [
      SELECT Id, IsConverted, ConvertedAccountId, ConvertedContactId, ConvertedOpportunityId, CreatedById 
      FROM Lead 
      WHERE Id = :lead.id
    ];
    
    system.assert(existingLead.isConverted);
    system.assert(existingLead.ConvertedAccountId != null);
    system.assert(existingLead.ConvertedOpportunityId == null);
    
    List<ELQA_Marketing_Activity__c> mActivityLst1 = [
      SELECT Id, Contact__c 
      FROM ELQA_Marketing_Activity__c
      WHERE Contact__c =: lcr.getContactId()
    ];
    
    system.debug('#######mActivityLst1.size()########' +mActivityLst1.size());                         
    system.assert(mActivityLst1.size() > 0);
    
    List<ELQA_Marketing_Activity__c> mActivityLst2 = [
      SELECT Id, Contact__c 
      FROM ELQA_Marketing_Activity__c
      WHERE Account__c =: lcr.getAccountId()
    ];
    
    system.debug('#######mActivityLst2.size()########' +mActivityLst2.size());                         
    system.assert(mActivityLst2.size() > 0);                          
    
  }
  
  
  @isTest
  private static void testLeadConversionWithOpportunity() {
    
    Lead lead = Test_Utils.createLead();
    lead.LastName = 'Test_Lead_Convert';
    lead.Capability__c = 'Application Processing';
    lead.Budget__c = 'Less than 1,000';
    lead.TCV__c = 40000;
    lead.Target_Close_Date__c = Date.today().addDays(30);
    lead.PO_Required__c = 'No';
    lead.Proposal_Type__c = 'Standard';
    lead.Street = 'Street Name';
    lead.State = 'State Name';
    lead.PostalCode = 'P05T C0D3';
    lead.Country = 'United Kingdom';
    lead.City = 'Test City';
    insert lead;
    system.debug('#########lead#########'+lead);
        
    ELQA_Marketing_Activity__c marketActivity = Test_Utils.createMarketingActivity(); 
    marketActivity.Lead__c = lead.Id;
    insert marketActivity;
    system.debug('#########marketActivity#########'+marketActivity);
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser;
    
    lead.Abort_Timed_Workflow__c = true; // PK: Added to fix tests
    update lead;
    
    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(lead.id);    
    lc.setOwnerId(testUser.Id);
    //Test.startTest();
    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
    lc.convertedStatus =convertStatus.MasterLabel;
    lc.setDoNotCreateOpportunity(false); // WE WANT AN OPP NOW
    system.debug('#########lc#########'+lc);
    
    Test.startTest();

    Database.LeadConvertResult lcr = Database.convertLead(lc);
    system.debug('#########lcr#########'+lcr);

    system.assert(lcr.isSuccess());
    
    Test.stopTest();
    
    Lead existingLead = [
      SELECT Id, IsConverted, ConvertedAccountId, ConvertedContactId, ConvertedOpportunityId, CreatedById 
      FROM Lead 
      WHERE Id = :lead.id
    ];
    
    system.assert(existingLead.isConverted);
    system.assert(existingLead.ConvertedAccountId != null);
    system.assert(existingLead.ConvertedOpportunityId != null);
    
    List<ELQA_Marketing_Activity__c> mActivityLst1 = [
      SELECT Id, Contact__c 
      FROM ELQA_Marketing_Activity__c
      WHERE Contact__c =: lcr.getContactId()
    ];
    
    system.debug('#######mActivityLst1.size()########' +mActivityLst1.size());                         
    system.assert(mActivityLst1.size() > 0);
    
    List<ELQA_Marketing_Activity__c> mActivityLst2 = [
      SELECT Id, Contact__c 
      FROM ELQA_Marketing_Activity__c
      WHERE Account__c =: lcr.getAccountId()
    ];
    
    system.debug('#######mActivityLst2.size()########' +mActivityLst2.size());                         
    system.assert(mActivityLst2.size() > 0);                          
    
  }
  
  @isTest
  private static void testLeadConversionWithOpportunityExistingAddress() {
    
    Lead lead = Test_Utils.createLead();
    lead.LastName = 'Test_Lead_Convert';
    lead.Capability__c = 'Application Processing';
    lead.Budget__c = 'Less than 1,000';
    lead.TCV__c = 40000;
    lead.Target_Close_Date__c = Date.today().addDays(30);
    lead.PO_Required__c = 'No';
    lead.Proposal_Type__c = 'Standard';
    lead.Street = 'Street Name';
    lead.State = 'State Name';
    lead.PostalCode = 'P05T C0D3';
    lead.Country = 'United Kingdom';
    lead.City = 'Test City';
    lead.Decision_Maker__c=true;
    insert lead;
    
    Address__c address = new Address__c(
      Address_1__c = 'Street Name',
      City__c = 'Test City', 
      Country__c = 'United Kingdom',
      State__c = 'State Name',   // might be county__c
      Postcode__c = 'P05T C0D3',
      Validation_Status__c = 'true', 
      EDQ_Integration_Id__c = String.valueOf(Math.random())
    );
    
    address = AddressUtility.checkDuplicateAddress(address);
    insert address;
    
    system.debug(address);
    
    system.debug('#########lead#########'+lead);
        
    ELQA_Marketing_Activity__c marketActivity = Test_Utils.createMarketingActivity(); 
    marketActivity.Lead__c = lead.Id;
    insert marketActivity;
    system.debug('#########marketActivity#########'+marketActivity);
    User testUser = Test_Utils.createUser(Constants.PROFILE_SYS_ADMIN);
    insert testUser;
    
    lead.Abort_Timed_Workflow__c = true; // PK: Added to fix tests
    update lead;
    
    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(lead.id);    
    lc.setOwnerId(testUser.Id);
    //Test.startTest();
    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
    lc.convertedStatus =convertStatus.MasterLabel;
    lc.setDoNotCreateOpportunity(false); // WE WANT AN OPP NOW
    system.debug('#########lc#########'+lc);
    
    Test.startTest();

    Database.LeadConvertResult lcr = Database.convertLead(lc);
    system.debug('#########lcr#########'+lcr);

    system.assert(lcr.isSuccess());
    
    Test.stopTest();
    
    Lead existingLead = [
      SELECT Id, IsConverted, ConvertedAccountId, ConvertedContactId, ConvertedOpportunityId, CreatedById 
      FROM Lead 
      WHERE Id = :lead.id
    ];
    
    system.assert(existingLead.isConverted);
    system.assert(existingLead.ConvertedAccountId != null);
    system.assert(existingLead.ConvertedOpportunityId != null);
    
    List<ELQA_Marketing_Activity__c> mActivityLst1 = [
      SELECT Id, Contact__c 
      FROM ELQA_Marketing_Activity__c
      WHERE Contact__c =: lcr.getContactId()
    ];
    
    system.debug('#######mActivityLst1.size()########' +mActivityLst1.size());                         
    system.assert(mActivityLst1.size() > 0);
    
    List<ELQA_Marketing_Activity__c> mActivityLst2 = [
      SELECT Id, Contact__c 
      FROM ELQA_Marketing_Activity__c
      WHERE Account__c =: lcr.getAccountId()
    ];
    
    system.debug('#######mActivityLst2.size()########' +mActivityLst2.size());                         
    system.assert(mActivityLst2.size() > 0);                          
    
  }

}