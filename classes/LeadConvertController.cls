/**=====================================================================
 * Appirio, Inc
 * Name: LeadConvertController
 * Description: : T-235362: Leads field "Ready to Convert Status" to move to VF page
 * Created Date: Feb 17th, 2014
 * Created By: Naresh Kr Ojha(Appirio)
 * 
 * Date Modified        Modified By                  Description of the update
 * Feb 18, 2014         Naresh Kr Ojha(Appirio)      Updated as per comments on task.
 * Mar 23, 2015         Nur Azlini                   Split Capability and Budget as additional fields for Opportunity.
 * Jul 16th, 2015       Diego Olarte                 Case #932880 - Adding New Start item#2 Target Close Date
 * Oct 1st, 2015        Arpita Bose                  I-182628: updated to show CNPJ Number for LATAM Serasa Record Type
 * Oct 20th, 2015       Paul Kissick                 Case #01198200 - Adding TCV to required fields for opp conversion
 =====================================================================*/
public with sharing class LeadConvertController {

  public List<String> errorOnfields {get;set;}
  public List<String> errorOnfieldsOppt {get;set;}
  public Map<String, Boolean> fieldStatusMap {get;set;}
  public Boolean countryInvalid {get{if (countryInvalid==null) countryInvalid = false; return countryInvalid;}set;}
  
  Set<String> validCountrySet = new Set<String>();
    
  public String getCountrySetJson() {
    return JSON.serialize(validCountrySet);
  }
  
  //Constructor 
  public LeadConvertController(ApexPages.StandardController controller) {
    Lead leadObj = new Lead();
    fieldStatusMap = new Map<String, Boolean>();
    errorOnfields = new List<String>();
    errorOnfieldsOppt = new List<String>();
    countryInvalid = false;
    leadObj = [SELECT Capability__c, Budget__c, Target_Close_Date__c,TCV__c, Email, Phone, Street, City,
                      State, Country, PostalCode, ID, RecordTypeId, CNPJ_Number__c
               FROM Lead
               WHERE ID =: controller.getId() LIMIT 1];
               
    String latamRecordTypeId = DescribeUtility.getRecordTypeIdByName(Constants.SOBJECT_LEAD, Constants.RECORDTYPE_LEAD_LATAM_SERASA);
               
    //Start Item # 2
    errorOnfieldsOppt.add(Label.CAPABILITY);
    errorOnfieldsOppt.add(Label.BUDGET);
    errorOnfieldsOppt.add(Label.TARGETCLOSEDATE);
    errorOnfieldsOppt.add(Label.Lead_Estimated_TCV);
    //End Item # 2
    errorOnfields.add(Label.EMAIL);
    errorOnfields.add(Label.PHONE);
    errorOnfields.add(Label.STREET);
    errorOnfields.add(Label.CITY);
    errorOnfields.add(Label.STATE);      
    errorOnfields.add(Label.COUNTRY);
    errorOnfields.add(Label.POSTALCODE);  
    //I-182628
    if ((leadObj.RecordTypeId == latamRecordTypeId))  { 
      errorOnfields.add(Label.CNPJ_NUMBER);  
    }
              
    if (String.isBlank(leadObj.Capability__c)) {
      fieldStatusMap.put(Label.CAPABILITY, false);
    } else {
      fieldStatusMap.put(Label.CAPABILITY, true);
    }
    
    if (leadObj.TCV__c == null) {
      fieldStatusMap.put(Label.Lead_Estimated_TCV, false);
    } else {
      fieldStatusMap.put(Label.Lead_Estimated_TCV, true);
    }
    

    if (String.isBlank(leadObj.Budget__c)) {
      fieldStatusMap.put(Label.BUDGET, false);
    } else {
      fieldStatusMap.put(Label.BUDGET, true);
    }

    if (leadObj.Target_Close_Date__c == null) {
      fieldStatusMap.put(Label.TARGETCLOSEDATE, false);
    } else {
      fieldStatusMap.put(Label.TARGETCLOSEDATE, true);
    }


    if (String.isBlank(leadObj.Email)) {
      fieldStatusMap.put(Label.EMAIL, false);
    } else {
      fieldStatusMap.put(Label.EMAIL, true);
    }

    if (String.isBlank(leadObj.Phone)) {
      fieldStatusMap.put(Label.PHONE, false);
    } else {
      fieldStatusMap.put(Label.PHONE, true);
    }

    if (String.isBlank(leadObj.Street)) {
      fieldStatusMap.put(Label.STREET, false);
    } else {
      fieldStatusMap.put(Label.STREET, true);
    }

    if (String.isBlank(leadObj.City)) {
      fieldStatusMap.put(Label.CITY, false);
    } else {
      fieldStatusMap.put(Label.CITY, true);
    }

    if (String.isBlank(leadObj.State)) {
      fieldStatusMap.put(Label.STATE, false);
    } else {
      fieldStatusMap.put(Label.STATE, true);
    }
    
    
    if (String.isBlank(leadObj.Country)) {
      fieldStatusMap.put(Label.COUNTRY, false);
    } else {
      fieldStatusMap.put(Label.COUNTRY, true);
    }

    if (String.isBlank(leadObj.PostalCode)) {
      fieldStatusMap.put(Label.POSTALCODE, false);
    } else {
      fieldStatusMap.put(Label.POSTALCODE, true);
    }
    
    // I-182628
    if (String.isBlank(leadObj.CNPJ_Number__c)) {
      fieldStatusMap.put(Label.CNPJ_NUMBER, false);
    } else {
      fieldStatusMap.put(Label.CNPJ_NUMBER, true);
    }

  }//End of the method
}//End of the class