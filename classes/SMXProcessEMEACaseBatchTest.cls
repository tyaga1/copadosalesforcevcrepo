/**=====================================================================
 * Experian
 * Name: SMXProcessEMEACaseBatchTest
 * Description: Implements the SMXProcessEMEACaseBatch test
 * Created Date: Sep 25th, 2017
 * Created By: Diego Olarte
 * 
 * Date Modified     Modified By        Description of the update
 *                                         
 =====================================================================*/
@isTest
global class SMXProcessEMEACaseBatchTest{
  public static String SOBJECT_CASE = 'Case';
  public static String PROFILE_SYS_ADMIN = 'System Administrator';
  public static String CASE_REC_TYPE_EDQ_CASE = 'EDQ Technical Support';
  
   public static case prepareTestData()  {    
      Account a = new Account(Name='AAA1',BillingPostalCode='211212',BillingStreet='TestStreet',BillingCity='TestCity',BillingCountry='United States of America');    
      Insert a;  
      Contact c = new Contact(FirstName='BBB1',LastName='SMX2SMX',accountId=a.Id, email = 'test123@experian.com');    
      insert c;
      Case cs = new Case(subject = 'CCC1', AccountId = a.id, Status = 'Closed Resolved', ContactId = c.Id,
                         RecordTypeId = DescribeUtility.getRecordTypeIdByName(SOBJECT_CASE , CASE_REC_TYPE_EDQ_CASE ));
        cs.EDQ_Support_Region__c = 'UK';
        insert cs;
      system.debug('Case>>'+cs);  
      return(cs); 
      }
       
    public static void ClearTestData(){    
      List<Feedback__c> lstFbk = [select id from Feedback__c where status__c='Nominated'];    
      delete lstFbk;
      List<Case> lstCs=[select id from Case where subject='CCC1'];
      delete lstCs;
      List<Contact> lstCon = [select id from Contact where FirstName='BBB1'];    
      delete lstCon;
      List<Account> lstAcc = [select id from account where Name='AAA1'];    
      delete lstAcc;  
      }
    
    static testmethod void SmxcaseScheduler(){
          User testUser = Test_Utils.createUser(PROFILE_SYS_ADMIN);
        testUser.Global_Business_Line__c = 'Corporate';
        testUser.Business_Line__c = 'Corporate';
        testUser.Business_Unit__c = 'APAC:SE';
        testUser.Region__c = 'EMEA';
        insert testUser;
        System.runAs(testUser) {       
         test.starttest();
         SMXProcessEMEACaseBatch b = new  SMXProcessEMEACaseBatch();
         id batch=database.executebatch(b);
         test.stoptest();
        }
    }
}