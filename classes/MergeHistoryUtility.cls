/*=============================================================================
 * Experian
 * Name: MergeHistoryUtility
 * Description: Utility class to build MergeHistory__c records. Case #29746
 * Created Date: Feb 19th, 2015
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * Aug 4th, 2016      Paul Kissick          Hotfix: SaaS__c removed from Contact
 * Aug 19th, 2016     Paul Kissick          Case 02107571: Extending to store CreatedBy/CreatedDate from loser on account 
 * Aug 30th, 2016     Mauricio Murillo      Case  02139856: Extending to store loser record name  
=============================================================================*/
public without sharing class MergeHistoryUtility {
  
  // Take 2 sobjects (only Lead, Contact and Account will be supported here) and create a MergeHistory record.
  public static MergeHistory__c createMergeHistoryRecord(sObject loserRec, sObject winnerRec) {
    
    MergeHistory__c mh = new MergeHistory__c();
    String objectType = loserRec.getSObjectType().getDescribe().getLocalName();
    mh.Object_Type__c = objectType;
    mh.Loser_Record_ID__c = (String)loserRec.get('Id');
    mh.Winner_Record_ID__c = (String)winnerRec.get('Id');
    if (loserRec.getSObjectType() == Account.sObjectType) {
      try {
        mh.Loser_Record_Name__c = (String)loserRec.get(Account.name); //MM 02139856
        mh.Loser_CSDA_Integration_ID__c = (String)loserRec.get(Account.CSDA_Integration_ID__c);
        mh.Loser_EDQ_Integration_Id__c = (String)loserRec.get(Account.EDQ_Integration_Id__c);
        mh.Loser_Top_Parent_ID__c = (String)loserRec.get(Account.Ultimate_Parent_Account__c);
        mh.Loser_Global_Unique_ID__c = (String)loserRec.get(Account.Global_Unique_ID__c);
        mh.Loser_Experian_ID__c = (String)loserRec.get(Account.Experian_ID__c);
        mh.Loser_SaaS_Ind__c = (Boolean)loserRec.get(Account.Saas__c);
        mh.Loser_Created_By__c = (Id)loserRec.get(Account.CreatedById);
        mh.Loser_Created_Date__c = (Datetime)loserRec.get(Account.CreatedDate);
        mh.Winner_CSDA_Integration_ID__c = (String)winnerRec.get(Account.CSDA_Integration_ID__c);
        mh.Winner_EDQ_Integration_Id__c = (String)winnerRec.get(Account.EDQ_Integration_Id__c);
        mh.Winner_Top_Parent_ID__c = (String)winnerRec.get(Account.Ultimate_Parent_Account__c);
        mh.Winner_Global_Unique_ID__c = (String)winnerRec.get(Account.Global_Unique_ID__c);
        mh.Winner_Experian_ID__c = (String)winnerRec.get(Account.Experian_ID__c);
        mh.Winner_SaaS_Ind__c = (Boolean)winnerRec.get(Account.Saas__c);
      }
      catch (SObjectException soex) {
        system.debug('createMergeHistoryRecord : field missing : '+soex.getMessage());
        apexLogHandler.createLogAndSave('MergeHistoryUtility','createMergeHistoryRecord', soex.getStackTraceString(), soex);
      }
      mh.Account__c = mh.Winner_Record_ID__c;
    }
    if (loserRec.getSObjectType() == Contact.sObjectType) {
      try {
        mh.Loser_Record_Name__c = (String)(loserRec.get(Contact.FirstName) + ' ' + loserRec.get(Contact.LastName)); //MM 02139856
        // mh.Loser_CSDA_Integration_ID__c = (String)loserRec.get('CSDA_Integration_ID__c');
        mh.Loser_EDQ_Integration_Id__c = (String)loserRec.get(Contact.EDQ_Integration_Id__c);
        mh.Loser_Global_Unique_ID__c = (String)loserRec.get(Contact.Global_Unique_ID__c);
        mh.Loser_Experian_ID__c = (String)loserRec.get(Contact.Experian_ID__c);
        //mh.Loser_SaaS_Ind__c = (Boolean)loserRec.get(Contact.Saas__c);
        // mh.Winner_CSDA_Integration_ID__c = (String)winnerRec.get(Contact.CSDA_Integration_ID__c);
        mh.Winner_EDQ_Integration_Id__c = (String)winnerRec.get(Contact.EDQ_Integration_Id__c);
        mh.Winner_Global_Unique_ID__c = (String)winnerRec.get(Contact.Global_Unique_ID__c);
        mh.Winner_Experian_ID__c = (String)winnerRec.get(Contact.Experian_ID__c);
        //mh.Winner_SaaS_Ind__c = (Boolean)winnerRec.get(Contact.Saas__c);
      }
      catch (SObjectException soex) {
        system.debug('createMergeHistoryRecord : field missing : '+soex.getMessage());
        apexLogHandler.createLogAndSave('MergeHistoryUtility','createMergeHistoryRecord', soex.getStackTraceString(), soex);
      }
      mh.Contact__c = mh.Winner_Record_ID__c;
    }
    return mh;
  }

}