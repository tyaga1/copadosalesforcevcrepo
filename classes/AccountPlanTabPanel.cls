/**=====================================================================
 * Name: AccountPlanTabPanel
 * Description: See case #01848189 - Account Planning Enhancements
 * Created Date: Mar. 21st, 2016
 * Created By: James Wills
 *
 * Date Modified         Modified By            Description of the update
 * Mar. 21st, 2016       James Wills            Case #01848189: Created
 * May 9th, 2016         James Wills            Case #01959211: AP - Saving Child Objects - Return to correct tab
 * Feb. 2nd, 2017        James Wills            Case #01999575: Added openAccountPlanPDFGenerator()
  =====================================================================*/
public with sharing class AccountPlanTabPanel {

  public ID accId {get;set;}
  public ID accountPlanId {get;set;}
  public String accName {get;set;}
  
  public String currentUserAccountPlanTab{get;set;}
  
  public String currentUserAccountPlanLastVisited {get;set;}
  
  public String selectedAccountPlanTab {get;set;}                            
 
  
  public Static Account_Plan__c AccountPlan= new Account_Plan__c();
  

  public AccountPlanTabPanel(ApexPages.standardController controller){
    AccountPlan = (Account_Plan__c) controller.getRecord();
    accountPlanId = AccountPlan.ID;
    accId   = AccountPlan.Account__c;
    accName = AccountPlan.Account__r.Name;
    
    Cookie accountPlanLastVisitedCookie = ApexPages.currentPage().getCookies().get('accountPlanLastVisited');
    Cookie tabLastVisitedCookie         = ApexPages.currentPage().getCookies().get('tabLastVisited');
    
    if(tabLastVisitedCookie != null){
      currentUserAccountPlanLastVisited = accountPlanLastVisitedCookie.getValue();
      if(currentUserAccountPlanLastVisited==accountPlanId){
        currentUserAccountPlanTab = tabLastVisitedCookie.getValue();
      } else {
        currentUserAccountPlanTab = Label.ACCOUNTPLANNING_ClientOverviewTab;
      }
    } else {
      currentUserAccountPlanTab = Label.ACCOUNTPLANNING_ClientOverviewTab;
    }
    //ApexTestRunResult t = new ApexTestRunResult();
    DatedConversionRate  d = new DatedConversionRate();
    //LinkedContactRelation l = new LinkedContactRelation();
    //AccountContactRelation a = new AccountContactRelation();
  }
  
  public PageReference setAccountPlanCookies(){
    AccountPlanUtilities.setAccountPlanCookies(accountPlanId, currentUserAccountPlanTab);
    return null;
  }
  
  
  public PageReference openAccountPlanPDFGenerator(){  
    PageReference account = new PageReference('/apex/AccountPlanPDFGenerator?id=' + accountPlanId); 
    account.setRedirect(true); 
    return account;      
  }

    
  public PageReference backToAccount(){  
    PageReference account = new PageReference('/' + accId); 
    account.setRedirect(true); 
    return account;      
  }


}