/**********************************************************************************************
 * Experian 
 * Name         : AssetEDQDependencyExists_Test
 * Created By   : Diego Olarte (Experian)
 * Purpose      : Test class of AssetEDQDependencyExists
 * Created Date : September 8th, 2015
 *
 * Date Modified                Modified By                 Description of the update
 * Nov 11th, 2015               Paul Kissick                Case 01266075: Removed bad global setting entry, and testSchedulable
***********************************************************************************************/

@isTest
private class AssetEDQDependencyExists_Test {
    
  private static testMethod void testAssetEDQDependency() {
    Test.startTest();
    AssetEDQDependencyExists batchToProcess = new AssetEDQDependencyExists();
    database.executebatch(batchToProcess,10);
    Test.stopTest();
    
    system.debug([SELECT Id, Status__c,Status, Order_Owner_BU__c, Saas__c FROM Asset]);
    
    system.assertEquals(1,[SELECT COUNT() FROM Asset WHERE Status__c in ('Live','Scheduled') AND (Order_Owner_BU__c LIKE '%Data Quality' OR SaaS__c = true)]);
  }
  
  @testSetup
  private static void setupData() {
       
    Account tstAcc = Test_utils.insertEDQAccount(true);
    tstAcc.EDQ_Dependency_Exists__c = false;
                
    update new List<Account>{tstAcc};
    
    Contact tstCon = Test_utils.insertEDQContact(tstAcc.ID,true);
        
    update new List<Contact>{tstCon};
    
    User tstUser = Test_utils.createUser(Constants.PROFILE_SYS_ADMIN);
    tstUser.Business_Unit__c = 'NA MS Data Quality';
        
    insert new List<User>{tstUser};
    IsDataAdmin__c IsdataAdmin = new IsDataAdmin__c(SetupOwnerId = UserInfo.getUserId(), IsDataAdmin__c = true);
    
    insert IsdataAdmin;
    Opportunity tstOpp = Test_utils.createOpportunity(tstAcc.ID);
    tstOpp.OwnerId = tstUser.Id;
    tstOpp.StageName = Constants.OPPTY_STAGE_7;
        
    insert new List<Opportunity>{tstOpp};
    
    Order__c tstOrd = Test_utils.insertOrder(true,tstAcc.ID,tstCon.ID,tstOpp.ID);
    tstOrd.OwnerId = tstUser.Id;
        
    update new List<Order__c>{tstOrd};
    
    Asset tstAst = Test_utils.insertAsset(true,tstAcc.ID);
    tstAst.Start_Date__c = Date.today().addDays(-1);
    tstAst.Cancellation_Date__c = null;
    tstAst.SaaS__c = true;
    tstAst.Order__c = tstOrd.Id;

    update new List<Asset>{tstAst};
    
    delete IsdataAdmin;
        
  }
  
}