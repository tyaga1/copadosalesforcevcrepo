/******************************************************************************
 * Name: TestClassforexpUserRemovePermission.cls
 * Created Date: 5/12/2017
 * Created By: Charlie Park, UCInnovation
 * Description : Test class for expcommUserRemovePermissionLicense
 * Change Log- 
 ****************************************************************************/

@isTest
public class TestClassforexpUserRemovePermission {
        private static testMethod void doTest()
    {
            Profile getProfile = [SELECT Id FROM Profile where name ='Standard Platform User' limit 1]; 
                User newUser = new User(Alias = 'uniq', Email='sampleuser.testuser@experian.com', FirstName='Unique Test',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = getProfile.Id, phone = '555555555',
                TimeZoneSidKey='America/Los_Angeles', UserName='sampleUser2@experian.com', Department ='Test',
                EmployeeNumber='1', Function__c = 'Billing', Region__c='APAC');
                insert newUser; 
        		list<String> newUserList = new List<String>();
            	newUserList.add(String.valueOf(newUser.Id));

            	expcommUserAssignPermission.assignPermissionLicense(newUserList);

                /*
                List<PermissionSet> permSetList = [SELECT id, Label FROM PermissionSet where Label = 'Global Experian Employee Case Access' LIMIT 1];
                PermissionSet permissionSet = new PermissionSet();
                if (permSetList.size() > 0)
                {
                    permissionSet = permSetList[0];
                }
                
                

                //PermissionSet permissionSet = [SELECT id, Label FROM PermissionSet where Name = 'Global Experian Employee Case Access' LIMIT 1];
                PermissionSetLicense permissionSetLicense = [SELECT id, MasterLabel FROM PermissionSetLicense WHERE MasterLabel = 'Company Community for Force.com' LIMIT 1];
                //PermissionSet permissionSet = new PermissionSet(Name='Test', Label='Global Experian Employee Case Access');
                //PermissionSetLicense permissionSetLicense = new PermissionSetLicense(MasterLabel = 'Company Community for Force.com');

                //insert permissionSet;

                PermissionSetAssignment permSetAssignment = new PermissionSetAssignment(AssigneeId = newUser.Id, PermissionSetId = permissionSet.Id);
                PermissionSetLicenseAssign permSetLicenseAssn = new PermissionSetLicenseAssign(AssigneeId = newUser.Id, PermissionSetLicenseId = permissionSetLicense.Id);

                insert permSetAssignment;
                insert permSetLicenseAssn;
                */

        Test.startTest();

        List<String> listOfUserIds = new List<String>();
        listOfUserIds.add(String.valueOf(newUser.Id));
        expcommUserRemovePermissionLicense.removePermissionLicense(listOfUserIds);

        Test.stopTest();
        }
}