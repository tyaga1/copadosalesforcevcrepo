/**=====================================================================
 * Experian
 * Name: ExportPackageXMLCtrla
 * Description:This class is to create Package.xml and attach it in Notes and Attachment
 *
 * Created Date: 08-09-2015
 * Created By: Nur Azlini Ammary
 *
 * Date Modified      Modified By                  Description of the update
 * Dec 4th, 2015      Paul Kissick                 Moved try statement to before the update
 * Feb 24th, 2016     Paul Kissick                 Case 01871801: Replaced API version with label
 * Mar 9th, 2016      Paul Kissick                 ????????: Adding support to delete existing package.xml files when creating a new one.
 ===================================================================== */
public class ExportPackageXMLCtrla {
  
  public class ExportException extends Exception{}
  
  private Deployment_Request__c currentDr;
  private ApexPages.StandardController stdCon;
  
  private String error = '';
  private String packageVersion = Label.RMA_Metadata_API_Version; // PK: Replaced with label.
  
  public ExportPackageXMLCtrla(ApexPages.StandardController standardController) {
    stdCon = standardController;
    if (!Test.isRunningTest()) {
      stdCon.addFields(new List<String>{'Status__c','Id'});
    }
    currentDr = (Deployment_Request__c)stdCon.getRecord();
  }
  
  private String generateXmlFromDCList(List<Deployment_Component__c> dcList, Boolean isDelete) {
    Set<string> metadataTypes = new Set<string>();
    
    if (dcList != null && dcList.size() > 0) {
      for(Deployment_Component__c dc : dcList){
        metadataTypes.add(dc.Metadata_Type__c);
        if (dc.Metadata_Name__c.contains('*') && isDelete) {
          error = 'Wildcards not supported in Delete operations.';
          return ''; // bail out and return error...
        }
      }
    }
    
    // Generate package.xml
    XmlStreamWriter w = new XmlStreamWriter();
    w.writeStartDocument('UTF-8', '1.0');
    w.writeCharacters('\n');
    w.writeStartElement('', 'Package', 'http://soap.sforce.com/2006/04/metadata');
    w.writeNamespace('', 'http://soap.sforce.com/2006/04/metadata');
    w.writeCharacters('\n');
    w.writeCharacters('\t');
    if (!isDelete) {
      // Only add this is not a destructive changes package
      w.writeStartElement('', 'fullName', '');
      w.writeCharacters (dcList[0].DR_Label__c);
      w.writeEndElement();
      w.writeCharacters('\n');
      w.writeCharacters('\t');
      w.writeStartElement('', 'version', '');
      w.writeCharacters (packageVersion); 
      w.writeEndElement();
      w.writeCharacters('\n');
    }
    Set<String> metaDataSet = new Set<String>(); // Used to check for duplicate entries by type and name.

    for(Integer i = 0; i < dcList.size(); i++){
      system.debug('##### dcList[i] - ' + i + ' : ' + dcList[i]);
      if((i == 0) || (dcList[i].Metadata_Type__c != dcList[i-1].Metadata_Type__c)) {
        w.writeCharacters('\t');
        w.writeStartElement('', 'types', '');
        w.writeCharacters('\n');
      }
      system.debug('##### Metadata_Name__c - ' + i + ' : ' + dcList[i].Metadata_Name__c);
      if (!metaDataSet.contains(dcList[i].Metadata_Type__c+'~'+dcList[i].Metadata_Name__c)) {
        metaDataSet.add(dcList[i].Metadata_Type__c+'~'+dcList[i].Metadata_Name__c);
        w.writeCharacters('\t\t');
        w.writeStartElement('', 'members', '');
        w.writeCharacters(dcList[i].Metadata_Name__c);
        w.writeEndElement();
        w.writeCharacters('\n');
      }
      if(((i+1) == dcList.size()) || (dcList[i].Metadata_Type__c != dcList[i+1].Metadata_Type__c)) {
        w.writeCharacters('\t\t');
        w.writeStartElement('', 'name', '');
        w.writeCharacters(dcList[i].Metadata_Type__c);
        w.writeEndElement(); // Write </name>
        w.writeCharacters('\n');
        w.writeCharacters('\t');
        w.writeEndElement(); // Write </types>
        w.writeCharacters('\n');
      }
    }
    w.writeEndElement(); // Write </Package>
    w.writeCharacters('\n');
    w.writeEndDocument();
    string xmlOutput = w.getXmlString();
    w.close();
    return xmlOutput;
  }
  
  private String generateEmptyDestructivePackage() {
    XmlStreamWriter w = new XmlStreamWriter();
    w.writeStartDocument('UTF-8', '1.0');
    w.writeCharacters('\n');
    w.writeStartElement('', 'Package', 'http://soap.sforce.com/2006/04/metadata');
    w.writeNamespace('', 'http://soap.sforce.com/2006/04/metadata');
    w.writeCharacters('\n');
    w.writeCharacters('\t');
    w.writeStartElement('', 'version', '');
    w.writeCharacters (packageVersion); 
    w.writeEndElement();
    w.writeCharacters('\n');
    w.writeEndElement(); // Write </Package>
    w.writeCharacters('\n');
    w.writeEndDocument();
    String xmlOutput = w.getXmlString();
    w.close();
    return xmlOutput;
  }
  
  public PageReference generatePackageXML() {
    PageReference pr = stdCon.view();
    
    // Adding savepoint to allow rollback later.
    Savepoint sp = Database.setSavepoint();
    
    //List<Deployment_Request__c> DRStatus = new List<Deployment_Request__c>();
    currentDr.Status__c = 'Locked for Deployment';
    
    try{
      pr = stdCon.save();
      
      String fileContentType = 'application/xml';
      
      List<Deployment_Component__c> dcDeployList = [
        SELECT Metadata_Type__c,  Metadata_Name__c, DR_Label__c,
          Case_Component__r.Component_Name__r.Name
        FROM Deployment_Component__c 
        WHERE Slot__c = :currentDr.Id
        AND Deployment_Type__c = 'Automated Deployment'
        AND Case_Component__r.Action__c != 'Delete'
        ORDER BY Metadata_Type__c, Metadata_Name__c
      ];
      
      // Only create a package.xml if there are components to add to it!
      if (dcDeployList.size() > 0) {
        
        String fileNamePackage = 'package.xml';
        
        
        String xmlOutput = generateXmlFromDCList(dcDeployList, false);
        if (String.isBlank(xmlOutput)) {
          throw new ExportException('Failed to generate package.xml due to an error: '+error); //Need to create label
        }
        
        // CHECK FOR EXISTING package.xml files, and remove
        delete [SELECT Id FROM Attachment WHERE ParentId = :currentDr.Id AND Name = :fileNamePackage AND ContentType = :fileContentType];
        
        // system.debug('##### package.xml - ' + xmlOutput);
        Attachment attach1 = new Attachment( 
          Name = fileNamePackage,  
          Body = Blob.valueOf(xmlOutput),
          ContentType = fileContentType,
          ParentId = currentDr.Id
        );
        insert attach1;
      }
      
      List<Deployment_Component__c> dcRemoveList = [
        SELECT Metadata_Type__c,  Metadata_Name__c, DR_Label__c,
          Case_Component__r.Component_Name__r.Name
        FROM Deployment_Component__c 
        WHERE Slot__c = :currentDr.Id
        AND Deployment_Type__c = 'Automated Deployment'
        AND Case_Component__r.Action__c = 'Delete'
        ORDER BY Metadata_Type__c, Metadata_Name__c
      ];
      
      // Only create a package.xml if there are components to add to it!
      if (dcRemoveList.size() > 0) {
        
        String fileNameDelPack = 'destructiveChanges.xml';
        String fileNamePackage2 = 'package-delete.xml';
        
        String xmlOutput2 = generateXmlFromDCList(dcRemoveList, true);
        if (String.isBlank(xmlOutput2)) {
          throw new ExportException('Failed to generate destructiveChanges.xml due to an error: '+error); //Need to create label
        }
        
        delete [SELECT Id 
                FROM Attachment 
                WHERE ParentId = :currentDr.Id 
                AND ContentType = :fileContentType 
                AND Name IN (:fileNameDelPack, :fileNamePackage2)
        ];
        
        // system.debug('##### package.xml - ' + xmlOutput);
        Attachment attach2 = new Attachment(
          Name = fileNameDelPack,
          Body = Blob.valueOf(xmlOutput2),
          ContentType = fileContentType,
          ParentId = currentDr.Id
        );
        insert attach2;
        
        String xmlOutput3 = generateEmptyDestructivePackage();
        if (String.isBlank(xmlOutput3)) {
          throw new ExportException('Failed to generate package-delete.xml due to an error: '+error); //Need to create label
        }
        Attachment attach3 = new Attachment(
          Name = fileNamePackage2,
          Body = Blob.valueOf(xmlOutput3),
          ContentType= fileContentType,
          ParentID = currentDr.Id
        );
        insert attach3;
        
      }
      
      return pr; //PK: Added to return to the deployment request after adding the attachment.
    }
    catch(Exception ex) {
      database.rollback(sp);
      system.debug('## Caught APEX exception '+ex.getMessage()+' [Code: ExportPackageXMLCtrl | Method: generatePackageXML]');
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Your request could not be handled at this time. Please try again later or contact your administrator for assistance.' +ex.getMessage());  //Need to create label
      ApexPages.addMessage(myMsg);
    }
    return null;
  }
  
}