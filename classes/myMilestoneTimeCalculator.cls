/**=====================================================================
  * Experian
  * Name: myMilestoneTimeCalculator
  * Description: Class used to calulate the Milestonetime based on SLA dynamically. (W-008058)
  *           
  * Created Date: April 18th 2017
  * Created By: Manoj Gopu 
  *
  * Date Modified      Modified By                  Description of the update
  =====================================================================*/
  global class myMilestoneTimeCalculator implements Support.MilestoneTriggerTimeCalculator {
       
     global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId){
         // Querying Case from the default Entitlement and get the SLA dynamically    
        Case c = [SELECT SLA__c,Status FROM Case WHERE Id=:caseId];      
        if (c.SLA__c!=null){
             integer sla = integer.valueof(c.SLA__c);
             return sla*60;
        }
        else {
            return 24*60;
        }
              
     }
}