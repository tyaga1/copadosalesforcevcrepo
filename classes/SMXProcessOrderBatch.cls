/**=====================================================================
 * Experian
 * Name: SMXProcessOrderBatch 
 * Description: 
 * Created Date: Unknown
 * Created By: Unknown
 * 
 * Date Modified      Modified By        Description of the update
 * Jul 22nd, 2015     Paul Kissick       Case #01040868 - Fixing nested soql query within for loop
 * Sep 9th, 2015      Paul Kissick       I-179463: Duplicate Management Failures
 * July 12th,2016(QA) Tyaga Pati         CRM2:W-005363 - Added code to update the Account on Survey record as part of the nomination record
                                         creation process.
 =====================================================================*/
global class SMXProcessOrderBatch implements Database.Batchable<Order__c>, Database.AllowsCallouts{

  global Iterable<Order__c> start(database.batchablecontext BC){
    List<String> businessUnitStr = new List<String>();
    List<Business_Unit_Group_Mapping__c> businessUnitList = [
      SELECT User_Business_Unit__c 
      FROM Business_Unit_Group_Mapping__c
      WHERE Common_Group_Name__c = 'EDQ'
    ];
    for(Business_Unit_Group_Mapping__c businessUnit : businessUnitList){
        businessUnitStr.add(businessUnit.User_Business_Unit__c);
    }
    
    List<Order__c> OrderToUpdate = [
      SELECT Id, Contact__c, Contact__r.AccountId, (SELECT Id FROM Survey__r) 
      FROM Order__c
      WHERE Renewal_Survey_Date__c = TODAY 
      AND Owner_Business_Unit__c IN :businessUnitStr 
      AND Owner_Region__c IN ('UK&I','North America')
      AND Contact__c != null
    ];
    return OrderToUpdate; 
  }

  global void execute(Database.BatchableContext BC, List<Order__c> scope) {
 
    String strSurveyName = 'EDQ Contract Renewal';
    String strSurveyId = 'EXPERIAN_106614';
    
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Long lgSeed = System.currentTimeMillis();

    for(Order__c ord : scope) {
      // List<Feedback__c> lstSurvey = [select Id from Feedback__c where Order__c =: ord.Id ];
      List<Feedback__c> lstSurvey = ord.Survey__r;
      //system.debug('test case id ' +ord.Id);
      //system.debug('Feedback id '+ lstSurvey.get(0).id);
      if (lstSurvey.isEmpty()) {
        lgSeed = lgSeed + 1;
        Feedback__c feedback = new Feedback__c();
        feedback.Order__c = ord.Id;
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = ord.Contact__c; //ContactName 
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionName__c = strSurveyName;
        feedback.DataCollectionId__c = strSurveyId;
        feedback.Account__c = ord.Contact__r.AccountId;//CRM2:W-005363
        feedbackList.add(feedback);        
      }
    }
    insert feedbackList;
 }

  global void finish (Database.BatchableContext info) {
  }//global void finish loop
    
  
}