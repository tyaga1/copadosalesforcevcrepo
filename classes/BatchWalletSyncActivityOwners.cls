/**=====================================================================
 * Experian
 * Name: BatchWalletSyncActivityOwners
 * Description: Designed to update the ownership of activities based on the changes to accounts from the WalletSync process
 *
 *  NOTE:
 *    This is started from the BatchWalletSyncOpportunityOwners batch.
 * 
 * Created Date: 3rd, August, 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description of the update
 * Aug 24th, 2015               Paul Kissick                 Updated to use WhatID on Tasks for Account
 * Aug 25th, 2015               Noopur                       Commented the code for process Complete and added the batch schedule for WalletSyncAccountSegments
 * Sep 10th, 2015               Paul Kissick                 Added try/catch around email sending
 * Oct 1st, 2015                Paul Kissick                 Changing lastStartDate to Wallet_Sync_Processing_Last_Run__c
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Nov 27th, 2015               Paul Kissick                 Case 01266075: Replacing Global_Setting__c with new Batch_Class_Timestamp__c
 =====================================================================*/

global class BatchWalletSyncActivityOwners implements  Database.Batchable<sObject>, Database.Stateful {
  
  global Datetime startTime;
  global static Set<String> openTasks = new Set<String>{'In Progress','Not Started'};
  
  global List<String> updateErrors;
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    updateErrors = new List<String>();
    Datetime lastStartDate = WalletSyncUtility.getWalletSyncProcessingLastRun();
    return Database.getQueryLocator([
      SELECT Id, Previous_Account_Owner__c, 
        Account_Owner__c, Account__c
      FROM WalletSync__c
      WHERE Previous_Account_Owner__c != null
      AND Account_Owner__c != null
      AND Account__c != null
      AND Last_Processed_Date__c >= :lastStartDate
    ]);
  }
  
  
  
  global void execute(Database.BatchableContext BC, List<WalletSync__c> scope) {
    
    // Need to replace the activity owner (task and events) based on the CNPJ on account & opp
    
    // Build a map of cnpj to owners, both old and new
    Map<Id,Id> cnpjToNewOwner  = new Map<Id,Id>();
    Map<Id,Id> cnpjToOldOwner  = new Map<Id,Id>();
    
    for(WalletSync__c ws : scope) {
      cnpjToNewOwner.put(ws.Account__c, ws.Account_Owner__c);
      cnpjToOldOwner.put(ws.Account__c, ws.Previous_Account_Owner__c);
    }
    
    // This will find all tasks for any account referenced on the tasks
    
    List<Task> allTasks = [
      SELECT Id, OwnerId, WhatID
      FROM Task
      WHERE Status IN :openTasks
      AND OwnerId IN :cnpjToOldOwner.values()
      AND WhatID IN :cnpjToOldOwner.keySet()
    ];
    
    for(Task tsk : allTasks) {
      // String tskCnpj = tsk.Account.Root_CNPJ__c;
      if (tsk.OwnerId == cnpjToOldOwner.get(tsk.WhatId)) {
        tsk.OwnerId = cnpjToNewOwner.get(tsk.WhatId);
      }
    }
    
    List<Database.SaveResult> updTasksRes = Database.update(allTasks,false);
    for(Database.SaveResult sr : updTasksRes) {
      if(!sr.isSuccess()) {
        for(Database.Error err : sr.getErrors()) {
          updateErrors.add(err.getMessage());
        }
      }
    }
    /*
    
    // Not sure we need to do events...
    List<Event> allEvents = [
      SELECT Id, OwnerId, Account.Root_CNPJ__c
      FROM Event
      WHERE Status IN ('In Progress','Not Started')
      AND OwnerId IN :cnpjToOldOwner.values()
      AND Account.Root_CNPJ__c IN :cnpjToOldOwner.keySet()
    ];
    
    // Same process as tasks
    
    for(Event evt : allEvents) {
      String evtCnpj = evt.Account.Root_CNPJ__c;
      if (evt.OwnerId == cnpjToOldOwner.get(evtCnpj)) {
        evt.OwnerId = cnpjToNewOwner.get(evtCnpj);
      }
    }
    
    Database.update(allEvents);
    */
  }
  
  
  global void finish(Database.BatchableContext BC) {
    try {
      BatchHelper bh = new BatchHelper();
      bh.checkBatch(BC.getJobId(), 'BatchWalletSyncActivityOwners', false);
    
      if (updateErrors.size() > 0) {
        bh.batchHasErrors = true;
        bh.emailBody += '\nThe following errors were observed when updating records:\n';
        bh.emailBody += String.join(updateErrors,'\n');
      }
    
      bh.sendEmail();
    }
    catch (Exception e) {
      system.debug(e.getMessage());
    }
    // THIS IS THE END OF THE BATCH SO CLOSE OFF
    //WalletSyncUtility.processingCompleted();
    if (!Test.isRunningTest()) { // Must be within this check as tests cannot start other batches
      // THIS BATCH, WHEN FINISHED, STARTS THE NEXT BATCH OF ACCOUNT SEGMENT UPDATE
      system.scheduleBatch(new BatchWalletSyncAccountSegments(), 'BatchWalletSyncAccountSegments'+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('BatchWalletSyncAccountSegments'));
    }
    
  }
  
}