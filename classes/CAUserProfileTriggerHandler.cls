/**=====================================================================
 * Experian
 * Name: CAUserProfilePlanTrigger
 * Description: Trigger on Career_Architecture_User_Profile__c
 * Created Date: Jul. 19th 2017
 * Created By: James Wills
 * 
 *  Date Modified      Modified By                  Description of the update
 *  July 12th 2017     James Wills                  ITCA:W-008961 - Approval Process code.
 *  3rd August 2017    James Wills                  ITCA:W-008961 - Updated for new requirements.
 *  31st August 2017   Malcolm Russell              ITCA added skillsProfileUpdate to add and remove skills from profile
 *  4th September 2017 Alexander McCall             ITCA - Uncommented lines 17&22 in order to achieve test class coverage
 *  7Th September 2017 Malcolm Russell              ITCA - Email format updates and BCC to CC
=====================================================================*/

public without sharing class CAUserProfileTriggerHandler{

  public static void afterInsert(List<Career_Architecture_User_Profile__c> newList){   
    sendEmailstoEmployeeFollowingApprovalProcess_afterInsert(newList); //AM Un-Commented
  }

  
  public static void afterUpdate(List<Career_Architecture_User_Profile__c> newList, Map<ID,Career_Architecture_User_Profile__c>oldMap){    
    sendEmailstoEmployeeFollowingApprovalProcess_afterUpdate(newList, oldMap); //AM Un-commented
    skillsProfileUpdate(newList, oldMap);             
  }
  
  
  
  
  public static void sendEmailstoEmployeeFollowingApprovalProcess_afterInsert(List<Career_Architecture_User_Profile__c> newList){
    List<Career_Architecture_User_Profile__c> caUserProfilesApprovalProcess = new List<Career_Architecture_User_Profile__c>();
    
    for(Career_Architecture_User_Profile__c casp : newList){
      if(casp.Status__c =='Submitted to Manager'){
        caUserProfilesApprovalProcess.add(casp);
      }
    }
    
    if(caUserProfilesApprovalProcess.isEmpty()){
      return;
    }
    
    Map<ID,ID> user_Map = new Map<ID,ID>();
    for(Career_Architecture_User_Profile__c caUserProf : caUserProfilesApprovalProcess){
      user_Map.put(caUserProf.id, caUserProf.Employee__c);
    }
    
    Map<ID,User> caUser_List = new Map<ID,User>([SELECT id, Name, Email, Manager.Name, Manager.Email FROM User WHERE id IN :user_Map.values()]);
    
    Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
    
    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@experian.com'];
    Id oweaId;
    if(owea.size() > 0){
      oweaId = owea.get(0).Id;
    }
    
    
    for(Career_Architecture_User_Profile__c userProf :  caUserProfilesApprovalProcess){
  
      String emailBody = 'Dear ' + caUser_List.get(user_Map.get(userProf.id)).Name + ',<br/>'; 
            
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setOrgWideEmailAddressId(oweaId);
      mail.setSaveAsActivity(false);
      //mail.setTargetObjectId(UserInfo.getUserId());
      mail.setToAddresses(new String[]{caUser_List.get(user_Map.get(userProf.id)).Email});
      //mail.setToAddresses(new String[]{'james.wills@experian.com'});
       
      if(userProf.Status__c=='Submitted to Manager'){
        mail.setSubject('Skills Profile SUBMITTED');
        emailBody += '<br/>Congratulations on taking the first step to build your skill profile as a tool for building your career within Experian IT Services!  ' +
                     'You should also schedule a conversation with your manager to discuss the '+
                     'content of your profile and your areas of interest regarding development.<br/><br/>'+
                     'You can always go back to modify your skills profile with new or updated information.<br/><br/>Thank you for completing your skill profile!' +
                     '<br/> From,<br/>IT Career Architecture Portal';      
        mail.setReplyTo('noreply@experian.com');
        mail.setCcAddresses(new String[]{caUser_List.get(userProf.Employee__c).Manager.Email});  
        mail.setUseSignature(false);
        mail.setHtmlBody(emailBody);
        messages.add(mail);
      }
    }
    if(!messages.isEmpty()){
      Messaging.sendEmail(messages);
    }
    
  }
  
  public static void sendEmailstoEmployeeFollowingApprovalProcess_afterUpdate(List<Career_Architecture_User_Profile__c> newList, Map<ID,Career_Architecture_User_Profile__c>oldMap){
    
    List<Career_Architecture_User_Profile__c> caUserProfilesApprovalProcess = new List<Career_Architecture_User_Profile__c>();
    
    for(Career_Architecture_User_Profile__c casp : newList){
      if(casp.Status__c =='Discussed with Employee' && casp.Status__c!= oldMap.get(casp.id).Status__c){
        caUserProfilesApprovalProcess.add(casp);
      }
    }
    
    if(caUserProfilesApprovalProcess.isEmpty()){
      return;
    }
    
    Map<ID,ID> user_Map = new Map<ID,ID>();
    for(Career_Architecture_User_Profile__c caUserProf : caUserProfilesApprovalProcess){
      user_Map.put(caUserProf.id, caUserProf.Employee__c);
    }
    
    Map<ID,User> caUser_Map = new Map<ID,User>([SELECT id, Name, Email, Manager.Name, Manager.Email FROM User WHERE id IN :user_Map.values()]);
    
    Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();
    
    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@experian.com'];
    Id oweaId;
    if ( owea.size() > 0 ) {
       oweaId = owea.get(0).Id;
    }
    
    
    for(Career_Architecture_User_Profile__c userProf :  caUserProfilesApprovalProcess){
  
      String emailBody = 'Dear ' + caUser_Map.get(user_Map.get(userProf.id)).Name + ',<br/>'; 
            
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setOrgWideEmailAddressId(oweaId);
      
      mail.setSaveAsActivity(false);
      //mail.setTargetObjectId(UserInfo.getUserId());
      mail.setToAddresses(new String[]{caUser_Map.get(user_Map.get(userProf.id)).Email});
      //mail.setToAddresses(new String[]{'james.wills@experian.com'});
       
      if(userProf.Status__c == 'Discussed with Employee'){
          mail.setSubject('Skills Profile DISCUSSED');
          emailBody += '<br/>Congratulations!  Your line manager has confirmed that you have reviewed your skill profile together and discussed ways you can use this ' +
                         'information to look at ways to grow your skills through stretch assignments and/or training.  You can also begin to look across the skills used in other ' + 
                         'EITS Career Areas to think about how you can grow the breadth of your skills as well as depth.  For assistance with this, please work with your line ' +
                         'manager and/or HR Business Partner.'+
                         '<br/><br/>From,<br/>IT Career Architecture Portal';                                
      }
      mail.setReplyTo('noreply@experian.com');
      mail.setCcAddresses(new String[]{caUser_Map.get(userProf.Employee__c).Manager.Email});  
      mail.setUseSignature(false);
      mail.setHtmlBody(emailBody);
      messages.add(mail);
    }

    if(!messages.isEmpty()){
      Messaging.sendEmail(messages);
    }
  
  }
  
   public static void skillsProfileUpdate(List<Career_Architecture_User_Profile__c> newList, Map<ID,Career_Architecture_User_Profile__c>oldMap){
    
    list<Career_Architecture_User_Profile__c> caupAdd= new list<Career_Architecture_User_Profile__c>();
    list<Career_Architecture_User_Profile__c> caupRemove= new list<Career_Architecture_User_Profile__c>();
    set<id> caupProfilesID = new set<id>();
    set<id> EmployeeID = new set<id>();

    
    for(Career_Architecture_User_Profile__c caup : newList){
      if(caup.Make_Skills_Public__c != oldMap.get(caup.id).Make_Skills_Public__c){
        if(caup.Make_Skills_Public__c){caupAdd.add(caup);}
         else{caupRemove.add(caup);}     
              }
       EmployeeID.add(caup.employee__c);
       caupProfilesID.add(caup.id);       
    }
    
   Map<Id, List<id>>  mapCASkill= new Map<Id, List<id>>(); 
    
            for(Career_Architecture_Skills_Plan__c casp : [select skill__c,Career_Architecture_User_Profile__c from
                                                           Career_Architecture_Skills_Plan__c 
                                                           where Career_Architecture_User_Profile__c in :caupProfilesID]){
                                                   
                    if(!mapCASkill.containsKey(casp.Career_Architecture_User_Profile__c)){
                        mapCASkill.put(casp.Career_Architecture_User_Profile__c, new List<id>());             
                    }
                     mapCASkill.get(casp.Career_Architecture_User_Profile__c).add(casp.skill__c);                               
              }
             
              Map<Id, List<ProfileSKillUser>>  mapProfileSkill= new Map<Id, List<ProfileSKillUser>>(); 
               for(ProfileSKillUser ps : [SELECT id, UserId, ProfileSkillId FROM ProfileSKillUser WHERE UserId in :EmployeeID]){
                                                   
                    if(!mapProfileSkill.containsKey(ps.UserID)){
                        mapProfileSkill.put(ps.UserID, new List<ProfileSKillUser>());             
                    }
                     mapProfileSkill.get(ps.UserID).add(ps);                               
              }

        
    if(!caupAdd.isEmpty()){
    
    List <ProfileSkillUser> skillUserToInsert = new List <ProfileSkillUser> ();
    
      for(Career_Architecture_User_Profile__c ca :caupAdd){
      if(!mapCASkill.isEmpty()){
        for(id cs : mapCASkill.get(ca.id)){
         ProfileSkillUser S = new ProfileSkillUser();
        
      // map the fields from skill plan to user skill
      S.UserId = ca.employee__c;
      S.ProfileSkillID = cs;
      //add these objects to the list to insert
      skillUserToInsert.add(S);

      }
      }
    }
     try{
       insert skillUserToInsert;}
    catch(system.DmlException e){
       System.debug(e);
      // ApexPages.Message dmlWarn = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
      // ApexPages.addMessage(dmlWarn);
      } 
      }
    
    if(!caupRemove.isEmpty()){
    
    List <ProfileSkillUser> skillUserToDelete = new List <ProfileSkillUser> ();

      
      for(Career_Architecture_User_Profile__c cr :caupRemove){
      
      if(!mapCASkill.isEmpty()){
      Set<id> setEmpSkills = new Set<id>(mapCASkill.get(cr.id));

        for(ProfileSKillUser ps : mapProfileSkill.get(cr.employee__c)){
         if( setEmpSkills.contains(ps.ProfileSkillId)){
            skillUserToDelete.add(ps); 
         }
         
      }
     }
    }
     try{
       delete  skillUserToDelete;}
    catch(system.DmlException e){
       System.debug(e);
      // ApexPages.Message dmlWarn = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
      // ApexPages.addMessage(dmlWarn);
      } 


   } 
  }
  
  
  
}