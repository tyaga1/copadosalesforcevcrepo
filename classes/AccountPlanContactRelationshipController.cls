/**=====================================================================
 * Appirio, Inc
 * Name        : AccountPlanContactRelationshipController
 * Description : Controller class for page AccountPlanContactRelationship (for T-434656)
 * Created Date: Sept 17th, 2015
 * Created By  : Jagjeet Singh (Appirio)
 * Modified On             Modified By                   Description
 * 24th Sept, 2015         Jagjeet Singh(Appirio)        T-434657 - Added another constructor for extension.
 * 20th June, 2016         James Wills                   Case 01966786: Update Cookies so that correct tab is returned to.  
 =====================================================================*/
public with sharing class AccountPlanContactRelationshipController {
    //Declare variables
    public String accPlanId{get;set;}
    
    public ID selectedID {get;set;}
    
    public List<ObjectStructureMap> asm ;
    public Map<String, ObjectStructureMap> masm;
    public List<Integer> maxLevel;
    //accountPlanContactList
    private List<Account_Plan_Contact__c> listAccountPlanContacts;
    //list of Parent Contacts.
    private List<Account_Plan_Contact__c> listTopContacts;
    //map Parent Contact and the related child contacts. 
    private Map<String, List<Account_Plan_Contact__c>> mapParent_LstContact;
    private Set<Id> accPlanConIds;
    public Account_Plan__c accountPlanObj{get;set;}
    
    /**
    * Contructor
    */
    public AccountPlanContactRelationshipController() {
        this.asm = new List<ObjectStructureMap>{};
        this.masm = new Map<String, ObjectStructureMap>{};
        this.maxLevel = new List<Integer>{};
        listTopContacts = new List<Account_Plan_Contact__c>();
        //retrieve the accPlanId from the page.
        accPlanId = ApexPages.currentPage().getParameters().get('accPlanId') ;
        listAccountPlanContacts = new List<Account_Plan_Contact__c>();
        mapParent_LstContact = new Map<String,List<Account_Plan_Contact__c>>();
        accPlanConIds = new Set<Id>();
        accountPlanObj = new Account_Plan__c();
    }
    
    /**
    * Parameterised COnstructor used for extensions.
    */
    public AccountPlanContactRelationshipController(ApexPages.StandardController stdController) {
        accountPlanObj = (Account_Plan__c)stdController.getRecord();
        accPlanId = accountPlanObj.Id;
        this.asm = new List<ObjectStructureMap>{};
        this.masm = new Map<String, ObjectStructureMap>{};
        this.maxLevel = new List<Integer>{};
        listTopContacts = new List<Account_Plan_Contact__c>();
        //retrieve the accPlanId from the page.
        accPlanId = ApexPages.currentPage().getParameters().get('accPlanId') ;
        listAccountPlanContacts = new List<Account_Plan_Contact__c>();
        mapParent_LstContact = new Map<String,List<Account_Plan_Contact__c>>();
        accPlanConIds = new Set<Id>();
        accountPlanObj = new Account_Plan__c();
        ApexPages.getMessages().clear();//Case 01966786: Prevent Warning messages displaying on other tabs for incorrectly formed HTML from standard functionality
    }
    
    /**
    * Allow page to set the current ID
    */
    public void setcurrentId( String cid ){
        accPlanId = cid;
    }

    /**
    * Return ObjectStructureMap to page
    * @return asm
    */
    public List<ObjectStructureMap> getObjectStructure(){
        asm.clear();
        if ( accPlanId == null ) {
            accPlanId = System.currentPageReference().getParameters().get( 'id' );
        }
        //if we do not have AccPlan Id supplied in URL display error.
        System.assertNotEquals( accPlanId, null, 'sObject ID must be provided' );
        //fetch all the Account Plan Contacts.
        listAccountPlanContacts = fetchAccPlanContacts(accPlanId);
        //get the top Parent Contacts which are not reportees.
        listTopContacts = GetTopContacts(listAccountPlanContacts);
        if(listAccountPlanContacts.isEmpty()){
            return null;
        }
        if(listTopContacts.isEmpty()){
            return null;
        }
        //debug logs
        system.debug('listAccountPlanContacts '+listAccountPlanContacts);
        system.debug('topcontacts '+listTopContacts);
        
        asm = formatObjectStructure( accPlanId );
        
        return asm;
    }

    /**
    * Query Account from top down to build the ObjectStructureMap
    * @param currentId
    * @return asm
    */
    public ObjectStructureMap[] formatObjectStructure( String accPlanId){
    
        List<ObjectStructureMap> asm = new List<ObjectStructureMap>{};
        masm.clear();

        //Change below
        List<Account_Plan_Contact__c> listAccPlanCon            = new List<Account_Plan_Contact__c>{};
        List<ID> currentParent      = new List<ID>{};
        Map<ID, String> nodeList    = new Map<ID, String>{};
        List<String> nodeSortList   = new List<String>{};
        List<Boolean> levelFlag     = new List<Boolean>{};
        List<Boolean> closeFlag     = new List<Boolean>{};
        String nodeId               = '0';
        String nodeType             = 'child';
        Integer count               = 0;
        Integer level               = 0;
        Boolean endOfStructure      = false;
        
        //Find highest level obejct in the structure
        currentParent.add(accPlanId);

        //Loop though all children
        while ( !endOfStructure ){
            if( level == 0 ){
                //Change below     
                listAccPlanCon = listTopContacts;
            } 
            else {
                //TODO : Query can be avoided and values can be retrieved from the mapParent_LstContact
                //Change below      
                listAccPlanCon = [ SELECT Id, Reports_To__c, Contact__c, Contact_Name__c, 
                                              Contact_Job_Title__c, Account_Plan__c,
                                              Contact__r.ReportsToId, Experian_Relationship__c,
                                              Primary_Contact_Role__c
                                   FROM Account_Plan_Contact__c 
                                   WHERE Contact__r.ReportsToId =: CurrentParent and Id In:listAccountPlanContacts
                                   ORDER BY Contact_Name__c, Contact_Job_Title__c ];
            }

            if( listAccPlanCon.size() == 0 ){
                endOfStructure = true;
            }
            else{
                currentParent.clear();
                for ( Integer i = 0 ; i < listAccPlanCon.size(); i++ ){
                    //Change below
                    Account_Plan_Contact__c apc = listAccPlanCon[i];
                    nodeId = ( level > 0 ) ? NodeList.get( apc.Contact__r.ReportsToId )+'.'+String.valueOf( i ) : String.valueOf( i );
                    masm.put( NodeID, new ObjectStructureMap( nodeID, levelFlag, closeFlag, nodeType, false, false, apc ) );
                    currentParent.add( apc.Contact__c );
                    nodeList.put( apc.Contact__c,nodeId );
                    nodeSortList.add( nodeId );
                }
                
                maxLevel.add( level );                
                level++;
            }
        }
        //Account structure must now be formatted
        NodeSortList.sort();
        for( Integer i = 0; i < NodeSortList.size(); i++ ){
            List<String> pnl = new List<String> {};
            List<String> cnl = new List<String> {};
            List<String> nnl = new List<String> {};
            
            if ( i > 0 ){
                String pn   = NodeSortList[i-1];
                pnl         = pn.split( '\\.', -1 );
            }

            String cn   = NodeSortList[i];
            cnl         = cn.split( '\\.', -1 );

            if( i < NodeSortList.size()-1 ){
                String nn = NodeSortList[i+1];
                nnl = nn.split( '\\.', -1 );
            }
            
            ObjectStructureMap tasm = masm.get( cn );
            if ( cnl.size() < nnl.size() ){
                //Parent
                tasm.nodeType = ( isLastNode( cnl ) ) ? 'parent_end' : 'parent';
            }
            else if( cnl.size() > nnl.size() ){
                tasm.nodeType   = 'child_end';
                tasm.closeFlag  = setcloseFlag( cnl, nnl, tasm.nodeType );
            }
            else{
                tasm.nodeType = 'child';
            }
            
            tasm.levelFlag = setlevelFlag( cnl, tasm.nodeType ); 
            
            //Change below
            if ( tasm.accPlanCon.id == accPlanId ) {
                tasm.currentNode = true;
            }
            asm.add( tasm );
        }
        
        asm[0].nodeType             = 'start';
        asm[asm.size()-1].nodeType  = 'end';
        
        return asm;
    }
    
    /**
    * Determin parent elements relationship to current element
    * @return flagList
    */
    public List<Boolean> setlevelFlag( List<String> nodeElements, String nodeType ){
        
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode   = '';
        String workNode     = '';
        Integer cn          = 0;
        
        for( Integer i = 0; i < nodeElements.size() - 1; i++ ){
            cn = Integer.valueOf( nodeElements[i] );
            cn++;
            searchNode  = workNode + String.valueOf( cn );
            workNode    = workNode + nodeElements[i] + '.';
            if ( masm.containsKey( searchNode ) ){
                flagList.add( true );
            }
            else {
                flagList.add( false );
            }
        }
        
        return flagList;
    }
    
    /**
    * Determin if the element is a closing element
    * @return flagList
    */
    public List<Boolean> setcloseFlag( List<String> cnl, List<String> nnl, String nodeType ){
        
        List<Boolean> flagList = new List<Boolean>{};
        String searchNode   = '';
        String workNode     = '';
        Integer cn          = 0;
        
        for( Integer i = nnl.size(); i < cnl.size(); i++ ){
            flagList.add( true );
        }
        
        return flagList;
    }
    
    /**
    * Determin if Element is the bottom node  
    * @return Boolean
    */
    public Boolean isLastNode( List<String> nodeElements ){
        
        String searchNode   = '';
        Integer cn          = 0;
        
        for( Integer i = 0; i < nodeElements.size(); i++ ){
            if ( i == nodeElements.size()-1 ){
                cn = Integer.valueOf( nodeElements[i] );
                cn++;
                searchNode = searchNode + String.valueOf( cn );
            }
            else {
                searchNode = searchNode + nodeElements[i] + '.';
            }
        }
        if ( masm.containsKey( searchNode ) ){
            return false;
        }
        else{
            return true;
        }
    }
    
        //----------------------------------------------------------------------------
      // Method to get all account plan contacts related to the account plan
      //----------------------------------------------------------------------------
      private List<Account_Plan_Contact__c> fetchAccPlanContacts(String accPlanId){
        //Account Plan Contact List
        List<Account_Plan_Contact__c> lstAccPlanContact = [SELECT Id, Reports_To__c, Contact__c, Contact_Name__c, 
                                                  Contact_Job_Title__c, Account_Plan__c,
                                                  Contact__r.ReportsToId, Experian_Relationship__c,
                                                  Primary_Contact_Role__c
                                           FROM Account_Plan_Contact__c 
                                           WHERE Account_Plan__c =: accPlanId
                                           ORDER BY Contact_Name__c, Contact_Job_Title__c];
        return lstAccPlanContact;
      }
    
    //gets the top contact list for a contact.
    public List<Account_Plan_Contact__c> GetTopContacts(List<Account_Plan_Contact__c> lstAccPlanContact){
        
        List<Account_Plan_Contact__c> lstTopContact = new List<Account_Plan_Contact__c>();
        Map<Id,Account_Plan_Contact__c> mapAccPlanContact = new Map<Id,Account_Plan_Contact__c>();
        
        for(Account_Plan_Contact__c apContact : lstAccPlanContact){
            //fill in the map and also the populate the Id Set
            mapAccPlanContact.put(apContact.Contact__c,apContact);
            accPlanConIds.Add(apContact.Id);
        }  
        // loop over the AccountPlanContact List and identify the parent contacts
        for(Account_Plan_Contact__c apContact : lstAccPlanContact){
            //If it is top contact, add it in top contact list
            if(apContact.Contact__r.ReportsToId == null || !mapAccPlanContact.containsKey(apContact.Contact__r.ReportsToId)){
                lstTopContact.add(apContact);
             }//Else, add it in a map with key as its ReportsTo contact name, and value as list of child contacts
           else{
                if(!mapParent_LstContact.containsKey(apContact.Contact__r.ReportsToId)){
                    mapParent_LstContact.put(apContact.Contact__r.ReportsToId, new List<Account_Plan_Contact__c>());
                }
                mapParent_LstContact.get(apContact.Contact__r.ReportsToId).add(apContact);
              }
        }
        return lstTopContact;
    }
    
    
    /**
    * Wrapper class
    */
    public with sharing class ObjectStructureMap{

        public String nodeId;
        public Boolean[] levelFlag = new Boolean[]{};
        public Boolean[] closeFlag = new Boolean[]{};
        public String nodeType;
        public Boolean currentNode;
        
        /**
        * @Change this to your sObject
        */
        public Account_Plan_Contact__c accPlanCon;
        
        public String getnodeId() { return nodeId; }
        public Boolean[] getlevelFlag() { return levelFlag; }
        public Boolean[] getcloseFlag() { return closeFlag; }
        public String getnodeType() { return nodeType; }
        public Boolean getcurrentNode() { return currentNode; }


        /**
        * @Change this to your sObject
        */
        public Account_Plan_Contact__c getaccPlanCon() { return accPlanCon; }
        
        public void setnodeId( String n ) { this.nodeId = n; }
        public void setlevelFlag( Boolean l ) { this.levelFlag.add(l); }
        public void setlcloseFlag( Boolean l ) { this.closeFlag.add(l); }
        public void setnodeType( String nt ) { this.nodeType = nt; }
        public void setcurrentNode( Boolean cn ) { this.currentNode = cn; }

        /**
        * @Change this to your sObject
        */
        public void setaccPlanCon( Account_Plan_Contact__c acp ) { this.accPlanCon = acp; }

        /**
        * @Change the parameters to your sObject
        */
        public ObjectStructureMap( String nodeId, Boolean[] levelFlag,Boolean[] closeFlag , String nodeType, Boolean lastNode, Boolean currentNode, Account_Plan_Contact__c a ){
            
            this.nodeId         = nodeId;
            this.levelFlag      = levelFlag; 
            this.closeFlag      = closeFlag;
            this.nodeType       = nodeType;
            this.currentNode    = currentNode;

            //Change this to your sObject  
            this.accPlanCon = a;
        }
    }
  
  
  public PageReference editAccountPlanContact(){
    contactsTabUpdateCookies();
    PageReference accountPlanContactToEdit = new PageReference('/' + selectedId);
    return accountPlanContactToEdit;
  }//Case 01966786
  
    
  public void contactsTabUpdateCookies(){
    AccountPlanUtilities.setAccountPlanCookies(accPlanId, Label.ACCOUNTPLANNING_ContactsTab);
  }//Case 01966786
    
}