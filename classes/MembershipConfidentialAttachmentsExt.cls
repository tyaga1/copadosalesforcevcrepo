/*=============================================================================
 * Experian
 * Name: MembershipConfidentialAttachmentsExt
 * Description: W-005297 : Extension for page MembershipConfidentialAttachments
 *              to return all related attachments in Confidential_Information__c
 *              records attached to Membership records.
 * Created Date: 13 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 13 Jul 2016 (QA)   Paul Kissick          CRM2:W-005297 : Extension for page MembershipConfidentialAttachments
 * 18 Jul 2016 (QA)   Paul Kissick          CRM2:W-005297 : Adding extra support for chatter files.
 =============================================================================*/

public with sharing class MembershipConfidentialAttachmentsExt {
  
  ApexPages.StandardController stdCon;
  Membership__c currentRecord;
  
  public MembershipConfidentialAttachmentsExt(ApexPages.StandardController con) {
    stdCon = con;
    currentRecord = (Membership__c)stdCon.getRecord();
  }
  
  public class CombinedAttachmentWrapper {
    public CombinedAttachment combAtt {get;set;}
    public Id contDocId {get;set;}
    public String membName {get;set;}
    public Boolean getHasContDocId() {
      return (combAtt.RecordType == 'File' && contDocId != null);
    }
    public CombinedAttachmentWrapper(CombinedAttachment ca) {
      combAtt = ca;
    }
  }
  
  public List<CombinedAttachmentWrapper> allConfAtts {
    get {
      if (allConfAtts == null) {
        allConfAtts = new List<CombinedAttachmentWrapper>();
      }
      return allConfAtts;
    }
    set;
  }

  public PageReference loadAttachments() {
    
    List<CombinedAttachment> confAtts2 = new List<CombinedAttachment>();
    Map<Id, String> confMembershipMap2 = new Map<Id, String>();
    Map<Id, Id> contDocToVerMap2 = new Map<Id, Id>();
    
    List<Confidential_Information__c> ciRecords = [
      SELECT Id, Name, (
        SELECT Title, Id, RecordType, CreatedDate, ContentSize, FileExtension, ContentUrl, FileType, ParentId, CreatedBy.Name 
        FROM CombinedAttachments
      )
      FROM Confidential_Information__c
      WHERE Membership__c = :currentRecord.Id
    ];
    
    Set<Id> contDocIds = new Set<Id>();
    
    for (Confidential_Information__c ciRecord : ciRecords) {
      if (ciRecord.CombinedAttachments != null && ciRecord.CombinedAttachments.size() > 0) {
        confAtts2.addAll(ciRecord.CombinedAttachments);
        for (CombinedAttachment ca : ciRecord.CombinedAttachments) {
          if (ca.RecordType == 'File') {
            contDocIds.add(ca.Id);
          }
        }
      }
      confMembershipMap2.put(ciRecord.Id, ciRecord.Name);
    }
    
    for (ContentDocument contDoc : [SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Id IN :contDocIds]) {
      contDocToVerMap2.put(contDoc.Id, contDoc.LatestPublishedVersionId);
    }
    
    confAtts2.sort();
    for (CombinedAttachment ca : confAtts2) {
      CombinedAttachmentWrapper caw = new CombinedAttachmentWrapper(ca);
      caw.contDocId = (contDocToVerMap2.containsKey(ca.Id)) ? contDocToVerMap2.get(ca.Id) : null;
      caw.membName = (confMembershipMap2.containsKey(ca.ParentId)) ? confMembershipMap2.get(ca.ParentId) : '';
      allConfAtts.add(caw);
    }
    return null;
  }
  
}