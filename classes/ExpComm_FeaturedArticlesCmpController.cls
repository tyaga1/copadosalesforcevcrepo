/**
* @description A page controller for featured article visualforce component
* Its main usage is to get current user's language setting.
*
* @author Ryan (Weijie) Hu, UC Innovation
*
* @date 02/14/2017
*/
public with sharing class ExpComm_FeaturedArticlesCmpController {

    public String userLanguage {get {
            Id currentUserId = UserInfo.getUserId();
            User currentUser = [SELECT Id, LanguageLocaleKey FROM USER WHERE Id =:currentUserId];
            return currentUser.LanguageLocaleKey;
        }
    }
    
    public ExpComm_FeaturedArticlesCmpController() {
        
    }
}