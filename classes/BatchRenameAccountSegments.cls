/******************************************************************************
 * Appirio, Inc
 * Name: BatchRenameAccountSegments.cls
 * Description: 
 *
 * Created Date: Aug 19th, 2015.
 * Created By: Arpita Bose (Appirio)
 *
 * Date Modified                Modified By                  Description of the update
 * Nov 9th, 2015                Paul Kissick                 Case 01234035: Adding FailureNotificationUtility
 * Aug 3rd, 2016                Paul Kissick                 CRM2:W-005332: Adding optimised code
 ******************************************************************************/
global class BatchRenameAccountSegments implements Database.Batchable<sObject>, Database.Stateful {
  
  global Set<String> failedAccountSegmentsErrors;
  global Set<Id> hierarchyIdsToUpdate;
  global List<Account_Segment__c> accSegsToUpdate;
  
  //========================================================================================
  // Start
  //========================================================================================
  global Database.QueryLocator start(Database.BatchableContext BC){
    hierarchyIdsToUpdate = new set<Id>();
    failedAccountSegmentsErrors = new Set<String>();
    //get all Account_Segment__c records
    String qryString = 'SELECT Id, Account__c, Segment__r.Processed_Renaming__c, Segment__r.Old_Value__c, Segment__r.Apply_Name_Change_to_Account_Segments__c,' +
                       ' Segment__r.Unique_Key__c, Segment__r.Type__c, Segment__r.Value__c, Segment__c, Type__c, Value__c, Account__r.Name   '+
                       ' FROM Account_Segment__c '+
                       ' WHERE Segment__r.Apply_Name_Change_to_Account_Segments__c = true ' +
                       ' AND Segment__r.Old_Value__c != null';
    return Database.getQueryLocator(qryString);
  }
  
  //========================================================================================
  // Execute
  //========================================================================================
  global void execute(Database.BatchableContext BC, List<sObject> scope) {
    accSegsToUpdate = new List<Account_Segment__c>();  
    system.debug('===scope==>>>' +scope);
    
    //store in a map with keyset pairing Segment__c.Unique_Key__c
    for (Account_Segment__c accSeg : (List<Account_Segment__c>) scope) {
      if (accSeg.Value__c == accSeg.Segment__r.Old_Value__c) {
	      accSeg.Value__c = accSeg.Segment__r.Value__c;
	      accSeg.Name = AccountSegmentationUtility.buildSegmentName(accSeg.Account__r.Name, accSeg.Segment__r.Value__c);
	      accSegsToUpdate.add(accSeg);
	      hierarchyIdsToUpdate.add(accSeg.Segment__c);
      }
    }
     
    // update Account_Segment__c record
    system.debug('=====accSegsToUpdate>>>>'+accSegsToUpdate);
    List<Database.SaveResult> srList = Database.update(accSegsToUpdate);
    for (Integer i = 0; i < srList.size(); i++) {
      if (!srList.get(i).isSuccess()) {
        // DML operation failed
        Database.Error error = srList.get(i).getErrors().get(0);
        String failedDML = error.getMessage();
        String errorStr = 'Account: ' + accSegsToUpdate.get(i).Account__c + '\nType: ' + accSegsToUpdate.get(i).Type__c;
        errorStr += '\nValue: ' + accSegsToUpdate.get(i).Value__c + '\nError: ' + failedDML;
        
        failedAccountSegmentsErrors.add(errorStr);
      } 
    }
 
  }
  
  //========================================================================================
  // Finish
  //========================================================================================
  global void finish(Database.BatchableContext BC) {
    List<Hierarchy__c> hierarchyUpdate = new List<Hierarchy__c>(); 
    
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchRenameAccountSegments', false);
    
    String emailBody = '';
    
    if (failedAccountSegmentsErrors != null && !failedAccountSegmentsErrors.isEmpty()) {
      bh.batchHasErrors = true;
      emailBody += '\n*** Account Segment record update failed:\n';  
      for (String currentRow : failedAccountSegmentsErrors) {
        emailBody += currentRow+'\n\n';
      }
      emailBody += '\n\n';
    }
        
    bh.emailBody += emailBody;
    
    // Update the Hierarchy records for which the Account Segments have been updated
    if (!hierarchyIdsToUpdate.isEmpty() ) {
      for (Hierarchy__c hrchy : [SELECT Id, Old_Value__c, Processed_Renaming__c
                                 FROM Hierarchy__c
                                 WHERE Id IN :hierarchyIdsToUpdate]) {
        hrchy.Old_Value__c = null;
        hrchy.Processed_Renaming__c = true;
        hierarchyUpdate.add(hrchy);
      }
      update hierarchyUpdate;
    }
    
    bh.sendEmail();
  }

}