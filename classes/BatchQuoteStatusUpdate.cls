/*======================================================================================
 * Experian Plc.
 * Name: BatchQuoteStatusUpdate
 * Description: Batch Job to query all Opty closed today and update status for all quotes realted to it.
 * Created Date: Oct 15th 2014
 * Created By: Richard Joseph
 * 
 * Date Modified                Modified By                  Description of the update
 * Dec 3rd, 2015                Paul Kissick                 Case 01266075: Refector and use of batchhelper
 =======================================================================================*/

global class BatchQuoteStatusUpdate implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
  
  global String query;
  global Map<id,Opportunity> updateOpportunityMap;
  
  //==============================================================
  // start method
  //==============================================================
  global Database.QueryLocator start(Database.BatchableContext BC) {
    if (query == null) {
      query = 'SELECT Id, Quote_Id__c, Primary__c, Opportunity__c, Opportunity__r.StageName, Name, Status_Change_Error_Message__c FROM Quote__c WHERE Opportunity__r.SyncCPQ__c = true';
      updateOpportunityMap = new Map<id,Opportunity>([
        SELECT Id, SyncCPQ__c
        FROM Opportunity
        WHERE SyncCPQ__c = true
        AND isclosed = true
      ]);
      system.debug('\n[BatchQuoteStatusUpdate - Opportunity Toupdated total]:'+ updateOpportunityMap.size() ); 
    }
    return Database.getQueryLocator(query);
  }
  
  //==============================================================
  // execute method
  //==============================================================
  global void execute(Database.BatchableContext BC, List<Quote__c> quoteProcessList) {
    system.debug('\n[BatchQuoteStatusUpdate : execute] : Scope size: '+quoteProcessList.size());
    String serviceResponseMessage = null;
    for (Quote__c quoteRec:quoteProcessList) {
      try {
        serviceResponseMessage = SFDCToCPQChangeStatusServiceClass.callCPQUserAdminSerivce(quoteRec);
        if(serviceResponseMessage != null) {
          quoteRec.Status_Change_Error_Message__c = serviceResponseMessage ; 
          //update quoteRecord;
          if (updateOpportunityMap!= null && updateOpportunityMap.containsKey(quoteRec.Opportunity__c)) {
            updateOpportunityMap.remove(quoteRec.Opportunity__c);    
          }
        }
      } 
      catch (Exception e) {
        if (updateOpportunityMap!= null && updateOpportunityMap.containsKey(quoteRec.Opportunity__c)) {
          updateOpportunityMap.remove(quoteRec.Opportunity__c);
        }
        ApexLogHandler.createLogAndSave('BatchQuoteStatusUpdate','execute', e.getStackTraceString(), e);
      }
    }
    update quoteProcessList;
  }

  //==============================================================
  // finish method
  //==============================================================
  global void finish(Database.BatchableContext BC) {
    
    // Update the Sync Flag in Opportunity
    if (updateOpportunityMap != null) {
      for (Opportunity optyRec: updateOpportunityMap.Values()) {
        optyRec.SyncCPQ__c =false;
      }
      try{
        update updateOpportunityMap.values();
      }
      catch (Exception ex) {
        system.debug('BatchQuoteStatusUpdate  Exception :'+ ex.getLineNumber() + ' Stack:' +ex.getStackTraceString() );
      }
      system.debug('\n[BatchQuoteStatusUpdate - Opportunity Updated total]:'+ updateOpportunityMap.size() );  
    }
    
    //Batch completion and verification process
   
    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'BatchQuoteStatusUpdate', false);

    String emailBody = '';
    if (updateOpportunityMap != null && updateOpportunityMap.size() > 0) {
      bh.batchHasErrors = true;    
      emailBody += '\n Opportunities which are not updated are :' + updateOpportunityMap.keySet();
      bh.emailBody += emailBody;
    }
    
    bh.sendEmail();
    
  }  
  /* Execution 
      BatchQuoteStatusUpdate objTest = new BatchQuoteStatusUpdate ();
      database.executeBatch(objTest ,1); 
  
  */
}