/**=====================================================================
 * Appirio, Inc
 * Name: AddActiveChatterGame
 * Description: The following batch class is designed to be scheduled to run once every Sunday.
                    This class will get all new Chatter user Stats from an analytic snapshot and assign a Chatter game to them
 * Created Date: 5/5/2015
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 * 02/25/2016                   Diego Olarte                 Case#01838499: Set values for 7 priorities instead of 4
 * 03/15/2016                   Diego Olarte                 Case#01838499: Set queue system
 * Apr 7th, 2016                Paul Kissick                 Fixed failing tests - Replaced finish method content
 =====================================================================*/
global class AddActiveChatterGame implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {
  
    String query = 'Select Id, User__c, Chatter_Game__c, X1st_Priority__c, X2nd_Priority__c, X3rd_Priority__c, X4th_Priority__c, X5th_Priority__c, X6th_Priority__c, X7th_Priority__c, CreatedDate FROM User_Chatter_stats__c WHERE CreatedDate = TODAY AND (Chatter_Game__c = null OR Chatter_Game__c = \'\')';
   
    return Database.getQueryLocator (query);
  }

  global void execute (Database.BatchableContext bc, List<User_Chatter_stats__c> chatterUserStatList) {
    
    // PK: Diego, I commented this line out as it was just querying the same data twice. Given you have queried it in the 'start' method, this isn't needed.
    // List<User_Chatter_stats__c> chatterUserStatList = [Select Id, User__c, Chatter_Game__c, User_Region__c, User_Function__c, User_Business_Line__c, X4th_Priority__c FROM User_Chatter_stats__c WHERE  Id in :scope AND Chatter_Game__c = ''];
        
    //Set off all User Chatter Stat for setting priorities
    
    Set<String> p1 = new Set<String>();
    Set<String> p2 = new Set<String>();
    Set<String> p3 = new Set<String>();
    Set<String> p4 = new Set<String>();
    Set<String> p5 = new Set<String>();
    Set<String> p6 = new Set<String>();
    Set<String> p7 = new Set<String>();
    
    for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList) {
      if (newUserChatterStat.X1st_Priority__c != null) {
        p1.add(newUserChatterStat.X1st_Priority__c);
      }
      
      if (newUserChatterStat.X2nd_Priority__c != null) {
        p2.add(newUserChatterStat.X2nd_Priority__c);
      }
        
      if (newUserChatterStat.X3rd_Priority__c != null) {
        p3.add(newUserChatterStat.X3rd_Priority__c);
      }
        
      if (newUserChatterStat.X4th_Priority__c != null) {
        p4.add(newUserChatterStat.X4th_Priority__c);
      }
      if (newUserChatterStat.X4th_Priority__c != null) {
        p5.add(newUserChatterStat.X5th_Priority__c);
      }
      if (newUserChatterStat.X4th_Priority__c != null) {
        p6.add(newUserChatterStat.X6th_Priority__c);
      }
      if (newUserChatterStat.X4th_Priority__c != null) {
        p7.add(newUserChatterStat.X7th_Priority__c);
      }
    }
    
    //List of all Active Chatter games via priority
    
    List<Chatter_Game__c> cgp1 = [Select Id, Active__c, Unique_Active_Game_name__c FROM Chatter_Game__c WHERE Active__c = true AND Unique_Active_Game_name__c IN :p1];
    List<Chatter_Game__c> cgp2 = [Select Id, Active__c, Unique_Active_Game_name__c FROM Chatter_Game__c WHERE Active__c = true AND Unique_Active_Game_name__c IN :p2];
    List<Chatter_Game__c> cgp3 = [Select Id, Active__c, Unique_Active_Game_name__c FROM Chatter_Game__c WHERE Active__c = true AND Unique_Active_Game_name__c IN :p3];
    List<Chatter_Game__c> cgp4 = [Select Id, Active__c, Unique_Active_Game_name__c FROM Chatter_Game__c WHERE Active__c = true AND Unique_Active_Game_name__c IN :p4];
    List<Chatter_Game__c> cgp5 = [Select Id, Active__c, Unique_Active_Game_name__c FROM Chatter_Game__c WHERE Active__c = true AND Unique_Active_Game_name__c IN :p5];
    List<Chatter_Game__c> cgp6 = [Select Id, Active__c, Unique_Active_Game_name__c FROM Chatter_Game__c WHERE Active__c = true AND Unique_Active_Game_name__c IN :p6];
    List<Chatter_Game__c> cgp7 = [Select Id, Active__c, Unique_Active_Game_name__c FROM Chatter_Game__c WHERE Active__c = true AND Unique_Active_Game_name__c IN :p7];
    
    //Map to search games via priority
    
    Map<String, Chatter_Game__c> cgp1ToUse = new Map<String, Chatter_Game__c>();
    Map<String, Chatter_Game__c> cgp2ToUse = new Map<String, Chatter_Game__c>();
    Map<String, Chatter_Game__c> cgp3ToUse = new Map<String, Chatter_Game__c>();
    Map<String, Chatter_Game__c> cgp4ToUse = new Map<String, Chatter_Game__c>();
    Map<String, Chatter_Game__c> cgp5ToUse = new Map<String, Chatter_Game__c>();
    Map<String, Chatter_Game__c> cgp6ToUse = new Map<String, Chatter_Game__c>();
    Map<String, Chatter_Game__c> cgp7ToUse = new Map<String, Chatter_Game__c>();
    
    for (Chatter_Game__c cg : cgp1) {
      cgp1ToUse.put(cg.Unique_Active_Game_name__c, cg);
    }
    for (Chatter_Game__c cg : cgp2) {
      cgp2ToUse.put(cg.Unique_Active_Game_name__c, cg);
    }
    for (Chatter_Game__c cg : cgp3) {
      cgp3ToUse.put(cg.Unique_Active_Game_name__c, cg);
    }
    for (Chatter_Game__c cg : cgp4) {
      cgp4ToUse.put(cg.Unique_Active_Game_name__c, cg);
    }
    for (Chatter_Game__c cg : cgp5) {
      cgp5ToUse.put(cg.Unique_Active_Game_name__c, cg);
    }
    for (Chatter_Game__c cg : cgp6) {
      cgp6ToUse.put(cg.Unique_Active_Game_name__c, cg);
    }
    for (Chatter_Game__c cg : cgp7) {
      cgp7ToUse.put(cg.Unique_Active_Game_name__c, cg);
    }
        
    //List of records to update
    
    List<User_Chatter_stats__c> chatterUserStatListtoUpdate = new List<User_Chatter_stats__c>{};
    
    //Set the Proper game to each Chatter User Stat
    
    if(!cgp1ToUse.isEmpty()) {    
      for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList){
        if (newUserChatterStat.X1st_Priority__c != null) {
          Chatter_Game__c x1priority = cgp1ToUse.get(newUserChatterStat.X1st_Priority__c);
          if (x1priority != null) {
            newUserChatterStat.Chatter_Game__c = x1priority.Id;
            chatterUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }
    }
    else if(!cgp2ToUse.isEmpty()) {
      for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList){
        if (newUserChatterStat.X2nd_Priority__c != null) {
          Chatter_Game__c x2priority = cgp2ToUse.get(newUserChatterStat.X2nd_Priority__c);
          if (x2priority != null) {
            newUserChatterStat.Chatter_Game__c = x2priority.Id;
            chatterUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }
    }
    else if(!cgp3ToUse.isEmpty()) {
      for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList){
        if (newUserChatterStat.X3rd_Priority__c != null) {
          Chatter_Game__c x3priority = cgp3ToUse.get(newUserChatterStat.X3rd_Priority__c);
          if (x3priority != null) {
            newUserChatterStat.Chatter_Game__c = x3priority.Id;
            chatterUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }
    }
    else if(!cgp4ToUse.isEmpty()){
      for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList){
        if (newUserChatterStat.X4th_Priority__c != null) {
          Chatter_Game__c x4priority = cgp4ToUse.get(newUserChatterStat.X4th_Priority__c);
          if (x4priority != null) {
            newUserChatterStat.Chatter_Game__c = x4priority.Id;
            chatterUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }            
    }
    else if(!cgp5ToUse.isEmpty()){
      for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList){
        if (newUserChatterStat.X5th_Priority__c != null) {
          Chatter_Game__c x5priority = cgp5ToUse.get(newUserChatterStat.X5th_Priority__c);
          if (x5priority != null) {
            newUserChatterStat.Chatter_Game__c = x5priority.Id;
            chatterUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }            
    }
    else if(!cgp6ToUse.isEmpty()){
      for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList){
        if (newUserChatterStat.X6th_Priority__c != null) {
          Chatter_Game__c x6priority = cgp6ToUse.get(newUserChatterStat.X6th_Priority__c);
          if (x6priority != null) {
            newUserChatterStat.Chatter_Game__c = x6priority.Id;
            chatterUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }            
    }
    else if(!cgp7ToUse.isEmpty()){
      for (User_Chatter_stats__c newUserChatterStat : chatterUserStatList){
        if (newUserChatterStat.X7th_Priority__c != null) {
          Chatter_Game__c x7priority = cgp7ToUse.get(newUserChatterStat.X7th_Priority__c);
          if (x7priority != null) {
            newUserChatterStat.Chatter_Game__c = x7priority.Id;
            chatterUserStatListtoUpdate.add(newUserChatterStat);
          }
        }
      }            
    }

    update chatterUserStatListtoUpdate;            
  }  

    //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {

    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'AddActiveChatterGame', true);
    
    if (!Test.isRunningTest()) {
      system.scheduleBatch(new AddFirstChatterGameUserStat(), 'UserChatterStat - Assign First User Stat '+String.valueOf(Datetime.now().getTime()), 0, ScopeSizeUtility.getScopeSizeForClass('AddFirstChatterGameUserStat'));
    }
    
    /*
    // PK Removed this and replaced with above as it's much easier to manage.
    
    BatchSchedulingIDstorage__c BSIDS = BatchSchedulingIDstorage__c.getOrgDefaults();
    
    DateTime n = datetime.now().addMinutes(2);
    String cron = '';

        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();

        BSIDS.BSIDS04__c = System.schedule('UserChatterStat - Assign First User Stat', cron, new ScheduleAddFirstChatterGameUserStat());

        update BSIDS;
  
      AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                               TotalJobItems, CreatedBy.Email
                        FROM AsyncApexJob WHERE Id =: BC.getJobId()];
      
      System.debug('\n[addActiveChatterGame: finish]: [The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.]]');
      */
  }
    
}