/**=====================================================================
 * Experian
 * Name: TaskFollowUpEventOnExtension
 * Description: Case 07848945
 * The autorun method of the FollowOnTaskExtension class is called from this VF page, which itself is called from a Custom button labelled 'Create Follow-Up Event' on the Task Standard page.
 * The purpose of the page is to give the user the option to complete the source task when creating a Follow-Up Event
 *
 * Created Date: Aug. 18th, 2017
 * Created By: Malcolm Russell
 *
 * Date Modified         Modified By           Description of the update
 *
  =====================================================================*/
public class TaskFollowUpEventExtension{   
    @testVisible private String parentID ;
    @testVisible public Task parentTask ;

       
    public TaskFollowUpEventExtension(ApexPages.StandardController stdController) {   
         
      parentID =  stdController.getId();
      this.parentTask =  [Select whoid,whatid,subject from task where id=:parentid];
      
    } 
    
    public PageReference autoRun(){  
             
       String decision = ApexPages.currentPage().getParameters().get('Decision');
      //Clone the Parent Task to get the Campaign__c field value
       if(decision == 'yes'){
           List<Task> lstTask = [SELECT Id, Status FROM Task WHERE Id = :parentID ];
        
          if (lstTask != null && lstTask.size() > 0) {
            for (Task t : lstTask) {
              t.Status = Constants.TASK_STATUS_COMPLETED;
            }
            update lstTask;
          }
     }
      
      PageReference pageRef = new PageReference('/00U/e?retURL=%2F' + parentID + '&who_id=' + parentTask.whoId + ((parentTask.whatId != null ) ? ('&what_id='+parentTask.whatId): '') +'&evt5=' + parentTask.Subject);
  
      
            
      return pageRef;     
    }

}