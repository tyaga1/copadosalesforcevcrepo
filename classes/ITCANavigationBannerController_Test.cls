/**=====================================================================
 * Experian
 * Name: ITCANavigationBannerController_Test
 * Description: Test Class for ITCANavigationBannerController
 *
 * Created Date: Sept. 1st 2017
 * Created By: James Wills for ITCA Project
 * Date Modified       Modified By           Description of the update
 * 6th September 2017  James Wills           Update.
=====================================================================*/
@isTest
private class ITCANavigationBannerController_Test{


  private static testMethod void test_nbc1(){

   PageReference pageRef = Page.ITCA_Home_Page;        
   Test.setCurrentPage(pageRef);
   
   //PageReference pageRef2 = Page.ITCA_My_Journey_Map;        
   //Test.setCurrentPage(pageRef2);
   
   ITCANavigationBannerController nbc1 = new ITCANavigationBannerController();
      
   //ApexPages.StandardController stdController = new ApexPages.StandardController(nbc1);
   
        
   Test.startTest();
     nbc1.level1_SelectedCareerArea='Select Career Area';   
     String l1 = nbc1.level1_selectedCareerArea;
     nbc1.level2_SelectedSkillSet='Select Skill Set';
     String l2 = nbc1.level2_selectedSkillSet;
  
     nbc1.bannerInfoLocal = new ITCABannerInfoClass();
      
     Map<ID,User> tm = nbc1.team_Map;  
     Boolean im = nbc1.isManager;
     
     Boolean pex = nbc1.profileExists;
     
     //ITCA:W-008961
     List<ITCANavigationBannerController.itcaTeamUser> itm = nbc1.teamList;
  
     //Called for MySkillsSummary
     nbc1.mySkillsSummaryAction();
   
     nbc1.isSummaryAction();
     
     //Called for CompareSkillSetsl
     nbc1.myJourneyMapAction();
     
     //Called for CompareSkillSets
     nbc1.compareSkillSetAction();
          
     //Called for CompareSkillSets
     nbc1.compareCareerAreaAction();
    
     //Method to find top 5 skill sets for the skill
     nbc1.matchSkillSetsAction();
          
     //Method to compare Skills under selected skill set and current user skills
     nbc1.compareCareerArea();
      
     //Method to compare Skills under Selected Career Area and current user skills
     nbc1.compareSkillSet();
     
   Test.stopTest();
  
  }
  


  @testSetup
  private static void createData(){
    
    buildProfileSkills();
    buildExperianSkillSet();
    buildSkillSetToSKill();
    createProfile();
  }

  private static void buildProfileSkills(){
  
    ProfileSkill ps1 = new ProfileSkill(Name='Apex', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=false);
                                        
    ProfileSkill ps2 = new ProfileSkill(Name='Programming & Software Development', 
                                        Level5__c='Maintains Code.', 
                                        Level6__c='Writes test classes.', 
                                        Level7__c='Designs Code.',
                                        Sfia_Skill__c=true);                                  

    List<ProfileSkill> ps_List = new List<ProfileSkill>{ps1,ps2};
    insert ps_List;
    
  }

  private static void buildExperianSkillSet(){
    Experian_Skill_Set__c exp1 = new Experian_Skill_Set__c(Name='Software Development', Career_Area__c='Applications');
    insert exp1;
  
  }

  private static void buildSkillSetToSKill(){
    List<ProfileSkill> psList = [SELECT id, Name FROM ProfileSkill];
    List<Experian_Skill_Set__c> essList = [SELECT id FROM Experian_Skill_Set__c];
   
    Skill_Set_to_Skill__c sts1 = new Skill_Set_to_Skill__c(Name='Software Development - Application Support', 
                                                          Skill__c=psList[0].id, 
                                                          Experian_Role__c='Professional',
                                                          Experian_Skill_Set__c=essList[0].id,
                                                          Level__c='Level4');
    insert sts1;
  
 }

  private static void createProfile(){
  
    //Profile p = [SELECT Id FROM Profile WHERE Name=: Constants.PROFILE_SYS_ADMIN ];
    //User testUser1 = Test_Utils.createUser(p, 'test1234@experian.com', 'test1');
    //insert testUser1;
    
    Career_Architecture_User_Profile__c caup1 = new Career_Architecture_User_Profile__c(//Career_Area__c=,
                                                                                        //Date_Discussed_with_Employee__c=,
                                                                                        //Discussed_with_Employee__c=,
                                                                                        Employee__c= userInfo.getUserId(),
                                                                                        //Manager_Comments__c=,
                                                                                        //Role__c=,
                                                                                        State__c='Current',
                                                                                        Status__c='Submitted to Manager');
    insert caup1;
     
    Career_Architecture_Skills_Plan__c casp1 = new Career_Architecture_Skills_Plan__c(Career_Architecture_User_Profile__c=caup1.id,
                                                                                      Experian_Skill_Set__c=[SELECT id FROM Experian_Skill_Set__c LIMIT 1].id,
                                                                                      Level__c='Level6',
                                                                                      Skill__c=[SELECT id FROM ProfileSkill Where Name = 'Apex' LIMIT 1].id  );
    
    insert casp1;
  }



}