/**=====================================================================
 * Experian
 * Name: AccountEDQDependencyCleaner02
 * Description: The following batch class is designed to be scheduled to run every day.
                    This class will get all Accounts with an EDQ Dependency and clean them if no longer related to EDQ
 * Created Date: 6/17/2015
 * Created By: Diego Olarte (Experian)
 *
 * Date Modified                Modified By                  Description of the update
 * Nov 9th, 2015                Paul Kissick                 Case 01266075: Optimised to reduce load on queries
 * Jun 6th, 2016                Paul Kissick                 Case 02013456: Fixed too many subquery count
 =====================================================================*/
global class AccountEDQDependencyCleaner02 implements Database.Batchable<sObject> {

  global Database.Querylocator start ( Database.BatchableContext bc ) {

    return Database.getQueryLocator ([
      SELECT Id, EDQ_Dependency_Exists__c,
        (
         SELECT Id, Order_Owner_BU__c, Product_Business_Line__c, AccountId, SaaS__c
         FROM Assets
         WHERE Status__c IN ('Live','Scheduled')
         AND (Order_Owner_BU__c LIKE '%Data Quality' OR Product_Business_Line__c LIKE '%Data Quality' OR SaaS__c = true)
         LIMIT 1
        ),(
         SELECT Id, Owner_BU_on_Order_Create_Date__c, Account__c, Saas_Order_Line_Count__c FROM Orders__r
         WHERE (Owner_BU_on_Order_Create_Date__c LIKE '%Data Quality%' OR Saas_Order_Line_Count__c > 0)
         LIMIT 1
        )
      FROM Account
      WHERE EDQ_Dependency_Exists__c = true
      AND SaaS__c = false
    ]);

  }

  global void execute (Database.BatchableContext bc, List<Account> scope) {

    List<Account> accountsList = new List<Account>();

    Integer assetCount;
    Integer orderCount;
    Boolean hasEDQrecords = false;

    for (Account acc : scope) {

      assetCount = 0;
      orderCount = 0;

      assetCount = (acc.Assets != null) ? acc.Assets.size() : 0;
      orderCount = (acc.Orders__r != null) ? acc.Orders__r.size() : 0;

      hasEDQrecords = ((assetCount+orderCount) > 0);

      if (!hasEDQrecords) {
        accountsList.add(
          new Account(
            Id = acc.Id,
            EDQ_Dependency_Exists__c = false
          )
        );
      }
    }

    if (!accountsList.isEmpty()) {
      try {
        update accountsList;
      }
      catch (DMLException ex) {
        apexLogHandler.createLogAndSave('AccountEDQDependencyCleaner02','execute', ex.getStackTraceString(), ex);
      }
    }
  }

  //To process things after finishing batch
  global void finish (Database.BatchableContext bc) {

    BatchHelper bh = new BatchHelper();
    bh.checkBatch(bc.getJobId(), 'AccountEDQDependencyCleaner02', true);

  }

}