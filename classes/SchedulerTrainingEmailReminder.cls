/*
  Author:       Sanket Vaidya
  Date:         March 23rd 2017
  Description:  This class is used to schedule BatchTrainingEmailReminderForOnboarding.cls which operates on records of object Training__c. 
*/

public class SchedulerTrainingEmailReminder implements Schedulable
{
    public void execute(SchedulableContext sc)
    {
        Database.executeBatch(new BatchTrainingEmailReminderForOnboarding());
    }
}