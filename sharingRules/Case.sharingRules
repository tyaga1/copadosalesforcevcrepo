<?xml version="1.0" encoding="UTF-8"?>
<SharingRules xmlns="http://soap.sforce.com/2006/04/metadata">
    <sharingCriteriaRules>
        <fullName>Access_to_all_HR_Cases</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Access to all HR cases for Recognition Payroll Group</description>
        <label>Access to all HR Cases</label>
        <sharedTo>
            <group>Recognition_Payroll_Group</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Platform Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Reason</field>
            <operation>equals</operation>
            <value>HR - Recognition</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>AdTruth_Support_Case_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Gives all AdTruth Support the ability to read and edit cases with the AdTruth Support record type</description>
        <label>AdTruth Support Case Sharing</label>
        <sharedTo>
            <group>MS_AdTruth_Support_Team</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>AdTruth Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Allow_EMEA_MS_France_Users_to_view_EMS_Request_Cases</fullName>
        <accessLevel>Read</accessLevel>
        <description>Allow EMEA MS France Users to view EMS Request Cases.</description>
        <label>Allow EMEA MS France Users to view EMS Request Cases</label>
        <sharedTo>
            <roleAndSubordinatesInternal>EMEA_MS_France</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Allow_EMEA_MS_Germany_Users_to_view_EMS_Request_Cases</fullName>
        <accessLevel>Read</accessLevel>
        <description>Allow EMEA MS Germany Users to view EMS Request Cases.</description>
        <label>Allow EMEA MS Germany Users to view EMS Request Cases</label>
        <sharedTo>
            <roleAndSubordinatesInternal>EMEA_MS_Germany</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Allow_EMEA_MS_Spain_Users_to_view_EMS_Request_Cases</fullName>
        <accessLevel>Read</accessLevel>
        <description>Allow EMEA MS Spain Users to view EMS Request Cases.</description>
        <label>Allow EMEA MS Spain Users to view EMS Request Cases</label>
        <sharedTo>
            <roleAndSubordinatesInternal>EMEA_MS_Spain</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Automotive_Autocheck_Service_Data</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Share cases set as the Automotve RT with an origin of AutoCheck Service Data with the Account Managers as they need to be able to see / edit these cases</description>
        <label>Automotive - Autocheck Service Data</label>
        <sharedTo>
            <group>Auto_Account_Managers</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Automotive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Origin</field>
            <operation>equals</operation>
            <value>Email - AutoCheck Service Data</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Automotive_Cases_Sharing_Rule</fullName>
        <accessLevel>Edit</accessLevel>
        <description>This rules gives Read/Write access to Automotive cases to a large number of roles in the Auto hierarchy</description>
        <label>Automotive Cases Sharing Rule</label>
        <sharedTo>
            <roleAndSubordinatesInternal>NA_CS_DA_CS_Automotive_Manager</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Automotive</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>CCM_NA_Case_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Share all CCM NA cases with the  CCM NA Support Roles, added together in public group</description>
        <label>CCM NA Case Sharing</label>
        <sharedTo>
            <group>CCM_NA_Case_Team</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>NA CCM Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>CPQ_CCB_Sharing</fullName>
        <accessLevel>Read</accessLevel>
        <description>Allows all members of the CPQ CCB to see those cases</description>
        <label>CPQ CCB Sharing</label>
        <sharedTo>
            <group>CPQ_CCB</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Reason</field>
            <operation>equals</operation>
            <value>CPQ / Quote</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>CSDA_All_RTs_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <description>All CSDA records shared based on Record Types</description>
        <label>CSDA All RTs Sharing</label>
        <sharedTo>
            <roleAndSubordinatesInternal>NA_CS_DA</roleAndSubordinatesInternal>
        </sharedTo>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>CSDA BIS Support,CSDA CIS BA Request,CSDA CIS Support,CSDA Contract Request</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>CSDA_DQ_Cases</fullName>
        <accessLevel>Read</accessLevel>
        <description>Sharing rule to allow other members of CSDA to see cases submitted to the DQ team</description>
        <label>CSDA DQ Cases</label>
        <sharedTo>
            <role>NA_CS_DA_Sales_Support</role>
        </sharedTo>
        <criteriaItems>
            <field>Secondary_Case_Reason__c</field>
            <operation>equals</operation>
            <value>Transfer contracts / confidential info,Amend opp team / ownership,Contract name amend,Assignment rules,Data quality,Assignment team modification / new hire,Data updates &amp; de-duplication,Delete duplicate contracts / confidential info</value>
        </criteriaItems>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>EDQ_BusinessOPS_cases</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Shares EDQ Business Operations cases with users in role UK&amp;I MS EDQ Customer Operations</description>
        <label>EDQ BusinessOPS cases</label>
        <sharedTo>
            <role>UK_I_MS_EDQ_Customer_Operations</role>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Business Operations</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>EDQ_CCB_Sharing_Rule</fullName>
        <accessLevel>Edit</accessLevel>
        <label>EDQ CCB Sharing Rule</label>
        <sharedTo>
            <group>EDQ_CCB_Group</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Requester_BU__c</field>
            <operation>contains</operation>
            <value>NA MS Data Quality,NA CS Data Quality</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Fraud_and_Identity_CEM_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <label>Fraud and Identity CEM Sharing</label>
        <sharedTo>
            <group>DA_Fraud_and_Identity_CEM</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Fraud and Identity Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Fraud_and_Identity_Risk_Analyst_Case_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Gives Fraud and Identity Risk Analyst support the ability to edit and transfer ownership of all Fraud and Identity cases</description>
        <label>Fraud and Identity Risk Analyst Case Sharing</label>
        <sharedTo>
            <group>DA_Fraud_and_Identity_Risk_Analyst_Team</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Fraud and Identity Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Fraud_and_Identity_Support_Case_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Gives Fraud and Identity Support the ability to edit and transfer ownership of all Fraud and Identity cases</description>
        <label>Fraud and Identity Support Case Sharing</label>
        <sharedTo>
            <group>DA_Fraud_and_Identity_Support</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Fraud and Identity Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>GPD_EDQ_Case_Sharing</fullName>
        <accessLevel>Read</accessLevel>
        <label>GPD EDQ Case Sharing</label>
        <sharedTo>
            <group>EDQ_Support_Group</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ GPD Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Global_Security_Office_Request_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Enables read write access to all Global Security Office team members on cases with this record type - case 02359088</description>
        <label>Global Security Office Request Sharing</label>
        <sharedTo>
            <group>Global_Security_Office</group>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Global Security Office Requests</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Serasa_Case_Sharing_with_Sales</fullName>
        <accessLevel>Edit</accessLevel>
        <description>sharing rule for cases for the below so sales people can edit certain cases: 

When the case is not closed and the secondary reason matches the below values and one of the serasa case RT&apos;s 

Forward sales executive 
Sales executive 
Sales executive - SME 
Encaminhar executivo de vendas</description>
        <label>Serasa Case Sharing with Sales</label>
        <sharedTo>
            <roleAndSubordinatesInternal>LATAM_Finance_Marketing_Sales_Operations</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>Serasa_Secondary_Case_Reason__c</field>
            <operation>equals</operation>
            <value>Forward sales executive,Sales executive</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Serasa_Cases_Edit_Access</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Serasa Cases edit access for the roles  LATAM Customer Service director</description>
        <label>Serasa Cases Edit Access</label>
        <sharedTo>
            <roleAndSubordinatesInternal>LATAM_Customer_Service_Director</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Serasa Cancellation,Serasa Support Request,Serasa Support Request Internal</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Serasa_Cases_Read_Access</fullName>
        <accessLevel>Read</accessLevel>
        <label>Serasa Cases Read Access</label>
        <sharedTo>
            <roleAndSubordinatesInternal>LATAM_Finance_Marketing_Sales_Operations</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Serasa Cancellation,Serasa Support Request,Serasa Support Request Internal</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Serasa_Cases_Third_level_Access</fullName>
        <accessLevel>Edit</accessLevel>
        <label>Serasa Cases Third level Access</label>
        <sharedTo>
            <roleAndSubordinatesInternal>LATAM_Third_Level_Support</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Serasa Cancellation,Serasa Support Request,Serasa Support Request Internal</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Service_Central_to_All</fullName>
        <accessLevel>Read</accessLevel>
        <label>Service Central to All</label>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingCriteriaRules>
        <fullName>Share_Client_Support_cases_with_All_Users</fullName>
        <accessLevel>Read</accessLevel>
        <description>Share all client support cases (EDQ RT and CCM NA RT) with all Global SFDC users - READ ONLY</description>
        <label>Share Client Support cases with All Users</label>
        <sharedTo>
            <roleAndSubordinatesInternal>Global_Admin</roleAndSubordinatesInternal>
        </sharedTo>
        <criteriaItems>
            <field>RecordTypeId</field>
            <operation>equals</operation>
            <value>NA CCM Support,EDQ Technical Support</value>
        </criteriaItems>
    </sharingCriteriaRules>
    <sharingOwnerRules>
        <fullName>CSDA_Data_Acquisition_1</fullName>
        <accessLevel>Edit</accessLevel>
        <label>CSDA Data Acquisition 1</label>
        <sharedTo>
            <group>CSDA_Data_Acquisition</group>
        </sharedTo>
        <sharedFrom>
            <group>CSDA_Data_Acquisition</group>
        </sharedFrom>
    </sharingOwnerRules>
    <sharingOwnerRules>
        <fullName>CSDA_Support_Team</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Provide read write access to all members of the team when case owned by members of the team</description>
        <label>CSDA  Support Team</label>
        <sharedTo>
            <roleAndSubordinatesInternal>NA_CS_DA</roleAndSubordinatesInternal>
        </sharedTo>
        <sharedFrom>
            <roleAndSubordinatesInternal>NA_CS_DA</roleAndSubordinatesInternal>
        </sharedFrom>
    </sharingOwnerRules>
    <sharingOwnerRules>
        <fullName>Data_Quality</fullName>
        <accessLevel>Edit</accessLevel>
        <label>Data Quality</label>
        <sharedTo>
            <group>Data_Quality</group>
        </sharedTo>
        <sharedFrom>
            <group>Data_Quality</group>
        </sharedFrom>
    </sharingOwnerRules>
    <sharingOwnerRules>
        <fullName>EDQ_APAC_Sharing_Rule</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Share records within the APAC support team so they can all edit and update any cases</description>
        <label>EDQ APAC Sharing Rule</label>
        <sharedTo>
            <roleAndSubordinatesInternal>APAC_MS_EDQ_Technical_Team_Lead</roleAndSubordinatesInternal>
        </sharedTo>
        <sharedFrom>
            <roleAndSubordinatesInternal>APAC_MS_EDQ_Technical_Team_Lead</roleAndSubordinatesInternal>
        </sharedFrom>
    </sharingOwnerRules>
    <sharingOwnerRules>
        <fullName>EDQ_NA_Support_Team</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Share cases owned by the team with the team - read write</description>
        <label>EDQ NA Support Team</label>
        <sharedTo>
            <roleAndSubordinatesInternal>NA_MS_EDQ_Customer_Support_Manager</roleAndSubordinatesInternal>
        </sharedTo>
        <sharedFrom>
            <roleAndSubordinatesInternal>NA_MS_EDQ_Customer_Support_Manager</roleAndSubordinatesInternal>
        </sharedFrom>
    </sharingOwnerRules>
    <sharingOwnerRules>
        <fullName>EDQ_UK_Support_Team</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Provide read write access to all members of the team when case owned by members of the team</description>
        <label>EDQ UK Support Team</label>
        <sharedTo>
            <roleAndSubordinatesInternal>UK_I_MS_EDQ_Technical_Support_Manager</roleAndSubordinatesInternal>
        </sharedTo>
        <sharedFrom>
            <roleAndSubordinatesInternal>UK_I_MS_EDQ_Technical_Support_Manager</roleAndSubordinatesInternal>
        </sharedFrom>
    </sharingOwnerRules>
    <sharingOwnerRules>
        <fullName>ESDEL_Case_Sharing</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Spain Delivery Workstream</description>
        <label>ESDEL Case Sharing</label>
        <sharedTo>
            <roleAndSubordinatesInternal>EMEA_MS_Spain</roleAndSubordinatesInternal>
        </sharedTo>
        <sharedFrom>
            <roleAndSubordinatesInternal>EMEA_MS_Spain</roleAndSubordinatesInternal>
        </sharedFrom>
    </sharingOwnerRules>
    <sharingOwnerRules>
        <fullName>Share_EDQ_GPD_Cases_with_team</fullName>
        <accessLevel>Edit</accessLevel>
        <description>Share all cases owned by GPD with rest of GPD team</description>
        <label>Share EDQ GPD Cases with team</label>
        <sharedTo>
            <group>GPD_Group</group>
        </sharedTo>
        <sharedFrom>
            <group>GPD_Group</group>
        </sharedFrom>
    </sharingOwnerRules>
</SharingRules>
