<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ESDEL_Spain_Ticket_Owner_Email2Case</fullName>
        <description>Update Owner on Email 2 Case</description>
        <field>OwnerId</field>
        <lookupValue>ESDEL_Spain_Delivery_Ticket_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Spain Ticket Owner Email2Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ESDEL_Spain_eMail_to_Case_Root</fullName>
        <description>Spain Delivery Workstream - Email to case update email root</description>
        <field>ESDEL_Email_to_Case_Root__c</field>
        <literalValue>MS Spain Delivery</literalValue>
        <name>Spain eMail to Case Root</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ESDEL_Spain_email_to_case_Type</fullName>
        <description>Spain Delivery Workstream - update type to ticket for email to case</description>
        <field>Type</field>
        <literalValue>Spain Delivery Ticket</literalValue>
        <name>Spain email to case Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Message_Reopen_case_on_user_reply</fullName>
        <description>Reopen CCM NA case on email reply</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Email Message.Reopen case on user reply</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Message_Received_Set_True_Date</fullName>
        <field>Last_Inbound_Message__c</field>
        <formula>NOW()</formula>
        <name>New Message Received Set True Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_Message_Received_Set_True_Email</fullName>
        <field>New_Message_Received__c</field>
        <literalValue>1</literalValue>
        <name>New Message Received Set True Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revert_Soft_close_cases</fullName>
        <description>Revert Soft close cases when an email is received</description>
        <field>Status</field>
        <literalValue>In progress</literalValue>
        <name>Revert Soft close cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Email_Received_On_Case</fullName>
        <description>Case #529962 - See when a new email has been received</description>
        <field>Last_Email_Received__c</field>
        <formula>NOW()</formula>
        <name>Set Last Email Received On Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Email_Sent_On_Case</fullName>
        <description>Case #529962 - See when a new email has been received</description>
        <field>Last_Email_Sent__c</field>
        <formula>NOW()</formula>
        <name>Set Last Email Sent On Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Review_Needed</fullName>
        <description>If case is &apos;Closed - Resolved&apos;, and the customer send email(case comment) we need to update the status to &apos;Review Needed&apos;.</description>
        <field>Status</field>
        <literalValue>Review Needed</literalValue>
        <name>Update Case Status to Review Needed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>EMSCaseReviewNeededIfCustomerRespondedAfterClosedResolved</fullName>
        <actions>
            <name>Update_Case_Status_to_Review_Needed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSChangeStatusToReviewNeededIfCustomerRepliesOnOnHoldStatus</fullName>
        <actions>
            <name>Update_Case_Status_to_Review_Needed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Subject</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSNewInboundEmailMessage</fullName>
        <actions>
            <name>New_Message_Received_Set_True_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>New_Message_Received_Set_True_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>On emailmessage creation, evaluates to true if message is incoming</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ESDEL_Spain eMail to Case Root</fullName>
        <actions>
            <name>ESDEL_Spain_Ticket_Owner_Email2Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ESDEL_Spain_eMail_to_Case_Root</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ESDEL_Spain_email_to_case_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.ToAddress</field>
            <operation>equals</operation>
            <value>experianspaindeliverytest@gmail.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Spanish Delivery</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ESDEL_Email_to_Case_Root__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Spain Delivery Workstream - Email to case receipts update owner and emailt2case root field</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Message%2EReopen case on user reply</fullName>
        <actions>
            <name>Email_Message_Reopen_case_on_user_reply</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>NA CCM Support</value>
        </criteriaItems>
        <description>Reopen a CCM case if an email reply was received.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Email Message Received</fullName>
        <actions>
            <name>Set_Last_Email_Received_On_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Case #529962 - See when a new email has been received</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Email Message Sent</fullName>
        <actions>
            <name>Set_Last_Email_Sent_On_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Case #529962 - See when a new email has been received</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Revert Soft close cases</fullName>
        <actions>
            <name>Revert_Soft_close_cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved,Closed Pending Acceptance,Closed No Action,Closed Cancelled,Closed Not Resolved,Closed Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ClosedDate</field>
            <operation>equals</operation>
            <value>LAST 5 DAYS</value>
        </criteriaItems>
        <description>If a message has being received for a Soft close case revert to In Progress</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
