<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Hierarchy_Name</fullName>
        <field>Name</field>
        <formula>Type__c + &apos;-&apos; +  Value__c</formula>
        <name>Populate Hierarchy Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Hierarchy_Unique_Key</fullName>
        <field>Unique_Key__c</field>
        <formula>Type__c + &apos;-&apos; +  Value__c</formula>
        <name>Populate Hierarchy Unique Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckProcessedRenamingForHierarchy</fullName>
        <field>Processed_Renaming__c</field>
        <literalValue>0</literalValue>
        <name>UncheckProcessedRenamingForHierarchy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UncheckProcessedReparentingForHierarchy</fullName>
        <field>Processed_Reparenting__c</field>
        <literalValue>0</literalValue>
        <name>UncheckProcessedReparentingForHierarchy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOldParentForHierarchy</fullName>
        <field>Old_Parent__c</field>
        <formula>PRIORVALUE( Parent__c )</formula>
        <name>UpdateOldParentForHierarchy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateOldValueForHierarchy</fullName>
        <field>Old_Value__c</field>
        <formula>PRIORVALUE( Value__c )</formula>
        <name>UpdateOldValueForHierarchy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Hierarchy Creation</fullName>
        <actions>
            <name>Populate_Hierarchy_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Hierarchy_Unique_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To Populate Unique Key and Name fields</description>
        <formula>AND(   NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),   OR(ISNEW(),  ISCHANGED(Type__c),  ISCHANGED(Value__c)),   NOT(ISBLANK(Type__c)),   NOT(ISBLANK(Value__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateOldParentRelatedFields</fullName>
        <actions>
            <name>UncheckProcessedReparentingForHierarchy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateOldParentForHierarchy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Parent__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateOldValueRelatedFields</fullName>
        <actions>
            <name>UncheckProcessedRenamingForHierarchy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateOldValueForHierarchy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(Value__c), ISBLANK(Old_Value__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
