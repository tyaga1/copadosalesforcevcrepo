<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_ContactName_Hidden_c</fullName>
        <field>ContactName_Hidden__c</field>
        <formula>Contact__r.FirstName &amp;&quot; &quot;&amp; Contact__r.LastName</formula>
        <name>Populate ContactName_Hidden__c</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate ContactName_Hidden%5F%5Fc</fullName>
        <actions>
            <name>Populate_ContactName_Hidden_c</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK( Contact__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
