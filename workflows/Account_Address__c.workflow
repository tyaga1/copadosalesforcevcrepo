<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Update_for_Country_of_Reg</fullName>
        <description>Update the Country of registration in account</description>
        <field>Country_of_Registration__c</field>
        <formula>TEXT(Address__r.Country__c)</formula>
        <name>Account Update for Country of Reg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Account_Name</fullName>
        <field>Account_Name__c</field>
        <formula>Account__r.Name</formula>
        <name>Populate Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Address_City_fields</fullName>
        <field>Address_City_Text__c</field>
        <formula>Address__r.City__c</formula>
        <name>Populate Address City fields</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Address_Country_fields</fullName>
        <field>Address_Country_Text__c</field>
        <formula>TEXT(Address__r.Country__c)</formula>
        <name>Populate Address Country fields</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Address_Postal_Code_fields</fullName>
        <field>Address_Postal_Code_Text__c</field>
        <formula>IF ( ISBLANK( Address__r.Postcode__c) , Address__r.Zip__c, Address__r.Postcode__c)</formula>
        <name>Populate Address Postal Code fields</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Address_Street</fullName>
        <field>Address_Street__c</field>
        <formula>TRIM(IF(!ISBLANK(Address__r.Address_1__c),Address__r.Address_1__c &amp; &apos; &apos;,&apos;&apos;) &amp; 
IF(!ISBLANK(Address__r.Address_2__c),Address__r.Address_2__c &amp; &apos; &apos;,&apos;&apos;))</formula>
        <name>Populate Address Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_CNPJ_From_Account</fullName>
        <field>Account_CNPJ__c</field>
        <formula>Account__r.CNPJ_Number__c</formula>
        <name>Populate CNPJ From Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_State_Text_fields</fullName>
        <field>State_Text__c</field>
        <formula>IF(ISBLANK(TEXT(Address__r.State__c)), Address__r.Province__c, TEXT(Address__r.State__c))</formula>
        <name>Populate State Text fields</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Address_on_account</fullName>
        <description>Update Billing Address on Account based on Account Registered Address</description>
        <field>BillingStreet</field>
        <formula>LEFT(TRIM(IF(!ISBLANK(Address__r.Address_1__c),Address__r.Address_1__c &amp; &apos; &apos;,&apos;&apos;) &amp;
IF(!ISBLANK(Address__r.Address_2__c),Address__r.Address_2__c &amp; &apos; &apos;,&apos;&apos;)),255)</formula>
        <name>Update Billing Address on account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_City_on_Account</fullName>
        <description>Update Billing City on Account based on Account Address</description>
        <field>BillingCity</field>
        <formula>LEFT(Address__r.City__c,40)</formula>
        <name>Update Billing City on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Country_on_Account</fullName>
        <description>Update Billing Country on Account based on the Registered Account Address</description>
        <field>BillingCountry</field>
        <formula>LEFT(TEXT(Address__r.Country__c),80)</formula>
        <name>Update Billing Country on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_State_on_Account</fullName>
        <description>Update Billing Address State based on registered Account Address</description>
        <field>BillingState</field>
        <formula>LEFT(IF(ISBLANK(Text(Address__r.State__c)),Address__r.Province__c,
Text(Address__r.State__c)),80)</formula>
        <name>Update Billing State on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Zip_on_Account</fullName>
        <description>Update Billing Zip on Account based on the registered Account Address</description>
        <field>BillingPostalCode</field>
        <formula>LEFT(IF(ISBLANK(Address__r.Zip__c), Address__r.Postcode__c,Address__r.Zip__c ),20)</formula>
        <name>Update Billing Zip on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Populate Account Address fields from Address</fullName>
        <actions>
            <name>Populate_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Address_City_fields</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Address_Country_fields</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Address_Postal_Code_fields</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Address_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_CNPJ_From_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_State_Text_fields</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(ISCHANGED(LastModifiedDate) || ISNEW()) &amp;&amp; $Setup.IsDataAdmin__c.IsDataAdmin__c == false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Billing Address on Account based on Registered Accnt Address</fullName>
        <actions>
            <name>Account_Update_for_Country_of_Reg</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Address_on_account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_City_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Country_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_State_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Zip_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Get the Registered Address back to the Billing Address on the Account</description>
        <formula>AND  (  ISPICKVAL(Address_Type__c,&apos;Registered&apos;),   OR ( ISNEW(), ISCHANGED( LastModifiedDate)   ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
