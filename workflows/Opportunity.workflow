<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_RT_JM_about_Proposal_Type_RFP</fullName>
        <description>Alert RT &amp; JM about Proposal Type RFP</description>
        <protected>false</protected>
        <recipients>
            <recipient>justin.muir@uk.experian.global1</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richardm.turner@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Proposal_Notification_to_RTurner_JMuir</template>
    </alerts>
    <alerts>
        <fullName>Automotive_Mash_Opportunity_Approved</fullName>
        <description>Automotive Mash Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>Automotive_Mash_Team</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/Automotive_Mash_Oppty_Approved</template>
    </alerts>
    <alerts>
        <fullName>Automotive_Mash_Opportunity_Rejected</fullName>
        <description>Automotive Mash Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>Automotive_Mash_Team</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/Automotive_Mash_Oppty_Rejected</template>
    </alerts>
    <alerts>
        <fullName>CCM_NA_Loss_Notification</fullName>
        <description>CCM NA Loss Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>CCM_NA_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Targeting_Renewal_has_been_abandoned_lost</template>
    </alerts>
    <alerts>
        <fullName>CCM_NA_Opp_WON_Notification</fullName>
        <ccEmails>stacey.herbst@experian.com</ccEmails>
        <ccEmails>brian.schmid@experian.com</ccEmails>
        <ccEmails>marcia.jacobs@experian.com</ccEmails>
        <ccEmails>yara.lutz@experian.com</ccEmails>
        <description>CCM NA Opp  WON Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>CCM_NA_Group</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel Renewal Owner</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Client Services</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Commercial Operations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Delivery Team</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Executive Sponsor</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Opportunity Owner</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pre-Sales Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Renewal Owner</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Strategic Client Director</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CCM_NA_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Delivery_Dates_Planned_and_Product_Start_Date_Change</fullName>
        <description>Delivery Dates Planned and Product Start Date Change</description>
        <protected>false</protected>
        <recipients>
            <field>Delivery_Email_Alert__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Product_Start_Dates_Changed</template>
    </alerts>
    <alerts>
        <fullName>Do_Not_Deal_Opportunity_AR_Breach_Notification</fullName>
        <ccEmails>accountsreceivable@uk.experian.com</ccEmails>
        <description>Do Not Deal Email Alert to AR if Opportunity Created</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Do_Not_Deal_Alert_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Do_Not_Deal_Opportunity_Breach_Notification</fullName>
        <ccEmails>ComplianceDeptUK@uk.experian.com</ccEmails>
        <ccEmails>accountsreceivable@uk.experian.com</ccEmails>
        <description>Do Not Deal Email Alert to Manager &amp; Compliance if Opportunity Created</description>
        <protected>false</protected>
        <recipients>
            <field>Creator_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Do_Not_Deal_Alert_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Do_Not_Deal_Opportunity_Compliance_Breach_Notification</fullName>
        <ccEmails>ComplianceDeptUK@uk.experian.com</ccEmails>
        <description>Do Not Deal Email Alert to Compliance if Opportunity Created</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Do_Not_Deal_Alert_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>EDQ_New_Business_Notification</fullName>
        <description>EDQ New Business Notification - NO Required any more. MZ</description>
        <protected>false</protected>
        <recipients>
            <recipient>crmsupport@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CCM_NA_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_COPs_and_Sales_User_Opp_Closed_Won</fullName>
        <description>Email COPs and Sales User Opp Closed Won</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>UK_I_MS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EDQ_Templates/EDQ_Email_COPS_Users_On_Close_Won_of_an_EDQ_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Email_COPs_and_Sales_User_Opp_Closed_Won_NA</fullName>
        <description>Email COPs and Sales User Opp Closed Won - NA</description>
        <protected>false</protected>
        <recipients>
            <recipient>NA_MS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>paul.blomerth@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EDQ_Templates/EDQ_Email_COPS_Users_On_Close_Won_of_an_EDQ_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_on_Lost_Oppty_Comp_Survey</fullName>
        <description>Email Owner on Lost Oppty (Comp Survey)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Owner_on_Lost_Oppty_Comp_Survey</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_on_Lost_Oppty_Pltform_Survey</fullName>
        <description>Email Owner on Lost Oppty (Pltform Survey)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Owner_on_Lost_Oppty_Pltform_Survey</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_on_Lost_Oppty_Product_Survey</fullName>
        <description>Email Owner on Lost Oppty (Product Survey)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Owner_on_Lost_Oppty_Product_Survey</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_on_Won_Oppty_Comp_Survey</fullName>
        <description>Email Owner on Won Oppty (Comp Survey)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Owner_on_Won_Oppty_Comp_Survey</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_on_Won_Oppty_Pltform_Survey</fullName>
        <description>Email Owner on Won Oppty (Pltform Survey)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Owner_on_Won_Oppty_Pltform_Survey</template>
    </alerts>
    <alerts>
        <fullName>Email_Owner_on_Won_Oppty_Product_Survey</fullName>
        <description>Email Owner on Won Oppty (Product Survey)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Owner_on_Won_Oppty_Product_Survey</template>
    </alerts>
    <alerts>
        <fullName>Email_to_the_IRB_members_when_International_Prod_Attached</fullName>
        <ccEmails>sarah.barber@experian.com</ccEmails>
        <description>Email to the IRB members when International Prod Attached</description>
        <protected>false</protected>
        <recipients>
            <recipient>calvin.castor@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>greg.carmean@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.hargreaves@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/International_Deal_Alert_for_IRB_Members</template>
    </alerts>
    <alerts>
        <fullName>Email_to_the_Opp_Owner_when_International_Prod_Attached</fullName>
        <description>Email to the Opp Owner when International Prod Attached</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/International_Deal_Alert_for_Opp_Owner2</template>
    </alerts>
    <alerts>
        <fullName>Email_when_Action_Required_Opportunity_1_Day_Overdue</fullName>
        <description>Email when Action Required: Opportunity 1 Day Overdue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Action_Required_Opportunity_1_Day_Overdue</template>
    </alerts>
    <alerts>
        <fullName>Email_when_Action_Required_Opportunity_3_Days_Overdue</fullName>
        <description>Email when Action Required: Opportunity 3 Days Overdue</description>
        <protected>false</protected>
        <recipients>
            <field>Owner_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Action_Required_Opportunity_3_Days_Overdue</template>
    </alerts>
    <alerts>
        <fullName>Global_Closed_Won_High_Value_Opportunity_Email</fullName>
        <description>Global - Closed Won High Value Opportunity Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>gareth.lee@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelli.stephenson@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Closed_Won_High_Value_Opportunity_Alert</template>
    </alerts>
    <alerts>
        <fullName>Global_Opportunity_High_Value_Alert</fullName>
        <description>Global Opportunity High Value Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>alexander.lethbridge@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gareth.lee@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.lgreen@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_High_Value_Opportunity_Alert</template>
    </alerts>
    <alerts>
        <fullName>Notification_for_Opportunities_open_30_days_after_close_date</fullName>
        <description>Notification for Opportunities open 30 days after close date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Close_date_beyond_30_days</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Lost</description>
        <protected>false</protected>
        <recipients>
            <recipient>EDQ_COPS_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Lost_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_APAC</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Lost (APAC)</description>
        <protected>false</protected>
        <recipients>
            <recipient>APAC_MS_ANZ_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>APAC_MS_AUS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>APAC_MS_AUS_EDQ_Customer_Operations_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Lost_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_EMEA</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Lost (EMEA)</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEA_MS_France_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Lost_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_NA</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Lost (NA)</description>
        <protected>false</protected>
        <recipients>
            <recipient>NA_MS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Lost_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_UK_I</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Lost (UK&amp;I)</description>
        <protected>false</protected>
        <recipients>
            <recipient>UK_I_MS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Lost_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>EDQ_COPS_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Won_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_APAC</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Won (APAC)</description>
        <protected>false</protected>
        <recipients>
            <recipient>APAC_MS_ANZ_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>APAC_MS_AUS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>APAC_MS_AUS_EDQ_Customer_Operations_Manager</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Won_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_EMEA</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Won (EMEA)</description>
        <protected>false</protected>
        <recipients>
            <recipient>EMEA_MS_France_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Won_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_NA</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Won (NA)</description>
        <protected>false</protected>
        <recipients>
            <recipient>NA_MS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Won_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_UK_I</fullName>
        <description>Notify SaaS Manager of an Opportunity Closed/Won (UK&amp;I)</description>
        <protected>false</protected>
        <recipients>
            <recipient>UK_I_MS_EDQ_Customer_Operations</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SaaS_Emails/EDQ_SaaS_Closed_Won_SaaS_Manager</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Opp_Won_Email_Alert</fullName>
        <ccEmails>salesshare@experian.com</ccEmails>
        <description>SalesShare Opp Won Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Lead_Referrer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Lead_Referrer_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Owner_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>brian.king2@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>claire.robinson@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>janice.cutri@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>joanna.couture@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lawrence.chen@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mayte.romo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nicholas.hopkins@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stacy.smith@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesshare@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SalesShare_Opp_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_about_Risk_Tool_Output</fullName>
        <ccEmails>commercialsupport@experian.com</ccEmails>
        <description>Send Email about Risk Tool Output</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Alert_for_Risk_Tool_Output_not_NOAA</template>
    </alerts>
    <alerts>
        <fullName>Send_Targeting_Survey_Email_to_APAC_Regions</fullName>
        <description>Send Targeting Survey Email to APAC Regions</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Channel Renewal Owner</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Client Services</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Commercial Operations</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Delivery Team</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Executive Sponsor</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Opportunity Owner</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pre-Sales Consultant</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Pricing Lead</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Renewal Owner</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Sales Rep</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>Strategic Client Director</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Consulting_Team_Survey_for_Sales</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_COPs_Users</fullName>
        <description>Send email to COPs Users</description>
        <protected>false</protected>
        <recipients>
            <recipient>EDQ_COPS_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>EDQ_Templates/EDQ_Email_COPS_Users_on_OLI_change_from_CPQ</template>
    </alerts>
    <alerts>
        <fullName>Senior_Approval</fullName>
        <description>Senior Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Senior_Approval</template>
    </alerts>
    <alerts>
        <fullName>Senior_Approver_Rejection</fullName>
        <description>Senior Approver Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Senior_Approver_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Stage_3_Approved</fullName>
        <description>Stage 3 Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Stage_3_Approved</template>
    </alerts>
    <alerts>
        <fullName>Stage_3_Rejected</fullName>
        <description>Stage 3 Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Stage_3_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Targeting_30_days_until_renewal_due</fullName>
        <description>Targeting 30 days until renewal due</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Targeting_30_days_until_renewal_due</template>
    </alerts>
    <alerts>
        <fullName>Targeting_60_days_until_renewal_due</fullName>
        <description>Targeting 60 days until renewal due</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Targeting_60_days_until_renewal_due</template>
    </alerts>
    <alerts>
        <fullName>Targeting_Opportunity_is_now_Closed_Won</fullName>
        <ccEmails>michael.daley@au.experian.com</ccEmails>
        <ccEmails>andrew.rudd@au.experian.com</ccEmails>
        <ccEmails>melissa.kendall@au.experian.com</ccEmails>
        <description>Targeting Opportunity is now Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>adrian.mcgill@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>elizabeth.pugh@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>john.leedham@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nicholas.j.merry@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Targeting_Opportunity_is_now_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Targeting_Renewal_has_been_abandoned_lost</fullName>
        <description>Targeting Renewal has been abandoned/lost</description>
        <protected>false</protected>
        <recipients>
            <recipient>david.colella@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Targeting_Renewal_has_been_abandoned_lost</template>
    </alerts>
    <alerts>
        <fullName>UK_I_Deal_Review_High_Value_Opportunity</fullName>
        <ccEmails>DealReviewRequests@experian.com</ccEmails>
        <description>UK&amp;I Deal Review - High Value Opportunity</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_UK_I_Deal_Review_High_Value_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>UK_I_Deal_Review_PayDay_loans_Opportunity</fullName>
        <ccEmails>DealReviewRequests@experian.com</ccEmails>
        <description>UK&amp;I Deal Review - PayDay loans Opportunity</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_UK_I_Deal_Review_Pay_Day_Lender</template>
    </alerts>
    <fieldUpdates>
        <fullName>Auto_Mash_Approval_Date_Field_Update</fullName>
        <description>WF to update Automotive&apos;s Mash Approval checkbox when an Opportunity needed Mash approval is required.</description>
        <field>Mash_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Auto Mash Approval Date - Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Original_Close_Date</fullName>
        <description>Populates the original close date with the close date (at point of creation)</description>
        <field>Original_close_date__c</field>
        <formula>CloseDate</formula>
        <name>Capture Original Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Oppt_record_type_to_Free_Trial</fullName>
        <description>Used to update the record type when an Oppt moves from Closed and is a Free Trial Opp</description>
        <field>RecordTypeId</field>
        <lookupValue>Free_Trial</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Oppt record type to Free Trial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Oppt_record_type_to_Standard</fullName>
        <description>Used to update the record type when an Oppt moves from Closed</description>
        <field>RecordTypeId</field>
        <lookupValue>Standard</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Oppt record type to Standard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date_from_Lead</fullName>
        <description>Close Date from lead conversion helper</description>
        <field>CloseDate</field>
        <formula>Target_Close_Date__c</formula>
        <name>Close Date from Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date_is_Today</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Close Date is Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>Id</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Forecast_Category_Omitted</fullName>
        <field>Forecast_Category__c</field>
        <literalValue>Omitted</literalValue>
        <name>Forecast Category Omitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Forecast_Category_on_Close</fullName>
        <field>Forecast_Category_on_Close__c</field>
        <formula>TEXT(PRIORVALUE( Forecast_Category__c ))</formula>
        <name>Forecast Category on Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Global_5M_USD_Threshold</fullName>
        <description>Tracks the usage of the Global Notification for 5 M USD</description>
        <field>Threshold_Notification_sent__c</field>
        <formula>PRIORVALUE(Threshold_Notification_sent__c)&amp; &quot; / &quot; &amp; &quot;Global 5M USD&quot;</formula>
        <name>Global 5M USD Threshold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Has_Senior_Approval</fullName>
        <field>Has_Senior_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Has Senior Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Has_Stage_3_Approval</fullName>
        <field>Has_Stage_3_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Has Stage 3 Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Num_Times_Close_Date_goes_back</fullName>
        <description>Field Update to increase the value of the Opportunity field Opportunity.Number_of_Times_Close_Date_Moved_Back__c</description>
        <field>Number_of_Times_Close_Date_Moved_Back__c</field>
        <formula>IF( ISNULL(Number_of_Times_Close_Date_Moved_Back__c), 1, Number_of_Times_Close_Date_Moved_Back__c + 1)</formula>
        <name>Increment Num Times Close Date goes back</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Number_of_Times_Prob_Reduced</fullName>
        <description>Field Update to increase the value of the Opportunity field “Number of Times Probability Reduced”</description>
        <field>Number_of_Times_Probability_Reduced__c</field>
        <formula>IF(
 ISNULL(Number_of_Times_Probability_Reduced__c),
 1, 
 Number_of_Times_Probability_Reduced__c + 1
)</formula>
        <name>Increment Number of Times Prob Reduced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Number_of_Times_Value_Reduced</fullName>
        <description>Field Update to increase the value of the Opportunity field “Number of Times Value Reduced”</description>
        <field>Number_of_Times_Value_Reduced__c</field>
        <formula>IF( 
ISNULL(Number_of_Times_Value_Reduced__c), 
1, 
Number_of_Times_Value_Reduced__c + 1 
)</formula>
        <name>Increment Number of Times Value Reduced</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsInApproval</fullName>
        <description>is checked when oppy record enters the approval process</description>
        <field>IsInApproval__c</field>
        <literalValue>1</literalValue>
        <name>IsInApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsInApproval_is_False</fullName>
        <description>Removed the true value when exiting the approval process</description>
        <field>IsInApproval__c</field>
        <literalValue>0</literalValue>
        <name>IsInApproval is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legacy_Next_Steps_Update</fullName>
        <description>This field update updates the legacy Next Steps status field with the previous Next Steps entry.</description>
        <field>Legacy_Opportunity_Status__c</field>
        <formula>IF(ISBLANK(PRIORVALUE( NextStep )), Legacy_Opportunity_Status__c, TEXT(MONTH(PRIORVALUE( Opportunity_Status_Last_Modified__c )))&amp;&quot;/&quot;&amp;
TEXT(DAY(PRIORVALUE(Opportunity_Status_Last_Modified__c )))&amp;&quot; - &quot;&amp;
PRIORVALUE( NextStep ) &amp;BR()&amp;
Legacy_Opportunity_Status__c)</formula>
        <name>Legacy Next Steps Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Next_Steps_Last_Modified_Update</fullName>
        <field>Opportunity_Status_Last_Modified__c</field>
        <formula>TODAY()</formula>
        <name>Next Steps Last Modified Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_RecordType_to_Free_Trial_Read_Only</fullName>
        <description>Will set the Record Type to Free_Trial_Read_Only</description>
        <field>RecordTypeId</field>
        <lookupValue>Free_Trial_Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp RecordType to Free Trial Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Closed_Lost</fullName>
        <field>Closed_Lost__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity 	Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Closed_No_Decision</fullName>
        <field>Closed_no_decision__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity Closed No Decision</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Senior_Approval_to_False</fullName>
        <description>Will set Opp.Has_Senior_Approval__c = false</description>
        <field>Has_Senior_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Opportunity Set Senior Approval to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Significant_Change_NULL</fullName>
        <description>Will set Opp.Has_There_Been_Significant_Change__c to NULL</description>
        <field>Has_There_Been_Significant_Change__c</field>
        <name>Opportunity Set Significant Change NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Significant_Change_YES</fullName>
        <description>Will set Opportunity.Has_There_Been_Significant_Change__c to &quot;Yes&quot;</description>
        <field>Has_There_Been_Significant_Change__c</field>
        <literalValue>Yes</literalValue>
        <name>Opportunity Set Significant Change YES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Significant_Change_is_NO</fullName>
        <description>Updates &apos;Has there been significant change?&apos; to &apos;No&apos; when approved during senior approval 4,5,6 process</description>
        <field>Has_There_Been_Significant_Change__c</field>
        <literalValue>No</literalValue>
        <name>Opportunity Set Significant Change is NO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_Default</fullName>
        <description>Opportunity Stage default = Stage 3</description>
        <field>StageName</field>
        <literalValue>3 - Qualify opportunity</literalValue>
        <name>Opportunity Stage Default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_TCV</fullName>
        <description>Update the Original TCV field when the opportunity is created</description>
        <field>Original_TCV__c</field>
        <formula>Amount</formula>
        <name>Original TCV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Managers_Email_Update</fullName>
        <field>Owner_Manager_s_Email__c</field>
        <formula>Owner.Manager.Email</formula>
        <name>Owner Managers Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Projected_Amount_capture</fullName>
        <description>Populated the Target amount with the initial opportunity Total Margin Value amount</description>
        <field>Projected_Amount__c</field>
        <formula>Previous_Opportunity__r.Total_Renewal_EDQ_Margin__c</formula>
        <name>Projected Amount capture</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_2</fullName>
        <description>See Case 01197945.</description>
        <field>Reached_Stage_2__c</field>
        <literalValue>1</literalValue>
        <name>Reached Stage 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_3</fullName>
        <field>Reached_Stage_3__c</field>
        <literalValue>1</literalValue>
        <name>Reached Stage 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_3_False</fullName>
        <field>Reached_Stage_3__c</field>
        <literalValue>0</literalValue>
        <name>Reached Stage 3 False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_4</fullName>
        <field>Reached_Stage_4__c</field>
        <literalValue>1</literalValue>
        <name>Reached Stage 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_4_False</fullName>
        <field>Reached_Stage_4__c</field>
        <literalValue>0</literalValue>
        <name>Reached Stage 4 False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_5</fullName>
        <field>Reached_Stage_5__c</field>
        <literalValue>1</literalValue>
        <name>Reached Stage 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_5_False</fullName>
        <field>Reached_Stage_5__c</field>
        <literalValue>0</literalValue>
        <name>Reached Stage 5 False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_6</fullName>
        <field>Reached_Stage_6__c</field>
        <literalValue>1</literalValue>
        <name>Reached Stage 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_6_False</fullName>
        <field>Reached_Stage_6__c</field>
        <literalValue>0</literalValue>
        <name>Reached Stage 6 False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_7</fullName>
        <field>Reached_Stage_7__c</field>
        <literalValue>1</literalValue>
        <name>Reached Stage 7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reached_Stage_7_False</fullName>
        <field>Reached_Stage_7__c</field>
        <literalValue>0</literalValue>
        <name>Reached Stage 7 False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SET_Quote_delivered_date_c_TO_NULL</fullName>
        <field>Quote_delivered_date__c</field>
        <name>SET Quote_delivered_date__c TO NULL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_no_decision_to_False</fullName>
        <description>Reset Closed No Decision flag to false</description>
        <field>Closed_no_decision__c</field>
        <literalValue>0</literalValue>
        <name>Set Closed - no decision to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Forecast_Category_Closed_Won</fullName>
        <field>Forecast_Category__c</field>
        <literalValue>Closed Won</literalValue>
        <name>Set Forecast Category Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Forecast_Category_to_Pipeline</fullName>
        <field>Forecast_Category__c</field>
        <literalValue>Pipeline</literalValue>
        <name>Set Forecast Category to Pipeline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_By</fullName>
        <description>Case 01199320</description>
        <field>Last_Modified_By_Not_System__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Set Last Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_Date</fullName>
        <description>Case 01199320</description>
        <field>Last_Modified_Date_Not_System__c</field>
        <formula>NOW()</formula>
        <name>Set Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_DQ_Score_Hold</fullName>
        <description>CRM2:W-005497: Sets a background field (copy of the formula result) to be used on another formula field.</description>
        <field>Opportunity_DQ_Score_Hold__c</field>
        <formula>Opportunity_DQ_Score__c</formula>
        <name>Set Opportunity DQ Score Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opty_Closed_Lost_to_False</fullName>
        <description>Reset Closed Lost to false</description>
        <field>Closed_Lost__c</field>
        <literalValue>0</literalValue>
        <name>Set Opty Closed Lost to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_To_Qualify</fullName>
        <field>StageName</field>
        <literalValue>Qualify</literalValue>
        <name>Set Stage To Qualify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_prior_to_closed_lost_no_decision</fullName>
        <field>Stage_prior_to_closed_lost_no_decision__c</field>
        <formula>TEXT(PRIORVALUE(StageName))</formula>
        <name>Stage prior to closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Company_Reg_from_account_to_opp</fullName>
        <field>Company_Registration__c</field>
        <formula>Account.Company_Registration__c</formula>
        <name>Stamp Company Reg from account to opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TCV_from_Lead</fullName>
        <field>Amount</field>
        <formula>TCV_from_Lead__c</formula>
        <name>TCV from Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TCV_on_Entering_Commit_Stage</fullName>
        <description>The field update stamped  TCV of the opportunity once it has left stage 4.  Originally called &apos;Original TCV&apos;</description>
        <field>TCV_on_Entering_Commit_Stage__c</field>
        <formula>IF(TEXT(StageName)== $Label.OPPTY_STAGE_COMMIT,Amount,0)</formula>
        <name>TCV on Entering Commit Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UK_250K_GBP_Threshold</fullName>
        <description>Tracks the usage of the UK&amp;I Notification for 250K GBP</description>
        <field>Threshold_Notification_sent__c</field>
        <formula>PRIORVALUE(Threshold_Notification_sent__c)&amp; &quot; / &quot; &amp; &quot;UK 250K GBP&quot;</formula>
        <name>UK 250K GBP Threshold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Forecast_from_Omitted</fullName>
        <description>Used when changing from Identify</description>
        <field>Forecast_Category__c</field>
        <literalValue>Pipeline</literalValue>
        <name>Update Forecast from Omitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Forecast_to_Omitted</fullName>
        <field>Forecast_Category__c</field>
        <literalValue>Omitted</literalValue>
        <name>Update Forecast to Omitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Record_Type</fullName>
        <description>When Oppty Status is equal to “Won – Implemented”, “Lost” and “Lost – Previously Won”</description>
        <field>RecordTypeId</field>
        <lookupValue>Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Opportunity Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_field_Total_Margin_Value_history</fullName>
        <description>Update Opportunity field Total Margin Value history from roll up summary field Total_Margin_Value__c</description>
        <field>Total_Margin_Value_history__c</field>
        <formula>Total_Margin_Value__c</formula>
        <name>Update field Total Margin Value history</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>APAC EDQ Follow Up on Wins</fullName>
        <actions>
            <name>X30_Day_Follow_Up</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>X90_Day_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Execute</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Team_Channel__c</field>
            <operation>equals</operation>
            <value>APAC MS AUS EDQ Enterprise Accounts,APAC MS EDQ Sales,APAC MS EDQ Technical,APAC MS SEA EDQ</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCM NA Lost Notification</fullName>
        <actions>
            <name>CCM_NA_Loss_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS(Owner.UserRole.Name, &quot;NA MS CCM&quot;) &amp;&amp; Owner.Profile.Name == &quot;Experian Sales Executive&quot; &amp;&amp; TEXT(StageName) == &quot;Closed Lost&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CCM NA Oppty Notification</fullName>
        <actions>
            <name>CCM_NA_Opp_WON_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS(Owner.UserRole.Name, &quot;NA MS CCM&quot;) &amp;&amp;  Owner.Profile.Name == &quot;Experian Sales Executive&quot; &amp;&amp;  IsWon &amp;&amp; TEXT(StageName) == $Label.OPPTY_STAGE_EXECUTE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Oppt record type to Free Trial</fullName>
        <actions>
            <name>Change_Oppt_record_type_to_Free_Trial</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Opportunity Record Type is &quot;Free Trial Read Only&quot; and the Stage name has changed from &quot;Execution&quot; or &quot;Abandoned&quot; to an open stage,  then set the Record Type to &quot;Free Trial&quot;</description>
        <formula>AND(  RecordType.Name = &apos;Free Trial Read Only&apos;,  NOT(   OR(    ISPICKVAL(StageName,&apos;Execution&apos;),    ISPICKVAL(StageName,&apos;Abandoned&apos;)   )  ),  $Permission.Administration_Profile )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Oppt record type to Standard</fullName>
        <actions>
            <name>Change_Oppt_record_type_to_Standard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Opportunity Record Type is Read Only and the Stage name has changed from Stage 7 then set the Record Type to Standard</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  $Permission.Administration_Profile,  RecordType.DeveloperName = &apos;Read_Only&apos;,  TEXT(StageName) != $Label.OPPTY_STAGE_EXECUTE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Free Trial Read Only</fullName>
        <actions>
            <name>Opp_RecordType_to_Free_Trial_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Free Trial</value>
        </criteriaItems>
        <description>Will change a Free Trial to be read only</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Read Only</fullName>
        <actions>
            <name>Update_Opportunity_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>notEqual</operation>
            <value>Free Trial,Transactional Sale</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change from Omitted after Identify</fullName>
        <actions>
            <name>Update_Forecast_from_Omitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(StageName) &amp;&amp; TEXT(PRIORVALUE(StageName)) == &apos;Identify&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change to Omitted for Identify</fullName>
        <actions>
            <name>Update_Forecast_to_Omitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Please don&apos;t change the workflow criteria, it&apos;s very fragile! (PK 13/10/15)</description>
        <formula>(ISPICKVAL(StageName,&apos;Identify&apos;) &amp;&amp; (  TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_QUALIFY ||  TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_PROPOSE ||  TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_CONTRACT ||  TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_COMMIT  )  || (   ISPICKVAL(StageName,&apos;Identify&apos;) &amp;&amp;    ISNEW() &amp;&amp;    Converted_from_Lead__c == false  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Date is Today</fullName>
        <actions>
            <name>Close_Date_is_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Regardless of the close date ensure on close (won, lost, no decision) when it changes the close date is set to &apos;today&apos;</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  NOT($Permission.Administration_Profile),  NOT(   AND(    OR($Profile.Name = &apos;Experian Sales Support&apos;,     $Profile.Name = &apos;Experian EDQ Sales Support&apos;), CONTAINS($UserRole.Name, &quot;EDQ&quot;)   )  ),  CloseDate != TODAY(),  IsClosed )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Opportunity DQ Score to hidden field</fullName>
        <actions>
            <name>Set_Opportunity_DQ_Score_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CRM2:W-005497: Sets a background field (copy of the formula result) to be used on another formula field.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Deal Opportunity AR Breach</fullName>
        <actions>
            <name>Do_Not_Deal_Opportunity_AR_Breach_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Will send an email if an Opportunity is created against an Account which has &quot;Do Not Deal&quot; set to true and the &quot;Do Not Deal Reason&quot; is Accounts Receivable</description>
        <formula>Account.Do_Not_Deal__c = true  &amp;&amp; ISPICKVAL(Account.Do_Not_Deal_Reason_PL__c, &quot;Accounts Receivable&quot;) &amp;&amp; NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Deal Opportunity Breach</fullName>
        <actions>
            <name>Do_Not_Deal_Opportunity_Breach_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Will send an email if an Opportunity is created against an Account which has &quot;Do Not Deal&quot; set to true and the Do Not Deal Reason is empty</description>
        <formula>Account.Do_Not_Deal__c = true  &amp;&amp; ISBLANK(TEXT(Account.Do_Not_Deal_Reason_PL__c)) &amp;&amp; NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Deal Opportunity Compliance Breach</fullName>
        <actions>
            <name>Do_Not_Deal_Opportunity_Compliance_Breach_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Will send an email if an Opportunity is created against an Account which has &quot;Do Not Deal&quot; set to true and the &quot;Do Not Deal Reason&quot; is Compliance</description>
        <formula>Account.Do_Not_Deal__c = true  &amp;&amp; ISPICKVAL(Account.Do_Not_Deal_Reason_PL__c, &quot;Compliance&quot;) &amp;&amp; NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ New Business Notification</fullName>
        <actions>
            <name>EDQ_New_Business_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;USD 8,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_s_Business_Unit__c</field>
            <operation>equals</operation>
            <value>NA MS Data Quality,NA CS Data Quality</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New From New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>7 - Implement solution</value>
        </criteriaItems>
        <description>Notification of when a new business deal is won over $8,000</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Close Won SaaS Manager Notification</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  AND(  IsClosed = true,  IsWon = true,  Count_of_SaaS_Line_Items__c &gt; 0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Close Won SaaS Manager Notification - APAC</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_APAC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (APAC)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( IsClosed = true, IsWon = true, Count_of_SaaS_Line_Items__c &gt; 0,  Region_of_Opportunity_owner__c =&apos;APAC&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Close Won SaaS Manager Notification - EMEA</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (EMEA)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( IsClosed = true, IsWon = true, Count_of_SaaS_Line_Items__c &gt; 0,  Region_of_Opportunity_owner__c =&apos;EMEA&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Close Won SaaS Manager Notification - NA</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_NA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (NA)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  AND(  IsClosed = true,  IsWon = true,  Count_of_SaaS_Line_Items__c &gt; 0,   Region_of_Opportunity_owner__c =&apos;North America&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Close Won SaaS Manager Notification - UK%26I</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Won_UK_I</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (UK&amp;I)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( IsClosed = true, IsWon = true, Count_of_SaaS_Line_Items__c &gt; 0,  Region_of_Opportunity_owner__c =&apos;UK&amp;I&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Closed Lost SaaS Manager Notification</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  AND(   IsChanged(IsClosed),    IsClosed = true,    IsWon = false,     Count_of_SaaS_Line_Items__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Closed Lost SaaS Manager Notification - APAC</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_APAC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (APAC)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( IsChanged(IsClosed), IsClosed = true, IsWon = false, Count_of_SaaS_Line_Items__c &gt; 0,Region_of_Opportunity_owner__c =&apos;APAC&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Closed Lost SaaS Manager Notification - EMEA</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_EMEA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (EMEA)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( IsChanged(IsClosed), IsClosed = true, IsWon = false, Count_of_SaaS_Line_Items__c &gt; 0,Region_of_Opportunity_owner__c =&apos;EMEA&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Closed Lost SaaS Manager Notification - NA</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_NA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (NA)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( IsChanged(IsClosed), IsClosed = true, IsWon = false, Count_of_SaaS_Line_Items__c &gt; 0,Region_of_Opportunity_owner__c =&apos;North America&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EDQ SaaS - Closed Lost SaaS Manager Notification - UK</fullName>
        <actions>
            <name>Notify_SaaS_Manager_of_an_Opportunity_Closed_Lost_UK_I</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email notification to the SaaS Managers when an opportunity with SaaS Line Items is closed won so they can provision the SaaS Users and Products in edq.com (UK)</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( IsChanged(IsClosed), IsClosed = true, IsWon = false, Count_of_SaaS_Line_Items__c &gt; 0,Region_of_Opportunity_owner__c =&apos;UK&amp;I&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert for EDQ Closed%2FWon Opportunity</fullName>
        <actions>
            <name>Email_COPs_and_Sales_User_Opp_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email to COPs inbox and Sales User when an EDQ opportunity is closed won</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND(   ISCHANGED(IsClosed),   ISCHANGED(IsWon),    IsClosed = true,   IsWon = true,   ISPICKVAL(Owner.Business_Unit__c, &apos;UK&amp;I MS Data Quality&apos;)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert for EDQ Closed%2FWon Opportunity - NA</fullName>
        <actions>
            <name>Email_COPs_and_Sales_User_Opp_Closed_Won_NA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; 
AND(
  ISCHANGED(IsClosed), 
  ISCHANGED(IsWon), 
  IsClosed = true, 
  IsWon = true, 
  OR(
    ISPICKVAL(Owner.Business_Unit__c, &apos;NA MS Data Quality&apos;),
    ISPICKVAL(Owner.Business_Unit__c, &apos;NA CS Data Quality&apos;)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Owner on Lost Oppty Comp Survey</fullName>
        <actions>
            <name>Email_Owner_on_Lost_Oppty_Comp_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 and 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Primary_Winning_Competitor__c</field>
            <operation>notEqual</operation>
            <value>NONE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_BU_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>NA CS Data Quality</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>notEqual</operation>
            <value>Credited</value>
        </criteriaItems>
        <description>Case 01973473: Opportunities-Workflow rule to add Automation of emails with surveys
Email Owner on Lost Opportunity (Competition Survey)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Owner on Lost Oppty Pltform Survey</fullName>
        <actions>
            <name>Email_Owner_on_Lost_Oppty_Pltform_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Count_of_Products_for_Surveys__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_BU_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>NA CS Data Quality</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>notEqual</operation>
            <value>Credited</value>
        </criteriaItems>
        <description>Case 01973473: Opportunities-Workflow rule to add Automation of emails with surveys
Email Owner on Lost Opportunity (Platform Survey)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Owner on Lost Oppty Product Survey</fullName>
        <actions>
            <name>Email_Owner_on_Lost_Oppty_Product_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Primary_Reason_W_L__c</field>
            <operation>equals</operation>
            <value>Inadequate Data Quality,Inadequate Data Range,Inadequate Product / Functionality,Price Too High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_BU_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>NA CS Data Quality</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>notEqual</operation>
            <value>Credited</value>
        </criteriaItems>
        <description>Case 01973473: Opportunities-Workflow rule to add Automation of emails with surveys
Email Owner on Lost Opportunity (Product Survey)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Owner on Won Oppty Comp Survey</fullName>
        <actions>
            <name>Email_Owner_on_Won_Oppty_Comp_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Execute</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New From Existing,New From New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_BU_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>NA CS Data Quality</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Competitor_name_s__c</field>
            <operation>notEqual</operation>
            <value>None</value>
        </criteriaItems>
        <description>Case 01973473: Opportunities-Workflow rule to add Automation of emails with surveys
Email Owner on Won Opportunity (Competition Survey)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Owner on Won Oppty Pltform Survey</fullName>
        <actions>
            <name>Email_Owner_on_Won_Oppty_Pltform_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Execute</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Count_of_Products_for_Surveys__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_BU_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>NA CS Data Quality</value>
        </criteriaItems>
        <description>Case 01973473: Opportunities-Workflow rule to add Automation of emails with surveys
Email Owner on Won Opportunity (Platform Survey)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Owner on Won Oppty Product Survey</fullName>
        <actions>
            <name>Email_Owner_on_Won_Oppty_Product_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Execute</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Primary_Reason_W_L__c</field>
            <operation>equals</operation>
            <value>Competitive Pricing,Superior Data Quality,Superior Data Range</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_BU_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>NA CS Data Quality</value>
        </criteriaItems>
        <description>Case 01973473: Opportunities-Workflow rule to add Automation of emails with surveys
Email Owner on Won Opportunity (Product Survey)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Field Updates on Closed Lost</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Field Updates on Closed Lost - Status, Stage, Exit Criteria</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Forecast Category Closed Won</fullName>
        <actions>
            <name>Forecast_Category_on_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Forecast_Category_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IsWon</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Forecast Category Omitted</fullName>
        <actions>
            <name>Forecast_Category_Omitted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Forecast_Category_on_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IsWon = false &amp;&amp; IsClosed</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global - Closed Won High Value Opportunity</fullName>
        <actions>
            <name>Global_Closed_Won_High_Value_Opportunity_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Execute</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;USD 3,000,000&quot;</value>
        </criteriaItems>
        <description>Email alert sent to the GSO reporting team to alert of high value closed won opportunities. This is so we can encourage sharing the win over Chatter and ensure recognition is given</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global - High Value Opportunity Alert</fullName>
        <actions>
            <name>Global_Opportunity_High_Value_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Global_5M_USD_Threshold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;USD 5,000,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Threshold_Notification_sent__c</field>
            <operation>notContain</operation>
            <value>Global 5M USD</value>
        </criteriaItems>
        <description>Email alert sent to the GSO reporting team to alert of unusually high value opportunities allowing them to sense check the value is genuine or a mistake - will allow them to be proactive   prior to running reports</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Next Step Description Update</fullName>
        <actions>
            <name>Legacy_Next_Steps_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Next_Steps_Last_Modified_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow takes the Next Step description and copies it into the Legacy Next Steps field.</description>
        <formula>ISCHANGED( NextStep )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notices for Past Close Date v2</fullName>
        <active>true</active>
        <description>For Opportunities with Close Date of 1 day and 3 days overdue on an open opportunity</description>
        <formula>IsClosed = False &amp;&amp; CloseDate &gt;= (Today()-3)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_when_Action_Required_Opportunity_1_Day_Overdue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_when_Action_Required_Opportunity_3_Days_Overdue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OLI change from CPQ Notification</fullName>
        <actions>
            <name>Send_email_to_COPs_Users</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify COPs Users when a line item is changed on an Opportunity</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; ISCHANGED( Renewal_Change_Timestamp__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %09Closed Lost</fullName>
        <actions>
            <name>Opportunity_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Closed_no_decision_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Amount Changed Significantly</fullName>
        <actions>
            <name>Opportunity_Set_Senior_Approval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Amount is dramatically increased, the Significant_Change__c field will be updated to true. - Replacement for &quot;Opportunity Significant Changes Made&quot; rule.</description>
        <formula>AND(  ISPICKVAL(Owner.Region__c, &apos;UK&amp;I&apos;),  OR(   ISPICKVAL(StageName, &apos;Propose&apos;),   ISPICKVAL(StageName, &apos;Commit&apos;),   ISPICKVAL(StageName, &apos;Contract&apos;)  ),  Amount &gt; 500000,  PRIORVALUE(Amount) &lt;= 500000 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Closed No Decision</fullName>
        <actions>
            <name>Opportunity_Closed_No_Decision</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Closed_Lost_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed No Decision</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Significant Changes Made</fullName>
        <actions>
            <name>Opportunity_Set_Senior_Approval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_Set_Significant_Change_YES</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If important fields on the Opp such as &quot;Is there Delivery/Financial/Commercial/Legal Risk?&quot; or Amount is dramatically increased, the Significant_Change__c field will be updated to true.</description>
        <formula>ISPICKVAL(Owner.Region__c, &apos;UK&amp;I&apos;) &amp;&amp;  (  TEXT(StageName) == $Label.OPPTY_STAGE_PROPOSE ||   TEXT(StageName) == $Label.OPPTY_STAGE_COMMIT ||   TEXT(StageName) == $Label.OPPTY_STAGE_CONTRACT ) &amp;&amp;  OR(  (   Amount &gt; 500000 &amp;&amp;    PRIORVALUE(Amount) &lt;= 500000  ),(   ISPICKVAL(Is_There_Commercial_Risk__c, &apos;Yes&apos;) &amp;&amp;    (    ISPICKVAL(PRIORVALUE(Is_There_Commercial_Risk__c), &apos;No&apos;) ||    ISPICKVAL(PRIORVALUE(Is_There_Commercial_Risk__c), &apos;&apos;)   )  ),(   ISPICKVAL(Is_There_Delivery_Risk__c, &apos;Yes&apos;) &amp;&amp;    (    ISPICKVAL(PRIORVALUE(Is_There_Delivery_Risk__c), &apos;No&apos;) ||    ISPICKVAL(PRIORVALUE(Is_There_Delivery_Risk__c), &apos;&apos;)   )  ),(   ISPICKVAL(Is_There_Financial_Risk__c, &apos;Yes&apos;) &amp;&amp;    (    ISPICKVAL(PRIORVALUE(Is_There_Financial_Risk__c), &apos;No&apos;) ||     ISPICKVAL(PRIORVALUE(Is_There_Financial_Risk__c), &apos;&apos;)   )  ),(   ISPICKVAL(Is_There_Legal_Risk__c, &apos;Yes&apos;) &amp;&amp;    (    ISPICKVAL(PRIORVALUE(Is_There_Legal_Risk__c), &apos;No&apos;) ||     ISPICKVAL(PRIORVALUE(Is_There_Legal_Risk__c), &apos;&apos;)   )  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Stage Default</fullName>
        <actions>
            <name>Opportunity_Stage_Default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Stage default when created</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; NOT(ISPICKVAL(StageName,&quot;3 - Qualify opportunity&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Total Margin Value history field update</fullName>
        <actions>
            <name>Update_field_Total_Margin_Value_history</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Total_Margin_Value__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to capture changes in the value on the Total_Margin_Value__c Opportunity field by updating the field Total Margin Value history</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%09Close date in the past</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Owner_Region_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>APAC CS ANZ,APAC DA ANZ,APAC MS ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_for_Opportunities_open_30_days_after_close_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opportunity%3A Internation Product Attached</fullName>
        <actions>
            <name>Email_to_the_IRB_members_when_International_Prod_Attached</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_to_the_Opp_Owner_when_International_Prod_Attached</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.International_Product_Total_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;USD 30,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.International_Product_Attached__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Notifies IRB and opportunity owner if an International product has been attached to the opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Lead conversion helper</fullName>
        <actions>
            <name>Close_Date_from_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TCV_from_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Converted_Lead_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Original TCV</fullName>
        <actions>
            <name>Original_TCV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Oppty_ID_18_chars__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to capture the TCV upon opportunity creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Owner Manager Email Update</fullName>
        <actions>
            <name>Owner_Managers_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(Owner.Manager.Email &lt;&gt;  Owner_Manager_s_Email__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Projected Amount capture</fullName>
        <actions>
            <name>Projected_Amount_capture</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Oppty_ID_18_chars__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to capture the Target Amount upon opportunity creation from the Total Margin Value field</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>RFP Notification to RTurner %26 JMuir</fullName>
        <actions>
            <name>Alert_RT_JM_about_Proposal_Type_RFP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Proposal_Type__c</field>
            <operation>equals</operation>
            <value>Proactive proposal (Experian initiated quote),RFP/RFI/ITT (With pricing),RFP/RFI/ITT (Without pricing)</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reached Stage 2</fullName>
        <actions>
            <name>Reached_Stage_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>See Case 01197945.  This rule sets the check-box &apos;Been in Identify Stage&apos; (which is the field Reached_Stage_2__c) to let the user know that the Opportunity has been in the Identify Stage i.e. the &apos;Stage&apos; field has been set to &apos;Identify&apos;.</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c )  &amp;&amp; ISPICKVAL( StageName, &apos;Identify&apos;) = TRUE &amp;&amp; Reached_Stage_2__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reached Stage 3</fullName>
        <actions>
            <name>Reached_Stage_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the appropriate checkboxes when opportunity is at stage 3</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c ) &amp;&amp; TEXT(StageName) == $Label.OPPTY_STAGE_QUALIFY &amp;&amp; Reached_Stage_3__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reached Stage 4</fullName>
        <actions>
            <name>Reached_Stage_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the appropriate checkboxes when opportunity is at stage 4</description>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )&amp;&amp;  TEXT(StageName)== $Label.OPPTY_STAGE_PROPOSE &amp;&amp;  Reached_Stage_4__c = False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reached Stage 5</fullName>
        <actions>
            <name>Reached_Stage_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the appropriate checkboxes when opportunity is at stage 5</description>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )&amp;&amp;  TEXT(StageName)== $Label.OPPTY_STAGE_COMMIT &amp;&amp;  Reached_Stage_5__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reached Stage 6</fullName>
        <actions>
            <name>Reached_Stage_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_6</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the appropriate checkboxes when opportunity is at stage 6</description>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )&amp;&amp;  TEXT(StageName)== $Label.OPPTY_STAGE_CONTRACT &amp;&amp;  Reached_Stage_6__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reached Stage 7</fullName>
        <actions>
            <name>Reached_Stage_3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_6</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_7</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the appropriate checkboxes when opportunity is at stage 7</description>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )&amp;&amp; IsWon&amp;&amp; Reached_Stage_7__c = FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reached Stage Removal</fullName>
        <actions>
            <name>Reached_Stage_3_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_4_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_5_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_6_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_7_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>removes the &apos;reached stage&apos; once the opportunity goes backwards, it is then reset to the appropriate &apos;reached stage&apos;</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c ),  OR(   AND(    TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_EXECUTE,    OR(     TEXT(StageName)== $Label.OPPTY_STAGE_CONTRACT,     TEXT(StageName)== $Label.OPPTY_STAGE_COMMIT,     TEXT(StageName)== $Label.OPPTY_STAGE_PROPOSE,     TEXT(StageName)== $Label.OPPTY_STAGE_QUALIFY    )   ),   AND(    TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_CONTRACT,    OR(     TEXT(StageName)== $Label.OPPTY_STAGE_COMMIT,     TEXT(StageName)== $Label.OPPTY_STAGE_PROPOSE,     TEXT(StageName)== $Label.OPPTY_STAGE_QUALIFY    )   ),   AND(    TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_COMMIT,    OR(     TEXT(StageName)== $Label.OPPTY_STAGE_PROPOSE,     TEXT(StageName)== $Label.OPPTY_STAGE_QUALIFY    )   ),   AND(    TEXT(PRIORVALUE(StageName)) == $Label.OPPTY_STAGE_PROPOSE,    TEXT(StageName)== $Label.OPPTY_STAGE_QUALIFY   )  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove Significant change on stage 3%2C 4%2C 5 or 6</fullName>
        <actions>
            <name>Opportunity_Set_Significant_Change_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Will reset the Opp.Significant_Change__c field to null after switching to stage   3 or 4 or 5 or  6.</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c ),  ISPICKVAL(Owner.Region__c, &apos;UK&amp;I&apos;),  ISCHANGED(StageName),  OR(   TEXT(StageName)== $Label.OPPTY_STAGE_QUALIFY,   TEXT(StageName)== $Label.OPPTY_STAGE_PROPOSE,   TEXT(StageName)== $Label.OPPTY_STAGE_COMMIT,   TEXT(StageName)== $Label.OPPTY_STAGE_CONTRACT  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset Reached Stage flags when Opportunity reopened to Commit Stage</fullName>
        <actions>
            <name>Reached_Stage_6_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_7_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Opty is Reopened Reset the Reached Stage Flags appropriately</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  AND(   /* ISCHANGED(IsClosed), */          IsClosed = false,           IsWon = false,  TEXT(StageName) == $Label.OPPTY_STAGE_COMMIT )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset Reached Stage flags when Opportunity reopened to Contract Stage</fullName>
        <actions>
            <name>Reached_Stage_7_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Opty is Reopened Reset the Reached Stage Flags appropriately</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  AND(   
/* ISCHANGED(IsClosed),  */         IsClosed = false,           IsWon = false,  TEXT(StageName) == $Label.OPPTY_STAGE_CONTRACT )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset Reached Stage flags when Opportunity reopened to Propose Stage</fullName>
        <actions>
            <name>Reached_Stage_5_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_6_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_7_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Opty is Reopened Reset the Reached Stage Flags appropriately</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  AND(   /* ISCHANGED(IsClosed), */          IsClosed = false,           IsWon = false,  TEXT(StageName) == $Label.OPPTY_STAGE_PROPOSE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset Reached Stage flags when Opportunity reopened to Qualify Stage</fullName>
        <actions>
            <name>Reached_Stage_4_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_5_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_6_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reached_Stage_7_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Opty is Reopened Reset the Reached Stage Flags appropriately</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  AND(  
 /* ISCHANGED(IsClosed),       */

    IsClosed = false,           IsWon = false,  TEXT(StageName) == $Label.OPPTY_STAGE_QUALIFY )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset flags when Closed%2FWon Opportunity</fullName>
        <actions>
            <name>Set_Closed_no_decision_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Closed_Lost_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Opty is Closed/Won Reset the Closed Lost and Closed No Decision flags to false</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( ISCHANGED(IsClosed), ISCHANGED(IsWon), IsClosed = true, IsWon = true, AND ( OR ( Closed_no_decision__c =TRUE, Closed_Lost__c =TRUE) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset flags when Opportunity reopened</fullName>
        <actions>
            <name>Set_Closed_no_decision_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opty_Closed_Lost_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Opty is Reopened Reset the Closed Lost and Closed No Decision flags to false</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; AND( ISCHANGED(IsClosed), IsClosed = false, IsWon = false, OR ( Closed_no_decision__c =TRUE, Closed_Lost__c =TRUE ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Risk Tool Output not NOAA</fullName>
        <actions>
            <name>Send_Email_about_Risk_Tool_Output</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email to ??? when the Risk Tool Output is set to anything except NOAA</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  OR(   BEGINS(Owner_s_Business_Unit__c,&apos;UK&amp;I GTM &apos;),   BEGINS(Owner_s_Business_Unit__c,&apos;UK&amp;I CS &apos;),   BEGINS(Owner_s_Business_Unit__c,&apos;UK&amp;I DA &apos;)  ),  OR(   INCLUDES(Risk_Tool_Output_Code__c,&apos;DEL-PCO&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;DEL-Tally&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;EOL-CEMS&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;EOL-Delphi&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;EOL-ESB&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;EOL-Transact&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;FBR&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;ITDR&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;LEG&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;SOL&apos;),   INCLUDES(Risk_Tool_Output_Code__c,&apos;VER&apos;)  ),  NOT(ISBLANK(Risk_Tool_Output__c)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SET Quote delivered date to NULL</fullName>
        <actions>
            <name>SET_Quote_delivered_date_c_TO_NULL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Quote_Status__c</field>
            <operation>notEqual</operation>
            <value>Completed,Closed/Won,Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Opportunity Notification</fullName>
        <actions>
            <name>SalesShare_Opp_Won_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(  
NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),
TEXT(StageName) == $Label.OPPTY_STAGE_EXECUTE,  ISPICKVAL(LeadSource,&apos;SalesShare&apos;),  

IsWon )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email alert to delivery</fullName>
        <actions>
            <name>Delivery_Dates_Planned_and_Product_Start_Date_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE(Delivery_Dates_Planned__c) = True &amp;&amp; Delivery_Dates_Planned__c = False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set New Opportunities to Qualify for Lead Conversions</fullName>
        <actions>
            <name>Set_Forecast_Category_to_Pipeline</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Stage_To_Qualify</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ) &amp;&amp; Converted_from_Lead__c = true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Significant Change Manually Set</fullName>
        <actions>
            <name>Opportunity_Set_Senior_Approval_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a user manually sets significant change as &apos;Yes&apos; and the &apos;Has Senior Approval is True, make it false.</description>
        <formula>ISPICKVAL(Has_There_Been_Significant_Change__c, &apos;Yes&apos;) &amp;&amp; OR(   (ISPICKVAL(PRIORVALUE(Has_There_Been_Significant_Change__c), &apos;No&apos;)),   (ISPICKVAL(PRIORVALUE(Has_There_Been_Significant_Change__c), &apos;&apos;)) ) &amp;&amp; Has_Senior_Approval__c == true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage prior to closed lost%2Fno decision</fullName>
        <actions>
            <name>Stage_prior_to_closed_lost_no_decision</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Field update to Stage prior to closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Company Reg from account to opportunity</fullName>
        <actions>
            <name>Stamp_Company_Reg_from_account_to_opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Company_Registration__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Stamp Company Reg from account to opportunity, order and assets on close won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TCV on Entering Commit Stage</fullName>
        <actions>
            <name>TCV_on_Entering_Commit_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Oppty_ID_18_chars__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Commit</value>
        </criteriaItems>
        <description>Workflow to capture the TCV of the opportunity once it has left stage 4</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Targeting 30%2F60 days until renewal due</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Owner_Region_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>APAC CS ANZ,APAC DA ANZ,APAC MS ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Targeting_30_days_until_renewal_due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Previous_Opportunity_Contract_End_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Targeting_60_days_until_renewal_due</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Previous_Opportunity_Contract_End_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Targeting Opportunity has been abandoned%2Flost</fullName>
        <actions>
            <name>Targeting_Renewal_has_been_abandoned_lost</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Owner_Region_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>APAC CS ANZ,APAC DA ANZ,APAC MS ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Abandoned,Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Targeting Opportunity is now Closed Won</fullName>
        <actions>
            <name>Targeting_Opportunity_is_now_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Owner_Region_on_Opp_Close_Date__c</field>
            <operation>equals</operation>
            <value>APAC CS ANZ,APAC DA ANZ,APAC MS ANZ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Targeting Survey - Consultancy</fullName>
        <actions>
            <name>Send_Targeting_Survey_Email_to_APAC_Regions</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_s_Business_Unit__c</field>
            <operation>contains</operation>
            <value>APAC MS ANZ</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Task for Lost Opportunities</fullName>
        <actions>
            <name>Follow_Up_on_Previously_Lost_Deal</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Win_Back_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UK%26I Deal Review - High Value Opportunity</fullName>
        <actions>
            <name>UK_I_Deal_Review_High_Value_Opportunity</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>UK_250K_GBP_Threshold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Owner_s_Region__c</field>
            <operation>equals</operation>
            <value>UK&amp;I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;GBP 250,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Threshold_Notification_sent__c</field>
            <operation>notContain</operation>
            <value>UK 250K GBP</value>
        </criteriaItems>
        <description>Email alert sent to the UK&amp;I commercial team when an opportunity meets criteria on value and risk and therefore requires to go through the deal review process</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UK%26I Deal Review - PayDay Loans Opportunity</fullName>
        <actions>
            <name>UK_I_Deal_Review_PayDay_loans_Opportunity</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Sector__c</field>
            <operation>equals</operation>
            <value>Consumer financing and payday loans</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_s_Region__c</field>
            <operation>equals</operation>
            <value>UK&amp;I</value>
        </criteriaItems>
        <description>Email alert sent to the UK&amp;I commercial team when an opportunity meets criteria of being created against an account flagged as a payday loans company and therefore requires to go through the deal review process</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Opportunities created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Modified Custom Functionality</fullName>
        <actions>
            <name>Set_Last_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 01199320</description>
        <formula>NOT($Permission.Bypass_Last_Modified_By)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Number of Times Date Moved Back</fullName>
        <actions>
            <name>Increment_Num_Times_Close_Date_goes_back</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs when Date goes back</description>
        <formula>AND(  NOT(ISPICKVAL(StageName,&apos;identify&apos;)),  (CloseDate &gt; PRIORVALUE(CloseDate)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Number of Times Probability Reduced</fullName>
        <actions>
            <name>Increment_Number_of_Times_Prob_Reduced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Runs when Probability is reduced</description>
        <formula>AND(  NOT(ISPICKVAL(StageName,&apos;identify&apos;)),  Probability &lt; PRIORVALUE(Probability))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Number of Times Value Reduced</fullName>
        <actions>
            <name>Increment_Number_of_Times_Value_Reduced</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Captures the number of times opp.Amount was reduced</description>
        <formula>AND(  NOT(ISPICKVAL(StageName,&apos;identify&apos;)),  Amount &lt; PriorValue(Amount))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Original Close Date</fullName>
        <actions>
            <name>Capture_Original_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Oppty_ID_18_chars__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to capture the original close date when opportunity is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to Won at Stage 7</fullName>
        <active>false</active>
        <description>Updates Opty Status to Won - Not implemented if Stage = Stage 7</description>
        <formula>AND( ISPICKVAL(StageName,&apos;7 - Implement Solution&apos;), NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Action_Required_Opportunity_1_Day_Overdue</fullName>
        <assignedToType>owner</assignedToType>
        <description>Action Required: Opportunity 1 Day Overdue</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Action Required: Opportunity 1 Day Overdue</subject>
    </tasks>
    <tasks>
        <fullName>Action_Required_Opportunity_3_Days_Overdue</fullName>
        <assignedToType>owner</assignedToType>
        <description>Action Required: Opportunity 3 Days Overdue</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Action Required: Opportunity 3 Days Overdue</subject>
    </tasks>
    <tasks>
        <fullName>Follow_Up_on_Previously_Lost_Deal</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please follow up on this account which we previously lost to</description>
        <dueDateOffset>-120</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Win_Back_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow Up on Previously Lost Deal</subject>
    </tasks>
    <tasks>
        <fullName>X30_Day_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Solution been integrated? 
Realising value? Measures of success for the solution being deployed? 
Who is the lead User? And the lead Decision Maker ongoing? Owner of the renewal? 
Referrals for other stakeholders and projects. 
Invoice been paid?</description>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>30 Day Follow Up</subject>
    </tasks>
    <tasks>
        <fullName>X90_Day_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>Solution been integrated? 
Realising value? Measures of success for the solution being deployed? 
Who is the lead User? And the lead Decision Maker ongoing? Owner of the renewal? 
Referrals for other stakeholders and projects. 
Invoice been paid?</description>
        <dueDateOffset>90</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>90 Day Follow Up</subject>
    </tasks>
</Workflow>
