<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Do_Not_Deal_Order_Breach_Notification</fullName>
        <ccEmails>ComplianceDeptUK@uk.experian.com</ccEmails>
        <ccEmails>accountsreceivable@uk.experian.com</ccEmails>
        <description>Do Not Deal Email Alert to Manager &amp; Compliance if Order Created</description>
        <protected>false</protected>
        <recipients>
            <field>Creator_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Do_Not_Deal_Alert_Order</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_SFDC_Support_when_EDQ_OnDemand_Order_is_pushed_to_On_Demand</fullName>
        <description>Send Email to SFDC Support when EDQ OnDemand Order is pushed to On Demand</description>
        <protected>false</protected>
        <recipients>
            <recipient>sadar.yacob@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EDQ_ONDemand_Order</template>
    </alerts>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>Order_Number__c</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Set_Send_to_On_Demand_to_true</fullName>
        <description>Will set the Order__c.EDQ_Send_to_On_Demand__c to true</description>
        <field>EDQ_Send_to_On_Demand__c</field>
        <literalValue>1</literalValue>
        <name>Order Set Send to On Demand to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_By</fullName>
        <description>Case 01199320</description>
        <field>Last_Modified_By_Not_System__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Set Last Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_Date</fullName>
        <description>Case 01199320</description>
        <field>Last_Modified_Date_Not_System__c</field>
        <formula>NOW()</formula>
        <name>Set Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stamp_Company_Reg_from_account_to_order</fullName>
        <description>Stamp Company Reg from account to order</description>
        <field>Company_Registration__c</field>
        <formula>Account__r.Company_Registration__c</formula>
        <name>Stamp Company Reg from account to order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Order_Id_to_Boomi</fullName>
        <apiVersion>32.0</apiVersion>
        <description>This OBM is now Obsolete and not used</description>
        <endpointUrl>http://DELLPRDAB1.svc.experian.com/ws/simple/getOnDemandOrder</endpointUrl>
        <fields>EDQ_Integration_Id__c</fields>
        <fields>EDQ_Send_to_On_Demand__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>debbie.chamkasem@experian.global</integrationUser>
        <name>Send Order Id to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Do Not Deal Order Breach</fullName>
        <actions>
            <name>Do_Not_Deal_Order_Breach_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Will send an email if an Order is created against an Account which has &quot;Do Not Deal&quot; set to true</description>
        <formula>Account__r.Do_Not_Deal__c = true &amp;&amp;  NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Order Needs to be synced with On Demand</fullName>
        <actions>
            <name>Send_Email_to_SFDC_Support_when_EDQ_OnDemand_Order_is_pushed_to_On_Demand</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Order_Id_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Order__c.EDQ_Send_to_On_Demand__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Data Migration User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Credit Note</value>
        </criteriaItems>
        <description>This WF Rule will run if the EDQ_Send_to_On_Demand__c is set to true, which will notify Boomi that the order needs to be synced with On Demand</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order On Demand Updates</fullName>
        <actions>
            <name>Order_Set_Send_to_On_Demand_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks if the Order has an update requiring it to be synched by other systems (On Demand)</description>
        <formula>AND (   OR (     ISCHANGED(Account__c),     ISCHANGED( EDQ_Integration_Id__c )   ),   Number_of_On_Demand_Order_Line_Items__c &gt; 0,    
Locked__c = true )   ||  AND(   ISCHANGED( Locked__c ),   Number_of_On_Demand_Order_Line_Items__c &gt; 0,    
Locked__c = true,
 Status__c !=&apos;Credit Note&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stamp Company Reg from account to order</fullName>
        <actions>
            <name>Stamp_Company_Reg_from_account_to_order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamp Company Reg from account to opportunity, order and assets on close won</description>
        <formula>AND(     Opportunity__r.IsClosed = true,     LEN(Company_Registration__c )=0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Orders created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order__c.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Modified Custom Functionality</fullName>
        <actions>
            <name>Set_Last_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 01199320</description>
        <formula>NOT($Permission.Bypass_Last_Modified_By)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
