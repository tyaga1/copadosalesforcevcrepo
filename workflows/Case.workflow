<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>APAC_EDQ_Case_Auto_Response</fullName>
        <description>APAC EDQ Case Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>qassupport@au.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/APAC_EDQ_Case_Auto_Response</template>
    </alerts>
    <alerts>
        <fullName>Access_Request_Granted</fullName>
        <description>Access Request Granted</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Access_Request_Granted</template>
    </alerts>
    <alerts>
        <fullName>AutoMotive_Email_Alert_For_Chrysler_Cases</fullName>
        <description>AutoMotive Email Alert For Chrysler Cases</description>
        <protected>false</protected>
        <recipients>
            <recipient>alejandra.rojas@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ashley.cusack@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bill.wyllie@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>brian.finn@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>daniela.esquivel@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>diego.sanchez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gina.mcdowell@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maureen.fernandez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/Automotive_Chrysler_Case_Notice</template>
    </alerts>
    <alerts>
        <fullName>AutoMotive_Email_Alert_for_Sales_Rep_when_GRADE_is_A_OR_B</fullName>
        <description>AutoMotive Email Alert for Sales Rep when GRADE is A OR B</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Rep</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/Automotive_AE_s_Accounts_A_B_Email_Template_Alert</template>
    </alerts>
    <alerts>
        <fullName>Auto_AC_Hosting_New_Case_Email_Alert</fullName>
        <description>Auto AC Hosting New Case Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>albert.love@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>edmund.sanchez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>freddy.sanchez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jafet.chavarria@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tyaga.pati@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/AC_Hosting_Case_Notification</template>
    </alerts>
    <alerts>
        <fullName>Auto_Email_Alert_For_Case_Owner_Notification_For_Account_Changes</fullName>
        <description>Auto Email Alert For Case Owner Notification For Account Changes</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automotive_Templates/Auto_Account_Inactive_Reason_and_No_Bill_Notification</template>
    </alerts>
    <alerts>
        <fullName>Automotive_Case_Comment_Notification</fullName>
        <description>Automotive - Case Comment Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/Automotive_Email_Notificaiton_on_Case_Comment</template>
    </alerts>
    <alerts>
        <fullName>Automotive_Legal_Case_Type_Notification</fullName>
        <description>Automotive Legal Case Type Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>edmund.sanchez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laura.balrig@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vanessa.enright@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/Automotive_Legal_Case_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>CRM_Request_Global</fullName>
        <description>CRM Request Global</description>
        <protected>false</protected>
        <recipients>
            <recipient>mark.parysz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Case_Assignment_Email_Alert</fullName>
        <description>Case Assignment Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Management_Templates/CASESubmissionconfirmationStrats</template>
    </alerts>
    <alerts>
        <fullName>Case_Assignment_to_Queue_Alert</fullName>
        <description>Case Assignment to Queue Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/CASESubmissionconfirmation</template>
    </alerts>
    <alerts>
        <fullName>Case_Routing_Email</fullName>
        <description>Case Routing Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Case_Submission_confirmation</fullName>
        <description>Case - Submission confirmation</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASENewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Cases_Cancel_Downgrade_Closed_Approved_Email_Requestor</fullName>
        <description>Cases - Cancel/Downgrade - Closed Approved - Email Requestor</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASE_Closed_Cancel_Downgrade_Approved</template>
    </alerts>
    <alerts>
        <fullName>Cases_Cancel_Downgrade_Closed_Rejected_Email_Requestor</fullName>
        <description>Cases - Cancel/Downgrade - Closed Rejected - Email Requestor</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/CASE_Closed_Cancel_Downgrade_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Chatter_Team_Case_Assigned_Alert</fullName>
        <ccEmails>GetChatter@experian.com</ccEmails>
        <description>Chatter Team Case Assigned Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>chennour.wright@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>colleen.stauffacher@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Competitor_Created</fullName>
        <description>Competitor Created</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Competitor_Created</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Closure_Chinese_Notification</fullName>
        <description>DA Community Case Closure (Chinese) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Closure_Chinese</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Closure_Chinese_Traditional_Notification</fullName>
        <description>DA Community Case Closure (Chinese Traditional) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Closure_Chinese_Traditional</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Closure_English_Notification</fullName>
        <description>DA Community Case Closure (English) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Closure_English</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Closure_Portuguese_Notification</fullName>
        <description>DA Community Case Closure (Portuguese) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Closure_Portuguese</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Closure_Spanish_Notification</fullName>
        <description>DA Community Case Closure (Spanish) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Closure_Spanish</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Creation_Chinese_Notification</fullName>
        <description>DA Community Case Creation (Chinese) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Creation_Chinese</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Creation_Chinese_Traditional_Notification</fullName>
        <description>DA Community Case Creation (Chinese Traditional) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Creation_Chinese_Traditional</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Creation_English_Notification</fullName>
        <description>DA Community Case Creation (English) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Creation_English</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Creation_Portuguese_Notification</fullName>
        <description>DA Community Case Creation (Portuguese) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Creation_Portuguese</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Creation_Spanish_Notification</fullName>
        <description>DA Community Case Creation (Spanish) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Creation_Spanish</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Update_Chinese_Notification</fullName>
        <description>DA Community Case Update (Chinese) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Update_Chinese</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Update_Chinese_Traditional_Notification</fullName>
        <description>DA Community Case Update (Chinese Traditional) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Update_Chinese_Traditional</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Update_English_Notification</fullName>
        <description>DA Community Case Update (English) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Update_English</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Update_Portuguese_Notification</fullName>
        <description>DA Community Case Update (Portuguese) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Update_Portuguese</template>
    </alerts>
    <alerts>
        <fullName>DA_Community_Case_Update_Spanish_Notification</fullName>
        <description>DA Community Case Update (Spanish) Notification</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DA_Community_Templates/DA_Community_Case_Update_Spanish</template>
    </alerts>
    <alerts>
        <fullName>EDQ_Auto_Response_Holiday_Hours</fullName>
        <description>EDQ Auto Response - Holiday Hours</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>us.support.qas@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EDQ_Templates/EDQ_Holiday_Hours</template>
    </alerts>
    <alerts>
        <fullName>EDQ_GPD_Incident_P1_notification</fullName>
        <ccEmails>edqgpdnoc@experian.com</ccEmails>
        <description>EDQ GPD Incident P1 notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_P1_incident_case_created_for_EDQ_GPD_NOC_v2</template>
    </alerts>
    <alerts>
        <fullName>EDQ_GPD_Incident_P2_notification</fullName>
        <ccEmails>edqgpdnoc@experian.com</ccEmails>
        <description>EDQ GPD Incident P2 notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_P2_incident_case_created_for_EDQ_GPD_NOC_v2</template>
    </alerts>
    <alerts>
        <fullName>EDQ_GPD_Incident_P3_notification</fullName>
        <ccEmails>edqgpdnoc@experian.com</ccEmails>
        <description>EDQ GPD Incident P3 notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_P3_incident_case_created_for_EDQ_GPD_NOC_v2</template>
    </alerts>
    <alerts>
        <fullName>EDQ_GPD_NOC_case_notification</fullName>
        <description>EDQ GPD NOC case notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>EDQ_GPD_NOC</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_case_created_for_EDQ_GPD_NOC</template>
    </alerts>
    <alerts>
        <fullName>EDQ_GPD_NOC_deployment_notification</fullName>
        <description>EDQ GPD NOC deployment notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>EDQ_GPD_NOC</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_deployment_case_created_for_EDQ_GPD_NOC</template>
    </alerts>
    <alerts>
        <fullName>EDQ_NA_Case_auto_response_for_Concierge_Elite</fullName>
        <description>EDQ NA Case auto-response for Concierge Elite</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>edqconciergeelite@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EDQ_NA_Case_auto_response_Concierge_Elite</template>
    </alerts>
    <alerts>
        <fullName>EDQ_NA_Case_auto_response_for_Concierge_Premier</fullName>
        <description>EDQ NA Case auto-response for Concierge Premier</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>edqconciergepremier@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EDQ_NA_Case_auto_response_Concierge_Premier</template>
    </alerts>
    <alerts>
        <fullName>EDQ_NA_Case_auto_response_for_Concierge_Standard</fullName>
        <description>EDQ NA Case auto-response for Concierge Standard</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>edqconciergestandard@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EDQ_NA_Case_auto_response_Concierge_Standard</template>
    </alerts>
    <alerts>
        <fullName>EDQ_Technical_Support_Case_Comment_Notification</fullName>
        <description>EDQ Technical Support - Case Comment Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Collaborator</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Community_Case_Comment_Notification</template>
    </alerts>
    <alerts>
        <fullName>EMSSendEmailSLASeverityEmailAlert</fullName>
        <description>EMSSendEmailSLASeverityEmailAlert</description>
        <protected>false</protected>
        <recipients>
            <field>CaseOwner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_Template_for_SLA_Severity</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_a_Email_when_case_is_created_for_English</fullName>
        <description>EMS - Send a Email when case is created for English</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_create_case_notification_for_English</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_a_Email_when_case_is_created_for_French</fullName>
        <description>EMS - Send a Email when case is created for French</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_create_case_notification_for_French</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_a_Email_when_case_is_created_for_German</fullName>
        <description>EMS Send a Email when case is created for German</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_create_case_notification_for_German</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_a_Email_when_case_is_created_for_Spanish</fullName>
        <description>EMS Send a Email when case is created for Spanish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_create_case_notification_for_Spanish</template>
    </alerts>
    <alerts>
        <fullName>EMS_Send_a_Email_when_case_type_is_set_to_Incident</fullName>
        <ccEmails>incident@cheetahmail.de</ccEmails>
        <description>EMS - Send a Email when case type is set to Incident</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>EMS/Email_template_for_Incident_internal_notification</template>
    </alerts>
    <alerts>
        <fullName>ESDEL_Email_Owner_Overdue_Task</fullName>
        <description>Spain Delivery Workstream - Email owner of Task for overdue record</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ESDEL_Spain_Email_Templates/ESDEL_Task_Overdue_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Escalated_to_Supervisor_status_Change</fullName>
        <description>Email Alert for Escalated to Supervisor status Change</description>
        <protected>false</protected>
        <recipients>
            <recipient>albert.love@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>edmund.sanchez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>freddy.sanchez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jafet.chavarria@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>karina.revillat@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.barquero@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>natalie.salas@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>supy.blanco@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>experianautomotivedonotreply@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Automotive_Templates/Auto_Escalation_to_Supervisor_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_DQ_Team_Competitor_Request</fullName>
        <description>Email DQ Team Competitor Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>carmen.lopez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>joseph.marconi@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.saldivar@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariasoledad.barrera@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>natalia.diaz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.kublik@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pamela.espinosa@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paulina.ahumada@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Competitor_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>Email_DQ_Team_Data_Quality_Request</fullName>
        <description>Email DQ Team Data Quality Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>carmen.lopez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>celina.hsiung@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>huiyee.lim@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.saldivar@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariasoledad.barrera@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>natalia.diaz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.kublik@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pamela.espinosa@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paulina.ahumada@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Data_Quality_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>Email_DQ_Team_New_Case</fullName>
        <description>Email DQ Team New Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>carmen.lopez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maria.saldivar@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariasoledad.barrera@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>natalia.diaz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.kublik@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pamela.espinosa@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paulina.ahumada@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Competitor_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>Email_Entitlement_Escalation_Manager</fullName>
        <description>Email Entitlement Escalation Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Entitlement_Escalation_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/CASE_Service_Level_Violation</template>
    </alerts>
    <alerts>
        <fullName>Email_Latin_America_Access_Request_Received</fullName>
        <description>Email Latin America Access Request Received</description>
        <protected>false</protected>
        <recipients>
            <recipient>elaine.santos2@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelli.stephenson@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vanessa.ferreira@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_Templates/Access_Request_Received_Serasa</template>
    </alerts>
    <alerts>
        <fullName>Email_Latin_America_Alert_for_Serasa_CRM_Support</fullName>
        <description>Email Latin America CRM Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>elaine.santos2@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelli.stephenson@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vanessa.ferreira@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Email_SE_APAC_members_for_C_D</fullName>
        <ccEmails>Mukund.Barsagade@in.experian.com</ccEmails>
        <ccEmails>jonathan.major@au.experian.com</ccEmails>
        <ccEmails>Isaac.Wang@cn.experian.com</ccEmails>
        <ccEmails>Takayuki.Gunge@jp.experian.com</ccEmails>
        <ccEmails>Richard.Jorgensen@sg.experian.com</ccEmails>
        <description>Email SE APAC members for C&amp;D</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cancellation_and_Downgrade_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_SE_NA_members_for_C_D</fullName>
        <ccEmails>saleseffectiveness@experian.com</ccEmails>
        <description>Email SE NA members for C&amp;D</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cancellation_and_Downgrade_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_SE_NA_members_for_New_Case_Assignment</fullName>
        <ccEmails>janet.hurd@experian.com</ccEmails>
        <ccEmails>mark.parysz@experian.com</ccEmails>
        <description>Email SE NA members for New Case Assignment</description>
        <protected>false</protected>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>gabriela.sanders@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Email_SE_UK_members_for_C_D</fullName>
        <ccEmails>crmsupport@experian.com</ccEmails>
        <description>Email SE UK members for C&amp;D</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cancellation_and_Downgrade_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Sales_Effectiveness</fullName>
        <ccEmails>crmsupport@experian.com</ccEmails>
        <description>Email Sales Effectiveness UK Access Request</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Access_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>Email_Sales_Effectiveness_NA</fullName>
        <ccEmails>crmsupportna@experian.com</ccEmails>
        <description>Email Sales Effectiveness NA Access Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>gabriela.sanders@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>janet.hurd@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.parysz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Access_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>Email_demand_generation_team_for_New_Case_Assignment</fullName>
        <description>Email demand generation team for New Case Assignment</description>
        <protected>false</protected>
        <recipients>
            <recipient>Demand_Generation</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_for_Incident_in_Germany_CCMP_internal</fullName>
        <ccEmails>incident@cheetahmail.de</ccEmails>
        <description>Email notification for Incident in Germany CCMP internal</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>EMS/Email_template_for_Incident_internal_notification</template>
    </alerts>
    <alerts>
        <fullName>Fraud_and_Identity_Case_Closure_Notification</fullName>
        <description>Fraud and Identity Case Closure Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Strategic Client Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Collaborator</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fraud_and_Identity/CASE_Fraud_and_Identity_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Fraud_and_Identity_Case_Comment_Notification</fullName>
        <description>Fraud and Identity - Case Comment Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Strategic Client Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Collaborator</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Fraud_and_Identity/CASE_Fraud_and_Identity_Case_Comment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Fraud_and_Identity_New_Case_Notification</fullName>
        <description>Fraud and Identity - New Case Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Strategic Client Director</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Collaborator</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>customer.community@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Fraud_and_Identity/Fraud_and_Identity_New_Case_Notification_to_Customer</template>
    </alerts>
    <alerts>
        <fullName>GSO_Training_Email_Alert</fullName>
        <ccEmails>gsotraining@experian.com</ccEmails>
        <description>GSO Training Case Assigned Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>geoff.gordon@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lee.glenn@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Global_Security_Office_Approaching_Due_Date</fullName>
        <ccEmails>GSO-CSS-SF-Notifications@experian.com</ccEmails>
        <description>Global Security Office Approaching Due Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>khlood.elsayed@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>gso-css@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Global_Security_About_to_Expire</template>
    </alerts>
    <alerts>
        <fullName>Global_Security_Office_Confirmation_of_Request</fullName>
        <description>Global Security Office - Confirmation of Request</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Global_Security_Office_Case</template>
    </alerts>
    <alerts>
        <fullName>Global_Security_Office_Due_Date</fullName>
        <ccEmails>GSO-CSS-SF-Notifications@experian.com</ccEmails>
        <description>Global Security Office Due Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>khlood.elsayed@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>gso-css@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Global_Security_due_today</template>
    </alerts>
    <alerts>
        <fullName>Global_Security_Office_Request_Case_Closed_Notification</fullName>
        <description>Global Security Office Request Case Closed Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Experian_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASE_Global_Security_Office_Request_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Global_Security_Office_Request_Overdue</fullName>
        <ccEmails>GSO-CSS-SF-Notifications@experian.com</ccEmails>
        <description>Global Security Office - Request Overdue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>khlood.elsayed@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>gso-css@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Global_Security_Overdue</template>
    </alerts>
    <alerts>
        <fullName>InsWeb_Alert</fullName>
        <description>InsWeb Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>josh.berg@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASENewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>NA_MS_EDQ_Case_Auto_Response</fullName>
        <description>NA MS EDQ Case Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>us.support.qas@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/EDQ_Case_Auto_Response_HTML</template>
    </alerts>
    <alerts>
        <fullName>NA_MS_EDQ_Case_Auto_Response_4_17_15</fullName>
        <description>NA MS EDQ Case Auto Response 4-17-15</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>us.support.qas@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EDQ_Templates/EDQ_Case_Auto_Response_for_4_17_15</template>
    </alerts>
    <alerts>
        <fullName>New_Company_ID_Request_Received</fullName>
        <description>New Company ID Request Received</description>
        <protected>false</protected>
        <recipients>
            <recipient>rpagsorobot1@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_Templates/New_Company_ID_Request_Received</template>
    </alerts>
    <alerts>
        <fullName>New_User_Welcome_Email</fullName>
        <description>Email sent to welcome the new user just created</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>User_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NewUserWelcomeEmail</template>
    </alerts>
    <alerts>
        <fullName>Ready_For_UAT_Email_Alert</fullName>
        <description>Ready For UAT Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Management_Templates/Ready_For_UAT_Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Access_Request</fullName>
        <description>Rejected Access Request</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejected_Account_Access_Request</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Competitor_Request</fullName>
        <description>Rejected Competitor Request</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Competitor_Not_Created</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Case_Notification</fullName>
        <description>SalesShare Case Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>janice.cutri@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stacy.smith@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>SendEmailWhenCaseIsInProgress2HoursEnglish</fullName>
        <description>SendEmailWhenCaseIsInProgress2HoursEnglish</description>
        <protected>false</protected>
        <recipients>
            <field>CaseOwner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_Incident_Pending_Case_for_English</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Case_Is_Created_Without_Contact</fullName>
        <description>Send Email for Case Is Created Without Contact</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_Case_created_without_contact_information</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Incident_Closed_for_French</fullName>
        <description>Send Email for Incident Closed for French</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_Incident_Closed_Case_for_French</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Incident_Closed_for_German</fullName>
        <description>Send Email for Incident Closed for German</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_Incident_Closed_Case_for_German</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Incident_Closed_for_Spanish</fullName>
        <description>Send Email for Incident Closed for Spanish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_Incident_Closed_Case_for_Spanish</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Unresolved_Case_English</fullName>
        <description>Send Email for Unresolved Case English</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_closed_unresolved_case_for_English</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Unresolved_Case_French</fullName>
        <description>Send Email for Unresolved Case French</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_closed_unresolved_case_for_French</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Unresolved_Case_German</fullName>
        <description>Send Email for Unresolved Case German</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_closed_unresolved_case_for_German</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_for_Unresolved_Case_Spanish</fullName>
        <description>Send Email for Unresolved Case Spanish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_closed_unresolved_case_for_Spanish</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Case_Owner</fullName>
        <description>Send Email to Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Management_Templates/Case_Comment_Added</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Case_Requestor</fullName>
        <description>Send Email to Case Requestor</description>
        <protected>false</protected>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMRESLVED</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Case_Requestor_when_rejected</fullName>
        <description>Send Email to Case Requestor when rejected</description>
        <protected>false</protected>
        <recipients>
            <recipient>Requestor</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMREJCTED</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Creator</fullName>
        <description>Send Email to Creator</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Case_Management_Templates/Case_Comment_Added</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_GSO_Training_Queue</fullName>
        <description>Send Email to GSO Training Queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CASECRMNEWASSGN</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_HR_Org_Requestor_on_New_Request</fullName>
        <description>Send Email to HR Org Requestor on New Request</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>gcssalesforcesupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_HR_Org_Request_New</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_HR_Org_Requestor_on_Rejected</fullName>
        <description>Send Email to HR Org Requestor on Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>gcssalesforcesupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_HR_Org_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_HR_Org_Requestor_on_Resolved</fullName>
        <description>Send Email to HR Org Requestor on Resolved</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>gcssalesforcesupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_HR_Org_Request_Resolved</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_HR_Org_Requestor_on_Updates</fullName>
        <description>Send Email to HR Org Requestor on Updates</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>gcssalesforcesupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_HR_Org_Request_Updated</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Case_is_On_Hold_for_3_days_for_English</fullName>
        <description>Send Email when Case is On Hold for 3 days for English</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/More_Information_email_template_for_English</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Case_is_On_Hold_for_3_days_for_French</fullName>
        <description>Send Email when Case is On Hold for 3 days for French</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/More_Information_email_template_for_French</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Case_is_On_Hold_for_3_days_for_German</fullName>
        <description>Send Email when Case is On Hold for 3 days for German</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/More_Information_email_template_for_German</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Case_is_On_Hold_for_3_days_for_Spanish</fullName>
        <description>Send Email when Case is On Hold for 3 days for Spanish</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/More_Information_email_template_for_Spanish</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Case_is_On_Hold_for_7_days</fullName>
        <description>Send Email when Case is On Hold for 7 days</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_closed_unresolved_case_for_English</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Incident_Case_is_closed_for_English</fullName>
        <description>Send Email when Incident Case is closed for English</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>crmsupport@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EMS/Email_template_for_Incident_Closed_Case_for_English</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Alert</fullName>
        <description>Serasa Case Closed Complete Alert</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Island_CAC</fullName>
        <description>Serasa Case Closed Complete Island CAC</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template_CAC</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Island_SME</fullName>
        <description>Serasa Case Closed Complete Island SME</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template_SME</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Island_SME_Logon</fullName>
        <description>Serasa Case Closed Complete Island SME Logon</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template_SME_Logon</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Island_Serasa_Consumer</fullName>
        <description>Serasa Case Closed Complete Island Serasa Consumer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template_Serasa_Consumer</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Island_VIP</fullName>
        <description>Serasa Case Closed Complete Island VIP</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template_VIP</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Island_eID</fullName>
        <description>Serasa Case Closed Complete Island eID</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template_e_ID</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Closed_Complete_Island_positive_Registration</fullName>
        <description>Serasa Case Closed Complete Island positive Registration</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Cases_Closed_Complete_Template_Positive_Registration</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_Consumidor_and_Reason_Rec_PF</fullName>
        <description>Serasa Case Island Consumidor and Reason Rec - PF</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_Consumidor_Rec_PF</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_Pos_Reg_and_Reason_Canc_CP</fullName>
        <description>Serasa Case Island Pos Reg and Reason Canc - CP</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_Positive_Reg_Cancellation_CP</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_Pos_Reg_and_Reason_Concept_CP</fullName>
        <description>Serasa Case Island Pos Reg and Reason Concept - CP</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_Positive_Reg_Concept_CP</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_Pos_Reg_and_Reason_Opening_CP</fullName>
        <description>Serasa Case Island Pos Reg and Reason Opening - CP</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Email_Template_for_Pos_reg_and_Abertura</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_Pos_Reg_and_Reason_Query_CP</fullName>
        <description>Serasa Case Island Pos Reg and Reason Query - CP</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_Positive_Reg_Query_CP</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_SME_Logon_and_Reason_Inv_PL</fullName>
        <description>Serasa Case Island SME Logon and Reason Inv - PL</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_SME_Logon_Inv_PL</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_eID_and_Reason_E_commerce_eID</fullName>
        <description>Serasa Case Island eID and Reason E commerce - eID</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_eID_E_commerce_eID</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_eID_and_Reason_Usage_eID</fullName>
        <description>Serasa Case Island eID and Reason Usage - eID</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_eID_Usage_eID</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_eID_and_Reason_Validation_eID</fullName>
        <description>Serasa Case Island eID and Reason Validation - eID</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_eID_Validation_eID</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Island_eID_and_Reason_products_eID</fullName>
        <description>Serasa Case Island eID and Reason products - eID</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_eID_Products_eID</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Origin_Contact_Us</fullName>
        <description>Serasa Case Origin Contact Us</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_Origin_Contact_Us</template>
    </alerts>
    <alerts>
        <fullName>Serasa_Case_Origin_Except_Contact_Us</fullName>
        <description>Serasa Case Origin Except Contact Us</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@br.experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Serasa_Customer_Care/Serasa_Case_Origin_Except_Contact_Us</template>
    </alerts>
    <alerts>
        <fullName>The_case_has_been_escalated_to_SOS</fullName>
        <description>The case has been escalated to SOS</description>
        <protected>false</protected>
        <recipients>
            <field>Serasa_CaseOwner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Serasa_Priority_SOS_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>UK_I_EDQ_Case_Auto_Response</fullName>
        <description>UK&amp;I EDQ Case Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>uk.support.qas@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>EDQ_Templates/UK_I_EDQ_Case_Auto_Response</template>
    </alerts>
    <alerts>
        <fullName>X14_Days_Case_Waiting_for_Response</fullName>
        <description>14 Days Case Waiting for Response</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Response_pending_from_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/X14_Days_Case_Waiting_for_Response</template>
    </alerts>
    <alerts>
        <fullName>X28_Days_Case_Waiting_for_Response</fullName>
        <description>28 Days Case Waiting for Response</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Response_pending_from_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/X28_Days_Case_Waiting_for_Response</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Demand_Generation</fullName>
        <field>OwnerId</field>
        <lookupValue>Demand_Generation</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Demand Generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_AutoCheck_Escalated_Cases</fullName>
        <description>Changes Owner to Queue AutoCheck Escalated Cases</description>
        <field>OwnerId</field>
        <lookupValue>AutoCheck_Escalated_Cases</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner AutoCheck Escalated Cases</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_NA_MS_EDQ_Support_Tier_1</fullName>
        <description>Assigns NA MS EDQ - Support Tier 1 as case Owner in lieu of assignment rule</description>
        <field>OwnerId</field>
        <lookupValue>NA_MS_EDQ_Support_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner NA MS EDQ - Support Tier 1</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_UK_I_MS_EDQ_SupportTier1</fullName>
        <description>Changes Owner to Queue &quot;UK&amp;I MS EDQ - Support Tier 1&quot;</description>
        <field>OwnerId</field>
        <lookupValue>UK_I_MS_EDQ_Support_Tier_1</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner UK&amp;I MS EDQ - SupportTier1</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_APAC_SE</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Effectiveness_APAC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC SE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Global_CRM_Support</fullName>
        <description>Assigns Access Requests created from the Account record directly to the Global CRM Support Queue for action</description>
        <field>OwnerId</field>
        <lookupValue>Global_CRM_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Global CRM Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_NA_SE</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Effectiveness_NA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to NA SE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Sales_Effectiveness_Team</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Effectiveness</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK&amp;I &amp; EMEA SE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Serasa_CRM_Support</fullName>
        <description>Assign to Serasa CRM Support queue</description>
        <field>OwnerId</field>
        <lookupValue>Serasa_CRM_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Serasa CRM Support</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assigned_to_GCS</fullName>
        <field>Assigned_Date_to_GCS__c</field>
        <formula>NOW()</formula>
        <name>Assigned to GCS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assigned_to_GCS_Member</fullName>
        <description>Stamps the date when a case was assigned to GCS team</description>
        <field>Assigned_Date_to_GCS_team_Member__c</field>
        <formula>NOW()</formula>
        <name>Assigned to GCS Member</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Uncheck_Has_New_Case_Comment</fullName>
        <field>Has_New_Case_Comment__c</field>
        <literalValue>0</literalValue>
        <name>Auto Uncheck Has New Case Comment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Update_Type_Field_with_Velocity</fullName>
        <field>Type</field>
        <literalValue>Velocity</literalValue>
        <name>Auto Update Type Field with  Velocity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Update_the_Case_Type_field</fullName>
        <field>Type</field>
        <literalValue>AutoSupport</literalValue>
        <name>Auto Update the Case Type field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Update_the_Type_with_AutoCount</fullName>
        <field>Type</field>
        <literalValue>AutoCount</literalValue>
        <name>Auto Update the Type with AutoCount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Update_the_Type_with_Autocheck</fullName>
        <field>Type</field>
        <literalValue>AutoCheck</literalValue>
        <name>Auto Update the Type with Autocheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_CR_Negotiation_Date_to_Blank</fullName>
        <description>Will set the Case.Negotiation_Complete_Date__c to a blank value</description>
        <field>Negotiation_Complete_Date__c</field>
        <name>CSDA CR Negotiation Date to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_Online_Price_Change_Request_default</fullName>
        <field>Subject</field>
        <formula>&quot;Price Change&quot;</formula>
        <name>CSDA Online Price Change Request default</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assign_APAC_CPQ_Request</fullName>
        <field>OwnerId</field>
        <lookupValue>Sales_Effectiveness_APAC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Assign APAC CPQ Request</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assign_APAC_CRM_Request</fullName>
        <description>Assign APAC CRM Request Case to APAC SE.</description>
        <field>OwnerId</field>
        <lookupValue>Sales_Effectiveness_APAC</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Assign APAC CRM Request</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assign_internal_CRM_Request</fullName>
        <field>OwnerId</field>
        <lookupValue>Global_CRM_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Assign internal CRM Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assign_internal_Serasa_CRM_Request</fullName>
        <description>Assign internal Serasa CRM Request to Serasa CRM Support Queue</description>
        <field>OwnerId</field>
        <lookupValue>Serasa_CRM_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Assign internal Serasa CRM Request</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assign_internal_chatter_team</fullName>
        <description>Field update to change the case owner to the chatter team</description>
        <field>OwnerId</field>
        <lookupValue>Chatter_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Assign internal Chatter Team</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assign_to_GSO_Training</fullName>
        <field>OwnerId</field>
        <lookupValue>GSO_Training</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case: Assign to GSO Training</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Assigned_to_CSDA_CIS_Sales_Support</fullName>
        <field>Date_assigned_to_CSDA_CIS_Sales_Support__c</field>
        <formula>NOW()</formula>
        <name>Case Assigned to CSDA CIS Sales Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Island_Resolved</fullName>
        <field>Island_Resolved__c</field>
        <formula>Text(LastModifiedBy.Sales_Team__c )</formula>
        <name>Case Island Resolved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_New_User_Update_Subject</fullName>
        <description>Update the subject field to contact information from the case</description>
        <field>Subject</field>
        <formula>TEXT(Type) + &quot; - &quot; + TEXT(Region__c)  + &quot; - &quot; + User_First_Name__c + &quot; &quot; + User_Last_Name__c  + &quot; (&quot; + TEXT(License_Type__c) + &quot;)&quot;</formula>
        <name>Case New User Update Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Origin_Global_Community</fullName>
        <description>Sets the Origin to &quot;Global Community&quot;</description>
        <field>Origin</field>
        <literalValue>Global Community</literalValue>
        <name>Case Origin Global Community</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Response_pending_from_User_reset</fullName>
        <description>Field Response pending from User is reset to blank on Case when case status is no longer Waiting for response</description>
        <field>Response_pending_from_User__c</field>
        <name>Case Response pending from User reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Route_to_Global_CRM_Support</fullName>
        <field>OwnerId</field>
        <lookupValue>Global_CRM_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Route to Global CRM Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Route_to_Roadmap</fullName>
        <description>Route cases to the Roadmap queue when indicated as being on the roadmap through the type field if open or on hold</description>
        <field>OwnerId</field>
        <lookupValue>Salesforce_com_Roadmap</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Route to Roadmap</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Route_to_Salesforce_IT_Group</fullName>
        <field>OwnerId</field>
        <lookupValue>Global_Corporate_Systems_GCS</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Route to Salesforce IT Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Waiting_for_Response_date_reset</fullName>
        <description>Field Date status set to Waiting for Response is reset when case status is no longer Waiting for Response</description>
        <field>Date_status_set_to_Waiting_for_Response__c</field>
        <name>Case Waiting for Response date reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_auto_closed_incomplete</fullName>
        <description>Case status change to Closed - Incomplete</description>
        <field>Status</field>
        <literalValue>Closed - No Response</literalValue>
        <name>Case auto closed-incomplete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_owner_name_tracking_when_SA_approve</fullName>
        <description>Stores the user ID for case owner when status is set to SA approved for change requests</description>
        <field>Case_Owner_when_SA_approved__c</field>
        <formula>PRIORVALUE( OwnerId )</formula>
        <name>Case owner name tracking when SA approve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_set_record_type_to_Contract_Request</fullName>
        <description>Set record type Contract Request to provide full layout access</description>
        <field>RecordTypeId</field>
        <lookupValue>CSDA_Contract_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case set record type to Contract Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_status_to_new</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Case status to new</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_RT_to_Serasa_Support_Request</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Serasa_Support_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change RT to Serasa Support Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case</fullName>
        <field>Status</field>
        <literalValue>Closed - Complete</literalValue>
        <name>Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case_Automatically</fullName>
        <field>Status</field>
        <literalValue>Closed No Action</literalValue>
        <name>Close Case Automatically</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Soft_closed_cases</fullName>
        <field>Status</field>
        <literalValue>Closed - Complete</literalValue>
        <name>Close Soft closed cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DA_Community_Case_Public_Comment_Sent</fullName>
        <field>Public_Comment_Added__c</field>
        <literalValue>0</literalValue>
        <name>DA Community Case Public Comment Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_status_set_to_Waiting_for_Response</fullName>
        <description>Stamps the date the case status is set to Waiting for Response</description>
        <field>Date_status_set_to_Waiting_for_Response__c</field>
        <formula>TODAY()</formula>
        <name>Date status set to Waiting for Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Demand_Generation_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>Demand_Generation</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Demand Generation Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Support_region_field_update_APAC</fullName>
        <description>Update EDQ Support region to APAC</description>
        <field>EDQ_Support_Region__c</field>
        <literalValue>APAC</literalValue>
        <name>EDQ Support region field update APAC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Support_region_field_update_EMEA</fullName>
        <description>Update EDQ Support region field to EMEA</description>
        <field>EDQ_Support_Region__c</field>
        <literalValue>EMEA</literalValue>
        <name>EDQ Support region field update EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Support_region_field_update_NA</fullName>
        <description>Update EDQ Support region field to NA</description>
        <field>EDQ_Support_Region__c</field>
        <literalValue>NA</literalValue>
        <name>EDQ Support region field update NA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Support_region_field_update_UK</fullName>
        <description>EDQ Support region update to &quot;UK&quot;</description>
        <field>EDQ_Support_Region__c</field>
        <literalValue>UK</literalValue>
        <name>EDQ Support region field update UK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Update_Case_Field_Platform_to_CCMP</fullName>
        <field>Platform__c</field>
        <literalValue>CCMP</literalValue>
        <name>EMS - Update Case Field Platform to CCMP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Update_Case_Field_Platform_to_CMCS</fullName>
        <field>Platform__c</field>
        <literalValue>CM/CS</literalValue>
        <name>EMS - Update Case Field Platform to CMCS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EMS_Update_Case_Field_Platform_to_EDQ</fullName>
        <field>Platform__c</field>
        <literalValue>EDQ</literalValue>
        <name>EMS - Update Case Field Platform to EDQ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ESDEL_Spain_Task_Owner</fullName>
        <description>Spain Delivery Workstream - Assign Task owner to queue</description>
        <field>OwnerId</field>
        <lookupValue>ESDEL_Spain_Delivery_Project_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Spain Task Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ESDEL_Spain_Tickety_Due_Date</fullName>
        <description>Spain Delivery Workstream - set ticket due date based on project tier</description>
        <field>ESDEL_Due_Date__c</field>
        <formula>IF(ISPICKVAL( ESDEL_Project__r.ESDEL_Project_Tier__c , &quot;Black&quot;), CreatedDate + 1, 
IF(ISPICKVAL( ESDEL_Project__r.ESDEL_Project_Tier__c , &quot;Blue&quot;), CreatedDate + 2,
IF(ISPICKVAL( ESDEL_Project__r.ESDEL_Project_Tier__c , &quot;White&quot;), CreatedDate + 3,
CreatedDate + 3)))</formula>
        <name>Spain Tickety Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ESDEL_Spain_Update_Billing_Date</fullName>
        <description>Spain Delivery Workstream - Update Billing Date if previously NULL</description>
        <field>ESDEL_Send_to_Billing_Date__c</field>
        <formula>TODAY()</formula>
        <name>Spain Update Billing Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalate_Case</fullName>
        <description>Case 01794099</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Escalate Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NA_Automotive_SFDC_update_Case_Reason</fullName>
        <description>Update Case Reason for automotive cases created from email-to-case autosfdcteam@experian.com</description>
        <field>Reason</field>
        <literalValue>CS Team Request</literalValue>
        <name>NA Automotive SFDC - update Case Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NA_Automotive_SFDC_update_Case_Type</fullName>
        <description>Update case type for automotive cases created from email-to-case autosfdcteam@experian.com</description>
        <field>Type</field>
        <literalValue>Salesforce Internal</literalValue>
        <name>NA Automotive SFDC - update Case Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NA_EDQ_CCB</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_EDQ_CCB</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NA EDQ CCB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reason_is_Acct_Mgt_for_Auto_VEX</fullName>
        <description>Case Reason is set to Account Management for Automotive VEX Membership cases</description>
        <field>Reason</field>
        <literalValue>Account Management</literalValue>
        <name>Reason is Acct Mgt for Auto VEX</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Case_Owner_Update_for_Invoice_Dis</fullName>
        <field>OwnerId</field>
        <lookupValue>Serasa_Cases_Atendimento_VIP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Serasa Case Owner Update for Invoice Dis</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Manager_Email_Update</fullName>
        <field>Serasa_CaseOwner_Manager_Email__c</field>
        <formula>Owner:User.Managers_Email__c</formula>
        <name>Serasa Manager Email Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Update_Case_Status</fullName>
        <field>Status</field>
        <literalValue>Closed - Complete</literalValue>
        <name>Serasa Update Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_Update_IsEscalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Serasa Update IsEscalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ServiceNow_Case_Owner_Change</fullName>
        <description>Change service now case owner to 00Gi0000005EiMK - Queue. This is a safenet for API user</description>
        <field>OwnerId</field>
        <lookupValue>ServiceNow_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ServiceNow Case Owner Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Owner_DQ_Team</fullName>
        <field>OwnerId</field>
        <lookupValue>DQ_Team_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Case Owner DQ Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_to_Closed</fullName>
        <description>Set the Case Status to Closed after 7 days since the case was set to On Hold- Pending no response.</description>
        <field>Status</field>
        <literalValue>Closed - No Response</literalValue>
        <name>Set Case Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_to_Open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Set Case to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Checkbox_to_Indicate_auto_closed</fullName>
        <field>Closed_by_System_No_Response__c</field>
        <literalValue>1</literalValue>
        <name>Set Checkbox to Indicate auto closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Implemented_Date_on_Cases</fullName>
        <description>Set Implemented Date on Cases when a Case Status is set to Implemented</description>
        <field>Implemented_Date__c</field>
        <formula>Now()</formula>
        <name>Set Implemented Date on Cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Response_Date</fullName>
        <description>Set initial response date on cases when status initially changes from New</description>
        <field>Initial_Response_Date__c</field>
        <formula>Now()</formula>
        <name>Set Initial Response Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Type_is_Service_Request_for_Auto_VEX</fullName>
        <description>Case Type field is set to Service Request for Automotive VEX membership cases</description>
        <field>Type</field>
        <literalValue>Service Request</literalValue>
        <name>Type is Service Request for Auto VEX</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Has_New_Case_Comment</fullName>
        <description>Unchecks &apos;Has New Case Comment?&apos; field on a case after the email notification is sent to the case owner or the customer case contact</description>
        <field>Has_New_Case_Comment__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Has New Case Comment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateCaseStatusToInProgress</fullName>
        <field>Status</field>
        <literalValue>In progress</literalValue>
        <name>UpdateCaseStatusToInProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateClosed_by_System_No_Response</fullName>
        <description>To indicate this case was closed by system</description>
        <field>Closed_by_System_No_Response__c</field>
        <literalValue>1</literalValue>
        <name>UpdateClosed by System - No Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CNPJ_Number_on_Case</fullName>
        <description>Updates the Field Account CNPJ on the case.</description>
        <field>Account_CNPJ__c</field>
        <formula>Account.CNPJ_Number__c</formula>
        <name>Update CNPJ Number on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CPF_Number_on_Case</fullName>
        <description>Updates the Field Contact CPF on the case from the Contact Populated on the Case</description>
        <field>Contact_CPF__c</field>
        <formula>Contact.CPF__c</formula>
        <name>Update CPF Number on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_CR_Owner_Field</fullName>
        <description>Set Case Owner on CSDA Contract Request when Submit is set to True</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Contract_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case CR Owner Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Recording_Time</fullName>
        <field>Recording_Time__c</field>
        <formula>0</formula>
        <name>Update Case Recording Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status</fullName>
        <field>Status</field>
        <literalValue>Closed Not Resolved</literalValue>
        <name>Update Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Field_to_Not_Started</fullName>
        <description>Set Case Status to &quot;Not Started&quot; on CSDA Contract Request when Submit is set to True</description>
        <field>Status</field>
        <literalValue>Not Started</literalValue>
        <name>Update Case Status Field to Not Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Unresolved</fullName>
        <field>Status</field>
        <literalValue>Closed Not Resolved</literalValue>
        <name>Update Case Status Unresolved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Unresolved_for_French</fullName>
        <field>Status</field>
        <literalValue>Closed Not Resolved</literalValue>
        <name>Update Case Status Unresolved for French</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_New</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Update Case Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Submitted_Date</fullName>
        <description>Set Submitted Date on CSDA Contract Request when Submit__c is set to True</description>
        <field>Submitted_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Case Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Implementation_Status</fullName>
        <description>Update the Implementation Status to &quot;Pending Assessment&quot; when status changed to &quot;CCB Approved&quot;</description>
        <field>Implementation_Status__c</field>
        <literalValue>Pending Assessment</literalValue>
        <name>Update Implementation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Negotiation_Complete_Date</fullName>
        <description>Update Negotiation Complete date when Status changed to Negotiation Complete - Pending Client</description>
        <field>Negotiation_Complete_Date__c</field>
        <formula>Now()</formula>
        <name>Update Negotiation Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Message_Recieved_False</fullName>
        <field>New_Message_Received__c</field>
        <literalValue>0</literalValue>
        <name>Update New Message Recieved False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Non_Sale_CR_Owner_field</fullName>
        <description>Set Case Owner on CSDA Non-Sale Contract Request when Submit is set to True</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_Contract_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Non Sale CR Owner field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Change_Time_Stamp</fullName>
        <field>Owner_Change_Time_Stamp__c</field>
        <formula>NOW()</formula>
        <name>Update Owner Change Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_case_status_owner_Level_2_Support</fullName>
        <description>If the owner is changed manually to level 2 support, Update status to &apos;Pending Level 2 Support&apos;.</description>
        <field>Status</field>
        <literalValue>Pending Level 2 Support</literalValue>
        <name>Update case status,owner=Level 2 Support</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_to_Pending_BA_Approval</fullName>
        <field>OwnerId</field>
        <lookupValue>Cases_pending_BA_approval</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update owner to Pending BA Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Initial_Term_Period_End_Date</fullName>
        <description>Update the Initial Term Period End Date</description>
        <field>Initial_Term_Period_End_Date__c</field>
        <formula>DATE( 
year(Contract_Effective_Date__c ) + floor((month(Contract_Effective_Date__c ) + Initial_Term_Period__c)/12) + if(and(month(Contract_Effective_Date__c )=12,Initial_Term_Period__c&gt;=12),-1,0), 
if( mod( month(Contract_Effective_Date__c ) + Initial_Term_Period__c, 12 ) = 0, 12 , mod( month(Contract_Effective_Date__c ) + Initial_Term_Period__c, 12 ) 
), 
min( 
day(Contract_Effective_Date__c ), 
case( 
max( mod( month(Contract_Effective_Date__c ) + Initial_Term_Period__c, 12 ) , 1), 
9,30, 
4,30, 
6,30, 
11,30, 
2, 
if(mod((year(Contract_Effective_Date__c ) 
+ floor((month(Contract_Effective_Date__c ) + Initial_Term_Period__c)/12) + if(and(month(Contract_Effective_Date__c )=12,Initial_Term_Period__c&gt;=12),-1,0)),4)=0,29,28), 
31 
) 
) 
)</formula>
        <name>Update the Initial_Term_Period_End_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updating_IsCreatorOwner_to_True_on_Case</fullName>
        <field>IsCreatorOwner__c</field>
        <literalValue>0</literalValue>
        <name>Updating IsCreatorOwner to True on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X2nd_reason_VX_Membership_for_Auto_VEX</fullName>
        <description>Secondary Case Reason is set to VX Membership for Automotive VEX cases</description>
        <field>Secondary_Case_Reason__c</field>
        <literalValue>VX Membership</literalValue>
        <name>2nd reason VX Membership for Auto VEX</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>APAC EDQ Case Auto Response</fullName>
        <actions>
            <name>APAC_EDQ_Case_Auto_Response</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>qassupport@au.experian.com</value>
        </criteriaItems>
        <description>APAC EDQ: auto-reply to customers who email qassupport@au.experian.com</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Access Request Granted</fullName>
        <actions>
            <name>Access_Request_Granted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Access Request Granted</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; RecordType.Name =&apos;Account Access Request&apos; &amp;&amp; ISPICKVAL(Status,&apos;Closed - Complete&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Access Request to EMEA %26 UK%26I SE</fullName>
        <actions>
            <name>Assign_to_Sales_Effectiveness_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Access Request Routing for UK&amp;I, EMEA, APAC and region not specified</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  RecordType.DeveloperName =&apos;Access_Request&apos; &amp;&amp;  ( ISPICKVAL(Requestor__r.Region__c,&apos;UK&amp;I&apos;)|| ISPICKVAL(Requestor__r.Region__c,&apos;EMEA&apos;)|| ISPICKVAL(Requestor__r.Region__c,&apos;APAC&apos;)|| ISBLANK(TEXT(Requestor__r.Region__c)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Access Request to Global CRM Support</fullName>
        <actions>
            <name>Assign_to_Global_CRM_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Access Request Routing for all regions excluding Serasa and APAC or where region not specified</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)  &amp;&amp;  RecordType.DeveloperName =&apos;Account_Access_Request&apos;  &amp;&amp;   ( ISPICKVAL(Requestor__r.Region__c,&apos;UK&amp;I&apos;)|| ISPICKVAL(Requestor__r.Region__c,&apos;EMEA&apos;)||  ISPICKVAL(Requestor__r.Region__c,&apos;North America&apos;)||  ISBLANK(TEXT(Requestor__r.Region__c)) ) &amp;&amp; NOT(ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA CS CIS&apos;)) &amp;&amp; NOT(ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA CS BIS&apos;)) &amp;&amp; NOT(ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA DA&apos;))&amp;&amp; NOT(ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA DA Fraud and Identity&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Access Request to NA SE</fullName>
        <actions>
            <name>Email_Sales_Effectiveness_NA</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_NA_SE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Access request routing for North America</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  RecordType.DeveloperName =&apos;Access_Request&apos; &amp;&amp; ( ISPICKVAL(Requestor__r.Region__c,&apos;North America&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Access Request to Serasa</fullName>
        <actions>
            <name>Email_Latin_America_Access_Request_Received</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_Serasa_CRM_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Access Request Routing for Serasa</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; RecordType.DeveloperName =&apos;Access_Request&apos; &amp;&amp; ( ISPICKVAL(Requestor__r.Region__c,&apos;Latin America&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assigment by region EMEA</fullName>
        <actions>
            <name>Assign_to_Sales_Effectiveness_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assignment to Sales Effectiveness UK&amp;I based in Account region</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISPICKVAL( Reason ,&quot;DQ Research Request&quot;),  $Profile.Name =  $Label.PROFILE_EXPERIAN_DQ_ADMIN, OR( TEXT( Account__r.Region__c )= &quot;APAC&quot;, TEXT( Account__r.Region__c )= &quot;EMEA&quot;, TEXT( Account__r.Region__c )= &quot;UK&amp;I&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assigment by region NA</fullName>
        <actions>
            <name>Assign_to_NA_SE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assignment to Sales Effectiveness NA based in Account region</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISPICKVAL( Reason ,&quot;DQ Research Request&quot;),  $Profile.Name =  $Label.PROFILE_EXPERIAN_DQ_ADMIN, OR( TEXT( Account__r.Region__c )= &quot;North America&quot;, TEXT( Account__r.Region__c )= &quot;Latin America&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign to DQ Team</fullName>
        <actions>
            <name>Email_DQ_Team_New_Case</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Case_Owner_DQ_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Case_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  OR(RecordType.DeveloperName =&apos;New_Competitor&apos;, ISPICKVAL(Reason, &apos;Data Quality&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to DQ Team - Competitor Request</fullName>
        <actions>
            <name>Email_DQ_Team_Competitor_Request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Case_Owner_DQ_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Case_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to change the owner to the DQ Team queue for &apos;New Competitor&apos; case record types</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  RecordType.DeveloperName =&apos;New_Competitor&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to DQ Team - Data Quality</fullName>
        <actions>
            <name>Email_DQ_Team_Data_Quality_Request</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Case_Owner_DQ_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Case_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to transfer Salesforce.com Support cases to the global data quality team when selected options are chosen.</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;     (  ( ISPICKVAL( Secondary_Case_Reason__c , &apos;Data quality&apos;)       || ISPICKVAL(Secondary_Case_Reason__c, &apos;Data updates &amp; de-duplication&apos;)       || ISPICKVAL(Secondary_Case_Reason__c, &apos;Delete duplicate contracts / confidential info&apos;)      || ISPICKVAL(Secondary_Case_Reason__c, &apos;Transfer contracts / confidential info&apos;)       || ISPICKVAL(Secondary_Case_Reason__c, &apos;Contract name amend&apos;)      || ISPICKVAL(Secondary_Case_Reason__c, &apos;Assignment team modification / new hire&apos;)  )      || (RecordType.DeveloperName =&apos;Account_Access_Request&apos; &amp;&amp;   (ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA CS CIS&apos;)||ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA CS BIS&apos;)||ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA DA&apos;) || ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA DA Fraud and Identity&apos;)))   || ((ISPICKVAL( Secondary_Case_Reason__c , &apos;Access Request&apos;)||ISPICKVAL( Secondary_Case_Reason__c , &apos;Amend opp team / ownership&apos;)||ISPICKVAL( Secondary_Case_Reason__c , &apos;Delete duplicate opportunity&apos;)) &amp;&amp;  (ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA CS CIS&apos;)|| ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA CS BIS&apos;)|| ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA DA&apos;) || ISPICKVAL(Requestor__r.Business_Unit__c,&apos;NA DA Fraud and Identity&apos;) ))  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assigned to GCS</fullName>
        <actions>
            <name>Assigned_to_GCS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Global Corporate Systems (GCS)</value>
        </criteriaItems>
        <description>Stamps the date/time when a case is assigned to the GCS team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assigned to GCS Member</fullName>
        <actions>
            <name>Assigned_to_GCS_Member</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamps the date/time when a case is assigned to the GCS team</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),   OwnerId  &lt;&gt;   $Setup.Experian_Global__c.GCS_Queue_ID__c,  LEN( TEXT(Assigned_Date_to_GCS__c) ) &gt; 0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automotive - Case Comment Notification</fullName>
        <actions>
            <name>Automotive_Case_Comment_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Auto_Uncheck_Has_New_Case_Comment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )  &amp;&amp; RecordType.DeveloperName == &apos;Automotive&apos; &amp;&amp; Has_New_Case_Comment__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automotive AC Hosting New Case Notification</fullName>
        <actions>
            <name>Auto_AC_Hosting_New_Case_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>AC Hosting Email notification</description>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISPICKVAL(Origin, &apos;Email - AC Hosting&apos;), Owner:Queue.QueueName  = &apos;AutoCheck Hosting&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Case Closure Contact Us Members Origin</fullName>
        <actions>
            <name>Close_Case_Automatically</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateClosed_by_System_No_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web-Auto-Contact Us Members</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Industry__c</field>
            <operation>equals</operation>
            <value>Dealer</value>
        </criteriaItems>
        <description>If a case comes from our &quot;Contact Us - Members&quot; origin, and in the form client selects &quot;Dealer&quot; as their Industry, the Case should be closed and a checkbox checked to keep records that was closed by this automation and not manually.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Case On Hold to Closed No Response</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold - Pending Customer Response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>AutoCheck</value>
        </criteriaItems>
        <description>If an AutoCheck case has been in a Status &quot;On Hold – Pending Customer Response&quot; after 7 days, the Case should be closed and a checkbox checked to keep records that was closed by this automation and not manually</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Case_Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Checkbox_to_Indicate_auto_closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Automotive Chrysler Case Notification</fullName>
        <actions>
            <name>AutoMotive_Email_Alert_For_Chrysler_Cases</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email alert to Velocity Team</description>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISPICKVAL(Origin, &apos;Email - Velocity&apos;), OR( CONTAINS(SuppliedEmail,&quot;@chrysler.com&quot;), CONTAINS(SuppliedEmail,&quot;@fcagroup.com&quot;), CONTAINS(SuppliedEmail,&quot;@maseratiusa.com&quot;), CONTAINS(SuppliedEmail,&quot;@fiatusa.com&quot;) ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Email Alert on Case Status Change to Escalated to Supervisor</fullName>
        <actions>
            <name>Email_Alert_for_Escalated_to_Supervisor_status_Change</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Owner_AutoCheck_Escalated_Cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Whenever a CSR changes the Case Status to &quot;Escalated&quot; or &quot;Escalated to Team Lead&quot; there is an Email alert that should be trigger to a group of users (queue) and the case should be re-assigned to the AutoCheck Escalated Cases</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  OR (ISPICKVAL(Status,&apos;Escalate&apos;),ISPICKVAL(Status,&apos;Escalated to Team Lead&apos;)), RecordType.DeveloperName = &apos;Automotive&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Email Alert to Auto Cust Support Legal Dept</fullName>
        <actions>
            <name>Automotive_Legal_Case_Type_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If a case is related to any of the following Category values: “Attorney General, Legal, BBB Inquiry&quot;  an email alert should be triggered to the legal responsible person from  Automtotive customer support</description>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), OR(ISPICKVAL(Secondary_Case_Reason__c, &apos;Attorney General&apos;),  ISPICKVAL(Secondary_Case_Reason__c, &apos;BBB&apos;),  ISPICKVAL(Secondary_Case_Reason__c, &apos;Legal&apos;) ),  RecordTypeId =&quot;012i0000001QL8n&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Email and Activity for Autocheck Cancell Request</fullName>
        <actions>
            <name>Auto_Email_Alert_For_Case_Owner_Notification_For_Account_Changes</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Inactive_Account_Not_Billed</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Update_Account_Segment_Field_Account_Inactive_Reason</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Whenever a case is set as an AutoCheck cancellation based on its Sub Category value, and the &quot;Account Inactive reason&quot; field is null, there are two tasks that should be generated to update this Account field immediately and to set the Account in the AutoC</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), 
   Case_Owned_by_Queue__c =FALSE, 
   ISPICKVAL(Type, &apos;AutoCheck&apos;), 
   OR(ISPICKVAL(Sub_Category__c , &apos;Lost&apos;), 
      ISPICKVAL(Sub_Category__c , &apos;Lost - Price Increase&apos;)) , 
     PRIORVALUE( OwnerId ) =&apos;00Gi0000004wEzI&apos;, 
     NOT(OwnerId =&apos;00Gi0000004wEzI&apos;), 
   Account.Name &lt;&gt; &apos;RED MOUNTAIN TECHNOLOGIES&apos; 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Onboarding Tasks</fullName>
        <actions>
            <name>Intro_to_corporate_office_assign_to_AE_if_5_location</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Onboarding_Confirm_Account_Details_send_email_3</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_10_Onboarding_Check_in</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_1_Onboarding_Account_Fastlink_Overview_send_email_1</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_2_Onboarding_Usage_Fastlink_send_email_2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_3_Onboarding_Service_Data_Marketing_Materials</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_5_Credit_Based_Marketing_Email_4</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_6_Onboarding_Check_in_assign_to_AE_if_5_location_send_email_5</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_7_Measurement_Analysis_Email_send_email_6</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Week_8_Onboarding_Check_in_assign_to_AE_if_5_location_send_email_7</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>IF( AND(          ISBLANK( Owner:Queue.Id ) ,            RecordType.Name = &quot;Automotive&quot;,           Text( Secondary_Case_Reason__c ) = &quot;Onboarding&quot;         ), 	true,  	false   )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Populate Auto Type AutoCount</fullName>
        <actions>
            <name>Auto_Update_the_Type_with_AutoCount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - AutoCount</value>
        </criteriaItems>
        <description>Automatically Populate auto Type field depending on the Case Origin = Email - AutoCount</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Populate Auto Type with AutoCheck</fullName>
        <actions>
            <name>Auto_Update_the_Type_with_Autocheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - AC Hosting,Email - AutoCheck,Email - AutoCheck Service Data,Email - CheckMyRide,Email - Contracts,Email - Drivetime,Email - RMT,Web-Auto-Contact Us Consumer,Web-Auto-Contact Us Members</value>
        </criteriaItems>
        <description>Automatically Populate auto Type field depending on the Case Origin = Email - AutoCheck</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Populate Auto Type with AutoSupport</fullName>
        <actions>
            <name>Auto_Update_the_Case_Type_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - AutoSupport</value>
        </criteriaItems>
        <description>Automatically Populate auto Type field depending on the Case Origin = Email - AutoSupport</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automotive Populate Auto Type with Velocity</fullName>
        <actions>
            <name>Auto_Update_Type_Field_with_Velocity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Velocity</value>
        </criteriaItems>
        <description>Automatically Populate auto Type field depending on the Case Origin = Email - Velocity</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Automotive SalesRep Email Alert on Grade AB</fullName>
        <actions>
            <name>AutoMotive_Email_Alert_for_Sales_Rep_when_GRADE_is_A_OR_B</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>If a case is created with one of following secondary Case reas: &quot; Contract end date inquiry, Other contract related, Pricing inquiry, Usage inquiry&quot; and is related to an account record with an Account Grade equals to &quot;A&quot; or &quot;B&quot; an  email alert should be t</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), OR( ISPICKVAL(Sub_Category__c,&apos;Contract end date inquiry&apos;), ISPICKVAL(Sub_Category__c,&apos;Pricing inquiry&apos;), ISPICKVAL(Sub_Category__c,&apos;Usage inquiry&apos;) ), OR( Account_Grade__c  = &apos;A&apos;, Account_Grade__c  = &apos;B&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Automotive VEX Membership</fullName>
        <actions>
            <name>Reason_is_Acct_Mgt_for_Auto_VEX</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Type_is_Service_Request_for_Auto_VEX</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>X2nd_reason_VX_Membership_for_Auto_VEX</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>vexmembership@experian.com</value>
        </criteriaItems>
        <description>Populates Case Type, Case Reason, Secondary Case Reason when case sub origin is vexmembership@experian.com</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>C%26D to APAC SE</fullName>
        <actions>
            <name>Assign_to_APAC_SE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Routes a cancellation / downgrade request to APAC sales effectiveness queue</description>
        <formula>Requester_Region__c  = &apos;APAC&apos; &amp;&amp;  RecordType.Name = &apos;Cancellation/Downgrade Request&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>C%26D to Global CRM Support</fullName>
        <actions>
            <name>Assign_to_Global_CRM_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Routes a cancellation / downgrade request to Global CRM Support queue for All Regions, Excluding Serasa or APAC or any where the requestors region is blank</description>
        <formula>( Requester_Region__c  = &apos;EMEA&apos;|| Requester_Region__c = &quot;North America&quot; || Requester_Region__c  = &apos;UK&amp;I&apos; || ISBLANK(Requester_Region__c ) )  &amp;&amp;  RecordType.Name = &apos;Cancellation/Downgrade Request&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>C%26D to NA SE</fullName>
        <actions>
            <name>Assign_to_NA_SE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Routes a cancellation / downgrade request to NA sales effectiveness queue for LATAM and NA</description>
        <formula>( Requester_Region__c  = &apos;Latin America&apos;|| Requester_Region__c  = &apos;North America&apos; )  &amp;&amp;  RecordType.Name = &apos;Cancellation/Downgrade Request&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>C%26D to UK%26I SE</fullName>
        <actions>
            <name>Assign_to_Sales_Effectiveness_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Routes a cancellation / downgrade request to UK&amp;I sales effectiveness queue for UK&amp;I Region, EMEA or any where the requestors region is blank</description>
        <formula>( Requester_Region__c  = &apos;EMEA&apos; || Requester_Region__c  = &apos;UK&amp;I&apos; || ISBLANK(Requester_Region__c ) )  &amp;&amp;  RecordType.Name = &apos;Cancellation/Downgrade Request&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CASE - Set Initial Response Date</fullName>
        <actions>
            <name>Set_Initial_Response_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Initial Response Date when the case status changes from &quot;New&quot; to another value</description>
        <formula>OR(AND(ISCHANGED( Status ), ISPICKVAL(PRIORVALUE(Status),&quot;New&quot;),  ISNULL( Initial_Response_Date__c ) ), AND( ISNEW(), NOT(ISPICKVAL(PRIORVALUE(Status),&quot;New&quot;) )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA CR Negotiation Date to Blank</fullName>
        <actions>
            <name>CSDA_CR_Negotiation_Date_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the Case.Status is changed from value = &apos;Negotiation Complete - Pending Client&apos; to any other value, will set the Case.Negotiation_Complete_Date__c to blank value</description>
        <formula>AND (   ISCHANGED( Status ),   ISPICKVAL(     PRIORVALUE(Status ), &quot;Negotiation Complete - Pending Client&quot;   ),   NOT(ISPICKVAL(Status, &quot;Contract Filed&quot;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Contract Request Submit Set to True</fullName>
        <actions>
            <name>Update_Case_CR_Owner_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Status_Field_to_Not_Started</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA Contract Request - Update Submitted Date if Submit is checked</description>
        <formula>Submit__c = true &amp;&amp;   RecordType.DeveloperName == &apos;CSDA_Contract_Request&apos; &amp;&amp;   OR (     Opportunity__c != null,     AND(       Opportunity__c == null,       ISPICKVAL($User.Sales_Sub_Team__c, &apos;Automotive&apos;)     )   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Non-Sale Contract Request Submit Set to True</fullName>
        <actions>
            <name>Update_Case_Status_Field_to_Not_Started</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Non_Sale_CR_Owner_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CSDA Non-Sale Contract Request - Update Submitted Date and Owner if Submit is checked</description>
        <formula>Submit__c = true &amp;&amp;   RecordType.DeveloperName = &apos;CSDA_Contract_Request&apos; &amp;&amp;   AND(     Opportunity__c == null,     NOT(ISPICKVAL($User.Sales_Sub_Team__c, &apos;Automotive&apos;))   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Online Price Change Request default Subject</fullName>
        <actions>
            <name>CSDA_Online_Price_Change_Request_default</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CSDA Online Price Change Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Internal Routing to Sales Effectiveness%28old rule dont use%29</fullName>
        <actions>
            <name>Case_Routing_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Assign_internal_CRM_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Data Quality</value>
        </criteriaItems>
        <description>Assign Salesforce.com Support cases (type = Internal) to Sales Effectiveness queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Assigned to CSDA CIS Sales Support</fullName>
        <actions>
            <name>Case_Assigned_to_CSDA_CIS_Sales_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>CSDA Sales Support,CSDA Sales Support Primary,CSDA BIS Preferred support,CSDA BIS Inside Sales Support,CSDA BIS Batch Non BIQ Support,CSDA BIS Reseller Support,CSDA BA Quest or Archive,CSDA BA Prescreen or Prospect Triggers</value>
        </criteriaItems>
        <description>Case : 01253564 Assign Cases to CSDA CIS Sales Support</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case EDQ Support Region Update APAC</fullName>
        <actions>
            <name>EDQ_Support_region_field_update_APAC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>APAC MS EDQ - Support Tier 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EDQ_Support_Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Case RT EDQ Technical support. 
A field update on EDQ Support Region = APAC, when Owner is Queue = APAC MS EDQ - Support Tier 1</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case EDQ Support Region Update EMEA</fullName>
        <actions>
            <name>EDQ_Support_region_field_update_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>France Level 1 EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EDQ_Support_Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Case RT EDQ Technical support. 
A field update on EDQ Support Region = EMEA, when Owner is Queue = France Level 1 EDQ</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case EDQ Support Region Update NA</fullName>
        <actions>
            <name>EDQ_Support_region_field_update_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>NA MS EDQ - Support Tier 1,NA CS EDQ - Concierge Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EDQ_Support_Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Case RT EDQ Technical support. 
A field update on EDQ Support Region = NA, when Owner is Queue = NA MS EDQ - Support Tier 1, NA CS EDQ - Concierge Support</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case EDQ Support Region Update UK</fullName>
        <actions>
            <name>EDQ_Support_region_field_update_UK</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>UK&amp;I MS EDQ - Support Tier 1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.EDQ_Support_Region__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Case RT EDQ Technical support. 
A field update on EDQ Support Region = UK, when Owner is Queue = UK&amp;I MS EDQ - Support Tier 1</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Island Resolved</fullName>
        <actions>
            <name>Case_Island_Resolved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>User Island will be Captured on Case Close.</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  IsClosed = True,  OR(  RecordType.DeveloperName = &quot;Serasa_Cancellation&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request_Internal&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Notification on Ready for UAT</fullName>
        <actions>
            <name>Ready_For_UAT_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This will send a notification when Case Implementation status chaged to Ready for UAT.</description>
        <formula>AND (  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  RecordType.DeveloperName = &apos;SFDC_Support&apos;,  ISPICKVAL( Implementation_Status__c, &quot;Ready for UAT&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Provide access to full Contract Request fields</fullName>
        <actions>
            <name>Case_set_record_type_to_Contract_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CSDA Contract Request.</value>
        </criteriaItems>
        <description>Changes record type to provide full layout access to Contract Requests</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Routing to Global CRM Support for Platform Login Issues</fullName>
        <actions>
            <name>Case_Routing_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Route_to_Global_CRM_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Secondary_Case_Reason__c</field>
            <operation>equals</operation>
            <value>Login/Access Issues</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Platform Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>HR - Recognition</value>
        </criteriaItems>
        <description>Workflow to transfer  Platform support cases record types to the GSO team for Login issues</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Routing to Global Corporate Systems %28GCS%29</fullName>
        <actions>
            <name>Case_Routing_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Route_to_Salesforce_IT_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4 AND (5 OR 6)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Platform Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>GCSS Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>HR - Recognition</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Secondary_Case_Reason__c</field>
            <operation>notEqual</operation>
            <value>Login/Access Issues</value>
        </criteriaItems>
        <description>Workflow to transfer Salesforce.com and Platform (Non-Core) support case record types to the GCSS team upon certain conditions</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Routing to Salesforce%2Ecom Roadmap</fullName>
        <actions>
            <name>Case_Route_to_Roadmap</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open,On Hold,CCB Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Roadmap</value>
        </criteriaItems>
        <description>Route cases automatically to the Roadmap queue when the type is set to Roadmap and the status is open or on hold for Salesforce.com Support case record types</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case owner change when status SA approved</fullName>
        <actions>
            <name>Case_owner_name_tracking_when_SA_approve</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_owner_to_Pending_BA_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>SA Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Secondary_Case_Reason__c</field>
            <operation>equals</operation>
            <value>Functionality change request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <description>PK 5/4/16 - Deactivated in favour of Process Builder process.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case waiting for response 14 days</fullName>
        <actions>
            <name>X14_Days_Case_Waiting_for_Response</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Time_Waiting_for_Response__c</field>
            <operation>equals</operation>
            <value>14</value>
        </criteriaItems>
        <description>If a Salesforce.com Support case has been waiting for a user response for 14 days, a reminder email is sent to this user and the case owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case waiting for response 28 days</fullName>
        <actions>
            <name>X28_Days_Case_Waiting_for_Response</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_auto_closed_incomplete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Time_Waiting_for_Response__c</field>
            <operation>equals</operation>
            <value>28</value>
        </criteriaItems>
        <description>If a Salesforce.com Support case has been waiting for a user response for 28 days, the case is automatically Closed - Incomplete and an email is sent to notify this user, the case requestor and the case owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close Soft closed cases</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved,Closed Pending Acceptance,Closed No Action,Closed Cancelled,Closed Not Resolved,Closed Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <description>Time based workflow to close cases when closed as Soft close</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_Soft_closed_cases</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Competitor Created</fullName>
        <actions>
            <name>Competitor_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email to confirm new competitor has been created upon closing a new competitor case record type</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)   &amp;&amp; RecordType.Name =&apos;New Competitor&apos;   &amp;&amp; ISPICKVAL(Status,&apos;Closed - Complete&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Closure %28Chinese Traditional%29</fullName>
        <actions>
            <name>DA_Community_Case_Closure_Chinese_Traditional_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Chinese (Traditional)</value>
        </criteriaItems>
        <description>DA Community Case Closure Notification (Chinese Traditional)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Closure %28Chinese%29</fullName>
        <actions>
            <name>DA_Community_Case_Closure_Chinese_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Chinese (Simplified)</value>
        </criteriaItems>
        <description>DA Community Case Closure Notification (Chinese Simplified)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Closure %28English%29</fullName>
        <actions>
            <name>DA_Community_Case_Closure_English_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>notEqual</operation>
            <value>Spanish,Chinese (Simplified),Chinese (Traditional),Portuguese (Brazilian),Portuguese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>DA Community Case Closure Notification (English)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Closure %28Portuguese%29</fullName>
        <actions>
            <name>DA_Community_Case_Closure_Portuguese_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Portuguese,Portuguese (Brazilian)</value>
        </criteriaItems>
        <description>DA Community Case Closure Notification (Portuguese)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Closure %28Spanish%29</fullName>
        <actions>
            <name>DA_Community_Case_Closure_Spanish_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <description>DA Community Case Closure Notification (Spanish)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Creation %28Chinese Traditional%29</fullName>
        <actions>
            <name>DA_Community_Case_Creation_Chinese_Traditional_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Chinese (Traditional)</value>
        </criteriaItems>
        <description>DA Community Case Creation Notification (Chinese Traditional)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Creation %28Chinese%29</fullName>
        <actions>
            <name>DA_Community_Case_Creation_Chinese_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Chinese (Simplified)</value>
        </criteriaItems>
        <description>DA Community Case Creation Notification (Chinese Simplified)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Creation %28English%29</fullName>
        <actions>
            <name>DA_Community_Case_Creation_English_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>notEqual</operation>
            <value>Spanish,Chinese (Simplified),Chinese (Traditional),Portuguese (Brazilian),Portuguese</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>DA Community Case Creation Notification (English)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Creation %28Portuguese%29</fullName>
        <actions>
            <name>DA_Community_Case_Creation_Portuguese_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Portuguese,Portuguese (Brazilian)</value>
        </criteriaItems>
        <description>DA Community Case Creation Notification (Portuguese)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Creation %28Spanish%29</fullName>
        <actions>
            <name>DA_Community_Case_Creation_Spanish_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <description>DA Community Case Creation Notification (Spanish)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Update %28Chinese Traditional%29</fullName>
        <actions>
            <name>DA_Community_Case_Update_Chinese_Traditional_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DA_Community_Case_Public_Comment_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>DA Community Case Update Notification (Chinese Traditional)</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Service_Central&apos;,       ISPICKVAL(Origin, &apos;Community&apos;),       ISPICKVAL(Contact.Language__c, &apos;Chinese (Traditional)&apos;),       OR(  AND(ISCHANGED(Status),                 OR(  ISPICKVAL(Status, &apos;In Progress&apos;),                     ISPICKVAL(Status, &apos;With Client&apos;)                  )                ),            AND(ISCHANGED( Public_Comment_Added__c ),                Public_Comment_Added__c=true               )          )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Update %28Chinese%29</fullName>
        <actions>
            <name>DA_Community_Case_Update_Chinese_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DA_Community_Case_Public_Comment_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>DA Community Case Update Notification (Chinese Simplified)</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Service_Central&apos;,       ISPICKVAL(Origin, &apos;Community&apos;),       ISPICKVAL(Contact.Language__c, &apos;Chinese (Simplified)&apos;),       OR(  AND(ISCHANGED(Status),                 OR(  ISPICKVAL(Status, &apos;In Progress&apos;),                     ISPICKVAL(Status, &apos;With Client&apos;)                  )                ),            AND(ISCHANGED( Public_Comment_Added__c ),                Public_Comment_Added__c=true               )          )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Update %28English%29</fullName>
        <actions>
            <name>DA_Community_Case_Update_English_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DA_Community_Case_Public_Comment_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>DA Community Case Update Notification (English)</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Service_Central&apos;,       ISPICKVAL(Origin, &apos;Community&apos;),       NOT(ISPICKVAL(Contact.Language__c, &apos;Spanish&apos;)), 	  NOT(ISPICKVAL(Contact.Language__c, &apos;Chinese (Simplified)&apos;)),           NOT(ISPICKVAL(Contact.Language__c, &apos;Chinese (Traditional)&apos;)), 	  NOT(ISPICKVAL(Contact.Language__c, &apos;Portuguese (Brazilian)&apos;)), 	  NOT(ISPICKVAL(Contact.Language__c, &apos;Portuguese&apos;)),       OR(  AND(ISCHANGED(Status),                 OR(  ISPICKVAL(Status, &apos;In Progress&apos;),                     ISPICKVAL(Status, &apos;With Client&apos;)                  )                ),            AND(ISCHANGED( Public_Comment_Added__c ),                Public_Comment_Added__c=true               )          )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Update %28Portuguese%29</fullName>
        <actions>
            <name>DA_Community_Case_Update_Portuguese_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DA_Community_Case_Public_Comment_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>DA Community Case Update Notification (Portuguese)</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Service_Central&apos;,       ISPICKVAL(Origin, &apos;Community&apos;),       OR(ISPICKVAL(Contact.Language__c, &apos;Portuguese&apos;),ISPICKVAL(Contact.Language__c, &apos;Portuguese (Brazilian)&apos;)),       OR(  AND(ISCHANGED(Status),                 OR(  ISPICKVAL(Status, &apos;In Progress&apos;),                     ISPICKVAL(Status, &apos;With Client&apos;)                  )                ),            AND(ISCHANGED( Public_Comment_Added__c ),                Public_Comment_Added__c=true               )          )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DA Community Case Update %28Spanish%29</fullName>
        <actions>
            <name>DA_Community_Case_Update_Spanish_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>DA_Community_Case_Public_Comment_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>DA Community Case Update Notification (Spanish)</description>
        <formula>AND(  RecordType.DeveloperName = &apos;Service_Central&apos;,       ISPICKVAL(Origin, &apos;Community&apos;),       ISPICKVAL(Contact.Language__c, &apos;Spanish&apos;),       OR(  AND(ISCHANGED(Status),                 OR(  ISPICKVAL(Status, &apos;In Progress&apos;),                     ISPICKVAL(Status, &apos;With Client&apos;)                  )                ),            AND(ISCHANGED( Public_Comment_Added__c ),                Public_Comment_Added__c=true               )          )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date reset when status changed from Waiting for Response</fullName>
        <actions>
            <name>Case_Response_pending_from_User_reset</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Waiting_for_Response_date_reset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Field Date status set to Waiting for Response is reset when case status changed from Waiting for Response on open Cases</description>
        <formula>AND(   (NOT( IsClosed = True)), ( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)),  RecordType.DeveloperName = &apos;SFDC_Support&apos;,  ISCHANGED( Status ),  ISPICKVAL( PRIORVALUE ( Status ), &apos;Waiting for Response&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Date status set to Waiting for Response</fullName>
        <actions>
            <name>Date_status_set_to_Waiting_for_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamps the date the case status is set to Waiting for Response</description>
        <formula>AND( ( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)), RecordType.DeveloperName = &apos;SFDC_Support&apos;, ISCHANGED( Status ), ISPICKVAL( Status , &apos;Waiting for Response&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Demand Generation Assignment</fullName>
        <actions>
            <name>Email_demand_generation_team_for_New_Case_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Demand_Generation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Demand_Generation_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Lead Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Secondary_Case_Reason__c</field>
            <operation>equals</operation>
            <value>Lead processing (Eloqua integration)</value>
        </criteriaItems>
        <description>Change the case owner to the Demand Generation Queue when a case is raised under certain reasons</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EDQ - Case Comment Notification</fullName>
        <actions>
            <name>EDQ_Technical_Support_Case_Comment_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Has_New_Case_Comment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow sends a notification to the case owner and case contact.  This workflow rule is applicable to cases with the &apos;EDQ Technical Support&apos; record type.</description>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )  &amp;&amp; RecordType.DeveloperName == &apos;EDQ_Technical_Support&apos; &amp;&amp; Has_New_Case_Comment__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EDQ Auto Response - Holiday Hours</fullName>
        <actions>
            <name>EDQ_Auto_Response_Holiday_Hours</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>startsWith</operation>
            <value>NA MS EDQ - Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>greaterOrEqual</operation>
            <value>7/2/2015 5:00 PM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>lessOrEqual</operation>
            <value>7/6/2015 5:00 AM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ Case Auto Response</fullName>
        <actions>
            <name>NA_MS_EDQ_Case_Auto_Response</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Auto Response rules will not fire due to Email2Case triggers, so this workflow is required.</description>
        <formula>AND(  Owner:Queue.QueueName = &apos;NA MS EDQ - Support Tier 1&apos;,  NOT(CONTAINS(Sub_Origin__c, &apos;edqconcierge&apos;)),  LastModifiedDate-0.0027777777777778 &lt; CreatedDate )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ GPD NOC case notification</fullName>
        <actions>
            <name>EDQ_GPD_NOC_case_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>EDQ GPD NOC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Deployment,Incident Notification</value>
        </criteriaItems>
        <description>Email sent to EDQ GPD NOC team when new case is received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ GPD NOC deployment notification</fullName>
        <actions>
            <name>EDQ_GPD_NOC_deployment_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>EDQ GPD NOC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Deployment</value>
        </criteriaItems>
        <description>Email sent to EDQ GPD NOC team when new deployment case is received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ GPD NOC incident P1 notification</fullName>
        <actions>
            <name>EDQ_GPD_Incident_P1_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>EDQ GPD NOC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1</value>
        </criteriaItems>
        <description>Email sent to EDQ GPD NOC team when new P1 incident is received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ NA case auto response to Concierge Elite customers</fullName>
        <actions>
            <name>EDQ_NA_Case_auto_response_for_Concierge_Elite</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>EDQConciergeElite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>edqconciergeelite</value>
        </criteriaItems>
        <description>Rule assigning EDQ Technical Support cases to NA EDQ Concierge queue when case is received, and sending an auto response to customers based on their support level agreement</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ NA case auto response to Concierge Premier customers</fullName>
        <actions>
            <name>EDQ_NA_Case_auto_response_for_Concierge_Premier</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>EDQConciergePremier</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>edqconciergepremier</value>
        </criteriaItems>
        <description>Rule assigning EDQ Technical Support cases to NA EDQ Concierge queue when case is received, and sending an auto response to customers based on their support level agreement</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ NA case auto response to Concierge Standard customers</fullName>
        <actions>
            <name>EDQ_NA_Case_auto_response_for_Concierge_Standard</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>EDQConciergeStandard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>edqconciergestandard</value>
        </criteriaItems>
        <description>Rule assigning EDQ Technical Support cases to NA EDQ Concierge queue when case is received, and sending an auto response to customers based on their support level agreement</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EDQ NA case reassigned to queue by CR analyst</fullName>
        <active>false</active>
        <description>Rule reassigning EDQ Technical Support cases to NA EDQ queue if owner is  in Costa Rica, when case status is changed to Transfer or Transfer Escalate</description>
        <formula>AND( 		RecordType.DeveloperName = &apos;EDQ_Technical_Support&apos;,			Owner:User.Country = &apos;Costa Rica&apos;, 				( 					ISPICKVAL(Status,&quot;Transfer&quot;) ||  					AND(			RecordType.DeveloperName = &apos;EDQ_Technical_Support&apos;,			Owner:User.Country = &apos;Costa Rica&apos;, 							ISPICKVAL(Status,&quot;Transfer Escalate&quot;)	) 				) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSCasesFromEmailorWebSetToNew</fullName>
        <actions>
            <name>Case_status_to_new</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email,Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailForMoreInformationOnCaseForEnglish</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>notEqual</operation>
            <value>French,German,Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_when_Case_is_On_Hold_for_3_days_for_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>More_Information_needed_for_Case</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EMSSendEmailForMoreInformationOnCaseForFrench</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>French</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_when_Case_is_On_Hold_for_3_days_for_French</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>More_Information_needed_for_Case</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EMSSendEmailForMoreInformationOnCaseForGermany</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>German</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_when_Case_is_On_Hold_for_3_days_for_German</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>More_Information_needed_for_Case</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EMSSendEmailForMoreInformationOnCaseForSpanish</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_when_Case_is_On_Hold_for_3_days_for_Spanish</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>More_Information_needed_for_Case</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EMSSendEmailSLASeverity</fullName>
        <actions>
            <name>EMSSendEmailSLASeverityEmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5) OR (6 AND 7) OR (8 AND 9) ) AND 10</booleanFilter>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Critical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Age__c</field>
            <operation>greaterThan</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Age__c</field>
            <operation>greaterThan</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Medium</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Age__c</field>
            <operation>greaterThan</operation>
            <value>48</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>Low</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Age__c</field>
            <operation>greaterThan</operation>
            <value>168</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>Email is sent if case is still open after red zone.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenCaseIsCreatedForEnglish</fullName>
        <actions>
            <name>EMS_Send_a_Email_when_case_is_created_for_English</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Case_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Email,Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_Language__c</field>
            <operation>notEqual</operation>
            <value>French,German,Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>when the case origin is Web or Email, send the email to the customer.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenCaseIsCreatedForFrench</fullName>
        <actions>
            <name>EMS_Send_a_Email_when_case_is_created_for_French</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Case_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Email,Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>French</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>when the case origin is Web or Email, send the email to the customer.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenCaseIsCreatedForGerman</fullName>
        <actions>
            <name>EMS_Send_a_Email_when_case_is_created_for_German</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Case_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Email,Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_Language__c</field>
            <operation>equals</operation>
            <value>German</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>when the case origin is Web or Email, send the email to the customer.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenCaseIsCreatedForSpanish</fullName>
        <actions>
            <name>EMS_Send_a_Email_when_case_is_created_for_Spanish</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Case_Status_to_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Email,Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <description>when the case origin is Web or Email, send the email to the customer.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenCaseIsCreatedWithoutContact</fullName>
        <actions>
            <name>Send_Email_for_Case_Is_Created_Without_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenCaseIsInProgress2HoursEnglish</fullName>
        <actions>
            <name>SendEmailWhenCaseIsInProgress2HoursEnglish</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2  AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.X2_Hours_In_In_Progress__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenIncidentIsClosedForEnglish</fullName>
        <actions>
            <name>Send_Email_when_Incident_Case_is_closed_for_English</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_send_for_Incident_Closed_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>notEqual</operation>
            <value>French,German,Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenIncidentIsClosedForFrench</fullName>
        <actions>
            <name>Send_Email_for_Incident_Closed_for_French</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_send_for_Incident_Closed_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>French</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenIncidentIsClosedForGerman</fullName>
        <actions>
            <name>Send_Email_for_Incident_Closed_for_German</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_send_for_Incident_Closed_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>German</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenIncidentIsClosedForParentCase</fullName>
        <actions>
            <name>Email_send_for_Incident_Closed_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSSendEmailWhenIncidentIsClosedForSpanish</fullName>
        <actions>
            <name>Send_Email_for_Incident_Closed_for_Spanish</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_send_for_Incident_Closed_Case</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseFieldPlatformToCCMPAccordingToSubOriginOfEmail2Case</fullName>
        <actions>
            <name>EMS_Update_Case_Field_Platform_to_CCMP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support@fr.ccmp.eu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support@de.ccmp.eu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support@es.ccmp.eu</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseFieldPlatformToCMCSAccordingToSubOriginOfEmail2Case</fullName>
        <actions>
            <name>EMS_Update_Case_Field_Platform_to_CMCS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 ) AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support@cheetahmail.fr</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support@cheetahmail.de</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support@cheetahmail.es</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseFieldPlatformToEDQAccordingToSubOriginOfEmail2Case</fullName>
        <actions>
            <name>EMS_Update_Case_Field_Platform_to_EDQ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>ems.support@experian.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>crmsupport@experian.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseRecordingTimeForOnHold</fullName>
        <actions>
            <name>Update_Case_Recording_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_New_Message_Recieved_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.New_Message_Received__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseStatusIfOwnerChangeToLevel2Support</fullName>
        <actions>
            <name>Update_case_status_owner_Level_2_Support</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the owner is changed manually to level 2  CM/CS support, Update status to &apos;Pending Level 2 Support&apos;.</description>
        <formula>AND( CONTAINS(Owner:Queue.QueueName, &apos;Level 2 CM/CS&apos;),  (RecordType.Name = &apos;EMS&apos;), (  ISPICKVAL(Status , &apos;In progress&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseStatusToInProgress</fullName>
        <actions>
            <name>UpdateCaseStatusToInProgress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;EMS&apos;,     ISPICKVAL(Status, &apos;New&apos;),     BEGINS(OwnerId, &apos;005&apos;) ,      BEGINS(PRIORVALUE(OwnerId), &apos;00G&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseStatusWhenStatusIsOnHoldFor7DaysForEnglish</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND ( 3 OR 4 )</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>notEqual</operation>
            <value>French,German,Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_for_Unresolved_Case_English</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Unresolved</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Close_Case_after_7_days_without_response</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseStatusWhenStatusIsOnHoldFor7DaysForFrench</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>French</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_for_Unresolved_Case_French</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Unresolved</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Close_Case_after_7_days_without_response</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseStatusWhenStatusIsOnHoldFor7DaysForGerman</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>German</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_for_Unresolved_Case_German</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Unresolved</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Close_Case_after_7_days_without_response</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EMSUpdateCaseStatusWhenStatusIsOnHoldFor7DaysForSpanish</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Language__c</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Email_for_Unresolved_Case_Spanish</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Update_Case_Status_Unresolved</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Close_Case_after_7_days_without_response</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.Days_On_hold__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ESDEL - Assign new Task Case to Spain Project Queue</fullName>
        <actions>
            <name>ESDEL_Spain_Task_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.ESDEL_Billing_Order_Line_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Spain Delivery Workstream - Assign ownership to Queue on new tasks with billing order line id</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ESDEL - Set Due Date for Tickets</fullName>
        <actions>
            <name>ESDEL_Spain_Tickety_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Spain Delivery Workstream - Set Due Date on Tickets based on Project Tier</description>
        <formula>AND(ISPICKVAL(Type, &quot;Spain Delivery Ticket&quot;), NOT(ISBLANK( ESDEL_Project__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ESDEL - Update Billing Date on Task</fullName>
        <actions>
            <name>ESDEL_Spain_Update_Billing_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Spain Delivery Task</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ESDEL_Send_to_Billing_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Spain Delivery Workstream - update billing date if null on closure of case</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ESDEL_Overdue Task Record</fullName>
        <active>true</active>
        <description>Spain Delivery Workstream - remind owner of case when task is overdue based on due date.</description>
        <formula>AND( RecordType.Name = &quot;Spanish Delivery&quot;, ISPICKVAL( Type , &quot;Spain Delivery Task&quot;), NOT(ISBLANK( ESDEL_Due_Date__c )), NOT(ISBLANK( Owner:User.Id ) ),  IsClosed = FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ESDEL_Email_Owner_Overdue_Task</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.ESDEL_Due_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email Alert to GSO Training Queue</fullName>
        <actions>
            <name>Send_Email_to_GSO_Training_Queue</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Error_Type_Root_Cause__c</field>
            <operation>equals</operation>
            <value>New user training</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>GSO Training</value>
        </criteriaItems>
        <description>Assign salesforce.com support training cases to GSO Training queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email notification for Incident internal</fullName>
        <actions>
            <name>Email_notification_for_Incident_in_Germany_CCMP_internal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 or 4 or 5 or 6)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EMS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>ccmp-notification@experian.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support-ms@experian.de</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support-ccmp@experian.de</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>support@experian.de</value>
        </criteriaItems>
        <description>Deployed 2016-06-07 for Case# 01947920</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fraud and Identity - Case Comment Notification</fullName>
        <actions>
            <name>Fraud_and_Identity_Case_Comment_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Uncheck_Has_New_Case_Comment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow sends a notification to the case owner and case contact.  This workflow rule is applicable to cases with the &apos;Fraud and Identity Support&apos; and &apos;AdTruth Support&apos; record types.</description>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )  &amp;&amp; OR(RecordType.DeveloperName == &apos;Fraud_and_Identity_Support&apos;,RecordType.DeveloperName == &apos;AdTruth_Support&apos;) &amp;&amp; Has_New_Case_Comment__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fraud and Identity - New Case Notification to Customer</fullName>
        <actions>
            <name>Fraud_and_Identity_New_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This sends a notification to the customer contact when a fraud and identity or an adtruth new case is created by a user.</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c ) &amp;&amp;  OR(  RecordType.Name == &apos;Fraud and Identity Support&apos;,  RecordType.Name == &apos;AdTruth Support&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Fraud and Identity%3A  Close Case Notification</fullName>
        <actions>
            <name>Fraud_and_Identity_Case_Closure_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends a closed case notification to Client Contact when a Fraud and Identity or an AdTruth case closes</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  OR(RecordType.DeveloperName = &quot;Fraud_and_Identity_Support&quot;, RecordType.DeveloperName = &quot;AdTruth_Support&quot;) &amp;&amp; ISCHANGED(IsClosed) &amp;&amp; IsClosed = True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Global Community DQ NA Case Assignment Rule</fullName>
        <actions>
            <name>NA_MS_EDQ_Case_Auto_Response</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Owner_NA_MS_EDQ_Support_Tier_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Origin_Global_Community</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.License__c</field>
            <operation>startsWith</operation>
            <value>Customer Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>North America</value>
        </criteriaItems>
        <description>&lt;&lt;isdatadmin exclusion criteria not needed&gt;&gt;

This Rule will be applied on New Case created by Community EDQ NA Users for the EDQ Technical Support Record Type. This is in lieu of an assignment rule

PK-241016-Disabled to use Assignment Rules</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Global Community DQ UK Case Assignment Rule</fullName>
        <actions>
            <name>UK_I_EDQ_Case_Auto_Response</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Owner_UK_I_MS_EDQ_SupportTier1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Origin_Global_Community</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>EDQ Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.License__c</field>
            <operation>startsWith</operation>
            <value>Customer Community</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>EDQ</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Region__c</field>
            <operation>equals</operation>
            <value>UK&amp;I</value>
        </criteriaItems>
        <description>&lt;&lt;isdatadmin exclusion criteria not needed&gt;&gt;

This Rule will be applied on New Case created by Community EDQ UK Users for the EDQ Technical Support Record Type. This is in lieu of an assignment rule</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Global Security Due Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Global Security Office Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notContain</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Global_Security_Office_Approaching_Due_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Requested_Due_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Global_Security_Office_Request_Overdue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Requested_Due_Date__c</offsetFromField>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Global_Security_Office_Request_Overdue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Requested_Due_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Global_Security_Office_Request_Overdue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Requested_Due_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Global_Security_Office_Due_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Requested_Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Global_Security_Office_Request_Overdue</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Requested_Due_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Global Security Office Request</fullName>
        <actions>
            <name>Global_Security_Office_Confirmation_of_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Global Security Office Requests</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Global Security Office Request Closed Notification</fullName>
        <actions>
            <name>Global_Security_Office_Request_Case_Closed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends a closed case notification to Case Requestor when a Global Security Office case closes</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; RecordType.DeveloperName = &quot;Global_Security_Office_Requests&quot; &amp;&amp; ISCHANGED(IsClosed) &amp;&amp; IsClosed = True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>HR Org Request %3A Closed Rejected</fullName>
        <actions>
            <name>Send_Email_to_HR_Org_Requestor_on_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR Org Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HR Org Request %3A Closed Resolved</fullName>
        <actions>
            <name>Send_Email_to_HR_Org_Requestor_on_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR Org Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HR Org Request %3A New Request</fullName>
        <actions>
            <name>Send_Email_to_HR_Org_Requestor_on_New_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR Org Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>New request created from HR Org</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>HR Org Request %3A Status or Owner Updated</fullName>
        <actions>
            <name>Send_Email_to_HR_Org_Requestor_on_Updates</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status Updated on HR Org Request</description>
        <formula>AND(  RecordType.Name = &apos;HR Org Request&apos;,  ISPICKVAL(Origin,&apos;Web&apos;),  NOT(IsClosed),  OR(   ISCHANGED(Status),   ISCHANGED(OwnerId)  ),  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>InsWeb Alert</fullName>
        <actions>
            <name>InsWeb_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>InsWeb</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>NA CCM Support</value>
        </criteriaItems>
        <description>Alert users that the client InsWeb has a case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Internal Routing for Cases GSO Training</fullName>
        <actions>
            <name>GSO_Training_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Assign_to_GSO_Training</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Training</value>
        </criteriaItems>
        <description>Assign salesforce.com support training cases to GSO Training queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Internal Routing for Cases Global CRM Support</fullName>
        <actions>
            <name>CRM_Request_Global</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Assign_internal_CRM_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assign salesforce.com support cases to Global CRM Support queue. 
**If amending this rule criteria you may also need to amend the following rules: 
Sales Effectiveness Internal Routing for Cases - APAC 
Internal Routing for Cases to Serasa</description>
        <formula>AND(  !ISPICKVAL(Type,&apos;GCSS Support&apos;),  RecordType.Name = &apos;Salesforce.com Support&apos;,  NOT(Contains (TEXT(Status),&apos;Closed&apos;)),  CASE(Secondary_Case_Reason__c,   &apos;Data quality&apos;,0,   &apos;Data updates &amp; de-duplication&apos;,0,   &apos;Delete duplicate contracts / confidential info&apos;, 0,   &apos;Transfer contracts / confidential info&apos;, 0,   &apos;Contract name amend&apos;, 0,   &apos;Assignment team modification / new hire&apos;, 0,    &apos;Lead processing (Eloqua integration)&apos;, 0,    1) == 1,  CASE(Requester_Region__c,   &apos;APAC&apos;,0,   &apos;Latin America&apos;, 0,   1) == 1,  !ISPICKVAL(Reason, &apos;Training&apos;),  !ISPICKVAL(Reason, &apos;Chatter&apos;),  OR(   !ISPICKVAL(Secondary_Case_Reason__c , &apos;Functionality change request&apos;),   TEXT(Requestor__r.Business_Unit__c) &lt;&gt; &apos;NA CS Data Quality&apos;  ),  OR(   CASE(Secondary_Case_Reason__c,    &apos;Access Request&apos;,0,    &apos;Amend opp team / ownership&apos;,0,    &apos;Delete duplicate opportunity&apos;,0,    1)== 1,   CASE(TEXT(Requestor__r.Business_Unit__c),    &apos;NA CS CIS&apos;, 0,    &apos;NA CS BIS&apos;,0,    &apos;NA DA&apos;,0,    &apos;NA DA Fraud and Identity&apos;,0,    1)== 1  ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Internal Routing for Cases to Chatter Team</fullName>
        <actions>
            <name>Chatter_Team_Case_Assigned_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Assign_internal_chatter_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Chatter</value>
        </criteriaItems>
        <description>Workflow to transfer new salesforce.com support cases to the chatter team when the reasons are chatter related</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Internal Routing for Cases to Serasa</fullName>
        <actions>
            <name>Email_Latin_America_Alert_for_Serasa_CRM_Support</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Assign_internal_Serasa_CRM_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Access Request,Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester_Region__c</field>
            <operation>contains</operation>
            <value>Latin America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Secondary_Case_Reason__c</field>
            <operation>notEqual</operation>
            <value>Data quality,Data updates &amp; de-duplication,Lead processing (Eloqua integration),Contract name amend,Delete duplicate contracts / confidential info,Transfer contracts / confidential info,Assignment team modification / new hire</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Training,Chatter</value>
        </criteriaItems>
        <description>Assign Salesforce.com support cases to Serasa CRM Support queue
**If amending this rule criteria you may also need to amend the following rules: 
Sales Effectiveness Internal Routing for Cases - APAC
Internal Routing for Cases Global CRM Support</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NA Automotive SFDC support case field updates</fullName>
        <actions>
            <name>NA_Automotive_SFDC_update_Case_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NA_Automotive_SFDC_update_Case_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>equals</operation>
            <value>autosfdcteam@experian.com</value>
        </criteriaItems>
        <description>Field updates &amp; assignment for cases coming up from email-to-case address autosfdcteam@experian.com</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NA EDQ CCB</fullName>
        <actions>
            <name>NA_EDQ_CCB</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Assigns North American EDQ change requests to their queue for an internal CCB</description>
        <formula>AND(  Text(Secondary_Case_Reason__c) =&apos;Functionality change request&apos;,  Text(Requestor__r.Business_Unit__c) =&apos;NA CS Data Quality&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Company ID Request Received</fullName>
        <actions>
            <name>New_Company_ID_Request_Received</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email alert to rpagsorobot1@experian.com when Company ID Request case is assigned to the DQ Team case queue.</description>
        <formula>AND( ISCHANGED( OwnerId ), Owner:Queue.DeveloperName = &apos;DQ_Team_Queue&apos;, ISPICKVAL(Reason , &apos;Accounts / Contacts&apos;), ISPICKVAL(Secondary_Case_Reason__c , &apos;Data updates &amp; de-duplication&apos;), ISPICKVAL(Error_Type_Root_Cause__c , &apos;Company ID Request&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New User Update Subject Field</fullName>
        <actions>
            <name>Case_New_User_Update_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Workflow to set subject field if the subject is left blank by the user - this will make it clear in the case queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Cancellation%2FDowngrade Request Completed</fullName>
        <actions>
            <name>Cases_Cancel_Downgrade_Closed_Approved_Email_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>contains</operation>
            <value>Full Cancellation,Item Cancellation,Downgrade</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Cancellation%2FDowngrade Request Rejected</fullName>
        <actions>
            <name>Cases_Cancel_Downgrade_Closed_Rejected_Email_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>contains</operation>
            <value>Full Cancellation,Item Cancellation,Downgrade</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rejected Access Request</fullName>
        <actions>
            <name>Rejected_Access_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Rejected Access Request</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; RecordType.Name =&apos;Account Access Request&apos; &amp;&amp; ISPICKVAL(Status,&apos;Closed - Rejected&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected Competitor Request</fullName>
        <actions>
            <name>Rejected_Competitor_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to send email upon the &apos;New Competitor&apos; record type being set to closed rejected.</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; RecordType.Name =&apos;New Competitor&apos; &amp;&amp; ISPICKVAL(Status,&apos;Closed - Rejected&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reset Iscreatorowner Flag</fullName>
        <actions>
            <name>Updating_IsCreatorOwner_to_True_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), 
IsCreatorOwner__c = True, 
OR( 
RecordType.DeveloperName = &quot;Serasa_Cancellation&quot;, 
RecordType.DeveloperName = &quot;Serasa_Support_Request&quot;, 
RecordType.DeveloperName = &quot;Serasa_Support_Request_Internal&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Effectiveness Internal Routing for Cases - APAC</fullName>
        <actions>
            <name>Case_Routing_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Assign_APAC_CRM_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support,Account Access Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Secondary_Case_Reason__c</field>
            <operation>notEqual</operation>
            <value>Data quality,Data updates &amp; de-duplication,Lead processing (Eloqua integration),Contract name amend,Delete duplicate contracts / confidential info,Transfer contracts / confidential info,Assignment team modification / new hire</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester_Region__c</field>
            <operation>contains</operation>
            <value>APAC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Training,Chatter</value>
        </criteriaItems>
        <description>Assign APAC requested salesforce.com cases to the SE APAC team
**If amending this rule criteria you may also need to amend the following rules:
Internal Routing for Cases to Serasa
Internal Routing for Cases Global CRM Support</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Effectiveness Internal Routing for Cases - APAC team</fullName>
        <actions>
            <name>Case_Routing_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Assign_APAC_CPQ_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CPQ Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Internal,New User,Modify User,Disable User,Problem,Feature Request,Question</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Data Quality</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester_Region__c</field>
            <operation>contains</operation>
            <value>APAC</value>
        </criteriaItems>
        <description>Assign CPQ Request which Requestor Owner from APAC (APAC Sales Effectiveness team)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Effectiveness Internal Routing for Cases - NA SE Group</fullName>
        <actions>
            <name>Email_SE_NA_members_for_New_Case_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_to_NA_SE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support,New User,CPQ Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Internal,New User,Modify User,Disable User,Problem,Feature Request,Question</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Data Quality,SalesShare,Lead Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester_Region__c</field>
            <operation>equals</operation>
            <value>North America,Latin America</value>
        </criteriaItems>
        <description>Assign Salesforce.com Support &amp; User Onboarding cases for North America and Latin America Sales Effectiveness Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Effectiveness Internal Routing for Cases - UK%26I Group</fullName>
        <actions>
            <name>Case_Assign_internal_CRM_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>,Salesforce.com Support,New User,CPQ Requests</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Internal,New User,Modify User,Disable User,Problem,Feature Request,Question</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Secondary_Case_Reason__c</field>
            <operation>notEqual</operation>
            <value>Data quality,Lead processing (Eloqua integration)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester_Region__c</field>
            <operation>contains</operation>
            <value>EMEA,UK&amp;I</value>
        </criteriaItems>
        <description>Assign Salesforce.com Support &amp; User Onboarding cases to UK&amp;I Sales Effectiveness queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Case Notification</fullName>
        <actions>
            <name>SalesShare_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Serasa 1st line support case save</fullName>
        <actions>
            <name>Serasa_Update_Case_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  OR(ISPICKVAL(Island__c,&apos;Back Office Retention&apos;),  ISPICKVAL(Island__c,&apos;Legal&apos;),  ISPICKVAL(Island__c,&apos;Positive Registration&apos;),  ISPICKVAL(Island__c,&apos;Sales Support&apos;),  ISPICKVAL(Island__c,&apos;eID Dealer Agreement&apos;),  ISPICKVAL(Island__c,&apos;Consumer&apos;),  ISPICKVAL(Island__c,&apos;Collections&apos;),  ISPICKVAL(Island__c,&apos;eID&apos;),  ISPICKVAL(Island__c,&apos;Data Operations - Negative&apos;),  ISPICKVAL(Island__c,&apos;Data Operations - Positive&apos;),  ISPICKVAL(Island__c,&apos;SME&apos;),  ISPICKVAL(Island__c,&apos;SME Logon&apos;),  ISPICKVAL(Island__c,&apos;Receivables&apos;),  ISPICKVAL(Island__c,&apos;Retention&apos;),  ISPICKVAL(Island__c,&apos;Serasa Consumidor&apos;),  ISPICKVAL(Island__c,&apos;VIP&apos;)), ISPICKVAL(Status,&apos;Resolved&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Call Dropped or Mistake</fullName>
        <actions>
            <name>Change_RT_to_Serasa_Support_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), OR(  ISPICKVAL(Case_Type__c, &apos;Call Dropped&apos;), ISPICKVAL(Case_Type__c, &apos;Mistake&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Case Closed Complete</fullName>
        <actions>
            <name>Serasa_Case_Closed_Complete_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>email alert on serasa case closed Complete</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISCHANGED(Status), ISPICKVAL( Status ,&apos;Closed - Complete&apos;), OR( ISPICKVAL(Island__c,&apos;SME&apos;), ISPICKVAL(Island__c,&apos;SME Logon&apos;), ISPICKVAL(Island__c,&apos;eID&apos;), ISPICKVAL(Island__c,&apos;CAC&apos;), ISPICKVAL(Island__c,&apos;Positive registration&apos;), ISPICKVAL(Island__c,&apos;Serasa Consumidor&apos;), ISPICKVAL(Island__c,&apos;VIP&apos;)),  OR(  RecordType.DeveloperName = &quot;Serasa_Cancellation&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request_Internal&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Case Owner Manager Update</fullName>
        <actions>
            <name>Serasa_Manager_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  OR(  RecordType.DeveloperName = &quot;Serasa_Cancellation&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request_Internal&quot;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Invoice Dispute Case by Sales</fullName>
        <actions>
            <name>Serasa_Case_Owner_Update_for_Invoice_Dis</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISPICKVAL( Serasa_Case_Reason__c,&apos;Commercial Discount&apos;), ISPICKVAL( Serasa_Secondary_Case_Reason__c,&apos;Commercial Discount&apos;),  OR ($Profile.Name = &quot;Experian Serasa Sales Executive&quot;, $Profile.Name = &quot;Experian Serasa Sales Manager&quot;, $Profile.Name = &quot;Experian Serasa Sales Operation&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Non-Postive Identification</fullName>
        <actions>
            <name>Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If the serasa case type is selected as &quot;Non Positive Identification&quot;, change to a general serasa case record type and close it.</description>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), OR(  ISPICKVAL(Case_Type__c, &apos;Non Positive Identification&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Priority SOS Update and Manager Email</fullName>
        <actions>
            <name>The_case_has_been_escalated_to_SOS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Serasa_Manager_Email_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_Update_IsEscalated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>SOS</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ServiceNow Case to be moved to a Queue</fullName>
        <actions>
            <name>ServiceNow_Case_Owner_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Central</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>00Gi0000005EiMK</value>
        </criteriaItems>
        <description>Assignment rules doesnt get processed for cases via API</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set the Implemented Date on Cases</fullName>
        <actions>
            <name>Set_Implemented_Date_on_Cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Salesforce.com Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Implementation_Status__c</field>
            <operation>equals</operation>
            <value>Implemented</value>
        </criteriaItems>
        <description>Set the Implemented Date on Cases when a Case Status is set to Implemented</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Time Stamp on Case Owner Change</fullName>
        <actions>
            <name>Update_Owner_Change_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will stamp the current date when the Owner of the Case Changes.</description>
        <formula>ISCHANGED(OwnerId)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Time Stamp on Case Owner Change on creation</fullName>
        <actions>
            <name>Update_Owner_Change_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>This workflow rule will stamp the current date when the Owner of the Case Changes on record creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UK%26I EDQ Case Auto Response</fullName>
        <actions>
            <name>UK_I_EDQ_Case_Auto_Response</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Sub_Origin__c</field>
            <operation>contains</operation>
            <value>uk.support.qas@experian.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>uk.support@experian.com</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status Field</fullName>
        <actions>
            <name>Update_Implementation_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Implementation Status to &quot;Pending Assessment&quot; when status changed to &quot;CCB Approved&quot; or &quot;CCB Exempt&quot;</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), RecordType.DeveloperName = &apos;SFDC_Support&apos;, (ISPICKVAL(Status, &quot;CCB Approved&quot;)|| ISPICKVAL(Status, &quot;CCB Exempt&quot;)) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update the CNPJ Number on Case</fullName>
        <actions>
            <name>Update_CNPJ_Number_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Work flow is Used for Updating the CNPJ Number on Case based on the Account</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  NOT(ISNULL(AccountId)),  OR(ISNEW(),ISCHANGED(AccountId)),  OR(  RecordType.DeveloperName = &quot;Serasa_Cancellation&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request_Internal&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the CPF Number on Case</fullName>
        <actions>
            <name>Update_CPF_Number_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Work flow is Used for Updating the CPF Number on Case based on the Contact</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  NOT(ISNULL(ContactId)),  OR(ISNEW(),ISCHANGED(ContactId)),  OR(  RecordType.DeveloperName = &quot;Serasa_Cancellation&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request&quot;,  RecordType.DeveloperName = &quot;Serasa_Support_Request_Internal&quot;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the Initial_Term_Period_End_Date</fullName>
        <actions>
            <name>Update_the_Initial_Term_Period_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Initial Term Period End Date</description>
        <formula>OR(ISNEW() ,OR(ISCHANGED(Initial_Term_Period__c),ISCHANGED(Contract_Effective_Date__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>When CRM Request Case is rejected</fullName>
        <actions>
            <name>Send_Email_to_Case_Requestor_when_rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When Salesforce.com Support Case is rejected, need to fire of an email alert</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  (RecordType.DeveloperName = &apos;SFDC_Support&apos; ||  RecordType.DeveloperName = &apos;Platform_Support&apos; ||  RecordType.DeveloperName = &apos;New_User&apos;),  (ISPICKVAL( Status, &apos;Closed - Rejected&apos;)|| ISPICKVAL( Status, &apos;Closed - Incomplete&apos;) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When CRM Request Case is resolved</fullName>
        <actions>
            <name>Send_Email_to_Case_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When Salesforce.com Support Case is resolved, need to fire of an email alert</description>
        <formula>AND(  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  (RecordType.DeveloperName = &apos;SFDC_Support&apos; ||  RecordType.DeveloperName = &apos;Platform_Support&apos; || RecordType.DeveloperName = &apos;New_User&apos;),  ISPICKVAL( Status, &apos;Closed - Complete&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Assigned_Task_for_Case_Creation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Assigned Task for Case Creation</subject>
    </tasks>
    <tasks>
        <fullName>Close_Case_after_7_days_without_response</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Close Case after 7 days without response</subject>
    </tasks>
    <tasks>
        <fullName>Email_send_for_Incident_Closed_Case</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email send for Incident Closed Case</subject>
    </tasks>
    <tasks>
        <fullName>Inactive_Account_Not_Billed</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please set Account status in Admin -AutoCheck System- as Not Billed on the 60th day after receiving this notification.</description>
        <dueDateOffset>59</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Inactive Account Not Billed</subject>
    </tasks>
    <tasks>
        <fullName>Intro_to_corporate_office_assign_to_AE_if_5_location</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Intro to corporate office (assign to AE if 5+ location)</subject>
    </tasks>
    <tasks>
        <fullName>More_Information_needed_for_Case</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>More Information needed for Case</subject>
    </tasks>
    <tasks>
        <fullName>Onboarding_Confirm_Account_Details_send_email_3</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>20</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Onboarding – Confirm Account Details (send email #3)</subject>
    </tasks>
    <tasks>
        <fullName>Update_Account_Segment_Field_Account_Inactive_Reason</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Update Account Segment Field - Account Inactive Reason</subject>
    </tasks>
    <tasks>
        <fullName>Week_10_Onboarding_Check_in</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>70</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 10 – Onboarding – Check in</subject>
    </tasks>
    <tasks>
        <fullName>Week_1_Onboarding_Account_Fastlink_Overview_send_email_1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 1 – Onboarding – Account &amp; Fastlink Overview (send email #1)</subject>
    </tasks>
    <tasks>
        <fullName>Week_2_Onboarding_Usage_Fastlink_send_email_2</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 2 – Onboarding- Usage &amp; Fastlink (send email #2)</subject>
    </tasks>
    <tasks>
        <fullName>Week_3_Onboarding_Service_Data_Marketing_Materials</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 3 – Onboarding – Service Data &amp; Marketing Materials</subject>
    </tasks>
    <tasks>
        <fullName>Week_5_Credit_Based_Marketing_Email_4</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>35</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 5 – Credit Based Marketing Email #4</subject>
    </tasks>
    <tasks>
        <fullName>Week_6_Onboarding_Check_in_assign_to_AE_if_5_location_send_email_5</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>42</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 6 – Onboarding – Check in (assign to AE if 5+ location) (send email #5))</subject>
    </tasks>
    <tasks>
        <fullName>Week_7_Measurement_Analysis_Email_send_email_6</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>49</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 7 – Measurement &amp; Analysis Email (send email #6)</subject>
    </tasks>
    <tasks>
        <fullName>Week_8_Onboarding_Check_in_assign_to_AE_if_5_location_send_email_7</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>56</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Week 8 – Onboarding – Check in (assign to AE if 5+ location) (send email #7)</subject>
    </tasks>
</Workflow>
