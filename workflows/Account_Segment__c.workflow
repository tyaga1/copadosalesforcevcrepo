<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Segment_BU_Lead_changed</fullName>
        <description>Account Segment BU Lead changed</description>
        <protected>false</protected>
        <recipients>
            <field>Business_Unit_Previous_Lead_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CRM_Templates/Account_Segment_BU_Lead_changed</template>
    </alerts>
    <alerts>
        <fullName>Fraud_and_Identify_Notify_CEM_of_Account_Health_Status</fullName>
        <ccEmails>NACE@Experian.com</ccEmails>
        <description>Fraud and Identify - Notify CEM of Account Health Status</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Fraud_and_Identity/FI_Account_Health_Status_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>AHS_Check_Include_in_Status_Report</fullName>
        <description>Checks &apos;AHS Include in Status Report&apos; field on the account segment when the &apos;AHS Health Status&apos; changes to yellow or red</description>
        <field>AHS_Include_in_Status_Report__c</field>
        <literalValue>1</literalValue>
        <name>AHS - Check Include in Status Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Segment_Populate_Type</fullName>
        <description>Will populate the Type from the Hierarchy (Segment__c) record</description>
        <field>Type__c</field>
        <formula>Segment__r.Type__c</formula>
        <name>Account Segment Populate Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Segment_Populate_Value</fullName>
        <description>Will populate from the Hierarchy (Segment__c) record&apos;s Value</description>
        <field>Value__c</field>
        <formula>Segment__r.Value__c</formula>
        <name>Account Segment Populate Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Business_Unit_Lead_email_stamp</fullName>
        <field>Business_Unit_Lead_Email__c</field>
        <formula>Business_Unit_Lead__r.Email</formula>
        <name>Business Unit Lead email stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Business_Unit_Previous_Lead_email_stamp</fullName>
        <field>Business_Unit_Previous_Lead_Email__c</field>
        <formula>PRIORVALUE( Business_Unit_Lead_Email__c )</formula>
        <name>Business Unit Previous Lead email stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Business_Unit_Previous_Lead_name_stamp</fullName>
        <field>Business_Unit_Previous_Lead__c</field>
        <formula>PRIORVALUE(  Business_Unit_Lead__c )</formula>
        <name>Business Unit Previous Lead name stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Clients_since_Date</fullName>
        <description>Populates the Client since date at point of criteria met</description>
        <field>Client_since__c</field>
        <formula>today()</formula>
        <name>Capture Clients since Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Former_Clients_since_Date</fullName>
        <description>Populates the Former Client since date at point of criteria met</description>
        <field>Former_Client_since__c</field>
        <formula>today()</formula>
        <name>Capture Former Clients since Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_Prospect_since_Date</fullName>
        <description>Populates the Prospect since date at point of criteria met</description>
        <field>Prospect_since__c</field>
        <formula>today()</formula>
        <name>Capture Prospect since Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Grant_Access_to_NA_CS_Auto_CS_Roles</fullName>
        <description>Sets the TEMPORARY Checkbox Field to TRUE to enable Sharing Rule that Give EDIT Access to the Account, Account Segments, Cases and Contacts.</description>
        <field>Grant_Access_to_NA_CS_Automotive_Support__c</field>
        <literalValue>1</literalValue>
        <name>Grant Access to NA CS Auto CS Roles</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Accnt_Segment_Sector</fullName>
        <description>Update Account Segment Sector to TOP SME if the Channel is TOP SME</description>
        <field>LATAM_Sector__c</field>
        <formula>&apos;TOP SME&apos;</formula>
        <name>Update Accnt Segment Sector</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LATAM_Business_Group</fullName>
        <description>Update LATAM Business Group field on Account from Account Segment</description>
        <field>LATAM_Business_Group__c</field>
        <formula>LATAM_Business_Group__c</formula>
        <name>Update LATAM Business Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LATAM_Sector_on_Account</fullName>
        <description>Set LATAM Sector on Account if its modified in Account segments</description>
        <field>LATAM_Sector__c</field>
        <formula>&apos;TOP SME&apos;</formula>
        <name>Update LATAM Sector on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Account Health Status - Red and Yellow - Check Include in Status Report</fullName>
        <actions>
            <name>AHS_Check_Include_in_Status_Report</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks the &apos;AHS Include in Status Report&apos; field on the Account Segment object when the &apos;AHS Health Status&apos; changes to Yellow or Red</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; ISCHANGED(AHS_Health_Status__c) &amp;&amp;  OR(ISPICKVAL(AHS_Health_Status__c,&quot;Red&quot;), ISPICKVAL(AHS_Health_Status__c,&quot;Yellow&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Business Unit Lead email stamp</fullName>
        <actions>
            <name>Business_Unit_Lead_email_stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 02183189 current account segment BU Lead email address stamp</description>
        <formula>ISCHANGED( Business_Unit_Lead__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Business Unit Previous Lead</fullName>
        <actions>
            <name>Account_Segment_BU_Lead_changed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Business_Unit_Previous_Lead_email_stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Business_Unit_Previous_Lead_name_stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 02183189 Notification to former account segment BU Lead</description>
        <formula>ISCHANGED( Business_Unit_Lead__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Accnt Segment Sales Leadership to Account object</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account_Segment__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Segment__c.LATAM_Sales_Leadership__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Copy value of Account Segment&apos;s Sales Leadership to related Account&apos;s Sales Leadership field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Fraud and Identity - Account Health Status Notification</fullName>
        <actions>
            <name>Fraud_and_Identify_Notify_CEM_of_Account_Health_Status</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends a email to the Fraud and Identity CEMs when the &apos;AHS Health Status&apos; changes to Yellow or Red</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)  &amp;&amp; ISCHANGED(AHS_Health_Status__c)  &amp;&amp; OR(ISPICKVAL(AHS_Health_Status__c,&quot;Red&quot;), ISPICKVAL(AHS_Health_Status__c,&quot;Yellow&quot;)) &amp;&amp; Type__c == &quot;Business Unit&quot;  &amp;&amp; OR(Value__c == &quot;APAC DA ANZ&quot;,  Value__c == &quot;APAC DA Greater China&quot;, Value__c == &quot;APAC DA Japan&quot;, Value__c == &quot;EMEA DA&quot;, Value__c == &quot;LATAM DA Fraud and Identity&quot;, Value__c == &quot;NA DA&quot;, Value__c == &quot;NA DA Fraud and Identity&quot;, Value__c == &quot;UK&amp;I DA ID&amp;F&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NA CS Automotive BU</fullName>
        <actions>
            <name>Grant_Access_to_NA_CS_Auto_CS_Roles</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Segment__c.Type__c</field>
            <operation>equals</operation>
            <value>Business Unit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Segment__c.Value__c</field>
            <operation>equals</operation>
            <value>NA CS Automotive</value>
        </criteriaItems>
        <description>Runs on ALL Account Segments that are the NA CS Automotive Business Unit:  This was created to temporarily run a field Update on the Account that allows a Sharing rule for the Automotive Client Services Roles Edit Access</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Segment Copy to Account Segment</fullName>
        <actions>
            <name>Account_Segment_Populate_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Account_Segment_Populate_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Segment__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Will run to copy fields from the Hierarchy (Segment__c) parent record</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Client since Date</fullName>
        <actions>
            <name>Capture_Clients_since_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Segment__c.Relationship_Type__c</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <description>Workflow to capture the &quot;Client since&quot; date when Account Segment is selected to &quot;Client &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Former Client since Date</fullName>
        <actions>
            <name>Capture_Former_Clients_since_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Segment__c.Relationship_Type__c</field>
            <operation>equals</operation>
            <value>Former Client</value>
        </criteriaItems>
        <description>Workflow to capture the &quot;Former Client since&quot; date when Account Segment is selected to &quot;Former Client &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update LATAM Business Group</fullName>
        <actions>
            <name>Update_LATAM_Business_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Segment__c.LATAM_Business_Group__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update LATAM Business Group field on Account from Account Group</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update LATAM Sector based on Channel</fullName>
        <actions>
            <name>Update_Accnt_Segment_Sector</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_LATAM_Sector_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account_Segment__c.LATAM_Channel__c</field>
            <operation>equals</operation>
            <value>Top SME</value>
        </criteriaItems>
        <description>Update LATAM Sector field on AccountSegment if the Channel is Top SME</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Prospect since Date</fullName>
        <actions>
            <name>Capture_Prospect_since_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Segment__c.Relationship_Type__c</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <description>Workflow to capture the &quot;Prospect since&quot; date when Account Segment is selected to &quot;Prospect s&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
