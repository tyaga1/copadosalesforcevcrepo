<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Do_Not_Deal_Contact_Breach_Notification</fullName>
        <ccEmails>ComplianceDeptUK@uk.experian.com</ccEmails>
        <ccEmails>accountsreceivable@uk.experian.com</ccEmails>
        <description>Do Not Deal Email Alert to Manager &amp; Compliance if Contact Created</description>
        <protected>false</protected>
        <recipients>
            <field>Creator_Managers_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Do_Not_Deal_Alert_Contact</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_when_OnDemand_Contact_is_found</fullName>
        <ccEmails>sadar.yacob@experian.com</ccEmails>
        <description>Email Alert when OnDemand Contact is found</description>
        <protected>false</protected>
        <recipients>
            <recipient>sadar.yacob@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EDQ_ONDemand_Contact</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_when_an_Community_Contact_is_Deactivated</fullName>
        <description>Email Alert when an Community Contact is Deactivated</description>
        <protected>false</protected>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>william.hemmerick@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CSDA_Client_Sales_Support_Email_Template/Contact_No_Longer_with_Company_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>CSDA_Include_in_Client_Loyalty_Survey_L</fullName>
        <description>Include in Client Loyalty Survey Last Updated By updated when  Include in Client Loyalty Survey is changed</description>
        <field>Include_in_Client_Survey_Last_Updated_By__c</field>
        <formula>LastModifiedBy.FirstName &amp; &apos; &apos; &amp;  LastModifiedBy.LastName</formula>
        <name>CSDA Include in Client Loyalty Survey L</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CSDA_No_Client_Survey_Last_Updated_Date</fullName>
        <description>Timestamps CSDA Include in Client Loyalty Survey Last Updated Date</description>
        <field>No_Client_Survey_Last_Updated_Date__c</field>
        <formula>Now()</formula>
        <name>CSDA No Client Survey Last Updated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Owner_to_Global</fullName>
        <field>OwnerId</field>
        <lookupValue>experianglobal@experian.global</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Change Owner to Global</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Send_to_On_Demand_to_true</fullName>
        <description>Will set the Contact.EDQ_Send_to_On_Demand__c to true</description>
        <field>EDQ_Send_to_On_Demand__c</field>
        <literalValue>1</literalValue>
        <name>Contact Set Send to On Demand to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_to_be_dormant_Null</fullName>
        <field>Dormant_Date__c</field>
        <name>Date to be dormant = Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dormant_Date_Today</fullName>
        <field>Dormant_Date__c</field>
        <formula>TODAY()</formula>
        <name>Dormant Date Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dormant_date_Today_180</fullName>
        <field>Dormant_Date__c</field>
        <formula>TODAY() +180</formula>
        <name>Dormant date Today +180</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dormant_is_False</fullName>
        <field>Dormant__c</field>
        <literalValue>0</literalValue>
        <name>Dormant is False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dormant_is_True</fullName>
        <field>Dormant__c</field>
        <literalValue>1</literalValue>
        <name>Dormant is True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Inactive_True</fullName>
        <field>Inactive__c</field>
        <literalValue>1</literalValue>
        <name>Inactive True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Include_in_Clnt_Survey_Last_Updated_Date</fullName>
        <field>Include_in_Clnt_Survey_Last_Updated_Date__c</field>
        <formula>NOW()</formula>
        <name>Include in Clnt Survey Last Updated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_Client_Survey_Last_Updated_By</fullName>
        <field>No_Client_Survey_Last_Updated_By__c</field>
        <formula>$User.FirstName &amp;&quot; &quot;&amp; $User.LastName</formula>
        <name>No Client Survey Last Updated By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_CNPJ_on_Contact</fullName>
        <field>Account_CNPJ__c</field>
        <formula>Account.CNPJ_Number__c</formula>
        <name>Set Account CNPJ on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conference_Phone_for_North_America</fullName>
        <description>Inside Sales Project.</description>
        <field>Conference_Phone__c</field>
        <formula>&apos;972-390-5222&apos;</formula>
        <name>Set Conference Phone for North America</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conference_Phone_for_UK_I</fullName>
        <field>Conference_Phone__c</field>
        <formula>&apos;+442037703375&apos;</formula>
        <name>Set Conference Phone for UK&amp;I</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contact_for_no_Surveys</fullName>
        <field>Include_in_Client_Loyalty_Survey__c</field>
        <literalValue>0</literalValue>
        <name>Set Contact for no Surveys</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Experian_ID_on_Contact_Object</fullName>
        <description>Set Experian ID on Contact Object</description>
        <field>Experian_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Set Experian ID on Contact Object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Serasa_Integration_ID_on_Contacts</fullName>
        <description>Set Serasa Integration ID with CNPJ + CPF when CPF !- null</description>
        <field>Serasa_Integration_ID__c</field>
        <formula>Account.CNPJ_Number__c + &apos;-&apos; + CPF__c</formula>
        <name>Set Serasa Integration ID on Contacts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RT_LATAM_Serasa_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>LATAM_Serasa_Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RT LATAM Serasa - Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SaaS_Contact_Timestamp</fullName>
        <field>SaaS_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>Update SaaS Contact Timestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Last_Updated_Date</fullName>
        <description>CRM2:W-005405: Resets the Status Last Updated Date field</description>
        <field>Status_Last_Updated_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Status Last Updated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Contact_Id_to_Boomi</fullName>
        <apiVersion>32.0</apiVersion>
        <description>This OBM is now obsolete and not used</description>
        <endpointUrl>http://DELLPRDAB1.svc.experian.com/ws/simple/getOnDemandContact</endpointUrl>
        <fields>EDQ_Integration_Id__c</fields>
        <fields>EDQ_Send_to_On_Demand__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>debbie.chamkasem@experian.global</integrationUser>
        <name>Send Contact Id to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Always Set Account CNPJ On Contact</fullName>
        <actions>
            <name>Set_Account_CNPJ_on_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Added for the AddOrCreateNewContactAddressController to manage duplicates</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Stamp Include in Client Loyalty Survey</fullName>
        <actions>
            <name>CSDA_Include_in_Client_Loyalty_Survey_L</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Include in Client Loyalty Survey is updated,  Include in Client Loyalty Survey Last Updated By is updated to the name of the user who performed the last update</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp; (     ISCHANGED( Include_in_Client_Loyalty_Survey__c ) ||   ISNEW()  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CSDA Stamp No Client Loyalty Survey Changes</fullName>
        <actions>
            <name>CSDA_No_Client_Survey_Last_Updated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When No Client Loyalty Survey is changed, field 
No Client Loyalty Survey Last Updated Date is updated</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;  (   ISCHANGED( No_Client_Loyalty_Survey__c ) ||  (ISNEW() &amp;&amp; No_Client_Loyalty_Survey__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Needs to be synced with On Demand</fullName>
        <actions>
            <name>Email_Alert_when_OnDemand_Contact_is_found</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Contact_Id_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.EDQ_Send_to_On_Demand__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Data Migration User</value>
        </criteriaItems>
        <description>This WF Rule will run if the EDQ_Send_to_On_Demand__c is set to true, which will notify Boomi that the contact needs to be synced with On Demand</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact On Demand Updates</fullName>
        <actions>
            <name>Contact_Set_Send_to_On_Demand_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Checks if the Contact has an update requiring it to be synched by other systems (On Demand)</description>
        <formula>AND (   OR (     ISCHANGED(FirstName),     ISCHANGED(LastName),     ISCHANGED(Email),     ISCHANGED(MobilePhone),     ISCHANGED(Phone),     ISCHANGED(Title),     ISCHANGED(Salutation)   ),   EDQ_On_Demand__c  )   ||  AND(   ISCHANGED(EDQ_On_Demand__c),   EDQ_On_Demand__c = true )  ||  AND (   ISNEW(),   EDQ_On_Demand__c = true )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Do Not Deal Contact Breach</fullName>
        <actions>
            <name>Do_Not_Deal_Contact_Breach_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Will send an email if an Order is created against an Account which has &quot;Do Not Deal&quot; set to true</description>
        <formula>Account.Do_Not_Deal__c = true &amp;&amp; NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dormant manually set%2C Dormant Date Today</fullName>
        <actions>
            <name>Dormant_Date_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Inactive_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If System admin or Sales Admin manually set dormant make dormant date today</description>
        <formula>Dormant__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Alert On Community User Contact Deactivation</fullName>
        <actions>
            <name>Email_Alert_when_an_Community_Contact_is_Deactivated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Is_Community_User__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Inactive__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>41st Parameter Email Alert when an Community User contact is made No longer with company</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inactive - queue for dormant</fullName>
        <actions>
            <name>Dormant_date_Today_180</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Inactive__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Dormant_is_True</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Inactive removed - clear dormant</fullName>
        <actions>
            <name>Date_to_be_dormant_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Dormant_is_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>PRIORVALUE(Inactive__c)=True &amp;&amp; Inactive__c =False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Include in Client Survey Last Updated Date</fullName>
        <actions>
            <name>Include_in_Clnt_Survey_Last_Updated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the &quot;Include in Client Survey Last Updated Date&quot; when the &quot;Include in Client Loyalty Survey&quot; field is modified</description>
        <formula>ISCHANGED(Include_in_Client_Loyalty_Survey__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>No Client Survey Last Updated By</fullName>
        <actions>
            <name>No_Client_Survey_Last_Updated_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the &quot;No Client Survey Last Updated By&quot; when the &quot;No Client Loyalty Survey&quot; field is modified</description>
        <formula>ISCHANGED( No_Client_Loyalty_Survey__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Owner change Global Owner</fullName>
        <actions>
            <name>Change_Owner_to_Global</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>where the owner is not Experian Global user change the owner to Experian Global user</description>
        <formula>OwnerId &lt;&gt; $Setup.Experian_Global__c.OwnerId__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SaaS Contact Integration</fullName>
        <actions>
            <name>Update_SaaS_Contact_Timestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sets SaaS Timestamp when Contact flagged checked a SaaS Contact by normal users</description>
        <formula>AND(     NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ),  	NOT($Profile.Name =  $Label.PROFILE_API_ONLY_SAAS_EDQ),      OR( 	   AND(ISNEW(), SaaS__c),  	   AND(SaaS__c, NOT(PRIORVALUE(SaaS__c)))     ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Conference Phone for North America</fullName>
        <actions>
            <name>Set_Conference_Phone_for_North_America</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>North America</value>
        </criteriaItems>
        <description>Inside Sales Project.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Conference Phone for UK%26I</fullName>
        <actions>
            <name>Set_Conference_Phone_for_UK_I</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Region__c</field>
            <operation>equals</operation>
            <value>UK&amp;I</value>
        </criteriaItems>
        <description>Inside Sales UK&amp;I Project.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Contact for no Surveys</fullName>
        <actions>
            <name>Set_Contact_for_no_Surveys</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update so that when the &apos;No Client Loyalty Survey&apos; is ticked, if the &apos;Include in Client Loyalty Survey&apos; is already ticked it should become unticked as the &apos;No Survey&apos; flag should overwrite.</description>
        <formula>AND(     No_Client_Loyalty_Survey__c = true,     ISCHANGED(No_Client_Loyalty_Survey__c) = true,     Include_in_Client_Loyalty_Survey__c = true,      ISCHANGED(Include_in_Client_Loyalty_Survey__c)= false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Serasa Integration ID for Serasa Contacts</fullName>
        <actions>
            <name>Set_Serasa_Integration_ID_on_Contacts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Serasa Integration ID for Serasa Contacts to prevent duplicates</description>
        <formula>NOT ISBLANK (CPF__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new Contacts created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update EDQ Integration Id on new Contacts created in SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Experian ID on new Accounts created in SFDC</fullName>
        <actions>
            <name>Set_Experian_ID_on_Contact_Object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Experian_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update Experian ID on new Accounts created in SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update RT to LATAM Serasa - Read Only</fullName>
        <actions>
            <name>Update_RT_LATAM_Serasa_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Source_System__c</field>
            <operation>notEqual</operation>
            <value>Serasa CRMOD,Salesforce.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Source_System__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Consumer Contact</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Status Last Updated Date</fullName>
        <actions>
            <name>Update_Status_Last_Updated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CRM2:W-005405: Updates the Status Last Updated Date field when Status field is changes - Includes bypass for IsDataAdmin</description>
        <formula>AND(  ISCHANGED(Status__c),  NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
