<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Mailing_Addr_on_Contact</fullName>
        <description>Update Mailing Addr on Contact</description>
        <field>MailingStreet</field>
        <formula>LEFT(TRIM(IF(!ISBLANK(Address__r.Address_1__c),Address__r.Address_1__c &amp; &apos; &apos;,&apos;&apos;) &amp;
IF(!ISBLANK(Address__r.Address_2__c),Address__r.Address_2__c &amp; &apos; &apos;,&apos;&apos;)),255)</formula>
        <name>Update Mailing Addr on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_City_on_Contact</fullName>
        <description>Update Mailing City on Contact</description>
        <field>MailingCity</field>
        <formula>LEFT(Address__r.City__c,40)</formula>
        <name>Update Mailing City on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_Country_on_Contact</fullName>
        <field>MailingCountry</field>
        <formula>LEFT(TEXT(Address__r.Country__c),80)</formula>
        <name>Update Mailing Country on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_State_On_Contact</fullName>
        <field>MailingState</field>
        <formula>LEFT(IF(ISBLANK(Text(Address__r.State__c)),Address__r.Province__c,
Text(Address__r.State__c)),80)</formula>
        <name>Update Mailing State On Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mailing_Zip_on_Contact</fullName>
        <field>MailingPostalCode</field>
        <formula>LEFT(IF(ISBLANK(Address__r.Zip__c), Address__r.Postcode__c,Address__r.Zip__c ),20)</formula>
        <name>Update Mailing Zip on Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Contact__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Contact Mailing Addr from Contact Registered Addr</fullName>
        <actions>
            <name>Update_Mailing_Addr_on_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_City_on_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_Country_on_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_State_On_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Mailing_Zip_on_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Get the Registered Address back to the Mailing Address on the Contact</description>
        <formula>AND ( ISPICKVAL(Address_Type__c,&apos;Registered&apos;), OR ( ISNEW(), ISCHANGED( LastModifiedDate) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
