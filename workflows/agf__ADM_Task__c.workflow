<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Task_Creation_Notification</fullName>
        <description>New Task Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>chennour.wright@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_task_Created_Notification</template>
    </alerts>
    <rules>
        <fullName>Task Created On Work Record</fullName>
        <actions>
            <name>New_Task_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Case : 02072126  New Task Created on User Story Notification</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
