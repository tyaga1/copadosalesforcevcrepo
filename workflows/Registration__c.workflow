<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Manual_registration_required</fullName>
        <description>Manual registration required</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Manager_Email_Hidden__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesshare@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Activated_Order_Requires_Manual_Registration</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Account_Mgr_Email_on_Registration</fullName>
        <description>Update Account Mgr Email on Registration</description>
        <field>Account_Manager_Email_Hidden__c</field>
        <formula>Account_Manager_Email__c</formula>
        <name>Update Account Mgr Email on Registration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Registration_Id_to_Boomi3</fullName>
        <apiVersion>35.0</apiVersion>
        <description>Send Registration Id to Boomi</description>
        <endpointUrl>http://DELLPRDAB1.svc.experian.com/ws/simple/getLicenseKey</endpointUrl>
        <fields>Account_License_Key_Reference__c</fields>
        <fields>DataSet__c</fields>
        <fields>Data_Usage__c</fields>
        <fields>EDQ_Integration_Id__c</fields>
        <fields>Id</fields>
        <fields>Operating_System__c</fields>
        <fields>Order_Line_Type__c</fields>
        <fields>Registration_Key__c</fields>
        <fields>Serial_Number__c</fields>
        <fields>Usage_End_Date__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>debbie.chamkasem@experian.global</integrationUser>
        <name>Send Registration Id to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Create License Key AE Task</fullName>
        <actions>
            <name>Client_License_Key_s_Are_Available</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>T-325614 - Automatically create a task when when the license key is populated</description>
        <formula>ISCHANGED(Serial_Number__c) &amp;&amp; ISBLANK(PRIORVALUE( Serial_Number__c )) &amp;&amp;  NOT(  ISBLANK(Serial_Number__c)  )  &amp;&amp; NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Manual Registration Notification</fullName>
        <actions>
            <name>Manual_registration_required</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Account_Mgr_Email_on_Registration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Email notification is sent to Acct Manager for manual registration required.</description>
        <formula>(Manual_Registration__c  = TRUE) &amp;&amp; ISBLANK( Serial_Number__c )  &amp;&amp; NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Registration%3A License Key Generation Request</fullName>
        <actions>
            <name>Send_Registration_Id_to_Boomi3</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow rule fires when a new registration record is created and data usage is not null and license key doesn&apos;t require a manual registration.  This sends an outbound message to Boomi so a license key can be generated.</description>
        <formula>ISBLANK(Serial_Number__c) &amp;&amp; NOT( ISBLANK(Data_Usage__c)) &amp;&amp;  NOT( Manual_Registration__c )  &amp;&amp; NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Client_License_Key_s_Are_Available</fullName>
        <assignedToType>owner</assignedToType>
        <description>Client license key(s) are available. Please send the client the information via email.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Client License Key(s) Are Available</subject>
    </tasks>
</Workflow>
