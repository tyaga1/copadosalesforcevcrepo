<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Experian_Id</fullName>
        <description>Updates the Experian Id on the Contract__c object to the 18 digit salesforce record id</description>
        <field>Experian_ID__c</field>
        <formula>CASESAFEID( Id )</formula>
        <name>Update Experian Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Term_Cancelled_Date_to_Blank</fullName>
        <field>Termination_Cancellation_Date__c</field>
        <name>Update Term / Cancelled Date to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Term_Cancelled_Date_to_Today</fullName>
        <field>Termination_Cancellation_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Term/Cancelled Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contract Status no longer Terminated or Cancelled</fullName>
        <actions>
            <name>Update_Term_Cancelled_Date_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   RecordType.Name = $Label.CONTRACT_RECTYPE_CSDA_CONTRACT_HEAD,   AND(     NOT(ISPICKVAL(Status__c, &apos;Experian Terminated&apos;)),     NOT(ISPICKVAL(Status__c, &apos;Client Cancelled&apos;))   ),   OR(     ISPICKVAL(PRIORVALUE(Status__c), &apos;Experian Terminated&apos;),     ISPICKVAL(PRIORVALUE(Status__c), &apos;Client Cancelled&apos;)   ),   NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contract Terminated or Cancelled</fullName>
        <actions>
            <name>Update_Term_Cancelled_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   RecordType.Name =  $Label.CONTRACT_RECTYPE_CSDA_CONTRACT_HEAD,   OR(     ISPICKVAL(Status__c, &apos;Experian Terminated&apos;),       ISPICKVAL(Status__c, &apos;Client Cancelled&apos;)   ),   NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Experian Id</fullName>
        <actions>
            <name>Update_Experian_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Experian Id to contain the 18 digit salesforce record id in order to support integration</description>
        <formula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ) &amp;&amp;  ISBLANK( Experian_ID__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
