<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ARIA_Billing_Account_Stuck_Email_Notification</fullName>
        <ccEmails>gcssalesforcesupport@experian.com</ccEmails>
        <ccEmails>AriaBillingAdministrator@exchange.experian.com</ccEmails>
        <ccEmails>GCSAriaSupport@experian.com</ccEmails>
        <description>ARIA Billing Account Stuck Email Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Aria_BillingAccnt_stuck_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Aria_Billling_Account_with_Tax_Exempt</fullName>
        <ccEmails>AriaBillingAdministrator@exchange.experian.com</ccEmails>
        <ccEmails>GCSAriaSupport@experian.com</ccEmails>
        <description>Send Email when Push to Aria is set to Yes and Tax Exempt is Yes</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ARIA_Push_to_Aria_isYes_Tax_Exempt_is_Yes_on_Billing_Account_Notify</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Push_to_Aria_is_set_to_Yes_and_Direct_Debit_is_Yes</fullName>
        <ccEmails>AriaBillingAdministrator@exchange.experian.com</ccEmails>
        <ccEmails>GCSAriaSupport@experian.com</ccEmails>
        <description>Send Email when Push to Aria is set to Yes and Direct Debit is Yes</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ARIA_Push_to_Aria_isYes_Direct_Debit_is_Yes_on_Billing_Account_Notify</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_when_Push_to_Aria_is_set_to_Yes_and_InterCompany_is_Yes</fullName>
        <ccEmails>AriaBillingAdministrator@exchange.experian.com</ccEmails>
        <ccEmails>GCSAriaSupport@experian.com</ccEmails>
        <description>Send Email when Push to Aria is set to Yes and InterCompany is Yes</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ARIA_Push_to_Aria_isYes_Inter_Company_is_Yes_on_Billing_Account_Notify</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Aria_Billing_Accnt_Status_to_Active</fullName>
        <description>Set Aria Billing Accnt Status to Active when Billing Accnt Ref # and Billing Accnt  are updated back from Aria</description>
        <field>ARIA_Billing_Account_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Set Aria Billing Accnt Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CRM_ID_on_Billing_Accnt_to_SFDC_ID</fullName>
        <description>Set CRM ID on Billing Accnt to SFDC ID of the Billing Account</description>
        <field>CRM_ID__c</field>
        <formula>casesafeid(Id)</formula>
        <name>Set CRM ID on Billing Accnt to SFDC ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Aria_Billing_Account_Exists_flag</fullName>
        <description>Set the Aria Billing Account Exists flag on the Account object</description>
        <field>Aria_Billing_Account_Exists__c</field>
        <literalValue>1</literalValue>
        <name>Set the Aria Billing Account Exists flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Billing_Accnt_Country_to_Users</fullName>
        <description>Set the Billing Account Country to Users Country</description>
        <field>Billing_Account_Country__c</field>
        <formula>Text($User.Country__c)</formula>
        <name>Set the Billing Accnt Country to Users</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Billing_Accnt_Currency</fullName>
        <description>Set the Billing Accnt Currency based on the Master Plan Currency</description>
        <field>Billing_Account_Currency__c</field>
        <formula>Master_Plan__r.Plan_Currency__c</formula>
        <name>Set the Billing Accnt Currency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Billing_Account_Cu</fullName>
        <description>Set the Billing Account  Country based on the Master Plan Country</description>
        <field>Billing_Account_Country__c</field>
        <formula>Master_Plan__r.Country__c</formula>
        <name>Set the Billing Accnt  Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_time_Aria_Billing_Account_sent_t</fullName>
        <field>Sent_to_Aria_DateTime__c</field>
        <formula>now()</formula>
        <name>Set the time Aria Billing Account sent t</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Send_to_Aria_to_TRUE</fullName>
        <description>Update the Send to Aria to TRUE</description>
        <field>SendBillingAccntToAria__c</field>
        <literalValue>1</literalValue>
        <name>Update Send to Aria to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Billing_Account_to_ARIA</fullName>
        <apiVersion>29.0</apiVersion>
        <description>Billing account details to Aria</description>
        <endpointUrl>http://DELLPRDAB1.svc.experian.com/ws/simple/getAriaBillingAccount</endpointUrl>
        <fields>Billing_System_Ref__c</fields>
        <fields>Id</fields>
        <fields>SendBillingAccntToAria__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>debbie.chamkasem@experian.global</integrationUser>
        <name>Billing Account to ARIA</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Send Aria Billing Accnt to Boomi</fullName>
        <actions>
            <name>Billing_Account_to_ARIA</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.SendBillingAccntToAria__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When a Billing Accnt is created or updated send it to Aria via Boomi</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email Alerts on Push to Aria and Direct Debit</fullName>
        <actions>
            <name>Send_Email_when_Push_to_Aria_is_set_to_Yes_and_Direct_Debit_is_Yes</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Push_To_Aria__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Experian Aria Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Direct_Debit__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email alerts when Direct Debit is Checked and Push to Aria is set to YES</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send email Alerts on Push to Aria and InterCompany</fullName>
        <actions>
            <name>Send_Email_when_Push_to_Aria_is_set_to_Yes_and_InterCompany_is_Yes</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Push_To_Aria__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Experian Aria Sales,System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Inter_Company__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email alerts when InterCompany is Checked and Push to Aria is set to YES</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send email Alerts on Push to Aria and Tax Exempt</fullName>
        <actions>
            <name>Aria_Billling_Account_with_Tax_Exempt</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Push_To_Aria__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Experian Aria Sales,System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Tax_Exempt__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email alerts when Tax Exempt is Checked and Push to Aria is set to YES</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Billing country based on User Profile</fullName>
        <actions>
            <name>Set_the_Billing_Accnt_Country_to_Users</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$User.IsActive</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set CRM ID on Billing Account</fullName>
        <actions>
            <name>Set_CRM_ID_on_Billing_Accnt_to_SFDC_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.CRM_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Billing_System_Ref__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set CRM ID on Billing Account based on SFDC ID of Billing Account for Newly created records</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set the Aria Billing Accnt exists on Account flag</fullName>
        <actions>
            <name>Set_the_Aria_Billing_Account_Exists_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Account_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Billing_System_Ref__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the Aria Billing Accnt exists flag on Account when a Billing Accnt is associated with an Account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set the Billing Accnt Status to Active when Ref Number is obtained from Aria</fullName>
        <actions>
            <name>Set_Aria_Billing_Accnt_Status_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Billing_System_Ref__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Billing_Account_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set the Billing Accnt Status to Active when Billing Accnt Ref # is obtained from Aria</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set the Billing Account Currency and Country</fullName>
        <actions>
            <name>Set_the_Billing_Accnt_Currency</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_the_Billing_Account_Cu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Master_Plan_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set the Billing Account Currency based on the Master Plan Currency &amp; Country</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Send to Aria on Push to Aria Select</fullName>
        <actions>
            <name>Set_the_Aria_Billing_Account_Exists_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Send_to_Aria_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account__c.Push_To_Aria__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>API Only</value>
        </criteriaItems>
        <description>Update the Send to Aria  Check box to TRUE when the Push to Aria is selected to YES</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
