<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Approved_Level_2_Downgrade_To_Nominator</fullName>
        <description>Email Approved - Level 2 Downgrade - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Approved_Level_2_Downgrade_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Approved_Level_2_To_Nominator</fullName>
        <description>Email Approved - Level 2 - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Approved_Level_2_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Approved_Level_2_To_Nominee</fullName>
        <description>Email Approved - Level 2 - To Nominee</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Approved_Level_2_To_Nominee</template>
    </alerts>
    <alerts>
        <fullName>Email_Business_Region_is_Changed_Notify_Coordinators</fullName>
        <ccEmails>Sophie.Pogson@experian.com</ccEmails>
        <description>Email Business Region is Changed - Notify Coordinators</description>
        <protected>false</protected>
        <recipients>
            <recipient>caroline.chan@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Business_Region_is_Changed_Notify_Coordinator</template>
    </alerts>
    <alerts>
        <fullName>Email_Nominated_Level_3_Indiv_To_Line_Manager</fullName>
        <description>Email Nominated - Level 3 Indiv - To Line Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Nominated_Level_3_Indiv_To_Line_Manager</template>
    </alerts>
    <alerts>
        <fullName>Email_Nominated_Level_3_Indiv_To_Nominator</fullName>
        <description>Email Nominated - Level 3 Indiv - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Nominated_Level_3_Indiv_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Nominated_Level_3_Indiv_To_Nominee</fullName>
        <description>Email Nominated - Level 3 Indiv - To Nominee</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Nominated_Level_3_Indiv_To_Nominee</template>
    </alerts>
    <alerts>
        <fullName>Email_Nominated_Level_3_Team_To_Line_Managers</fullName>
        <description>Email Nominated - Level 3 Team - To Line Managers</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Nominated_Level_3_Team_To_Line_Managers</template>
    </alerts>
    <alerts>
        <fullName>Email_Nominated_Level_3_Team_To_Nominator</fullName>
        <description>Email Nominated - Level 3 Team - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Nominated_Level_3_Team_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Nominated_Level_3_Team_To_Nominees</fullName>
        <description>Email Nominated - Level 3 Team - To Nominees</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Nominated_Level_3_Team_To_Nominees</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Approval_Level_2_To_Line_Manager</fullName>
        <description>Email Pending Approval - Level 2 - To Line Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Approval_Level_2_To_Line_Manager</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Approval_Level_2_to_Level_3_To_Nominator</fullName>
        <description>Email Pending Approval - Level 2 to Level 3 - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Approval_Level_2_to_Level_3_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Approval_Level_3_Indiv_To_Line_Manager</fullName>
        <description>Email Pending Approval - Level 3 Indiv - To Line Manager</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Approval_Level_3_Indiv_To_Line_Manager</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Approval_Level_3_Team_To_Line_Managers</fullName>
        <description>Email Pending Approval - Level 3 Team - To Line Managers</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Approval_Level_3_Team_To_Line_Manager</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Approval_Level_3_Team_To_Project_Sponsor</fullName>
        <description>Email Pending Approval - Level 3 Team - To Project Sponsor</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Approval_Level_3_Team_To_Project_Sponsor</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Panel_Approval_Level_3_Indiv_To_Nominator</fullName>
        <description>Email Pending Panel Approval - Level 3 Indiv - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Panel_Approval_Level_3_Indiv_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Panel_Approval_Level_3_Indiv_To_Nominee</fullName>
        <description>Email Pending Panel Approval - Level 3 Indiv - To Nominee</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Panel_Approval_Level_3_Indiv_To_Nominee</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Panel_Approval_Level_3_Team_Notify_Coords</fullName>
        <description>Email Pending Panel Approval - Level 3 Team - Notify Coords</description>
        <protected>false</protected>
        <recipients>
            <recipient>Recognition_Award_Committee</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Panel_Approval_Level_3_Team_Notify_Coords</template>
    </alerts>
    <alerts>
        <fullName>Email_Pending_Panel_Approval_Manager_Approved_Level_3_Team_To_Nominees</fullName>
        <description>Email Pending Panel Approval &amp; Manager Approved - Level 3 Team - To Nominees</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Pending_Panel_Approval_Manager_Approved_Level_3_Team_To_Nominees</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_Level_2_To_Nominator</fullName>
        <description>Email Rejected - Level 2 - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_Level_2_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_Level_3_Indiv_To_Nominator</fullName>
        <description>Email Rejected - Level 3 Indiv - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_Level_3_Indiv_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_Level_3_Team_To_Line_Managers</fullName>
        <description>Email Rejected - Level 3 Team - To Line Managers</description>
        <protected>false</protected>
        <recipients>
            <field>Nominees_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_Level_3_Team_To_Line_Managers</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_Level_3_Team_To_Nominator</fullName>
        <description>Email Rejected - Level 3 Team - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_Level_3_Team_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_with_Badge_Level_2_To_Nominator</fullName>
        <description>Email Rejected with Badge - Level 2 - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_with_Badge_Level_2_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_with_Badge_Level_2_To_Nominee</fullName>
        <description>Email Rejected with Badge - Level 2 - To Nominee</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_with_Badge_Level_2_To_Nominee</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_with_Badge_Level_3_Indiv_To_Nominator</fullName>
        <description>Email Rejected with Badge - Level 3 Indiv - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_with_Badge_Level_3_Indiv_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_with_Badge_Level_3_Indiv_To_Nominee</fullName>
        <description>Email Rejected with Badge - Level 3 Indiv - To Nominee</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_with_Badge_Level_3_Indiv_To_Nominee</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_with_Badge_Level_3_Team_To_Managers</fullName>
        <description>Email Rejected with Badge - Level 3 Team - To Managers</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_with_Badge_Level_3_Team_To_Managers</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_with_Badge_Level_3_Team_To_Nominator</fullName>
        <description>Email Rejected with Badge - Level 3 Team - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_with_Badge_Level_3_Team_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Rejected_with_Badge_Level_3_Team_To_Nominees</fullName>
        <description>Email Rejected with Badge - Level 3 Team - To Nominees</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Rejected_with_Badge_Level_3_Team_To_Nominees</template>
    </alerts>
    <alerts>
        <fullName>Email_Won_Level_3_Individual_To_Nominator</fullName>
        <description>Email Won - Level 3 Individual - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Won_Level_3_Individual_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Won_Level_3_Individual_To_Nominee</fullName>
        <description>Email Won - Level 3 Individual - To Nominee</description>
        <protected>false</protected>
        <recipients>
            <field>Nominee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Won_Level_3_Individual_To_Nominee</template>
    </alerts>
    <alerts>
        <fullName>Email_Won_Level_3_Team_To_Line_Managers</fullName>
        <description>Email Won - Level 3 Team - To Line Managers</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Won_Level_3_Team_To_Line_Managers</template>
    </alerts>
    <alerts>
        <fullName>Email_Won_Level_3_Team_To_Nominator</fullName>
        <description>Email Won - Level 3 Team - To Nominator</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Won_Level_3_Team_To_Nominator</template>
    </alerts>
    <alerts>
        <fullName>Email_Won_Level_3_Team_To_Project_Sponsor</fullName>
        <description>Email Won - Level 3 Team - To Project Sponsor</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Sponsor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Email_Won_Level_3_Team_To_Project_Sponsor</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Appoved_Notification_to_Payroll</fullName>
        <description>Nomination Appoved Notification to Payroll</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Notification_Approved</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Level2_APAC</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Approved Level2 APAC</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ammu.punnoose@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ling.chan@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>masami.okubo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mizuho.nagasaki@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>neha.dodeja@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.hughes@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shaharizan.salleh@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Rewards_Payroll_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Level2_Default</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Approved Level2 Default</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Rewards_Payroll_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Level2_EMEA</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>EZA.DL.HR@experian.com</ccEmails>
        <ccEmails>payroll_dk@experian.com</ccEmails>
        <ccEmails>lonn.mailbox@no.experian.com</ccEmails>
        <description>Nomination Approved Level2 EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>beata.gombault@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>debbie.chamkasem@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>didem.koprucu@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>elena.racanicchi@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>monica.mariani@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>neo.chaane@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rikke.nielsen@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sandra.turrillo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svetoslava.shopova@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Rewards_Payroll_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Level2_SLATAM</fullName>
        <ccEmails>novedadesnomina@experian.com</ccEmails>
        <ccEmails>GD_Acredita_Peru@experian.com</ccEmails>
        <ccEmails>Cynthia.Espinoza@experian.com</ccEmails>
        <ccEmails>FolhadePagamento@exchange.experian.com</ccEmails>
        <description>Nomination Approved Level2 SLATAM</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>andreia.williams@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>beth.wheat@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>debbie.chamkasem@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gemma.james@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jose.ortiz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>juan.lopez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>matias.wenger@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pamela.ttica@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vanessa.espinoza@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Rewards_Payroll_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Level2_UK</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Approved Level2 UK</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>darren.buxton@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelly.swift@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mikeuk.skinner@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stuart.matthews@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Rewards_Payroll_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Notification_APAC</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Approved Notification APAC</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ammu.punnoose@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ling.chan@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>masami.okubo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mizuho.nagasaki@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>neha.dodeja@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.hughes@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shaharizan.salleh@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Notification_Approved</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Notification_Default</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Approved  Nomination Default</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Notification_Approved</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Notification_EMEA</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>EZA.DL.HR@experian.com</ccEmails>
        <ccEmails>payroll_dk@experian.com</ccEmails>
        <ccEmails>lonn.mailbox@no.experian.com</ccEmails>
        <description>Nomination Approved Notification EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>beata.gombault@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>debbie.chamkasem@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>didem.koprucu@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>elena.racanicchi@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>monica.mariani@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>neo.chaane@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rikke.nielsen@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sandra.turrillo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svetoslava.shopova@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Notification_Approved</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Notification_SLATAM</fullName>
        <ccEmails>novedadesnomina@experian.com</ccEmails>
        <ccEmails>GD_Acredita_Peru@experian.com</ccEmails>
        <ccEmails>Cynthia.Espinoza@experian.com</ccEmails>
        <ccEmails>FolhadePagamento@exchange.experian.com</ccEmails>
        <description>Nomination Approved Notification SLATAM</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>andreia.williams@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>beth.wheat@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>debbie.chamkasem@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gemma.james@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jose.ortiz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>juan.lopez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>matias.wenger@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pamela.ttica@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vanessa.espinoza@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Notification_Approved</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Approved_Notification_UK</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Approved Notification UK</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>darren.buxton@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelly.swift@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mikeuk.skinner@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stuart.matthews@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Notification_Approved</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Notification_to_Managers</fullName>
        <description>Nomination Notification to Managers</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Approved_Notification</fullName>
        <description>Nomination Won/Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>beth.wheat@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>colette.johnson@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Approved_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Won Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification_APAC</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Won Notification APAC</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>ammu.punnoose@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ling.chan@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>masami.okubo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mizuho.nagasaki@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>neha.dodeja@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.hughes@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shaharizan.salleh@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification_Default</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Won Notification Default</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification_EMEA</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>EZA.DL.HR@experian.com</ccEmails>
        <ccEmails>payroll_dk@experian.com</ccEmails>
        <ccEmails>lonn.mailbox@no.experian.com</ccEmails>
        <description>Nomination Won Notification EMEA</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>beata.gombault@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>debbie.chamkasem@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>didem.koprucu@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>elena.racanicchi@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>monica.mariani@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>neo.chaane@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rikke.nielsen@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sandra.turrillo@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>svetoslava.shopova@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification_NA</fullName>
        <ccEmails>payroll@experian.com</ccEmails>
        <description>Nomination Won Notification NA</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification_SLATAM</fullName>
        <ccEmails>novedadesnomina@experian.com</ccEmails>
        <ccEmails>GD_Acredita_Peru@experian.com</ccEmails>
        <ccEmails>Cynthia.Espinoza@experian.com</ccEmails>
        <ccEmails>FolhadePagamento@exchange.experian.com</ccEmails>
        <description>Nomination Won Notification SLATAM</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>andreia.williams@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>beth.wheat@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>debbie.chamkasem@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gemma.james@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jose.ortiz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>juan.lopez@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>matias.wenger@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pamela.ttica@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.joseph@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vanessa.espinoza@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification_UK</fullName>
        <ccEmails>gemma.james@experian.com</ccEmails>
        <ccEmails>Richard.joseph@experian.com</ccEmails>
        <ccEmails>beth.wheat@experian.com</ccEmails>
        <ccEmails>debbie.chamkasem@experian.com</ccEmails>
        <description>Nomination Won Notification UK</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>darren.buxton@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kelly.swift@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mikeuk.skinner@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>stuart.matthews@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Nomination_Half_Yearly_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nomination_Won_Notification_UK_I</fullName>
        <ccEmails>Payroll@uk.experian.com</ccEmails>
        <description>Nomination Won Notification - UK&amp;I</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Rewards_Payroll_Notification</template>
    </alerts>
    <alerts>
        <fullName>Nominee_Manager_Not_available_or_Inactive</fullName>
        <description>Nominee Manager Not available or Inactive</description>
        <protected>false</protected>
        <recipients>
            <recipient>caroline.chan@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Nominee_Manager_Inactive_or_Not_Available</template>
    </alerts>
    <alerts>
        <fullName>Payroll_Notification_for_Level2_Rewards</fullName>
        <description>Payroll Notification for Level2 Rewards</description>
        <protected>false</protected>
        <recipients>
            <field>Region_Payroll_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>beth.wheat@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Employee_Rewards/Rewards_Payroll_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Nominator_Downgrade_Notification_from_Level_3_Indiv_to_Level_2</fullName>
        <description>Send Nominator Downgrade Notification from Level 3 Indiv to Level 2</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Downgraded_from_L3_Indiv_to_L2</template>
    </alerts>
    <alerts>
        <fullName>Send_Nominator_Downgrade_Notification_from_Level_3_Team_to_Level_2</fullName>
        <description>Send Nominator Downgrade Notification from Level 3 Team to Level 2</description>
        <protected>false</protected>
        <recipients>
            <field>Requestor__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>grhr@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Employee_Rewards/Downgraded_from_L3_Team_to_L2</template>
    </alerts>
    <fieldUpdates>
        <fullName>Nomination_Status_to_Approved</fullName>
        <description>If Manager creates Nomination then the Status is Approved</description>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Nomination Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nomination_Status_to_Pending_Panel_App</fullName>
        <description>If Manager creates Nomination Level 3 Individual then the Status is Pending Panel Approval</description>
        <field>Status__c</field>
        <literalValue>Pending Panel Approval</literalValue>
        <name>Nomination Status to Pending Panel App</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nomination_Status_to_Won</fullName>
        <field>Status__c</field>
        <literalValue>Won</literalValue>
        <name>Nomination Status to Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Downgraded_to_Level_2</fullName>
        <field>Downgraded_to_Level_2__c</field>
        <literalValue>1</literalValue>
        <name>Set Downgraded to Level 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_H1_H2_Field_to_H1</fullName>
        <field>H1_or_H2__c</field>
        <literalValue>H1</literalValue>
        <name>Set H1/H2 Field to H1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_H1_H2_Field_to_H2</fullName>
        <field>H1_or_H2__c</field>
        <literalValue>H2</literalValue>
        <name>Set H1/H2 Field to H2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Manager_Approved_Date_to_Today</fullName>
        <field>Manager_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Manager Approved Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nomination_Payroll_Award_Date</fullName>
        <field>Payroll_Award_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Nomination Payroll Award Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nomination_Payroll_Business_Unit</fullName>
        <field>Payroll_Business_Unit__c</field>
        <formula>Nominee__r.Oracle_Business_Unit_Costing__c</formula>
        <name>Set Nomination Payroll Business Unit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nomination_Payroll_Company</fullName>
        <field>Payroll_Company__c</field>
        <formula>Nominee__r.Oracle_Company_Costing__c</formula>
        <name>Set Nomination Payroll Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nomination_Payroll_Cost_Centre</fullName>
        <field>Payroll_Cost_Centre__c</field>
        <formula>Nominee__r.Oracle_Cost_Centre_Costing__c</formula>
        <name>Set Nomination Payroll Cost Centre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nomination_Payroll_Country</fullName>
        <field>Payroll_Country__c</field>
        <formula>Nominee__r.Oracle_Country_Costing__c</formula>
        <name>Set Nomination Payroll Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Nomination_Status_to_PendingApproval</fullName>
        <description>Sets the Nomination Status to Pending Approval.</description>
        <field>Status__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Set Nomination Status to PendingApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Oracle_Payroll_Name_from_Nominee</fullName>
        <field>Oracle_Payroll_Name__c</field>
        <formula>TEXT(Nominee__r.Oracle_Payroll_Name__c)</formula>
        <name>Set Oracle Payroll Name from Nominee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Upgraded_to_Level_3</fullName>
        <field>Upgraded_to_Level_3__c</field>
        <literalValue>1</literalValue>
        <name>Set Upgraded to Level 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Approved - Level 2 - To Nomin%28ee%2Fator%29</fullName>
        <actions>
            <name>Email_Approved_Level_2_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Approved_Level_2_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Approved&quot; - Type = &quot;Level 2 - Spot Award&quot; - To Nominee and Nominator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Approved&apos;),  ISPICKVAL(Type__c, &apos;Level 2 - Spot Award&apos;),  Downgraded_to_Level_2__c == false, Requestor__c &lt;&gt; Nominee__r.ManagerId )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved - Level 2 - To Nominee</fullName>
        <actions>
            <name>Email_Approved_Level_2_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Approved&quot; - Type = &quot;Level 2 - Spot Award&quot; - To Nominee and Nominator When Manager is nominator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Approved&apos;),  ISPICKVAL(Type__c, &apos;Level 2 - Spot Award&apos;),  Downgraded_to_Level_2__c == false , Requestor__c == Nominee__r.ManagerId)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved - Level 2 Downgrade - To Nomin%28ee%2Fator%29</fullName>
        <actions>
            <name>Email_Approved_Level_2_Downgrade_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Approved_Level_2_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Downgraded - Status = &quot;Approved&quot; - Type = &quot;Level 2 - Spot Award&quot; - To Nominee and Nominator (Non Team)</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Approved&apos;),  ISPICKVAL(Type__c, &apos;Level 2 - Spot Award&apos;),  Downgraded_to_Level_2__c == true,  Master_Nomination__c == null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Oracle Payroll Name</fullName>
        <actions>
            <name>Set_Oracle_Payroll_Name_from_Nominee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Nomination__c.Nominee__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.EmployeeNumber</field>
            <operation>notEqual</operation>
            <value>72532816</value>
        </criteriaItems>
        <description>Populates the Oracle Payroll Name from the Nominee when Nomination is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Business Region changed by Recognition Coordinator</fullName>
        <actions>
            <name>Email_Business_Region_is_Changed_Notify_Coordinators</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>whenever business region value is changed by a recognition Coordinator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),   $Permission.Recognition_Coordinator,ISCHANGED( Category__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Manager Nomination Auto Approval</fullName>
        <actions>
            <name>Nomination_Status_to_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Nomination submitted by Manager shall be auto Approved</description>
        <formula>AND ( Nominee__r.ManagerId  =  Requestor__r.Id ,  ISPICKVAL(Status__c, &apos;Submitted&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manager Nomination Auto Approval-Level2</fullName>
        <actions>
            <name>Nomination_Status_to_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Nominee&apos;s Manager is the Requestor, then set to Approved. (Only for Level 2)</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),   ISPICKVAL(Status__c, &apos;Submitted&apos;),   ISPICKVAL(Type__c,&apos;Level 2 - Spot Award&apos;), Nominee__c != null,  Nominee__r.ManagerId = Requestor__r.Id )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manager Nomination Auto Approval-level3Indi</fullName>
        <actions>
            <name>Nomination_Status_to_Pending_Panel_App</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Nominee&apos;s Manager is the Requestor, then set to Approved. (Level 3 Individual)</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),   ISPICKVAL(Status__c, &apos;Submitted&apos;),   ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Individual)&apos;), Nominee__c != null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nominated - Level 3 Indiv - To Nomin%28ee%2Fator%29 %26 Manager</fullName>
        <actions>
            <name>Email_Nominated_Level_3_Indiv_To_Line_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Nominated_Level_3_Indiv_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Nominated_Level_3_Indiv_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Nominated&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - Send Email To Nominee, Nominator &amp; Manager</description>
        <formula>AND(  NOT( $Permission.Recognition_Data_Admin ),  ISPICKVAL(Status__c, &apos;Nominated&apos;),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Individual)&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nominated - Level 3 Team - To Nominator</fullName>
        <actions>
            <name>Email_Nominated_Level_3_Team_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Nominated&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Nominator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Nominated&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),  Team_Members_List__c != null,  Master_Nomination__c == null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nominated - Level 3 Team - To Nominees%2FManagers</fullName>
        <actions>
            <name>Email_Nominated_Level_3_Team_To_Line_Managers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Nominated_Level_3_Team_To_Nominees</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Nominated&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Line Managers &amp; Nominees</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c,&apos;Nominated&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),  Master_Nomination__c != null,  Nominee__c != null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nomination Payroll On Level2 Reward</fullName>
        <actions>
            <name>Payroll_Notification_for_Level2_Rewards</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Nomination__c.Type__c</field>
            <operation>equals</operation>
            <value>Level 2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Won,Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Award_for_Level_2__c</field>
            <operation>equals</operation>
            <value>Spot Award</value>
        </criteriaItems>
        <description>Nomination Req Sumbission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nomination Request Approved</fullName>
        <actions>
            <name>Nomination_Appoved_Notification_to_Payroll</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Nomination_Won_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Nomination__c.Type__c</field>
            <operation>equals</operation>
            <value>H1,H2,Half Yearly Nomination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Nomination Req Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nomination Request Submission</fullName>
        <actions>
            <name>Nomination_Notification_to_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Nomination__c.Type__c</field>
            <operation>equals</operation>
            <value>H1,H2,Half Yearly Nomination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <description>Nomination Req Sumbission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nomination Request Won</fullName>
        <actions>
            <name>Nomination_Won_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Nomination__c.Type__c</field>
            <operation>equals</operation>
            <value>H1,H2,Half Yearly Nomination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <description>Nomination Req WON</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nomination Request Won NA</fullName>
        <actions>
            <name>Nomination_Won_Notification_NA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Nomination__c.Type__c</field>
            <operation>equals</operation>
            <value>H1,H2,Half Yearly Nomination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Nominee_s_Region__c</field>
            <operation>equals</operation>
            <value>North America</value>
        </criteriaItems>
        <description>Nomination Req WON</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nomination Request Won- UK%26I</fullName>
        <actions>
            <name>Nomination_Won_Notification_UK_I</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Nomination__c.Type__c</field>
            <operation>equals</operation>
            <value>H1,H2,Half Yearly Nomination</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Nominee_s_Region__c</field>
            <operation>equals</operation>
            <value>UK&amp;I</value>
        </criteriaItems>
        <description>Nomination Req WON UK&amp;I</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Nominee Manager Not Available</fullName>
        <actions>
            <name>Nominee_Manager_Not_available_or_Inactive</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Nomination__c.Type__c</field>
            <operation>equals</operation>
            <value>Level 2 - Spot Award,Level 3 - Half Year Nomination (Individual),Level 3 - Half Year Nomination (Team)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Nominee Manager Not available</value>
        </criteriaItems>
        <description>Nominee&apos;s Manager Inactive or manager is Not available.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Notify Nominator when Downgraded from L3 Individual to L2</fullName>
        <actions>
            <name>Send_Nominator_Downgrade_Notification_from_Level_3_Indiv_to_Level_2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify the nominator when the Level-3 (Individual) nomination has been downgraded to Level-2</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(PRIORVALUE(Type__c),&apos;Level 3 - Half Year Nomination (Individual)&apos;),  ISPICKVAL(Type__c,&apos;Level 2 - Spot Award&apos;),  ISPICKVAL(Status__c,&apos;Pending Approval&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Nominator when Downgraded from L3 Team to L2</fullName>
        <actions>
            <name>Send_Nominator_Downgrade_Notification_from_Level_3_Team_to_Level_2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Team)&apos;),  ISPICKVAL(Status__c, &apos;Downgraded To Level 2&apos;), Master_Nomination__c == null )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pending Approval - Level 2 - To Line Manager</fullName>
        <actions>
            <name>Email_Pending_Approval_Level_2_To_Line_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Pending Approval&quot; - Type = &quot;Level 2 - Spot Award&quot; - Send Email To Line Manager</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c,&apos;Pending Approval&apos;),  ISPICKVAL(Type__c, &apos;Level 2 - Spot Award&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Approval - Level 3 Indiv - To Line Manager</fullName>
        <actions>
            <name>Email_Pending_Approval_Level_3_Indiv_To_Line_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Pending Approval&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - Send Email To Line Manager</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c,&apos;Pending Approval&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Individual)&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Approval - Level 3 Indiv - Upgrade - To Nominator</fullName>
        <actions>
            <name>Email_Pending_Approval_Level_2_to_Level_3_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Pending Approval&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - Upgraded to Level 3 = true - To Nominator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Individual)&apos;),  ISPICKVAL(Status__c,&apos;Pending Approval&apos;),  Upgraded_to_Level_3__c == true )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Approval - Level 3 Team - To Line Managers</fullName>
        <actions>
            <name>Email_Pending_Approval_Level_3_Team_To_Line_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Pending Approval&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Line Managers</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c,&apos;Pending Approval&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),  Master_Nomination__c != null,  Nominee__c != null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Approval - Level 3 Team - To Project Sponsor</fullName>
        <actions>
            <name>Email_Pending_Approval_Level_3_Team_To_Project_Sponsor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Pending Approval&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Project Sponsor</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Pending Approval&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),  Team_Members_List__c != null,  Master_Nomination__c == null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Panel Approval %26 Manager Approved - Level 3 Team - To Nominees</fullName>
        <actions>
            <name>Email_Pending_Panel_Approval_Manager_Approved_Level_3_Team_To_Nominees</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Pending Panel Approval &amp; Manager Approved - Level 3 Team - To Nominees</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Master_Nomination__r.Status__c,&apos;Pending Panel Approval&apos;),  ISPICKVAL(Status__c, &apos;Manager Approved&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),  Master_Nomination__c != null,  Nominee__c != null,  Notify_Nominees__c == true )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Panel Approval - Level 3 Indiv - To Nomin%28ee%2Fator%29</fullName>
        <actions>
            <name>Email_Pending_Panel_Approval_Level_3_Indiv_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Pending Panel Approval&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - To Nominator &amp; Nominee</description>
        <formula>AND(  NOT( $Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Pending Panel Approval&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Individual)&apos;), Nominee__r.ManagerId = Requestor__r.Id )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Panel Approval - Level 3 Indiv - To Nominee</fullName>
        <actions>
            <name>Email_Pending_Panel_Approval_Level_3_Indiv_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Status = &quot;Pending Panel Approval&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - To  Nominee</description>
        <formula>AND(  NOT( $Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Pending Panel Approval&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Individual)&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pending Panel Approval - Level 3 Team - Notify Coords</fullName>
        <actions>
            <name>Email_Pending_Panel_Approval_Level_3_Team_Notify_Coords</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>If the Notify Coordinators checkbox is true (set in Trigger handler), then send an email to the coordinators.</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  Notify_Coordinators__c == true,  ISPICKVAL(Status__c, &apos;Pending Panel Approval&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Downgrade to Level 2</fullName>
        <actions>
            <name>Set_Downgraded_to_Level_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a Nomination type is changed from Level 3 to Level 2, populate the field</description>
        <formula>AND(   NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Type__c,&apos;Level 2 - Spot Award&apos;),  OR(   ISPICKVAL(PRIORVALUE(Type__c),&apos;Level 3 - Half Year Nomination (Individual)&apos;),    ISPICKVAL(PRIORVALUE(Type__c),&apos;Level 3 - Half Year Nomination (Team)&apos;)  ),  ISPICKVAL(Status__c,&apos;Pending Approval&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate H1%2FH2 Field for Half Year Noms to H1</fullName>
        <actions>
            <name>Set_H1_H2_Field_to_H1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For any half year noms approved before 1st Oct</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  OR(   AND(    OR(     ISPICKVAL(Status__c, &apos;Submitted&apos;),     ISPICKVAL(Status__c, &apos;Pending Approval&apos;)    ),    AND(      /* Set this only when between Apr and Oct */     MONTH(TODAY()) &gt;= 4,     MONTH(TODAY()) &lt; 10    )   ),   AND(    OR(      ISPICKVAL(Status__c, &apos;Manager Approved&apos;),     ISPICKVAL(Status__c, &apos;Approved&apos;)    ),    AND(      /* Set this only when between Apr and Oct */     MONTH(Manager_Approved_Date__c) &gt;= 4,     MONTH(Manager_Approved_Date__c) &lt; 10    )   )  ),  OR(   ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Individual)&apos;),   AND(    ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),    Master_Nomination__c == null   )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate H1%2FH2 Field for Half Year Noms to H2</fullName>
        <actions>
            <name>Set_H1_H2_Field_to_H2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For any half year noms approved after 30th Sept</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  OR(   AND(    OR(     ISPICKVAL(Status__c, &apos;Submitted&apos;),     ISPICKVAL(Status__c, &apos;Pending Approval&apos;)    ),    /* Only when before Apr or after Sept */    OR(     MONTH(TODAY()) &lt; 4,     MONTH(TODAY()) &gt;= 10    )   ),   AND(    OR(      ISPICKVAL(Status__c, &apos;Manager Approved&apos;),     ISPICKVAL(Status__c, &apos;Approved&apos;)    ),    /* Only when before Apr or after Sept */    OR(     MONTH(Manager_Approved_Date__c) &lt; 4,     MONTH(Manager_Approved_Date__c) &gt;= 10    )   )  ),  OR(   ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Individual)&apos;),   AND(    ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),    Master_Nomination__c == null   )  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Payroll Fields</fullName>
        <actions>
            <name>Set_Nomination_Payroll_Award_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Nomination_Payroll_Business_Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Nomination_Payroll_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Nomination_Payroll_Cost_Centre</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Nomination_Payroll_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved,Won</value>
        </criteriaItems>
        <description>Once Approved (level 2) or Won (level 3/4), set the payroll fields on the nomination.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Upgrade to Level 3</fullName>
        <actions>
            <name>Set_Upgraded_to_Level_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Pending Approval&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot;, Type(before) = &quot;Level 2 - Spot Award&quot; - To Nominator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(PRIORVALUE(Type__c),&apos;Level 2 - Spot Award&apos;),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Individual)&apos;),  ISPICKVAL(Status__c,&apos;Pending Approval&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rejected - Level 2 - To Nominator</fullName>
        <actions>
            <name>Email_Rejected_Level_2_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected&quot; - Type = &quot;Level 2 - Spot Award&quot; - To Nominator</description>
        <formula>AND(   NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Type__c, &apos;Level 2 - Spot Award&apos;),  ISPICKVAL(Status__c, &apos;Rejected&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected - Level 3 Indiv - To Nominator</fullName>
        <actions>
            <name>Email_Rejected_Level_3_Indiv_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - Send email to Nominator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Rejected&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Individual)&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected - Level 3 Team - To Line Managers</fullName>
        <actions>
            <name>Email_Rejected_Level_3_Team_To_Line_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Line Managers</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Team)&apos;),  ISPICKVAL(Status__c,&apos;Rejected&apos;),  ISPICKVAL(Master_Nomination__r.Status__c,&apos;Rejected&apos;),  Master_Nomination__c != null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected - Level 3 Team - To Nominator</fullName>
        <actions>
            <name>Email_Rejected_Level_3_Team_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Nominator</description>
        <formula>AND(  NOT( $Permission.Recognition_Data_Admin ),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Team)&apos;),  ISPICKVAL(Status__c,&apos;Rejected&apos;),  Master_Nomination__c == null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected with Badge - Level 2 - To Nominator</fullName>
        <actions>
            <name>Email_Rejected_with_Badge_Level_2_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Rejected_with_Badge_Level_2_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected with Badge&quot; - Type = &quot;Level 2 - Spot Award&quot; - Send Email To Nominator</description>
        <formula>AND(  NOT( $Permission.Recognition_Data_Admin ),  ISPICKVAL(Status__c, &apos;Rejected with Badge&apos;),  ISPICKVAL(Type__c,&apos;Level 2 - Spot Award&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected with Badge - Level 3 Indiv - To Nomin%28ee%2Fator%29</fullName>
        <actions>
            <name>Email_Rejected_with_Badge_Level_3_Indiv_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Rejected_with_Badge_Level_3_Indiv_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected with Badge&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - Send Email To Nominator and Nominee</description>
        <formula>AND(  NOT( $Permission.Recognition_Data_Admin ),  ISPICKVAL(Status__c, &apos;Rejected with Badge&apos;),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Individual)&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected with Badge - Level 3 Team - To Managers%2FNominees</fullName>
        <actions>
            <name>Email_Rejected_with_Badge_Level_3_Team_To_Managers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Rejected_with_Badge_Level_3_Team_To_Nominees</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected with Badge&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Line Managers and Nominees</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Team)&apos;),  ISPICKVAL(Status__c,&apos;Rejected with Badge&apos;),  ISPICKVAL(Master_Nomination__r.Status__c,&apos;Rejected with Badge&apos;),  Master_Nomination__c != null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected with Badge - Level 3 Team - To Nominator</fullName>
        <actions>
            <name>Email_Rejected_with_Badge_Level_3_Team_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Rejected with Badge&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Nominator</description>
        <formula>AND(  NOT( $Permission.Recognition_Data_Admin ),  ISPICKVAL(Type__c,&apos;Level 3 - Half Year Nomination (Team)&apos;),  ISPICKVAL(Status__c,&apos;Rejected with Badge&apos;),  Master_Nomination__c == null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Nomination Manager Approved Date when Approved</fullName>
        <actions>
            <name>Set_Manager_Approved_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Nomination__c.Manager_Approved_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Nomination__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Panel Approval,Manager Approved,Approved</value>
        </criteriaItems>
        <description>Status = &quot;Pending Panel Approval&quot; OR &quot;Manager Approved&quot; OR &quot;Approved&quot; - Update Manager Approved Date to Today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Submitted - Level 4 - By Recognition Coordinator</fullName>
        <actions>
            <name>Nomination_Status_to_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Simple rule to set the Status to Won for any Elite awards entered by the Recognition Coordinator.</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c,&apos;Submitted&apos;),  ISPICKVAL(Type__c,&apos;Level 4 - Elite&apos;),  $Permission.Recognition_Coordinator )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Submitted - Selective Update to Pending Approval</fullName>
        <actions>
            <name>Set_Nomination_Status_to_PendingApproval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a nomination is created with Status &apos;Submitted&apos;, L2 and L3I awards where the requestor is not the manager will be changed to Pending Approval. L3T awards for the master nomination will also be set to Pending Approval.</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Submitted&apos;),  OR(   AND(    Nominee__c != null,    Requestor__r.Id != Nominee__r.ManagerId,    OR(     ISPICKVAL(Type__c, &apos;Level 2 - Spot Award&apos;),     ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Individual)&apos;)    )   ),   AND(    /* Send approval for the Master Nom only */     ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),     Master_Nomination__c == null,    Nominee__c == null   )  ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Won - Level 3 Individual - To Nominator</fullName>
        <actions>
            <name>Email_Won_Level_3_Individual_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Won_Level_3_Individual_To_Nominee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Won&quot; - Type = &quot;Level 3 - Half Year Nomination (Individual)&quot; - Send Email To Nominator</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Won&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination(Individual)&apos;)   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Won - Level 3 Team - To Managers</fullName>
        <actions>
            <name>Email_Won_Level_3_Team_To_Line_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Won&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Line Managers</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c,&apos;Won&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),  Master_Nomination__c != null,  Nominee__c != null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Won - Level 3 Team - To Nominator %26 Sponsor</fullName>
        <actions>
            <name>Email_Won_Level_3_Team_To_Nominator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_Won_Level_3_Team_To_Project_Sponsor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Status = &quot;Won&quot; - Type = &quot;Level 3 - Half Year Nomination (Team)&quot; - Send Email To Nominator &amp; Sponsor</description>
        <formula>AND(  NOT($Permission.Recognition_Data_Admin),  ISPICKVAL(Status__c, &apos;Won&apos;),  ISPICKVAL(Type__c, &apos;Level 3 - Half Year Nomination (Team)&apos;),  Team_Members_List__c != null,  Master_Nomination__c == null )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
