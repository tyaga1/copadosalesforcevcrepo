<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_after_1_day_of_Expiration_Date</fullName>
        <description>Email Alert after 1 day of Expiration Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Serasa_Customer_Care/Email_Template_after_one_day_of_Expiration_Date</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_after_3_days_after_Expiration_Date</fullName>
        <description>Email Alert after 3 days of Expiration Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Serasa_Customer_Care/Email_Template_after_3_days_of_Expiration_Date</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_Activity_Creation</fullName>
        <description>Email Alert on Activity Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Serasa_Customer_Care/Email_Template_on_Activity_Opening</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_the_day_of_Expiration_Date</fullName>
        <description>Email Alert on the day of Expiration Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Serasa_Customer_Care/Email_Template_after_one_day_of_Expiration_Date</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_one_day_before_Expiration_Date</fullName>
        <description>Email Alert one day before Expiration Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Serasa_Customer_Care/Email_Template_on_one_day_before_expiration</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Case_Activity_Completed_Date</fullName>
        <field>Completed_Date__c</field>
        <formula>Today()</formula>
        <name>Update Case Activity Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Serasa Case Activity Completed Date</fullName>
        <actions>
            <name>Update_Case_Activity_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Serasa Case Activity Completed Date when the status is completed</description>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  ISPICKVAL( Status__c , &apos;Completed&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Serasa Case Activity Escalation</fullName>
        <actions>
            <name>Email_Alert_on_Activity_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Serasa Case Activity Escalation Alerts</description>
        <formula>AND( NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), TEXT(Status__c)!=&apos;Completed&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_after_1_day_of_Expiration_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Activity__c.Expected_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_after_3_days_after_Expiration_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Activity__c.Expected_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_on_the_day_of_Expiration_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Activity__c.Expected_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_one_day_before_Expiration_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case_Activity__c.Expected_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
