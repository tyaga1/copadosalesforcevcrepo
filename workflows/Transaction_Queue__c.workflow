<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Transaction_Queue_Status_is_New_or_In_progress_for_15_mins_alert</fullName>
        <ccEmails>esmitsm@experian.com</ccEmails>
        <ccEmails>gcssalesforcesupport@experian.com</ccEmails>
        <description>Transaction Queue Status is New or In progress for 15 mins alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Transaction_Queue_with_Status</template>
    </alerts>
    <outboundMessages>
        <fullName>Account_TransQueue_Record_Send_to_Boomi</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Send Account Transaction Queue Record to Boomi UK Server</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNAccount</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>External_Source_ID__c</fields>
        <fields>Id</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>richard.joseph@experian.global</integrationUser>
        <name>Account TransQueue Record Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Asset_TransQueue_Record_Send_to_Boomi</fullName>
        <apiVersion>38.0</apiVersion>
        <description>Send Asset Transaction Queue Record to Boomi</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNAsset</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>External_Source_ID__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>richard.joseph@experian.global</integrationUser>
        <name>Asset TransQueue Record Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>CaseAttachmnt_TransQue_Rec_Send_to_Boomi</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Send Case Attachment Transaction Queue Records to Boomi</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNCaseAttachment</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>richard.joseph@experian.global</integrationUser>
        <name>CaseAttachmnt TransQue Rec Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>CaseComment_TransQue_Rec_Send_to_Boomi</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Send Case Comment Transaction Queue Records to Boomi</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNCaseComments</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>External_Source_ID__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>richard.joseph@experian.global</integrationUser>
        <name>CaseComment TransQue Rec Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Case_TransQueue_Record_Send_to_Boomi</fullName>
        <apiVersion>38.0</apiVersion>
        <description>Send Case Transaction Queue Records to Boomi</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNCase</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>External_Source_ID__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Start_Time__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>debbie.chamkasem@experian.global</integrationUser>
        <name>Case TransQueue Record Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Contact_TransQueue_Record_Send_to_Boomi</fullName>
        <apiVersion>38.0</apiVersion>
        <description>Send Contact Transaction Queue Record to Boomi</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNContact</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>External_Source_ID__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Start_Time__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>richard.joseph@experian.global</integrationUser>
        <name>Contact TransQueue Record Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Merge_TransQueue_Record_Send_to_Boomi</fullName>
        <apiVersion>39.0</apiVersion>
        <description>Send Account or Contact merge Transaction Queue Records to Boomi</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNMerge</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>External_Source_ID__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>richard.joseph@experian.global</integrationUser>
        <name>Merge TransQueue Record Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Product_TransQueue_Record_Send_to_Boomi</fullName>
        <apiVersion>38.0</apiVersion>
        <description>Send Product Transaction Queue Records to Boomi</description>
        <endpointUrl>https://boomi.prod.uk.experian.com/ws/simple/getSFSNProduct</endpointUrl>
        <fields>Action_Type__c</fields>
        <fields>External_Source_ID__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Operation_Type__c</fields>
        <fields>SF_Record_ID__c</fields>
        <fields>Target_Application__c</fields>
        <fields>Transaction_Object_Name__c</fields>
        <fields>Transaction_Status__c</fields>
        <fields>Transaction_Type__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>richard.joseph@experian.global</integrationUser>
        <name>Product TransQueue Record Send to Boomi</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Account Trans Queue Send to Boomi</fullName>
        <actions>
            <name>Account_TransQueue_Record_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When an Account record  is created send it to to  Boomi (NOTE: Temporarily modified to use an temp TransactonStatus of Start to prevent large # of OBMS from firing and also included Updates on the record.</description>
        <formula>AND (  NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c), TEXT(Transaction_Object_Name__c) ==&apos;Account&apos;,  Text(Transaction_Status__c)== &apos;New&apos;,  Text(Target_Application__c)  ==&apos;ServiceNow&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account or Contact Merge TransQueue Record Send to Boomi</fullName>
        <actions>
            <name>Merge_TransQueue_Record_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When an Account or Contact is merged  and created a new Record in Transaction queue send it to to  Boomi</description>
        <formula>AND (   NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c),   TEXT(Transaction_Object_Name__c) ==&apos;MergeHistory__c&apos;,  Text(Transaction_Status__c)== &apos;New&apos;,  Text(Target_Application__c)  ==&apos;ServiceNow&apos;, OR(TEXT( Operation_Type__c )=&apos;AccountMerge&apos; , TEXT( Operation_Type__c )=&apos;ContactMerge&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Asset Trans Queue Send to Boomi</fullName>
        <actions>
            <name>Asset_TransQueue_Record_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When an Asset record  is created send it to to  Boomi (NOTE: Temporarily modified to use an temp TransactonStatus of Start to prevent large # of OBMS from firing and also included Updates on the record.</description>
        <formula>AND (  NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c),   TEXT(Transaction_Object_Name__c) ==&apos;Asset&apos;,  Text(Transaction_Status__c)== &apos;New&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Trans Queue Send to Boomi</fullName>
        <actions>
            <name>Case_TransQueue_Record_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When a Case  record  is created send it to to  Boomi</description>
        <formula>AND (  NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c),  TEXT(Transaction_Object_Name__c) ==&apos;Case&apos;, Text(Transaction_Status__c) ==&apos;New&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CaseAttachment Trans Queue Send to Boomi</fullName>
        <actions>
            <name>CaseAttachmnt_TransQue_Rec_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When a CaseAttachment  record  is created send it to to  Boomi</description>
        <formula>AND (  NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c),  TEXT(Transaction_Object_Name__c) ==&apos;CaseAttachment&apos;, Text(Transaction_Status__c) ==&apos;New&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CaseComment Trans Queue Send to Boomi</fullName>
        <actions>
            <name>CaseComment_TransQue_Rec_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When a CaseComment  record  is created send it to to  Boomi</description>
        <formula>AND (  NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c),  TEXT(Transaction_Object_Name__c) ==&apos;CaseComment&apos;, Text(Transaction_Status__c) ==&apos;New&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Trans Queue Send to Boomi</fullName>
        <actions>
            <name>Contact_TransQueue_Record_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When a Contact record  is created send it to to  Boomi  NOTE: Temp condition added for Edited or created. Need to remove Edited after initial testing</description>
        <formula>AND (   NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c),
TEXT(Transaction_Object_Name__c) ==&apos;Contact&apos;,  Text(Transaction_Status__c)==&apos;New&apos;,  Text(Target_Application__c)  ==&apos;ServiceNow&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ProductTrans Queue Send to Boomi</fullName>
        <actions>
            <name>Product_TransQueue_Record_Send_to_Boomi</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>When a Product record  is created send it to to  Boomi (NOTE: Temporarily modified to use an temp TransactonStatus of Start to prevent large # of OBMS from firing and also included Updates on the record.</description>
        <formula>AND (   NOT($Setup.isITSMSkipUser__c.isITSMSkipUser__c),   TEXT(Transaction_Object_Name__c) ==&apos;Product_Master__c&apos;,  OR (   Text(Transaction_Status__c)== &apos;Start&apos; ,  Text(Transaction_Status__c)== &apos;New&apos;  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SendTransaction Queue to Boomi</fullName>
        <active>false</active>
        <description>When a record  is created send it to to  Boomi</description>
        <formula>NOT($Setup.IsDataAdmin__c.IsDataAdmin__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Transaction Queue with Status New or Inprogress</fullName>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Transaction_Queue__c.Transaction_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Transaction_Queue__c.Transaction_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Transaction Queue record with  Status New or Inprogress in Queue for 15 minutes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Transaction_Queue_Status_is_New_or_In_progress_for_15_mins_alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Transaction_Queue__c.Time_Trigger_15__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
