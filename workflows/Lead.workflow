<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CCM_Lead_Notification</fullName>
        <description>CCM Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>nicole.seidman1@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_Lead_Owner_After_2_Working_days_of_Lead_Creation</fullName>
        <description>Email Alert to Lead Owner After 2 Working days of Lead Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>mark.parysz@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tyaga.pati@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesshare@experian.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/X2_Day_Pending_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>Lead_Disqualified_Email_Notification</fullName>
        <ccEmails>marketing.services@uk.experian.com</ccEmails>
        <description>Lead Disqualified Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadDisqualifiedNotification</template>
    </alerts>
    <alerts>
        <fullName>Lead_Lead_to_be_sent_back_to_Marketing_in_3_days</fullName>
        <description>Lead: Lead to be sent back to Marketing in 3 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Lead_to_be_sent_back_to_Marketing_in_3_days</template>
    </alerts>
    <alerts>
        <fullName>Lead_Rejected_Email_Notification</fullName>
        <ccEmails>marketing.services@uk.experian.com</ccEmails>
        <description>Lead Rejected Email Notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadRejectedNotification</template>
    </alerts>
    <alerts>
        <fullName>Lead_Reminder_to_Accept_or_Reject_New_Lead</fullName>
        <description>Lead: Reminder to Accept or Reject New Lead</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Reminder_to_Accept_or_Reject_New_Lead</template>
    </alerts>
    <alerts>
        <fullName>Lead_Reminder_to_Convert_or_Disqualify_Lead</fullName>
        <description>Lead: Reminder to Convert or Disqualify Lead</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Reminder_to_Convert_or_Disqualify_Lead</template>
    </alerts>
    <alerts>
        <fullName>NA_MS_Targeting_Lead_Notification</fullName>
        <description>NA MS Targeting Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Business_Informa</fullName>
        <ccEmails>linda.jackson@experian.com</ccEmails>
        <ccEmails>salesshare@experian.com</ccEmails>
        <description>SalesShare Leads: New assignment notification for Non SFDC Users Business Information Services</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Consumer_Informa</fullName>
        <ccEmails>sandy.anderson@experian.com</ccEmails>
        <ccEmails>salesshare@experian.com</ccEmails>
        <description>SalesShare Leads: New assignment notification for Non SFDC Users Consumer Information Services</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Consumer_Insight</fullName>
        <ccEmails>salesshare@experian.com</ccEmails>
        <ccEmails>pamela.barlow@experian.com</ccEmails>
        <ccEmails>kaj.seel@experian.com</ccEmails>
        <description>SalesShare Leads: New assignment notification for Non SFDC Users Consumer Insights</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Consumer_Service</fullName>
        <ccEmails>Michael.Vaccaro@experianinteractive.com</ccEmails>
        <ccEmails>salesshare@experian.com</ccEmails>
        <description>SalesShare Leads: New assignment notification for Non SFDC Users Consumer Services</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Decision_Analyti</fullName>
        <ccEmails>salesshare@experian.com</ccEmails>
        <ccEmails>Lindsey.Wood@experian.com</ccEmails>
        <description>SalesShare Leads: New assignment notification for Non SFDC Users Decision Analytics</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Experian_Automot</fullName>
        <ccEmails>autosupport@experian.com</ccEmails>
        <ccEmails>salesshare@experian.com</ccEmails>
        <description>SalesShare Leads: New assignment notification for Non SFDC Users Experian Automotive</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users</template>
    </alerts>
    <alerts>
        <fullName>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Healthcare</fullName>
        <ccEmails>salesshare@experian.com</ccEmails>
        <ccEmails>scott.lee@experian.com</ccEmails>
        <ccEmails>Paige.Kinnett@experian.com</ccEmails>
        <description>SalesShare Leads: New assignment notification for Non SFDC Users Healthcare</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lead_Region_to_APAC</fullName>
        <field>Region__c</field>
        <literalValue>APAC</literalValue>
        <name>Lead Region to APAC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Region_to_EMEA</fullName>
        <field>Region__c</field>
        <literalValue>EMEA</literalValue>
        <name>Lead Region to EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Region_to_LATAM</fullName>
        <field>Region__c</field>
        <literalValue>Latin America</literalValue>
        <name>Lead Region to LATAM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Region_to_NA</fullName>
        <field>Region__c</field>
        <literalValue>North America</literalValue>
        <name>Lead Region to NA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Region_to_UK_I</fullName>
        <field>Region__c</field>
        <literalValue>UK&amp;I</literalValue>
        <name>Lead Region to UK&amp;I</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_CRMOD_ID_on_Lead_Object</fullName>
        <description>Set CRMOD ID on Lead Object</description>
        <field>CRMOD_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Set CRMOD ID on Lead Object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conference_Phone_for_North_America</fullName>
        <description>Inside Sales Project.</description>
        <field>Conference_Phone__c</field>
        <formula>&apos;972-390-5222&apos;</formula>
        <name>Set Conference Phone for North America</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Conference_Phone_for_UK_I</fullName>
        <field>Conference_Phone__c</field>
        <formula>&apos;+442037703375&apos;</formula>
        <name>Set Conference Phone for UK&amp;I</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Who_Rejected_to_Sales_Marketing</fullName>
        <field>Who_Rejected__c</field>
        <formula>Text(Owner:User.Business_Unit__c)</formula>
        <name>Set Who Rejected to Sales/Marketing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Owner_to_CSDA_DA_US_DSS</fullName>
        <description>update the lead owner field to the  CSDA DA US DSS Lead Queue</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_DA_US_DSS</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Lead Owner to CSDA DA US DSS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Owner_to_CSDA_DA_US_Lead_Que</fullName>
        <description>update the lead owner field to the CSDA DA US Lead Queue</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_DA_US_Lead_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Lead Owner to CSDA DA US Lead Que</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Owner_to_Marketing_Disqulfy</fullName>
        <field>OwnerId</field>
        <lookupValue>Marketing_Disqualified</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Lead Owner to Marketing Disqulfy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Owner_to_Marketing_Rejected</fullName>
        <field>OwnerId</field>
        <lookupValue>Marketing_Rejected</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Lead Owner to Marketing Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_to_Disqualified</fullName>
        <field>Status</field>
        <literalValue>Disqualified</literalValue>
        <name>Update Lead Status to Disqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Lead Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_lead_owner_to_CSDA_DA_US_FID</fullName>
        <description>update the lead owner field to the CSDA DA US FID Lead Queue</description>
        <field>OwnerId</field>
        <lookupValue>CSDA_DA_US_FID</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update lead owner to CSDA DA US FID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CCM Lead Notification</fullName>
        <actions>
            <name>CCM_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Martina Midlin</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Lead to be sent back to Marketing in 3 days%2C Convert or Disqualify</fullName>
        <active>true</active>
        <formula>AND(  OR(   Owner:User.UserRole.Name  &lt;&gt; &quot;Inside Sales&quot;,   CONTAINS(Owner:User.Profile.Name,&quot;Experian Marketing Administrator&quot;)  ),  OR(   ISPICKVAL(Status, &quot;Open&quot;),   ISPICKVAL(Status, &quot;Qualifying&quot;),   ISPICKVAL(Status, &quot;Sales Accepted&quot;)  ),  NOT( Abort_Timed_Workflow__c ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Reminder_to_Convert_or_Disqualify_Lead</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Lead_to_be_sent_back_to_Marketing_in_3_days</name>
                <type>Alert</type>
            </actions>
            <timeLength>28</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead%3A Reminder to Accept or Reject New Lead</fullName>
        <active>true</active>
        <formula>AND(  OR(   Owner:User.UserRole.Name  &lt;&gt; &quot;Inside Sales&quot;,   CONTAINS(Owner:User.Profile.Name,&quot;Experian Marketing Administrator&quot;)  ),  OR(   ISPICKVAL(Status, &quot;Open&quot;),   ISPICKVAL(Status, &quot;Qualifying&quot;)  ),  NOT( Abort_Timed_Workflow__c ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_Reminder_to_Accept_or_Reject_New_Lead</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>NA MS Targeting Routing</fullName>
        <actions>
            <name>NA_MS_Targeting_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(CONTAINS( CreatedBy.FirstName + CreatedBy.LastName,&quot;Marketing&quot;), NOT(CONTAINS(Owner:User.Profile.Name, &quot;Inside Sales&quot;)), (ISPICKVAL(Region__c, &quot;North America&quot;)), (NOT(CONTAINS( Owner_s_Business_Unit__c, &quot;Data Quality&quot;))), (Owner_s_Business_Unit__c&lt;&gt;null))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Rule to Send Email 2 Days Of Lead Creation</fullName>
        <active>true</active>
        <description>This is the workflow rule to fire emails to remind users of assigned leads after 2 business days of their creation and its edited subsequently meet the criteria  if the status has not changed since creation.</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ISPICKVAL( Status , &quot;Open&quot;), ISPICKVAL( LeadSource , &quot;SalesShare&quot;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Alert_to_Lead_Owner_After_2_Working_days_of_Lead_Creation</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Calc_2_Day_Email_Alert__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SalesShare Leads%3A New assignment notification for Non SFDC Users Business Information Services</fullName>
        <actions>
            <name>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Business_Informa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>SalesShare Catch all</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Business_Unit_SalesShare__c</field>
            <operation>equals</operation>
            <value>Business Information Services</value>
        </criteriaItems>
        <description>External notification to lead owners when new SalesShare lead is assigned to SalesShare Catch all for Business Information Services</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Leads%3A New assignment notification for Non SFDC Users Consumer Information Services</fullName>
        <actions>
            <name>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Consumer_Informa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>SalesShare Catch all</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Business_Unit_SalesShare__c</field>
            <operation>equals</operation>
            <value>Consumer Information Services</value>
        </criteriaItems>
        <description>External notification to lead owners when new SalesShare lead is assigned to SalesShare Catch all for Consumer Information Services</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Leads%3A New assignment notification for Non SFDC Users Consumer Insights</fullName>
        <actions>
            <name>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Consumer_Insight</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>SalesShare Catch all</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Business_Unit_SalesShare__c</field>
            <operation>equals</operation>
            <value>Consumer Insights</value>
        </criteriaItems>
        <description>External notification to lead owners when new SalesShare lead is assigned to SalesShare Catch all for Consumer Insights</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Leads%3A New assignment notification for Non SFDC Users Consumer Services</fullName>
        <actions>
            <name>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Consumer_Service</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>SalesShare Catch all</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Business_Unit_SalesShare__c</field>
            <operation>equals</operation>
            <value>Consumer Services</value>
        </criteriaItems>
        <description>External notification to lead owners when new SalesShare lead is assigned to SalesShare Catch all for Consumer Services</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Leads%3A New assignment notification for Non SFDC Users Decision Analytics</fullName>
        <actions>
            <name>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Decision_Analyti</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>SalesShare Catch all</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Business_Unit_SalesShare__c</field>
            <operation>equals</operation>
            <value>Decision Analytics</value>
        </criteriaItems>
        <description>External notification to lead owners when new SalesShare lead is assigned to SalesShare Catch all for Decision Analytics</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Leads%3A New assignment notification for Non SFDC Users Experian Automotive</fullName>
        <actions>
            <name>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Experian_Automot</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>SalesShare Catch all</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Business_Unit_SalesShare__c</field>
            <operation>equals</operation>
            <value>Experian Automotive</value>
        </criteriaItems>
        <description>External notification to lead owners when new SalesShare lead is assigned to SalesShare Catch all for Experian Automotive</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SalesShare Leads%3A New assignment notification for Non SFDC Users Healthcare</fullName>
        <actions>
            <name>SalesShare_Leads_New_assignment_notification_for_Non_SFDC_Users_Healthcare</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>SalesShare Catch all</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>SalesShare</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Business_Unit_SalesShare__c</field>
            <operation>equals</operation>
            <value>Healthcare</value>
        </criteriaItems>
        <description>External notification to lead owners when new SalesShare lead is assigned to SalesShare Catch all for Healthcare</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email Alert  to Marketing Queue when Disqualified</fullName>
        <actions>
            <name>Lead_Disqualified_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email Alert  to Marketing Queue when Lead is Disqualified</description>
        <formula>AND( ISPICKVAL(Status,&quot;Disqualified&quot;), NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),
CONTAINS( $Profile.Name ,&quot;Sales&quot;),
Text(Owner:User.Business_Unit__c) = &quot;UK&amp;I MS CI&amp;T&quot; 
 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email Alert  to Marketing Queue when Rejected</fullName>
        <actions>
            <name>Lead_Rejected_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Who_Rejected_to_Sales_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Send Email Alert  to Marketing Queue when Lead is Rejected</description>
        <formula>AND( ISPICKVAL(Status,&quot;Rejected&quot;), NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),
CONTAINS( $Profile.Name ,&quot;Sales&quot;),
Text(Owner:User.Business_Unit__c) = &quot;UK&amp;I MS CI&amp;T&quot;
 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Conference Phone for North America</fullName>
        <actions>
            <name>Set_Conference_Phone_for_North_America</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Region__c</field>
            <operation>equals</operation>
            <value>North America</value>
        </criteriaItems>
        <description>Inside Sales Project.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Conference Phone for UK%26I</fullName>
        <actions>
            <name>Set_Conference_Phone_for_UK_I</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Region__c</field>
            <operation>equals</operation>
            <value>UK&amp;I</value>
        </criteriaItems>
        <description>Inside Sales UK&amp;I Project.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update CRMOD ID on new Leads created in SFDC</fullName>
        <actions>
            <name>Set_CRMOD_ID_on_Lead_Object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CRMOD_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update CRMoD ID on new Leads created in SFDC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Owner to Marketing Disqualified</fullName>
        <actions>
            <name>Update_Lead_Owner_to_Marketing_Disqulfy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISPICKVAL(Status,&quot;Disqualified&quot;), NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Owner to Marketing Rejected</fullName>
        <actions>
            <name>Update_Lead_Owner_to_Marketing_Rejected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( ISPICKVAL(Status,&quot;Rejected&quot;), NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead User Region to APAC</fullName>
        <actions>
            <name>Lead_Region_to_APAC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), OwnerId &lt;&gt; PRIORVALUE(OwnerId), TEXT(Region__c) = TEXT(PRIORVALUE(Region__c)), ISPICKVAL(Owner:User.Region__c, &apos;APAC&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead User Region to EMEA</fullName>
        <actions>
            <name>Lead_Region_to_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), OwnerId &lt;&gt; PRIORVALUE(OwnerId), TEXT(Region__c) = TEXT(PRIORVALUE(Region__c)), ISPICKVAL(Owner:User.Region__c, &apos;EMEA&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead User Region to Latin America</fullName>
        <actions>
            <name>Lead_Region_to_LATAM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), OwnerId &lt;&gt; PRIORVALUE(OwnerId), TEXT(Region__c) = TEXT(PRIORVALUE(Region__c)), ISPICKVAL(Owner:User.Region__c, &apos;Latin America&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead User Region to North America</fullName>
        <actions>
            <name>Lead_Region_to_NA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), OwnerId &lt;&gt; PRIORVALUE(OwnerId), TEXT(Region__c) = TEXT(PRIORVALUE(Region__c)), ISPICKVAL(Owner:User.Region__c, &apos;North America&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead User Region to UK%26I</fullName>
        <actions>
            <name>Lead_Region_to_UK_I</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), OwnerId &lt;&gt; PRIORVALUE(OwnerId), TEXT(Region__c) = TEXT(PRIORVALUE(Region__c)), ISPICKVAL(Owner:User.Region__c, &apos;UK&amp;I&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update lead owner to CSDA DA US DSS Lead Queue</fullName>
        <actions>
            <name>Update_Lead_Owner_to_CSDA_DA_US_DSS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a lead is updated as rejected or disqualified and the owner sales sub-team is set to NA DA - Decisioning , move the lead to CSDA DA US DSS Lead Queue</description>
        <formula>AND(ISCHANGED( Status ), OR(ISPICKVAL(Status, &apos;Rejected&apos;),ISPICKVAL(Status, &apos;Disqualified&apos;)), ISPICKVAL(Owner:User.Sales_Sub_Team__c, &apos;NA DA - Decisioning&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update lead owner to CSDA DA US FID Lead Queue</fullName>
        <actions>
            <name>Update_lead_owner_to_CSDA_DA_US_FID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a lead is updated as rejected or disqualified and the owner sales sub-team is set to NA DA - Fraud  , move the lead to CSDA DA US FID Lead Queue</description>
        <formula>AND(ISCHANGED( Status ), OR(ISPICKVAL(Status, &apos;Rejected&apos;),ISPICKVAL(Status, &apos;Disqualified&apos;)), ISPICKVAL(Owner:User.Sales_Sub_Team__c, &apos;NA DA - Fraud&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update lead owner to CSDA DA US Lead Queue</fullName>
        <actions>
            <name>Update_Lead_Owner_to_CSDA_DA_US_Lead_Que</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a lead is updated as rejected or disqualified and the owner is set to NA DA or NA DA Fraud and Indentity move the lead to the DA US Queue</description>
        <formula>AND(ISCHANGED( Status ),  OR(ISPICKVAL(Status, &apos;Rejected&apos;),ISPICKVAL(Status, &apos;Disqualified&apos;)),  OR(ISPICKVAL(Owner:User.Business_Unit__c, &apos;NA DA&apos;),ISPICKVAL(Owner:User.Business_Unit__c, &apos;NA DA Fraud and Identity&apos;)),  NOT(OR(ISPICKVAL(Owner:User.Sales_Sub_Team__c, &apos;NA DA - Fraud&apos;),ISPICKVAL(Owner:User.Sales_Sub_Team__c, &apos;NA DA - Decisioning&apos;)))  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
