<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Sent_to_Serasa_Knowledge_Managers</fullName>
        <description>Email Sent to Serasa Knowledge Managers</description>
        <protected>false</protected>
        <recipients>
            <recipient>sarah.paretti@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Article_Created</template>
    </alerts>
    <rules>
        <fullName>Serasa Knowledge Managers - New FAQ Article</fullName>
        <actions>
            <name>Email_Sent_to_Serasa_Knowledge_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>FAQ__kav.ArticleType</field>
            <operation>equals</operation>
            <value>FAQ</value>
        </criteriaItems>
        <description>Email is sent to Knowledge Managers once FAQ articles created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
