<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NA_MS_CCM_Lead_Notification</fullName>
        <description>NA MS CCM Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>da-janae.fabian@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dave.evans@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jamie.helf@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zahra.asgari@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>NA_MS_CCM_Task_Notification</fullName>
        <description>NA MS CCM Task Notification v2</description>
        <protected>false</protected>
        <recipients>
            <recipient>da-janae.fabian@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>dave.evans@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jamie.helf@experian.global</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zahra.asgari@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CCM_NA_New_Assignment_Notification</template>
    </alerts>
    <alerts>
        <fullName>Partner_Email</fullName>
        <description>Partner Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>esther.sigona@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Partner_Request</template>
    </alerts>
    <rules>
        <fullName>Notification Partner Campaign Member</fullName>
        <actions>
            <name>Partner_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Movable Ink,Liveclicker,Return Path,SmarterHQ,MyBuys,Persado,Opt-Intelligence</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email Alert to Marketing NA MS CCM Users</fullName>
        <actions>
            <name>NA_MS_CCM_Task_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>contains</operation>
            <value>Automation</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>NA MS CCM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Name</field>
            <operation>equals</operation>
            <value>Test Campaign</value>
        </criteriaItems>
        <description>When a Contact Activity/Task is created by the Marketing Automation User for a NA MS CCM user the notification email sent to the lead owner also needs to be sent to the NA MS CCM Marketers</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
