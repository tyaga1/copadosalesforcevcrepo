<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Total_Opportunity_Margin</fullName>
        <description>Total Opportunity Margin value populated on Sales Planning Process Opportunity from parent opportunity record</description>
        <field>Opportunity_Margin__c</field>
        <formula>Opportunity__r.Total_Margin_Value__c</formula>
        <name>Total Opportunity Margin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Sales Planning Process Opportunity Margin</fullName>
        <actions>
            <name>Total_Opportunity_Margin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sales Planning Process Opportunity Margin stamped</description>
        <formula>not(  ISNULL( Opportunity__r.LastModifiedById )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
