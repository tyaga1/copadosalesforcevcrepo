<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Auto_notification_of_change_in_product_name</fullName>
        <description>Auto notification of change in product name</description>
        <protected>false</protected>
        <recipients>
            <recipient>UK_I_Service_Strategy_and_Design_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Product_Name_Change_to_UK_I_and_Global_products</template>
    </alerts>
    <alerts>
        <fullName>Auto_notification_of_change_in_product_status</fullName>
        <description>Auto notification of change in product status</description>
        <protected>false</protected>
        <recipients>
            <recipient>UK_I_Service_Strategy_and_Design_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Status_Change_to_UK_I_and_Global_products</template>
    </alerts>
    <fieldUpdates>
        <fullName>Product_Master_Name_Update</fullName>
        <field>Name</field>
        <formula>Product_Name__c</formula>
        <name>Product Master Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto notification of change in product Name</fullName>
        <actions>
            <name>Auto_notification_of_change_in_product_name</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Group Notification when a UK&amp;I Product  Name is changed</description>
        <formula>AND(     ISCHANGED(  Name  ),     INCLUDES(Region__c, &apos;UK&amp;I&apos;),      CreatedDate  &lt;&gt;  LastModifiedDate      )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Auto notification of change in product status</fullName>
        <actions>
            <name>Auto_notification_of_change_in_product_status</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Group Notification when a UK&amp;I Product status is changed</description>
        <formula>AND(     ISCHANGED( Product_life_cycle__c ),     INCLUDES(Region__c, &apos;UK&amp;I&apos;),      CreatedDate  &lt;&gt;  LastModifiedDate      )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Product Master Name</fullName>
        <actions>
            <name>Product_Master_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(OR( Name = null ,  Product_Name__c  &lt;&gt;  Name), Product_Name__c &lt;&gt; null)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
