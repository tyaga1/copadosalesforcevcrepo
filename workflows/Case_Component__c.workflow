<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Developer_to_review_code_change</fullName>
        <description>Email to Developer to review code change</description>
        <protected>false</protected>
        <recipients>
            <recipient>Code_Review_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Code_Review_Required</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Review_Required_field_to_True</fullName>
        <description>This workflow field update to set Code Review Required field in Item manifest to be TRUE.</description>
        <field>Code_review_required__c</field>
        <literalValue>1</literalValue>
        <name>Set Review Required field to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Code Review Required is TRUE</fullName>
        <actions>
            <name>Email_to_Developer_to_review_code_change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case_Component__c.Code_review_required__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case_Component__c.Code_changes_Approved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>The workflow will send email to reviewer for review.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Code Review to be set to TRUE</fullName>
        <actions>
            <name>Set_Review_Required_field_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case_Component__c.Metadata_Type__c</field>
            <operation>equals</operation>
            <value>ApexClass</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case_Component__c.Code_review_required__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>The workflow will update Code Review Required when Metadata Type is equal to Apex Class</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
