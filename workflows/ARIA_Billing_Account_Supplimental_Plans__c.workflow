<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Experian_ID_on_Billing_Accnt_SupPlan</fullName>
        <description>Set Experian ID on Billing Accnt Supplemental Plan to SFDC ID</description>
        <field>Experian_ID__c</field>
        <formula>Id</formula>
        <name>Set Experian ID on Billing Accnt SupPlan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Experian ID on Billing Accnt SupplePlan</fullName>
        <actions>
            <name>Set_Experian_ID_on_Billing_Accnt_SupPlan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ARIA_Billing_Account_Supplimental_Plans__c.Parent_Plan_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set Experian ID on Billing Accnt SupplePlan object to SFDC ID</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
