<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>NA_CS_DA_Fraud_Loss</fullName>
        <description>NA CS/DA Fraud Loss</description>
        <protected>false</protected>
        <recipients>
            <recipient>debbie.sutherland@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Targeting_Renewal_has_been_abandoned_lost</template>
    </alerts>
    <alerts>
        <fullName>NA_CS_DA_Fraud_Win</fullName>
        <description>NA CS/DA Fraud Win</description>
        <protected>false</protected>
        <recipients>
            <recipient>debbie.sutherland@experian.global</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CCM_NA_Opportunity_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_SeismicProductTag</fullName>
        <description>Brings SeismicProductTag from the related product master into the opportunity</description>
        <field>SeismicProductTag__c</field>
        <formula>PricebookEntry.Product2.Product_Master__r.SeismicProductTag__c</formula>
        <name>Add SeismicProductTag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EDQ_Integration_Id</fullName>
        <field>EDQ_Integration_Id__c</field>
        <formula>Id</formula>
        <name>EDQ Integration Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internation_Product_Attached</fullName>
        <description>Check that an international product was added to an opportunity</description>
        <field>International_Product_Attached__c</field>
        <literalValue>1</literalValue>
        <name>Internation Product Attached</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Is_International_Product_True</fullName>
        <description>Flags opportunity product &apos;Is International Product&apos; TRUE</description>
        <field>Is_International_Product__c</field>
        <literalValue>1</literalValue>
        <name>Is International Product True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Original_Sales_Price</fullName>
        <description>Field to capture the original sales price when the product is added to the opportunity</description>
        <field>Original_Sales_Price__c</field>
        <formula>UnitPrice</formula>
        <name>Original Sales Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_BI_spilt</fullName>
        <field>Serasa_BI_Split__c</field>
        <formula>PricebookEntry.Product2.Serasa_BI_Split__c</formula>
        <name>Serasa BI spilt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_CI_split</fullName>
        <field>Serasa_CI_Split__c</field>
        <formula>PricebookEntry.Product2.Serasa_CI_Split__c</formula>
        <name>Serasa CI split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_DA_Split</fullName>
        <field>Serasa_DA_Split__c</field>
        <formula>PricebookEntry.Product2.Serasa_DA_Split__c</formula>
        <name>Serasa DA Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_ECS_split</fullName>
        <field>Serasa_ECS_Split__c</field>
        <formula>PricebookEntry.Product2.Serasa_ECS_Split__c</formula>
        <name>Serasa ECS split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_ID_split</fullName>
        <field>Serasa_ID_Split__c</field>
        <formula>PricebookEntry.Product2.Serasa_ID_Split__c</formula>
        <name>Serasa ID split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Serasa_MS_split</fullName>
        <field>Serasa_MS_Split__c</field>
        <formula>PricebookEntry.Product2.Serasa_MS_Split__c</formula>
        <name>Serasa MS split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_End_Dt_to_OptyProd_End_Dt</fullName>
        <description>Set_Contract_End_Date_to_OptyProd_End_Date</description>
        <field>Contract_End_Date__c</field>
        <formula>End_Date__c</formula>
        <name>Set Contract End Dt to OptyProd End Dt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contract_Start_Dt_to_OptyProd_St_Dt</fullName>
        <description>Set Contract Start Dt to OptyProd Start Date</description>
        <field>Contract_Start_Date__c</field>
        <formula>Start_Date__c</formula>
        <name>Set Contract Start Dt to OptyProd St Dt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Item_Number_to_be_OptyLineItemId</fullName>
        <description>Set Item Number to be OptyLineItemId</description>
        <field>Item_Number__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Set Item Number to be OptyLineItemId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_By</fullName>
        <description>Case 01199320</description>
        <field>Last_Modified_By_Not_System__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Set Last Modified By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Modified_Date</fullName>
        <description>Case 01199320</description>
        <field>Last_Modified_Date_Not_System__c</field>
        <formula>NOW()</formula>
        <name>Set Last Modified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Product_Term</fullName>
        <description>To enable calculation of the NBQ field</description>
        <field>Product_Term__c</field>
        <formula>((YEAR(End_Date__c ) - YEAR(Start_Date__c ) - 1) *12) + (12 - (MONTH(Start_Date__c )) + MONTH(End_Date__c )) + ((DAY(End_Date__c)-DAY(Start_Date__c))/CASE(MONTH(End_Date__c ),1,31,2,IF(OR(MOD(YEAR(End_Date__c),400)=0,AND(MOD(YEAR(End_Date__c ),4)=0,MOD(YEAR(End_Date__c ),100)&lt;&gt;0)),29,28),3,31,5,31,7,31,8,31,10,31,12,31,30))</formula>
        <name>Set Product Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CS_Product</fullName>
        <field>CS_product__c</field>
        <literalValue>1</literalValue>
        <name>Update CS Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Consumer_Services_Product</fullName>
        <field>ConS_product__c</field>
        <literalValue>1</literalValue>
        <name>Update Consumer Services Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DA_Product</fullName>
        <field>DA_product__c</field>
        <literalValue>1</literalValue>
        <name>Update DA Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MS_Product</fullName>
        <field>MS_product__c</field>
        <literalValue>1</literalValue>
        <name>Update MS Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Add SeismicProductTag</fullName>
        <actions>
            <name>Add_SeismicProductTag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.SeismicProductTag__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Brings the Product Master&apos;s SeismicProductTag into the related Opportunity</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Capture Original Sales Price</fullName>
        <actions>
            <name>Original_Sales_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Order_Item_ID_18__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>workflow to capture the sales price set on product creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Has CS Product</fullName>
        <actions>
            <name>Update_CS_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Global_Business_Line__c</field>
            <operation>equals</operation>
            <value>Credit Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Has Consumer Services Product</fullName>
        <actions>
            <name>Update_Consumer_Services_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Global_Business_Line__c</field>
            <operation>equals</operation>
            <value>Experian Consumer Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Has DA Product</fullName>
        <actions>
            <name>Update_DA_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Global_Business_Line__c</field>
            <operation>equals</operation>
            <value>Decision Analytics</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Has MS Product</fullName>
        <actions>
            <name>Update_MS_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Global_Business_Line__c</field>
            <operation>equals</operation>
            <value>Marketing Services</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NA CS%2FDA Fraud Losses</fullName>
        <actions>
            <name>NA_CS_DA_Fraud_Loss</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( CONTAINS(TEXT(Product2.CSDA_Product_Suite__c), &apos;FRAUD&apos;)  , ISPICKVAL(Opportunity.StageName, &apos;Closed Lost&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NA CS%2FDA Fraud Wins</fullName>
        <actions>
            <name>NA_CS_DA_Fraud_Win</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( CONTAINS(TEXT(Product2.CSDA_Product_Suite__c), &apos;FRAUD&apos;) 
, ISPICKVAL(Opportunity.StageName, &apos;Execute&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Product%3A Internation Product Attached</fullName>
        <actions>
            <name>Internation_Product_Attached</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Is_International_Product_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check the International Product field in Opportunities once an international product has been added to an opportunity.</description>
        <formula>AND(OR(     Product2.Name = &apos;BusinessIQ Global Linkages&apos;,     Product2.Name = &apos;BusinessIQ International Reports&apos;,     Product2.Name = &apos;Business Summary - Canadian&apos;,     Product2.Name = &apos;E-series Business International Reports&apos;,     Product2.Name = &apos;Global Data Network&apos;,     Product2.Name = &apos;International Business Credit Profile&apos;,     Product2.Name = &apos;International Developed Profile&apos;,     Product2.Name = &apos;International GBIN with Corporate Linkage append&apos;,     Product2.Name = &apos;International Reports&apos;,     Product2.Name = &apos;IRE International Reports&apos;,     Product2.Name = &apos;Supplemental international credit element append&apos;,     Product2.Name = &apos;Foreign reports&apos;),     Opportunity.International_Product_Attached__c = false )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Serasa BU Percentage Split updates</fullName>
        <actions>
            <name>Serasa_BI_spilt</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_CI_split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_DA_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_ECS_split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_ID_split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Serasa_MS_split</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User.Business_Line__c</field>
            <operation>equals</operation>
            <value>LATAM Corporate,LATAM Credit Services,LATAM Decision Analytics,LATAM Marketing Services,LATAM Serasa Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>System Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Serasa</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Contract End Date on Opty Product if %3C Product End Date</fullName>
        <actions>
            <name>Set_Contract_End_Dt_to_OptyProd_End_Dt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>if the product end date is greater than the contract end date update the contract end date to the product end date   Deactivated as a part of case:02171191</description>
        <formula>OR(
 End_Date__c &gt; Opportunity.Contract_End_Date__c,
 ISBLANK(Opportunity.Contract_End_Date__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Contract Start Date on Opty Product if %3E Product Start Date</fullName>
        <actions>
            <name>Set_Contract_Start_Dt_to_OptyProd_St_Dt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>if the product start date is less than the contract start date update the contract start date to the product start date - Deactivated as a part of case:02171191</description>
        <formula>OR(
 Start_Date__c &lt; Opportunity.Contract_Start_Date__c,
 ISBLANK(Opportunity.Contract_Start_Date__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Item Number to be OptyLineItemID</fullName>
        <actions>
            <name>Set_Item_Number_to_be_OptyLineItemId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Item_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set Item Number on OptyLineItem to be Opty Record ID when it is blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Product Term</fullName>
        <actions>
            <name>Set_Product_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow sets the value of the Product Term for the NBQ calculation</description>
        <formula>OR(ISCHANGED(Start_Date__c), ISCHANGED(End_Date__c), ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update EDQ Integration Id on new OpportunityLineItems created in SFDC</fullName>
        <actions>
            <name>EDQ_Integration_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.EDQ_Integration_Id__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Modified Custom Functionality</fullName>
        <actions>
            <name>Set_Last_Modified_By</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Last_Modified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Case 01199320</description>
        <formula>NOT($Permission.Bypass_Last_Modified_By)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
