<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Global_Project_Delivery_Line_Overdue_Alert</fullName>
        <description>Global Project Delivery Line Overdue Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Owner_s_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Project_Delivery_Line_Overdue</template>
    </alerts>
    <alerts>
        <fullName>Global_Project_Delivery_Line_Status_or_Due_Date_Change</fullName>
        <description>Global Project Delivery Line Status or Due Date Change</description>
        <protected>false</protected>
        <recipients>
            <field>Project_Owner_s_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Project_Delivery_Line_status_or_date_change</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_To_APAC_MS_CI_T_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_CI_T</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign To APAC MS CI&amp;T Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_APAC_MS_CCM_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_CCM</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC MS CCM Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_APAC_MS_CI_T_CCM_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_CCM</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC MS CI&amp;T CCM Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_APAC_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>APAC_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to APAC MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_EMEA_MS_Spain_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>EMEA_MS_Spain</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to EMEA MS Spain Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_NA_CS_Automotive_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_CS_Automotive</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to NA CS Automotive Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_NA_MS_CI_T_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_MS_CI_T</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to NA MS CI&amp;T Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_NA_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>NA_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to NA MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_UK_I_MS_CI_T_Hitwise_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_I_MS_CI_T_Hitwise</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK&amp;I MS CI&amp;T Hitwise Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_UK_I_MS_EDQ_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_I_MS_EDQ</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to UK&amp;I MS EDQ Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Revenue_Reached_Date</fullName>
        <field>Revenue_Reached_Date__c</field>
        <name>Reset Revenue Reached Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_original_due_date</fullName>
        <field>Original_Due_Date__c</field>
        <formula>Due_Date__c</formula>
        <name>Set original due date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Delivery_Line_Revenue_from_Order</fullName>
        <description>Update Delivery Line Revenue from Order Line item</description>
        <field>Revenue__c</field>
        <formula>IF(NOT(ISNULL( Order_Line_Item__r.EDQ_Margin__c )),Order_Line_Item__r.EDQ_Margin__c , Order_Line_Item__r.Total__c )</formula>
        <name>Update Delivery Line Revenue from Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Line_Expiry_Date_from_Order</fullName>
        <field>Service_Expiry_Date__c</field>
        <formula>Order_Line_Item__r.End_Date__c</formula>
        <name>Update Line Expiry Date from Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Product_Quantity_from_OLI</fullName>
        <field>Order_Product_Qty__c</field>
        <formula>Order_Line_Item__r.CPQ_Quantity__c</formula>
        <name>Update Order Product Quantity from OLI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Project_Owner_s_Email_Address</fullName>
        <field>Project_Owner_s_Email__c</field>
        <formula>Project__r.Owner:User.Email</formula>
        <name>Update Project Owner&apos;s Email Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Revenue_Reached_Date</fullName>
        <field>Revenue_Reached_Date__c</field>
        <formula>Last_Timecard_Date__c</formula>
        <name>Update Revenue Reached Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assign to APAC MS CCM Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_APAC_MS_CCM_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),( CONTAINS( TEXT( Project__r.Business_Unit__c ) ,        &apos;APAC MS CCM&apos;)),        Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to APAC MS CI%26T CCM Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_APAC_MS_CI_T_CCM_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), ( CONTAINS( TEXT( Project__r.Business_Unit__c ) ,  &apos;APAC MS CI&amp;T CCM&apos;)),Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to APAC MS CI%26T Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_To_APAC_MS_CI_T_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), (CONTAINS( TEXT(Project__r.Business_Unit__c) ,                &apos;APAC MS CI&amp;T&apos;)) ,      Assign_to_Queue__c  = True )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to APAC MS EDQ Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_APAC_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  (CONTAINS( TEXT( Project__r.Business_Unit__c ), &apos;APAC MS EDQ&apos;)) ,       Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to EMEA MS Spain Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_EMEA_MS_Spain_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),(CONTAINS( TEXT( Project__r.Business_Unit__c ) ,                 &apos;EMEA MS Spain&apos;)) ,       Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to NA CS Automotive Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_NA_CS_Automotive_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),( CONTAINS( TEXT( Project__r.Business_Unit__c ) ,                   &apos;NA CS Automotive&apos;) ),         Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to NA MS CI%26T Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_NA_MS_CI_T_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), (CONTAINS( TEXT( Project__r.Business_Unit__c ) ,                   &apos;NA MS CI&amp;T&apos;)) ,         Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to NA MS EDQ Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_NA_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), (CONTAINS( TEXT( Project__r.Business_Unit__c ) ,                  &apos;NA MS EDQ&apos;)) ,        Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to UK%26I MS CI%26T Hitwise Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_UK_I_MS_CI_T_Hitwise_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c), (CONTAINS( TEXT( Project__r.Business_Unit__c ) ,                   &apos;UK&amp;I MS CI&amp;T Hitwise&apos;)) ,         Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign to UK%26I MS EDQ Queue for Delivery Line</fullName>
        <actions>
            <name>Assign_to_UK_I_MS_EDQ_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-311804: Assign queue to same named business unit value, when Assign to Queue = TRUE</description>
        <formula>AND(NOT($Setup.IsDataAdmin__c.IsDataAdmin__c),  (CONTAINS( TEXT( Project__r.Business_Unit__c ) ,                  &apos;UK&amp;I MS EDQ&apos;)) ,         Assign_to_Queue__c = True)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Date 100%25 revenue recognised</fullName>
        <actions>
            <name>Update_Revenue_Reached_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>3 AND (1 OR 2 OR 4 OR (6 AND 7)) AND 5</booleanFilter>
        <criteriaItems>
            <field>Delivery_Line__c.Calculated_Complete__c</field>
            <operation>greaterOrEqual</operation>
            <value>100</value>
        </criteriaItems>
        <criteriaItems>
            <field>Delivery_Line__c.Calculated_Revenue_Remaining__c</field>
            <operation>equals</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Delivery_Line__c.Revenue__c</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Delivery_Line__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Delivery_Line__c.Revenue_Reached_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Delivery_Line__c.Status__c</field>
            <operation>equals</operation>
            <value>Expired</value>
        </criteriaItems>
        <criteriaItems>
            <field>Delivery_Line__c.Project_Business_Unit__c</field>
            <operation>equals</operation>
            <value>UK&amp;I MS EDQ</value>
        </criteriaItems>
        <description>Workflow will do a 1 time update of the 100% Revenue achieved date, to be used to recognize no more than 100 of revenue in given month or when an EDQ Project Expires</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Global Project Delivery Line Overdue</fullName>
        <active>true</active>
        <description>WF to send an email to alert the delivery line owner and project owner when due date becomes overdue for the Global SFDC Project Delivery lines</description>
        <formula>RecordType.DeveloperName = &apos;Global_SFDC_Delivery_Line&apos;  &amp;&amp; !ISPICKVAL( Status__c, &apos;Completed&apos;)  &amp;&amp; !ISPICKVAL( Status__c, &apos;Not Required&apos;) &amp;&amp; !ISPICKVAL( Status__c, &apos;Cancelled&apos;) &amp;&amp;  Due_Date__c &gt;= (Today()-1)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Global_Project_Delivery_Line_Overdue_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Delivery_Line__c.Due_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Global Project Delivery Line Status or Date Change</fullName>
        <actions>
            <name>Global_Project_Delivery_Line_Status_or_Due_Date_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>WF to send an email to alert the delivery line owner and project owner when the status or due date of the delivery line is changed for the Global SFDC Project delivery line</description>
        <formula>RecordType.DeveloperName = &apos;Global_SFDC_Delivery_Line&apos; &amp;&amp;  (ISCHANGED( Status__c )|| ISCHANGED(  Due_Date__c  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset Revenue Reached Date</fullName>
        <actions>
            <name>Reset_Revenue_Reached_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Delivery_Line__c.Revenue_Reached_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Delivery_Line__c.Calculated_Revenue_Remaining__c</field>
            <operation>greaterThan</operation>
            <value>USD 0</value>
        </criteriaItems>
        <description>Workflow will reset revenue reached date to blank if line adjustment so completed revenue below 100%</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set original due date</fullName>
        <actions>
            <name>Set_original_due_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Delivery_Line__c.Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets the original due date on a delivery line</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Delivery Line Revenue</fullName>
        <actions>
            <name>Update_Delivery_Line_Revenue_from_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Line_Expiry_Date_from_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Order_Product_Quantity_from_OLI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>T-312860: Update Delivery Line Revenue from Order Line item</description>
        <formula>AND (   ISNEW(),   Order_Line_Item__c &lt;&gt; null )  || AND (   ISCHANGED(Order_Line_Item__c),   Order_Line_Item__c &lt;&gt; null )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Project Owner%27s Email Address</fullName>
        <actions>
            <name>Update_Project_Owner_s_Email_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Delivery_Line__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the Project Owners email address when the delivery line is edited</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
