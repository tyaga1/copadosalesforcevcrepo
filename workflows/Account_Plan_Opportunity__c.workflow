<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Status_to_Open</fullName>
        <field>Type__c</field>
        <literalValue>Open Opps</literalValue>
        <name>Update Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Won</fullName>
        <field>Type__c</field>
        <literalValue>Current Business</literalValue>
        <name>Update Type to Current Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_to_None</fullName>
        <field>Type__c</field>
        <name>Update Type to None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_to_Open_Opps</fullName>
        <field>Type__c</field>
        <literalValue>Open Opps</literalValue>
        <name>Update Type to Open Opps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_to_Potential_Future_Opps</fullName>
        <field>Type__c</field>
        <literalValue>Potential Future Opps</literalValue>
        <name>Update Type to Potential Future Opps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Status to Current</fullName>
        <actions>
            <name>Update_Status_to_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Status__c=&apos;Won&apos;,  Close_Date__c &lt;  Opportunity__r.Contract_End_Date__c , NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Account_Plan_Opportunity__c.Close_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Type_to_None</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account_Plan_Opportunity__c.Contract_End_Date_Formula__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Status to Open</fullName>
        <actions>
            <name>Update_Status_to_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Status__c=&apos;Open&apos;, NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Type to Open Opps</fullName>
        <actions>
            <name>Update_Type_to_Open_Opps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  OR(   Sales_Stage__c == $Label.OPPTY_STAGE_QUALIFY,    Sales_Stage__c == $Label.OPPTY_STAGE_PROPOSE,    Sales_Stage__c == $Label.OPPTY_STAGE_COMMIT,   Sales_Stage__c == $Label.OPPTY_STAGE_CONTRACT  ),  Opportunity__r.Contract_End_Date__c &gt;TODAY(),   NOT($Setup.IsDataAdmin__c.IsDataAdmin__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Type to Potential Future Opps</fullName>
        <actions>
            <name>Update_Type_to_Potential_Future_Opps</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISBLANK(Opportunity__r.Id ), NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
