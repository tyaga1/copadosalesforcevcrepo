<messaging:emailTemplate subject="Experian Data Quality: Important: upcoming PAF® Data usage audit – Action required" recipientType="User" relatedToType="Mail_Merge_Adhoc__c">
<messaging:htmlEmailBody >

<p> Dear {!relatedTo.Recepient_Name__c},</p>
<p></p>

<p>
<b> <center>Important: upcoming PAF® Data usage audit – action required </center></b>
<b> <center>{!relatedTo.Recepient_Account__c} </center></b>
</p>

<p>As a valued customer of Experian Data Quality (EDQ), I need to inform you that the Royal Mail is conducting an audit of PAF® Data usage within EDQ solutions, starting in October.
</p>

<p>To ensure your usage of PAF® Data is compliant with the licensing terms and conditions, please review your product and data information below, and confirm this is an accurate representation. 
</p>

<p>
Whilst we endeavour to ensure you are licensed in the most efficient way, please give consideration to the following – </p>
<p>
o   <b>User Definition</b> – on 1st April 2015 EDQ aligned its definition of User with the Royal Mail therefore we require the number of individuals, as opposed to terminals or workstations, with the ability to access PAF® Data contained within your EDQ solutions.
</p>
<p>
o   <b>Batch usage</b> – if your use of the EDQ Batch data cleansing software with PAF ® Data is automated, we need to understand the number of devices this is installed on.
</p>
<p>
o   <b>External website usage</b> – If you currently have an ‘unlimited transaction’ licence for PAF® Data use within EDQ solutions on your website we require an estimation of your annual transactions. The Royal Mail is introducing a high usage threshold as part of this licence model. Please consider the amount of address searches that are undertaken throughout a 12 month period on your website.  
</p>

<p><b> What you need to do next? </b></p>
<p>Please reply by 31st October, to confirm your usage volumes.  </p> 

To confirm that your product and data usage below is accurate, simply tick this box and forward your reply to your Customer Success Manager, {!relatedTo.Renewal_Owner_Name__c} at {!relatedTo.Renewal_Owner_Email__c}.

<p>☐    Please tick here </p>    
                                             
<apex:repeat var="cx" value="{!relatedTo.Mail_Merge_Details__r}">
<p><td>{!cx.Detail_Name__c}</td> <td> x {!cx.Quantity__c}</td></p>
</apex:repeat>


<p>If you are not sure, please complete the following:</p>
<table border ="1">
<tr>
<th> Description </th>
<th> Count </th>
</tr>

<tr><td><b>FOR USERS:</b> No. of individuals using PAF® Data contained within your EDQ solutions   </td><td> </td></tr>
<tr><td><b>FOR BATCH: No.</b> of devices with PAF ® Data, if the process is automated   </td><td> </td></tr>
<tr><td><b>FOR WEBSITES:</b> If you have an ‘unlimited transaction’ licence for PAF® Data on your website, an estimation of your annual transactions</td><td> </td></tr>   

</table>    

<p>If you have any concerns or need help to understand details of this audit, please contact {!relatedTo.Renewal_Owner_Name__c} who will be able to help.
</p>
<p>Kind regards </p>

<!--img src="{!URLFOR($Resource.CharleneSign)}" alt="sign" /-->

<apex:image id="theImage" value="https://c.NA4.content.force.com/servlet/servlet.ImageServer?id=015i000000D3Yh0&oid=00Di0000000faSr" width="100" height="60"/>


<p>Charlene Marecheau MCIPS</p>
<p>EDQ Commercial Manager</p>
<p></p>
<p>Experian Marketing Services</p>


<p>Experian | George West House | 2-3 Clapham Common North Side | London | SW4 0QL</p>
<p>T. +44 (0) 207 819 5316 | 07815 712 332</p>
<p><a href="mailto:Charlene.Marecheau@experian.com"> Charlene.Marecheau@experian.com</a></p>
<p><a href="https://www.edq.com"> www.edq.com</a></p>


</messaging:htmlEmailBody>
</messaging:emailTemplate>