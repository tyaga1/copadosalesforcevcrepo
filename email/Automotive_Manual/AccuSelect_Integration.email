<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
<title>Experian Automotive</title>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	line-height: 152%;
	margin: 0px !important;
	padding: 0px !important;
}
a:link, a:visited {
	color: #406EB3;
}
.size-controller {
	max-width: 650px !important;
	min-width: 320px !important;
}
.appleLinkOverride-Address a {
	color: #464646 !important;
	text-decoration: none;
}

@media (max-width: 480px) {
.size-controller {
	width: 100%;
}
.mobile-16px-font {
	font-size: 16px !important;
}
.mobile-show {
	display: block !important;
	width: 100% !important;
}
.mobile-hide {
	display: none !important;
}
.mobile-100-percent {
	width: 100% !important;
}
}
</style>

<!--[if gte mso 9]>
<style type="text/css">
body {
	font-family: Arial, Helvetica, sans-serif;
	line-height: 1;
	margin: 0px !important;
	padding: 0px !important;
}
</style>
<![endif]-->

<!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
 </o:OfficeDocumentSettings>
</xml>
<![endif]-->
</head>

<body style="font-family: 'Arial', 'Helvetica','Sans-serif'; background-color: #F4F4F4; line-height: 152%;">

<!-- Preheader text -->
<div style="display:none;font-size:1px;color:#464646;line-height:1px;font-family:Arial, sans-serif;;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;">Experian Automotive's AccuSelect, the API Center of Excellence.</div>
<!-- end Preheader text --> 

<!-- start logo -->
<table cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td width="650" style="background-color: #FFFFFF; width: 650px;" class="size-controller">
    
    <table cellpadding="0" cellspacing="0" border="0" width="650" style="background-color: #FFFFFF; width: 650px;" class="mobile-100-percent">
  <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">
  <td width="650" style="padding: 15px; width: 650px; background-color: #FFFFFF;" class="size-controller"><img src="http://images.go.experian.com/EloquaImages/clients/ExperianInformationSolutionsInc/{645befd4-f4e4-41c1-ac81-c41fc2920375}_experian-logo-328x106.png" alt="" width="164" height="53" style="border-style: none;" border="0"></td>
  </tr>
</table>

</td>
</tr>
</table>
<!-- end logo -->




<!-- start marquee wrapper -->
<table cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td width="650" style="background-color: #FFFFFF; width: 650px;" class="size-controller">
    
    <!--[if (gte mso 9)|(IE)]>
 <table cellpadding="0" cellspacing="0" border="0">
  <tr>
   <td width="450" style="width: 450px;">
<![endif]--> 
    
<table width="650" border="0" cellpadding="0" cellspacing="0" class="size-controller">
          <tr>
            <td align="left" valign="middle" bgcolor="#26478d" width="375" style="width: 375px;">
              
              <table cellpadding="0" cellspacing="0" border="0" width="375" class="mobile-100-percent" style="width: 375px;">
                <tr>
                  <td width="0" style="padding: 15px; background-color: #26478d;"><img src="http://www.autocheck.com/communications/auto-accuselect-icon-68x68.jpg" alt="" width="68" height="68" style="border-style: none;" border="0"></td>
                  <td style="font-size: 26px; font-weight: bold; padding: 15px; color: #FFFFFF; width: 350px;" width="350">Welcome to <br /> Auto AccuSelect<sup>TM</sup></td>
                  </tr>
                 
              </table>
              
              <!--[if (gte mso 9)|(IE)]>
   </td>
   <td width="250" style="width: 250px;">
<![endif]--> 
              
              </td>
            <td valign="middle" bgcolor="#FFFFFF" style="margin: 0px; width: 275px;" width="275" class="mobile-hide"><img src="http://www.autocheck.com/communications/auto-car-275x150.jpg" alt="" width="275" height="150" style="border-style: none;" border="0"></td>
          </tr>
      </table>
      
      <!--[if (gte mso 9)|(IE)]>
   </td>
  </tr>
 </table>
<![endif]-->


</td>
</tr>
</table>
<!-- end marquee wrapper --> 




<!-- start wrapper left/right content -->
<table cellpadding="0" cellspacing="0" border="0"align="center">
  <tr>
    <td width="650" style="background-color: #FFFFFF; width: 650px;" class="size-controller"><table cellpadding="0" cellspacing="0" border="0" align="center">
        <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">
          <td width="650" style="padding: 0px 15px 0px 15px; width: 650px;" class="size-controller"><!-- start left content -->
            
            <table cellpadding="0" cellspacing="0" border="0" align="left" style="width: 350px;" class="mobile-100-percent">
              <tr>
                <td style="padding-top: 10px; padding-right: 20px;"><!--[if (gte mso 9)|(IE)]>
 <table width="350" cellpadding="0" cellspacing="0" border="0">
  <tr>
   <td width="350">
<![endif]--> 
                  
                  <!-- start body copy -->
                  
                  <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td style="font-size: 14px; color: #464646; line-height: 17px;" class="mobile-16px-font">
                      <strong>
                      Thank you for choosing Experian Automotive's Auto Accuselect, the API Center of Excellence and unleash vehicle data information through seamless integration.</strong> 
                      <br />
                      <br />
                      We are glad you have joined us and many other satisfied Auto AccuSelect partners.
                      <hr style="height:1px; border:none; color:#575755; background-color:#575755;">
                      <br>
You will be able to develop and test the integration of these valuable attributes to your product or solutions. Attached to this email is an Auto AccuSelect Developer Guide that will guide you to the next step. <br>
<br>
Below are the credentials for you to use per instructions. Please note these credentials are only for the User Testing Acceptance stage and will be expired in thirty (30) days. Upon the conclusion of your testing, you will need to set up an integration approval with your Customer Representative and once approve, you will be given your production credentials. We are excited to begin this journey with you.<br>
<br>
<font style="line-height: 22px;"> <font style="color:#26478d;"><strong>User ID:</strong></font> (User ID) <br>
<font style="color:#26478d;"><strong>Password:</strong></font> (Password!) <br>
<font style="color:#26478d;"><strong>Client ID:</strong></font> (Client ID) <br>
<font style="color:#26478d;"><strong>Client Secret:</strong></font> (Client Secret) <br>
</font> <br>
<font style="font-size: 14px; font-weight: bold;"> Additional information </font> <br>
Make sure you visit our <a href="https://developer.experian.com/product/autocheck-accuselect" style="color: #406EB3; text-decoration: none;"> <strong>Auto AccuSelect Developer Portal</strong></a> for more information. <br>
<br>
We are confident we can help your business grow, and we look forward to our successful partnership. <br>
<br>
Sincerely, <br>
<br>
(Client Services Rep Name Here) <br>
<br>
<a href="mailto:ac.hosting@autocheck.com?Subject=AccuSelect" style="color: #406EB3; text-decoration: none;"><strong>ac.hosting@autocheck.com</strong>.</a> <br>
Experian Automotive Client Support <br>
<br>
<br></td>
                    </tr>
                  </table>
                  
                  <!-- end body copy --></td>
              </tr>
            </table>
            
            <!--[if (gte mso 9)|(IE)]>
   </td>
   </tr>
   </table>
   
   <table width="240" cellpadding="0" cellspacing="0" border="0">
  <tr>
   <td width="240" valign="top">
<![endif]--> 
            
            <!-- end left content --> 
            
            <!-- start right content --> 
            
            <!-- start sidebar Wrapper Table -->
            
            <table cellpadding="0" cellspacing="0" border="0" style="width: 240px;" class="mobile-100-percent">
              <tr>
                <td style="padding-top: 10px;"><!-- start sidebar content -->
                  
                  <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                      <td style="text-align: top; background-color: #ffffff; color: #464646; font-size: 14px; padding-left: 5px;" class="mobile-16px-font"><!--<span style="font-size: 20px; font-weight: bold;">New white paper</span><br /><br />--> 
                        
                        <span style="font-size: 18px; font-weight: bold;">
                        <img src="http://www.autocheck.com/communications/auto-autocheck-logo-164x49.jpg" width="164" height="49" style="border-style: none; padding-bottom: 20px; padding-right:50px;" border="0">
                        Get more information by accessing our <a href="https://developer.experian.com/product/autocheck-accuselect" style="color: #406EB3; text-decoration: none;"> <strong>Auto AccuSelect Developer Portal</strong></a>.</span><br><br>
                        
                        <font style="line-height: 18px;">
                        Our award-winning customer service is also available to answer any of your questions. 
                        <br>
                        <br>
						Please contact: <a href="mailto:ac.hosting@autocheck.com?Subject=AccuSelect" style="color: #406EB3; text-decoration: none;"><strong>ac.hosting@autocheck.com</strong>.</a>
                       </font>
                  
                  <br><br></td>
                    </tr>
                  </table>
                  
                  <!-- end sidebar content --></td>
              </tr>
            </table>
            
            <!-- end sidebar Wrapper table --> 
            
            <!-- end right content --> 
            
            <!--[if (gte mso 9)|(IE)]>
   </td>
  </tr>
 </table>
<![endif]--></td>
        </tr>
      </table></td>
  </tr>
</table>
<!-- end wrapper left/right content -->

<!-- Start Gradient Image -->
<table cellpadding="0" cellspacing="0" border="0" width="650" align="center" class="size-controller">
  <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">
    <td height="2" style="background-color: #ffffff;"><img src="http://images.go.experian.com/EloquaImages/clients/ExperianInformationSolutionsInc/{93ae77f6-490f-4d10-979a-446fb7e82fbf}_email-footer-gradient.jpg" width="650" height="2" class="mobile-100-percent" alt="" style="border-style: none;" border="0"/></td>
  </tr>
</table>
<!-- End Gradient Image -->


<!-- Start Footer Job Number -->
<table cellpadding="0" cellspacing="0" border="0" width="650" align="center" class="size-controller" style="margin: 0px auto; line-height: 1.5;">
  <tr>
    <td style="padding-left: 10px; padding-top: 10px; text-align: left;"><span style="font-size: 10px; color: #575755;"><a href="http://www.experianautomotive.com">www.experianautomotive.com</a></span></td>
  </tr>
</table>
<!-- End Footer Job Number -->




<!-- Start Footer Disclaimer -->
<table cellpadding="10" cellspacing="0" border="0" width="650" align="center" class="size-controller" style="margin: 0 auto; line-height: 1.5;">
  <tr>
    <td style="font-size: 10px; color: #575755">&#0169; 2017 Experian Information Solutions, Inc. All rights reserved.<br />
      <br />
      Experian and the Experian marks used herin are trademarks or registered trademarks of Experian Information Solutions, Inc. Other product and company names mentioned herin are the property of their respective owners. <br />
     </td>
  </tr>
</table>
<!-- End Footer Disclaimer -->
</body>
</html>