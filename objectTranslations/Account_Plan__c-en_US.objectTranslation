<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Account Plan</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Account Plans</value>
    </caseValues>
    <fields>
        <label><!-- Account Annualised Revenue --></label>
        <name>Account_Annualised_Revenue_Formula__c</name>
    </fields>
    <fields>
        <help><!-- Value taken from the &apos;Previous FY won orders&apos; field of the account segment relating to the creator&apos;s GBL, BL and BU --></help>
        <label><!-- Account Annualised Revenue --></label>
        <name>Account_Annualised_Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Account Industry --></label>
        <name>Account_Industry__c</name>
    </fields>
    <fields>
        <label><!-- Account Sector --></label>
        <name>Account_Sector__c</name>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>Account__c</name>
        <relationshipLabel><!-- Account Plans --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Field to be used to capture additional information about the client that may be relevant --></help>
        <label><!-- Additional Information --></label>
        <name>Additional_Information__c</name>
    </fields>
    <fields>
        <label><!-- Annual CAPEX in Experian Domain --></label>
        <name>Annual_CAPEX_in_Experian_Domain__c</name>
    </fields>
    <fields>
        <label><!-- Annual OPEX in Experian Domain --></label>
        <name>Annual_OPEX_in_Experian_Domain__c</name>
    </fields>
    <fields>
        <help><!-- What are the client&apos;s key business objectives? These are commonly stated within the annual report or at the client website. --></help>
        <label><!-- Client&apos;s Objectives --></label>
        <name>Business_Objectives__c</name>
    </fields>
    <fields>
        <help><!-- We understand the client&apos;s strategy and plans for growth and/or expansion --></help>
        <label><!-- Client Plan --></label>
        <name>Client_Plan__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have established a contact plan for the next 12 months --></help>
        <label><!-- Contact Plan --></label>
        <name>Contact_Plan__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have adequate contracts in place to protect us --></help>
        <label><!-- Contracts --></label>
        <name>Contracts__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Please provide a brief summary of the client&apos;s key products and services within the regions in scope of the Account Plan. --></help>
        <label><!-- Client&apos;s Core Business --></label>
        <name>Core_Business__c</name>
    </fields>
    <fields>
        <help><!-- We have strong relationships with this client at executive level --></help>
        <label><!-- Executive --></label>
        <name>Executive__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Experian Annualised Revenue --></label>
        <name>Experian_Annualised_Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Experian CAPEX share % --></label>
        <name>Experian_CAPEX_share__c</name>
    </fields>
    <fields>
        <label><!-- Experian CAPEX Total --></label>
        <name>Experian_Capex_Total__c</name>
    </fields>
    <fields>
        <label><!-- Experian OPEX Share % --></label>
        <name>Experian_OPEX_Share__c</name>
    </fields>
    <fields>
        <label><!-- Experian OPEX Total --></label>
        <name>Experian_OPEX_Total__c</name>
    </fields>
    <fields>
        <help><!-- What is Experian&apos;s Strategy to achieve these SMART Objectives? --></help>
        <label><!-- Experian Strategy --></label>
        <name>Experian_Strategy_for_Account__c</name>
    </fields>
    <fields>
        <help><!-- What are Experian&apos;s Goals for our engagement with this client? --></help>
        <label><!-- Experian Goals --></label>
        <name>Experian_Vision_for_Account__c</name>
    </fields>
    <fields>
        <help><!-- The contract does not expire in the next 18 months --></help>
        <label><!-- Expiration --></label>
        <name>Expiration__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have monthly client face-to-face meetings --></help>
        <label><!-- Face-to-Face --></label>
        <name>Face_to_Face__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have completed health-status interviews in the last 12 months --></help>
        <label><!-- Health Status --></label>
        <name>Health_Status__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The customer sees us as experts in their industry --></help>
        <label><!-- Industry Expertise --></label>
        <name>Industry_Expertise__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have good client relations --></help>
        <label><!-- Internal Account Team --></label>
        <name>Internal_Account_Team__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The date the plan was last reviewed by the account team. --></help>
        <label><!-- Last Review Date --></label>
        <name>Last_Review_Date__c</name>
    </fields>
    <fields>
        <label><!-- Last Tab Viewed --></label>
        <name>Last_Tab_Viewed__c</name>
    </fields>
    <fields>
        <help><!-- Given our relationship experience with this client, what do we know works well, and what should be avoided? --></help>
        <label><!-- Relationship lessons learned --></label>
        <name>Lessons_Learned__c</name>
    </fields>
    <fields>
        <help><!-- What is materially changing in the client organisation since the last Account Plan review? Consider both stakeholder and internal/external business change. --></help>
        <label><!-- Client&apos;s Recent / Upcoming Changes --></label>
        <name>Major_Changes__c</name>
    </fields>
    <fields>
        <help><!-- Which five organisations does the client view as their major competitors (current or future) in Experian&apos;s potential field of engagement? --></help>
        <label><!-- Client&apos;s Competitors --></label>
        <name>Major_Competitors__c</name>
    </fields>
    <fields>
        <help><!-- On which firms, within the scope of Experian&apos;s potential engagement, does the client rely for products, services or advice? --></help>
        <label><!-- Client&apos;s Partners --></label>
        <name>Major_Partners__c</name>
    </fields>
    <fields>
        <help><!-- We have won major new opportunities in the last 12 months --></help>
        <label><!-- Major Wins --></label>
        <name>Major_Wins__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The net Promoter Score is good with a positive momentum --></help>
        <label><!-- NPS --></label>
        <name>NPS__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- What objectives agreed internally for the Experian / Client relationship have not been achieved in the last twelve months and why?  How have we reacted to this? --></help>
        <label><!-- Relationship objectives not achieved --></label>
        <name>Objectives_Not_Achieved__c</name>
    </fields>
    <fields>
        <help><!-- We are servicing the client through all business units --></help>
        <label><!-- One Experian --></label>
        <name>One_Experian__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have identified major opportunities with this client --></help>
        <label><!-- Opportunities --></label>
        <name>Opportunities__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We do not have have credit issues with this client --></help>
        <label><!-- Payment Issues --></label>
        <name>Payment_Issues__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Use to indicate the level at which the plan is for. --></help>
        <label><!-- Plan Type --></label>
        <name>Plan_Type__c</name>
        <picklistValues>
            <masterLabel>Country</masterLabel>
            <translation><!-- Country --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Global</masterLabel>
            <translation><!-- Global --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Regional</masterLabel>
            <translation><!-- Regional --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Primary Account --></label>
        <name>Primary_Account__c</name>
    </fields>
    <fields>
        <label><!-- Primary Contact --></label>
        <name>Primary_Contact__c</name>
        <relationshipLabel><!-- Account Plans --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- What successes have resulted from the Experian / Client relationship in the last 12 mths?  This may include a recent win / renewal, case study, industry event, PR etc... --></help>
        <label><!-- Recent Relationship Successes --></label>
        <name>Recent_Successes__c</name>
    </fields>
    <fields>
        <help><!-- The client will give us a reference on request --></help>
        <label><!-- Reference --></label>
        <name>Reference__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Is in line with or better than our expectations for this client --></help>
        <label><!-- Revenue --></label>
        <name>Revenue__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have a plan to address all identified risks --></help>
        <label><!-- Risks --></label>
        <name>Risks__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have solutions which meet the client&apos;s critical business issues --></help>
        <label><!-- Solutions --></label>
        <name>Solutions__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- We have had a stable technical solution over the past 12 months --></help>
        <label><!-- Stability --></label>
        <name>Stability__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- What is the client&apos;s stated strategy to meet their Business Objectives? This can often be found in the client&apos;s company report.Client&apos;s Strategic Direction. --></help>
        <label><!-- Client&apos;s Strategy --></label>
        <name>Strategic_Direction__c</name>
    </fields>
    <fields>
        <help><!-- Summarise the recent (maximum 3 year) Experian relationship with this client. --></help>
        <label><!-- Experian Relationship Overview --></label>
        <name>Summary_Recent_History__c</name>
    </fields>
    <fields>
        <help><!-- The client has a clear perception that they are getting value from our solutions --></help>
        <label><!-- Value --></label>
        <name>Value__c</name>
        <picklistValues>
            <masterLabel>Negative</masterLabel>
            <translation><!-- Negative --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Neutral</masterLabel>
            <translation><!-- Neutral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Strength</masterLabel>
            <translation><!-- Strength --></translation>
        </picklistValues>
    </fields>
    <startsWith>Consonant</startsWith>
    <webLinks>
        <label><!-- Account_Penetration --></label>
        <name>Account_Penetration</name>
    </webLinks>
    <webLinks>
        <label><!-- Create_Follow_up_Task --></label>
        <name>Create_Follow_up_Task</name>
    </webLinks>
    <webLinks>
        <label><!-- DownloadPlan --></label>
        <name>DownloadPlan</name>
    </webLinks>
    <webLinks>
        <label><!-- New_Account_Plan --></label>
        <name>New_Account_Plan</name>
    </webLinks>
    <webLinks>
        <label><!-- SWOT --></label>
        <name>SWOT</name>
    </webLinks>
</CustomObjectTranslation>
