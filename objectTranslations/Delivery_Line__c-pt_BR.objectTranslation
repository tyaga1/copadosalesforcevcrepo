<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value><!-- Delivery Line --></value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value><!-- Delivery Line --></value>
    </caseValues>
    <fields>
        <label><!-- Actual End Date --></label>
        <name>Actual_End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Actual LOE Days --></label>
        <name>Actual_LOE_Days__c</name>
    </fields>
    <fields>
        <label><!-- Actual LOE Hours --></label>
        <name>Actual_LOE_Hours__c</name>
    </fields>
    <fields>
        <help><!-- If set to true, will allow users to enter a timecard. Will prevent them from doing so if set to false. --></help>
        <label><!-- Allow Timecard Entry --></label>
        <name>Allow_Timecard_Entry__c</name>
    </fields>
    <fields>
        <label><!-- Assign to Queue --></label>
        <name>Assign_to_Queue__c</name>
    </fields>
    <fields>
        <label><!-- Assigned Date --></label>
        <name>Assigned_Date__c</name>
    </fields>
    <fields>
        <label><!-- Billable? --></label>
        <name>Billable__c</name>
    </fields>
    <fields>
        <label><!-- Calculated % Complete --></label>
        <name>Calculated_Complete__c</name>
    </fields>
    <fields>
        <help><!-- Estimated revenue yet to be recognised to date --></help>
        <label><!-- Calculated Revenue Remaining --></label>
        <name>Calculated_Revenue_Remaining__c</name>
    </fields>
    <fields>
        <label><!-- Calculated Revenue to Date (Number) --></label>
        <name>Calculated_Revenue_to_Date_Number__c</name>
    </fields>
    <fields>
        <help><!-- Based on Calculated % Complete, if completed 100% recognised, cannot be over 100% of revenue. --></help>
        <label><!-- Calculated Revenue to date --></label>
        <name>Calculated_Revenue_to_date__c</name>
    </fields>
    <fields>
        <label><!-- % Complete Variance --></label>
        <name>Complete_Variance__c</name>
    </fields>
    <fields>
        <label><!-- % Complete --></label>
        <name>Complete__c</name>
    </fields>
    <fields>
        <label><!-- Completed Month.Year --></label>
        <name>Completed_Month__c</name>
    </fields>
    <fields>
        <label><!-- Delivery Line ID 18 Characters --></label>
        <name>Delivery_Line_ID_18_Characters__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>Description__c</name>
    </fields>
    <fields>
        <label><!-- Due Date --></label>
        <name>Due_Date__c</name>
    </fields>
    <fields>
        <help><!-- Will only calculate when Delivery Line is completed or expired --></help>
        <label><!-- End LOE Variance --></label>
        <name>End_LOE_Variance__c</name>
    </fields>
    <fields>
        <label><!-- Estimated LOE Days --></label>
        <name>Estimated_LOE_Days__c</name>
    </fields>
    <fields>
        <label><!-- Estimated LOE Hours --></label>
        <name>Estimated_LOE_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Estimated LOE Remaining Days --></label>
        <name>Estimated_LOE_Remaining_Days__c</name>
    </fields>
    <fields>
        <label><!-- Estimated LOE Remaining Hours --></label>
        <name>Estimated_LOE_Remaining_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Key Milestone --></label>
        <name>Key_Milestone__c</name>
    </fields>
    <fields>
        <label><!-- Last Timecard Date --></label>
        <name>Last_Timecard_Date__c</name>
    </fields>
    <fields>
        <label><!-- OL EDQ Margin --></label>
        <name>OL_EDQ_Margin__c</name>
    </fields>
    <fields>
        <label><!-- Order Line Item --></label>
        <name>Order_Line_Item__c</name>
        <relationshipLabel><!-- Delivery Lines --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- CRM Product Name from Order Line --></help>
        <label><!-- Order Line Product --></label>
        <name>Order_Line_Product__c</name>
    </fields>
    <fields>
        <label><!-- Order Product Qty --></label>
        <name>Order_Product_Qty__c</name>
    </fields>
    <fields>
        <label><!-- Order --></label>
        <name>Order__c</name>
        <relationshipLabel><!-- Delivery Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Original Due Date --></label>
        <name>Original_Due_Date__c</name>
    </fields>
    <fields>
        <label><!-- Owner Full Name --></label>
        <name>Owner_Full_Name__c</name>
    </fields>
    <fields>
        <help><!-- This is a Business Unit value of the delivery line&apos;s project --></help>
        <label><!-- Project Business Unit --></label>
        <name>Project_Business_Unit__c</name>
    </fields>
    <fields>
        <label><!-- Project Owner&apos;s Email --></label>
        <name>Project_Owner_s_Email__c</name>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>Project__c</name>
        <relationshipLabel><!-- Delivery Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Qualification Status --></label>
        <name>Qualification_Status__c</name>
        <picklistValues>
            <masterLabel>High</masterLabel>
            <translation><!-- High --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Low</masterLabel>
            <translation><!-- Low --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Medium</masterLabel>
            <translation><!-- Medium --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Revenue (Number) --></label>
        <name>Revenue_Number__c</name>
    </fields>
    <fields>
        <help><!-- The Timecard date when 100% of revenue was recognised - used on open lines that reached revenue recognition. --></help>
        <label><!-- Revenue Reached Date --></label>
        <name>Revenue_Reached_Date__c</name>
    </fields>
    <fields>
        <label><!-- Revenue --></label>
        <name>Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Sequence --></label>
        <name>Sequence__c</name>
    </fields>
    <fields>
        <help><!-- Auto populated from Order Line Item end date. --></help>
        <label><!-- Service Expiry Date --></label>
        <name>Service_Expiry_Date__c</name>
    </fields>
    <fields>
        <label><!-- Service Type --></label>
        <name>Service_Type__c</name>
        <picklistValues>
            <masterLabel>Bid Management</masterLabel>
            <translation><!-- Bid Management --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Engagement</masterLabel>
            <translation><!-- Customer Engagement --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Delivery</masterLabel>
            <translation><!-- Delivery --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal</masterLabel>
            <translation><!-- Internal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Delivery</masterLabel>
            <translation><!-- Internal Delivery --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>International Delivery</masterLabel>
            <translation><!-- International Delivery --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Presales</masterLabel>
            <translation><!-- Presales --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Project Co-ordination</masterLabel>
            <translation><!-- Project Co-ordination --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Project Management</masterLabel>
            <translation><!-- Project Management --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>Cancelled</masterLabel>
            <translation><!-- Cancelled --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Completed</masterLabel>
            <translation><!-- Completed --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Expired</masterLabel>
            <translation><!-- Expired --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Hold</masterLabel>
            <translation><!-- Hold --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Progress</masterLabel>
            <translation><!-- In Progress --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Required</masterLabel>
            <translation><!-- Not Required --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Started</masterLabel>
            <translation><!-- Not Started --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Unassigned</masterLabel>
            <translation><!-- Unassigned --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Define the intended purpose of delivery, in line with IFRS15 regulation. See Professional Services Integration Definition file for further detail. --></help>
        <label><!-- Type of Delivery --></label>
        <name>Type_of_Delivery__c</name>
        <picklistValues>
            <masterLabel>Integration</masterLabel>
            <translation><!-- Integration --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Non-Integration</masterLabel>
            <translation><!-- Non-Integration --></translation>
        </picklistValues>
    </fields>
    <gender><!-- Feminine --></gender>
    <nameFieldLabel><!-- Delivery Line Name --></nameFieldLabel>
    <recordTypes>
        <label>Linha de entrega GTS</label>
        <name>GTS_Delivery_Line</name>
    </recordTypes>
    <recordTypes>
        <label>Linha de entrega do GCS SFDC</label>
        <name>Global_SFDC_Delivery_Line</name>
    </recordTypes>
    <recordTypes>
        <label>Linha de entrega de projeto</label>
        <name>Project_Delivery_Line</name>
    </recordTypes>
    <validationRules>
        <errorMessage><!-- If a delivery line is billable then the project should be billable as well. Please update the project. --></errorMessage>
        <name>Billable_Check</name>
    </validationRules>
    <validationRules>
        <errorMessage>Favor garantir que a data atual de fechamento foi imputada para completar esta linha de entrega</errorMessage>
        <name>DL_Ensure_End_Date_on_Completed_lin</name>
    </validationRules>
    <validationRules>
        <errorMessage>Para fechar esta linha de entrega, favor assegurar que o proprietário da linha de entrega seja uma pessoa e não uma fila ou grupo.</errorMessage>
        <name>DL_Ensure_Owner_isUser</name>
    </validationRules>
    <validationRules>
        <errorMessage>Não é possivel atualizar o campo do pedido na linha de entrega. Favor ir no registro do projeto para realizar a alteração</errorMessage>
        <name>DL_Order_Should_Match_Projects_Order</name>
    </validationRules>
    <validationRules>
        <errorMessage>Você é imcapaz de alterar o LOE estimado quando o reconhecimento de receita for alcançado</errorMessage>
        <name>Stop_Estimated_LOE_update_if_already_rec</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please select &quot;Type of delivery&quot; --></errorMessage>
        <name>Type_of_delivery_required_for_UK_I_EDQ</name>
    </validationRules>
    <webLinks>
        <label><!-- Add_Delivery_Line --></label>
        <name>Add_Delivery_Line</name>
    </webLinks>
    <webLinks>
        <label><!-- New_Delivery_Line --></label>
        <name>New_Delivery_Line</name>
    </webLinks>
</CustomObjectTranslation>
