<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Story</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Stories</value>
    </caseValues>
    <fields>
        <label><!-- Acceptance Criteria --></label>
        <name>Acceptance_Criteria__c</name>
    </fields>
    <fields>
        <help><!-- Describe the story scope and dependencies. --></help>
        <label><!-- Assumptions --></label>
        <name>Assumptions__c</name>
    </fields>
    <fields>
        <help><!-- Describe stakeholder impact, communication and training considerations, impact to legacy systems, or process definition that may be needed. --></help>
        <label><!-- Change Enablement Notes --></label>
        <name>Change_Enablement_Notes__c</name>
    </fields>
    <fields>
        <help><!-- Date the story was set to Completed status --></help>
        <label><!-- Completed Date --></label>
        <name>Completed_Date__c</name>
    </fields>
    <fields>
        <help><!-- High-level assessment of implementation effort based upon estimated complexity and/or size. --></help>
        <label><!-- Complexity --></label>
        <name>Complexity__c</name>
        <picklistValues>
            <masterLabel>High</masterLabel>
            <translation><!-- High --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Low</masterLabel>
            <translation><!-- Low --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Medium</masterLabel>
            <translation><!-- Medium --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Project component that this story affects. --></help>
        <label><!-- Component --></label>
        <name>Component__c</name>
        <relationshipLabel><!-- Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Level of effort estimate for development, as measured in story points. --></help>
        <label><!-- Dev LOE --></label>
        <name>Dev_LOE__c</name>
    </fields>
    <fields>
        <label><!-- Dev Owner --></label>
        <name>Dev_Owner__c</name>
        <relationshipLabel><!-- Stories (Dev Owner) --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- External reference id for this story. --></help>
        <label><!-- External Id --></label>
        <name>External_Id__c</name>
    </fields>
    <fields>
        <help><!-- The date the feature is accepted. --></help>
        <label><!-- Feature Accepted Date --></label>
        <name>Feature_Accepted_Date__c</name>
    </fields>
    <fields>
        <help><!-- The story/feature has been signed-off by the business and is ready for further promotion/deployment. --></help>
        <label><!-- Feature Accepted --></label>
        <name>Feature_Accepted__c</name>
    </fields>
    <fields>
        <help><!-- The primary owner from a functional perspective. --></help>
        <label><!-- Functional Owner --></label>
        <name>Functional_Owner__c</name>
        <relationshipLabel><!-- Stories (Functional Owner) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Priority --></label>
        <name>Priority__c</name>
        <picklistValues>
            <masterLabel>P1</masterLabel>
            <translation><!-- P1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>P2</masterLabel>
            <translation><!-- P2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>P3</masterLabel>
            <translation><!-- P3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>P4</masterLabel>
            <translation><!-- P4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>P5</masterLabel>
            <translation><!-- P5 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Link to the Product to which this story\&amp;#39;s component is associated. --></help>
        <label><!-- Project --></label>
        <name>Project__c</name>
        <relationshipLabel><!-- Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Relative ranking of priority with other stories. --></help>
        <label><!-- Ranking --></label>
        <name>Ranking__c</name>
    </fields>
    <fields>
        <help><!-- Link to the release during which this story will be implemented. --></help>
        <label><!-- Release --></label>
        <name>Release__c</name>
        <relationshipLabel><!-- Stories --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Sprint --></label>
        <name>Sprint__c</name>
        <relationshipLabel><!-- Stories --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Additional details to help provide further context on the status. --></help>
        <label><!-- Status Notes --></label>
        <name>Status_Notes__c</name>
    </fields>
    <fields>
        <help><!-- Status of the story. --></help>
        <label><!-- Status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>Backlog</masterLabel>
            <translation><!-- Backlog --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Blocked</masterLabel>
            <translation><!-- Blocked --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Cancelled</masterLabel>
            <translation><!-- Cancelled --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Complete</masterLabel>
            <translation><!-- Complete --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Dev Complete</masterLabel>
            <translation><!-- Dev Complete --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Progress</masterLabel>
            <translation><!-- In Progress --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>QA Complete</masterLabel>
            <translation><!-- QA Complete --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>QA In Progress</masterLabel>
            <translation><!-- QA In Progress --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ready for QA</masterLabel>
            <translation><!-- Ready for QA --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ready for Tech Review</masterLabel>
            <translation><!-- Ready for Tech Review --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Ready for UAT</masterLabel>
            <translation><!-- Ready for UAT --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Requested</masterLabel>
            <translation><!-- Requested --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>UAT Complete</masterLabel>
            <translation><!-- UAT Complete --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The date requirements are approved. --></help>
        <label><!-- Story Approval Date --></label>
        <name>Story_Approval_Date__c</name>
    </fields>
    <fields>
        <help><!-- The story description is complete and captures the core requirements to be delivered. --></help>
        <label><!-- Story Complete --></label>
        <name>Story_Complete__c</name>
    </fields>
    <fields>
        <help><!-- Name of the Requirement. --></help>
        <label><!-- Story Name --></label>
        <name>Story_Name__c</name>
    </fields>
    <fields>
        <help><!-- Have all tasks necessary to implement this story been entered? --></help>
        <label><!-- Tasks Entered? --></label>
        <name>Tasks_Entered__c</name>
    </fields>
    <fields>
        <help><!-- A User Requirement is a short expression of functionality, presented from the user&apos;s point of view, in the following format: --></help>
        <label><!-- User Requirement --></label>
        <name>User_Requirement__c</name>
    </fields>
    <startsWith>Consonant</startsWith>
</CustomObjectTranslation>
