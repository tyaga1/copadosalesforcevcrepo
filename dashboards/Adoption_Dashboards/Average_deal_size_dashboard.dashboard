<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>APAC</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>EMEA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Latin America,LATAM</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North America,NA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Owner region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Credit Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Decision Analytics</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Experian Consumer Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Marketing Services</values>
        </dashboardFilterOptions>
        <name>Product GBL</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Owner_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product_GBL__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average won product revenue for the last 12 months, split by close month.</footer>
            <groupingColumn>Order__c$Close_Date__c</groupingColumn>
            <header>Average won product revenue - Sales price</header>
            <legendPosition>Bottom</legendPosition>
            <report>Adoption/X8_83_Adoption_Avg_order_size_Prod_GBL</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Monthly tracking</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c$Order_Revenue_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Owner_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product_GBL__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average won order size for the last 12 months, split by close month. This component will not work with the &apos;Product GBL&apos; filter.</footer>
            <groupingColumn>Order__c$Close_Date__c</groupingColumn>
            <header>Average won deal size - TCV</header>
            <legendPosition>Bottom</legendPosition>
            <report>Adoption/X8_83_Adoption_Avg_order_size_Prod_GBL</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Monthly tracking</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Owner_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product_GBL__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average won product revenue for the current financial year, split by Product GBL.</footer>
            <groupingColumn>Order__c.Order_Line_Items__r$Product_GBL__c</groupingColumn>
            <header>Average won product revenue - Sales price</header>
            <legendPosition>Bottom</legendPosition>
            <report>Adoption/X8_99_Adoption_Avg_sales_price_Prod_GBL</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current financial year</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c$Order_Revenue_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Owner_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product_GBL__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average won order size for the current financial year, split by Region.</footer>
            <groupingColumn>Order__c$Owner_Region__c</groupingColumn>
            <header>Average won deal size - TCV</header>
            <legendPosition>Bottom</legendPosition>
            <report>Adoption/X8_84_Adoption_Avg_order_size_Region</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current financial year</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Owner_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product_GBL__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average won product revenue for the current financial year, split by Region.</footer>
            <groupingColumn>Order__c$Owner_Region__c</groupingColumn>
            <header>Average won product revenue - Sales price</header>
            <legendPosition>Bottom</legendPosition>
            <report>Adoption/X8_99_Adoption_Avg_sales_price_Prod_GBL</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current financial year</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Owner_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product_GBL__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Order__c$Owner_Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c$Order_Revenue_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the average won order size of the top 10 Sales teams for the current financial year.</footer>
            <header>FY average won deal size - TCV</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Adoption/X8_89_Adoption_ST_avg_order_size1</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 sales teams</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Owner_Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product_GBL__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Order__c$Owner_Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the average won sales price of the top 10 Sales teams for the current financial year.</footer>
            <header>FY average sales price</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Adoption/X8_89_Adoption_ST_avg_sales_price</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 sales teams</title>
        </components>
    </rightSection>
    <runningUser>gareth.lee@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Average deal size dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
