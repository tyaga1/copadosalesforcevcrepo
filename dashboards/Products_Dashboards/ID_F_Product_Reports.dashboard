<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Renewal,Existing Business</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Trial</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Upsell (Existing Product Line)</values>
        </dashboardFilterOptions>
        <name>Type</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the number of open opportunities, split by Sales stage. Record count refers to the number of opportunities.</footer>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X0_1_Number_of_open_opportunities</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Number of open opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Order__c$Owner_Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the current financial year&apos;s won &apos;Sales price&apos; revenue leaderboard for Sales teams. The leaderboard is ranked by won &apos;Sales price&apos; revenue. Record count refers to the number of orders.</footer>
            <header>Won &apos;Sales price&apos; revenue leaderboard</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>ID_F/X0_4_YTD_top_10_Sales_teams</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>YTD - top 10 Sales teams</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Order__c$Owner_Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c.Order_Line_Items__r.Order_Revenue_Schedules__r$Revenue__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the a Business Unit&apos;s won scheduled revenue for the current financial year.</footer>
            <header>Won scheduled revenue leaderboard</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>ID_F/X0_7_Current_FY_top_10_Sales_teams</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Current FY - top 10 Sales teams</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Line</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>his component shows both the scheduled revenue and expected scheduled revenue of open opportunities.</footer>
            <header>Scheduled revenue for open opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X10_0_Scheduled_revenue_for_open_opps</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Month by month view</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Order__c$Account__c.Reporting_Name__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 10 accounts, ranked by won &apos;Sales price&apos; revenue. Record count refers to the number of won orders.</footer>
            <header>Top 10 accounts</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>ID_F/X13_Won_orders_in_the_YTD</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Won orders in the YTD</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 15 open &apos;New Business&apos; opportunities for Global Strategic client, ranked by Total Contract Value.</footer>
            <header>Top 10 open opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>ID_F/X16_Closing_in_the_current_or_next_FQ</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Closing in the current or next FQ</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the amount of &apos;Sales price&apos; revenue associated with a product for all open opportunities, split by Sales stage.</footer>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X0_2_Sales_price_of_products</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>&apos;Sales price&apos; of products</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>LineGrouped</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the monthly won &apos;Sales price&apos; revenue for the current financial year, compared to the previous two financial years, split by month.</footer>
            <header>Monthly won &apos;Sales price&apos; revenue comparison</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X0_5_YTD_compared_to_previous_two_FYs</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>YTD - compared to the previous two FYs</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Line</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows monthly won scheduled revenue for the current financial year.</footer>
            <header>Won scheduled revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X0_8_Month_by_month_view</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Month by month view</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component the % of won &apos;Sales price&apos; revenue associated to each Strategic Client type. You can hover over the chart to make the won revenue and % details appear.</footer>
            <header>Won &apos;Sales price&apos; revenue % by Strategic client level</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X11_Won_Sales_price_revenue_by_Strate</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>YTD</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the won deals by product name, valued by &apos;Sales price&apos;.</footer>
            <header>YTD</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X14_Won_Deals_by_Product_Name</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Won Deals by Product Name</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the won deals and pipeline for this FY by Sales price.</footer>
            <header>YTD</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X17_Won_Deals_and_Pipeline</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Won Deals and Pipeline</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the value of won deals this FY by &apos;Sales price&apos; compared to the new business sales target</footer>
            <gaugeMax>3840000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Closed Sales</header>
            <indicatorBreakpoint1>3000000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>3500000.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>ID_F/X19_Current_FY_Sales_Vs_Target</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Current FY Sales Vs Target</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the current stage and &apos;Sales price&apos; of open opportunities, split by their expected closed date. Record count refers to the number of open opportunities.</footer>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X0_3_Open_opps_by_range_of_closed_date</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Open opps by range of closed date</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>LineGroupedCumulative</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the cumulative won &apos;Sales price&apos; revenue for the current financial year, compared to the previous two financial years, split by month.</footer>
            <header>Cumulative won &apos;Sales price&apos; revenue comparison</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X0_6_Cumulative_won_Sales_price</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>YTD - compared to the previous two FYs</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>LineCumulative</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows a cumulative won scheduled revenue for the current financial year.</footer>
            <header>Cumulative won scheduled revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X10_1_Payments_Product_BL_SR1</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current financial year</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the % of won &apos;Sales price&apos; revenue for each Industty in the current financial year. You can hover over the chart to make the won revenue and % details appear.</footer>
            <header>Won &apos;Sales price&apos; revenue % by Industry</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X12_Won_Sales_price_revenue_by_Indust</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>YTD</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline of deals by product name valued by &apos;Sales price&apos;.</footer>
            <header>YTD</header>
            <legendPosition>Bottom</legendPosition>
            <report>ID_F/X15_Pipeline_Deals_by_Product_Name</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Pipeline Deals by Product Name</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>UNIT_PRICE.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 10 overdue opportunities. These opportunities need to either be closed or have their expected close date adjusted. Opportunities are ranked by &apos;Sales price&apos; revenue.</footer>
            <header>Top 10 overdue opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>ID_F/X18_Close_dates_in_need_of_attention</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Close dates in need of attention</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the value of won deals this quarter by &apos;Sales price&apos; compared to the new business sales target.</footer>
            <gaugeMax>962000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Closed Sales</header>
            <indicatorBreakpoint1>750000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>874000.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>ID_F/X20_Current_Quarter_Sales_Vs_Target</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Current Quarter Sales Vs Target</title>
        </components>
    </rightSection>
    <runningUser>simon.dibble@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>ID&amp;F Product Reports</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
