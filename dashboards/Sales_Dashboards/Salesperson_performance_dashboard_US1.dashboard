<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Credit Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Decision Analytics</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Experian Consumer Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Marketing Services</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Credit Services,Decision Analytics</values>
        </dashboardFilterOptions>
        <name>Product GBL</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From Existing</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <name>Opportunity type</name>
    </dashboardFilters>
    <dashboardType>MyTeamUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Order__c$Order_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product__c.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Won orders</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of FY won orders</metricLabel>
            <report>Dashboard_reports/KS_GJL_Metrics_16_Dec_10</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Order__c$Order_Count__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product__c.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the won &apos;Sales price&apos; of a Salesperson. Record count refers to the number of won orders.</footer>
            <groupingColumn>Order__c$Close_Date__c</groupingColumn>
            <header>Won orders</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Metrics_16_Dec_10</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total &apos;Sales price&apos;</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the win + loss rates % of all opps created in Salesforce. Win rate is calculated by (# of won opps / # total opps). Loss rate is calculated by (# lost opps / # total opps). % of open opps is calculated by (# open opps / # total opps).</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Win, loss and overdue opportunities rate %</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Win_rates_SP_11</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_3__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_4__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_5__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_6__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average Sales Stage duration (in days) for a Salesperson&apos;s won opportunities. Only opportunities created in Salesforce since the 12th May 2014 are included.</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Average Sales Stage duration</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_SS_duration</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Won opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the conversion rate of a Salesperson. This is the probability that an opportunity is calculated to have of progressing to the next Sales stage.</footer>
            <groupingColumn>ACCOUNT_OWNER</groupingColumn>
            <header>Conversion rate %</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Conversion_rate_SS_to_next_SS</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>From Sales Stage → Next Sales Stage</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Order__c.Order_Line_Items__r$Sales_Price__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product__c.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Won orders</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total &apos;Sales price&apos; of FY won orders</metricLabel>
            <report>Dashboard_reports/KS_GJL_Metrics_16_Dec_10</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Order_Line_Items__r.Order_Revenue_Schedules__r$Revenue__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product__c.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the won scheduled revenue of a Salesperson, split by schedule month.</footer>
            <groupingColumn>Order__c.Order_Line_Items__r.Order_Revenue_Schedules__r$Scheduled_Date__c</groupingColumn>
            <header>Won orders</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Metrics_16_Dec_11</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Scheduled revenue</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the win rate % of closed opportunities created in Salesforce since launch. Win rate is calculated by (# of won opps / total # of closed opps).</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Win rate of closed opportunities %</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Closed_win_rates_SP_12</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the lost &apos;Sales price&apos; of a Salesperson. Opportunity count refers to the number of lost opportunities.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Lost opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/X3_01_DR_SM_PeD_Lost_Opps_US</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total &apos;Sales price&apos;</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the conversion rate of a Salesperson. This is the probability that an opportunity is calculated to have of progressing from its current Sales Stage to &apos;Closed Won&apos;</footer>
            <groupingColumn>ACCOUNT_OWNER</groupingColumn>
            <header>Conversion rate %</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Conversion_rate_SS_to_Won</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>From Sales Stage → Won</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Order__c.Order_Line_Items__r.Order_Revenue_Schedules__r$Revenue__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product__c.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Won orders</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total FY scheduled revenue for won orders</metricLabel>
            <report>Dashboard_reports/KS_GJL_Metrics_16_Dec_11</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Order__c.Order_Line_Items__r.Order_Revenue_Schedules__r$Revenue__c.CONVERT</column>
            </chartSummary>
            <componentType>LineCumulative</componentType>
            <dashboardFilterColumns>
                <column>Order__c.Order_Line_Items__r$Product__c.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Order__c$Opportunity__c.Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the cumulative won scheduled revenue of a Salesperson, split by schedule month. Only orders won in the current FY are included.</footer>
            <groupingColumn>Order__c.Order_Line_Items__r.Order_Revenue_Schedules__r$Scheduled_Date__c</groupingColumn>
            <header>Won orders</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Metrics_16_Dec_11</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Cumulative scheduled revenue</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the retention rate of renewal opportunities. Only renewal opportunities closed in Salesforce since 9th December 2013 are included.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Retention rate %</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Retention_rate_SP</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>UNIT_PRICE.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>Product2.Global_Business_Line__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average &apos;Sales price&apos; for opportunities closing in  this financial year. Opportunity count refers to the number of opportunities in each status..</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Average value of opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJl_Average_deal_size</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closing in the current Financial Year</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>david.hilton@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Salesperson performance dashboard - US$</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
