<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>APAC</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>EMEA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Latin America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North America</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>UK&amp;I</values>
        </dashboardFilterOptions>
        <name>Opportunity owner&apos;s region</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Alicia Bohannon</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Terri Williams</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Donnie Gates</values>
        </dashboardFilterOptions>
        <name>Opportunity Owner</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the won ACV for opportunities won in the current FQ.</footer>
            <gaugeMax>9000000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Won ACV</header>
            <indicatorBreakpoint1>5500000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>7288961.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>X41st_Parameter_dashboard_reports/X41DR01_FN_TI_Closed_Won_CFQ_Opps</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Closed in current FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the won and late stage ACV for opportunities closing in the current FQ.</footer>
            <gaugeMax>9000000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Won and late stage ACV</header>
            <indicatorBreakpoint1>5500000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>7288961.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>X41st_Parameter_dashboard_reports/X41DR02_FN_TI_Closed_plus_LS_opps</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Closing in current FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the pipeline by sales stage for the current FQ. Revenue is measured by ACV.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline by Sales stage</header>
            <legendPosition>Bottom</legendPosition>
            <report>X41st_Parameter_dashboard_reports/X41DR03_FN_TI_Closed_Commit_Upside</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closing in current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the pipeline by sales stage for the next FQ. Revenue is measured by ACV.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline by Sales stage</header>
            <legendPosition>Bottom</legendPosition>
            <report>X41st_Parameter_dashboard_reports/X41DR04_FN_TI_Next_Quarter_Pipeline</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closing in next FQ</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 10 closed won opportunities for the current fiscal quarter. Opportunities are ranked by ACV.</footer>
            <header>Top won opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>X41st_Parameter_dashboard_reports/X41DR05_FN_TI_Top_won_opps_wo_orders</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Closing in current FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top open late stage opportunities closing in the current fiscal quarter. Opportunities are ranked by ACV.</footer>
            <header>Top late stage opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>X41st_Parameter_dashboard_reports/X41DR06_FN_TI_Top_Commit_LS_opps</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Closing in current FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top open early stage opportunities closing in the current fiscal quarter. Opportunities are ranked by ACV.</footer>
            <header>Top early stage opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>X41st_Parameter_dashboard_reports/X41DR07_FN_TI_Early_stage_opps_CFQ</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Closing in current FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top open opportunities closing in the next fiscal quarter. Opportunities are ranked by ACV.</footer>
            <header>Top opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>X41st_Parameter_dashboard_reports/X41DR08_FN_TI_Top_Upside_Opps_NFQ</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Closing in next FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline for the next fiscal quarter split by both Sales stage and Opportunity Owner region.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <groupingColumn>User.Region__c</groupingColumn>
            <header>Next Quarter pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>X41st_Parameter_dashboard_reports/X41DR09_FN_TI_Opps_NFQ_Stage_Opp</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closing in the next FQ</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the won ACV for each Region for the current fiscal quarter, split by product.</footer>
            <groupingColumn>User.Region__c</groupingColumn>
            <groupingColumn>PRODUCT_NAME</groupingColumn>
            <header>Closed won in current FQ</header>
            <legendPosition>Bottom</legendPosition>
            <report>X41st_Parameter_dashboard_reports/X41DR10_FN_TI_Won_CFQ_Prod_Opp_wo_orders</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>By Region and Product</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the closed won and late stage pipeline closing in the current fiscal quarter, split by Opportunity owner Region.</footer>
            <header>Closed won and late stage opps closing in current FQ</header>
            <legendPosition>Bottom</legendPosition>
            <report>X41st_Parameter_dashboard_reports/X41DR11_FN_TI_Late_stage_and_won_opps</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>By Region</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the early stage opportunities closing in the current fiscal quarter, split by Opportunity owner Region.</footer>
            <groupingColumn>User.Region__c</groupingColumn>
            <header>Open early stage opportunities closing in current FQ</header>
            <legendPosition>Bottom</legendPosition>
            <report>X41st_Parameter_dashboard_reports/X41DR12_FN_TI_Early_stage_opps_CFQ_Reg</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>By Region</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>User.Region__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>FULL_NAME</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Annual_Contract_Value__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top opportunities created in the current fiscal quarter. Opportunities are ranked by ACV.</footer>
            <header>Top opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>X41st_Parameter_dashboard_reports/X41DR13_FN_TI_Top_Opps_Created_CFQ</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Opportunities created in current FQ</title>
        </components>
    </rightSection>
    <runningUser>gareth.lee@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>41st Parameter dashboard - LS opp methodology</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
