<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From Existing</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Upsell (Existing Product Line)</values>
        </dashboardFilterOptions>
        <name>Opportunity Type</name>
    </dashboardFilters>
    <dashboardType>MyTeamUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ForecastingQuota$StartDate</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Total_Margin_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the % of quota achieved and total margin for opportunities closing this month. The data is ranked by quota % achieved.</footer>
            <header>Current month performance</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_CM</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Quota % achieved for current month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Attaining current month quota</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Current gap to achieve current month quota</metricLabel>
            <report>Dashboard_reports/Quota_SP_Closed_CM_Total_margin</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Total_Margin_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total margin of pipeline closing in the current month</metricLabel>
            <report>Dashboard_reports/Quota_SP_Pipeline_CM_Total_margin</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the ratio of pipeline closing in the current month compared to the current month quota.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Pipeline to quota ratio</header>
            <indicatorBreakpoint1>3.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>4.5</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/Quota_SP_Open_CM_Margin_Ratio</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Pipeline closing in the current month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Total_Margin_Value__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows current month performance versus quota target.</footer>
            <groupingColumn>ForecastingQuota$StartDate</groupingColumn>
            <header>Closed won Margin vs. current month Margin quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_CM</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in current month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the current month quota % achieved. The breakpoints for the gauge are 90% and 100%.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Current month quota performance</header>
            <indicatorBreakpoint1>90.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>100.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_CM</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>% month quota achieved to date</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Total_Margin_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 10 opps closing next month which can be potentially brought forward in to this month.</footer>
            <header>Potential opps to be brought forward into current month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/Quotas_SP_Next_CM_opportunities_MG</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Top 10 opps closing in next month</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ForecastingQuota$StartDate</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Total_Margin_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the % of quota achieved and total margin for opportunities closing this fiscal quarter. The data is ranked by quota % achieved.</footer>
            <header>FQ performance</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_FQ</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Quota % achieved for current FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Attaining FQ quota</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Current gap to achieve current FQ quota</metricLabel>
            <report>Dashboard_reports/Quota_SP_Closed_FQ_Total_margin</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Total_Margin_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total margin of pipeline closing in the current FQ</metricLabel>
            <report>Dashboard_reports/Quota_SP_Pipeline_FQ_Total_margin</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the ratio of pipeline closing in the current FQ compared to the current FQ quota.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Pipeline to quota ratio</header>
            <indicatorBreakpoint1>3.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>4.5</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/Quota_SP_Open_FQ_Margin_Ratio</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Pipeline closing in the current FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Total_Margin_Value__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows current FQ performance versus quota target.</footer>
            <groupingColumn>ForecastingQuota$StartDate</groupingColumn>
            <header>Closed won Margin vs. current FQ Margin quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_FQ</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the current FQ quota % achieved. The breakpoints for the gauge are 90% and 100%.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Current FQ quota performance</header>
            <indicatorBreakpoint1>90.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>100.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_FQ</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>% FQ quota achieved to date</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Total_Margin_Value__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the current pipeline, split by forecast category, and the quota amount for each month of the current FY.</footer>
            <groupingColumn>ForecastingQuota$StartDate</groupingColumn>
            <groupingColumn>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Forecast_Category__c</groupingColumn>
            <header>Pipeline forecast vs quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/Quota_SP_All_opp_fc_vs_quota_FY_TM</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current FY</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>ForecastingQuota$StartDate</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Total_Margin_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the % of quota achieved and total margin for opportunities closing this fiscal year. The data is ranked by quota % achieved.</footer>
            <header>FY performance</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_FY</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Quota % achieved for current FY</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Attaining FY quota</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Current gap to achieve current FY quota</metricLabel>
            <report>Dashboard_reports/Quota_SP_Closed_FY_Total_margin</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Total_Margin_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Total margin of pipeline closing in the current FY</metricLabel>
            <report>Dashboard_reports/Quota_SP_Pipeline_FY_Total_margin</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the ratio of pipeline closing in the current FY compared to the current FY quota.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Pipeline to quota ratio</header>
            <indicatorBreakpoint1>3.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>4.5</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/Quota_SP_Open_FY_Margin_Ratio</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Pipeline closing in the current FY</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Total_Margin_Value__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows current FY performance versus quota target.</footer>
            <groupingColumn>ForecastingQuota$StartDate</groupingColumn>
            <header>Closed won Margin vs. current FY Margin quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_FY</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Type</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the current FY quota % achieved. The breakpoints for the gauge are 90% and 100%.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Current FY quota performance</header>
            <indicatorBreakpoint1>90.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>100.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/EDQ_preformance_vs_quota_FY</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>% FY quota achieved to date</title>
        </components>
    </rightSection>
    <runningUser>nicholas.wilson@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Sales person quotas dashboard - Margin</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
