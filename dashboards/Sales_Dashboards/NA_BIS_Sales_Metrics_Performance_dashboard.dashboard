<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number and CSDA ACV of won New Business opportunities in the current and last 12 months. Opportunity count refers to the number of won opportunities.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>New Business wins</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/PSSMD11_Wins_CM_L12M</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closed in current + last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnStacked100</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the win and unsuccessful rate % of closed New business opportunities closed in the last 12 months. Win rate is calculated by (# of won opps / total # of closed opps).</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Win rate % by New business opportunity count</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/PSSMD16_Win_rate_CM_L12M</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closed in current + last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked100</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the % of won and unsuccessful CSDA ACV for New business opportunities closed in the last 12 months. Won revenue % is calculated by (won CSDA ACV / total closed CSDA ACV).</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Won revenue % by New business CSDA ACV</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/PSSMD16_Win_rate_CM_L12M</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closed in current + last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnStacked100</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the win and unsuccessful rate % of closed Renewal business opportunities closed in the last 12 months. Win rate is calculated by (# of won opps / total # of closed opps).</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Win rate % by Renewal business opportunity count</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/PSSMD26_Win_rate_CM_L12M</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closed in current + last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked100</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the % of won and unsuccessful CSDA ACV for Renewal business opportunities closed in the last 12 months. Won revenue % is calculated by (won CSDA ACV / total closed CSDA ACV).</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Won revenue % by Renewal business CSDA ACV</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/PSSMD26_Win_rate_CM_L12M</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps closed in current + last 12 months</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Won New business opportunities in last month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of won New Business opportunities</metricLabel>
            <report>Dashboard_reports/PSSMD14_Top_wins</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>The above components show the number of CSDA ACV of won new Business opportunities in the last month.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Won New Business CSDA ACV</metricLabel>
            <report>Dashboard_reports/PSSMD14_Top_wins</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>ACCOUNT.NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 5 won New Business opportunities closed in the last month. Opportunities are ranked by CSDA ACV.</footer>
            <header>Top 5 won New business opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Dashboard_reports/PSSMD14_Top_wins</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Opps closed in the last month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the conversion rate % of closed New Business opportunities from each sales stage to Execute. Only opportunities created in SFDC over the last 365 days..</footer>
            <groupingColumn>OpportunityTeamMember.Team_Member_Region__c</groupingColumn>
            <header>Conversion rate % of closed New Business opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/PSSMD18_Conversion_rate</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>SFDC opps created in last 365 days</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Won renewal business opportunities in last month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of won Renewal Business opportunities</metricLabel>
            <report>Dashboard_reports/PSSMD14_Top_RB_wins</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Won Renewal Business CSDA ACV</metricLabel>
            <report>Dashboard_reports/PSSMD14_Top_RB_wins</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>ACCOUNT.NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 5 won New Business opportunities closed in the last month. Opportunities are ranked by CSDA ACV.</footer>
            <header>Top 5 won renewal business opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Dashboard_reports/PSSMD14_Top_RB_wins</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Opps closed in the last month</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Unsuccesful New business opportunities in last month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of unsuccessful New Business opportunities</metricLabel>
            <report>Dashboard_reports/PSSMD15_Top_unsuccessful_opportunities</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>The above components show the number of CSDA ACV of unsuccessful new Business opportunities in the last month.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Unsuccessful New Business CSDA ACV</metricLabel>
            <report>Dashboard_reports/PSSMD15_Top_unsuccessful_opportunities</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>ACCOUNT.NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 5 unsuccessful New Business opportunities closed in the last month. Opportunities are ranked by CSDA ACV.</footer>
            <header>Top 5 unsuccessful New business opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Dashboard_reports/PSSMD15_Top_unsuccessful_opportunities</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Opps closed in the last month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_3__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_4__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_5__c</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Days_in_Stage_6__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average stage duration (in days) for closed New Business opportunities, split by stage. Only opportunities created in SFDC over the last 365 days are included.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Average stage duration of closed New Business opps</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/PSSMD19_Avg_stage_duration_NB_closed</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by closed stage</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Unsuccesful New business opportunities in last month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of unsuccessful Renewal Business opportunities</metricLabel>
            <report>Dashboard_reports/PSSMD25_Top_unsuccessful_RB_opps</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Unsuccessful Renewal Business CSDA ACV</metricLabel>
            <report>Dashboard_reports/PSSMD25_Top_unsuccessful_RB_opps</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>ACCOUNT.NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows the top 5 unsuccessful Renewal Business opportunities closed in the last month. Opportunities are ranked by CSDA ACV.</footer>
            <header>Top 5 unsuccessful renewal business opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Dashboard_reports/PSSMD25_Top_unsuccessful_RB_opps</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Opps closed in the last month</title>
        </components>
    </rightSection>
    <runningUser>mark.parysz@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>NA BIS Sales Metrics Performance dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
