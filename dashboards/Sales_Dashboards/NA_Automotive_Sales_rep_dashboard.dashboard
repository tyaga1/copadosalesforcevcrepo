<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows all new opportunities added from the current and previous FQ.</footer>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <header>Quarterly BU Pipeline - Current &amp; Previous FQ by Annual Revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT01_Quarterly_Opportunities_NB</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>New Business Opportunities Added</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows all new opportunities added from the current and previous FQ.</footer>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <header>Quarterly BU Pipeline - Current &amp; Previous FQ by Current Fiscal Yr Revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT01_Quarterly_Opportunities_NB</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>New Business Opportunities Added</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component will show an aging analysis of opportunities in their current stages and total record count</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <groupingColumn>BucketField_86149364</groupingColumn>
            <header>Opportunity Aging Analysis by Count</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT06_Opp_Aging_Analysis_By_Stage</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Stages &amp; Total Opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FULL_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the total annual revenue associated with the opportunities over $50k with a 50% chance of closing grouped by Sales rep.</footer>
            <header>Top Opportunities 50/50</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/AUT09_Quarterly_Wins_by_Rep_Amount</report>
            <showPicturesOnTables>true</showPicturesOnTables>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows all existing and renewal opportunities added from the current and previous FQ.</footer>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <groupingColumn>TYPE</groupingColumn>
            <header>Quarterly BU Pipeline - Current &amp; Previous FQ by Annual Revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT02_Quarterly_Opportunities_RB</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Existing &amp; Renewal Opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <groupingColumn>TYPE</groupingColumn>
            <header>Quarterly BU Pipeline - Current &amp; Previous FQ by Current Fiscal Yr Revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT02_Quarterly_Opportunities_RB</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Existing &amp; Renewal Opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component will show an aging analysis of opportunities in their current stages and total revenue associated with the opportunity by stage.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <groupingColumn>BucketField_86149364</groupingColumn>
            <header>Opportunity Aging Analysis by Revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT06_Opp_Aging_Analysis_By_Stage</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Stages &amp; Total Annual Revenue</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FULL_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows wins summarized by total annual revenue amount for the current and previous fiscal quarters.</footer>
            <header>Quarterly Wins - Current &amp; Previous FQ</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/AUT09_Quarterly_Wins_by_Rep_Amount</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Wins by Total Amount</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows annual revenue from a cumulative view for all opportunities added from the current and previous FQ.</footer>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <header>Quarterly BU Pipeline - Current &amp; Previous FQ by Annual Revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT04_Total_optys</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total Oppty&apos;s - New, Existing &amp; Renewal</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows current fiscal year revenue from a cumulative view for all opportunities added from the current and previous FQ.</footer>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <header>Quarterly BU Pipeline - Current &amp; Previous FQ by Current Fiscal Yr Revenue</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/AUT04_Total_optys</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total Oppty&apos;s - New, Existing &amp; Renewal</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>CONVERTED</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component will show the average % in closed lead conversion rates by rep for the current FY.</footer>
            <groupingColumn>LEAD_OWNER</groupingColumn>
            <header>Lead Close Rates - Current FY</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/AUT08_Lead_Close_Rates_Current_FY</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>true</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Converted Leads - Avg % Closed</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>alexander.lethbridge@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>NA Automotive Sales rep dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
