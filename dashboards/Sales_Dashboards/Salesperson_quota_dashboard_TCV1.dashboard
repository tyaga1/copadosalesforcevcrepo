<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>MyTeamUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Attaining current month quota</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Current gap to achieve current month quota</metricLabel>
            <report>Quotas/Quota_Closed_CM_TCV1</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>TCV of pipeline closing in the current month</metricLabel>
            <report>Quotas/Quota_Pipeline_CM_TCV</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Amount.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>his component shows a Salesperson&apos;s current month performance versus their quota target.</footer>
            <groupingColumn>ForecastingQuota$Owner</groupingColumn>
            <header>Closed won TCV vs. current month TCV quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Quotas/Quota_Closed_CM_TCV1</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in current month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the current month quota % achieved by a Salesperson. The breakpoints for the gauge are 90% and 100%.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Current month quota performance</header>
            <indicatorBreakpoint1>90.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>100.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Quotas/Quota_Closed_CM_TCV1</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>% month quota achieved to date</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the pipeline, split by Sales stage, for the current month. Revenue is measured in TCV.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>Quotas/Quota_Pipeline_CM_TCV</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closing in current month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows open opportunities closing next month. Opportunities are ranked by their sales stage.</footer>
            <header>Potential opps to be brought forward into current month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Quotas/Quotas_Next_CM_opportunities_TCV</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 opps closing in next month</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Amount.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the current pipeline, split by forecast category, and a Salesperson&apos;s quota for each month of the current FY.</footer>
            <groupingColumn>ForecastingQuota$StartDate</groupingColumn>
            <groupingColumn>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps*Opportunity$Forecast_Category__c</groupingColumn>
            <header>Pipeline forecast vs quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Quotas/Quota_All_opps_forecast_vs_quota_FY</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current FY</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Attaining FQ quota</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Current gap to achieve current FQ quota</metricLabel>
            <report>Quotas/Quota_Closed_CM_TCV</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>TCV of pipeline closing in the current FQ</metricLabel>
            <report>Quotas/Quota_Open_closed_FQ_TCV1</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Amount.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows a Salesperson&apos;s current FQ performance versus their quota target.</footer>
            <groupingColumn>ForecastingQuota$Owner</groupingColumn>
            <header>Closed won TCV vs. current FQ TCV quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Quotas/Quota_Closed_CM_TCV</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the current FQ quota % achieved by a Salesperson. The breakpoints for the gauge are 90% and 100%.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Current FQ quota performance</header>
            <indicatorBreakpoint1>90.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>100.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Quotas/Quota_Closed_CM_TCV</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>% FQ quota achieved to date</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the pipeline, split by Sales stage, for the current FQ. Revenue is measured in TCV.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>Quotas/Quota_Open_closed_FQ_TCV1</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closing in current FQ</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows open opportunities closing next fiscal quarter. Opportunities are ranked by their sales stage.</footer>
            <header>Potential opps to be brought forward into current FQ</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Quotas/Quotas_Next_FQ_opportunities_TCV</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 opps closing in next FQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA4</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the conversion rate for all Experian opportunities. This is the probability that an opportunity is calculated to have of progressing from its current Sales Stage to &apos;Execute&apos;.</footer>
            <groupingColumn>ACCOUNT_OWNER</groupingColumn>
            <header>Conversion rate %</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/KS_GJL_Conversion_rate_SS_to_Won</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>From Sales Stage → Won</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <header>Attaining FY quota</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Current gap to achieve current FY quota</metricLabel>
            <report>Quotas/Quota_Closed_FY_TCV</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Integer</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>TCV of pipeline closing in the current FY</metricLabel>
            <report>Quotas/Quota_Open_closed_FY_TCV</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota.ForecastingQuotaItems.ForecastingFactOpps$Amount.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>ForecastingQuota$QuotaAmount.CONVERT</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows a Salesperson&apos;s current FY performance versus their quota target.</footer>
            <groupingColumn>ForecastingQuota$Owner</groupingColumn>
            <header>Closed won TCV vs. current FY TCV quota</header>
            <legendPosition>Bottom</legendPosition>
            <report>Quotas/Quota_Closed_FY_TCV</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closed in current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the current FY quota % achieved by a Salesperson. The breakpoints for the gauge are 90% and 100%.</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Current FY quota performance</header>
            <indicatorBreakpoint1>90.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>100.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Quotas/Quota_Closed_FY_TCV</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>% FY quota achieved to date</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the pipeline, split by Sales stage, for the current FY. Revenue is measured in TCV.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>Quotas/Quota_Open_closed_FY_TCV</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Closing in current FY</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows open opportunities closing in the next fiscal year. Opportunities are ranked by their sales stage.</footer>
            <header>Potential opps to be brought forward into current FY</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Quotas/Quotas_Next_FY_opportunities_TCV</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 10 opps closing in next FY</title>
        </components>
    </rightSection>
    <runningUser>suzett.lawrence@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Salesperson quota dashboard - TCV</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
