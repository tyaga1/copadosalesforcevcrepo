<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From Existing</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From New</values>
        </dashboardFilterOptions>
        <name>Opportunity type</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS CIS,NA CS BIS,NA CS Public Sector,NA CS Automotive</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS CIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS BIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS Public Sector</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS Automotive</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA,NA DA Fraud and Identity,NA DA Global Consulting Practice,NA DA Sciences,NA DA Software Solutions</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA Fraud and Identity</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA Global Consulting Practice</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA Sciences</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA Software Solutions</values>
        </dashboardFilterOptions>
        <name>Owner&apos;s Business Unit</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH,LAST_N_MONTHS:12</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH,LAST_N_MONTHS:11</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_QUARTER</values>
        </dashboardFilterOptions>
        <name>Close date range</name>
    </dashboardFilters>
    <dashboardType>MyTeamUser</dashboardType>
    <description>Dashboard is designed to be used by sales managers in the North America CSDA business. The dashboard is designed so that a sales manager can see their teams quoting statistics.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Opportunity$Owner</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA3</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA2</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the # of opportunities created, # won and the # of quotes created by sales rep. # of opportunities and quotes created is based on all closed and open quotes. Chart is ranked by # of quotes created.</footer>
            <header># of opps, opps won and quotes by sales rep</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/CSDA_CPQ46_Quotes_Count_Won_Owner</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Ranked by # of quotes created</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>BarStacked</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component is designed to show the number of quotes by quote status and sales rep. Data is based on open opportunities only.</footer>
            <groupingColumn>Opportunity.Quotes__r$Status__c</groupingColumn>
            <groupingColumn>Opportunity$Owner</groupingColumn>
            <header># of quotes by quote status and sales rep</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CSDA_CPQ69_Quotes_by_Status</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>for open opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of quotes and the number of opportunities won that are created each month. Record count refers to the number of quotes created. Quote count is for both open and closed opportunities.</footer>
            <groupingColumn>Opportunity$CreatedDate</groupingColumn>
            <header># of quotes and opportunities won</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CSDA_CPQ_52_of_Quotes_Created_Won</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by quote created date</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>User.Sales_Team__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Days_in_Stage_3__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Days_in_Stage_4__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the average lifespan and average amount of days spent in the qualify and propose sales stages of an opportunity by sales team for won opportunities. Lifespan of an opportunity is based on the time it&apos;s created to the time it&apos;s closed.</footer>
            <header>Sales velocity (days)</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Dashboard_reports/CSDA_CPQ56_Sales_Stage_Velocity</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>by sales team</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Opportunity$Owner</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <decimalPlaces>1</decimalPlaces>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>true</calculatePercent>
                <column>FORMULA3</column>
                <decimalPlaces>1</decimalPlaces>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component is designed to show the average # of quotes per an opportunity and the quote conversion rate by sales rep. Data is based on open and closed opportunities and is ranked by quote conversion rate.</footer>
            <header>Average # quotes per opportunity and quote conversion rate by sales rep</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/CSDA_CPQ47_Avg_Quotes_Deal_Size</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Ranked by average # of quotes per opp</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>FULL_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>true</calculatePercent>
                <column>FORMULA2</column>
                <decimalPlaces>1</decimalPlaces>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Days_in_Stage_3__c</column>
                <decimalPlaces>1</decimalPlaces>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Days_in_Stage_4__c</column>
                <decimalPlaces>1</decimalPlaces>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component is designed to show the average number of days spent in the qualify and propose sales stages and the % of time a sales reps opps have spent in these stages compared to the lifespan of the opps. Data is based on open and closed opps.</footer>
            <header>Sales velocity by sales rep</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/CSDA_CPQ48_Avg_Age_Owner</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Ranked by days spent in qualify stage</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of quotes and average number of quotes per an opportunity by deal size. The deal size bracket is based on the CSDA Annual Contract Value field. Chart is based on closed and open opportunities.</footer>
            <groupingColumn>BucketField_89571187</groupingColumn>
            <header># of quotes and average # of quotes per opportunity</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CSDA_CPQ70_Deal_size_All</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by deal size</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average deal size of won opportunities by opportunity close date. Deal size is based on the CSDA Annual Contract Value field.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Average deal size</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CSDA_CPQ06_Average_Deal_Size</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by close date</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>FULL_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>OpportunityLineItem.Adj_Percentage__c</column>
                <decimalPlaces>1</decimalPlaces>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows the average discount % and average deal size by opportunity owner. Average deal size is based on CSDA annual contract value. Data is based on won opportunities and is ranked by discount %.</footer>
            <header>Average discount % and average deal size by sales rep</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/CSDA_CPQ50_Avg_Disc_by_Salesperson</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Ranked by average discount %</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Sales_Price__c.CONVERT</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows top 10 product groups by stage based on total annual sales price. Data is based on open opportunities.</footer>
            <groupingColumn>Product2.CSDA_Product_Group__c</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Top 10 product groups by stage based on total annual sales price</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Dashboard_reports/CSDA_CPQ03_Top_10_Prods_for_open_opps</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>for open opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>ColumnGrouped</componentType>
            <dashboardFilterColumns>
                <column>Opportunity$Type</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity$CloseDate</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the average number of quotes created per an opportunity by opportunity status and opportunity close date. Data is based on closed opportunities only.</footer>
            <groupingColumn>BucketField_18704863</groupingColumn>
            <groupingColumn>Opportunity$CloseDate</groupingColumn>
            <header>Average # of quotes per opportunity</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CSDA_CPQ54_Avg_quotes_deal_size</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by status and close date</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA2</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA3</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Opportunity.Owner_s_Business_Unit__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the win, loss and no decision rate for closed opportunities by opportunity close date.</footer>
            <groupingColumn>CLOSE_DATE</groupingColumn>
            <header>Win, loss and no decision rate</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/CSDA_CPQ55_Win_Loss_Rates</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by close date</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>alexander.lethbridge@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>CSDA CPQ Sales Manager Dashboard - US$</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
