<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Component shows closed won deals this month linked to campaign codes</footer>
            <header>Closed won deals this month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>unfiled$public/Marketing_CI_T_Closed_Opp_CC_All</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>CC</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Component shows closed lost and closed no decision deals this month that are linked to campaigns</footer>
            <header>Closed lost deals this month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>unfiled$public/Marketing_CI_T_Lost_Opps_This_Month</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>CC</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Closed deals this month that are not linked to any campaigns</footer>
            <header>Closed won deals this month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>unfiled$public/Marketing_CI_T_Closed_Opp_No_CC_All</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>No CC</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Component shows quantity of leads created this month that are linked to specific campaign codes split by sales team</footer>
            <groupingColumn>BucketField_35408621</groupingColumn>
            <groupingColumn>STATUS</groupingColumn>
            <header>Leads</header>
            <legendPosition>Bottom</legendPosition>
            <report>unfiled$public/Marketing_CI_T_Leads_This_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Created this month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Component shows the value of opps due to close this month by stage (greater than 4) that are linked to specified campaign codes</footer>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>unfiled$public/Marketing_CI_T_Pipeline_This_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps due to close this month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Component shows the value of opps due to close current FQ by stage (greater than 4) that are linked to specified campaign codes</footer>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>unfiled$public/Marketing_CI_T_Pipeline_This_FQ</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps due to close this FQ</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Component shows quantity of leads that are linked to specific campaign codes split by status and sales team</footer>
            <groupingColumn>STATUS</groupingColumn>
            <groupingColumn>BucketField_50954600</groupingColumn>
            <header>Leads</header>
            <legendPosition>Bottom</legendPosition>
            <report>unfiled$public/Marketing_CI_T_Qty_Leads_by_Status</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Qty by status</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Component shows the value of opps due to close next month by stage (greater than 4) that are linked to specified campaign codes</footer>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>unfiled$public/Marketing_CI_T_Pipeline_Next_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Opps due to close next month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Component shows the total value of open opps by sales team (greater than 4) that are linked to specified campaign codes</footer>
            <groupingColumn>BucketField_48093919</groupingColumn>
            <header>Pipeline</header>
            <legendPosition>Bottom</legendPosition>
            <report>unfiled$public/Marketing_CI_T_Pipeline_By_Team</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total by team</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>sarah.lgreen@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>UK&amp;I CI&amp;T - Marketing Manager Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
