<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Shows total activities created and the current status of those activities.</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_Total_Activities_NA_CIS</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Total Activities (Fiscal YTD)</title>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All numbers shown are Fiscal YTD.</footer>
            <groupingColumn>INDUSTRY</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_NA_CIS_Campaigns</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 Industries by Activity Count</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Shows total activities created by month.</footer>
            <groupingColumn>CREATED_DATE</groupingColumn>
            <groupingColumn>STATUS</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_Total_Activities_NA_CIS</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Activities by Month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All numbers shown are Fiscal YTD.</footer>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_NA_CIS_Campaigns</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 Campaigns by Activity Count</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Shows activities that are still in &quot;Not Started&quot; status.</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_Activities_Not_Started_NA_CIS</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Activities Still In &quot;Not Started&quot; Status</title>
            <useReportChart>true</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Shows the % of activities that have been reviewed and  had their status changed to &quot;completed&quot;, &quot;cancelled&quot;, &quot;deferred&quot;, or &quot;rejected&quot;. All numbers shown are Fiscal YTD.</footer>
            <gaugeMax>100.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <indicatorBreakpoint1>25.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>75.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#54C254</indicatorLowColor>
            <indicatorMiddleColor>#54C254</indicatorMiddleColor>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_Total_Activities_NA_CIS</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Activities Closed Out</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>All numbers shown are Fiscal YTD.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>10</maxValuesDisplayed>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_Activities_Accounts_NA_CIS</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
            <title>Top 10 Accounts by Activity Count</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>SUBJECT</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>The &quot;subject&quot; field of an activity gives insight into where the activity came from and what action was involved in creating it. All numbers shown are Fiscal YTD.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Web_Analytics_Mktg_Automation_Staging/MAuto_Activities_Types_NA_CIS</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Activities by Subject</title>
        </components>
    </rightSection>
    <runningUser>nicholas.hopkins@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>(Alpha) MKTNG - NA - CIS - Activities Created via Automated Integration</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
