<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>LAST_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_N_MONTHS:12</values>
        </dashboardFilterOptions>
        <name>Activity date/Case open date</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Terry Butler,Sarah Barber,Adrian Mair,James Weatherall</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Natalie Rowley,Colleen Stauffacher,Chennour Wright,Kelli Stephenson</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Simon Kent,Allie Davidge,Stephen Hanson-Abbott,Kamila Trzasko</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Mark Parysz,Elodie Auger,Monica Saborio,Catalina Blandon,Gabriela Sanders,Kaloyan Petrov,Sergio Carvajal,Veronika Peycheva,Aleksandar Naydenov,Vladimir Nikolov,Joshua Edwards</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Maria Saldivar,Joseph Marconi,Matias Zanni,Paulina Ahumada,Olga Kublik,Carmen Lopez,Hui Yee Lim,Celina Hsiung,Jose Gutierrez</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Jim Fick,Tom Mueller</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Gareth Lee,Alex Lethbridge</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Dan Zyrek,Mary Shiang,Vinny Mainolfi,Chris Metzke,Erin Rezmer,Claire Robinson</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Alison Collins,Aniksha Sanghani,Thelma Onyeka,Evelyn Sevilla,Conrad Bator</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Gary Sonnenthal,Ron Nagel</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Geoff Gordon,Lee Glenn</values>
        </dashboardFilterOptions>
        <name>Team</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>DUE_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>ASSIGNED</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of pending tasks by team.</footer>
            <groupingColumn>Activity.AssignedToSalesTeam__c</groupingColumn>
            <header># of scheduled tasks</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD04_open_tasks_by_user</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by team</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Donut</componentType>
            <dashboardFilterColumns>
                <column>DUE_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>ASSIGNED</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of completed tasks by type. Data does not extend further back than the last 12 months.</footer>
            <groupingColumn>TASK_TYPE</groupingColumn>
            <header># of completed tasks</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD07_completed_tasks_by_type</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by type</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>DUE_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>ASSIGNED</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of completed feedback tasks by business unit. Data does not extend further back than the last 12 months.</footer>
            <groupingColumn>Contact.Business_Unit__c</groupingColumn>
            <header># of completed feedback tasks</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD03_completed_feedback_tasks_by_biz</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by business unit</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Case.Entitlement_Escalation_Manager__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of closed cases by month the case was closed in. Data does not extend further back than the last 12 months. Team filter does not work on this component.</footer>
            <header>Closed cases</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD13_Cases_by_closed_month</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by month closed</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>DUE_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>ASSIGNED</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of overdue tasks by team.</footer>
            <groupingColumn>Activity.AssignedToSalesTeam__c</groupingColumn>
            <header># of overdue tasks</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD14_overdue_tasks_by_team</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by team</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>DUE_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>ASSIGNED</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of completed tasks by team and month. Data does not extend further back than the last 12 months.</footer>
            <groupingColumn>DUE_DATE</groupingColumn>
            <groupingColumn>Activity.AssignedToSalesTeam__c</groupingColumn>
            <header># of completed tasks</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD05_completed_tasks_by_user</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by team and month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Contact$Authentication_Date__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Contact$Authentication_Processed_By__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of training sessions delivered by month and to what group attended the session. Data does not extend further back than the last 12 months. Filters do not work on this component.</footer>
            <groupingColumn>Contact.Training__r$Training_Date__c</groupingColumn>
            <groupingColumn>Contact.Training__r$Training_Group__c</groupingColumn>
            <header>Training delivered</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD10_Training_with_users</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by month and group</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Case.Entitlement_Escalation_Manager__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of cases closed by requester business unit. Data does not extend further back than the last 12 months. Team filter does not work on this component.</footer>
            <header>Closed cases</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD11_Cases_by_requestor_BU</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by business unit</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>DUE_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>ASSIGNED</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of completed tasks by business unit. Data does not extend further back than the last 12 months.</footer>
            <groupingColumn>Contact.Business_Unit__c</groupingColumn>
            <header># of completed tasks</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD06_completed_tasks_by_BU</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by business unit</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>DUE_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>ASSIGNED</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>Activity.AssignedToSalesTeam__c</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This table shows the number of completed tasks by team. Data does not extend further back than the last 12 months.</footer>
            <header># of completed tasks</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>20</maxValuesDisplayed>
            <report>Dashboard_reports/GSOD08_completed_tasks_by_user_table</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>by team</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>CREATED_DATE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>Case.Entitlement_Escalation_Manager__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the # of cases closed by case reason. Data does not extend further back than the last 12 months. Team filter does not work on this component.</footer>
            <header>Closed cases</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/GSOD12_Cases_by_reason</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>by reason</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>alexander.lethbridge@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>GSO engagement dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
