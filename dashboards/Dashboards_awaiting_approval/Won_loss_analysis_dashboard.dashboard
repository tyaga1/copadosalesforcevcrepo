<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS BIS,NA CS CIS,NA CS Public Sector,NA DA,NA DA Fraud and Identity,NA MS AdTruth,NA MS CCM,NA MS Consumer Insights &amp; Targeting,NA MS Data Quality,NA MS SME,NA MS Strategic Sales</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS BIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS CIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA CS Public Sector</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA DA Fraud and Identity</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA MS AdTruth</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA MS CCM</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA MS Consumer Insights &amp; Targeting</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NA MS Data Quality</values>
        </dashboardFilterOptions>
        <name>Business Unit value</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r$Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Identifying winning competitors</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of opps lost to &apos;None/Not Known/Blank/Would not say&apos; competitors</metricLabel>
            <report>Dashboard_reports/WLA03_Comp_None_Lost_no_decision</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r.OpportunityLineItems$Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>These components show the number and value of opportunities lost to competitors who have not been identified.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>FY impact of opps lost to &apos;None/Not Known/Blank/Would not say&apos; competitors</metricLabel>
            <report>Dashboard_reports/WLA03_Comp_None_Lost_no_decision</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r$Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Identifying competitors in open opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of open opps with &apos;None/Not Known/Blank/Would not say&apos; competitors</metricLabel>
            <report>Dashboard_reports/WLA04_Comp_None_Open</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r.OpportunityLineItems$Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>These components show the number and value of open opportunities which currently have competitors who have not been identified.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>FY impact of open opps with &apos;None/Not Known/Blank/Would not say&apos; competitors</metricLabel>
            <report>Dashboard_reports/WLA04_Comp_None_Open</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r$Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Lost opportunities with &apos;Other&apos; reason</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of opps lost to &apos;Other/Blank&apos; reason</metricLabel>
            <report>Dashboard_reports/WLA06_Other_reason_Lost_no_decision</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r.OpportunityLineItems$Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>These components show the number and value of lost opportunities with the &apos;Other&apos; closed reason.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>FY impact of opps lost to &apos;Other/Blank&apos; reason</metricLabel>
            <report>Dashboard_reports/WLA06_Other_reason_Lost_no_decision</report>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r$Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the percentage and number of opportunities lost to competitors.</footer>
            <groupingColumn>Account.Account_Segments__r.Opportunities_Business_Units__r$Primary_Winning_Competitor__c</groupingColumn>
            <header>Identifying winning competitors</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/WLA02_Competitor_Lost_no_decision</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>% of opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r$Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the percentage and number of opportunities lost to each closed reason.</footer>
            <groupingColumn>Account.Account_Segments__r.Opportunities_Business_Units__r$Primary_Reason_W_L__c</groupingColumn>
            <header>Lost reasons</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/WLA05_Reason_Closed</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>% of opportunities</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r.OpportunityLineItems$Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the percentage and FY revenue impact of opportunities lost to competitors.</footer>
            <groupingColumn>Account.Account_Segments__r.Opportunities_Business_Units__r$Primary_Winning_Competitor__c</groupingColumn>
            <header>Identifying winning competitors</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/WLA02_Competitor_Lost_no_decision</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>% of Current FY revenue impact</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Account.Account_Segments__r.Opportunities_Business_Units__r.OpportunityLineItems$Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <dashboardFilterColumns>
                <column>Account.Account_Segments__r$Value__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the percentage and FY revenue impact of opportunities lost to each closed reason.</footer>
            <groupingColumn>Account.Account_Segments__r.Opportunities_Business_Units__r$Primary_Reason_W_L__c</groupingColumn>
            <header>Lost reasons</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/WLA05_Reason_Closed</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>% of Current FY revenue impact</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>gareth.lee@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Won loss analysis dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
