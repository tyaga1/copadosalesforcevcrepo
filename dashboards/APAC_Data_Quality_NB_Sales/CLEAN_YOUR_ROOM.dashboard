<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>This dashboard identifies the individual account managers, and teams not adhering to Salesforce best practice.

DQ Score: https://experian.my.salesforce.com/sfc/p/#i0000000faSr/a/i0000000Xa7O/IaDLOV5HY3XYl24UCJ723VOd1NM_OQE2VInYdx33v5I</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Number of overdue opportunities and sum of $TCV by AM</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>AM Overdue Opportunities &amp; $TCV</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Overdue_Opps7</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Account Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>These opps WILL NOT show in Individual Dashboard MTD or Pipeline</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Opportunities Without FY Margin $</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Opportunites_Without_FY_Margin</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Account Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Opportunities with a close date this month, or in the next 2 months</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Opportunities without an Opp Sheet</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Opps_Without_Opp_Sheets</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Account Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Total_Margin_Value__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Signed contract to be uploaded and saved to &apos;Confidential Information&apos; tab</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Executed Opportunities Without Signed Contract</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/All_Opps_Without_Confidential_Info</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Account Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ASSIGNED</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Open Tasks assigned by the Marketing team to AM&apos;s that are overdue</footer>
            <header>Open Marketing Tasks</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/Open_Tasks1</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Overdue Marketing Tasks</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Number of opportunities without the &apos;Primary Campaign Source&apos; field entered</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Opportunities Without Primary Campaign Source</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Opps_Without_Primary_Campaign_Source1</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Account Manager</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Manager/Team Volume of Overdue Opportunities</footer>
            <groupingColumn>OWNER_MANAGER</groupingColumn>
            <header>Overdue Opportunities</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Overdue_Opps</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Team/Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Number of Opportunities with $TCV but missing FY Margin $</footer>
            <groupingColumn>OWNER_MANAGER</groupingColumn>
            <header>Opportunities Without FY Margin $</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_Without_FY_Margin</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Team/Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Opportunities with a close date this month, or in the next 2 months</footer>
            <groupingColumn>OWNER_MANAGER</groupingColumn>
            <header>Opportunities without an Opp Sheet</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_Without_Opp_Sheet</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Team/Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>AMOUNT</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Signed contract to be uploaded and saved to &apos;Confidential Information&apos; tab</footer>
            <groupingColumn>OWNER_MANAGER</groupingColumn>
            <header>Executed Opportunities Without Signed Contract</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_w_o_Confidential_I</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Team/Manager</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OWNER</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Leads with status Open assigned by the Marketing team</footer>
            <header>Open Marketing Leads</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>User_created_reports/X1_3_A_NZ_Leads_DQ_Open_Overdue</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Lead status = Open</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Pie</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Number of opportunities without the &apos;Primary Campaign Source&apos; field entered</footer>
            <groupingColumn>OWNER_MANAGER</groupingColumn>
            <header>Opportunities Without Primary Campaign Source</header>
            <legendPosition>Bottom</legendPosition>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_w_o_Primary_Campaign_S</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Team/Manager</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OWNER_MANAGER</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Sum of $TCV of Teams Overdue Opps</footer>
            <header>$TCV of Overdue Opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Overdue_Opps</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>$TCV of Overdue Opportunities</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OWNER_MANAGER</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Sum of $TCV of Opps w/o FY Margin</footer>
            <header>$TCV of Opps w/o FY Margin</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_Without_FY_Margin</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>$TCV of Opps w/o FY Margin</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OWNER_MANAGER</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Sum of $TCV of Opps w/o Opp Sheet with a close date this month, or in the next 2 months</footer>
            <header>$TCV of Opps w/o Opp Sheet</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_Without_Opp_Sheet</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>$TCV of Opps w/o Opp Sheet</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OWNER_MANAGER</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Sum of $TCV of Opps w/o Signed Contract</footer>
            <header>$TCV of Opps Without Signed Contract</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_w_o_Confidential_I</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>$TCV of Opps Without Signed Contract</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OWNER</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Leads with status Qualifying or Sales Accepted</footer>
            <header>Marketing Leads</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/A_NZ_Leads_DQ_In_progress</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>Leads in progress</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Sum of opportunities without &apos;Primary Campaign Source&apos;</footer>
            <header>Opportunities Without Primary Campaign Source</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/Manager_Team_Opps_w_o_Primary_Campaign_S</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FK_OPP_OWNER_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>FK_OPP_AMOUNT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Opportunities executed where we are still waiting for a valid PO from the client. This affects our Bad Debt and EBIT</footer>
            <header>Executed Opps - Waiting for a PO</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_CUSTOMER_OPERATIONS/Pending_a_PO</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Account Manager</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>FULL_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Opportunity_DQ_Score__c</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FORMULA1</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>The CRM DQ Score provides a quick view to the health of opps based on the presence or condition of certain elements.</footer>
            <header>Executed Opps - Average DQ Score %</header>
            <indicatorBreakpoint1>70.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>80.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_DATA_QUALITY_NB_SALES/AM_Average_DQ_Score</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Account Manager</title>
        </components>
    </rightSection>
    <runningUser>john.sichau@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>CLEAN YOUR ROOM!</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
