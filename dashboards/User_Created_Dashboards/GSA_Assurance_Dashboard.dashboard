<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>LoggedInUser</dashboardType>
    <description>Recognition Dashboard outlining recognition received &amp; given, most popular badges, and rewards usage.</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Badges received this week</header>
            <legendPosition>Bottom</legendPosition>
            <report>Work_com_Recognition_Reports/Badges_received_this_week_for_Managers</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Top Users Recognized this week</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Users by badges/thanks received</footer>
            <groupingColumn>WorkThanks.Badges$Recipient</groupingColumn>
            <header>Who is Getting Recognized?</header>
            <legendPosition>Bottom</legendPosition>
            <report>Work_com_Recognition_Reports/Manager_Thanks_Given_Recipient_Data</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>true</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top Users being Recognized</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Users by Thanks given</footer>
            <groupingColumn>WorkThanks$Giver</groupingColumn>
            <header>Who is Giving Recognition?</header>
            <legendPosition>Bottom</legendPosition>
            <report>Work_com_Recognition_Reports/Manager_Thanks_by_Giver</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>true</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top Users giving Recognition</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>WorkThanks.Badges$Recipient</groupingColumn>
            <header>Badges received this month</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Work_com_Recognition_Reports/Badges_received_this_month_for_Managers</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>true</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top Users Recognized this month</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>LineCumulative</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>What is the trend in using Thanks?</header>
            <legendPosition>Bottom</legendPosition>
            <report>Work_com_Recognition_Reports/Manager_Badges_Given_By_Date_Given</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelDescending</sortBy>
            <title>Total Thanks Given</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Level 2 Awards Given</header>
            <legendPosition>Bottom</legendPosition>
            <report>Work_com_Recognition_Reports/Manager_Level_2_Awards</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>true</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>WorkThanks.Badges$Recipient</groupingColumn>
            <header>Badges received this quarter</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Work_com_Recognition_Reports/Badges_received_this_quarter_for_Mgmt</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>true</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top Users Recognized this quarter</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Badges by number given</footer>
            <header>What badges are popular?</header>
            <legendPosition>Bottom</legendPosition>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Work_com_Recognition_Reports/Manager_Breakdown_of_Badges_Received</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Badges by Number Given</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>Nominations</header>
            <legendPosition>Bottom</legendPosition>
            <report>Work_com_Recognition_Reports/Manager_Nominations</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>User Nominations</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>elsa.boulter@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>GSA Assurance Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
