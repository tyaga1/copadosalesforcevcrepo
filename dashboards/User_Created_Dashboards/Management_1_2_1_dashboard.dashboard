<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_N_DAYS:7</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_N_DAYS:14</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_MONTH</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_FISCAL_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_FISCAL_QUARTER</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>THIS_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>NEXT_FISCAL_YEAR</values>
        </dashboardFilterOptions>
        <name>Close Date</name>
    </dashboardFilters>
    <dashboardType>MyTeamUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Open opportunities</metricLabel>
            <report>Dashboard_reports/X121MD01_Pipeline</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Pipeline TCV</metricLabel>
            <report>Dashboard_reports/X121MD01_Pipeline</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Current_FY_revenue_impact__c.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the a salesperson&apos;s number of pipeline opportunities, the TCV of their pipeline opportunities and the revenue impact in the current FY of these opportunities.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Revenue impact in current FY</metricLabel>
            <report>Dashboard_reports/X121MD01_Pipeline</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>true</drillToDetailEnabled>
            <footer>This component shows a salesperson&apos;s top 5 open opportunities, ranked by TCV.</footer>
            <header>Top 5 open opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Dashboard_reports/X121MD02_Top_opps</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>ranked by TCV</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <header>Overdue opportunities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of overdue opportunities</metricLabel>
            <report>Dashboard_reports/X121MD04_Overdue_opportunities</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows a salesperson&apos;s number of overdue opportunities and the TCV of the overdue opportunities.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>TCV of overdue opportunities</metricLabel>
            <report>Dashboard_reports/X121MD04_Overdue_opportunities</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Misaligned scheduled revenue</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of opportunities to fix</metricLabel>
            <report>Dashboard_reports/X121MD07_Misaligned_schedule_revenue</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>REVENUE_AMOUNT.CONVERT</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows a salesperson&apos;s number of opportunities with misaligned scheduled revenue and the amount of misaligned schedule revenue.</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Amount of misaligned schedule revenue</metricLabel>
            <report>Dashboard_reports/X121MD07_Misaligned_schedule_revenue</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>CLOSE_DATE</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Average</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Stage_Number__c</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>REVENUE_AMOUNT.CONVERT</column>
                <showTotal>false</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>This component shows a salesperson&apos;s top 5 opportunities that have misaligned scheduled revenue. Opportunities are ranked by misaligned scheduled revenue.</footer>
            <header>Top 5 opps with misaligned scheduled revenue</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>5</maxValuesDisplayed>
            <report>Dashboard_reports/X121MD09_Opps_with_misaligned_SR</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 5 ranked by misaligned schedule reve</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Opportunity.Opportunity_DQ_Score__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>CLOSE_DATE</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <footer>This component shows a salesperson&apos;s average Opportunity DQ score % for their pipeline opportunities.</footer>
            <header>Opportunity DQ % score of pipeline opportunities</header>
            <indicatorBreakpoint1>65.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>85.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average Opportunity DQ % score</metricLabel>
            <report>Dashboard_reports/X121MD03_Opportunity_DQ_score</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account$LastActivityDate</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <header>Contact DQ % score</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of contacts</metricLabel>
            <report>Dashboard_reports/X121MD05_Average_Contact_DQ_score</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Average</aggregate>
                <column>Account.Contacts$Contact_DQ_Score__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Account$LastActivityDate</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the number of contacts a salesperson&apos;s on the contact team for and the average contact DQ score % for a salesperson.</footer>
            <indicatorBreakpoint1>65.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>85.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Average contact DQ score %</metricLabel>
            <report>Dashboard_reports/X121MD05_Average_Contact_DQ_score</report>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Activity.Completed_Date__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the number of activities completed by a salesperson in the last 30 days.</footer>
            <header>Completed activities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of activities completed in last 30 days</metricLabel>
            <report>Dashboard_reports/X121MD06_Completed_activities</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Activity.Competitor_Contract_Renewal_Date__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the number of activities completed by a salesperson in the last 30 days, split by completed date and activity type.</footer>
            <groupingColumn>DUE_DATE</groupingColumn>
            <groupingColumn>TASK_TYPE</groupingColumn>
            <header>Completed activities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/X121MD06_Completed_activities</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Completed in last 30 days, split by type</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <dashboardFilterColumns>
                <column>Activity.Competitor_Contract_Renewal_Date__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>This component shows the number of open activities for a salesperson due to be completed in the next 30 days.</footer>
            <header>Open activities</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Number of open activities planned for the next 30 days</metricLabel>
            <report>Dashboard_reports/X121MD07_Open_activities_N30D</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked</componentType>
            <dashboardFilterColumns>
                <column>Activity.Competitor_Contract_Renewal_Date__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows a salesperson&apos;s number of open activities due to be completed in the next 30 days, split by due date and activity type.</footer>
            <groupingColumn>DUE_DATE</groupingColumn>
            <groupingColumn>TASK_TYPE</groupingColumn>
            <header>Open activities</header>
            <legendPosition>Bottom</legendPosition>
            <report>Dashboard_reports/X121MD07_Open_activities_N30D</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Planned for next 30 days, split by type</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>alberto.gayo@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>1-2-1 Direct report (sales) dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
