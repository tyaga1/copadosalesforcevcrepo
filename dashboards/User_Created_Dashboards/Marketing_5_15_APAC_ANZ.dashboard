<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>3.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Tasks assigned in FY1718</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#54C254</indicatorLowColor>
            <indicatorMiddleColor>#54C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/Marketing_EDQ_Total_Tasks</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>EDQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ASSIGNED</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Number tasks &gt;5 days</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/Marketing_EDQ_Tasks_5_days</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>EDQ</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Marketing pipeline DQ</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>90 day rolling</metricLabel>
            <report>User_created_reports/DQ_90_rolling_p_l_Marketing</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Closed won deals associated with Marketing Campaign</footer>
            <header>Revenue by Marketing - FY1718</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/ANZ_Revenue_Generated_FY1718_DQ</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>EDQ Revenue</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>3.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Tasks assigned in FY1718</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#54C254</indicatorLowColor>
            <indicatorMiddleColor>#54C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/Marketing_EMS_total_tasks_created</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Targeting/DAS</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ASSIGNED</column>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>RowCount</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Number tasks &gt;5 days</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>User_created_reports/Marketing_Targeting_Tasks_5_days</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Targeting</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>Close date used as indicator of pipeline</footer>
            <header>Marketing pipeline Targeting</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>90 day rolling</metricLabel>
            <report>APAC_ANZ_Marketing/Updated_Targeting_NB_rolling_90_day_P_L1</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Closed won deals associated with Marketing Campaign</footer>
            <header>Revenue by Marketing - FY1718</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/ANZ_Mkt_Revenue_Targeting_FY1718</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>Targeting Revenue</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>ACCOUNT_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>AMOUNT.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Marketing linked</footer>
            <header>DQT Top Deals in pipeline</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/A_NZ_Top_deals_in_pipeline</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Deals &gt;$50k</title>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Medium</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <gaugeMin>0.0</gaugeMin>
            <header>Tasks assigned in FY1718</header>
            <indicatorBreakpoint1>90.0</indicatorBreakpoint1>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C2C254</indicatorLowColor>
            <indicatorMiddleColor>#54C254</indicatorMiddleColor>
            <report>User_created_reports/ANZ_Marketing_DQT_Total_tasks</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Total DQT</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>CSDA Tasks&gt; 5 Days</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>User_created_reports/Marketing_CSDA_Tasks_5_days</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>CSDA</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Closed won deals associated with Marketing Campaign</footer>
            <header>DQT Total FY Revenue</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/A_NZ_Mkt_Revenue_DQT</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowLabelAscending</sortBy>
            <title>DQT Revenue (excl FB below)</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Marketing pipeline CSDA/ID</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>90 day rolling</metricLabel>
            <report>APAC_ANZ_Marketing/CSDA_NB_rolling_90_day_P_L</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardTableColumn>
                <column>OPP.NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity.Annual_Contract_Value__c.CONVERT</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <footer>Closed won deals influenced by min 2x Marketing campaigns</footer>
            <header>CSDA Total FY Revenue</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/A_NZ_Mktg_CSDA_5_15_Cld</report>
            <showPicturesOnTables>false</showPicturesOnTables>
            <title>CSDA Revenue FY</title>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Green = Marketing share of FB</footer>
            <gaugeMin>0.0</gaugeMin>
            <header>Q1 + July + Aug  Data</header>
            <indicatorBreakpoint1>571821.0</indicatorBreakpoint1>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#54C254</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>APAC_ANZ_Marketing/A_NZ_Marketing_FB_Partner_Categories</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Facebook Partner Categories</title>
        </components>
    </rightSection>
    <runningUser>melissa.croom@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>Marketing 5.15 APAC ANZ</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
