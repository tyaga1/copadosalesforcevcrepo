<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New,New Business,New From Existing,New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From New</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>New From Existing</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Existing Business,Renewal</values>
        </dashboardFilterOptions>
        <name>Opportunity type</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Automotive</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>BIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>CIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Decision Analytics</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Experian Healthcare</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Experian Interactive</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Marketing Services Group</values>
        </dashboardFilterOptions>
        <name>CSDA Product Org</name>
    </dashboardFilters>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>BIS,BIS - Alternate Channels,BIS - Inside,BIS - Strategic,BIS - Vertical</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>BIS - Alternate Channels</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>BIS - Inside</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>BIS - Strategic</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>BIS - Vertical</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>BIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS,CIS - APR,CIS - Growth,CIS - Preferred,CIS - RentBureau,CIS - Strategic,CIS - Vertical</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS - APR</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS - Growth</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS - Preferred</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS - RentBureau</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS - Strategic</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS - Vertical</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>CIS</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>contains</operator>
            <values>US Channel/Telesales</values>
        </dashboardFilterOptions>
        <name>Primary owner&apos;s Sales Team/Channel</name>
    </dashboardFilters>
    <dashboardType>MyTeamUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity.CSDA_Annual_Contract_Value__c.CONVERT</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>This component shows the pipeline, split by sales stage. Revenue is measured in CSDA Annual Contract Value. PLEASE NOTE THAT THIS COMPONENT IS UNABLE TO BE FILTERED BY THE &apos;CSDA PRODUCT ORG&apos; DASHBOARD FILTER.</footer>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Pipeline - measured by CSDA Annual Contract Value</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_dashboard_reports/R_P_L_DB_TCV_SS</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current view of pipeline by sales stage</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Sales_Price__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.CSDA_Product_Org__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline for each CSDA Product org. Revenue is measured by CSDA Annual Contract Value for the product. Opportunity count is the number of open opportunities.</footer>
            <groupingColumn>Product2.CSDA_Product_Org__c</groupingColumn>
            <header>Product org</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_dashboard_reports/R_P_L_DB_Prod_org1</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current view of pipeline</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Sales_Price__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.CSDA_Product_Org__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline for each CSDA Product org, split by sales stage. Revenue is measured by CSDA Annual Contract Value for the product. Opportunity count is the number of open opportunities.</footer>
            <groupingColumn>Product2.CSDA_Product_Org__c</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Product org</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_dashboard_reports/R_P_L_DB_Prod_org1</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current view of pipeline by sales stage</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>TAMOUNT.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the historical trended pipeline as of the 1st  of each month. Revenue is measured in Total Contract Value. PLEASE NOTE THAT THIS COMPONENT IS UNABLE TO BE FILTERED BY THE &apos;CSDA PRODUCT ORG&apos; DASHBOARD FILTER.</footer>
            <groupingColumn>INTERVAL_DATE</groupingColumn>
            <header>Trended pipeline - measured by Total Contract Value</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_pipeline_and_trended_reports/R_11b_P_L_trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Monthly snapshot</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Sales_Price__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.CSDA_Product_Org__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline for each CSDA Product suite. Revenue is measured by CSDA Annual Contract Value for the product. Opportunity count is the number of open opportunities.</footer>
            <groupingColumn>Product2.CSDA_Product_Suite__c</groupingColumn>
            <header>Product suite</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_dashboard_reports/R_P_L_DB_Prod_suite</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current view of pipeline</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Sales_Price__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.CSDA_Product_Org__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline for each CSDA Product suite, split by sales stage. Revenue is measured by CSDA Annual Contract Value for the product. Opportunity count is the number of open opportunities.</footer>
            <groupingColumn>Product2.CSDA_Product_Suite__c</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Product suite</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_dashboard_reports/R_P_L_DB_Prod_suite</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current view of pipeline by sales stage</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>TAMOUNT.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <axisBinding>y2</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the historical trended pipeline, split by Sales stage, as of the 1st  of each month. Revenue is measured in Total Contract Value. PLEASE NOTE THAT THIS COMPONENT IS UNABLE TO BE FILTERED BY THE &apos;CSDA PRODUCT ORG&apos; DASHBOARD FILTER.</footer>
            <groupingColumn>INTERVAL_DATE</groupingColumn>
            <groupingColumn>TSTAGE</groupingColumn>
            <header>Trended pipeline by sales stage - measured by Total Contract Value</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_pipeline_and_trended_reports/R_11b_P_L_trend_Opp_type_SS</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Monthly snapshot</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Sales_Price__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLine</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.CSDA_Product_Org__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline for each CSDA Product group. Revenue is measured by CSDA Annual Contract Value for the product. Opportunity count is the number of open opportunities.</footer>
            <groupingColumn>Product2.CSDA_Product_Group__c</groupingColumn>
            <header>Product group</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_dashboard_reports/R_P_L_DB_Prod_group</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current view of pipeline</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>OpportunityLineItem.Annual_Sales_Price__c.CONVERT</column>
            </chartSummary>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y2</axisBinding>
                <column>Opportunity.Opportunity_Count__c</column>
            </chartSummary>
            <componentType>ColumnLineStacked</componentType>
            <dashboardFilterColumns>
                <column>TYPE</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>OpportunityLineItem.CSDA_Product_Org__c</column>
            </dashboardFilterColumns>
            <dashboardFilterColumns>
                <column>User.Sales_Team__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>true</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>This component shows the pipeline for each CSDA Product group, split by sales stage. Revenue is measured by CSDA Annual Contract Value for the product. Opportunity count is the number of open opportunities.</footer>
            <groupingColumn>Product2.CSDA_Product_Group__c</groupingColumn>
            <groupingColumn>STAGE_NAME</groupingColumn>
            <header>Product group</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_CSDA_dashboard_reports/R_P_L_DB_Prod_group</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Current view of pipeline by sales stage</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>gareth.lee@experian.global</runningUser>
    <textColor>#000000</textColor>
    <title>NA CSDA pipeline dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
