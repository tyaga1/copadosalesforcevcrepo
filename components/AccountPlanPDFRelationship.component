<!--
/**=====================================================================
 * Name: AccountPlanPDFRelationship
 * Description: 
 * Created Date: Jan. 25th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Jan. 25th, 2017       James Wills            Created.
 * =====================================================================*/
-->
<apex:component controller="AccountPlanContactRelationshipController">

  <apex:attribute name="accountPlan1" description="" type="Account_Plan__c" required="true" assignTo="{!accountPlanobj}"/>

     <!-- Account Relationship Status Section -->    
     <table class="table-bordered" style="width:94%;border-spacing: 0 !important;"> 
        <tr><td colspan="7" style="padding-left:0px !important;"> <div class="title"> {!$Label.ACCOUNTPLANNING_PDF_Account_Relationship_Status}</div></td></tr>
        <table style="position:relative; padding: 10px 5px 5px 15px;">
          <tr><td style="border:0px;">
             <div class="caption" style = "background-color: #6aa84f;width: 15px;height: 15px;"></div></td><td style="border:0px;"><span style = "color: #6aa84f">{!$Label.ACCOUNTPLANNING_ACCPLANCONT_POSITIVE}</span></td></tr><tr><td style="border:0px;">
             <div class="caption" style = "background-color: #cc0000;width: 15px;height: 15px;"></div></td><td style="border:0px;"><span style = "color: #cc0000">{!$Label.ACCOUNTPLANNING_ACCPLANCONT_NEGATIVE}</span></td></tr><tr><td style="border:0px;">
             <div class="caption" style = "background-color: #f1c232;width: 15px;height: 15px;"></div></td><td style="border:0px;"><span style = "color: #f1c232">{!$Label.ACCOUNTPLANNING_ACCPLANCONT_NEUTRAL}</span></td></tr><tr><td style="border:0px;">
             <div class="caption" style = "background-color: #999999;width: 15px;height: 15px;"></div></td><td style="border:0px;"><span  style = "color: #999999" >{!$Label.ACCOUNTPLANNING_ACCPLANCONT_NO_RELATIONSHIP}</span></td></tr><tr><td style="border:0px;">
             <div class="caption" style = "background-color: #000000;width: 15px;height: 15px;"></div></td><td style="border:0px;">{!$Label.ACCOUNTPLANNING_ACCPLANCONT_UNSPECIFIED}</td>
           </tr>
         </table>
         <div class="treeNode" style="padding: 20px 5px 5px 40px;font-size:15px;">
                <apex:repeat value="{!ObjectStructure}" var="pos" >
                <apex:repeat value="{!pos.levelFlag}" var="flag" first="0">
                    <apex:image url="/img/tree/empty.gif" height="16" width="20" rendered="{!IF(flag,false,true)}"/>
                    <apex:image url="/s.gif" alt="" width="3" height="16" rendered="{!IF(flag,true,false)}"/>
                    <apex:image url="/img/tree/chain.gif" height="16" width="20" rendered="{!IF(flag,true,false)}"/>
                </apex:repeat>
                <span height="16" v="top">
                <apex:outputText rendered="{!IF(pos.nodeType=='start',true,false)}">
                    <apex:image id="tree_start" url="/img/tree/minusStart.gif" height="16" width="20" title="Click to expand/collapse nested items." onClick="TreeNodeElement.prototype.toggle(this,'{!pos.nodeId}')"/>
                </apex:outputText>
                <apex:outputText rendered="{!IF(OR(pos.nodeType=='parent',pos.nodeType=='parent_end'),true,false)}">
                    <apex:image id="Tree_parent" url="/img/tree/minus.gif" rendered="{!IF(pos.nodeType=='parent',true,false)}" height="16" width="20" title="Click to expand/collapse nested items." onClick="TreeNodeElement.prototype.toggle(this,'{!pos.nodeId}')"/>
                    <apex:image id="Tree_parent_end" url="/img/tree/minusEnd.gif" rendered="{!IF(pos.nodeType=='parent_end',true,false)}" height="16" width="20" title="Click to expand/collapse nested items." onClick="TreeNodeElement.prototype.toggle(this,'{!pos.nodeId}')"/>                
                </apex:outputText>
                <apex:outputText rendered="{!IF(OR(pos.nodeType=='child',pos.nodeType=='child_end'),true,false)}">
                    <apex:image id="Tree_child" url="/img/tree/node.gif" rendered="{!IF(pos.nodeType=='child',true,false)}" height="16" width="20" title="Click to expand/collapse nested items." onClick="TreeNodeElement.prototype.toggle(this,'{!pos.nodeId}')"/>
                    <apex:image id="Tree_child_current" url="/img/tree/nodeEnd.gif" rendered="{!IF(pos.nodeType=='child_end',true,false)}" height="16" width="20" title="Click to expand/collapse nested items." onClick="TreeNodeElement.prototype.toggle(this,'{!pos.nodeId}')"/>
                </apex:outputText>
                <apex:outputText rendered="{!IF(pos.nodeType=='end',true,false)}">
                    <apex:image id="Tree_end" url="/img/tree/nodeEnd.gif" height="16" width="20"/>&nbsp;
                    <apex:image id="Icon_end_current" url="/img/icon/star16.png" width="16" height="16" rendered="{!IF(pos.currentNode,true,false)}"/>
                </apex:outputText>
            <!-- Change Below -->
                <apex:outputText style="font-weight: bold; {!IF(pos.accPlanCon.Experian_Relationship__c == 'Positive','color: #6aa84f','')} {!IF(pos.accPlanCon.Experian_Relationship__c == 'Negative','color: #cc0000','')} {!IF(pos.accPlanCon.Experian_Relationship__c == 'Neutral','color: #f1c232','')} {!IF(pos.accPlanCon.Experian_Relationship__c == 'No Relationship','color: #999999','')}" value="{!pos.accPlanCon.Contact_Name__c}" styleClass="columnHeadActiveBlack"/><!--
             --><apex:outputText style="{!IF(pos.currentNode,'font-weight: bold;','')}"  value=", {!pos.accPlanCon.Contact_Job_Title__c}" rendered="{!IF(pos.accPlanCon.Contact_Job_Title__c != '', true, false)}"/>
                <apex:outputText style="{!IF(pos.currentNode,'font-weight: bold;','')}"  value=", {!pos.accPlanCon.Primary_Contact_Role__c}" rendered="{!IF(pos.accPlanCon.Primary_Contact_Role__c != '', true, false)}"/>
               <!-- Stop -->
                </span>
                <div> </div>
                <apex:outputText rendered="{!IF(OR(pos.nodeType=='child_end',pos.nodeType=='child'),false,true)}">
                    <div id='{!pos.nodeId}'/>
                </apex:outputText>
                <apex:outputText rendered="{!IF(OR(pos.nodeType=='child_end',pos.nodeType=='child'),true,false)}">
                    <div id='{!pos.nodeId}'><apex:image url="/s.gif" alt="" width="1" height="1"/></div>
                </apex:outputText>
                <apex:repeat value="{!pos.closeFlag}" var="close">
                </apex:repeat>            
          </apex:repeat>
          <br/><br/><br/>
       </div>
    </table>
                
      <br />
      <br />
   <div class="page-break" />
  
</apex:component>