<!--
/**=====================================================================
 * Experian
 * Name: ITCA_My_Journey_Map
 * Description: 
 * Created Date: 17th August 2017
 * Created By: James Wills
 *
 * Date Modified      Modified By           Description of the update
 * 29/08/2017         Alexander McCall      ITCA Issue I1440
 * 30/08/2017         Alexander McCall      ITCA Issue I1449 & I1454 
 * 05/09/2017         James Wills           ITCA Issue 1456: Adding functionality to Compare Skill from the Skill Sets list.
 * 07/09/2017         Alexander McCall      ITCA Issue I1478
 * =====================================================================*/
 -->
<apex:component controller="ITCA_My_Journey_Map_Controller" extensions="ITCANavigationBannerController">
    
 
    <apex:attribute type="ITCABannerInfoClass" name="currentActiveTab2" required="true" description="The Data." assignTo="{!bannerInfoLocal}"/>
    
    <title> ITCA </title>
    
    <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />

    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/bootstrap.exp.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/main.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/exp.rebrand.proto.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/itca-styles.css')}"/>
    
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"/>
        
        
   
    <body class="home">

        <div class="container journey-map">
        
        <!--apex:outputText value="Value from Component is: {!bannerInfoLocal.currentActiveTab}"/--><br/>        
        <a name="my-summary"></a>
            <div class="row">
                <div class="col-sm-12 head">
                  <h2>{!employeeNameForProfile} Profile</h2>                  
                </div>                
                
                <div class="col-sm-12 body">
                    <div class="row">
                        <div class="col-sm-12 description">
                            <p>
                                Now that you have created your skill profile, you can begin to explore your opportunities
                                for growth and development within your current Career
                                Area or in new ones by creating your own journey
                                maps.
                            </p>
                        </div>

                        <!--Dashboard Controls-->
                        <div class="col-sm-4 dashboard-controls">
                            <div class="row is-flex">
                                <apex:form >
                                
                                <div class="col-sm-12">
                                    <div class="single-bucket">
                                        <a href="ITCA_Home_Page#my-summary"> <!-- AM Edited Link I1478 -->
                                            <div class="bucket-img">
                                                <i class="icon i-sm i-primary-purple i-person"></i>
                                            </div>
                                            <div class="bucket-text text-center">
                                                <h6><apex:commandLink action="{!mySkillsSummaryAction}"  rerender="employeeDisplayPanel">My Profile</apex:commandLink></h6>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="single-bucket">
                                        <a href="ITCA_Home_Page#create-journey-map">
                                            <div class="bucket-img">
                                                <i class="icon i-sm i-primary-purple i-pencil"></i>
                                            </div>
                                            <div class="bucket-text text-center">
                                                <h6><apex:commandLink action="{!myJourneyMapAction}"  rerender="employeeDisplayPanel">Create Journey Map</apex:commandLink></h6>
                                                
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="single-bucket">
                                        <a href="ITCA_Home_Page#journey-map">
                                            <div class="bucket-img">
                                                <i class="icon i-sm i-primary-purple i-magnifying-glass"></i>
                                            </div>
                                            <div class="bucket-text text-center">
                                                <h6><apex:commandLink action="{!compareCareerAreaAction}"  rerender="employeeDisplayPanel">Compare CareerArea</apex:commandLink></h6>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="single-bucket">
                                        <a href="ITCA_Home_Page#match-skillsets">
                                            <div class="bucket-img">
                                                <i class="icon i-sm i-primary-purple i-client"></i>
                                            </div>
                                            <div class="bucket-text text-center">
                                                <h6>
                                                  <apex:commandLink action="{!matchSkillSetsAction}"  rerender="employeeDisplayPanel">Match Skill Set</apex:commandLink>
                                                </h6>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="single-bucket">
                                        <a href="ITCA_Home_Page#compare-skillsets">
                                            <div class="bucket-img">
                                                <i class="icon i-sm i-primary-purple i-employee-check"></i>
                                            </div>
                                            <div class="bucket-text text-center">
                                                <h6><apex:commandLink action="{!compareSkillSetAction}"  rerender="employeeDisplayPanel">Compare Skill Set</apex:commandLink></h6>
                                            </div>
                                        </a>
                            
                                    </div>
                                </div>
                                </apex:form>
                            </div>
                        </div>
                         <!--../Dashboard Controls-->
                         
                  <!--Results Display Section-->
                  <apex:outputPanel id="employeeDisplayPanel">
                    <!-- Compare Career Area-->
                    <apex:outputPanel id="myComapreCareerAreaPanel" rendered="{!bannerInfoLocal.currentActiveTab=='isCompareCareerArea'}">                                
                      <div class="col-sm-8">                            
                        <p>
                          Compare your skills against Skills from another Career Area. Select a Career Area from the menu below :
                        </p>
                      </div>
                      <apex:form > 
                        <div class="col-sm-8"> 
                          <div class="row">
                            <div class="col-sm-12 skill-set-selector">
                              <apex:selectList id="level2_skillsSetList" label="Skill Set" value="{!level1_selectedCareerArea}" size="1" style="width:300px" multiselect="false"  styleClass="form-control">
                                <apex:selectOptions value="{!level1_CareerAreaOptions}"/>
                                  <apex:actionSupport event="onchange" rerender="mySkillProgessSummaryPanel4" action="{!compareCareerArea}">
                                    <apex:param name="{!level1_selectedCareerArea}" value="{!level1_selectedCareerArea}" assignTo="{!level1_selectedCareerArea}" />              
                                  </apex:actionSupport>
                                </apex:selectList>      
                              </div>
                            </div>
                          </div>    
                        </apex:form>
                      </apex:outputPanel>
                      <!-- /Compare Career Area-->
                        
                      <!-- Compare SkillSet-->
                      <apex:outputPanel id="myComapreSkillSetPanel" rendered="{!bannerInfoLocal.currentActiveTab=='isCompareSKillSet'}">                                
                        <div class="col-sm-8">                            
                          <p>
                            Compare your skills against another Skill Set. Select a Skill Set from the menu below :
                          </p>
                        </div>
                        <apex:form > 
                          <div class="col-sm-8">                           
                            <div class="row">
                              <div class="col-sm-12 skill-set-selector">
                                <apex:selectList id="level2_skillsSetList" label="Skill Set" value="{!level2_selectedSkillSet}" size="1" style="width:300px" multiselect="false"  styleClass="form-control">
                                  <apex:selectOptions value="{!level2_skillSetOptions}"/>
                                  <apex:actionSupport event="onchange" rerender="mySkillProgessSummaryPanel3" action="{!compareSkillSet}">
                                    <apex:param name="{!level2_selectedSkillSet}" value="{!level2_selectedSkillSet}" assignTo="{!level2_selectedSkillSet}" />                                                
                                  </apex:actionSupport>
                                </apex:selectList>   
                              </div>
                            </div>
                          </div>
                        </apex:form>                            
                      </apex:outputPanel>
                      <!-- /Compare SKillSet-->
                      
                      
                      <apex:form >
                        <apex:actionFunction name="setSelectedSkill_AF" reRender="mySkillProgessSummaryPanel,levelCompSection">
                          <apex:param name="setSkill" assignTo="{!selectedSkillID}" value=""/>
                        </apex:actionFunction>    
                      </apex:form> 
                      
                      <!-- Skill progress-bar --->  
                      <apex:outputPanel id="mySkillProgessSummaryPanel" rendered="{!bannerInfoLocal.currentActiveTab=='isSummary'}">
                        <div class="col-sm-8 dashboard-graph">
                          <div class="table-responsive">
                            <table class="table">
                              <apex:repeat value="{!bannerInfoLocal.employeeSkillDisplayList}" var="empSkill">
                                <tr>
                                  <td class="col-sm-4 skill">
                                    <a href="#" onclick="setSelectedSkill('{!empSkill.skillId}');" data-toggle="modal" data-target="#level-comparison">
                                      <apex:outputText value="{!empSkill.skillName}"/>                                          
                                    </a>
                                  </td>
                                  <td class="col-sm-8">
                                    <div class="progress-label">{!empSkill.curentSkillValue }/{!empSkill.maxSkillValue}</div>
                                    <div class="progress">
                                      <div class="progress-bar sfia-skills" role="progressbar" aria-valuenow="{!empSkill.curentSkillValue }" aria-valuemin="0" aria-valuemax="{!empSkill.maxSkillValue}" style="width:{!empSkill.skillAccPercentage}%"></div>
                                    </div>
                                  </td>
                                </tr>
                              </apex:repeat>
                            </table>
                            <!-- AM Commented Out I1454 <h3><apex:outputText value="Current Overall Skill Level: {!userProfileCurrentLevel}" rendered="{!bannerInfoLocal.currentActiveTab=='isSummary'}"/></h3> -->
                          </div>
                        </div>                            
                      </apex:outputPanel>                        
                      <!-- /Skill progress-bar --->  
                        
                      <!-- Skill progress-bar --->  
                      <apex:outputPanel id="mySkillProgessSummaryPanel2" rendered="{!bannerInfoLocal.currentActiveTab=='isMatchSKills'}">              
                        <div class="col-sm-8 dashboard-graph">
                          <apex:outputText >
                            <p>Takes the top 5 Skill Sets that your current skill is associated with.</p>
                          </apex:outputText>
                          
                          <apex:form >                          
                            <apex:actionFunction name="compareSkillSetAction_AF" action="{!compareSkillSetFromSkills}" reRender="My_Journey_Map">              
                              <apex:param name="level2_selectedSkillSet" assignTo="{!level2_selectedSkillSet}" value=""/>              
                            </apex:actionFunction>
                          </apex:form>
                          
                          <div class="table-responsive">
                            <table class="table">
                              <apex:repeat value="{!bannerInfoLocal.employeeSkillDisplay_SkillSets_List}" var="empSkill">
                                <tr>
                                  <td class="col-sm-4 skill">
                                    <li><a href="../apex/ITCA_Home_Page#my-summary" onClick="compareSkillSetAction_JS('{!empSkill.skillId}');">{!empSkill.skillName}</a></li>
                                    <!--a href="../apex/ITCA_Home_Page#my-summary" onClick="compareCareerAreaFromSkills_JS('{!empSkill.skillName}');">
                                      <apex:outputText value="{!empSkill.skillName}"/>
                                    </a-->
                                  </td>
                                  <td class="col-sm-8">
                                    <div class="progress-label">{!empSkill.curentSkillValue }/{!empSkill.maxSkillValue}</div>
                                    <div class="progress">
                                      <div class="progress-bar sfia-skills" role="progressbar" aria-valuenow="{!empSkill.curentSkillValue }" aria-valuemin="0" aria-valuemax="{!empSkill.maxSkillValue}" style="width:{!empSkill.skillAccPercentage}%"></div>
                                    </div>
                                  </td>
                                </tr>
                              </apex:repeat>
                            </table>
                          </div>
                        </div>                            
                      </apex:outputPanel>            
                      
                      <!-- Skill progress-bar --->  
                      <apex:outputPanel id="mySkillProgessSummaryPanel3" rendered="{!bannerInfoLocal.currentActiveTab=='isCompareSKillSet'}">              
                        <div class="col-sm-8 dashboard-graph">
                          <div class="table-responsive">
                            <table class="table">
                              <apex:repeat value="{!bannerInfoLocal.employeeSkillDisplay_SkillComp_List}" var="empSkill">
                                <tr>
                                  <td class="col-sm-4 skill">
                                    <a href="#" onclick="setSelectedSkill('{!empSkill.skillId}');" data-toggle="modal" data-target="#level-comparison">
                                      <apex:outputText value="{!empSkill.skillName}"/>                                          
                                    </a>
                                  </td>
                                  <td class="col-sm-8">
                                    <div class="progress-label">{!empSkill.curentSkillValue }/{!empSkill.maxSkillValue}</div>
                                    <div class="progress">
                                      <div class="progress-bar sfia-skills" role="progressbar" aria-valuenow="{!empSkill.curentSkillValue }" aria-valuemin="0" aria-valuemax="{!empSkill.maxSkillValue}" style="width:{!empSkill.skillAccPercentage}%"></div>
                                    </div>
                                  </td>
                                </tr>
                              </apex:repeat>
                            </table>
                          </div>
                        </div>                            
                      </apex:outputPanel>            
                      
                      <!-- Skill progress-bar --->  
                      <apex:outputPanel id="mySkillProgessSummaryPanel4" rendered="{!bannerInfoLocal.currentActiveTab=='isCompareCareerArea'}">              
                        <div class="col-sm-8 dashboard-graph">
                          <div class="table-responsive">
                            <table class="table">
                              <apex:repeat value="{!bannerInfoLocal.employeeSkillDisplay_CarComp_List}" var="empSkill">
                                <tr>
                                  <td class="col-sm-4 skill">
                                    <a href="#" onclick="setSelectedSkill('{!empSkill.skillId}');" data-toggle="modal" data-target="#level-comparison">
                                      <apex:outputText value="{!empSkill.skillName}"/>                                          
                                    </a>
                                  </td>
                                  <td class="col-sm-8">
                                    <div class="progress-label">{!empSkill.curentSkillValue }/{!empSkill.maxSkillValue}</div>
                                    <div class="progress">
                                      <div class="progress-bar sfia-skills" role="progressbar" aria-valuenow="{!empSkill.curentSkillValue }" aria-valuemin="0" aria-valuemax="{!empSkill.maxSkillValue}" style="width:{!empSkill.skillAccPercentage}%"></div>
                                    </div>
                                  </td>
                                </tr>
                              </apex:repeat>
                            </table>
                          </div>
                        </div>                            
                      </apex:outputPanel>            
                                                
                      <apex:outputPanel id="myJourneyMapPanel" rendered="{!bannerInfoLocal.currentActiveTab=='isMyJourneyMap'}">                                
                        <div class="col-sm-8">                            
                          <p>
                           <!-- AM Commented Out Your Journey Map module is In the process of Beautification!!!  -->
                           Your personal Journey Map will be provided in a later release. 
                          </p>
                         </div>
                       </apex:outputPanel>
                                          
                      </apex:outputPanel>

                      <!--../Summary Dashboard Controls-->
                    </div>
                </div>
            </div>
        </div>
        <!--../Journey Map-->

       

        <!--Skill Level Modal-->
        <div class="modal fade in" id="level-comparison" tabindex="-1" role="dialog" style="display: none;" aria-hidden="false">
            <div class="modal-dialog level-comparison-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h3 class="modal-title" id="level-comparison-label">
                            Skill Overview
                        </h3>
                    </div>
                    <div class="modal-body">
                      <div class="description">
                        <apex:outputPanel id="levelCompSection">                            
                          <div>
                            The following results are based on your self-evaluation.
                          </div>
                          <apex:repeat value="{!skillOverview_List}" var="skillOverview">
                            <div>
                              <span>Your Skill: </span>
                              <span class="skill-name">{!skillOverview.skillName}</span>
                            </div>
                            <div>
                              <span>Under Skill Set: </span>
                              <span class="skill-set-name">{!skillOverview.skillSet}</span>
                            </div><br/><br/>
                              
                            <!--td class="col-sm-6"> Current Level </td>
                            <td rowspan="2">
                              <div class="level">
                                <div class="fa fa-square-o box-number fa-3x"></div>
                                <div class="number">
                                  {!skillOverview.currentSkillValue}
                                </div>
                              </div>
                              <ul class="list-bullet">
                                <li>
                                  <apex:outputText value="{!skillOverview.currentLevel}"/>
                                </li>
                              </ul>
                            </td>
                                
                            <td class="col-sm-6"> Growth Opportunity </td>                                
                            <td rowspan="2">
                              <div class="level">
                                <div class="fa fa-square-o box-number fa-3x"></div>
                                <div class="number">
                                  {!skillOverview.nextSkillValue}
                                </div>
                              </div>
                              <ul class="list-bullet">
                                <li>
                                  <apex:outputText value="{!skillOverview.nextLevel}"/>
                                </li>
                              </ul>
                            </td-->
                                
                            <apex:repeat value="{!skillOverview.skillLevels_Map}" var="key">  
                              <td rowspan="2">
                                <div class="level">
                                  <div class="fa fa-square-o box-number fa-3x"></div>
                                  <div class="number">
                                    <apex:outputText value="{!key}" rendered="{!key!=skillOverview.currentSkillValue}"/>
                                    <apex:outputText value="{!key}" style="color:blue" rendered="{!key=skillOverview.currentSkillValue}"/>
                                  </div>
                                </div>
                                <ul class="list-bullet">
                                  <li>
                                    <apex:outputText value="{!skillOverview.skillLevels_Map[key]}" rendered="{!key!=skillOverview.currentSkillValue}"/>
                                    <apex:outputText value="{!skillOverview.skillLevels_Map[key]}" style="color:blue" rendered="{!key=skillOverview.currentSkillValue}"/>
                                  </li>
                                </ul><br/><!--Maybe remove this br once we update the description data type-->
                              </td>
                            </apex:repeat>
                          </apex:repeat>
                        </apex:outputPanel>  
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--../Skill Level Modal-->

        


        <!-- Scripts -->
        
        <!--script type="text/javascript" src="js/jquery.min.js"/>
        <script type="text/javascript" src="js/bootstrap.min.js"/>
        <script type="text/javascript" src="js/jquery.touchSwipe.min.js"/>
        <script type="text/javascript" src="js/jquery.bootstrap-responsive-tabs.min.js"/>
        <script type="text/javascript" src="js/exp.navbar.js"/>
        <script type="text/javascript" src="js/floatl.js"/>
        <script type="text/javascript" src="js/script.js"--/>
        
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"/>
        
        <!--.. /Scripts -->
        
    </body>
    
    <script>
    function setSelectedSkill(selectedSkillId){    
      setSelectedSkill_AF(selectedSkillId);

      return false;
    }
    
    function compareSkillSetAction_JS(skillName){
      compareSkillSetAction_AF(skillName);
      tabName = "apex__activeTabCookie=isCompareCareerArea; path=/";
      document.cookie = tabName;
      //return false;
    }
    
    
  
    
    </script>
    
</apex:component>