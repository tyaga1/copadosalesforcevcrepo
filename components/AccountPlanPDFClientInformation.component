<!--
/**=====================================================================
 * Name: AccountPlanPDFClientInformation
 * Description: 
 * Created Date: Jan. 25th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Jan. 25th, 2017       James Wills            Created.
 * Feb. 8th, 2017        James Wills            Case:02194059 Changed Custom labels to Field labels.
 * Feb. 23rd, 2017       James Wills            Case:02255727 - Update to allow display of rich-text format (for DO).
 * =====================================================================*/
-->
<apex:component controller="AccountPlanPDFComponentsController">

   <apex:attribute name="showClientsCoreBusiness1" description="" type="Boolean" assignTo="{!showClientsCoreBusiness}"/>
   <apex:attribute name="showCapexInOurDomain1" description="" type="Boolean" assignTo="{!showCapexInOurDomain}"/>
   <apex:attribute name="showOpexInOurDomain1" description="" type="Boolean" assignTo="{!showOpexInOurDomain}"/>
   <apex:attribute name="showClientsStrategy1" description="" type="Boolean" assignTo="{!showClientsStrategy}"/>  
   <apex:attribute name="showClientsObjectives1" description="" type="Boolean" assignTo="{!showClientsObjectives}"/>
   <apex:attribute name="showClientsChanges1" description="" type="Boolean" assignTo="{!showClientsChanges}"/>
   <apex:attribute name="showClientsCompetitors1" description="" type="Boolean" assignTo="{!showClientsCompetitors}"/>
   <apex:attribute name="showClientsPartners1" description="" type="Boolean" assignTo="{!showClientsPartners}"/>    
   <apex:attribute name="showExperianAnnualisedRevenue1" description="" type="Boolean" assignTo="{!showExperianAnnualisedRevenue}"/>


      <!-- Customer Information Section on PDF -->
      <table class="table-bordered" style="width:94%;border-spacing: 0 !important;margin-top:10px;">
      <tr><td colspan="6" style="padding-left:0px !important;"> <div class="title">{!$Label.ACCOUNTPLANNING_PDF_Client_Information}</div></td></tr>
         <apex:outputText rendered="{!showClientsCoreBusiness}">
           <tr>
             <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">{!$ObjectType.Account_Plan__c.fields.Core_Business__c.label}
             </td>
            <td colspan="5" style="padding-left : 10px;" ><apex:outputText escape="false" value="{!accountPlanObj.Core_Business__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showCapexInOurDomain}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Annual_CAPEX_in_Experian_Domain__c.label}
            </td>
            <td style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Annual_CAPEX_in_Experian_Domain__c}"/></td>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
              {!$ObjectType.Account_Plan__c.fields.Experian_CAPEX_share__c.label}
            </td>
            <td  style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Experian_CAPEX_share__c}"/></td>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Experian_Capex_Total__c.label}
            </td>
            <td style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Experian_Capex_Total__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showOpexInOurDomain}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Annual_OPEX_in_Experian_Domain__c.label}
            </td>
            <td style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Annual_OPEX_in_Experian_Domain__c}"/></td>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;"> 
               {!$ObjectType.Account_Plan__c.fields.Experian_OPEX_Share__c.label}
            </td>
            <td style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Experian_OPEX_Total__c}"/></td>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Experian_CAPEX_share__c.label}
            </td>
            <td  style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Experian_OPEX_Total__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showClientsStrategy}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Strategic_Direction__c.label}
            </td>
            <td colspan="5" style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Strategic_Direction__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showClientsObjectives}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Business_Objectives__c.label}
            </td>
            <td colspan="5" style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Business_Objectives__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showClientsChanges}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Major_Changes__c.label}
            </td>
            <td colspan="5" style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Major_Changes__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showClientsCompetitors}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Major_Competitors__c.label}
            </td>
            <td colspan="5" style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Major_Competitors__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showClientsPartners}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Major_Partners__c.label}
            </td>
            <td colspan="5" style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Major_Partners__c}"/></td>
           </tr>
         </apex:outputText>
         <apex:outputText rendered="{!showExperianAnnualisedRevenue}">
           <tr>
            <td width="25%" style="font-size: 13px; font-weight: bold; text-align:left;padding-left : 10px;">
               {!$ObjectType.Account_Plan__c.fields.Experian_Annualised_Revenue__c.label}
            </td>
            <td colspan="5" style = "padding-left : 10px;"><apex:outPutText escape="false" value="{!accountPlanObj.Experian_Annualised_Revenue__c}"/></td>
           </tr>
         </apex:outputText>
      </table>
      
      <br />
      <br />
</apex:component>