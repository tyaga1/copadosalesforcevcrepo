<!--
/**=====================================================================
* Date modified       Modified by                  Description
* Oct 11th, 2016      Yordan Terziev (Apt Sys)     EDQ v5 solution integrated.
* Nov 1st, 2016       Yordan Terziev (Apt Sys)     Fixed: Barcode shows in the address pop up however when you click save and go to the address the Bar code is not populated.
* Feb 6th, 2017       Sanket Vaidya                 Case#02149969 Address Type is auto-selected and warning for 'Secondary' address.
* June 1st, 2017      Ryan (Weijie) Hu             I1124 - Consumer contact can ignore address
=====================================================================*/
-->
<apex:component controller="ASS_QAS_Address_Lookup_Controller" id="qasAddrComp" allowDML="true" >
    <apex:attribute name="addressRec" type="Address__c" description="Address Record" assignTo="{!address}"/>
    <apex:attribute name="enableManualSelection" type="boolean" description="Allow user to search/select address" />
    <apex:attribute name="isAddressPopupOnload" type="boolean" description="Auto Popup on load" />
    <apex:attribute name="accountAddressRec" type="Account_Address__c" description="Account Address Record" />
    <apex:attribute name="contactAddressRec" type="Contact_Address__c" description="Contact Address Record" />
    <apex:attribute name="buttonId" type="String" description="Button Id to make vissible after loading." />
    <apex:attribute name="accountId" type="Id" assignTo="{!accId}" description="Account Id to populate list of corresponding Account Address records" />
    <apex:attribute name="bypassQAS" type="Boolean" description="To check if Address is to be required or not." />
    <apex:attribute name="isConsumerContact" type="Boolean" description="To check if this is for consumer contact or not" default="false"/>
    <apex:attribute name="ignoreAddress" type="Boolean" description="To check if skipping address for consumer contact" default="false"/>
    
    <!--Start: Experian Data Quality Scripts and Styles  -->
    <apex:includeScript value="/soap/ajax/33.0/connection.js"/>
    <apex:includeScript value="/soap/ajax/33.0/apex.js"/>
    <apex:includeScript value="{!$Resource.EDQConfigurations}" />
    <apex:includeScript value="{!$Resource.EDQGlobalSettings}" />
    <apex:includeScript value="{!$Resource.EDQSessionToken}" />
    <apex:includeScript value="https://sfv5.online.qas.com/SalesforceV5RapidSearch/Scripts/allEdqForSalesforce.js" />
    <link rel="stylesheet" type="text/css" href="https://sfv5.online.qas.com/SalesforceV5RapidSearch/Scripts/edqPopup.css" />
    <!--End: Experian Data Quality Scripts and Styles  -->
    
    <script>
    
    //Override the isEditMode function to always return true
    //EDQ change: innertext property replaced with textContent because of Mozila Firefox.
    function testCallBack(elementId, val){
        var inputTxtObj = document.getElementById(varOplConcatenatedAddress);
        if(elementId.indexOf("Address_1") != -1)
        {
            if(inputTxtObj.textContent!=null)
                inputTxtObj.textContent='';
            else
                inputTxtObj.textContent='';
        }
        
        var inputTxtValue = (inputTxtObj.textContent!=null)?inputTxtObj.textContent:(inputTxtObj.textContent!=null)?inputTxtObj.textContent:'';
        
        if(val!='' && inputTxtValue.indexOf(val) == -1 && val!='Verified by Experian QAS!')
        {
            if(inputTxtObj.textContent!=null)
                inputTxtObj.textContent+=val+', ';
            else
                inputTxtObj.textContent+=val+', ';
        }
    };
    
    //Override the setElementValue function.
    EDQ.DataQuality.Salesforce.Address.Client.prototype.setElementValue = function setElementValue(elementId, value, callback) {
        var sys = EDQ.system;
        var sfdc = EDQ.DataQuality.Salesforce;
        value = sfdc.convertCountryValueToCountryPicklistValue(value); //Convert United States of America to United States.
        
        var oldCallBack = callback;
        callback = function() {
            testCallBack(elementId, value);
            if(sys.isFunction(oldCallBack )) { oldCallBack (); };
        }
        sfdc.Address.Client.uber.setElementValue.apply(this, [elementId, value, callback]);
    }
    
    var recordId = 'a09';
    window.IsEDQDynamicPage = true;
    sforce.connection.sessionId = "{!$Api.Session_ID}";
    
    (function () {
        var sys = EDQ.system;
        var sfdc = EDQ.DataQuality.Salesforce;
        sfdc.initialize({}, recordId.substring(0, 3), recordId);
        
        //Onload invoke as per v4
        EDQ.jQuery(document).ready(function(){
            window.edqAddressClient.beforeInvokeRapidSearch = function beforeInvokeRapidSearch (touchpoint, args, typedownClient) { //This function checks whether the invocation cause is on focus and if it is cancels typedown invoke. This is to be used when you choose to invoke typedown with a custom button.
                var client = EDQ.DataQuality.Address.Client;
                
                if(args.type === client.LoadRapidSearchElementFocus) { return false; };
                
                return true;
            };
            
            if({!enableManualSelection} && {!isAddressPopupOnload} && getUrlVars()["action"] != null && {!bypassQAS} == false)
                invokeTypedown();
            
            document.getElementById('{!$Component.pbsAddressFields.msgWarningForSecondaryAddrType}').style.display = 'none';
        });
    })();
    
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }
    
    //No Edit address functionality is available on the form, so this function is not attached to Save buttons click event.
    function fixValidationStatusIfFieldsWereEdited() {
        var sys = EDQ.system;
        if(window.edqAddressClient != null && window.edqAddressClient != undefined) {
            try {
                window.edqAddressClient.fixValidationStatusIfFieldsWereEdited();
            } catch(error) {
                sys.logError(error);
            }
        }
    }
    
    //Start Button only invoke.
    function invokeTypedown() {
        var sys = EDQ.system;
        var dqAddressClient = EDQ.DataQuality.Address.Client;
        var touchpoint = window.edqAddressClient._touchpoints[0];
        var args = dqAddressClient._createArguments(dqAddressClient.LoadRapidSearchExplicit, null);
        window.edqAddressClient.invokeRapidSearch(touchpoint, args);
        
        //Show the save buttons after RapidSearch is invoked
        try {
            document.getElementById('{!buttonId}').style.display='';
            var pos = '{!buttonId}'.lastIndexOf(':');
            var bottomButtonId = '{!buttonId}'.substring(0,pos) + ':bottom' + '{!buttonId}'.substring(pos);
            document.getElementById(bottomButtonId).style.display='';
        }catch(ex){
            sys.logError(ex);
        }
    }
    </script>
    
    <!-- END INPUT FIELDS-->    
    <apex:pageBlockSection collapsible="false" title="{!$ObjectType.Address__c.Label}" id="pbsAddressFields" columns="1">
        <!-- onchange event modified to support IE 8   -->
        <apex:inputHidden id="consumerNoAddr" value="{!ignoreAddress}" />
        <apex:outputPanel rendered="{!contactAddressRec!=null}">
            <apex:pageBlockSectionItem rendered="{!isConsumerContact}">
                <input type="radio" id="noAddress" name="radClass" value="NoAddr" onclick="radioButtonSelected(this.value);" />{!$Label.Create_Contact_Without_Address}<br />
            </apex:pageBlockSectionItem>
            <input type="radio" name="radClass" value="Existing" onclick="radioButtonSelected(this.value);" />{!$Label.Choose_an_Existing_Address}<br />
            <br />
            
            <apex:pageBlockTable rendered="{!IF(ISNULL(accountAddressList),false,true)}" value="{!accountAddressList}"
                                 var="item" id="pbTable">
                <apex:column headerValue="Select">
                    <input type="radio" name="radAddressOptions" class="radAddress" value="{!item.Address__c}"
                           disabled="disabled" onclick="existingAddressSelected(this)" />
                    <apex:inputHidden value="{!item.Address_Type__c}" id="addrType" />
                </apex:column>
                <apex:column headerValue="Address Line 1">
                    <apex:outputField value="{!item.Address__r.Address_1__c}" id="address1"/>
                </apex:column>
                <apex:column headerValue="Address Line 2">
                    <apex:outputField value="{!item.Address__r.Address_2__c}" id="address2"/>
                </apex:column>
                <apex:column headerValue="Address Line 3">
                    <apex:outputField value="{!item.Address__r.Address_3__c}" id="address3"/>
                </apex:column>
                <apex:column headerValue="City">
                    <apex:outputField value="{!item.Address__r.City__c}" id="city"/>
                </apex:column>
                <apex:column headerValue="Country">
                    <apex:outputField value="{!item.Address__r.Country__c}" id="country"/>
                </apex:column>
                <apex:column headerValue="Postcode">
                    <apex:outputField value="{!item.Address__r.Postcode__c}" id="postCode"/>
                </apex:column>
            </apex:pageBlockTable>
        </apex:outputPanel>
        <apex:outputPanel rendered="{!IF(bypassQAS,false,true)}" >
            <input type="radio" name="radClass" value="new" onclick="radioButtonSelected(this.value)" />{!$Label.Search_for_a_new_address}<br />
        </apex:outputPanel>
        <br />
        <apex:outputPanel rendered="{!IF(enableManualSelection && !bypassQAS , true ,false)}" layout="block" >
            <input type="button" id="searchButton" class="btnDisabled" value="{!$Label.ASS_Button_Search}" onclick="invokeTypedown(); return false;" disabled="disabled" />
        </apex:outputPanel>
        <br />

        <apex:pageMessage id="msgWarningForSecondaryAddrType" summary="You've selected a 'Secondary' address." severity="warning" strength="3" />

        <apex:inputfield value="{!accountAddressRec.Address_Type__c}" id="addrPurpose1"  required="{!IF(bypassQAS,false,true)}"  rendered="{!IF(ISNULL(accountAddressRec),false,IF(bypassQAS,false,true))}"/>
        <apex:inputfield value="{!contactAddressRec.Address_Type__c}" id="addrPurpose2"  required="{!AND(IF(bypassQAS,false,true),isConsumerContact==false)}"  rendered="{!IF(ISNULL(contactAddressRec),false,IF(bypassQAS,false,true))}"/>
        <apex:pageBlockSectionItem id="pbsiAddress" rendered="{!!bypassQAS}" >
            <apex:outputLabel value="{!$ObjectType.Address__c.Label}" />
            <apex:outputText id="oplConcatenatedAddress" />
        </apex:pageBlockSectionItem>
    </apex:pageBlockSection>
    
    <script>
    var varOplConcatenatedAddress = '{!$Component.pbsAddressFields.pbsiAddress.oplConcatenatedAddress}';
    var varAddrPurpose2 = '{!$Component.pbsAddressFields.addrPurpose2}';
    //added by JG
    //function to help functionality of checkboxes and search button
    function radioButtonSelected (optionSelected) {

        document.getElementById('{!$Component.pbsAddressFields.consumerNoAddr}').value = false;

        if (optionSelected.toLowerCase() == 'new') {
            document.getElementById("searchButton").className = 'btn';
            document.getElementById("searchButton").disabled = false;
            var arrCheckBoxes =  document.getElementsByName("radAddressOptions");
            for (index = 0; index < arrCheckBoxes.length; ++index) {
                arrCheckBoxes[index].disabled = true;
                arrCheckBoxes[index].checked = false;
            }
            updateAddressId('');//to re-initiate the address id ; uncommented 10/13/16 sky
        } else if (optionSelected.toLowerCase() == 'existing') {
            document.getElementById("searchButton").className = 'btnDisabled';
            document.getElementById("searchButton").disabled = true;
            var arrCheckBoxes = document.getElementsByName("radAddressOptions");
            for (index = 0; index < arrCheckBoxes.length; ++index) {
                arrCheckBoxes[index].disabled = false;
            }
        } else if (optionSelected.toLowerCase() == 'noaddr') { //Ryan
            document.getElementById("searchButton").className = 'btnDisabled';
            document.getElementById("searchButton").disabled = true;
            var arrCheckBoxes = document.getElementsByName("radAddressOptions");
            for (index = 0; index < arrCheckBoxes.length; ++index) {
                arrCheckBoxes[index].disabled = true;
                arrCheckBoxes[index].checked = false;
            }

            document.getElementById('{!$Component.pbsAddressFields.consumerNoAddr}').value = true;   
        }
        
        //empty the address record on change
        document.getElementById(varAddrPurpose2).value = '';
        document.getElementById(varOplConcatenatedAddress).innerText='';
        document.getElementById(varOplConcatenatedAddress).textContent='';
    }
    //added by JG
    //function to update the address on page (text only-for UI purpose on page)
    function existingAddressSelected (addressSelected) {
        var chkBoxId = new String (addressSelected.parentNode.getAttribute('id'));
        updateAddressId(addressSelected.value); //uncommented 10/13/16 sky
                
        var addrType = document.getElementById(chkBoxId.substring(0,chkBoxId.lastIndexOf(":"))+':addrType').value;
        
        var addressTxt =
            document.getElementById(chkBoxId.substring(0,chkBoxId.lastIndexOf(":"))+':address1').innerHTML+', '+
            document.getElementById(chkBoxId.substring(0,chkBoxId.lastIndexOf(":"))+':city').innerHTML+', '+
            document.getElementById(chkBoxId.substring(0,chkBoxId.lastIndexOf(":"))+':country').innerHTML+', '+
            document.getElementById(chkBoxId.substring(0,chkBoxId.lastIndexOf(":"))+':postCode').innerHTML;
        
        document.getElementById(varAddrPurpose2).value = addrType;
        
        document.getElementById(varOplConcatenatedAddress).innerText=addressTxt;
        document.getElementById(varOplConcatenatedAddress).textContent=addressTxt;

        if(addrType == 'Secondary')
        {
            document.getElementById('{!$Component.pbsAddressFields.msgWarningForSecondaryAddrType}').style.display = 'block';    
        }
        else
        {
            document.getElementById('{!$Component.pbsAddressFields.msgWarningForSecondaryAddrType}').style.display = 'none';
        }
    }
    </script>
    
    <!-- All Fields Start --->
    <!-- Experian Data Quality: updated Id's to include __c suffix -->
    <apex:outputPanel rendered="{!enableManualSelection}" style="display:none">
        <apex:inputtext value="{!addressRec.Address_1__c}" id="Address_1__c"/>
        <apex:inputtext value="{!addressRec.Address_2__c}" id="Address_2__c" />
        <apex:inputtext value="{!addressRec.Address_3__c}" id="Address_3__c" />
        <apex:inputtext value="{!addressRec.Address_4__c}" id="Address_4__c" />
        <apex:inputtext value="{!addressRec.CEDEX__c}" id="CEDEX__c" />
        <apex:inputtext value="{!addressRec.City__c}" id="City__c" />
        <apex:inputtext value="{!addressRec.Codiga_Postal__c}" id="Codiga_Postal__c" />
        <apex:inputtext value="{!addressRec.Country__c}" id="Country__c" />
        <apex:inputtext value="{!addressRec.County__c}" id="County__c" />
        <apex:inputtext value="{!addressRec.District__c}" id="District__c" />
        <apex:inputtext value="{!addressRec.Emirate__c}" id="Emirate__c" />
        <apex:inputtext value="{!addressRec.Floor__c}" id="Floor__c" />
        <apex:inputtext value="{!addressRec.Partofterritory__c}" id="Partofterritory__c" />
        <apex:inputtext value="{!addressRec.POBox__c}" id="POBox__c" />
        <apex:inputtext value="{!addressRec.Postcode__c}" id="Postcode__c" />
        <apex:inputtext value="{!addressRec.Prefecture__c}" id="Prefecture__c" />
        <apex:inputtext value="{!addressRec.Province__c}" id="Province__c" />
        <apex:inputtext value="{!addressRec.SortingCode__c}" id="SortingCode__c" />
        <apex:inputtext value="{!addressRec.State__c}" id="State__c" />
        <apex:inputtext value="{!addressRec.Suite__c}" id="Suite__c" />
        <apex:inputtext value="{!addressRec.Zip__c}" id="Zip__c" />
        
        <apex:inputfield value="{!addressRec.Validation_Status__c}" id="Validation_Status__c" />
        <apex:inputfield value="{!addressRec.Last_Validated__c}" id="Last_Validated__c" />
        <!-- 2nd Dec, 2014 - Case 29635 by JW - Add Barcode variable  -->
        <apex:inputfield value="{!addressRec.Bar_Code__c}" id="Bar_Code__c" />
    </apex:outputPanel>
    <!--All Fields End -->
    <script>
    
    EDQ.jQuery(document).find('input[id$=":Address_1__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Address_2__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Address_3__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Address_4__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":CEDEX__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":City__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Codiga_Postal__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Country__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":County__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":District__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Emirate__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Floor__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Partofterritory__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":POBox__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Postcode__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Prefecture__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Province__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":SortingCode__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":State__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Suite__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Zip__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":LastValidated__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":ValidationStatus__c"]').attr('readonly', true);
    EDQ.jQuery(document).find('input[id$=":Bar_Code__c"]').attr('readonly', true);


    var isConsum = '{!isConsumerContact}';
    if (isConsum == 'true') {
        document.getElementById("noAddress").checked = true;
        document.getElementById('{!$Component.pbsAddressFields.consumerNoAddr}').value = true;
    }

    </script>
    <!-- END INPUT FIELDS-->
    
    <!-- START OUTPUT FIELDS -->
    <c:AddressFieldsByCountry addressRecId="{!addressRec.id}" rendered="{!!enableManualSelection}" title="{!$ObjectType.Address__c.Label}" columns="2"/>
    <!-- END READ ONLY -->
    
    
</apex:component>