<?xml version="1.0" encoding="UTF-8"?>
<Flow xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Updated record look up so that {!UserFunction} variable is populated from a different field.</description>
    <formulas>
        <description>formula to define the description of the case</description>
        <name>DescriptionNewTraining</name>
        <dataType>String</dataType>
        <expression>&quot;New user training request: &quot;  +  &quot; | &quot; +
&quot;User Name: &quot; + {!UserFirstName} + &quot; &quot; + {!UserLastName} + &quot; | &quot; +
&quot;Job Title: &quot; +  {!UserJobTitle} + &quot; | &quot; +
&quot;Reports To: &quot; +  {!UserReportsInto} + &quot; | &quot; +
&quot;Country: &quot; +  {!UserCountry}</expression>
    </formulas>
    <formulas>
        <description>Formula to set the Subject field on the case being created based on information from the case just closed.</description>
        <name>SubjectNewTraining</name>
        <dataType>String</dataType>
        <expression>{!UserRegion} + &quot; - &quot; + {!UserBU} + &quot; - &quot; + {!UserFunction} + &quot; - &quot; + {!UserFirstName} + &quot; &quot; + {!UserLastName} + &quot; (&quot; + {!UserStartDate} + &quot;)&quot;</expression>
    </formulas>
    <interviewLabel>Case-CreateTrainingCase {!$Flow.CurrentDateTime}</interviewLabel>
    <label>Case-CreateTrainingCase</label>
    <processType>AutoLaunchedFlow</processType>
    <recordCreates>
        <description>Create a new training request using information from the case just closed</description>
        <name>Create_New_Training_Case</name>
        <label>Create New Training Case</label>
        <locationX>524</locationX>
        <locationY>281</locationY>
        <inputAssignments>
            <field>BusinessHoursId</field>
            <value>
                <stringValue>01mi0000000YXUk</stringValue>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Description</field>
            <value>
                <elementReference>DescriptionNewTraining</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Error_Type_Root_Cause__c</field>
            <value>
                <stringValue>New user training</stringValue>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>ParentId</field>
            <value>
                <elementReference>ClosedCaseID</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Reason</field>
            <value>
                <stringValue>Training</stringValue>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>RecordTypeId</field>
            <value>
                <stringValue>012i00000019lV3</stringValue>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Requester_BU__c</field>
            <value>
                <elementReference>RequestorBU</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Requestor_Email__c</field>
            <value>
                <elementReference>RequestorEmail</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Requestor_Work_Phone__c</field>
            <value>
                <elementReference>RequestorTelephone</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Requestor__c</field>
            <value>
                <elementReference>RequestorUserRecordID</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Secondary_Case_Reason__c</field>
            <value>
                <stringValue>Training request</stringValue>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>Subject</field>
            <value>
                <elementReference>SubjectNewTraining</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>User_Email_Address__c</field>
            <value>
                <elementReference>UserEmail</elementReference>
            </value>
        </inputAssignments>
        <inputAssignments>
            <field>User_Requestor__c</field>
            <value>
                <elementReference>RequestorName</elementReference>
            </value>
        </inputAssignments>
        <object>Case</object>
    </recordCreates>
    <recordLookups>
        <description>Look up the case just closed to obtain information to pass onto the new case being created</description>
        <name>Closed_Case_Look_Up</name>
        <label>Closed Case Look Up</label>
        <locationX>525</locationX>
        <locationY>178</locationY>
        <assignNullValuesIfNoRecordsFound>false</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Create_New_Training_Case</targetReference>
        </connector>
        <filters>
            <field>Id</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>ClosedCaseID</elementReference>
            </value>
        </filters>
        <object>Case</object>
        <outputAssignments>
            <assignToReference>UserCountry</assignToReference>
            <field>Country__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserStartDate</assignToReference>
            <field>Date_Due_for_Completion__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserFunction</assignToReference>
            <field>Function__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserBU</assignToReference>
            <field>Organization_BU__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserRegion</assignToReference>
            <field>Region__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserReportsInto</assignToReference>
            <field>Reports_Into_in_CRM_hierarchy__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>RequestorBU</assignToReference>
            <field>Requester_BU__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>RequestorRegion</assignToReference>
            <field>Requester_Region__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>RequestorCountry</assignToReference>
            <field>Requestor_Country__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>RequestorEmail</assignToReference>
            <field>Requestor_Email__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>RequestorTelephone</assignToReference>
            <field>Requestor_Work_Phone__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>RequestorUserRecordID</assignToReference>
            <field>Requestor__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserEmail</assignToReference>
            <field>User_Email_Address__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserFirstName</assignToReference>
            <field>User_First_Name__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserJobTitle</assignToReference>
            <field>User_Job_Title__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>UserLastName</assignToReference>
            <field>User_Last_Name__c</field>
        </outputAssignments>
        <outputAssignments>
            <assignToReference>RequestorName</assignToReference>
            <field>User_Requestor__c</field>
        </outputAssignments>
    </recordLookups>
    <startElementReference>Closed_Case_Look_Up</startElementReference>
    <variables>
        <description>Variable to store the ID of the case just closed</description>
        <name>ClosedCaseID</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>false</isOutput>
    </variables>
    <variables>
        <description>Stores the request BU information from the case just closed</description>
        <name>RequestorBU</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the requestor&apos;s country from the case just closed</description>
        <name>RequestorCountry</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the requestors email of the case just closed</description>
        <name>RequestorEmail</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>stores the requestor&apos;s name of the case just closed</description>
        <name>RequestorName</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>stores the requestors region of the case just closed</description>
        <name>RequestorRegion</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the requestor&apos;s phone number from the case just closed</description>
        <name>RequestorTelephone</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the Requestor Users record ID of the case just closed</description>
        <name>RequestorUserRecordID</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the new users BU from the case just closed</description>
        <name>UserBU</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the country set on the new user case closed</description>
        <name>UserCountry</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>stores the email address of the new user</description>
        <name>UserEmail</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the new users first name from the case just closed</description>
        <name>UserFirstName</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the user function set on the new user case</description>
        <name>UserFunction</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the job title of the new user case just closed</description>
        <name>UserJobTitle</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the users last name from the case just closed</description>
        <name>UserLastName</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the new user&apos;s region from the case just closed</description>
        <name>UserRegion</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the new user&apos;s manager as set on the new user case</description>
        <name>UserReportsInto</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
    <variables>
        <description>Stores the start of the new user from the case just closed</description>
        <name>UserStartDate</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
    </variables>
</Flow>
