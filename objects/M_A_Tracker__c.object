<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Custom object for report snapshot for M&amp;A team (James King)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Current_year_EBITDA_converted__c</fullName>
        <externalId>false</externalId>
        <label>Current year EBITDA (converted)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Current_year_revenue_converted__c</fullName>
        <externalId>false</externalId>
        <label>Current year revenue (converted)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Deal_Last_Modified_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Deal: Last Modified Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Maximum_transaction_value_converted__c</fullName>
        <externalId>false</externalId>
        <label>Maximum transaction value (converted)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Minimum_transaction_value_converted__c</fullName>
        <externalId>false</externalId>
        <label>Minimum transaction value (converted)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Region__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Region</label>
        <length>125</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Report_Run_Time__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Report Run Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Segment__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Segment</label>
        <length>125</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Stage__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Stage</label>
        <length>125</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sub_Stage__c</fullName>
        <externalId>false</externalId>
        <label>Sub Stage</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Future Reactive Target</fullName>
                    <default>false</default>
                    <label>Future Reactive Target</label>
                </value>
                <value>
                    <fullName>Future Proactive Target</fullName>
                    <default>false</default>
                    <label>Future Proactive Target</label>
                </value>
                <value>
                    <fullName>Review</fullName>
                    <default>false</default>
                    <label>Review</label>
                </value>
                <value>
                    <fullName>Evaluation</fullName>
                    <default>false</default>
                    <label>Evaluation</label>
                </value>
                <value>
                    <fullName>BU Sponsorship</fullName>
                    <default>false</default>
                    <label>BU Sponsorship</label>
                </value>
                <value>
                    <fullName>Regional SPC Recommended</fullName>
                    <default>false</default>
                    <label>Regional SPC Recommended</label>
                </value>
                <value>
                    <fullName>Due Diligence</fullName>
                    <default>false</default>
                    <label>Due Diligence</label>
                </value>
                <value>
                    <fullName>Negotiation</fullName>
                    <default>false</default>
                    <label>Negotiation</label>
                </value>
                <value>
                    <fullName>Global SPC Recommended</fullName>
                    <default>false</default>
                    <label>Global SPC Recommended</label>
                </value>
                <value>
                    <fullName>Board Approved</fullName>
                    <default>false</default>
                    <label>Board Approved</label>
                </value>
                <value>
                    <fullName>Deal Signed</fullName>
                    <default>false</default>
                    <label>Deal Signed</label>
                </value>
                <value>
                    <fullName>Deal Closed</fullName>
                    <default>false</default>
                    <label>Deal Closed</label>
                </value>
                <value>
                    <fullName>Dead</fullName>
                    <default>false</default>
                    <label>Dead</label>
                </value>
                <value>
                    <fullName>Preliminary Review/Watch</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Preliminary Review/Watch</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Type</label>
        <length>125</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>M&amp;A Tracker</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Deal #</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>M&amp;ATracker</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
