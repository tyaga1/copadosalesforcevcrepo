<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object is used to store critical success factors which can be plugged into the SWOTs
S-210309</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Account_Plan_Objective_Name__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>Account_Plan_Objective__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Plan Objective Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Plan_Objective__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>See case #01848189 - Account Planning Enhancements.</description>
        <externalId>false</externalId>
        <label>Account Plan Objective</label>
        <referenceTo>Account_Plan_Objective__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Account_Plan_SWOT__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <externalId>false</externalId>
        <label>Account Plan SWOT</label>
        <referenceTo>Account_Plan_SWOT__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors3</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Account_Plan__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <externalId>false</externalId>
        <label>Account Plan</label>
        <referenceTo>Account_Plan__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>What are the critical things that need to happen in order for Experian to achieve our Objectives?</inlineHelpText>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_1_Description__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>SWOT_1__r.Description__c</formula>
        <label>SWOT 1 Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_1_Importance__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT(SWOT_1__r.Importance__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SWOT 1 Importance</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <externalId>false</externalId>
        <label>SWOT 1</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Account_Plan__c</field>
                <operation>equals</operation>
                <valueField>Account_Plan_SWOT__c.Account_Plan__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account_Plan_SWOT__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SWOT_2_Description__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>SWOT_2__r.Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SWOT 2 Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_2_Importance__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT(SWOT_2__r.Importance__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SWOT 2 Importance</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <externalId>false</externalId>
        <label>SWOT 2</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Account_Plan__c</field>
                <operation>equals</operation>
                <valueField>Account_Plan_SWOT__c.Account_Plan__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account_Plan_SWOT__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SWOT_3_Description__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>SWOT_3__r.Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SWOT 3 Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_3_Importance__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT(SWOT_3__r.Importance__c)</formula>
        <label>SWOT 3 Importance</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <externalId>false</externalId>
        <label>SWOT 3</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Account_Plan__c</field>
                <operation>equals</operation>
                <valueField>Account_Plan_SWOT__c.Account_Plan__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account_Plan_SWOT__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SWOT_4_Description__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>SWOT_4__r.Description__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SWOT 4 Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_4_Importance__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT( SWOT_4__r.Importance__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SWOT 4 Importance</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_4__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <externalId>false</externalId>
        <label>SWOT 4</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Account_Plan__c</field>
                <operation>equals</operation>
                <valueField>Account_Plan_SWOT__c.Account_Plan__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account_Plan_SWOT__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors5</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SWOT_5_Description__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>SWOT_5__r.Description__c</formula>
        <label>SWOT 5 Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_5_Importance__c</fullName>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT(SWOT_5__r.Importance__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SWOT 5 Importance</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SWOT_5__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Story # S-210309- Used for Value Areas flow</description>
        <externalId>false</externalId>
        <label>SWOT 5</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Account_Plan__c</field>
                <operation>equals</operation>
                <valueField>Account_Plan_SWOT__c.Account_Plan__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account_Plan_SWOT__c</referenceTo>
        <relationshipName>Account_Plan_Critical_Success_Factors4</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Critical Success Factor</label>
    <nameField>
        <displayFormat>CSF-{0000}</displayFormat>
        <label>Critical Success Factor</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Critical Success Factors</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_Account_Plan_Critical_Success_Factor</fullName>
        <availability>online</availability>
        <description>Similar to standard button, except that it will redirect to the Account Plan record after clicking &quot;Save&quot;
Story # S-351672</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Account Plan Critical Success Factor</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/{!$ObjectType.Account_Plan_Critical_Success_Factor__c}/e?
{!$Setup.Custom_Fields_Ids__c.AP_Critical_Success_Factor_AccountPlan__c}=
{!Account_Plan__c.Name}
&amp;{!$Setup.Custom_Fields_Ids__c.AP_Critical_Success_Factor_AccountPlan__c}_lkid=
{!Account_Plan__c.Id}
&amp;saveURL={!Account_Plan__c.Id}
&amp;retURL={!Account_Plan__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Critical_Success_Factor</fullName>
        <availability>online</availability>
        <description>See case #01848189 - Account Planning Enhancements.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Critical Success Factor</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/a11/e?&amp;{!$Setup.Custom_Fields_Ids__c.AP_Critical_Success_Factor_AccountPlan__c}={!Account_Plan__c.Name}

&amp;{!$Setup.Custom_Fields_Ids__c.AP_Critical_Success_Factor_AccountPlan__c}_lkid={!Account_Plan__c.Id}

&amp;{!$Setup.Custom_Fields_Ids__c.AP_Critical_Success_Factor_AccountPlanOb__c}=
{!Account_Plan_Objective__c.Name}

&amp;saveURL={!Account_Plan_Objective__c.Id}
&amp;retURL={!Account_Plan_Objective__c.Id}</url>
    </webLinks>
</CustomObject>
