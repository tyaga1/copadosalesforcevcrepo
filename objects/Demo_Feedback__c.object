<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Date_Created__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TODAY()</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Date Created</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Decision__c</fullName>
        <externalId>false</externalId>
        <label>Decision</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Out of Scope/Future Enh</fullName>
                    <default>false</default>
                    <label>Out of Scope/Future Enh</label>
                </value>
                <value>
                    <fullName>Discarded</fullName>
                    <default>false</default>
                    <label>Discarded</label>
                </value>
                <value>
                    <fullName>Discuss Further</fullName>
                    <default>false</default>
                    <label>Discuss Further</label>
                </value>
                <value>
                    <fullName>Create a Story</fullName>
                    <default>false</default>
                    <label>Create a Story</label>
                </value>
                <value>
                    <fullName>Add to Story</fullName>
                    <default>false</default>
                    <label>Add to Story</label>
                </value>
                <value>
                    <fullName>No action required</fullName>
                    <default>false</default>
                    <label>No action required</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Demo_Feedbacks</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>In-Progress</fullName>
                    <default>false</default>
                    <label>In-Progress</label>
                </value>
                <value>
                    <fullName>Not Started</fullName>
                    <default>false</default>
                    <label>Not Started</label>
                </value>
                <value>
                    <fullName>N/A</fullName>
                    <default>false</default>
                    <label>N/A</label>
                </value>
                <value>
                    <fullName>Waiting on someone else</fullName>
                    <default>false</default>
                    <label>Waiting on someone else</label>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                    <label>Cancelled</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Work__c</fullName>
        <externalId>false</externalId>
        <label>Work</label>
        <referenceTo>agf__ADM_Work__c</referenceTo>
        <relationshipLabel>Demo Feedbacks</relationshipLabel>
        <relationshipName>Demo_Feedbacks</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackFeedHistory>true</trackFeedHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Demo Feedback</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Demo Feedback Name</label>
        <trackFeedHistory>true</trackFeedHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Demo Feedbacks</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Create_Story</fullName>
        <availability>online</availability>
        <description>Create a Story/Work from Demo Feedback.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Create Story</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/35.0/connection.js&quot;)} 

var returnURL = &apos;retURL=/{!Demo_Feedback__c.Id}&apos;;
var retStr = &apos;/{!$ObjectType.agf__ADM_Work__c}/e?&apos; + returnURL;

   retStr += &apos;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Description_Details__c}={!Demo_Feedback__c.Description__c}&apos;;
   retStr += &apos;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Subject__c}=Story created from Demo Feedback on Story:{!Demo_Feedback__c.Work__c}&apos;;


var result; 
var query = &quot;SELECT Id, Name,&quot; +   
                &quot; Work__r.Experian_Project__r.Name, &quot; + 
                &quot; Work__r.Experian_Project__c, &quot; + 
                &quot; Work__r.Business_Unit__c, &quot; + 
                &quot; Work__r.agf__Product_Area__c, &quot; + 
                &quot; Work__r.agf__Major_Func_Area__c, &quot; + 
                &quot; Work__r.agf__Minor_Func_Area__c &quot; + 
                &quot; FROM Demo_Feedback__c WHERE ID=&apos;{!Demo_Feedback__c.Id}&apos; LIMIT 1&quot;; 

try 
{ 
    result = sforce.connection.query(query); 
} 
catch(e) 
{ 
    console.log(e); 
} 

  records = result.getArray(&quot;records&quot;); 

if(records.length &gt; 0) 
{ 
  var rec = records[0];   
	
	if(rec.Work__r != null) 
	{ 
		/*if(rec.Work__r.Business_Unit__c != null) 
			retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Business_Unit__c}=&quot; + encodeURIComponent(rec.Work__r.Business_Unit__c); 
                    //UK&amp;I GTM - ampersand causes problem for encoding and decoding.
                */
	
		if(rec.Work__r.Experian_Project__c != null) 
		{
			retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Experian_Project__c}=&quot; + rec.Work__r.Experian_Project__r.Name; 
			retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Experian_Project__c}_lkid=&quot; + rec.Work__r.Experian_Project__c; 
		}
	
		if(rec.Work__r.agf__Product_Area__c != null) 
			retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Product_Area__c}=&quot; + rec.Work__r.agf__Product_Area__c; 
	
		if(rec.Work__r.agf__Major_Func_Area__c != null) 
			retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Major_Func_Area__c}=&quot; + rec.Work__r.agf__Major_Func_Area__c; 

		if(rec.Work__r.agf__Minor_Func_Area__c != null) 
			retStr+= &quot;&amp;{!$Setup.Custom_Fields_Ids__c.Work_Minor_Func_Area__c}=&quot; + rec.Work__r.agf__Minor_Func_Area__c; 
                
	
	} 
	
} 
else 
{ 
	console.log(&apos;Custom button \&apos;Create Story\&apos; returned no result for query : &apos; + query); 
	alert(&apos;No record found. Contact your administrator.&apos;); 
}

window.open(encodeURI(retStr), &quot;_self&quot;);</url>
    </webLinks>
</CustomObject>
