<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Maps a particular package service to a particular project</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Read</externalSharingModel>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Business Processes</relationshipLabel>
        <relationshipName>Business_Processes</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Process_Template__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Business Process Template</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>((1 AND 4) OR (2 AND 5) OR (3 AND 6) OR (7 AND 8)) AND 9</booleanFilter>
            <errorMessage>Please verify the template is active, and that the value in the Related To field matches the parent record.</errorMessage>
            <filterItems>
                <field>$Source.Case__c</field>
                <operation>notEqual</operation>
                <value></value>
            </filterItems>
            <filterItems>
                <field>$Source.Membership__c</field>
                <operation>notEqual</operation>
                <value></value>
            </filterItems>
            <filterItems>
                <field>$Source.Opportunity__c</field>
                <operation>notEqual</operation>
                <value></value>
            </filterItems>
            <filterItems>
                <field>Business_Process_Template__c.Related_To__c</field>
                <operation>equals</operation>
                <value>Cases</value>
            </filterItems>
            <filterItems>
                <field>Business_Process_Template__c.Related_To__c</field>
                <operation>equals</operation>
                <value>Membership</value>
            </filterItems>
            <filterItems>
                <field>Business_Process_Template__c.Related_To__c</field>
                <operation>equals</operation>
                <value>Opportunities</value>
            </filterItems>
            <filterItems>
                <field>$Source.Account__c</field>
                <operation>notEqual</operation>
                <value></value>
            </filterItems>
            <filterItems>
                <field>Business_Process_Template__c.Related_To__c</field>
                <operation>equals</operation>
                <value>Accounts</value>
            </filterItems>
            <filterItems>
                <field>Business_Process_Template__c.Inactive__c</field>
                <operation>equals</operation>
                <value>False</value>
            </filterItems>
            <infoMessage>Business Process Templates available for selection are specific to the parent object type: a single case OR a membership OR an opportunity OR an account</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Business_Process_Template__c</referenceTo>
        <relationshipLabel>Business Processes</relationshipLabel>
        <relationshipName>R00N40000001lfHwEAI</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <externalId>false</externalId>
        <label>Business Unit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>CSDA</fullName>
                    <default>false</default>
                    <label>CSDA</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipName>Project_Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Closed_Date__c</fullName>
        <description>Date the service was closed so revenue can be recognized.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Closed Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Membership__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Membership</label>
        <referenceTo>Membership__c</referenceTo>
        <relationshipLabel>Business Processes</relationshipLabel>
        <relationshipName>Business_Processes</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Open_Duration__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL( Status__c , &quot;Closed&quot;),
 Time_to_Close__c ,
  NOW() -CreatedDate  )</formula>
        <label>Open Duration</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipName>Project_Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Related_Parent_Record_s_Account__c</fullName>
        <description>This is the related parent&apos;s account name</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>If( 
NOT(IsBlank(Case__c)) &amp;&amp; NOT(IsBlank(Case__r.Account.Id)),  HYPERLINK(&quot;/&quot; &amp; Case__r.Account.Id,&quot;Case&apos;s Account: &quot; &amp; Case__r.Account.Name, &quot;_self&quot;),
IF(
NOT(IsBlank( Membership__c )) &amp;&amp; NOT(IsBlank( Membership__r.Account__c)), HYPERLINK(&quot;/&quot; &amp; Membership__r.Account__r.Id, &quot;Membership&apos;s Account: &quot; &amp; Membership__r.Account__r.Name, &quot;_self&quot;),
IF(
NOT(IsBlank( Opportunity__c )) &amp;&amp; NOT(IsBlank(Opportunity__r.Account.Id)), HYPERLINK(&quot;/&quot; &amp; Opportunity__r.Account.Id, &quot;Opportunity&apos;s Account: &quot; &amp; Opportunity__r.Account.Name, &quot;_self&quot;),
&apos;&apos;
)
) 
)</formula>
        <inlineHelpText>This is the related parent&apos;s account name</inlineHelpText>
        <label>Related Parent Record&apos;s Account</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Open</fullName>
                    <default>true</default>
                    <label>Open</label>
                </value>
                <value>
                    <fullName>Closed</fullName>
                    <default>false</default>
                    <label>Closed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Sub_Business_Unit__c</fullName>
        <externalId>false</externalId>
        <label>Sub Business Unit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>CIS</fullName>
                    <default>false</default>
                    <label>CIS</label>
                </value>
                <value>
                    <fullName>BIS</fullName>
                    <default>false</default>
                    <label>BIS</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Time_to_Close__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL( Status__c , &quot;Closed&quot;),
Closed_Date__c -  CreatedDate, NULL)</formula>
        <inlineHelpText>Closed Date - Created Date</inlineHelpText>
        <label>Time to Close</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Business Process</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Business_Process_Template__c</columns>
        <columns>Status__c</columns>
        <columns>OWNER.ALIAS</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All1</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>ProjectServicesModifiedByAppirioUse</fullName>
        <columns>NAME</columns>
        <columns>Business_Process_Template__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>UPDATEDBY_USER.ALIAS</field>
            <operation>equals</operation>
            <value>Appirio</value>
        </filters>
        <label>Project Services Modified By Appirio Use</label>
    </listViews>
    <listViews>
        <fullName>ProjectServiceswithoutProject</fullName>
        <columns>NAME</columns>
        <columns>Business_Process_Template__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>Project Services without Project</label>
    </listViews>
    <nameField>
        <displayFormat>BP-{000000}</displayFormat>
        <label>Business Process</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Business Processes</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Business_Process_Template__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Open_Duration__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Closed_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Time_to_Close__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Business_Process_Template__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Open_Duration__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Closed_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Time_to_Close__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Case__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Membership__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Opportunity__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>Case__c</searchFilterFields>
        <searchFilterFields>Membership__c</searchFilterFields>
        <searchFilterFields>Opportunity__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Business_Unit__c</searchFilterFields>
        <searchFilterFields>Sub_Business_Unit__c</searchFilterFields>
        <searchFilterFields>Business_Process_Template__c</searchFilterFields>
        <searchResultsAdditionalFields>Business_Process_Template__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Business_Unit__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Sub_Business_Unit__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Open_Duration__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Time_to_Close__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Closed_Date__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>Enforce_Single_Parent_Relationship</fullName>
        <active>true</active>
        <description>This validation rule enforces the user to choose one parent records and prevents the user from selecting multiple parent records.</description>
        <errorConditionFormula>(NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c)) &amp;&amp;
(
 (
 	IsBlank(Case__c) &amp;&amp;  
 	IsBlank(Membership__c) &amp;&amp;  
 	IsBlank(Opportunity__c) &amp;&amp;
        IsBlank(Account__c)
 ) ||
 (
 	NOT(IsBlank(Case__c)) &amp;&amp; 
 	(
 		NOT(IsBlank(Membership__c)) || 
 		NOT(IsBlank(Opportunity__c))||
                NOT(IsBlank(Account__c))

 	)
 ) ||
 (
 	NOT(IsBlank(Membership__c)) &amp;&amp; 
 	(
 		NOT(IsBlank(Case__c)) || 
 		NOT(IsBlank(Opportunity__c))||
                NOT(IsBlank(Account__c))
 	)
 ) ||
 (
 	NOT(IsBlank(Opportunity__c)) &amp;&amp; 
 	(
 		NOT(IsBlank(Case__c)) || 
 		NOT(IsBlank(Membership__c))||
                NOT(IsBlank(Account__c))
 	)
 )||
 (
 	NOT(IsBlank(Account__c)) &amp;&amp; 
 	(
 		NOT(IsBlank(Case__c)) || 
 		NOT(IsBlank(Membership__c))||
                NOT(IsBlank(Opportunity__c))
 	)
 )
)</errorConditionFormula>
        <errorMessage>Business Process must have a single parent, and cannot be related to multiple parents.   A  single parent can be Case, Membership, Account or Opportunity.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>New_Account_Business_Process</fullName>
        <availability>online</availability>
        <description>To be applied to Account Business Processes.  On Save of a new business process created from an account, this redirects the user back to the account.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Account Business Process</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Business_Process__c}/e?{!$Setup.Custom_Fields_Ids__c.Business_Process_Account__c}={!Account.Name}&amp;{!$Setup.Custom_Fields_Ids__c.Business_Process_Account__c}_lkid={!Account.Id}&amp;retURL={!Account.Id}&amp;saveURL={!Account.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Case_Business_Process</fullName>
        <availability>online</availability>
        <description>To be applied to Case Business Processes.  On Save of a new business process created from a case, this redirects the user back to the case.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Case Business Process</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Business_Process__c}/e?{!$Setup.Custom_Fields_Ids__c.Business_Process_Case__c}={!Case.CaseNumber}&amp;{!$Setup.Custom_Fields_Ids__c.Business_Process_Case__c}_lkid={!Case.Id}&amp;retURL={!Case.Id}&amp;saveURL={!Case.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Membership_Business_Process</fullName>
        <availability>online</availability>
        <description>To be applied to Membership Business Processes.  On Save of a new business process created from a membership, this redirects the user back to the membership.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Membership Business Process</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Business_Process__c}/e?{!$Setup.Custom_Fields_Ids__c.Business_Process_Membership__c}={!Membership__c.Name}&amp;{!$Setup.Custom_Fields_Ids__c.Business_Process_Membership__c}_lkid={!Membership__c.Id}&amp;retURL={!Membership__c.Id}&amp;saveURL={!Membership__c.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>New_Opportunity_Business_Process</fullName>
        <availability>online</availability>
        <description>To be applied to Opportunity Business Processes.  On Save of a new business process created from an opportunity, this redirects the user back to the opportunity.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <linkType>url</linkType>
        <masterLabel>New Opportunity Business Process</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>/{!$Setup.Custom_Object_Prefixes__c.Business_Process__c}/e?{!$Setup.Custom_Fields_Ids__c.Business_Process_Opportunity__c}={!Opportunity.Name}&amp;{!$Setup.Custom_Fields_Ids__c.Business_Process_Opportunity__c}_lkid={!Opportunity.Id}&amp;retURL={!Opportunity.Id}&amp;saveURL={!Opportunity.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Reset_All_PWIs_to_Wait</fullName>
        <availability>online</availability>
        <description>THIS IS FOR DEMO PURPOSES ONLY.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Reset Service</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/14.0/connection.js&quot;)}

{!REQUIRESCRIPT(&quot;/soap/ajax/14.0/apex.js&quot;)}



//reset ps to open and remove closed date and rev rec date

var ps = new sforce.SObject(&quot;Project_Service__c&quot;);

ps.Id = &quot;{!Business_Process__c.Id}&quot;;

ps.Status__c = &quot;Open&quot;;

ps.Closed_Date__c = null;

ps.Revenue_Recognition_Date__c = null;

var result = sforce.connection.upsert(&quot;Id&quot;, [ps]);



//update pwis

var pwis = sforce.connection.query(&quot;SELECT Id, Status__c FROM Project_Work_Item__c where Project_Service__c  = &apos;{!Business_Process__c.Id}&apos;&quot;);

var rec = pwis.getArray(&quot;records&quot;);

for (var i = 0;i&lt;rec.length;i++) {

  rec[i].Status__c = &quot;Wait&quot;;

}

sforce.connection.update(rec);


 
//refresh the page

window.top.location.href = window.top.location.href;</url>
    </webLinks>
</CustomObject>
