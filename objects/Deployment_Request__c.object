<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <content>DeploymentRequestClone</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Deployment_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Deployment Date</label>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Deployment_Label__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>IF(AND(NOT(ISBLANK(Project__c)),NOT(ISBLANK(Release__c))),
	Project__r.Name &amp;&quot; &quot;&amp; Release__c &amp;&quot; &quot;&amp; TEXT(Deployment_Date__c),
		IF(AND(NOT(ISBLANK(Project__c)), ISBLANK(Release__c)),
			Project__r.Name &amp;&quot; &quot;&amp; TEXT(Deployment_Date__c),
				IF(AND(ISBLANK(Project__c),NOT(ISBLANK(Release__c))),
					Release__c &amp;&quot; &quot;&amp; TEXT(Deployment_Date__c),
						Project__r.Name &amp;&quot; &quot;&amp; Release__c &amp;&quot; &quot; &amp;TEXT(Deployment_Date__c))))</formula>
        <label>Deployment Label</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deployment_Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Deployment Lead</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Deployment_Requests</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Has_Manual_Steps__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Will be ticked when components with Manual Steps are added.</inlineHelpText>
        <label>Has Manual Steps</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Production__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Production?</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Platform__c</fullName>
        <description>Displays the Platform related to the Source Environment to filter Export options</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT( Source__r.Platform__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Platform from Source Environment</inlineHelpText>
        <label>Platform</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Deployment Requests</relationshipLabel>
        <relationshipName>Deployment_Requests</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Release_Manager__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Release Manager</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Deployment_Requests1</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Release_Type__c</fullName>
        <description>Case 01250947</description>
        <externalId>false</externalId>
        <label>Release Type</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Immediate</fullName>
                    <default>false</default>
                    <label>Immediate</label>
                </value>
                <value>
                    <fullName>Standard</fullName>
                    <default>false</default>
                    <label>Standard</label>
                </value>
                <value>
                    <fullName>Major</fullName>
                    <default>false</default>
                    <label>Major</label>
                </value>
                <value>
                    <fullName>Minor</fullName>
                    <default>false</default>
                    <label>Minor</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Release__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Release</label>
        <length>80</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Secondary_Deployment_Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Secondary Deployment Lead</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Deployment_Requests2</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Source_Org__c</fullName>
        <externalId>false</externalId>
        <label>Source Org.</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>DEV</fullName>
                    <default>false</default>
                    <label>DEV</label>
                </value>
                <value>
                    <fullName>PRODUCTION</fullName>
                    <default>false</default>
                    <label>PRODUCTION</label>
                </value>
                <value>
                    <fullName>PROJECTDEV</fullName>
                    <default>false</default>
                    <label>PROJECTDEV</label>
                </value>
                <value>
                    <fullName>PROJECTUAT</fullName>
                    <default>false</default>
                    <label>PROJECTUAT</label>
                </value>
                <value>
                    <fullName>UAT</fullName>
                    <default>false</default>
                    <label>UAT</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Source</label>
        <referenceTo>Salesforce_Environment__c</referenceTo>
        <relationshipLabel>Deployment Requests</relationshipLabel>
        <relationshipName>Deployment_Requests</relationshipName>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Not Started</fullName>
                    <default>true</default>
                    <label>Not Started</label>
                </value>
                <value>
                    <fullName>Approval Requested</fullName>
                    <default>false</default>
                    <label>Approval Requested</label>
                </value>
                <value>
                    <fullName>Approved for Deployment</fullName>
                    <default>false</default>
                    <label>Approved for Deployment</label>
                </value>
                <value>
                    <fullName>Locked for Deployment</fullName>
                    <default>false</default>
                    <label>Locked for Deployment</label>
                </value>
                <value>
                    <fullName>Picked Up</fullName>
                    <default>false</default>
                    <label>Picked Up</label>
                </value>
                <value>
                    <fullName>Failed</fullName>
                    <default>false</default>
                    <label>Failed</label>
                </value>
                <value>
                    <fullName>Closed</fullName>
                    <default>false</default>
                    <label>Closed</label>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                    <label>Cancelled</label>
                </value>
                <value>
                    <fullName>Deployed to DEV</fullName>
                    <default>false</default>
                    <label>Deployed to DEV</label>
                </value>
                <value>
                    <fullName>Deployed to QA</fullName>
                    <default>false</default>
                    <label>Deployed to QA</label>
                </value>
                <value>
                    <fullName>Deployed to UAT</fullName>
                    <default>false</default>
                    <label>Deployed to UAT</label>
                </value>
                <value>
                    <fullName>Deployed to Production</fullName>
                    <default>false</default>
                    <label>Deployed to Production</label>
                </value>
                <value>
                    <fullName>Deployed to Merge Sandbox</fullName>
                    <default>false</default>
                    <label>Deployed to Merge Sandbox</label>
                </value>
                <value>
                    <fullName>Deployed to PreProd</fullName>
                    <default>false</default>
                    <label>Deployed to PreProd</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Target_Org__c</fullName>
        <externalId>false</externalId>
        <label>Target Org.</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>DEV</fullName>
                    <default>false</default>
                    <label>DEV</label>
                </value>
                <value>
                    <fullName>PRODUCTION</fullName>
                    <default>false</default>
                    <label>PRODUCTION</label>
                </value>
                <value>
                    <fullName>PROJECTDEV</fullName>
                    <default>false</default>
                    <label>PROJECTDEV</label>
                </value>
                <value>
                    <fullName>PROJECTUAT</fullName>
                    <default>false</default>
                    <label>PROJECTUAT</label>
                </value>
                <value>
                    <fullName>UAT</fullName>
                    <default>false</default>
                    <label>UAT</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Target__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Target</label>
        <referenceTo>Salesforce_Environment__c</referenceTo>
        <relationshipLabel>Deployment Requests (Salesforce Environment)</relationshipLabel>
        <relationshipName>Deployment_Requests1</relationshipName>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Deployment Request</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Project__c</columns>
        <columns>Source__c</columns>
        <columns>Status__c</columns>
        <columns>Target__c</columns>
        <columns>Is_Production__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>Deployment_Date__c</columns>
        <columns>LAST_UPDATE</columns>
        <columns>Release_Manager__c</columns>
        <columns>Deployment_Lead__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>LAST_ACTIVITY</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Slots_Awaiting_Deployment</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Is_Production__c</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>Project__c</columns>
        <columns>Release__c</columns>
        <columns>Release_Manager__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>,Approval Requested,Approved for Deployment,Locked for Deployment</value>
        </filters>
        <label>All Slots Awaiting Deployment</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Available_Production_Slots</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Deployment_Lead__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>Release_Manager__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>Project__c</columns>
        <columns>Release__c</columns>
        <columns>Status__c</columns>
        <columns>OWNER.ALIAS</columns>
        <columns>LAST_UPDATE</columns>
        <columns>LAST_ACTIVITY</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </filters>
        <label>Available Production Slots</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Available_Sandbox_Slots</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>Status__c</columns>
        <columns>Project__c</columns>
        <columns>Release__c</columns>
        <columns>Deployment_Lead__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>Release_Manager__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </filters>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>Available Sandbox Slots</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Available_Slots</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>Status__c</columns>
        <columns>Deployment_Lead__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>Release_Manager__c</columns>
        <columns>Is_Production__c</columns>
        <columns>Project__c</columns>
        <columns>Release__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Not Started</value>
        </filters>
        <label>Available Slots</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Completed_GCSS_CRMProduction_Deployments</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Is_Production__c</columns>
        <columns>Project__c</columns>
        <columns>Status__c</columns>
        <columns>Deployment_Lead__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Deployed,Deployed to Production</value>
        </filters>
        <filters>
            <field>Deployment_Label__c</field>
            <operation>startsWith</operation>
            <value>GCSS CRM BAU</value>
        </filters>
        <label>Completed GCSS CRMProduction Deployments</label>
        <language>en_US</language>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>Completed_Production_Deployments</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Is_Production__c</columns>
        <columns>Project__c</columns>
        <columns>Status__c</columns>
        <columns>Deployment_Lead__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Deployed,Deployed to Production</value>
        </filters>
        <label>Completed Production Deployments</label>
        <language>en_US</language>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>Completed_sandboxes_Deployments</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Project__c</columns>
        <columns>Status__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>LAST_UPDATE</columns>
        <columns>Is_Production__c</columns>
        <columns>UPDATEDBY_USER.ALIAS</columns>
        <columns>CREATEDBY_USER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Deployed,Deployed to QA,Deployed to UAT</value>
        </filters>
        <label>Completed Sandboxes Deployments</label>
        <language>en_US</language>
        <sharedTo>
            <allInternalUsers></allInternalUsers>
        </sharedTo>
    </listViews>
    <listViews>
        <fullName>PreProd_Slots</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Deployment_Lead__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>Release_Manager__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>Project__c</columns>
        <columns>Release__c</columns>
        <columns>Status__c</columns>
        <columns>OWNER.ALIAS</columns>
        <columns>LAST_UPDATE</columns>
        <columns>LAST_ACTIVITY</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>Target__c</field>
            <operation>equals</operation>
            <value>GLOBAL - PreProd</value>
        </filters>
        <label>Pre-Prod Slots</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Production_Slots_In_Progress</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>Status__c</columns>
        <columns>Project__c</columns>
        <columns>Release__c</columns>
        <columns>Release_Manager__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Approval Requested,Approved for Deployment,Locked for Deployment</value>
        </filters>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Production Slots In Progress</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Sandbox_Slots_In_Progress</fullName>
        <columns>NAME</columns>
        <columns>Deployment_Label__c</columns>
        <columns>Deployment_Date__c</columns>
        <columns>Source__c</columns>
        <columns>Target__c</columns>
        <columns>Status__c</columns>
        <columns>Project__c</columns>
        <columns>Release__c</columns>
        <columns>Release_Manager__c</columns>
        <columns>Secondary_Deployment_Lead__c</columns>
        <columns>OWNER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>Approval Requested,Approved for Deployment,Locked for Deployment</value>
        </filters>
        <filters>
            <field>Is_Production__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>Sandbox Slots In Progress</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>DR-{00000}</displayFormat>
        <label>DR Name</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Deployment Requests</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Deployment_Label__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Deployment_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Source__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Target__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Deployment_Label__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Deployment_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Source__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Target__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Release__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>Deployment_Date__c</lookupFilterFields>
        <lookupFilterFields>Deployment_Label__c</lookupFilterFields>
        <lookupFilterFields>Release__c</lookupFilterFields>
        <lookupFilterFields>Source__c</lookupFilterFields>
        <lookupFilterFields>Target__c</lookupFilterFields>
        <lookupFilterFields>Status__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Deployment_Label__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Deployment_Label__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Deployment_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Release__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Source__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Target__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Cannot_Export_package_if_not_approved</fullName>
        <active>true</active>
        <description>This Validation Rule is to prevent user from being able to export package without approval</description>
        <errorConditionFormula>AND(ISPICKVAL(Status__c,&quot;Locked for Deployment&quot;), 
NOT(ISPICKVAL(PRIORVALUE(Status__c),&quot;Approved for Deployment&quot;)))</errorConditionFormula>
        <errorMessage>You can&apos;t export package without the slot being approved</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Cannot_Update_to_Cancelled_if_deployed</fullName>
        <active>true</active>
        <description>This to prevent user from cancelled the slot if changes have deployed</description>
        <errorConditionFormula>AND(ISPICKVAL(Status__c,&quot;Cancelled&quot;), 
OR(ISPICKVAL(PRIORVALUE(Status__c),&quot;Deployed to DEV&quot;),ISPICKVAL(PRIORVALUE(Status__c),&quot;Deployed to QA&quot;),ISPICKVAL(PRIORVALUE(Status__c),&quot;Deployed to UAT&quot;),ISPICKVAL(PRIORVALUE(Status__c),&quot;Deployed to Production&quot;)))</errorConditionFormula>
        <errorMessage>You can&apos;t cancelled the slot if the changes have been deployed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Cannot_have_different_Platforms</fullName>
        <active>true</active>
        <errorConditionFormula>Platform__c  &lt;&gt;  TEXT(Target__r.Platform__c )</errorConditionFormula>
        <errorMessage>Target and Source Platforms does not match, please select a Target environment in the same Platform</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>If_Production_Set_Release_Type</fullName>
        <active>true</active>
        <description>Case 01250947</description>
        <errorConditionFormula>AND(
 Is_Production__c,
 ISBLANK(TEXT(Release_Type__c)), 
 CASE(TEXT(Status__c),
  &apos;Approval Requested&apos;,1,
  &apos;Not Started&apos;,1,
  &apos;Approved for Deployment&apos;,1,
  0
 ) = 1
)</errorConditionFormula>
        <errorDisplayField>Release_Type__c</errorDisplayField>
        <errorMessage>Please complete.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validation_Rule_for_Project_and_Release</fullName>
        <active>true</active>
        <description>This VR is to prompt user to enter value on Project / Release</description>
        <errorConditionFormula>AND(ISBLANK(Project__c) , ISBLANK( Release__c ))</errorConditionFormula>
        <errorMessage>At least Project or Release must be specified.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Deploy_Now</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Deploy Now</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>var currStatus = &apos;{!Deployment_Request__c.Status__c}&apos;;
var deployUrl = &apos;http://usmkpcrm01.mck.experian.com:8080/job/ForceDeploymentByID/buildWithParameters?token=23095763290525823&amp;DEPLOYMENT_REQUEST_ID={!Deployment_Request__c.Id}&amp;cause=Requested%20by%20{!$User.FirstName}%20{!$User.LastName}&apos;;

if (currStatus !== &apos;Locked for Deployment&apos;) {
  alert(&apos;Deployment request must be Locked for Deployment&apos;);
}
else {
  window.open(deployUrl,&apos;_blank&apos;,&quot;width=800,height=400&quot;,false)
}</url>
    </webLinks>
    <webLinks>
        <fullName>Export_Package</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Export Package</masterLabel>
        <openType>replace</openType>
        <page>ExportPackageXML</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>Validate_Now</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Validate Now</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>var currStatus = &apos;{!Deployment_Request__c.Status__c}&apos;;
var deployUrl = &apos;http://usmkpcrm01.mck.experian.com:8080/job/ForceValidationByID/buildWithParameters?token=9273509273509732553566&amp;DEPLOYMENT_REQUEST_ID={!Deployment_Request__c.Id}&amp;cause=Requested%20by%20{!$User.FirstName}%20{!$User.LastName}&apos;;

if (currStatus !== &apos;Locked for Deployment&apos;) {
  alert(&apos;Deployment request must be Locked for Deployment&apos;);
}
else {
  window.open(deployUrl,&apos;_blank&apos;,&quot;width=800,height=400&quot;,false)
}</url>
    </webLinks>
    <webLinks>
        <fullName>View_Manual_Steps</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>View Manual Steps</masterLabel>
        <openType>replace</openType>
        <page>DeploymentRequestManualSteps</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
