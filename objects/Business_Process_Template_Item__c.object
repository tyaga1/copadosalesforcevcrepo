<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Assign_To_Queue_when_Ready__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Assign To Queue when Ready</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Business_Hours__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Standard Business hours. Will help calculate # of business days on the instance of the business process.</description>
        <externalId>false</externalId>
        <label>Business Hours</label>
        <referenceTo>BusinessHours</referenceTo>
        <relationshipName>Business_Process_Template_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Process_Template__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Business Process Template</label>
        <referenceTo>Business_Process_Template__c</referenceTo>
        <relationshipLabel>Business Process Template Items</relationshipLabel>
        <relationshipName>Business_Process_Template_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT( Business_Process_Template__r.Business_Unit__c )</formula>
        <label>Business Unit</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CSDA_Service_Type__c</fullName>
        <description>Service Type of business process template item specific to CSDA BU</description>
        <externalId>false</externalId>
        <inlineHelpText>Service Type of business process template item specific to CSDA BU</inlineHelpText>
        <label>CSDA Service Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>Account Maintenance</fullName>
                    <default>false</default>
                    <label>Account Maintenance</label>
                </value>
                <value>
                    <fullName>Additional Subcode</fullName>
                    <default>false</default>
                    <label>Additional Subcode</label>
                </value>
                <value>
                    <fullName>Address Change</fullName>
                    <default>false</default>
                    <label>Address Change</label>
                </value>
                <value>
                    <fullName>Admin Report Pulled</fullName>
                    <default>false</default>
                    <label>Admin Report Pulled</label>
                </value>
                <value>
                    <fullName>AS Subcodes/Metronet</fullName>
                    <default>false</default>
                    <label>AS Subcodes/Metronet</label>
                </value>
                <value>
                    <fullName>Authenticated</fullName>
                    <default>false</default>
                    <label>Authenticated</label>
                </value>
                <value>
                    <fullName>Branch Add</fullName>
                    <default>false</default>
                    <label>Branch Add</label>
                </value>
                <value>
                    <fullName>Cancel Account</fullName>
                    <default>false</default>
                    <label>Cancel Account</label>
                </value>
                <value>
                    <fullName>Company ID Maintenance</fullName>
                    <default>false</default>
                    <label>Company ID Maintenance</label>
                </value>
                <value>
                    <fullName>Credit Issued</fullName>
                    <default>false</default>
                    <label>Credit Issued</label>
                </value>
                <value>
                    <fullName>Credit Request</fullName>
                    <default>false</default>
                    <label>Credit Request</label>
                </value>
                <value>
                    <fullName>Fee-Physical Inspection</fullName>
                    <default>false</default>
                    <label>Fee-Physical Inspection</label>
                </value>
                <value>
                    <fullName>File Research</fullName>
                    <default>false</default>
                    <label>File Research</label>
                </value>
                <value>
                    <fullName>Lead Follow-Up(to Siebel User)</fullName>
                    <default>false</default>
                    <label>Lead Follow-Up(to Siebel User)</label>
                </value>
                <value>
                    <fullName>Maintenance</fullName>
                    <default>false</default>
                    <label>Maintenance</label>
                </value>
                <value>
                    <fullName>Membership Questions</fullName>
                    <default>false</default>
                    <label>Membership Questions</label>
                </value>
                <value>
                    <fullName>MEM-File Submitted for Scan</fullName>
                    <default>false</default>
                    <label>MEM-File Submitted for Scan</label>
                </value>
                <value>
                    <fullName>MEM-Investigation</fullName>
                    <default>false</default>
                    <label>MEM-Investigation</label>
                </value>
                <value>
                    <fullName>MEM-NACM GL Inspection</fullName>
                    <default>false</default>
                    <label>MEM-NACM GL Inspection</label>
                </value>
                <value>
                    <fullName>MEM-On Desk</fullName>
                    <default>false</default>
                    <label>MEM-On Desk</label>
                </value>
                <value>
                    <fullName>MEM-Pending Decision</fullName>
                    <default>false</default>
                    <label>MEM-Pending Decision</label>
                </value>
                <value>
                    <fullName>MEM-Pending Recommendation</fullName>
                    <default>false</default>
                    <label>MEM-Pending Recommendation</label>
                </value>
                <value>
                    <fullName>MEM-Pending Requirement</fullName>
                    <default>false</default>
                    <label>MEM-Pending Requirement</label>
                </value>
                <value>
                    <fullName>MEM-Physical Inspection</fullName>
                    <default>false</default>
                    <label>MEM-Physical Inspection</label>
                </value>
                <value>
                    <fullName>MEM-Physical Results Received</fullName>
                    <default>false</default>
                    <label>MEM-Physical Results Received</label>
                </value>
                <value>
                    <fullName>MEM-Post Analyst Submission</fullName>
                    <default>false</default>
                    <label>MEM-Post Analyst Submission</label>
                </value>
                <value>
                    <fullName>MEM-Received Packet</fullName>
                    <default>false</default>
                    <label>MEM-Received Packet</label>
                </value>
                <value>
                    <fullName>MEM-To Management</fullName>
                    <default>false</default>
                    <label>MEM-To Management</label>
                </value>
                <value>
                    <fullName>MEM-Welcome Letter Mailed</fullName>
                    <default>false</default>
                    <label>MEM-Welcome Letter Mailed</label>
                </value>
                <value>
                    <fullName>Merge Request</fullName>
                    <default>false</default>
                    <label>Merge Request</label>
                </value>
                <value>
                    <fullName>Metronet</fullName>
                    <default>false</default>
                    <label>Metronet</label>
                </value>
                <value>
                    <fullName>Name Change</fullName>
                    <default>false</default>
                    <label>Name Change</label>
                </value>
                <value>
                    <fullName>Ownership Change</fullName>
                    <default>false</default>
                    <label>Ownership Change</label>
                </value>
                <value>
                    <fullName>Paperwork Follow up</fullName>
                    <default>false</default>
                    <label>Paperwork Follow up</label>
                </value>
                <value>
                    <fullName>Paperwork Received</fullName>
                    <default>false</default>
                    <label>Paperwork Received</label>
                </value>
                <value>
                    <fullName>Paperwork Requested</fullName>
                    <default>false</default>
                    <label>Paperwork Requested</label>
                </value>
                <value>
                    <fullName>Paperwork Sent</fullName>
                    <default>false</default>
                    <label>Paperwork Sent</label>
                </value>
                <value>
                    <fullName>Price Change Request Completed</fullName>
                    <default>false</default>
                    <label>Price Change Request Completed</label>
                </value>
                <value>
                    <fullName>Price Change Request Received</fullName>
                    <default>false</default>
                    <label>Price Change Request Received</label>
                </value>
                <value>
                    <fullName>Pricing Execution</fullName>
                    <default>false</default>
                    <label>Pricing Execution</label>
                </value>
                <value>
                    <fullName>Pricing Follow Up</fullName>
                    <default>false</default>
                    <label>Pricing Follow Up</label>
                </value>
                <value>
                    <fullName>Pricing Question</fullName>
                    <default>false</default>
                    <label>Pricing Question</label>
                </value>
                <value>
                    <fullName>Pricing Research</fullName>
                    <default>false</default>
                    <label>Pricing Research</label>
                </value>
                <value>
                    <fullName>Pricing Type Validation</fullName>
                    <default>false</default>
                    <label>Pricing Type Validation</label>
                </value>
                <value>
                    <fullName>Primary Address Change</fullName>
                    <default>false</default>
                    <label>Primary Address Change</label>
                </value>
                <value>
                    <fullName>Product Turn On/Off</fullName>
                    <default>false</default>
                    <label>Product Turn On/Off</label>
                </value>
                <value>
                    <fullName>Profile ID AS/Metronet</fullName>
                    <default>false</default>
                    <label>Profile ID AS/Metronet</label>
                </value>
                <value>
                    <fullName>Reassignment</fullName>
                    <default>false</default>
                    <label>Reassignment</label>
                </value>
                <value>
                    <fullName>SSO Question/Issue</fullName>
                    <default>false</default>
                    <label>SSO Question/Issue</label>
                </value>
                <value>
                    <fullName>SSO Set-up</fullName>
                    <default>false</default>
                    <label>SSO Set-up</label>
                </value>
                <value>
                    <fullName>Status Change</fullName>
                    <default>false</default>
                    <label>Status Change</label>
                </value>
                <value>
                    <fullName>Subcode Info</fullName>
                    <default>false</default>
                    <label>Subcode Info</label>
                </value>
                <value>
                    <fullName>Submit Online Pricing Form</fullName>
                    <default>false</default>
                    <label>Submit Online Pricing Form</label>
                </value>
                <value>
                    <fullName>X.25 HD Created</fullName>
                    <default>false</default>
                    <label>X.25 HD Created</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CSDA_Type__c</fullName>
        <description>CSDA Type is specific to the CSDA BU</description>
        <externalId>false</externalId>
        <inlineHelpText>CSDA Type is specific to the CSDA BU</inlineHelpText>
        <label>CSDA Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Account Review</fullName>
                    <default>false</default>
                    <label>Account Review</label>
                </value>
                <value>
                    <fullName>Com-Notes</fullName>
                    <default>false</default>
                    <label>Com-Notes</label>
                </value>
                <value>
                    <fullName>Com-Phone Out</fullName>
                    <default>false</default>
                    <label>Com-Phone Out</label>
                </value>
                <value>
                    <fullName>Data Quality</fullName>
                    <default>false</default>
                    <label>Data Quality</label>
                </value>
                <value>
                    <fullName>Membership Investigation</fullName>
                    <default>false</default>
                    <label>Membership Investigation</label>
                </value>
                <value>
                    <fullName>Membership Package</fullName>
                    <default>false</default>
                    <label>Membership Package</label>
                </value>
                <value>
                    <fullName>Payment-Credit Card</fullName>
                    <default>false</default>
                    <label>Payment-Credit Card</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Default_Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Identifies the owner to assign when project work items are created.</description>
        <externalId>false</externalId>
        <label>Default Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>R00N40000001lfLNEAY</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Description</label>
        <length>500</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Expire__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, update the project work item status to &quot;Success&quot; after the time quota has been reached.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, update the project work item status to &quot;Success&quot; after the time quota has been reached.</inlineHelpText>
        <label>Expire</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>External_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <encrypted>false</encrypted>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Final_State__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates this is the final work item for the service</description>
        <externalId>false</externalId>
        <inlineHelpText>This box should be checked if this work item is the final work item for this service.  &apos;success&apos; of this work will item will change the status of the service to done.  The only &apos;successors&apos; for this work item should be loop back links.</inlineHelpText>
        <label>Final State</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Inactive__c</fullName>
        <description>This is set to true automatically when the business process template is inactivated. When inactivated, users can&apos;t apply the business process template item as an ad hoc item</description>
        <externalId>false</externalId>
        <formula>Business_Process_Template__r.Inactive__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This is set to true automatically when the business process template is inactivated. When inactivated, users can&apos;t apply the business process template item as an ad hoc item</inlineHelpText>
        <label>Inactive?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Queue__c</fullName>
        <externalId>false</externalId>
        <label>Queue</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>CSDA Contract Queue</fullName>
                    <default>false</default>
                    <label>CSDA Contract Queue</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Send_Owner_Email__c</fullName>
        <defaultValue>false</defaultValue>
        <description>When checked, the selected email notice will be sent to the task owner.</description>
        <externalId>false</externalId>
        <inlineHelpText>When checked, the selected email notice will be sent to the task owner.</inlineHelpText>
        <label>Send Owner Email</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sequence__c</fullName>
        <description>Allows Administrators to specify a sequence number that can be used to order the display of work items within a service.</description>
        <externalId>false</externalId>
        <inlineHelpText>Controls the sequence in which this item is displayed in a list of work items for t Project or Service.</inlineHelpText>
        <label>Sequence</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sub_Business_Unit__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <formula>TEXT( Business_Process_Template__r.Sub_Business_Unit__c )</formula>
        <label>Sub Business Unit</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time_Quota__c</fullName>
        <description>The number of calendar days from &quot;task ready&quot; in which this task should be completed.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of calendar days from &quot;task ready&quot; in which this task should be completed.</inlineHelpText>
        <label>Time Quota</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Update_Status_When__c</fullName>
        <externalId>false</externalId>
        <label>Update to Ready When</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Service is created</fullName>
                    <default>false</default>
                    <label>Service is created</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Wait_On_All_Preds__c</fullName>
        <defaultValue>true</defaultValue>
        <description>This work item will either wait on all predecessors to complete, or will execute as soon as one of the predecessors is complete.  This condition applies to non-loop-back links only.</description>
        <externalId>false</externalId>
        <inlineHelpText>This work item will either wait on all predecessors to complete, or will execute as soon as one of the predecessors is complete.  This condition applies to non-loop-back links only.</inlineHelpText>
        <label>Wait On All Preds</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Work_Item_Id__c</fullName>
        <displayFormat>WI-{0}</displayFormat>
        <externalId>false</externalId>
        <label>Work Item Id</label>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <label>Business Process Template Item</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Business_Process_Template__c</columns>
        <columns>Sequence__c</columns>
        <columns>Final_State__c</columns>
        <columns>Queue__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Business Process Template Item</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Business Process Template Items</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Business_Process_Template__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Default_Owner__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Queue__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Sequence__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Final_State__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Business_Unit__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Sub_Business_Unit__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Business_Process_Template__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Default_Owner__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Queue__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Sequence__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Final_State__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Business_Unit__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Sub_Business_Unit__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>Business_Process_Template__c</lookupFilterFields>
        <lookupFilterFields>Business_Unit__c</lookupFilterFields>
        <lookupFilterFields>Sub_Business_Unit__c</lookupFilterFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Business_Process_Template__c</searchFilterFields>
        <searchFilterFields>Default_Owner__c</searchFilterFields>
        <searchFilterFields>Queue__c</searchFilterFields>
        <searchFilterFields>Sequence__c</searchFilterFields>
        <searchFilterFields>Final_State__c</searchFilterFields>
        <searchFilterFields>Business_Unit__c</searchFilterFields>
        <searchFilterFields>Sub_Business_Unit__c</searchFilterFields>
        <searchResultsAdditionalFields>Business_Process_Template__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Default_Owner__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Queue__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Sequence__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Final_State__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Business_Unit__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Sub_Business_Unit__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Prevent_Default_Owner_And_Queue</fullName>
        <active>true</active>
        <description>A user cannot have the ability to choose both a Default Owner and a Queue.  A user can either (1) Choose a Default Owner, OR (2) Choose a Queue OR (3) NOT choose a Default Owner and Queue</description>
        <errorConditionFormula>NOT( $Setup.IsDataAdmin__c.IsDataAdmin__c) &amp;&amp;
NOT(IsBlank(  TEXT(Queue__c ))) &amp;&amp;
NOT(IsBlank( Default_Owner__c ))</errorConditionFormula>
        <errorMessage>Do not select a Default Owner and a Queue.  You can either (1) Choose a Default Owner, OR (2) Choose a Queue OR (3) NOT choose a Default Owner and Queue</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
