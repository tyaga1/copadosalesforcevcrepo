<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <articleTypeChannelDisplay>
        <articleTypeTemplates>
            <channel>App</channel>
            <template>Toc</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Prm</channel>
            <template>Toc</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Csp</channel>
            <template>Toc</template>
        </articleTypeTemplates>
        <articleTypeTemplates>
            <channel>Pkb</channel>
            <template>Toc</template>
        </articleTypeTemplates>
    </articleTypeChannelDisplay>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Troubleshooting Article Type</description>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <fields>
        <fullName>Downloads__c</fullName>
        <description>External files that are relevant to solving the problem</description>
        <externalId>false</externalId>
        <inlineHelpText>Please add files that are relevant to solving the problem</inlineHelpText>
        <label>Downloads</label>
        <required>false</required>
        <type>File</type>
    </fields>
    <fields>
        <fullName>Error_Message__c</fullName>
        <description>Error message (if applicable) for troubleshooting article for user to enter the error message.</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Please enter the Error message you see if applicable.</inlineHelpText>
        <label>Error Message</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Product_Version__c</fullName>
        <description>Names and version numbers of products that are impacted (e.g. Pro Web 5.65)</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Names and version numbers of products that are impacted (e.g. Pro Web 5.65)</inlineHelpText>
        <label>Product &amp; Version #</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Related_Resources__c</fullName>
        <description>Links to articles or online resources that are relevant to solving the problem</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <inlineHelpText>Enter links to articles or online resources that are relevant to solving the problem</inlineHelpText>
        <label>Related Resources</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Solution_Internal__c</fullName>
        <description>Section/Header that contains multiple options.</description>
        <externalId>false</externalId>
        <label>Solution (Internal)</label>
        <length>32768</length>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Solution__c</fullName>
        <description>Section/Header that contains multiple options.</description>
        <externalId>false</externalId>
        <label>Solution</label>
        <length>32768</length>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Steps_to_Reproduce__c</fullName>
        <description>Details about steps taken / conditions required to reproduce the behavior.</description>
        <externalId>false</externalId>
        <inlineHelpText>Please enter detail about the steps you have taken so we can try to reproduce what you are experiencing/seeing.</inlineHelpText>
        <label>Steps to Reproduce</label>
        <length>32768</length>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <label>Troubleshooting</label>
    <pluralLabel>Troubleshooting</pluralLabel>
    <validationRules>
        <fullName>Required_Fields</fullName>
        <active>true</active>
        <description>Troubleshooting article requires the following fields to be completed:
-Summary
-Product Version
-Steps to reproduce
-Solution (Internal)
-Solution (External)</description>
        <errorConditionFormula>OR(
   LEN (Summary)=0,
   LEN (Product_Version__c)=0,
   LEN (Steps_to_Reproduce__c)=0,
   LEN (Solution_Internal__c)=0,
   LEN (Solution__c)=0
   )</errorConditionFormula>
        <errorMessage>Make sure all required fields are filled-in:
-Title
-Summary
-Product Version
-Steps to reproduce
-Solution (Internal)
-Solution (External)</errorMessage>
    </validationRules>
</CustomObject>
