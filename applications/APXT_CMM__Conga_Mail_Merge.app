<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>APXT_CMM__About_Conga_Mail_Merge</defaultLandingTab>
    <description>Conga Mail Merge enables salesforce.com customers to simplify mass mailings by generating hundreds of printable letters, labels and envelopes all at once.</description>
    <formFactors>Large</formFactors>
    <label>Conga Mail Merge</label>
    <tab>APXT_CMM__About_Conga_Mail_Merge</tab>
    <tab>APXT_CMM__Conga_Mail_Merge</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
