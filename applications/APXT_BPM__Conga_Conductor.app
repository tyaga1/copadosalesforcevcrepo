<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>APXT_BPM__Conductor__c</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Conga Conductor</label>
    <tab>APXT_BPM__About_Conga_Conductor</tab>
    <tab>APXT_BPM__Conductor__c</tab>
    <tab>APXT_BPM__Conductor_Setup</tab>
    <tab>APXT_BPM__Conductor_Dashboard</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
