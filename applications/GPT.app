<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Product_Master__c</defaultLandingTab>
    <description>App to Access Product Master</description>
    <formFactors>Large</formFactors>
    <label>GPT</label>
    <tab>standard-report</tab>
    <tab>Product_Master__c</tab>
    <tab>ProductMaster_Manager__c</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
