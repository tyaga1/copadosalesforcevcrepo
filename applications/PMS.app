<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Project Management System</description>
    <formFactors>Large</formFactors>
    <label>SMC</label>
    <tab>Project__c</tab>
    <tab>Component__c</tab>
    <tab>Release__c</tab>
    <tab>Sprint__c</tab>
    <tab>Story__c</tab>
    <tab>Test_Case__c</tab>
    <tab>Issue__c</tab>
    <tab>Metadata_Component__c</tab>
    <tab>Deployment_Request__c</tab>
    <tab>standard-report</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
