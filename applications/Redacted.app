<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>The redacted Salesforce content filtering system.</description>
    <formFactors>Large</formFactors>
    <label>Redacted</label>
    <tab>Redacted_Filter__c</tab>
    <tab>Redacted_Filter_Match__c</tab>
    <tab>standard-report</tab>
    <tab>Lesson_Learned__c</tab>
</CustomApplication>
