({
    doInit : function(component, event, helper) {
        var translatedLabels = [];
        translatedLabels.push({
            label : $A.get("$Label.c.CustCom_Footer_Legal"),
            link  : "http://www.experian.com/corporate/legalterms.html"
        });
        translatedLabels.push({
            label : $A.get("$Label.c.CustCom_Footer_Privacy"),
            link  : "http://www.experian.com/privacy"
        });
        translatedLabels.push({
            label : $A.get("$Label.c.CustCom_Footer_Press"),
            link  : "http://press.experian.com"
        });
        translatedLabels.push({
            label : $A.get("$Label.c.CustCom_Footer_About"),
            link  : "http://www.experian.com/corporate/about-experian.html?intcmp=hpft_ae"
        });
        translatedLabels.push({
            label : $A.get("$Label.c.CustCom_Footer_Global"),
            link  : "http://www.experianplc.com/en/about-experian/worldwide-locations.aspx"
        });

        component.set("v.translatedLinkLabels", translatedLabels);
    }
})