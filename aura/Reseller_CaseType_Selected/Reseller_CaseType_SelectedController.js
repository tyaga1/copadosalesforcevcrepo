({
   doInit : function(component,event,helper) {
        var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
         
        var caseType = getUrlParameter('type');
        var caseID = getUrlParameter('id');

        component.set("v.caseId", caseID);
        component.set("v.caseType", caseType);

        if (caseID != undefined) {
            console.log('test1');
            $A.util.addClass(component.find('InputSelectDynamic'), "slds-hide"); 
            console.log('hidden');
        }  else if (caseType === undefined) {
            console.log('test2 ' + caseType);
            caseType = 'BIS';            
        }
 
        var apiName = component.get("v.actionId");
        var opts =[];
        var myMap = {};
        myMap['BIS'] = 'Create_BIS_Case';
        myMap['CIS'] = 'Create_CIS_Case';

        for (var key in myMap) {
            console.log('1 ' + key + ' ' + caseType);
            if (caseType === key) {
                opts.push({ class: "optionClass", label: key, value: key, selected: "true"});
            } else {
            	opts.push({ class: "optionClass", label: key, value: key});
            }
        }
        
        var action = component.get("c.getProfileName");
       
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.profileName", response.getReturnValue());
				console.log('getProfileName :'+response.getReturnValue());
                if (component.find("InputSelectDynamic") != null)
                {
                    component.find("InputSelectDynamic").set("v.options", opts);
                }
            }
        });
        
        $A.enqueueAction(action);
    },
        

    
    changeSelect : function(component, event, helper) {
        console.log("hello");
        var selected = component.find("InputSelectDynamic").get("v.value");
        console.log("selected " + selected);
        /*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": '/case-create?type=' + selected
        });
        urlEvent.fire();*/
        window.location.href = '../s/newcase?type=' + selected;
    }
})