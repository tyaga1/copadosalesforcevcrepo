({
	navigateToDetail : function(environmentType, dataFromMainPage, documentParam, detailRedirectionPage) {
        //navigate in Lightning, transform params with event.
        if(environmentType == "Standard"){
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                isredirect: true,
                componentDef: "seismic:SeismicEmbeddedApplicationDetail",
                componentAttributes: {
                    "dataFromMainPage": dataFromMainPage,
                    "documentParam" : documentParam
                },
            });
            evt.fire();
        //navigate in Community, transform params in URL.
        }else{
            var transferData = {
                "dataFromMainPage": dataFromMainPage,
                "documentParam" : documentParam
            };
            //var address = '/' + detailRedirectionPage + '#' + $.base64.encode(JSON.stringify(transferData));
            /*var pageName = detailRedirectionPage.substring(detailRedirectionPage.lastIndexOf('/') + 1, detailRedirectionPage.length);
            var address = '/' + pageName + '#' + $.base64.encode(JSON.stringify(transferData));
            var evt = $A.get("e.force:navigateToURL");
            evt.setParams({
                url: address,
                isredirect: true
            });
            evt.fire();*/
            var address = detailRedirectionPage + '#' + $.base64.encode(JSON.stringify(transferData));
            window.location.replace(address);
        }
	},
    
    getAppData : function(component) {
        var action = component.get("c.getAppInfo");
        var sObjectType = component.get('v.sObjectName');
        var sObjectId = component.get('v.recordId');
        action.setParams({sObjectId: sObjectId, sObjectType: sObjectType, returnUrl: window.location.href});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                var dataString = response.getReturnValue();
                component.set('v.dataString', dataString);
            }else if(state === "ERROR") {
                var error = response.getError();
                console.log(error)
            }else {
                console.log("unknown error");
            }
        });
        $A.enqueueAction(action);
    },
    
    setIframeHeight : function(component){
        var designHeight = component.get("v.appContainerHeight");
        var appContainerHeight= "";
        var appContainerWidth= "100%";
        if(designHeight=="100%"){
            appContainerHeight = "100%";
        }
        else{
            appContainerHeight = designHeight + "px";
        }
        var appContainerStyle = "border: 1px solid rgb(216, 221, 230);border-radius: 0 0 .25rem .25rem;height:" + appContainerHeight + ";width:" + appContainerWidth +";";
        component.set("v.appContainerStyle", appContainerStyle);
    },
    
    navigateBack : function(component){
        var transferData = JSON.parse($.base64.decode(window.location.hash.substring(1, window.location.hash.length)));
        var dataFromMainPage = transferData.dataFromMainPage;
        var mainPageDataJSON = JSON.parse(dataFromMainPage);
        var backURL = mainPageDataJSON.backUrl;
        var evt = $A.get("e.force:navigateToURL");
        evt.setParams({
            url: backURL,
            isredirect: true
        });
        evt.fire();
    }
})