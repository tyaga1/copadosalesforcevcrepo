({
	doInit : function(component, event, helper) {
		var action = component.get("c.getPrivateDocs"); 
        //alert('charlie' + component.get("v.docType"));
        action.setParams({"docType":component.get("v.docType")});       
        
        action.setCallback(this, function(response) {
            console.log("inside getPrivateDocs action");
            var state = response.getState(); 
        
            if (state === "SUCCESS") {   
                console.log("Success docs");
                var returnDocs = response.getReturnValue();
                var arrayOfMapKeys = [];
                console.log(returnDocs);
                component.set("v.confInfoAttachment", returnDocs);
                console.log('charlie test' + component.get("v.confInfoAttachment"));

                for(var singleKey in returnDocs)
                {
                    arrayOfMapKeys.push({value:returnDocs[singleKey], key:singleKey});
                }

                component.set("v.confInfoMapKeys", arrayOfMapKeys);
                
            } else {
                console.log("error");
            }
                
        });
         $A.enqueueAction(action);
	}
})