({
	doInit : function(component, event, helper) {
        console.log('init');
		var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
        
        var caseId = getUrlParameter('id');
        //console.log('test ' + testId);
        if (caseId === undefined) {
            component.set("v.caseId", '');
            //console.log('test un ' + testId);
        } else {
            component.set("v.caseId", '?id='+caseId);
            //console.log('test no ' + testId);
        }

        alert(component.get("v.caseId"));
	}
})