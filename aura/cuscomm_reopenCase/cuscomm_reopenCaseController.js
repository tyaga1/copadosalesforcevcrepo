({
	doInit : function(component, event, helper) {     
        var button = component.find('caseReopen'); 
		var caseIDvar = component.get("v.caseID");  
        console.log("doInit");
        
        var action = component.get("c.isResolved");
        action.setParams({
            caseID : caseIDvar
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
           
            if (state === "SUCCESS") {
                console.log("Success true");
                if (response.getReturnValue() == false) {
                $A.util.addClass(button, "slds-hide");
                }
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log("Problem encountered");
            }
        });

        $A.enqueueAction(action);
        
	}, 
    
    reopen : function(component, event, helper) {     
        //var reopenBtn = event.getSource();
        //var reopenLabel = reopenBtn.get("v.label");   
		var caseIDvar = component.get("v.caseID");
        
        function reloadPage() {
            console.log("reload?");
            location.reload();
        }
        
        var action = component.get("c.changeStatus");
        action.setParams({
            caseID : caseIDvar
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
           
            if (state === "SUCCESS") {
                //$A.get('e.force:refreshView').fire();
                reloadPage();
                console.log("Success");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                console.log("Problem encountered");
            }
        });

        $A.enqueueAction(action);
	}
})