({ 
    prepareBasicData : function(component, event, helper){
        if(!component.isValid())return;
        helper.setIframeHeight(component);
        helper.getAppData(component);
    },
    
    jsLoaded : function(component, event, helper){
        if(!component.isValid())return;
        initEmbeddedApp(component, helper);
        
        function initEmbeddedApp(component, helper){
            var timeCounter = 30;
            checkData();
            
            function checkData(){
                timeCounter--
                //handle error in community builder;
                if(!component.isValid())return;
                
                var dataString = component.get('v.dataString');
                if(!!dataString){
                    getParamsReadyToInit(dataString);
                }else if(timeCounter > 0){
                    setTimeout(function(){
                        checkData();
                    }, 500);
                }else{
                    alert('Init data failed, please refresh the page.');
                }
            }
        }
        
        function getSearchObj(){
            var queryString = window.location.search.length > 0 ? window.location.search.substr(1) : '';
            var args = {};  
            var items = queryString.length > 0 ? queryString.split('&') : [];
            var item = null, name = null, value = null, i = 0, len = items.length;
    
            for(i = 0;i < len; i++){
                item = items[i].split('=');
                name = decodeURIComponent(item[0]);
                value = decodeURIComponent(item[1]);
    
                if(name.length){
                    args[name] = value;
                }
            }
    
            return args;
        }
        
        function getParamsReadyToInit(dataString){
            var documentParams = getSearchObj();
            var dataJSON = JSON.parse(dataString);
            var contextInfo = dataJSON[0];
            var credentialInfo = dataJSON[1];
            
            var applicationName = component.get('v.applicationName');
            var isFullPage = true;
            var notShowGoBack = true;
            var sObjectId = component.get('v.recordId');
            var sObjectType = component.get('v.sObjectName');
            var detailInfo = {
                tabName: '',
                type: 'doccenter',
                viewState: '',
                orderBy: '',
                showFullPage: true,
                showGoBack: true,
                documentId: encodeURIComponent(documentParams.documentId),
                profileVersionId: documentParams.profileVersionId
            };
            
            loadSeismicApp(contextInfo, credentialInfo, sObjectType, sObjectId, applicationName, detailInfo, isFullPage, notShowGoBack);
        }
        
        function loadSeismicApp(contextInfo, credentialInfo, sObjectType, sObjectId, applicationName, detailInfo, isFullPage, notShowGoBack){
            var iframeDom = document.getElementById(component.getGlobalId()+'_seismicAppContainer');
            iframeDom.setAttribute('allowfullscreen', true);
            var config = {
                isFullPage: isFullPage,
                currentId: sObjectId,
                notShowGoBack: notShowGoBack
            };
            var context = {
                Salesforce: {
                    ServerBaseUrl: contextInfo.SFBaseUrl,
                    SessionId: contextInfo.SessionId,
                    ObjectType: sObjectType,
                    ObjectId: sObjectId,
                    ObjectName: contextInfo.SObjectName,
                    OrganizationId: contextInfo.SFOrganizationId,
                    UserId: credentialInfo.UserId
                }
            };
            var loginOptions = {
                RememberMeToken: credentialInfo.RememberMeToken,
                DisableRememberMe : credentialInfo.DisableRememberMe,
                DisableSSOAutoRedirection: credentialInfo.DisableSSOAutoRedirection,
                SSOState: credentialInfo.SSOState,
                CredentialsKey: credentialInfo.CredentialsKey
            };
            Seismic.IntegratedSDK.loadEmbeddedApplication(iframeDom, credentialInfo.ClientType, applicationName, contextInfo.ServerBaseUrl, detailInfo, config, loginOptions, context);
        }  
    } 
    
})