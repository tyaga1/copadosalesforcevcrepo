/**=====================================================================
 * Name: FeedItemTrigger
 * Description: Trigger for FeedItem object                 
 * Created Date: Aug 14th, 2017
 * Created By: Mauricio Murillo
 * 
 * Date Modified                Modified By                  Description of the update
**=====================================================================*/

trigger FeedItemTrigger on FeedItem (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

     if (TriggerState.isActive(Constants.FEED_ITEM_TRIGGER)) {
         
        if (Trigger.isBefore && Trigger.isDelete) {
            FeedItemTriggerHandler.beforeDelete(Trigger.old, IsDataAdmin__c.getInstance().IsDataAdmin__c);
        }
         
     }  
    
}