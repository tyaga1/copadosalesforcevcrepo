/**=====================================================================
 * Experian
 * Name: DeploymentRequestTrigger 
 * Description: Trigger on Deployment Request object
 *
 * Created Date: November 9th, 2015
 * Created By: Nur Azlini (Experian)
 *
 * Date Modified            Modified By              Description of the update
 ======================================================================*/
trigger DeploymentRequestTrigger on Deployment_Request__c (after update) {
  
        // Before insert call
    if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
        if (trigger.isAfter && trigger.isUpdate) {
      	system.debug('***executed-----'+Trigger.newMap);
      	DeploymentRequestTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    	}
    }
        
}