/*******************
Created BY : Richard Joseph.
Created Date: Jun 20th 2016.
Desc: Trigger  for Deal.
Change Log:

*************/
trigger DealTrigger on Deal__c (Before Insert, Before update) {
    
    if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
       if (Trigger.isBefore && Trigger.isInsert) {
           DealTriggerHandler.beforeInsert(Trigger.new);
       }else
       if (Trigger.isBefore && Trigger.isUpdate) {
           DealTriggerHandler.beforeUpdate(Trigger.NewMap, Trigger.oldMap);
       }
      
    }

}