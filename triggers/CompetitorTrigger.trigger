/**=====================================================================
 * Appirio, Inc
 * Trigger Name : CompetitorTrigger
 * Handler Class: CompetitorTriggerHandler  
 * Reference    : T-291592
 * Created Date : Jul 04th, 2014
 * Created By   : Sonal Shrivastava (Appirio JDC)
 * Date Modified                Modified By                  Description of the update
 * Jul 11th, 2014               Arpita Bose(Appirio)         I-120496: Added method in beforeInsert, beforeUpdate to
 *                                                           update Competitor Name with relevant Account Name
 * Jul 11th, 2014               Arpita Bose(Appirio)         I-120496: Commented the code as the requirement is changed as per the chatter
 * Oct 27th, 2015               Paul Kissick                 Case 00617277: Added after delete
 * Mar 23rd, 2016               UC Innovation                Case 01861351 : To order the Competitor Names on the Opty in asc order. Remove workflow Rule and do it in trigger
 * Mar 24th, 2016               Sadar Yacob                  Case 01861351 : We dont need 2 methods to handle the new  list and new map for afterInsert; hence merged into same method. 
 =====================================================================*/
trigger CompetitorTrigger on Competitor__c (after insert, before delete, before insert, before update, after delete, after update) {
  
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {
    
    if (Trigger.isBefore && Trigger.isInsert) {
      // I-120496: Commented this code as the requirement is changed
      // CompetitorTriggerHandler.beforeInsert(Trigger.new);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
      // I-120496: Commented this code as the requirement is changed
      // CompetitorTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
    if (Trigger.isBefore && Trigger.isDelete) {
      CompetitorTriggerHandler.beforeDelete(Trigger.oldMap);
    }
    if (Trigger.isAfter && Trigger.isInsert) {
      CompetitorTriggerHandler.afterInsert(Trigger.new,Trigger.newMap);
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
      CompetitorTriggerHandler.afterUpdate(Trigger.newMap);
    }
    if (Trigger.isAfter && Trigger.isDelete) {
      CompetitorTriggerHandler.afterDelete(Trigger.oldMap);
    }
    if (Trigger.isAfter && Trigger.isUnDelete) {
      // Nothing
    }
    
  }
  
}