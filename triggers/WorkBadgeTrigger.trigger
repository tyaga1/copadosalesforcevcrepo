/**=====================================================================
 * Name: WorkBadgeTrigger
 * Description: Trigger on WorkBadge
 * Date: March 29th 2016
 * Before Insert : To Populate recepients manager in Badge recevied 
 **/                               

trigger WorkBadgeTrigger on WorkBadge  (Before Insert) {

if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false){    
if (Trigger.isBefore && Trigger.isInsert) {
        WorkBadgeTriggerHandler.beforeInsert(Trigger.new);        
      }

}
}