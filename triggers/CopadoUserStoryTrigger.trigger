/**=====================================================================
 * Experian, Inc
 * Name: CopadoUserStoryTrigger
 * Description: US-0000026                
 * Created Date: Jul 26th, 2017
 * Created By: Diego Olarte (Experian)
 * 
 * Date Modified                Modified By                  Description of the update
 * 
  =====================================================================*/

trigger CopadoUserStoryTrigger on copado__User_Story__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
  if (TriggerState.isActive(Constants.COPADO_USER_STORY_TRIGGER)) {
    
    if (Trigger.isBefore && Trigger.isInsert) {
      CopadoUserStoryTriggerHandler.beforeInsert(Trigger.new, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
    if (Trigger.isBefore && Trigger.isUpdate) {
      CopadoUserStoryTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    } 
    if (Trigger.isBefore && Trigger.isDelete) {
        CopadoUserStoryTriggerHandler.beforeDelete(Trigger.old, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }  
    
    if (Trigger.isAfter && Trigger.isInsert) {
      CopadoUserStoryTriggerHandler.afterInsert(Trigger.new, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
    if (Trigger.isAfter && Trigger.isUpdate) {
      CopadoUserStoryTriggerHandler.afterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap, IsDataAdmin__c.getInstance().IsDataAdmin__c);
    }
  }
  
}