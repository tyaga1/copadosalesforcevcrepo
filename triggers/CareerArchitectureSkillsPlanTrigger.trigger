/**=====================================================================
 * Experian
 * Name: CareerArchitectureSkillsPlanTrigger
 * Description: Trigger on Career_Architecture_Skills_Plan__c
 * Created Date: Jul. 19th 2017
 * Created By: James Wills
 * Date Modified      Modified By           Description of the update
 * July 28th 2017     Alexander McCall      Added CASkillsPlanTriggerHandler.afterInsert/update method(s)
 * August 31st 2017   Malcolmn Russell      Added AfterDelete
 
=====================================================================*/
trigger CareerArchitectureSkillsPlanTrigger on Career_Architecture_Skills_Plan__c (after insert, after update,after delete) {
  
  if (IsDataAdmin__c.getInstance().IsDataAdmin__c == false) {

      //After Insert
      if (Trigger.isAfter && Trigger.isInsert) {
        //CASkillsPlanTriggerHandler.afterInsert(Trigger.new);
        CASkillsPlanTriggerHandler.afterInsert(Trigger.new);
      }

      //After Update
      if (Trigger.isAfter && Trigger.isUpdate) {
        //CASkillsPlanTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        CASkillsPlanTriggerHandler.afterUpdate(Trigger.new);
      }
      
       //After Delete
      if (Trigger.isAfter && Trigger.isDelete) {
        //CASkillsPlanTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        CASkillsPlanTriggerHandler.afterDelete(Trigger.old);
      }

  }
  
}