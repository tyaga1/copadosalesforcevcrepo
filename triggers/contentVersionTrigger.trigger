trigger contentVersionTrigger on ContentVersion (before insert, before update, after insert, after update, after delete) {
    if (Trigger.isAfter && Trigger.isInsert) {
         ContentVersion_TriggerHandler.LibraryMemberAlerts(trigger.new);                                            
    }
}