<!--
/**=====================================================================
 * Name: AccountPlanCurrentProviders
 * Description: This is the AccountPlanCurrentProviders VF page.
 *              It is referenced from the AccountPlanCompetitionTab VF page.
 * Created Date: July 19th, 2016
 * Created By: James Wills
 * 
 *  Date Modified      Modified By         Description of the update
 *  July 19th, 2016    James Wills         Case: 01061020 - AP - Competitors (Current Providers)
 *  Oct. 27th, 2016    James Wills         Case #01983126 - Added Custom Labels and Field Labels
 * =====================================================================*/
-->
<apex:page standardController="Account_Plan__c" extensions="AccountPlanControllerExtension,AccountPlan_Competitors_Controller">  
  
  <apex:form id="frm">
   
    <apex:sectionHeader title="{!$ObjectType.Account_Plan__c.label}" subtitle="{!Account_Plan__c.Name}"/> 
    <apex:commandLink action="{!backToAccountPlan}">Account Plan: {!Account_Plan__c.Name}</apex:commandlink>

    <apex:pageBlock id="accountPlanCurrentProvidersPageBlock" title="Account Plan Current Providers">
      <apex:actionFunction name="editAccountPlanCurrentProvider_AF" action="{!editAccountPlanCurrentProvider}" rerender="">
        <apex:param name="CompetitorId" assignTo="{!selectedId}" value=""/>
      </apex:actionFunction>
      <!--apex:actionFunction name="deleteAccountPlanCurrentProvider_AF" action="{!doDeleteAccountPlanCurrentProvider}" rerender="accountPlanCurrentProvidersPageBlock,msg"-->
        <!--apex:param name="delCompetitorParam" assignTo="{!selectedIdToDelete}" value=""/-->
      <!--/apex:actionFunction-->
      <apex:pageBlockTable id="accountPlanCurrentProvidersForAccountPlan" value="{!accountPlanCurrentProvidersWrapperList}" var="apcp">
        <apex:column width="5%" headerValue="{!$Label.AccountTeamMembersList_Action}">
          <apex:outputPanel layout="inline"><a onClick="editAccountPlanCurrentProvider('{!apcp.ID}');" style="cursor: pointer;text-decoration: underline;color: #015ba7;">Edit</a>&nbsp;|&nbsp;</apex:outputPanel>
        </apex:column> 
        <apex:column headerValue="apcID" rendered="false">    
          <apex:outputText value="{!apcp.ID}"/>
        </apex:column>  
        <apex:column headerValue="{!$Label.ACCOUNTPLANNING_Current_Provider}">
           <a onClick="editAccountPlanCompetitor('{!apcp.ID}');" style="cursor: pointer;text-decoration: underline">{!apcp.AccountPlanCurrentProvider}</a>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Account.label} ">
           <apex:outputLink value="/{!LEFT(apcp.Account,15)}">{!apcp.AccountName}</apex:outputLink>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Current_Provider__c.fields.Current_Provider_Contract_End_Date__c.label}">
            <apex:outputText value="{0, date, dd'/'MM'/'yyyy hh':'mm}">
              <apex:param value="{!apcp.CurrentProviderContractEndDate}"/>
           </apex:outputText> 
        </apex:column>  
        <apex:column headerValue="{!$ObjectType.Current_Provider__c.fields.Estimated_Amount__c.label}">
          <apex:outputText value="{0, number, 0,000.00} ({!userCurrency})">
            <apex:param value="{!apcp.EstimatedAmount}"/>
          </apex:outputText> 
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Current_Provider__c.fields.Competitor_Product_s__c.label}">
          <apex:outputText value="{!apcp.CompetitorProducts}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Current_Provider__c.fields.Business_Unit__c.label}">
          <apex:outputText value="{!apcp.BusinessUnit}"/>
        </apex:column>
        <apex:column headerValue="{!$ObjectType.Current_Provider__c.fields.Experian_Provider__c.label}">
          <apex:outputText value="{!apcp.ExperianProvider}"/>
        </apex:column>
      </apex:pageBlockTable>
      <!--apex:panelGrid id="apcpListNavigation" columns="5"--> 
        <!--apex:outputText id="startEndAPCurrentProvider" value="Records ({!startAPCurrentProviderNumber}-{!endAPCurrentProviderNumber})"/-->
        <!--apex:commandLink action="{!seeMoreCurrentProviders}" rendered="{!IF(endAPCurrentProviderNumber == totalAPCurrentProviders,false,true)}" reRender="accountPlanCurrentProvidersListPageBlock" value="See {!additionalCurrentProvidersToView} more"/-->
        <!--apex:commandLink value="View Full List" action="{!viewAccountPlanCurrentProvidersListFull}"/-->
        <!--apex:outputText id="totalAPCon" value="({!totalAPCurrentProviders})"/--> 
      <!--/apex:panelGrid-->
 
    </apex:pageBlock>

  </apex:form>
  
  <script>
  function editAccountPlanCompetitor ( apcId) {  
    var editUrl = "/" + apcId;
    
    editAccountPlanCompetitor_AF(apcId);
    
    //window.parent.location.href = editUrl;
    return false;
  } 

  function editAccountPlanCurrentProvider ( apcpId) {  
    var editUrl = "/" + apcpId;
    
    editAccountPlanCurrentProvider_AF(apcpId);
    
    //window.parent.location.href = editUrl;
    return false;
  } 


  </script>
  
</apex:page>