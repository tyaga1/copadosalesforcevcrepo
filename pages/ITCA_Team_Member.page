<!--
/**=====================================================================
 * Experian
 * Name: ITCA_Home_Page
 * Description: ITCA_Team_Member
 * Created Date: July 9th 2017    
 * Created By:   Richard Joseph
 *
 * Date Modified      Modified By           Description of the update
 * 31 August 2017     Alexander McCall      ITCA Issues I1450 & I1451
 * =====================================================================*/
 -->
<apex:page controller="ITCA_Team_Member_Controller" docType="html-5.0" showHeader="false" standardStylesheets="false" sidebar="false" action="{!ITCA_Homepage_Load}">
  <title> ITCA </title>
  
  <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />

  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/bootstrap.exp.min.css')}"/>
  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/main.css')}" />
  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/exp.rebrand.proto.css')}"/>
  <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/itca-styles.css')}"/>

  <style>
    .ITCAFooter ul li,ol li{margin-left:1.5em;padding-left:0;}
    .ITCAFooter {font-size: 15px;    font-family: 'Roboto', sans-serif;}
  </style> 

  <body class="home">
    <!--Navigation -->

    <!-- Navigation -->
    <c:ITCA_Navigation_Banner currentActiveTab1="{!bannerInfoLocal}"/>
    <!-- ../Navigation-->
                
    <!--Journey Map-->
    
    <div class="container journey-map">        
      <a name="my-summary"></a>
        <div class="row">
          
            <h2>{!employeeNameForProfile} Profile</h2>
          
          
          <div class="col-sm-12">
            <div class="row is-flex">
              <!-- I1451 <div class="single-bucket">-->
                <apex:pageBlock >
                  <apex:form id="frm1">
                    <apex:pageBlockSection id="employeeUserProfilePageBlockSection" columns="1">
                      <h2><apex:outputText value="Profile Status: {!profileStatus}"/></h2>
                      <apex:outputText value="Date Discussed with Employee : {!dateDiscussedWithEmployee}"/>
                      <apex:outputText value="Manager's Comments: "/>
                      <!--<apex:inputTextarea rows="5" value="{!managerComments}" style="width:1000px" rendered="{!isManagerOfEmployee}" disabled="{!managerComments!=null}"/>-->
                      <apex:inputTextarea rows="5" value="{!managerComments}" style="width:1000px" rendered="{!isManagerOfEmployee}"/>
                      <apex:outputText value="{!managerComments}" style="width:1000px" rendered="{!!isManagerOfEmployee}"/>
                      <apex:commandButton id="managerDiscussedButton" value="Discussed with Employee" action="{!discussedEmployeeProfile}" rendered="{!isManagerOfEmployee}" styleClass="btn btn-experian-dark-blue" reRender="employeeUserProfilePageBlockSection"/>
                    </apex:pageBlockSection>
                  </apex:form>
                </apex:pageBlock>
             <!-- </div> -->
            </div>
          </div>    
          <div class="col-sm-12 body">
            <div class="row">  
                                
              <!--Dashboard Controls-->                        
              <!--../Dashboard Controls-->
                         
              <!--Results Display Section-->
              
              <apex:outputPanel id="employeeDisplayPanel">       
                <!-- Skill progress-bar --->  
                <h3><apex:outputText value="Skills:"/></h3>
                <apex:outputPanel id="mySkillProgessSummaryPanel" rendered="{!currentActiveTab=='isSummary' || currentActiveTab=='isMatchSKills' || currentActiveTab=='isCompareSKillSet' || currentActiveTab=='isCompareCareerArea'}">              
                  <div class="col-sm-8 dashboard-graph">
                    <apex:outputText rendered="{!currentActiveTab=='isMatchSKills'}"> <p>Takes the top 5 Skill Sets that your current skill are associated with. </p></apex:outputText>
                      <div class="table-responsive">
                        <table class="table">
                          <apex:repeat value="{!employeeSkillDisplayList}" var="empSkill">
                            <tr>
                              <td class="col-sm-4 skill"><a href="#" onclick="setSelectedSkill('{!empSkill.skillId}');" data-toggle="modal" data-target="#level-comparison">{!empSkill.skillName}</a></td>
                              <td class="col-sm-8">
                                <div class="progress-label">{!empSkill.curentSkillValue }/{!empSkill.maxSkillValue}</div>
                                <div class="progress">
                                  <div class="progress-bar sfia-skills" role="progressbar" aria-valuenow="{!empSkill.curentSkillValue }" aria-valuemin="0" aria-valuemax="{!empSkill.maxSkillValue}" style="width:{!empSkill.skillAccPercentage}%"></div>
                                </div>
                              </td>
                            </tr>
                          </apex:repeat>
                        </table>
                      </div>
                    </div>                            
                  </apex:outputPanel>   
                                       
                  <!-- /Skill progress-bar --->  
                </apex:outputPanel>
                </div>
                <div class="row">  
                <apex:outputPanel id="technicalDisplayPanel">       
                <!-- Skill progress-bar --->  
                <h3><apex:outputText value="Technical Specialisms:"/></h3>
                <apex:outputPanel id="mySkillProgessSummaryPanel2" rendered="{!currentActiveTab=='isSummary' || currentActiveTab=='isMatchSKills' || currentActiveTab=='isCompareSKillSet' || currentActiveTab=='isCompareCareerArea'}">              
                  <div class="col-sm-8 dashboard-graph">
                    <apex:outputText rendered="{!currentActiveTab=='isMatchSKills'}"> <p>Takes the top 5 Skill Sets that your current skill are associated with. </p></apex:outputText>
                      <div class="table-responsive">
                        <table class="table">
                          <apex:repeat value="{!technicalSkillDisplayList}" var="empSkill">
                            <tr>
                              <td class="col-sm-4 skill"><a href="#" onclick="setSelectedSkill('{!empSkill.skillId}');" data-toggle="modal" data-target="#level-comparison">{!empSkill.skillName}</a></td>
                              <td class="col-sm-8">
                                <div class="progress-label">{!empSkill.curentSkillValue }/{!empSkill.maxSkillValue}</div>
                                <div class="progress">
                                  <div class="progress-bar sfia-skills" role="progressbar" aria-valuenow="{!empSkill.curentSkillValue }" aria-valuemin="0" aria-valuemax="{!empSkill.maxSkillValue}" style="width:{!empSkill.skillAccPercentage}%"></div>
                                </div>
                              </td>
                            </tr>
                          </apex:repeat>
                        </table>
                      </div>
                    </div>                            
                  </apex:outputPanel>   
                                       
                  <!-- /Skill progress-bar --->  
                </apex:outputPanel>
              <!--../Summary Dashboard Controls-->
              </div>
            </div>
          </div>
        </div>
        <!--../Journey Map-->
 
        <!--Information-->
        <apex:form >
                        <apex:actionFunction name="setSelectedSkill_AF" reRender="mySkillProgessSummaryPanel,levelCompSection">
                          <apex:param name="setSkill" assignTo="{!selectedSkillID}" value=""/>
                        </apex:actionFunction>    
                      </apex:form> 
        <!--../Information-->
        
        <!--Skill Level Modal-->
        <div class="modal fade in" id="level-comparison" tabindex="-1" role="dialog" style="display: none;" aria-hidden="false">
            <div class="modal-dialog level-comparison-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h3 class="modal-title" id="level-comparison-label">
                            Skill Overview
                        </h3>
                    </div>
                    <div class="modal-body">
                      <div class="description">
                        <apex:outputPanel id="levelCompSection">                            
                          <div>
                            The following results are based on your self-evaluation.
                          </div>
                          <apex:repeat value="{!skillOverview_List}" var="skillOverview">
                            <div>
                              <span>Your Skill: </span>
                              <span class="skill-name">{!skillOverview.skillName}</span>
                            </div>
                            <div>
                              <span>Under Skill Set: </span>
                              <span class="skill-set-name">{!skillOverview.skillSet}</span>
                            </div><br/><br/>
                              
                            <!--td class="col-sm-6"> Current Level </td>
                            <td rowspan="2">
                              <div class="level">
                                <div class="fa fa-square-o box-number fa-3x"></div>
                                <div class="number">
                                  {!skillOverview.currentSkillValue}
                                </div>
                              </div>
                              <ul class="list-bullet">
                                <li>
                                  <apex:outputText value="{!skillOverview.currentLevel}"/>
                                </li>
                              </ul>
                            </td>
                                
                            <td class="col-sm-6"> Growth Opportunity </td>                                
                            <td rowspan="2">
                              <div class="level">
                                <div class="fa fa-square-o box-number fa-3x"></div>
                                <div class="number">
                                  {!skillOverview.nextSkillValue}
                                </div>
                              </div>
                              <ul class="list-bullet">
                                <li>
                                  <apex:outputText value="{!skillOverview.nextLevel}"/>
                                </li>
                              </ul>
                            </td-->
                                
                            <apex:repeat value="{!skillOverview.skillLevels_Map}" var="key">  
                              <td rowspan="2">
                                <div class="level">
                                  <div class="fa fa-square-o box-number fa-3x"></div>
                                  <div class="number">
                                    <apex:outputText value="{!key}" rendered="{!key!=skillOverview.currentSkillValue}"/>
                                    <apex:outputText value="{!key}" style="color:blue" rendered="{!key=skillOverview.currentSkillValue}"/>
                                  </div>
                                </div>
                                <ul class="list-bullet">
                                  <li>
                                    <apex:outputText value="{!skillOverview.skillLevels_Map[key]}" rendered="{!key!=skillOverview.currentSkillValue}"/>
                                    <apex:outputText value="{!skillOverview.skillLevels_Map[key]}" style="color:blue" rendered="{!key=skillOverview.currentSkillValue}"/>
                                  </li>
                                </ul><br/><!--Maybe remove this br once we update the description data type-->
                              </td>
                            </apex:repeat>
                          </apex:repeat>
                        </apex:outputPanel>

                            
                            
                            
                        </div>
                        
                        
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--../Skill Level Modal-->

        <!--Footer-->
        
        <!--<c:ITCA_Page_Footer />-->
        <div class="ITCAFooter">
          <apex:composition template="Community_template_footer"></apex:composition>
        </div>
        
        <!--../Footer-->


        <!-- Scripts -->
        <!--<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="js/jquery.bootstrap-responsive-tabs.min.js"></script>
        <script type="text/javascript" src="js/exp.navbar.js"></script>
        <script type="text/javascript" src="js/floatl.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        -->
        
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"/>
        
        <script>
    function setSelectedSkill(selectedSkillId){    
      setSelectedSkill_AF(selectedSkillId);

      return false;
    }
    </script>
        <!--.. /Scripts -->
    </body>

</apex:page>