<!--
/**=====================================================================
 * Experian
 * Name: ITCA_Home_Page
 * Description: ITCA_Home_Page
 * Created Date: July 9th 2017    
 * Created By:   Richard Joseph
 *
 * Date Modified      Modified By           Description of the update
 * 29th July 2017     James Wills           ITCA:W-008961 - Build list of Employees for Manager
 * 10th Aug.2017      Richard Joseph        Removing the Manager comment and function from home page and moving it to ITCA_Team_Member 
 * 26th Aug.2017      Malcolm Russell       ITCA
 * 31st Aug. 2017     Alexander McCall      I1448 
 * 7th Sep. 2017      Alexander McCall      ITCA - Remove bullet points from comparison modal
 * =====================================================================*/
 -->
<apex:page controller="ITCA_Homepage_Controller" docType="html-5.0" showHeader="false" standardStylesheets="false" sidebar="false" action="{!ITCA_Homepage_Load}">

    <title> ITCA </title>
    
    <apex:stylesheet value="{!URLFOR($Resource.SLDS214, 'assets/styles/salesforce-lightning-design-system.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EmployeeCommunity_MainRes_2017, 'css/community_mainstyles.css')}" />

    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/bootstrap.exp.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/main.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/exp.rebrand.proto.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.ITCARes, 'ITCARes/css/itca-styles.css')}"/>
    
  
 

    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"/>
 

    
    <style>
      .ITCAFooter ul li,ol li{margin-left:1.5em;padding-left:0;}
      .ITCAFooter {font-size: 15px;    font-family: 'Roboto', sans-serif;}
      #myjourneyBucket { box-shadow: 0 3px 10px rgba(0,0,0,0);}
      #level-comparison ul.list-bullet > li:before {display:none;} <!-- AM Added 07/09/17 Comparison Modal Change-->
    </style> 
        
    
   
    <body class="home">
        <!--Navigation -->

        <!-- Navigation -->
        <!--c:ITCA_Navigation_Banner /-->
        <!-- ../Navigation-->
        <c:ITCA_Navigation_Banner currentActiveTab1="{!bannerInfoLocal}"/>
        <!-- ../Navigation-->        
        
        <!--Hero Marquee-->
         <div class="banner_container hero_marquee" style="background:url( {!URLFOR($Resource.ITCARes, 'ITCARes/images/Hero-marquee_GettyImages-514168064.jpg')} );filter: progid:DXImageTransform.Microsoft.AlphaImageLoader( src={!URLFOR($Resource.ITCARes, 'ITCARes/images/Hero-marquee_GettyImages-514168064.jpg')}, sizingMethod='scale');">
            <div id="art" style="background:url({!URLFOR($Resource.ITCARes, 'ITCARes/images/data-art-500.png')}"> 
        <div>

            <div>
                <div class="container">
                    <div class="row">
                        <div class="hero-text col-sm-10 col-xs-12">
                            <h1>Welcome to your IT Career Architecture Portal</h1>
                            <h3>Your Career Compass</h3>                            
                              <apex:outputPanel rendered="{!NOT(profileExists)}">
                              <div>
                                <a class="btn btn-experian-dark-blue " href="../apex/ITCA_buildProfile_page" title="Learn more"
                                    target="_self">Get Started</a>
                                     </div>
                              </apex:outputPanel>
                              <apex:outputPanel rendered="{!profileExists}">
                              <div>
                                <a class="btn btn-experian-dark-blue " href="../apex/ITCA_Profile_Update_Skills" title="Learn more"
                                    target="_self">Continue Journey</a>
                                     </div>
                              </apex:outputPanel>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <!--./Hero Marquee-->
     
      
 <!--Three Content Buckets-->
        <div class="container three-content-buckets">
            <div class="row">
                <div class="col-sm-12">
                    <h2></h2>
                    <p>
                        This online portal is under development and in the first phase the functionality
                        is focused on helping you create your own personal skills
                        profile and compare your skills and levels of responsibility
                        to those required at all levels in your current career area
                        and other career areas within Experian IT Services (EITS).
                    </p>
                    <p>
                        The IT Career Architecture provides a standard language for objective conversations
                        between employees and managers about performance development,
                        career goals and progression. As we grow our abilities to
                        use this information for all facets of the talent lifecycle,
                        we will be positioned to create a dual career path which
                        supports both Technical Leadership and People Leadership
                        opportunities. It does not provide precise role descriptions,
                        career ladders or step-by-step actions to take. It is a compass,
                        not a map.
                    </p>
                </div>
                <div class="col-sm-12">
                    <div class="row is-flex">
                        <div class="col-sm-4">
                            <div class="single-bucket">
                                <div class="bucket-img">
                                    <i class="icon i-md i-primary-purple i-profile"></i>
                                </div>
                                <div class="bucket-text">
                                    <h3>Build your profile</h3>
                                    <apex:outputpanel rendered="{!Not(profileExists)}">
                                    <p>
                                        If this is your first time logging in, please
                                        <a href="../apex/ITCA_buildProfile_page">click here</a> to begin building your profile
                                    </p>
                                    <p><a class="btn-link" href="../apex/ITCA_buildProfile_page">Create Profile</a></p>
                                    </apex:outputpanel>
                                    <apex:outputpanel rendered="{!profileExists}">
                                    <p>
                                        To review your Profile and add new skills, please
                                        <a href="../apex/ITCA_Profile_Update_Skills">click here</a>
                                    </p>
                                    <p><a class="btn-link" href="../apex/ITCA_Profile_Update_Skills">Update Profile</a></p>
                                    </apex:outputpanel>
                                                                      
                                    <!-- I1448 <p><a class="btn-link" href="../apex/ITCA_buildProfile_page">Manage Skills</a></p> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="single-bucket">
                                <div class="bucket-img">
                                    <i class="icon i-md i-primary-purple i-glossary"></i>
                                </div>
                                <div class="bucket-text">
                                    <h3>Resources</h3>
                                    <p>
                                        We have provided a library of resources that may answer any questions you may have.
                                    </p>
                                    <p><a class="btn-link" href="../apex/ITCA_Resources_How_To_Use_ITCA">How to use the ITCA</a></p>
                                    <p><a class="btn-link" href=" http://zoomglobal/services/gtsportal/_layouts/15/WopiFrame2.aspx?sourcedoc=/services/gtsportal/Documents/SFIA%20Link.pdf&action=default">About SFIA</a></p>
                                    <p><a class="btn-link" href="../apex/ITCA_IT_Professional_Skills">IT Professional Skills</a></p>
                                    <p><a class="btn-link" href="../apex/ITCA_Experian_Way">Experian Way</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="single-bucket">
                                <div class="bucket-img">
                                    <i class="icon i-md i-primary-purple i-client"></i>
                                </div>
                                <div class="bucket-text">
                                    <h3>ITCA network</h3>
                                    <p>
                                        Connect with Experian Employess or submit a question to our help desk.
                                    </p>
                                    <p><a class="btn-link" href="../apex/ITCA_Chatter?id=0F90H000000UKlo">ITCA Chatter Group</a></p>
                                    <p><a class="btn-link" href="../apex/ITCA_FAQs">FAQs</a></p>
                                   <!-- <p><a class="btn-link" href="#">Answers</a></p> AM Phase 2 -->
                                   <!-- <p><a class="btn-link" href="#">Submit your ideas</a></p> AM Phase 2-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        <!--../Three Content Buckets-->
        
         
      
        
        <div class="container journey-map">          
          <a name="my-summary"></a>
          <apex:outputPanel id="My_Journey_Map">
          <a href="ITCA_Home_Page#my-summary">        
          <div class="col-sm-4 dashboard-controls">            
            <div class="row is-flex">                     
              <div class="col-sm-12">              
                <div id="myjourneyBucket" class="single-bucket">                
                  <c:ITCA_My_Journey_Map currentActiveTab2="{!bannerInfoLocal}"/>
                </div>                
              </div>             
            </div>            
          </div>  
              </a> 
          </apex:outputPanel>       
        </div>      
        </div>
        
        <!--First-Time-Modal-->
        <div class="modal fade in" id="first-time" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog first-time-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h3 class="modal-title" id="first-time-label">
                            Welcome!
                        </h3>
                    </div>
                    <div class="modal-body">
                        <p>
                            The ITCA Online Portal is a place where you can find information to help you define
                            and understand your capabilities, identify areas for
                            development and, with the help of your manager, navigate
                            your career at Experian. If you are a manager, you can
                            find information to help define requirements for new
                            roles, compile role descriptions, assess candidates and
                            support performance, development, promotions and career
                            conversations with your employees. Our online tool is
                            new and evolving. If you don’t find what you need here,
                            please reference the <a href="http://zoomglobal/services/gtsportal/News/Pages/EITS-Career-Architecture-Toolkit.aspx">ITCA Tool Kit</a>.
                            You can also reach out to your HR Business Partner.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--../First-Time-Modal-->

       
                       
        <!--../Skill Level Modal-->

        <!--Information-->
      
        <!--../Information-->

        <!--Footer-->
        
       
        
        <!--c:ITCA_Page_Footer /-->
        <div class="ITCAFooter">
          <apex:composition template="Community_template_footer"></apex:composition>
        </div>
                <!--../Footer-->


        <!-- Scripts -->
        
        <!--script type="text/javascript" src="js/jquery.min.js"/>
        <script type="text/javascript" src="js/bootstrap.min.js"/>
        <script type="text/javascript" src="js/jquery.touchSwipe.min.js"/>
        <script type="text/javascript" src="js/jquery.bootstrap-responsive-tabs.min.js"/>
        <script type="text/javascript" src="js/exp.navbar.js"/>
        <script type="text/javascript" src="js/floatl.js"/>
        <script type="text/javascript" src="js/script.js"--/>
        
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/bootstrap.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.touchSwipe.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/jquery.bootstrap-responsive-tabs.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/exp.navbar.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/floatl.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.ITCARes, 'ITCARes/js/script.js')}"/>
        
        <!--.. /Scripts -->
        
    </body>
    
    <script>
    function setSelectedSkill(selectedSkillId){    
      setSelectedSkill_AF(selectedSkillId);

      return false;
    }
    </script>
    
    <script >
      var itcacook="{!itcaCookie}";   
           console.log('Malc' + itcacook);
      $(document).ready(function() {
        if (itcacook!='itcaVisit') {
          $('#first-time').modal('show');
        }
      });
    </script>
    
    
    
</apex:page>