<!-- 
/**=====================================================================
 * Appirio, Inc
 * Name: Console_DisplayMyBUAccountSeg
 * Description:  T-426970 - Page used as custom console component on account layout
 * 
 * Created Date: Aug 18, 2015
 * Created By: Parul Gupta (Appirio)
 *
 * Date Modified            Modified By           Description of the update 
 * Aug 25, 2015             Parul Gupta           I-176975
 * Sep 03rd, 2015           Parul Gupta           I-178954 - FI Legacy DI Standalone field should be read only for NON Data Admin
 ======================================================================*/
 -->
<apex:page standardController="Account" extensions="Console_DisplayMyBUAccSegController">
  <apex:form >
  <apex:pageBlock mode="maindetail" title="{!segmentMapEntryName}">
    <apex:pageMessages id="pgMsgs"/>
    
    <apex:pageBlockButtons location="top">
      <apex:outputPanel id="buttons">
	      <apex:commandButton value="Edit" action="{!edit}" rerender="segmentSpecific, buttons" id="editButton" rendered="{!NOT(isEdit) && fieldSet != null}"/>
	      <apex:commandButton value="Save" action="{!save}" rerender="segmentSpecific, buttons, pgMsgs" id="saveButton"  rendered="{!isEdit}"/>
	      <apex:commandButton value="Cancel" action="{!cancel}" rerender="segmentSpecific, buttons, pgMsgs" id="cancelButton" rendered="{!isEdit}"/>
      </apex:outputPanel>
    </apex:pageBlockButtons>
    
    <apex:outputPanel id="segmentSpecific">      
        <apex:pageBlockSection rendered="{!NOT(isEdit)}">
          <apex:outputLabel value="There are no specific data for account segment" rendered="{!fieldSet == null}"/>
          <apex:pageBlockSectionItem dataStyle="text-align: right;" rendered="{!fieldSet != null}"> 
            <apex:outputPanel >
              
            </apex:outputPanel>
          </apex:pageBlockSectionItem>
          <apex:pageBlockSectionItem />
          <apex:repeat value="{!fieldSet}" var="field">
            <apex:outputField value="{!accSeg[field.fieldPath]}">
            </apex:outputField>
          </apex:repeat>
           
        </apex:pageBlockSection>
  
        <apex:pageBlockSection title="Segment Specific Section {!IF(segmentMapEntryName==null, '', segmentMapEntryName)}" rendered="{!isEdit}">
          <apex:pageBlockSectionItem dataStyle="text-align: right;" rendered="{!fieldSet != null}">
          </apex:pageBlockSectionItem>
          <apex:pageBlockSectionItem />
          <apex:repeat value="{!fieldSet}" var="field">
            <apex:inputField value="{!accSeg[field.fieldPath]}" required="{!OR(field.required, field.dbrequired)}" rendered="{!field.Label != 'FI Legacy DI Standalone'}"/>
            <apex:inputField value="{!accSeg[field.fieldPath]}" required="{!OR(field.required, field.dbrequired)}" rendered="{!isDataAdmin && field.Label == 'FI Legacy DI Standalone'}"/>
            <apex:outputField value="{!accSeg[field.fieldPath]}" rendered="{!!isDataAdmin && field.Label == 'FI Legacy DI Standalone'}"/>
          </apex:repeat>
        </apex:pageBlockSection>      
    </apex:outputPanel>
  
  </apex:pageBlock>
  </apex:form>
</apex:page>