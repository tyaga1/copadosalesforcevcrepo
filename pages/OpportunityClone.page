<!--
/**=====================================================================
 * Experian
 * Name: OpportunityClone
 * Description: CRM2:W-005338: New page to handle the deep cloning of opportunities
 * Created Date: 14 Jul 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * 18 Jul 2016        Paul Kissick          CRM2:W-005338: Removing partners due to permission problems.
 * 09 Sep 2016        Paul Kissick          CRM2:W-005338: Deny products for CPQ users (unless admins)
 * 17 Oct 2016        Paul Kissick          Case 02165894: Added support to check account/opp access before allowing a clone.
 * =====================================================================*/
 -->
<apex:page standardController="Opportunity" action="{!prepareCloning}" extensions="OpportunityCloneExt" title="{!Opportunity.Name}">
  <apex:form >
    <apex:sectionHeader title="{!$ObjectType.Opportunity.label}" />
    <apex:pageMessages />
    <apex:pageBlock >
      <apex:pageBlockButtons >
        <apex:commandButton disabled="{!NOT(allowedClone)}" action="{!createClone}" value="Clone" id="doClone" onclick="setTimeout('document.getElementById(\''+this.id+'\').className=\'btnDisabled\';', 1);  setTimeout('document.getElementById(\''+this.id+'\').disabled=true;', 50);" />
        <apex:commandButton action="{!cancel}" value="{!$Label.Cancel}" />
      </apex:pageBlockButtons>
      <apex:pageMessage rendered="{!NOT(allowedClone)}" severity="error" strength="3" summary="{!$Label.Opp_Clone_No_Account_Access}" />
      <apex:pageBlockSection >
        <apex:inputField value="{!newOpp.Name}" label="New {!$ObjectType.Opportunity.fields.Name.label}" style="width:100%;" />
        <apex:inputField value="{!newOpp.AccountId}" />
        <apex:inputField value="{!newOpp.StageName}" />
        <apex:inputField value="{!newOpp.Probability}" />
        <apex:inputField value="{!newOpp.CloseDate}" />
      </apex:pageBlockSection>
      <apex:pageBlockSection title="{!$ObjectType.OpportunityLineItem.labelPlural}" columns="1" collapsible="false">
        <apex:pageBlockSectionItem rendered="{!availableOppLineItems.size == 0}">
          {!$Label.Opp_Clone_No_records_to_display}
        </apex:pageBlockSectionItem>
        <apex:pageBlockTable value="{!availableOppLineItems}" var="o" rendered="{!availableOppLineItems.size > 0}">
          <apex:column headerValue="{!$Label.Opp_Clone_Add_to_Clone}" width="5%">
            <apex:inputCheckbox value="{!o.selected}" disabled="{!AND(currentUser.CPQ_User__c,NOT($Permission.Administration_Profile))}" />
            <apex:outputPanel rendered="{!AND(currentUser.CPQ_User__c,NOT($Permission.Administration_Profile))}">
              <span class="helpButton" id="ProductSelectDisabled-_help">
                 <img src="/s.gif" alt="" class="helpOrb" title=""/> 
                  <script type="text/javascript">
                    sfdcPage.setHelp('ProductSelectDisabled', '{!JSENCODE($Label.Opp_Clone_CPQ_User_Disabled_Help)}');
                  </script>
                </span>
              </apex:outputPanel>
          </apex:column>
          <apex:column headerValue="{!$ObjectType.OpportunityLineItem.fields.Product2Id.label}" value="{!o.oli.Product2Id}" />
          <apex:column headerValue="{!$ObjectType.OpportunityLineItem.fields.TotalPrice.label}" value="{!o.oli.TotalPrice}" />
          <apex:column headerValue="{!$ObjectType.OpportunityLineItem.fields.Type__c.label}" value="{!o.oli.Type__c}" />
          <apex:column headerValue="{!$ObjectType.OpportunityLineItem.fields.Order_Type__c.label}" value="{!o.oli.Order_Type__c}" />
          <apex:column headerValue="{!$ObjectType.OpportunityLineItem.fields.Type_of_Sale__c.label}" value="{!o.oli.Type_of_Sale__c}" />
        </apex:pageBlockTable>
      </apex:pageBlockSection>
      <apex:pageBlockSection title="{!$ObjectType.OpportunityContactRole.labelPlural}" columns="1" collapsible="false">
        <apex:pageBlockSectionItem rendered="{!availableOppContRoles.size == 0}">
          {!$Label.Opp_Clone_No_records_to_display}
        </apex:pageBlockSectionItem>
        <apex:pageBlockTable value="{!availableOppContRoles}" var="o" rendered="{!availableOppContRoles.size > 0}">
          <apex:column headerValue="{!$Label.Opp_Clone_Add_to_Clone}" width="5%">
            <apex:inputCheckbox value="{!o.selected}" />
          </apex:column>
          <apex:column headerValue="{!$ObjectType.OpportunityContactRole.fields.ContactId.label}" value="{!o.ocr.ContactId}" />
          <apex:column headerValue="{!$ObjectType.OpportunityContactRole.fields.Role.label}" value="{!o.ocr.Role}" />
          <apex:column headerValue="{!$ObjectType.OpportunityContactRole.fields.IsPrimary.label}" value="{!o.ocr.IsPrimary}" />
        </apex:pageBlockTable>
      </apex:pageBlockSection>
      <!--
      <apex:pageBlockSection title="{!$ObjectType.OpportunityPartner.labelPlural}" columns="1" collapsible="false">
        <apex:pageBlockSectionItem rendered="{!availablePartners.size == 0}">
          {!$Label.Opp_Clone_No_records_to_display}
        </apex:pageBlockSectionItem>
        <apex:pageBlockTable value="{!availablePartners}" var="o" rendered="{!availablePartners.size > 0}">
          <apex:column headerValue="{!$Label.Opp_Clone_Add_to_Clone}" width="5%">
            <apex:inputCheckbox value="{!o.selected}" />
          </apex:column>
          <apex:column headerValue="{!$ObjectType.Partner.fields.AccountToId.label}" value="{!o.ptr.AccountToId}" />
          <apex:column headerValue="{!$ObjectType.Partner.fields.Role.label}" value="{!o.ptr.Role}" />
          <apex:column headerValue="{!$ObjectType.Partner.fields.IsPrimary.label}" value="{!o.ptr.IsPrimary}" />
        </apex:pageBlockTable>
      </apex:pageBlockSection>
      -->
      <apex:pageBlockSection title="{!$ObjectType.Competitor__c.labelPlural}" columns="1" collapsible="false">
        <apex:pageBlockSectionItem rendered="{!availableCompetitors.size == 0}">
          {!$Label.Opp_Clone_No_records_to_display}
        </apex:pageBlockSectionItem>
        <apex:pageBlockTable value="{!availableCompetitors}" var="o" rendered="{!availableCompetitors.size > 0}">
          <apex:column headerValue="{!$Label.Opp_Clone_Add_to_Clone}" width="5%">
            <apex:inputCheckbox value="{!o.selected}" />
          </apex:column>
          <apex:column headerValue="{!$ObjectType.Competitor__c.fields.Account__c.label}" value="{!o.comp.Account__c}" />
        </apex:pageBlockTable>
      </apex:pageBlockSection>
      <apex:pageBlockSection title="{!$ObjectType.OpportunityTeamMember.labelPlural}" columns="1" collapsible="false">
        <apex:pageBlockSectionItem rendered="{!availableOppTeamMembs.size == 0}">
          {!$Label.Opp_Clone_No_records_to_display}
        </apex:pageBlockSectionItem>
        <apex:pageBlockTable value="{!availableOppTeamMembs}" var="o" rendered="{!availableOppTeamMembs.size > 0}">
          <apex:column headerValue="{!$Label.Opp_Clone_Add_to_Clone}" width="5%">
            <apex:inputCheckbox value="{!o.selected}" />
          </apex:column>
          <apex:column headerValue="{!$ObjectType.OpportunityTeamMember.fields.UserId.label}" value="{!o.otm.UserId}" />
          <apex:column headerValue="{!$ObjectType.OpportunityTeamMember.fields.TeamMemberRole.label}" value="{!o.otm.TeamMemberRole}" />
        </apex:pageBlockTable>
      </apex:pageBlockSection>
      <apex:pageBlockSection title="{!$ObjectType.OpportunitySplit.labelPlural}" columns="1" collapsible="false">
        <apex:pageBlockSectionItem rendered="{!availableOppSplits.size == 0}">
          {!$Label.Opp_Clone_No_records_to_display}
        </apex:pageBlockSectionItem>
        <apex:pageBlockTable value="{!availableOppSplits}" var="o" rendered="{!availableOppSplits.size > 0}">
          <apex:column headerValue="{!$Label.Opp_Clone_Add_to_Clone}" width="5%">
            <apex:inputCheckbox value="{!o.selected}" />
          </apex:column>
          <apex:column headerValue="{!$ObjectType.OpportunitySplit.fields.SplitOwnerId.label}" value="{!o.os.SplitOwnerId}" />
          <apex:column headerValue="{!$ObjectType.OpportunitySplit.fields.SplitPercentage.label}" value="{!o.os.SplitPercentage}" />
          <apex:column headerValue="{!$ObjectType.OpportunitySplit.fields.SplitTypeId.label}" value="{!o.os.SplitTypeId}" />
          <apex:column headerValue="{!$ObjectType.OpportunitySplit.fields.SplitAmount.label}" value="{!o.os.SplitAmount}" />
        </apex:pageBlockTable>
      </apex:pageBlockSection>
    </apex:pageBlock>
  </apex:form>
</apex:page>