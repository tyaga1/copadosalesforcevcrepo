<!--
/**=====================================================================
  * Experian
  * Name: SerasaFailedContractRquestAlertSetup
  * Description: A configuration page to schedule alert when there are failed contract cancellation request
  * Created Date: March 31 2017
  * Created By: Ryan (Weijie) Hu, UCInnovation
  *
  * Date Modified      Modified By                  Description of the update
  * May 2nd 2017       Ryan (Weijie) Hu             Custom labels for translation added
  *=====================================================================*/
-->
<apex:page controller="SerasaFailedContractCancellationAlert" showHeader="true" sidebar="true" docType="html-5.0">

  <style>
    .serasa-sfcras-status-section {
      margin-top: 32px;
      text-align: center;
    }

    .serasa-sfcras-status-header {
      font-size: 16px;
      background: rgba(0,0,0,0.25);
      width: 80%;
      padding: 8px 16px;
      display: inline-block;
      border-radius: 4px;
    }

    .serasa-sfcras-status-body, .serasa-sfcras-schedule-control-section {
      font-size: 16px;
      margin-top: 16px;
      display: inline-block;
      width: 80%;
    }

    .serasa-sfcras-status-detail {
      text-align: left;
      display: block;
    }

    .serasa-sfcras-status-detail-half {
      margin-top: 16px;
      width: 48%;
      height: auto;
      display: inline-block;
      -webkit-box-shadow:inset 2px 0px 0px 0px darkblue;
      -moz-box-shadow:inset 2px 0px 0px 0px darkblue;
      box-shadow:inset 2px 0px 0px 0px darkblue;
      margin-right: 2%;
      float:left;
    }

    .serasa-sfcras-status-detail-col {
      margin-bottom: 8px;
      padding-left: 8px;
      font-weight: bold;
    }

    .serasa-sfcras-custom-setting {
      font-size: 16px;
      margin-top: 8px;
    }

    .serasa-sfcras-custom-setting-opt {
      font-size: 16px;
    }

    .serasa-sfcras-schedule-control-btns {
      margin-top: 8px;
    }

    .serasa-cmd-btns {
      font-size: 14px !important;
      margin-left: 8px !important;
      margin-right: 8px !important;
      background: none !important;
      border: 1px solid lightgrey !important;
      border-radius: 4px !important;
      padding: 4px 8px !important;

      -webkit-transition: border 0.4s ease-in-out;
      -ms-transition: border 0.4s ease-in-out;
      transition: border 0.4s ease-in-out;
    }

    .serasa-cmd-btns:hover {
      border: 1px solid black !important;
      background-color: #bbbbbb !important;
      -webkit-transition: border 0.4s ease-in-out, background-color 0.4s linear;
      -ms-transition: border 0.4s ease-in-out, background-color 0.4s linear;
      transition: border 0.4s ease-in-out, background-color 0.4s linear;
    }

    .serasa-sfcras-field-info {
      padding-left: 8px;
    }
  </style>

  <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en">

    <apex:outputpanel >
      <apex:actionstatus id="entryStatus">
        <apex:facet name="start">
          <div class="waitingSearchDiv" id="el_loading" style="background-color: #fbfbfb; height: 100%;opacity:0.75;width:100%;"> 
            <div class="waitingHolder" style="top: 49%; width: 91px;">
              <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
              <span class="waitingDescription">Please Wait...</span>
            </div>
          </div>
        </apex:facet>
      </apex:actionstatus>
     </apex:outputpanel>

    <div class="serasa-sfcras-status-section">
      <div class="serasa-sfcras-status-header">{!$Label.SerasaFailedContractRquestAlertSetup_Header}</div>

      <apex:form id="serasa_sfcras_schedule_form">

        <div class="serasa-sfcras-status-body">
          <apex:outputPanel rendered="{!currentSchedule}">
            <div class="serasa-sfcras-status-detail">

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_JobName}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.CronJobDetail.Name}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_JobTime}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputText value="{!scheduledHour + ':00:00'}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_CreatedDate}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.CreatedDate}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_CreatedBy}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.CreatedById}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_EndDate}: </div>
                <div class="serasa-sfcras-field-info">
                  <apex:outputField rendered="{!IF(currentCron.EndTime != '', true, false)}" value="{!currentCron.EndTime}"/>
                  <apex:outputText rendered="{!IF(currentCron.EndTime != '', false, true)}" value="{!$Label.SerasaFailedContractRquestAlertSetup_NeverEnd}"/>
                </div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_NextFire}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.NextFireTime}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_LastFire}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.PreviousFireTime}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_SubmittedDate}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.StartTime}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_JobStatus}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.State}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_Count}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.TimesTriggered}"/></div>
              </div>

              <div class="serasa-sfcras-status-detail-half">
                <div class="serasa-sfcras-status-detail-col"> {!$Label.SerasaFailedContractRquestAlertSetup_Timezone}: </div>
                <div class="serasa-sfcras-field-info"><apex:outputField value="{!currentCron.TimeZoneSidKey}"/></div>
              </div>

            </div>
          </apex:outputPanel>

          <apex:outputPanel rendered="{!NOT(currentSchedule)}">
            <div>
              <div>{!$Label.SerasaFailedContractRquestAlertSetup_NoJobFound}</div>
            </div>
          </apex:outputPanel>
        </div>

        <hr/>

        <div class="serasa-sfcras-custom-setting">
          {!$Label.SerasaFailedContractRquestAlertSetup_AssignNewTime}: &nbsp;
          <apex:selectList styleclass="serasa-sfcras-custom-setting-opt" value="{!newScheduledHour}" size="1">
            <apex:selectOption itemValue="0" itemLabel="0"/>
            <apex:selectOption itemValue="1" itemLabel="1"/>
            <apex:selectOption itemValue="2" itemLabel="2"/>
            <apex:selectOption itemValue="3" itemLabel="3"/>
            <apex:selectOption itemValue="4" itemLabel="4"/>
            <apex:selectOption itemValue="5" itemLabel="5"/>
            <apex:selectOption itemValue="6" itemLabel="6"/>
            <apex:selectOption itemValue="7" itemLabel="7"/>
            <apex:selectOption itemValue="8" itemLabel="8"/>
            <apex:selectOption itemValue="9" itemLabel="9"/>
            <apex:selectOption itemValue="10" itemLabel="10"/>
            <apex:selectOption itemValue="11" itemLabel="11"/>
            <apex:selectOption itemValue="12" itemLabel="12"/>
            <apex:selectOption itemValue="13" itemLabel="13"/>
            <apex:selectOption itemValue="14" itemLabel="14"/>
            <apex:selectOption itemValue="15" itemLabel="15"/>
            <apex:selectOption itemValue="16" itemLabel="16"/>
            <apex:selectOption itemValue="17" itemLabel="17"/>
            <apex:selectOption itemValue="18" itemLabel="18"/>
            <apex:selectOption itemValue="19" itemLabel="19"/>
            <apex:selectOption itemValue="20" itemLabel="20"/>
            <apex:selectOption itemValue="21" itemLabel="21"/>
            <apex:selectOption itemValue="22" itemLabel="22"/>
            <apex:selectOption itemValue="23" itemLabel="23"/>
          </apex:selectList>
          :00:00 {!$Label.SerasaFailedContractRquestAlertSetup_EveryDay}
          &nbsp;
          <apex:commandButton rendered="{!CurrentCustomSetting == ''}" styleClass="serasa-cmd-btns" action="{!updateCustomSetting}" rerender="serasa_sfcras_schedule_form" value="{!$Label.SerasaFailedContractRquestAlertSetup_Button_Update}" status="entryStatus"/>
        </div>

        <apex:outputPanel id="custom_setting">
          <apex:outputPanel rendered="{!CurrentCustomSetting != ''}">

            <div class="serasa-sfcras-schedule-control-btns">
              <apex:commandButton styleClass="serasa-cmd-btns" action="{!pageScheduleMyJob}" rendered="{!NOT(currentSchedule)}" rerender="serasa_sfcras_schedule_form" value="{!$Label.SerasaFailedContractRquestAlertSetup_Button_Schedule}" status="entryStatus"/>
              <apex:commandButton styleClass="serasa-cmd-btns" action="{!pageAbortScheduledJobs}" rerender="serasa_sfcras_schedule_form" value="{!$Label.SerasaFailedContractRquestAlertSetup_Button_CancelSchedule}" status="entryStatus"/>
              <apex:commandButton styleClass="serasa-cmd-btns" action="{!pageRescheduleAlert}" rerender="serasa_sfcras_schedule_form" value="{!$Label.SerasaFailedContractRquestAlertSetup_Button_Reschedule}" status="entryStatus"/>
            </div>
            
          </apex:outputPanel>

          <apex:outputPanel rendered="{!CurrentCustomSetting == ''}">
            <div class="serasa-sfcras-custom-setting">*** {!$Label.SerasaFailedContractRquestAlertSetup_NoCustomSetting}. ***</div>
          </apex:outputPanel>
        </apex:outputPanel>

        


        
      </apex:form>
    </div>

  </html>

</apex:page>