<!--
/**=====================================================================
 * Experian
 * Name: NominationPayrollUpdate
 * Description: Page to allow mass updating of payroll gross values.
 * Created Date: 19 Sep 2016
 * Created By: Paul Kissick
 *
 * Date Modified      Modified By           Description of the update
 * Nov 8th, 2016      Paul Kissick          Added support to alert if not saved.
 * Nov 15th, 2016     Paul Kissick          Used apex:dynamicComponent instead of rendering table in VF, to fix viewstate issues.
 * =====================================================================*/
 -->
<apex:page controller="NominationPayrollUpdateCtrlr" tabStyle="Nomination__tab" action="{!loadReport}" title="Rewards Updater" sidebar="false" docType="html-5.0">
  <script>
    var confirmOnPageExit = function(e) {
        e = e || window.event;
        var message = 'It looks like you haven\'t saved. Click Cancel and then Save to save your changes.';
        if (e) {
            e.returnValue = message;
        }
        return message;
    };
  </script>
  <apex:sectionHeader title="Rewards Updater" />
  <apex:form id="updateForm" >
    <apex:actionFunction action="{!saveChanges}" name="submitForm" />
    <apex:pageBlock id="wholePage">
      <apex:pageBlockButtons >
        <apex:commandButton onclick="allowSave(this); return false;" value="Save" id="saveButton" />
        <apex:commandButton action="{!finished}" value="Done" />
      </apex:pageBlockButtons>
      <apex:pageBlockSection columns="2">
        <apex:pageMessage severity="info" strength="1" summary="If copy/pasting multiple rows, this only works if pasting into the top text box." />
        <apex:pageMessage severity="info" escape="false" summary="Language/Locale Settings" strength="1"><a href='/setup/languageAndTimeZoneSetup.apexp' target='_blank'>Click here to update your language/location settings.</a></apex:pageMessage>
      </apex:pageBlockSection>
      <apex:pageMessages id="errorMessages" />
      <apex:variable value="{!1}" var="rowNum"/>
      <apex:dynamicComponent componentValue="{!theTable}" /> 
      <!-- <apex:pageBlockSection columns="1">
        <apex:pageBlockTable value="{!nominationsToUpdate}" var="n">
          <apex:column headerValue="Number" width="5%">
            <apex:outputText value="{!FLOOR(rowNum)}"/>
            <apex:variable var="rowNum" value="{!rowNum + 1}"/>
          </apex:column>
          <apex:repeat value="{!reportFields}" var="fh">
            <apex:column rendered="{!fh.required}" value="{!n[fh.fieldPath]}" headerValue="{!fh.label}" />
          </apex:repeat>
          <apex:column headerValue="{!$ObjectType.Nomination__c.fields.Payroll_Gross_Amount__c.label}">
            <apex:inputField styleClass="payrollGross" value="{!n.Payroll_Gross_Amount__c}" html-placeHolder="e.g. 120.00" onchange="grossValueUpdated();" />
          </apex:column>
        </apex:pageBlockTable>
      </apex:pageBlockSection>
       -->
    </apex:pageBlock>
  </apex:form>
  <script>
    var grossValueUpdated = function() {
        window.onbeforeunload = confirmOnPageExit;
    };
    var allowSave = function(btn) {
      var btnId = btn.id;
      setTimeout('document.getElementById(\''+btnId+'\').className=\'btn btnDisabled\';', 1);
      setTimeout('document.getElementById(\''+btnId+'\').value=\'Saving...\';', 2);
      setTimeout('document.getElementById(\''+btnId+'\').disabled=true;', 50);
      
      window.onbeforeunload = null;
      submitForm();
    };
    if (document.getElementsByClassName('payrollGross').length > 0) {
      Array.from(document.getElementsByClassName('payrollGross')).forEach(function(i) {
        i.placeholder = "e.g. 120.00";
      });
      document.getElementsByClassName('payrollGross')[0].addEventListener('paste', function(e) {
        var valuesPasted = e.clipboardData.getData('Text').split(/\r?\n/);
        if (valuesPasted.length == 1) {
          return;
        }
        e.preventDefault();
        var inputRefs = document.getElementsByClassName('payrollGross');
        var maxLength = valuesPasted.length;
        if (valuesPasted.length > inputRefs.length) {
          maxLength = inputRefs.length;
        }
        for (i = 0; i < maxLength; i++) {
          inputRefs[i].value = valuesPasted[i];
        }
      });
    }
  </script>
</apex:page>