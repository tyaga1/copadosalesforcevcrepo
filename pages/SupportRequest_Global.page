<apex:page docType="html-5.0" standardStylesheets="false" showHeader="false" sidebar="false" >
    <!-- USES jQuery and Bootstrap 
    Both of these are hosted on CDNs, external to Sf.
    It is possible to pass fields as url parameters, e.g. ?00Ni000000CV6IW=Fred Jones&00Ni000000CV6IN=fred.jones@experian.com
    This can improve the experience by prefilling these from Salesforce. e.g. Custom Tab, or VF Page to populate these fields from the HR Org.
    
    -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8" />
    
    <div class="container well">
        <div class="page-header media">
            <div class="media-left media-middle">
                <img src="//www.experian.com/global-images/experian.png" height="58" width="164" alt="Experian" title="Experian" />
            </div>
            <div class="media-body">
                <h1>Contact Support</h1>
                <p>Please complete all the boxes below and press Send to submit a request.</p>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                Support Request
            </div>
            <div class="panel-body">
                <form action="https://www.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8" method="POST" class="form-horizontal">
                    <input type="hidden" name="orgid" value="00Di0000000faSr" /><!-- CHECK ORG ID FOR PRODUCTION --> 
                    <input type="hidden" name="retURL" value="https://experian--uat--c.cs15.visual.force.com/apex/SupportRequestHROrgComplete" />
                    <input type="hidden" id="recordType" name="recordType" value="012i00000019lV3" /> <!-- CHECK REQUEST RECORD TYPE ID -->
                    <input type="hidden" id="type" name="type" value="GCS Support" /> 
                    <input type="hidden" id="status" name="status" value="Open" />
                    <input type="hidden" id="00Ni000000G7F2M" name="00Ni000000G7F2M" value="Corporate" /> <!-- REQUESTOR BU -->
                    <input type="hidden" id="reason" name="reason" value="M&A Support Request" />
                    <!--  -------------------------------------------------------------------------- -->
                    <!--  Please uncomment  these lines if you wish to test in debug mode.           -->
                    <!--  ALSO: Remove these completely before putting into production.              -->
                    <!-- <input type="hidden" name="debug" value="1" />                              -->
                    <!-- <input type="hidden" name="debugEmail" value="paul.kissick@experian.com" /> -->
                    <!--  -------------------------------------------------------------------------- -->
                    <fieldset>
                        <legend>
                            Contact Details
                        </legend>
                        <div class="form-group">
                            <label for="00Ni000000CV6IW" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input id="00Ni000000CV6IW" required="true" maxlength="100" value="{!$User.FirstName} {!$User.LastName}" name="00Ni000000CV6IW" size="20" type="text" class="form-control" /><!-- REQUESTOR NAME -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="00Ni000000CV6IN" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input  id="00Ni000000CV6IN" required="true"  maxlength="80" value="{!$User.Email}" name="00Ni000000CV6IN" size="20" type="email" class="form-control" /><!-- REQUESTOR EMAIL -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="00Ni000000CV6IO" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input  id="00Ni000000CV6IO" required="true" maxlength="40" value="{!$User.Phone}" name="00Ni000000CV6IO" size="20" type="tel" class="form-control" /><!-- REQUESTOR PHONE -->
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>
                            Request Details
                        </legend>
                        <!--<div class="form-group">
                            <label for="reason" class="col-sm-2 control-label">Reason</label>
                            <div class="col-sm-10">
                                <select  id="reason" name="reason" required="true"  class="form-control">
                                    <option value="Change Request">Change Request</option>
                                    <option value="Data Quality">Data Quality</option>
                                    <option value="Login Issues">Login Issues</option>
                                    <option value="Mobile">Mobile</option>
                                    <option value="Process Guidance">Process Guidance</option>
                                    <option value="Reporting">Reporting</option>
                                    <option value="System / Technical Issues" selected="selected">System / Technical Issues</option>
                                    <option value="Training Request">Training Request</option>
                                    <option value="User – Modify">User – Modify</option>
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="priority" class="col-sm-2 control-label">Priority</label>
                            <div class="col-sm-10">
                                <select  id="priority" name="priority" required="true" class="form-control">
                                    <option value="Critical">Critical</option>
                                    <option value="High">High</option>
                                    <option value="Medium">Medium</option>
                                    <option value="Low" selected="selected">Low</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><!-- USER IMPACT -->
                            <label for="00Ni000000Ev1PZ" class="col-sm-2 control-label">Impact</label>
                            <div class="col-sm-10">
                                <select  id="00Ni000000Ev1PZ" required="true" name="00Ni000000Ev1PZ" title="User Impact" class="form-control">
                                    <option value="One user">One user</option>
                                    <option value="Multiple users">Multiple users</option>
                                    <option value="One Business Unit">One Business Unit</option>
                                    <option value="Multiple Business Units">Multiple Business Units</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-sm-2 control-label">Subject</label>
                            <div class="col-sm-10">
                                <input placeholder="Brief summary of the request" id="subject" maxlength="80" name="subject" size="20" type="text" class="form-control" required="true" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Full Description</label>
                            <div class="col-sm-10">
                                <textarea name="description" class="form-control" rows="5" required="true" ></textarea>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input class="btn btn-primary" type="submit" value="Send" />
                            <input class="btn btn-danger" type="reset" value="Reset" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    
    
    <script>
    $(function () {
        //grab the entire query string
        var query = document.location.search.replace('?', '');
        //extract each field/value pair
        query = query.split('&');
        //run through each pair
        for (var i = 0; i < query.length; i++) {
            //split up the field/value pair into an array
            var field = query[i].split("=");
            //target the field and assign its value
            $("input[name='" + field[0] + "'], select[name='" + field[0] + "']").val(decodeURIComponent(field[1]));
        }
    });
    
    </script>
    
    
</apex:page>