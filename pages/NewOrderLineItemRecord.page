<!-- 
 /**=====================================================================
 * Experian
 * Name: NewOrderLineItemRecord
 * Description: 
 * Created Date: Nov 2nd, 2015
 * Created By: Paul Kissick
 *
 * Date Modified                Modified By                  Description
 * 
 =====================================================================*/
 -->
<apex:page docType="html-5.0" standardController="Order_Line_Item__c" extensions="NewOrderLineItemExtension" id="thePage" tabStyle="Order__c" title="Order Line Item">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
  <apex:styleSheet value="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/themes/smoothness/jquery-ui.css" />
  <style>
    .btnPagination{
      margin: 4px !important; 
      text-decoration: none !important;
      font-size: 10pt !important;
      background-image: none !important;
      background-color: orange !important;
      color: white !important;
    }
    .btnDisabled{
      margin: 4px !important; 
      text-decoration: none !important;
      font-size: 10pt !important;
      background-image: none !important;
      background-color: lightgray !important;
      color: white !important;
    }
     
    .displayNone { 
      display: none; 
    }
    .displayBlock {
      display: block;
    }
    .ui-autocomplete-loading { 
      background: white url(/img/loading32.gif) right center no-repeat;
      background-size: 15px 15px; 
    }
    .placeHolder {
      font-style: italic;
    }
    .errorMsg {
      color: red; 
      font-weight: bold;
    }
  </style>
  <apex:sectionHeader title="{!$Label.Product_Search_Page_Title}" subtitle="{!$ObjectType.Order_Line_Item__c.label}" />
  <apex:form id="theForm">
    <script type="text/javascript">
      var j$ = jQuery.noConflict();
      j$(function(){
        createAutocomplete('__searchMasterName','DE_Product_Name__c');
        createAutocomplete('__searchName','Name');
        createAutocomplete('__searchGroup','Product_group__c');
      });
    </script>
    <apex:pageBlock >
      <apex:pageBlockSection >
        <apex:inputField value="{!Order_Line_Item__c.Order__c}" />
      </apex:pageBlockSection>
    </apex:pageBlock>
    <apex:outputPanel id="firstStep" >
      <!-- filter action -->
      <apex:actionFunction action="{!loadData}" name="__queryProducts" rerender="theForm" status="loadingFilter"/>
      <apex:pageBlock title="{!$Label.Product_Search_Block_Title}" id="pb">
        <p> 
          {!$Label.Product_Search_Step_1_Guide_Text}
        </p>
        <br/>
        <apex:pageBlockSection title="{!$Label.Product_Search_Filters_title}" id="filters" columns="1">
          <apex:outputPanel style="width:100% !important">
            <apex:inputText id="__searchMasterName" value="{!holdingProduct.productMasterName}" html-placeholder="{!$Label.Filter_Product_Master_Name_Placeholder}" />
            <apex:inputText id="__searchName" value="{!holdingProduct.ProductName}" html-placeholder="{!$Label.Product_Search_Filter_Name_Placeholder}" />
            <apex:inputText id="__searchGroup" value="{!holdingProduct.ProductGroup}" html-placeholder="{!$Label.Product_Search_Filter_Group_Placeholder}" /> 
            <apex:selectList size="1" value="{!holdingProduct.ProductFamily}" onchange="__queryProducts();">
              <apex:selectOption itemvalue="" itemLabel="{!$Label.Product_Search_Filter_Family_None}"/>
              <apex:selectoptions value="{!familyList}"/>
            </apex:selectList>
            <apex:selectList size="1" value="{!countryFilter}" onchange="__queryProducts();">
              <apex:selectOption itemvalue="" itemLabel="{!$Label.Product_Search_Filter_Country_None}"/>
              <apex:selectoptions value="{!countriesList}"/>
            </apex:selectList>
            <apex:selectList size="1" value="{!regionFilter}" onchange="__queryProducts();">
              <apex:selectOption itemvalue="" itemLabel="{!$Label.Product_Search_Filter_Region_None}"/>
              <apex:selectoptions value="{!regionsList}"/>
            </apex:selectList>
            <apex:selectList size="1" value="{!holdingProduct.globalBusinessLine}" onchange="__queryProducts();">
              <apex:selectOption itemvalue="" itemLabel="{!$Label.Product_Search_Filter_business_unit_none}"/>
              <apex:selectoptions value="{!gblList}"/>
            </apex:selectList>
            <apex:selectList size="1" value="{!holdingProduct.businessLine}" onchange="__queryProducts();">
              <apex:selectOption itemvalue="" itemLabel="{!$Label.Product_Search_Filter_business_line_none}"/>
              <apex:selectoptions value="{!businessLineList}"/>
            </apex:selectList>
            
            <apex:outputPanel >
              <apex:actionStatus id="loadingFilter">
                <apex:facet name="start">
                  <apex:outputPanel >{!$Label.Product_Search_Filtering} <apex:image value="/img/loading32.gif" width="15"/></apex:outputPanel>
                </apex:facet>
              </apex:actionStatus>
            </apex:outputPanel>
          </apex:outputPanel>
        </apex:pageBlockSection>
        <br/>
        <apex:pageMessages />
        <br/>
        <apex:pageBlockSection columns="2" id="showme" >
          <apex:pageBlockSectionItem >
          </apex:pageBlockSectionItem>
          <apex:pageBlockSectionItem id="showmeBtns">
            <apex:outputPanel >
              {!$Label.Product_Search_Show_me}
              <apex:commandButton styleClass="{!IF(filterFollowedProducts,'btnPagination','btnHighlight')}" 
                        value="{!$Label.Product_Search_Show_Me_All}" disabled="{!filterFollowedProducts==false}"
                        action="{!toggleFollowedProducts}"
                        rerender="theForm" status="loadingFilter">
              </apex:commandButton>
              <apex:commandButton styleClass="{!IF(filterFollowedProducts==false,'btnPagination','btnHighlight')}" 
                        value="{!$Label.Product_Search_Show_Me_Products_I_follow}" disabled="{!filterFollowedProducts}"
                        action="{!toggleFollowedProducts}"
                        rerender="theForm" status="loadingFilter">
              </apex:commandButton>
            </apex:outputPanel>
          </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        <apex:pageBlockTable value="{!records}" var="p" id="results" >
          
          <apex:column >
            <apex:facet name="header">
              <span>{!$Label.Product_Search_Column_Select}</span>
            </apex:facet>
            <apex:outputPanel > 
              <apex:inputCheckbox value="{!p.selected}" id="__selectedChkBox" onClick="radioClick();">
                <apex:actionSupport event="onchange" 
                  action="{!p.selectItem}" 
                  rerender="pageButtonsBottom" 
                  onsubmit="toggleCheckboxes(false);" 
                  oncomplete="toggleCheckboxes(true);"
                />
              </apex:inputCheckbox>
            </apex:outputPanel>
          </apex:column>
          
           <apex:column >
            <apex:facet name="header">
              <apex:outputPanel >
                <apex:commandLink action="{!sortRecordset}" rerender="theForm" status="loadingFilter">
                  <apex:param name="sf" value="{!IF(BEGINS(sortField,'DE_Product_Name__c '),IF(CONTAINS(sortField,' ASC'),'DE_Product_Name__c DESC','DE_Product_Name__c ASC'),'Name ASC')}" assignTo="{!sortField}"/>
                  <span >{!$Label.Product_Search_Column_Product_Master_Name}</span>
                </apex:commandLink>
                <apex:image value="/img/sort_asc_arrow.gif" rendered="{!BEGINS(sortField,'DE_Product_Name__c ') && CONTAINS(sortField,' ASC')}"/>
                <apex:image value="/img/sort_desc_arrow.gif" rendered="{!BEGINS(sortField,'DE_Product_Name__c ') && CONTAINS(sortField,' DESC')}"/>
              </apex:outputPanel>
            </apex:facet>
            <apex:outputPanel >
              <apex:outputLink value="/{!p.product.Id}" target="_blank">
                <apex:outputField value="{!p.product.DE_Product_Name__c}"/>
              </apex:outputLink>
            </apex:outputPanel>
          </apex:column>
          
          
          <apex:column >
            <apex:facet name="header">
              <apex:outputPanel >
                <apex:commandLink action="{!sortRecordset}" rerender="theForm" status="loadingFilter">
                  <apex:param name="sf" value="{!IF(BEGINS(sortField,'Name '),IF(CONTAINS(sortField,' ASC'),'Name DESC','Name ASC'),'Name ASC')}" assignTo="{!sortField}"/>
                  <span >{!$Label.Product_Search_Column_Product_Name}</span>
                </apex:commandLink>
                <apex:image value="/img/sort_asc_arrow.gif" rendered="{!BEGINS(sortField,'Name ') && CONTAINS(sortField,' ASC')}"/>
                <apex:image value="/img/sort_desc_arrow.gif" rendered="{!BEGINS(sortField,'Name ') && CONTAINS(sortField,' DESC')}"/>
              </apex:outputPanel>
            </apex:facet>
            <apex:outputPanel >
              <apex:outputLink value="/{!p.product.Id}" target="_blank">
                <apex:outputField value="{!p.product.Name}"/>
              </apex:outputLink>
            </apex:outputPanel>
          </apex:column>
          
          <apex:column >
            <apex:facet name="header">
              {!$Label.Product_Search_Column_Description}
            </apex:facet>
            <apex:outputPanel >
              <apex:outputText value="{!IF(LEN(p.product.Product_Desc__c)>200,LEFT(p.product.Product_Desc__c,197) & '...',p.product.Product_Desc__c)}" title="{!p.product.Product_Desc__c}"/>
            </apex:outputPanel>
          </apex:column> 

          <apex:column >  
            <apex:facet name="header">
              <apex:outputPanel >
                <apex:commandLink action="{!sortRecordset}" rerender="theForm" status="loadingFilter">
                  <apex:param name="sf" value="{!IF(BEGINS(sortField,'IsActive '),IF(CONTAINS(sortField,' ASC'),'IsActive DESC','IsActive ASC'),'IsActive ASC')}" assignTo="{!sortField}"/>
                  <span >{!$Label.Product_Search_Column_Status}</span>
                </apex:commandLink>
                <apex:image value="/img/sort_asc_arrow.gif" rendered="{!BEGINS(sortField,'IsActive ') && CONTAINS(sortField,' ASC')}"/>
                <apex:image value="/img/sort_desc_arrow.gif" rendered="{!BEGINS(sortField,'IsActive ') && CONTAINS(sortField,' DESC')}"/>
              </apex:outputPanel>
            </apex:facet>
            <apex:outputPanel >
              <apex:outputText value="{!IF(p.product.IsActive,$Label.Product_Search_Active,$Label.Product_Search_Inactive)}"/>
            </apex:outputPanel>
          </apex:column>
          
        </apex:pageBlockTable>
                
        <apex:pageBlockSection columns="2" id="pagination" >
          <apex:pageBlockSectionItem id="pageSize">
            <apex:outputPanel > 
              {!$Label.Product_Search_Show} 
              <apex:selectList value="{!newPageSize}" size="1">
                <apex:actionSupport event="onchange" action="{!loadData}" />
                <apex:selectoption itemvalue="2" itemlabel="{!$Label.Product_Search_2_per_page}"/>
                <apex:selectoption itemvalue="10" itemlabel="{!$Label.Product_Search_10_per_page}"/>
                <apex:selectoption itemvalue="25" itemlabel="{!$Label.Product_Search_25_per_page}"/>
                <apex:selectoption itemvalue="50" itemlabel="{!$Label.Product_Search_50_per_page}"/>
                <apex:selectoption itemvalue="100" itemlabel="{!$Label.Product_Search_100_per_page}"/>
                <apex:selectoption itemvalue="200" itemlabel="{!$Label.Product_Search_200_per_page}"/>
              </apex:selectList>
              
              <span>{!$Label.Product_Search_Showing} {!IF(resultSize < pageSize,resultSize,pageSize)} {!$Label.Product_Search_OF} {!resultSize} {!$Label.Product_Search_Records}</span>
            </apex:outputPanel>
          </apex:pageBlockSectionItem>
          
          <apex:pageBlockSectionItem id="pageNavigation">
            <apex:outputPanel id="paginator" >
              <apex:commandButton value="{!$Label.Product_Search_Button_First_Page}" styleClass="{!IF(pageNumber != 1,'btnPagination','btnDisabled')}" 
                        disabled="{!pageNumber == 1}" action="{!goToFirstPage}" />
              <apex:commandButton value="{!pageNumber-2}" styleClass="btnPagination" rendered="{!pageNumber > 2}" 
                        action="{!goToPrev2Page}" rerender="theForm" status="loadingFilter"/>
              <apex:commandButton value="{!pageNumber-1}" styleClass="btnPagination" rendered="{!pageNumber > 1}" 
                        action="{!goToPrevPage}" />
              <apex:commandButton disabled="true" styleClass="btnDisabled" value="{!pageNumber}" />
              <apex:commandButton value="{!pageNumber+1}" styleClass="btnPagination" rendered="{!totalPages >= (pageNumber+1)}" 
                        action="{!goToNextPage}" />
              <apex:commandButton value="{!pageNumber+2}" styleClass="btnPagination" rendered="{!totalPages >= (pageNumber+2)}" 
                        action="{!goToNext2Page}" />
              <apex:commandButton value="{!$Label.Product_Search_Button_Last_page}" styleClass="{!IF(pageNumber < totalPages,'btnPagination','btnDisabled')}" 
                        disabled="{!pageNumber >= totalPages}" action="{!goToLastPage}" />
            </apex:outputPanel>
          </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
        <apex:pageBlockButtons location="bottom">
          <apex:outputPanel id="pageButtonsBottom">
            <apex:commandButton action="{!redirect}" rendered="{!saveEnabled}" value="{!$Label.Product_Search_Button_Save}" />
            <apex:commandButton action="{!cancel}" value="{!$Label.Product_Search_Button_Cancel}" />
          </apex:outputPanel>
        </apex:pageBlockButtons>
        
      </apex:pageBlock>
    
    </apex:outputPanel>
  </apex:form>
  <script type="text/javascript">
    function radioClick() {
      this.blur();
      this.focus();
    }
    
    /* this function creates the autocmplete input texts */
      
    function createAutocomplete(filterInputId, fieldName) {
      j$('[id$='+filterInputId+']').autocomplete({
        minLength: 3,
        source: function(request, response) {
          var queryTerm = request.term || null;
          if (!queryTerm || queryTerm.length < 3) { return []; }
          Visualforce.remoting.Manager.invokeAction(
            '{!$RemoteAction.NewOrderLineItemExtension.searchFilters}', 
            queryTerm, 
            fieldName, 
            function(result, event) {
              if(event.type == 'exception') {
                console.error(event.message);
              } 
              else {
                var alertFallback = true;
                if (typeof console === "undefined" || typeof console.log === "undefined") {
                  console = {};
                  if (alertFallback) {
                    console.log = function(result) {
                      alert(result);
                    };
                  } 
                  else {
                    console.log = function() {};
                  }
                }
                //console.log(result);
                response(result);
              }
            });
          },
          select: function( event, ui ) {
            j$('[id$='+filterInputId+']').val( ui.item[fieldName] );
            __queryProducts();
            return false;
          },
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
          var entry = "<a>" + item[fieldName]+ "</a>";
          return j$( "<li></li>" )
            .data("item.autocomplete", item)
            .append(entry)
            .appendTo(ul);
        };
        //support for ENTER keypress
        j$('[id$='+filterInputId+']').keyup(function(e){
          if(e.keyCode == 13){
             __queryProducts();
          }
      });
      
      j$('[id$='+filterInputId+']').focus(function(){
        j$(this).val('');
      });
    }
    
    /*
      Disabled selection checkboxes while loading 
    */
    function toggleCheckboxes(state){
      j$('[id$=__selectedChkBox]').attr('disabled',!state);
    }
  </script>
  

</apex:page>