<!--
/**=====================================================================
 * Name: AccountPlanPDFGenerator
 * Description: Page used to customise the data shown on the AccountPlanPDF page
 * Created Date: Jan. 19th, 2016
 * Created By: James Wills
 * 
 * Date Modified         Modified By            Description of the update
 * Jan. 19th, 2017       James Wills            Created.
 * =====================================================================*/
-->
<apex:page standardController="Account_Plan__c" extensions="AccountPlanTabPanel,AccountPlanPDFGeneratorExtension" showHeader="true" showChat="true" sidebar="false" tabStyle="Account_Plan__c">

  <apex:stylesheet value="/sCSS/38.0/sprites/Theme3/default/gc/versioning.css"/>   
  <apex:stylesheet value="/sCSS/38.0/sprites/Theme3/default/gc/extended.css"/>

  <apex:form id="frm">
    
    <apex:actionFunction name="updateSchema_AF" action="{!updateSchema}" rerender="frm"/>    
    <apex:actionFunction name="saveSchema_AF" action="{!saveSchema}" rerender="frm, selectedSectionsList, msg"/>    
    <apex:actionFunction name="buildPDF_AF" action="{!buildPDFDesign}" rerender="frm, selectedSectionsList, msg"/>
    
    
    <apex:pageMessages id="msg"/>
    
    <apex:sectionHeader title="{!$ObjectType.Account_Plan__c.label}" subtitle="{!Account_Plan__c.Name}"/> 
    <apex:commandLink action="{!backToAccountPlan}">{!$ObjectType.Account_Plan__c.label}: {!Account_Plan__c.Name}</apex:commandlink>
    
    <apex:outputField id="Id" value="{!Account_Plan__c.Id}" rendered="false"/>
    <apex:outputField id="Account__c" value="{!Account_Plan__c.Account__c}" rendered="false"/>
    <apex:outputField id="Account__r" value="{!Account_Plan__c.Account__r.Name}" rendered="false"/>             
    
    <apex:pageBlock title="{!$Label.ACCOUNTPLANNING_PDF_Design_Selection_Heading}" mode="Edit">
      <apex:pageBlockButtons id="detailButtons">
        <apex:commandButton action="{!saveSchema}" value="{!$Label.BTN_SAVE}" reRender="frm, msg, userSchemaList"/>
        <apex:commandButton action="{!saveMessage}" onclick="window.open('/apex/GenerateAccountPlanPDF?id={!Account_Plan__c.Id}&schemaToUseForPDFId={!selectedSchemaId}','','width=900,height=600');" value="{!$Label.ACCOUNTPLANNING_PDF_BTN_Build_PDF_Design}" reRender="frm"/>
        <apex:commandButton action="{!cancel}" value="{!$Label.Cancel}"/>
        <apex:commandButton action="{!deleteSchema}" value="{!$Label.ACCOUNTPLANNING_PDF_BTN_Delete_PDF_Design}" reRender="frm, msg, userSchemaList" disabled="{!readOnly}"/>
      </apex:pageBlockButtons>
      <apex:pageBlockSection columns="2">
        <apex:outputLabel value="{!$Label.ACCOUNTPLANNING_PDF_New_PDF_Design}"/>
        <apex:inputText value="{!newUserSchema}"/>
        <apex:outputLabel value="{!$Label.ACCOUNTPLANNING_PDF_Current_PDF_Design}"/>
        <apex:outputText value="{!selectedSchemaName}" style="font-size:14px"/>
        <apex:outputLabel value="{!$Label.ACCOUNTPLANNING_PDF_Saved_PDF_Design_List}"/>
        <apex:selectList id="userSchemaList" value="{!selectedSchemaId}" size="1" style="width:400px" multiselect="false" onChange="updateSchema_AF();">
          <apex:selectOptions value="{!items}"/>
        </apex:selectList>
      </apex:pageBlockSection>
      <apex:pageBlockSection columns="2" rendered="{!selectedSchemaId!=null}">
        <apex:outputLabel value="{!$Label.ACCOUNTPLANNING_Created_By}"/>
        <apex:outputText label="" value="{!selectedSchema.CreatedBy.Name}">
          <apex:param value="{!selectedSchema.CreatedBy.Name}"/>  
        </apex:outputText>
        <apex:outputLabel value="{!$ObjectType.Account_Plan_PDF_Data__c.fields.CreatedDate.label}"/>
        <apex:outputText value="{0, date, dd'/'MM'/'yyyy', 'HH:mm:ss}">
          <apex:param value="{!selectedSchema.CreatedDate}"/>
        </apex:outputText>       
        <apex:outputLabel value="{!$ObjectType.Account_Plan_PDF_Data__c.fields.LastModifiedDate.label}"/>
        <apex:outputText value="{0, date, dd'/'MM'/'yyyy', 'HH:mm:ss}">
          <apex:param value="{!selectedSchema.LastModifiedDate}"/>
        </apex:outputText>  
      </apex:pageBlockSection>
    </apex:pageBlock>
    
    <apex:pageBlock title="{!$Label.ACCOUNTPLANNING_PDF_Sections_Selection_Heading}">
      <apex:pageBlockButtons id="detailButtons2">
        <apex:commandButton action="{!selectAll}" value="{!$Label.BTN_Select_All_Sections}" reRender="frm, msg, userSchemaList" disabled="{!readOnly}"/>
        <apex:commandButton action="{!deSelectAll}" value="{!$Label.BTN_Deselect_All_Sections}" reRender="frm, msg, userSchemaList" disabled="{!readOnly}"/>
      </apex:pageBlockButtons>
    
       <table class="table-bordered" style="width:75%; border-spacing: 0; margin-top:10px;">
       <tr>
         <td width="5%" style="font-size: 12px; font-weight: bold; text-align:left;padding-left : 10px;"/>
         <td width="20%" style="font-size: 12px; font-weight: bold; text-align:left; padding-left:10px;">
            <apex:outputText style="font-weight:bold" value="{!$Label.ACCOUNTPLANNING_PDF_Available_Sections}"/>
            <apex:selectList id="deSelectedSectionsList" value="{!selectedSectionFromScreen_List}" title="{!$Label.ACCOUNTPLANNING_PDF_Sections_Available_Info}" size="16" style="width:400px" multiselect="true" disabled="{!readOnly}">
              <apex:selectOptions value="{!pdfBuildInstructionsUnselected_SelectList}"/>
            </apex:selectList>
         </td>
         <td width="5%" style="font-size: 12px; text-align:left; padding-left : 10px;">
           <br/><br/>
           <apex:outputText value="{!$Label.Add}" style="text-align:center; width:100px"/><br/>
           <apex:commandButton action="{!selectSection}" style="align:center" image="{!URLFOR($Resource.navigation, 'right_button.png')}"  reRender="frm, msg, selectedSectionsList,deSelectedSectionsList" disabled="{!readOnly}"/><br/>   
           <apex:commandButton action="{!deSelectSection}" style="align:center" image="{!URLFOR($Resource.navigation, 'left_button.png')}" reRender="frm, msg, selectedSectionsList,deSelectedSectionsList" disabled="{!readOnly}"/><br/>
           <apex:outputText value="{!$Label.Product_Search_Link_Remove}" style="text-align:center; width:100px"/><br/>
         </td>
         <td width="20%" style="font-size: 12px; font-weight: bold; text-align:left; padding-left:10px;">
            <apex:outputText style="font-weight:bold" value="{!$Label.ACCOUNTPLANNING_PDF_Selected_Sections}"/>
            <apex:selectList id="selectedSectionsList" value="{!deSelectedSectionFromScreen_List}" title="{!$Label.ACCOUNTPLANNING_PDF_Sections_Selected_Info}" size="16" style="width:400px" multiselect="true" disabled="{!readOnly}">
              <apex:selectOptions value="{!selectedSectionsList}"/>
            </apex:selectList>
         </td>
         <td width="5%" style="font-size: 12px; text-align:left;padding-left : 10px;">
            <br/><br/>
            <apex:outputText value="{!$Label.BTN_Move_Up}" style="text-align:center; width:100px"/><br/>
            <apex:commandButton action="{!promoteSection}" image="{!URLFOR($Resource.navigation, 'up_button.png')}" reRender="frm, msg, selectedSectionsList" disabled="{!readOnly}"/><br/>      
            <apex:commandButton action="{!demoteSection}" image="{!URLFOR($Resource.navigation, 'down_button.png')}" reRender="frm, msg, selectedSectionsList" disabled="{!readOnly}"/><br/>
            <apex:outputText value="{!$Label.BTN_Move_Down}" style="text-align:center; width:100px"/><br/>
         </td>
         <td width="5%" style="font-size: 12px; font-weight: bold; text-align:left;padding-left : 10px;"/>
        </tr>
      </table>   
    </apex:pageBlock>
    <apex:pageBlock id="custInfoPageBlock" title="{!$Label.ACCOUNTPLANNING_PDF_Account_Team_Vision} {!$Label.ACCOUNTPLANNING_PDF_Fields_to_Display}" rendered="{!showCustInfo}">
       <apex:pageBlockButtons id="detailButtons3">
        <apex:commandButton action="{!selectAllCustInfo}" value="{!$Label.BTN_Select_All_Fields}" reRender="custInfoPageBlock, msg" disabled="{!readOnly}"/>
        <apex:commandButton action="{!deSelectAllCustInfo}" value="{!$Label.BTN_Deselect_All_Fields}" reRender="custInfoPageBlock, msg" disabled="{!readOnly}"/>
      </apex:pageBlockButtons>
      <apex:pageBlockSection columns="4">
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Account__c.label}"/>
        <apex:inputCheckbox value="{!alwaysShowTheseFields}" disabled="true"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Account_Industry__c.label}"/>
        <apex:inputCheckbox value="{!alwaysShowTheseFields}" disabled="true"/>
        
         <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Account_Sector__c.label}"/>
        <apex:inputCheckbox value="{!alwaysShowTheseFields}" disabled="true"/>
      
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Experian_Vision_for_Account__c.label}"/>
        <apex:inputCheckbox value="{!showExperianGoals}" disabled="{!readOnly}"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Experian_Strategy_for_Account__c.label}"/>
        <apex:inputCheckbox value="{!showExperianStrategy}" disabled="{!readOnly}"/>
        
      </apex:pageBlockSection>
    </apex:pageBlock>
    <apex:pageBlock id="clientInfoPageBlock" title="{!$Label.ACCOUNTPLANNING_PDF_Client_Information} {!$Label.ACCOUNTPLANNING_PDF_Fields_to_Display}" rendered="{!showClientInfo}">
      <apex:pageBlockButtons id="detailButtons4">
        <apex:commandButton action="{!selectAllClientInfo}" value="{!$Label.BTN_Select_All_Fields}" reRender="clientInfoPageBlock, msg" disabled="{!readOnly}"/>
        <apex:commandButton action="{!deSelectAllClientInfo}" value="{!$Label.BTN_Deselect_All_Fields}" reRender="clientInfoPageBlock, msg" disabled="{!readOnly}"/>
      </apex:pageBlockButtons>
      <apex:pageBlockSection columns="4">
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Core_Business__c.label}"/>
        <apex:inputCheckbox value="{!showClientsCoreBusiness}" disabled="{!readOnly}"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Annual_CAPEX_in_Experian_Domain__c.label}"/>
        <apex:inputCheckbox value="{!showCapexInOurDomain}" disabled="{!readOnly}"/>
        
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Annual_OPEX_in_Experian_Domain__c.label}"/>
        <apex:inputCheckbox value="{!showOpexInOurDomain}" disabled="{!readOnly}"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Strategic_Direction__c.label}"/>
        <apex:inputCheckbox value="{!showClientsStrategy}" disabled="{!readOnly}"/>
        
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Business_Objectives__c.label}"/>
        <apex:inputCheckbox value="{!showClientsObjectives}" disabled="{!readOnly}"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Major_Changes__c.label}"/>
        <apex:inputCheckbox value="{!showClientsChanges}" disabled="{!readOnly}"/>
        
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Major_Competitors__c.label}"/>
        <apex:inputCheckbox value="{!showClientsCompetitors}" disabled="{!readOnly}"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Major_Partners__c.label}"/>
        <apex:inputCheckbox value="{!showClientsPartners}" disabled="{!readOnly}"/>
        
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Experian_Annualised_Revenue__c.label}"/>
        <apex:inputCheckbox value="{!showExperianAnnualisedRevenue}" disabled="{!readOnly}"/>
      </apex:pageBlockSection>
    </apex:pageBlock>
    <apex:pageBlock id="recentHistoryPageBlock" title="{!$Label.ACCOUNTPLANNING_PDF_Recent_History} {!$Label.ACCOUNTPLANNING_PDF_Fields_to_Display}" rendered="{!showRecentHistory}">
       <apex:pageBlockButtons id="detailButtons5">
        <apex:commandButton action="{!selectAllRecentHistory}" value="{!$Label.BTN_Select_All_Fields}" reRender="recentHistoryPageBlock, msg" disabled="{!readOnly}"/>
        <apex:commandButton action="{!deSelectAllRecentHistory}" value="{!$Label.BTN_Deselect_All_Fields}" reRender="recentHistoryPageBlock, msg" disabled="{!readOnly}"/>
      </apex:pageBlockButtons>
       <apex:pageBlockSection columns="4">
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Summary_Recent_History__c.label}"/>
        <apex:inputCheckbox value="{!showSummaryRecentHistory}" disabled="{!readOnly}"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Recent_Successes__c.label}"/>
        <apex:inputCheckbox value="{!showRecentRelationshipSuccess}" disabled="{!readOnly}"/>
        
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Objectives_Not_Achieved__c.label}"/>
        <apex:inputCheckbox value="{!showRelationshipObjNotAchieved}" disabled="{!readOnly}"/>
    
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Lessons_Learned__c.label}"/>
        <apex:inputCheckbox value="{!showRelationshipLessonsLearned}" disabled="{!readOnly}"/>
        
        <apex:outputText value="{!$ObjectType.Account_Plan__c.fields.Additional_Information__c.label}"/>
        <apex:inputCheckbox value="{!showAdditionalInformation}" disabled="{!readOnly}"/>
        
      </apex:pageBlockSection>

    </apex:pageBlock>
    
  </apex:form>

  <script>


    //function updateSelectedSections_JS ( paramValueFromScreen ) {  
    
      //updateSelectedSections_AF(paramValueFromScreen);

      //return false;
    //} 

    function updateSchema_JS ( apcId) {  
      //var editUrl = "/" + apcId;
    
      updateSchema_AF(apcId);

      return false;
    } 
  
  
    function buildPDFDesign_JS (apcId,selectedSchemaId) {  
      
      //saveSchema_AF();
      var saved=buildPDF_AF();
      
      if(!saved){      
        window.open('/apex/GenerateAccountPlanPDF?id={!Account_Plan__c.Id}&schemaToUseForPDFId={!selectedSchemaId}','_blank','width=900,height=600');
      }
      //return false;
    } 
  
  

  </script>
</apex:page>