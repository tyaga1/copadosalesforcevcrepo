<!--
/**=====================================================================
 * Appirio, Inc;
 * Name: AccountPlanPenetration.page
 * Description: 
 * Created Date: 
 * Created By: Appirio
 * 
 * Date Modified      Modified By                  Description of the update
 * Dec 30th, 2014     Noopur Sundriyal             T-343375: Refactoring CSS/Labels
 * July 03rd, 2015    Sarah Barber                 Case: 982988: addition of new Experian Strategy field
 * Sept 09th,2015     Jagjeet Singh                T-432468: Added 'Additional Information' in Recent History Section
 * Sept 09th,2015     Jagjeet Singh                T-432474: Corrected the css styling to display the proper colors on Account Dashboard.
 * Sept 9th,2015      Jagjeet Singh(Appirio)       T-432678 : Displaying new fields for the Account Plan Competitor section.
 * Sept 09th,2015     Jagjeet Singh(Appirio)       T-432472 : Updated the colors to paler versions of a) Account Dashboard b) Account Penetration c) Potential future opportunities
 * Sept 10th,2015     Jagjeet Singh(Appirio)       T-432473 : Updated Closed Won & Open Opportunities section.
 * Sept 10th,2015     Jagjeet Singh(Appirio)       T-432476 : Added page breaks before every new section.
 * Sept 10th,2015     Jagjeet Singh(Appirio)       T-432477: Updated the styling to display more than 6 records in SWOT analysis section.
 * Sept 14th,2015     Jagjeet Singh(Appirio)       T-432478: Added the Notes section in PDF.
 * Sept 22nd, 2015    Jagjeet Singh(Appirio)       I-181371: Adjusted the Client And experian swot analysis section styling.
 * Sept 24th, 2015    Jagjeet Singh(Appirio)       T-434657: Updated the AccountRelaionshipStatus section to display in tree format and also styling.
 * Oct 8th, 2015      Jagjeet Singh(Appirio)       I-181371: Added the inline style colors to fix the coloring Issue in Pdf.
 * Nov 17th, 2015     Paul Kissick                 Case 00992155: Fix for dates on tasks
 * Apr 28th, 2016     James Wills                  Case 01848189: Account Planning - Updates to reflect new and changed Account Planning functionality.
 * Oct. 19th, 2016    James Wills                  Case 02154716: Added section for Current Providers.
 * Jan. 25th, 2016    James Wills                  Case 01999757: Put sections into components
 * =====================================================================*/
-->
<apex:page standardcontroller="Account_Plan__c" extensions="GenerateAccountPlanPDFController" showHeader="false" sidebar="false" applyHtmlTag="false" id="base" renderas="{!$Request.mode}"> 
<!--   renderAs="pdf">-->
          
  <!--  Include Static resources -->
  <link rel="stylesheet" href="{!URLFOR($Resource.AccountPlanningCSS, 'css/default-style.css')}" type="text/css"/>
  <apex:stylesheet value="{!URLFOR($Resource.PDFGenerator, 'PDFDocumentStyle.css')}" />
  <apex:stylesheet value="{!URLFOR($Resource.SWOT_Analysis_UI, 'SWOT_Analysis_UI/css/style.css')}" />
  <apex:stylesheet value="{!URLFOR($Resource.AccountPlanningCSS, 'css/accountPlanPDF.css')}"/>
  <style>
     .Positive     {color: #6aa84f !important;} 
     .Negative     {color: #cc0000 !important;}  
     .Neutral      {color: #f1c232 !important;}
     .Relationship {color: #999999 !important;}
     .BoldFont     {font-weight:bold; color: #000000;}
     .caption      {width: 15px; height: 15px;}
  </style>
  

  <head>  
    <!-- This would be Header and footer for all pages -->
    <c:HeaderFooterComponent type="header" position="left">
      <div style="padding: 5px;">
        <img src="{!URLFOR($Resource.prototypeCss, 'i/spritelogo.png')}" style="width:200px;"/>
      </div>
    </c:HeaderFooterComponent>
   
    <c:HeaderFooterComponent type="header" position="right" showPageNumbers="true"/>
        
    <apex:includeScript value="{!URLFOR($Resource.chartToPdf, '/jspdf.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.chartToPdf, '/html2canvas.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.chartToPdf, '/canvas2image.js')}" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <apex:includeScript value="/soap/ajax/28.0/connection.js"/>
    <apex:includeScript value="/soap/ajax/28.0/apex.js"/>
         
  </head>
  
  <apex:outputPanel rendered="{!$Request.mode != 'pdf'}">
    <apex:outputPanel id="blankPanel"/>
    <apex:form >
      <apex:actionFunction action="{!saveAttachment}" name="SaveAccPlan" rerender="blankPanel"/>
    </apex:form>
    <center><a class="btn" onclick="SaveAccPlan();" href="apex/GenerateAccountPlanPDF?id={!accountPlanObj.Id}&schemaToUseForPDFId={!schemaToUseForPDFId}&mode=pdf" style="text-decoration:none" download="AccountPlan.pdf">Download PDF</a></center>
    <br/>
  </apex:outputPanel>
           
  <apex:dynamicComponent componentValue="{!pdfSections}"/>      

  <br/>
  
</apex:page>