<!--
/**=====================================================================
 * Appirio, Inc
 * Name: AccountTeamMembersList.page
 * Description:  T-366039: Account Team VF Page due to Confidential Information
 * Created Date: Feb 27th, 2015
 * Created By: Appirio
 * 
 * Date Modified      Modified By              Description of the update
 * Mar 4th,2015       Noopur                   Added show more and go to list links.
 * May 21st, 2015     Noopur                   I-163516 - Modified to add console related methods 
 *                                             to resolve the issues related to EMS console
 * June 2nd, 2015     Paul Kissick             Case #941536 - Adding checks for the current user Account Access
 * Jun 18th, 2015     Paul Kissick             Case #967382: Bug found when adding confidential info sharing and need to split 
                                               this into 2 pages. Removing edit stuff from this page.
 * June 22nd, 2015    Diego Olarte             Case #00569665 - Add button visible only to admin profiles
 * Augt 12th, 2015    Nur Azlini               Change profile name 'Sales Effectiveness' to 'Experian Sales Effectiveness'
 * Augt 12th, 2015    Diego Olarte             Added missing profiles 'Experian Sales Support' and 'Experian Sales Manager'
 * Sept 29th, 2015    Noopur                   I-180764 : Added parameter for the Account Id, when clicking on new access button.
 * Octo 14th, 2015    Diego Olarte             Case #01177612 - Remove button visible only to admin profiles
 * Nov 24th, 2015     James Wills              Case #1192655: Added column for 'Team Member Business Unit' for display in 'Account Team Member' related list.
 * Nov 24th, 2015     James Wills              Case #1227650: Added column for 'Date Added / By' for display in 'Account Team Member' related list.
 * Dec 16th, 2015     Sadar Yacob              Removing the Alert  on /acc/salesteamedit.jsp? when the Edit Link is clicked on the AccountTeam vf page
 * Feb  1st, 2016     Sadar Yacob              Case #01177612 : For Users who are not on Accnt Team, hide the Add button (except certain profiles like CSA,SE,Support etc)
 * Feb  5th, 2016     Sadar Yacob              Case #01177612 : Hide the Edit/Delete links if User doesnt have access to Edit/Delete from the Account Team
 * May  5th, 2016     James Wills              Case #01192655 : Show the Business Unit alongside each AE under Account Team
 * June 22nd, 2016    James Wills              Case #01192655 : Account Team Members list link changed from standard to custom VF page
 * Sep 14th, 2016     Paul Kissick             CRM2:I303: Issue due to characters in the account name for Access Requests
*  =====================================================================*/
-->
<apex:page standardController="Account" extensions="AccountTeamMembersList" showheader="true" sidebar="true">
  <apex:includeScript value="/xdomain/xdomain.js"/>
  <apex:includeScript value="/soap/ajax/26.0/connection.js"/>
  <apex:includeScript value="/support/console/26.0/integration.js"/>
  <apex:form id="frm">
    <apex:outputPanel id="fullPage">
      <apex:actionFunction name="deleteATM_AF" action="{!doDelete}" rerender="fullPage">
        <apex:param name="delParam" assignTo="{!selectedId}" value=""/>
      </apex:actionFunction>
      <apex:pageBlock >
        <apex:pageMessages id="msg"/>
          <apex:pageBlockButtons location="top">
            <apex:commandButton value="{!$Label.AccountTeamMembersList_Add}" action="{!checkAccess}" rendered="{!userhasAccess}"  onClick="addMember('{!acc.Id}');"/>
            <!--apex:commandButton value="{!$Label.AccountTeamMembersList_Add}" rendered="{!$ObjectType.Account.updateable}"  onClick="addMember('{!acc.Id}');"/-->
            <apex:commandButton rerender="frm" value="{!$Label.AccountTeamMembersList_Display_Access}" onClick="goToList('{!acc.Id}');" rendered="{!$ObjectType.Account.updateable}" disabled="{!displayAccess}"/>
            <apex:commandButton value="{!$Label.NewAccessRequest}" rendered="{!$ObjectType.Account.updateable}"  onClick="newCase('{!acc.Id}');"/>
          </apex:pageBlockButtons>
          <apex:pageBlockSection columns="1">
          <apex:outputPanel id="showMembersPanel">
            <apex:pageBlockTable value="{!accountTeamMembersToShow}" var="teamMember">
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Action}">
                <apex:outputPanel layout="inline" rendered="{!userhasAccess}"><a onClick="editMember('{!acc.Id}','{!teamMember.member.Id}');" style="cursor: pointer;text-decoration: underline;color: #015ba7;">{!$Label.AccountTeamMembersList_Edit}</a>&nbsp;|&nbsp;</apex:outputPanel>
                <apex:outputPanel layout="inline" rendered="{!userhasDeleteAccess}"><a onClick="deleteATM('{!teamMember.member.Id}');" style="cursor: pointer;text-decoration: underline;color: #015ba7;">{!$Label.AccountTeamMembersList_Del}</a></apex:outputPanel>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Team_Member}">
                <apex:outputLink value="/{!teamMember.member.userId}" target="_blank">
                  <apex:outputField value="{!teamMember.member.user.Name}" />
                </apex:outputLink>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Account_Access}" rendered="{!displayAccess}">
                <apex:outputField value="{!teamMember.member.AccountAccessLevel}"/>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Opportunity_Access}" rendered="{!displayAccess}">
                <apex:outputText value="{!teamMember.opportunityAccess}"/>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Case_Access}" rendered="{!displayAccess}">
                <apex:outputText value="{!teamMember.caseAccess}"/>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Team_Role}">
                <apex:outputField value="{!teamMember.member.TeamMemberRole}"/>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Team_Member_Business_Unit}"> <!-- 24 Nov 2014 Case #1192655 JW-->
                <apex:outputField value="{!teamMember.member.User.Business_Unit__c}"/>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Sales_Team}"> <!-- 11 Mar 15 GC - T-369312 -->
                <apex:outputText value="{!teamMember.member.User.Sales_Team__c}"/>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Sales_Sub_Team}"> <!-- 11 Mar 15 GC - T-369312 -->
                <apex:outputText value="{!teamMember.member.User.Sales_Sub_Team__c}"/>
              </apex:column>
              <apex:column headerValue="{!$Label.AccountTeamMembersList_Date_Added_By}"> <!-- 24 Nov 2014 Case #1227650 JW-->
                <apex:outputText value="{!teamMember.createDate & ' / ' & teamMember.member.CreatedBy.Alias}"/>
              </apex:column>
            </apex:pageBlockTable>
            <br/>
            <apex:commandLink value="{!$Label.AccountTeamMembersList_Show_More}" action="{!showMoreRecords}" rerender="frm" rendered="{!!reachedMax}"/>
            <!-- <a onClick = "showMore();" style="cursor: pointer;text-decoration: underline;"></a> -->
            <apex:outputText value=" | " rendered="{!!reachedMax}" />
            <!-- <a onClick = "goToList('{!acc.Id}');" style="cursor: pointer;text-decoration: underline;" --><!--/a--><!--Case 01192655-->
            <a href="javascript:goToFullList('{!acc.Id}');" style="cursor: pointer;text-decoration: underline;" >{!$Label.AccountTeamMembersList_Go_to_list} ({!listSize})</a><!--Case 01192655-->
          </apex:outputPanel>
        </apex:pageBlockSection>
      </apex:pageBlock>
    </apex:outputPanel>
  </apex:form>
  <script>
    function editMember (accntId, memId) {
      var editUrl = "/acc/salesteamedit.jsp?retURL=/"+accntId+"&id="+memId;
      //alert (editUrl);
      if (sforce.console.isInConsole()) {    
        sforce.console.getEnclosingTabId(function(enclosingResult) {
          sforce.console.getEnclosingPrimaryTabId(function(primaryResult) {
            sforce.console.openSubtab(primaryResult.id, editUrl, true, '', null);
          });
        });
      }
      else {
        window.parent.location.href = editUrl;
      }
      return false;
    }
   
    function deleteATM (atmId) {
      if (confirm('{!JSENCODE($Label.AccountTeamMembersList_Record_Delete_Message)}') ) {
        deleteATM_AF(atmId);
      }
      return false;
    }
    
    function addMember (accntId) {
      var addMemberUrl = "/apex/AccountTeamMembersListEdit?Id="+accntId+"&showAddMemberSection=true";
      if (sforce.console.isInConsole()) {
        sforce.console.getEnclosingTabId(function(enclosingResult) {
          sforce.console.getEnclosingPrimaryTabId(function(primaryResult) {
            sforce.console.openSubtab(primaryResult.id, addMemberUrl, true, '', null);
          });
        });
      }
      else {
        window.parent.location.href = addMemberUrl;
      }
      return false;
    }
    
    function goToFullList (accntId) {
      var addMemberUrl = "/apex/AccountTeamMembersList_Full?Id="+accntId+"&showAddMemberSection=true";
      window.parent.location.href = addMemberUrl;
      return false;
    }
    
    function goToList (accntId) {
      var listUrl = "/acc/accteammemberlist.jsp?id="+accntId;
      if (sforce.console.isInConsole()) {
        sforce.console.getEnclosingTabId(function(enclosingResult) {
          sforce.console.getEnclosingPrimaryTabId(function(primaryResult) {
            sforce.console.openSubtab(primaryResult.id, listUrl, true, '', null);
          });
        });
      }
      else {
        window.parent.location.href = listUrl;
      }
    }
  
    function newCase (accntId) {
      // CRM2:I303: Wrapped account names in URLENCODE 
      var addCaseUrl = "/500/e?retURL=%2F500%2Fo&cas14={!URLENCODE('New Access Request for '+Account.Name)}&cas4={!URLENCODE(Account.Name)}&cas4_lkid={!Account.Id}&RecordType=012i00000019lV2&ent=Case";
      // TODO: Remove hard coded Record Type ID above.
      if (sforce.console.isInConsole()) {
        sforce.console.getEnclosingTabId(function(enclosingResult) {
          sforce.console.getEnclosingPrimaryTabId(function(primaryResult) {
            sforce.console.openSubtab(primaryResult.id, addCaseUrl, true, '', null);
          });
        });
      }
      else {
        window.parent.location.href = addCaseUrl;
      } 
      return false;
    }
  </script>
</apex:page>